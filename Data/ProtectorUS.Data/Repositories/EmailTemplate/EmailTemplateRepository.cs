﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class EmailTemplateRepository : Repository<EmailTemplate, long>, IEmailTemplateRepository
    {
        public EmailTemplateRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
