﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class PaymentMethodRepository : Repository<PaymentMethod, long>, IPaymentMethodRepository
    {
        public PaymentMethodRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
