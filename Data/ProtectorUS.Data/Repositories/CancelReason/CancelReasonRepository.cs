﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class CancelReasonRepository : Repository<CancelReason, long>, ICancelReasonRepository
    {
        public CancelReasonRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
