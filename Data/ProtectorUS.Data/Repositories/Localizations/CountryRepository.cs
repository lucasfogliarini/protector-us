﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class CountryRepository : Repository<Country, long>, ICountryRepository
    {
        public CountryRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
