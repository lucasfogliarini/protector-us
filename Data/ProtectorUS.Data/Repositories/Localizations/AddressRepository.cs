﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System;
    using System.Linq;

    public class AddressRepository : Repository<Address, long>, IAddressRepository
    {
        public AddressRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory) { }
        /// <summary>
        /// Find a entity colection by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<Address> AllDeleted()
        {
            DatabaseContext.SoftDeleteFilterIsActive = false;

            var deleted = this.FindAll(x => x.IsDeleted == true);

            DatabaseContext.SoftDeleteFilterIsActive = true;

            return deleted;
        }
        /// <summary>
        /// Find a entity colection async by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public async Task<IEnumerable<Address>> AllDeletedAsync()
        {
            DatabaseContext.SoftDeleteFilterIsActive = false;

            var deleted = await this.FindAllAsync(x => x.IsDeleted == true);

            DatabaseContext.SoftDeleteFilterIsActive = true;
            return deleted;
        }
    }
}
