﻿using ProtectorUS.Model;
using ProtectorUS.Model.Repositories;
using System.Linq;

namespace ProtectorUS.Data.Repositories
{
    public class RatingPlanFormVersionRepository : Repository<RatingPlanFormVersion, long>, IRatingPlanFormVersionRepository
    {
        public RatingPlanFormVersionRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
            : base(connectionFactory)
        {
        }

        public RatingPlanFormVersion GetLatestVersion(string productAlias, long stateId)
        {
            var query = FindAll(rp => rp.RatingPlanForm.Product.Alias == productAlias && rp.RatingPlanForm.StateId == stateId,
                        rp => rp.RatingPlanForm, rp =>
                        rp.RatingPlanForm.Product).OrderBy(rp => rp.Version);
            RatingPlanFormVersion latestRatingPlanForm = query.LastOrDefault();
            Reload(latestRatingPlanForm);
            return latestRatingPlanForm;
        }
    }
}
