﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class QuotationDeliveryRepository : Repository<QuotationDelivery, long>, IQuotationDeliveryRepository
    {
        public QuotationDeliveryRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
