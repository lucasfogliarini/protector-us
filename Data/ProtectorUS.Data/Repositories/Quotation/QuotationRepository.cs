﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System;
    public class QuotationRepository : Repository<Quotation, long>, IQuotationRepository
    {
        public QuotationRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }

        public Quotation Find(Guid hash)
        {
            return Find(q=>q.Hash == hash, //&&
                    //q.Status == ActivationStatus.DetailsCompleted && 
                   // q.InsuredId != null && 
                   // q.BusinessId != null,
                    q => q.BrokerageProduct,
                    q => q.BrokerageProduct.Product,
                    q => q.BrokerageProduct.WebpageProducts);
        }
    }
}
