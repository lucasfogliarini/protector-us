﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class DeclineReasonRepository : Repository<DeclineReason, long>, IDeclineReasonRepository
    {
        public DeclineReasonRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}