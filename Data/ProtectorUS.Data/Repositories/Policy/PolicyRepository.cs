﻿using System.Linq;

namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class PolicyRepository : Repository<Policy, long>, IPolicyRepository
    {
        public PolicyRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }

        public int? GetLastSequentialNumber(long productId)
        {
            return Set.Include(p => p.Condition).Where(p=>p.Condition.ProductId == productId).Max(p => (int?)p.SequentialNumber);
        }

        public int GetLastRenewal(int sequentialNumber)
        {
             return Set.Count(p => p.SequentialNumber == sequentialNumber);
        }
    }
}
