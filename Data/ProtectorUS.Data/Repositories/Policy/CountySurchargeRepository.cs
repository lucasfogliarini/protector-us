﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Linq;
    public class CountySurchargeRepository : Repository<CountySurcharge, long>, ICountySurchargeRepository
    {
        public CountySurchargeRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
        public SurchargePercentage GetPercentage(long cityId)
        {
            var countySurcharge = Find(e => e.Cities.Any(c => c.Id == cityId), i => i.Percentages, i=> i.Cities);
            return countySurcharge?.Percentages?.OrderByDescending(e => e.Id).FirstOrDefault();
        }
    }
}
