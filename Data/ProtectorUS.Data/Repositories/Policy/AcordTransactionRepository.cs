﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class AcordTransactionRepository : Repository<AcordTransaction, long>, IAcordTransactionRepository
    {
        public AcordTransactionRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
