﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Linq;
    public class StateSurchargeRepository : Repository<StateSurcharge, long>, IStateSurchargeRepository
    {
        public StateSurchargeRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
        public SurchargePercentage GetPercentage(long stateId)
        {
            return Find(e => e.StateId == stateId, e => e.Percentages)?.Percentages?.OrderByDescending(e => e.Id).FirstOrDefault();
        }
    }
}
