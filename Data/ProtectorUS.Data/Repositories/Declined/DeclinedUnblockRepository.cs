﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class DeclinedUnblockRepository : Repository<DeclinedUnblock, long>, IDeclinedUnblockRepository
    {
        public DeclinedUnblockRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
