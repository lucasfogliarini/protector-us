﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class DeclinedBlockRepository : Repository<DeclinedBlock, long>, IDeclinedBlockRepository
    {
        public DeclinedBlockRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
