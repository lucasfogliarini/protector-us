﻿using ProtectorUS.Model;
using ProtectorUS.Model.Repositories;

namespace ProtectorUS.Data.Repositories
{
    /// <summary>
    /// Implements the <see cref="IRefreshTokenRepository"/> interface.
    /// </summary>
    public class RefreshTokenRepository : Repository<RefreshToken, long>, IRefreshTokenRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshTokenRepository"/> class.
        /// </summary>
        /// <param name="connectionFactory">the database context connection factory.</param>
        public RefreshTokenRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
            : base(connectionFactory)
        {
        }        
    }
}
