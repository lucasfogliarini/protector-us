﻿using ProtectorUS.Model;
using ProtectorUS.Model.Repositories;

namespace ProtectorUS.Data.Repositories
{
    /// <summary>
    /// Implements the <see cref="IAppClientRepository"/> interface.
    /// </summary>
    public class AppClientRepository : Repository<AppClient, long>, IAppClientRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppClientRepository"/> class.
        /// </summary>
        /// <param name="connectionFactory">the database context connection factory.</param>
        public AppClientRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
            : base(connectionFactory)
        {
        }        
    }
}
