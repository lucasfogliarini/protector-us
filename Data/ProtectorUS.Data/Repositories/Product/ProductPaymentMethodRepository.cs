﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class ProductPaymentMethodRepository : Repository<ProductPaymentMethod, long>, IProductPaymentMethodRepository
    {
        public ProductPaymentMethodRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
