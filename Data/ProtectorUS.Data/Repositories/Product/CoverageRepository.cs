﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Collections.Generic;
    public class CoverageRepository : Repository<Coverage, long>, ICoverageRepository
    {
        public CoverageRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }

        public IEnumerable<Coverage> FindAll(long productId)
        {
            return FindAll(c => c.ProductId == productId);
        }
    }
}
