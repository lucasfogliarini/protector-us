﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class ProductConditionRepository : Repository<ProductCondition, long>, IProductConditionRepository
    {
        public ProductConditionRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
