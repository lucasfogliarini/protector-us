﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProtectorUS.Data
{

    using Model.Repositories;
    using Model;
    using Exceptions;
    /// <summary>
    /// Defines basic behavior for repositories.
    /// </summary>
    /// <typeparam name="E">Entity type.</typeparam>
    /// <typeparam name="K">Entity unique identifier property type.</typeparam>
    public class Repository<E, K> : IRepository<E, K>, IRepositoryList<E> where E : Entity<K> where K : IComparable<K>, IEquatable<K>
    {
        protected Repository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
        {
            DatabaseContext = connectionFactory.Get();
        }

        protected void Reload(E entity)
        {
            if (entity != null)
                DatabaseContext.Entry(entity).Reload();
        }

        private void ThrowNotFoundIfNull(E entity, string id = null)
        {
            if (entity == null)
            {
                entity = Activator.CreateInstance(typeof(E), true) as E;
                if (id == null)
                    throw new NotFoundException(e => e.EL026, entity.EntityName);
                else
                    throw new NotFoundException(e => e.EL005, entity.EntityName, id);
            }
        }

        #region Properties
        /// <summary>
        /// Query AsNoTracking
        /// </summary>
        protected DbSet<E> Set
        {
            get { return this.DatabaseContext.Set<E>(); }
        }
        protected ProtectorUSDatabase DatabaseContext { get; private set; }

        #endregion

        #region IRepository Methods
        /// <summary>
        /// Gets a single <typeparamref name="E"/>.
        /// </summary>
        /// <param name="id">the entity's unique identifier.</param>
        /// <returns>an instance of <typeparamref name="E"/>.</returns>
        public E Get(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            E entity = Set.Include(includeExpressions).FirstOrDefault(e => e.Id.Equals(id));

            // touchs (uiii) an inner object to load its data, this way, even with a "HasOptional" mapping we can delete it using "= null";
            if (entity != null)
            {
                entity = LoadProperty(entity);
            }

            return entity;
        }

        /// <summary>
        /// Gets a single <typeparamref name="E"/>.
        /// </summary>
        /// <param name="id">the entity's unique identifier.</param>
        /// <returns>an instance of <typeparamref name="E"/>.</returns>
        public E GetAsNoTracking(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            E entity = Set.Include(includeExpressions).AsNoTracking().FirstOrDefault(e => e.Id.Equals(id));

            // touchs (uiii) an inner object to load its data, this way, even with a "HasOptional" mapping we can delete it using "= null";
            if (entity != null)
            {
                entity = LoadProperty(entity);
            }

            return entity;
        }

        /// <summary>
        /// Gets a single <typeparamref name="E"/> asynchronous.
        /// </summary>
        /// <param name="id">the entity's unique identifier.</param>
        /// <returns>an instance of <typeparamref name="E"/>.</returns>
        public async Task<E> GetAsync(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            return await Set.Include(includeExpressions).FirstOrDefaultAsync(i => i.Id.Equals(id));
        }

        /// <summary>
        /// Try to get the Entity, if null throw NotFoundException
        /// </summary>
        public E TryGet(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            if (id.CompareTo(default(K)) != 1)
            {
                throw new ArgumentRequiredException(e => e.ES004, $"{typeof(E).Name}Id");
            }
            E entity = Get(id, includeExpressions);
            ThrowNotFoundIfNull(entity, id.ToString());

            return entity;
        }

        public async Task<E> TryGetAsync(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            if (id.CompareTo(default(K)) != 1)
            {
                throw new ArgumentRequiredException(e => e.ES004, $"{typeof(E).Name}Id");
            }
            E entity = await GetAsync(id, includeExpressions);
            ThrowNotFoundIfNull(entity, id.ToString());

            return entity;
        }

        /// <summary>
        /// Try to get the Entity, if null throw NotFoundException
        /// </summary>
        public E TryGetAsNoTracking(K id, params Expression<Func<E, object>>[] includeExpressions)
        {
            if (id.CompareTo(default(K)) != 1)
            {
                throw new ArgumentRequiredException(e => e.ES004, $"{ typeof(E).Name}Id");
            }
            E entity = GetAsNoTracking(id, includeExpressions);
            ThrowNotFoundIfNull(entity, id.ToString());

            return entity;
        }

        /// <summary>
        /// Try to find the Entity, if null throw NotFoundException
        /// </summary>
        public E TryFind(Func<E, bool> match, params Expression<Func<E, object>>[] includeExpressions)
        {
            E entity = Find(match, includeExpressions);
            ThrowNotFoundIfNull(entity);

            return entity;
        }

        public async Task<E> TryFindAsync(Expression<Func<E, bool>> match, params Expression<Func<E, object>>[] includeExpressions)
        {
            E entity = await FindAsync(match, includeExpressions);
            ThrowNotFoundIfNull(entity);

            return entity;
        }

        /// <summary>
        /// Adds an entity.
        /// </summary>
        /// <param name="entity">an instance of <typeparamref name="E"/>.</param>
        public E Add(E entity)
        {
            return this.Set.Add(entity);
        }

        /// <summary>
        /// Updates an entity.
        /// </summary>
        /// <param name="entity">an instance of <typeparamref name="E"/>.</param>
        public void Update(E entity)
        {
            this.Set.Attach(entity);
            this.DatabaseContext.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Removes an entity.
        /// </summary>
        /// <param name="entity">an instance of <typeparamref name="E"/>.</param>
        public virtual void Remove(E entity)
        {
            if (entity is ISoftDeletable)
            {
                ISoftDeletable softdelete = entity as ISoftDeletable;
                softdelete.Delete();
            }
            this.Set.Remove(entity);
        }

        /// <summary>
        /// Find a entity async by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>an instance of <typeparamref name="E"/>.</returns>
        public E Find(Func<E, bool> match, params Expression<Func<E, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions).FirstOrDefault(match);
        }

        /// <summary>
        /// Find a entity async by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>an instance of <typeparamref name="E"/>.</returns>
        public async Task<E> FindAsync(Expression<Func<E, bool>> match, params Expression<Func<E, object>>[] includeExpressions)
        {
            return await Set.Include(includeExpressions).FirstOrDefaultAsync(match);
        }

        #endregion IRepository Methods

        #region IRepositoryList Methods

        /// <summary>
        /// Get all <typeparamref name="E"/>.
        /// </summary>
        /// <returns>a collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<E> GetAll(params Expression<Func<E, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions);
        }

        /// <summary>
        /// Find a entity collection by expression query.
        /// </summary>
        /// <param name="filter">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<E> FindAll(Func<E, bool> filter, params Expression<Func<E, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions).Where(filter);
        }
        /// <summary>
        /// Find a entity collection by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<E> FindAll(Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions)
        {
            return Filter(filters, includeExpressions);
        }

        /// <summary>
        /// Find a entity collection by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<E> FindAll(string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions)
        {
            var query = Filter(filters, includeExpressions);
            return query.ApplySort(sort);
        }

        /// <summary>
        /// Find a entity collection by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        private IQueryable<E> Filter(Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions)
        {
            IEnumerable<E> query = Set.Include(includeExpressions);
            foreach (var filter in filters)
            {
                query = query.Where(filter);
            }
            return query.AsQueryable();
        }

        public async Task<IEnumerable<E>> FindAllAsync(Expression<Func<E, bool>> filter, params Expression<Func<E, object>>[] includeExpressions)
        {
            return await Set.Include(includeExpressions).Where(filter).ToListAsync();
        }
        /// <summary>
        /// Get all async<typeparamref name="E"/>.
        /// </summary>
        /// <returns>a collection of <typeparamref name="E"/>.</returns>
        public async Task<IEnumerable<E>> GetAllAsync(params Expression<Func<E, object>>[] includeExpressions)
        {
            return await Set.Include(includeExpressions).ToListAsync();
        }
        /// <summary>
        /// Find a entity collection by expression query and page.
        /// </summary>
        /// <param name="page">Current page</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sort">Sort value</param>
        /// <param name="includes">Includes</param>
        /// <returns>A paginated collection of <typeparamref name="E"/></returns>
        public PagedResult<E> FindByPage(int page, int pageSize, string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions)
        {
            filters = filters ?? new Filters<E>();
            var queryaux = Filter(filters, includeExpressions).ApplySort(sort);
            var query = queryaux.Skip(pageSize * (page - 1)).Take(pageSize);
            var itens = query.ToList();
            var pageOfItens = new PagedResult<E>
            {
                TotalItems = queryaux.Count(),
                ItemsPerPage = pageSize,
                Items = itens,
                Page = page,
                Sort = sort
            };
            return pageOfItens;
        }
        public async Task<PagedResult<E>> FindByPageAsync(int page, int pageSize, string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions)
        {
            filters = filters ?? new Filters<E>();
            var queryaux = Filter(filters, includeExpressions).ApplySort(sort);
            var query = queryaux.Skip(pageSize * (page - 1)).Take(pageSize);
            var itensAsync = await query.ToListAsync();
            var pageOfItens = new PagedResult<E>
            {
                TotalItems = query.Count(),
                ItemsPerPage = pageSize,
                Items = itensAsync,
                Page = page,
                Sort = sort
            };
            return pageOfItens;
        }

        /// <summary>
        /// Get all  by page.
        /// </summary>
        /// <param name="page">Current page</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sort">Sort value</param>
        /// <returns>A paginated collection of <typeparamref name="E"/></returns>
        public PagedResult<E> GetAllByPage(int page, int pageSize, string sort, params Expression<Func<E, object>>[] includeExpressions)
        {
            var query = this.Set.Include(includeExpressions);
            var pageOfItens = new PagedResult<E>
            {
                TotalItems = query.Count(),
                ItemsPerPage = pageSize,
                Items = query
                .ApplySort(sort)
                .Skip(pageSize * (page - 1))
                .Take(pageSize).ToList(),
            };
            return pageOfItens;
        }
        #endregion

        #region private methods
        private E LoadProperty(E entity)
        {
            foreach (var p in entity.GetType().GetProperties())
            {
                if (p.PropertyType.Namespace != null && p.PropertyType.Namespace.Contains("ProtectorUS.Model"))
                {
                    try
                    {
                        ((IObjectContextAdapter)this.DatabaseContext).ObjectContext.LoadProperty(entity, p.Name);
                    }
                    catch (InvalidOperationException) { }
                }
            }
            return entity;
        }

        #endregion

    }
}