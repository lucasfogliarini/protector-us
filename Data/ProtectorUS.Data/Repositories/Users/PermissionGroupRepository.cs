﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class PermissionGroupRepository : Repository<PermissionGroup, long>, IPermissionGroupRepository
    {
        public PermissionGroupRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
