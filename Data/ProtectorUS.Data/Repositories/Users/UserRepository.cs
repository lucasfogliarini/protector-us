﻿using System.Linq;
namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Collections.Generic;
    public abstract class UserRepository<TUser> : Repository<TUser, long>, IUserRepository<TUser> where TUser : User
    {
        public UserRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
        public IEnumerable<Permission> GetPermissions(long userId)
        {
            var query = from u in Set
                        from p in u.Profiles
                        from pe in p.Permissions
                        where u.Id == userId
                        select pe;
            List<Permission> permissions = query.ToList();


            IEnumerable<ExceptedPermission> exceptedPermissions = from u in Set
                                                                  from ep in u.ExceptedPermissions
                                                                  where u.Id == userId
                                                                  select ep;

            foreach (var excepted in exceptedPermissions)
            {
                if (excepted.Allowed)
                {
                    permissions.AddIfNot(excepted.Permission, (n, i) => n.Id == i.Id);
                }
                else
                {
                    permissions.RemoveAll(p => p.Id == excepted.Id);
                }
            }

            return permissions;
        }
        public bool IsUsed(string email, string exceptEmail = null)
        {
            var user = Find(x => x.Email == email && x.Email != exceptEmail);
            return user != null;
        }
    }

    public class UserRepository : UserRepository<User>, IUserRepository
    {
        public UserRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
