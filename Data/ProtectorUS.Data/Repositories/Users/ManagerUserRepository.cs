﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class ManagerUserRepository : UserRepository<ManagerUser>, IManagerUserRepository
    {
        public ManagerUserRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
