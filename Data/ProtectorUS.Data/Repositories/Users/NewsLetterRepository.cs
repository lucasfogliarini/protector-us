﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class NewsLetterRepository : Repository<NewsLetter, long>, INewsLetterRepository
    {
        public NewsLetterRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
