﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ProtectorUS.Data;
    using System.Linq;
    using Exceptions;
    public class BrokerRepository : UserRepository<Broker>, IBrokerRepository
    {
        public BrokerRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory) { }
        /// <summary>
        /// Find a entity colection by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public IEnumerable<Broker> AllDeleted()
        {
            DatabaseContext.SoftDeleteFilterIsActive = false;

            var deleted = this.FindAll(x => x.IsDeleted == true);

            DatabaseContext.SoftDeleteFilterIsActive = true;

            return deleted;
        }
        /// <summary>
        /// Find a entity colection async by expression query.
        /// </summary>
        /// <param name="match">Linq query</param>
        /// <returns>collection of <typeparamref name="E"/>.</returns>
        public async Task<IEnumerable<Broker>> AllDeletedAsync()
        {
            DatabaseContext.SoftDeleteFilterIsActive = false;

            var deleted = await this.FindAllAsync(x => x.IsDeleted == true);

            DatabaseContext.SoftDeleteFilterIsActive = true;
            return deleted;
        }

        public Broker GetMasterByBrokerageProduct(long brokerageProductId)
        {
            var query = from b in Set
                        from bproduct in b.Brokerage.BrokerageProducts
                        from p in b.Profiles
                        where p.Id == Profile.Ids.MasterBroker && bproduct.Id == brokerageProductId
                        select b;
            return query.FirstOrDefault();
        }
        public Broker GetMasterByBrokerage(long brokerageId)
        {
            var query = (from b in Set
                        from p in b.Profiles
                        where p.Id == Profile.Ids.MasterBroker && b.BrokerageId == brokerageId
                         select new
                         {
                             Broker = b,
                             Brokerage = b.Brokerage
                         }).FirstOrDefault();
            if (query == null)
            {
                throw new ArgumentInvalidException(e => e.EL022, "Brokerage");
            }
            query.Broker.Brokerage = query.Brokerage;
            return query.Broker;
        }

        /// <summary>
        /// Inactivate brokers.
        /// </summary>
        /// <param name="entity">an instance of <typeparamref name="E"/>.</param>
        public int Inactivate(IEnumerable<long> actives)
        {
            var inactives = this.FindAll(x => !actives.Contains(x.Id) && x.Status == UserStatus.Active);
            if (inactives == null || inactives.Count() <= 0)
                return 0;

            inactives.ToList().ForEach(entity =>
            {
                this.Set.Attach(entity);
                entity.Status = UserStatus.Inactive;
                Update(entity);
            });
            return inactives.Count();
        }

        /// <summary>
        /// Reactivate inactive brokers.
        /// </summary>
        /// <param name="entity">an instance of <typeparamref name="E"/>.</param>
        public int Reactivate(IEnumerable<long> actives)
        {
            var mustReactivate = this.FindAll(x => actives.Contains(x.Id) && x.Status == UserStatus.Inactive);
            if (mustReactivate == null || mustReactivate.Count() <= 0)
                return 0;

            mustReactivate.ToList().ForEach(entity =>
            {
                this.Set.Attach(entity);
                entity.Status = UserStatus.Active;
                Update(entity);
            });
            return mustReactivate.Count();
        }

    }
}
