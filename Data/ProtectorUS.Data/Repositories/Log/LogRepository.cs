﻿using ProtectorUS.Model;
using ProtectorUS.Model.Repositories;

namespace ProtectorUS.Data.Repositories
{
    /// <summary>
    /// Implements the <see cref="ILogRepository"/> interface.
    /// </summary>
    public class LogRepository : Repository<Log, long>, ILogRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogRepository"/> class.
        /// </summary>
        /// <param name="connectionFactory">the database context connection factory.</param>
        public LogRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
            : base(connectionFactory)
        {
        }        
    }
}
