﻿using ProtectorUS.Model;
using ProtectorUS.Model.Repositories;

namespace ProtectorUS.Data.Repositories
{
    /// <summary>
    /// Implements the <see cref="ICampaignRepository"/> interface.
    /// </summary>
    public class CampaignRepository : Repository<Campaign, long>, ICampaignRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CampaignRepository"/> class.
        /// </summary>
        /// <param name="connectionFactory">the database context connection factory.</param>
        public CampaignRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory)
            : base(connectionFactory)
        {
        }        
    }
}
