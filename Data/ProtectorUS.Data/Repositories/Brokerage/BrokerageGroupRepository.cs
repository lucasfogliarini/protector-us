﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class BrokerageGroupRepository : Repository<BrokerageGroup, long>, IBrokerageGroupRepository
    {
        public BrokerageGroupRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
