﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;

    public class BrokerageRepository : Repository<Brokerage, long>, IBrokerageRepository
    {
        public BrokerageRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {

        }

    }
    
}
