﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class WebpageViewRepository : Repository<WebpageView, long>, IWebpageViewRepository
    {
        public WebpageViewRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
