﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class BrokerageProductRepository : Repository<BrokerageProduct, long>, IBrokerageProductRepository
    {
        public BrokerageProductRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
