﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class WebpageProductRepository : Repository<WebpageProduct, long>, IWebpageProductRepository
    {
        public WebpageProductRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
