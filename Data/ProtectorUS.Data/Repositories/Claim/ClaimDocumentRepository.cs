﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class ClaimDocumentRepository : Repository<ClaimDocument, long>, IClaimDocumentRepository
    {
        public ClaimDocumentRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
