﻿
namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    using System.Linq;
    public class ClaimRepository : Repository<Claim, long>, IClaimRepository
    {
        public ClaimRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
        public int? GetLastSequentialNumber()
        {
            return Set.Max(p => (int?)p.SequentialNumber);
        }
    }
}
