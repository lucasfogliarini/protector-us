﻿namespace ProtectorUS.Data.Repositories
{
    using Model.Repositories;
    using Model;
    public class ThirdPartyRepository : Repository<ThirdParty, long>, IThirdPartyRepository
    {
        public ThirdPartyRepository(IConnectionFactory<ProtectorUSDatabase> connectionFactory) : base(connectionFactory)
        {
        }
    }
}
