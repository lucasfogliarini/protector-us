﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System;
using System.Threading;

namespace ProtectorUS.Data
{
    using Model;
    using Mappings;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Database context.
    /// </summary>
    [DbConfigurationType(typeof(ProtectorUSDbConfiguration))]
    public class ProtectorUSDatabase : DbContext, IDbContext
    {
        /// <summary>
        /// Include soft deleted records in queries.
        /// </summary>
        public bool SoftDeleteFilterIsActive { get; set; } = true;
        /// <summary>
        /// Initializes a new instance of <see cref="ProtectorUSDatabase"/> class.
        /// </summary>
        public ProtectorUSDatabase()
            : base("protectorUS")
        {
            Configuration.LazyLoadingEnabled = false;
        }
        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        ///                 before the model has been locked down and used to initialize the context.  The default
        ///                 implementation of this method does nothing, but it can be overridden in a derived class
        ///                 such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        ///                 is created.  The model for that context is then cached and is for all further instances of
        ///                 the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///                 property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///                 More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///                 classes directly.
        /// </remarks>

        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new AddressMap());
            modelBuilder.Configurations.Add(new BrokerageMap());
            modelBuilder.Configurations.Add(new BrokerageProductMap());
            modelBuilder.Configurations.Add(new BrokerageGroupMap());
            modelBuilder.Configurations.Add(new BrokerMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new InsuredMap());
            modelBuilder.Configurations.Add(new ManagerUserMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new PhoneMap());
            modelBuilder.Configurations.Add(new RegionMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new ProfileMap());
            modelBuilder.Configurations.Add(new ExceptedPermissionMap());
            modelBuilder.Configurations.Add(new PermissionMap());
            modelBuilder.Configurations.Add(new PermissionGroupMap());
            modelBuilder.Configurations.Add(new DeclinedRuleMap());
            modelBuilder.Configurations.Add(new DeclinedMap());
            modelBuilder.Configurations.Add(new DeclinedUnblockMap());
            modelBuilder.Configurations.Add(new DeclinedBlockMap());
            modelBuilder.Configurations.Add(new ClaimMap());
            modelBuilder.Configurations.Add(new WebpageProductMap());
            modelBuilder.Configurations.Add(new EndorsementMap());
            modelBuilder.Configurations.Add(new CampaignMap());
            modelBuilder.Configurations.Add(new EmailMarketingMap());
            modelBuilder.Configurations.Add(new BrochureMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new PolicyMap());
            modelBuilder.Configurations.Add(new PaymentMethodMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new ProductPaymentMethodMap());
            modelBuilder.Configurations.Add(new QuotationDeliveryMap());
            modelBuilder.Configurations.Add(new QuotationMap());
            modelBuilder.Configurations.Add(new AppClientMap());
            modelBuilder.Configurations.Add(new RefreshTokenMap());
            modelBuilder.Configurations.Add(new ReasonMap());
            modelBuilder.Configurations.Add(new CancelReasonMap());
            modelBuilder.Configurations.Add(new CoverageMap());
            modelBuilder.Configurations.Add(new RatingPlanFormMap());
            modelBuilder.Configurations.Add(new DeviceMap());
            modelBuilder.Configurations.Add(new NotificationMap());
            modelBuilder.Configurations.Add(new SeedHistoryMap());
            modelBuilder.Configurations.Add(new RatingPlanFormVersionMap());
            modelBuilder.Configurations.Add(new NewsLetterMap());
            modelBuilder.Configurations.Add(new ProductConditionMap());
            modelBuilder.Configurations.Add(new EmailTemplateMap());
            modelBuilder.Configurations.Add(new SurchargeMap());
            modelBuilder.Configurations.Add(new CountySurchargeMap());
            modelBuilder.Configurations.Add(new StateSurchargeMap());
            modelBuilder.Configurations.Add(new SurchargePercentageMap());
            modelBuilder.Configurations.Add(new AcordTransactionMap());

            var conv = new AttributeToTableAnnotationConvention<SoftDeleteAttribute, string>(
                "SoftDeleteColumnName",
                (type, attributes) => attributes.Single().ColumnName);

            modelBuilder.Conventions.Add(conv);


        }

        /// <summary>
        /// This method is called for commit changes in database and log data changes.
        ///                 How it work: if the class implements the interface IAuditableEntity, 
        ///                 that have the properties CreatedBy and CreatedDate whe go put the actualy date and the current
        ///                 logged user
        /// </summary>
        /// <remarks>
        /// For this work, we need to set Thread.CurrentPrincipal.Identity.Name or change the change the implementation 
        /// to get the current user otherwise.
        /// </remarks>
        /// <returns></returns>
        public override int SaveChanges()
        {
            this.SetAuditableFields();

            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync()
        {
            this.SetAuditableFields();
            return base.SaveChangesAsync();
        }
        public void SetAuditableFields()
        {
            var modifiedEntries = ChangeTracker.Entries()
                            .Where(x => x.Entity is IAuditableEntity
                                && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }
        }
    }
}