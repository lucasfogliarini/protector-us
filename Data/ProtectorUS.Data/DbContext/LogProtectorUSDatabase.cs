﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System;

namespace ProtectorUS.Data
{
    using Model;
    using Mappings;
    using System.Collections.Generic;

    /// <summary>
    /// Log context.
    /// </summary>
    [DbConfigurationType(typeof(LogProtectorUSDbConfiguration))]
    public class LogProtectorUSDatabase : DbContext, ITrackerContext
    {
       // public LogTracker LogTracker { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<AuditLogDetail> LogDetails { get; set; }


        /// <summary>
        /// Initializes a new instance of <see cref="LogProtectorUSDatabase"/> class.
        /// </summary>
        public LogProtectorUSDatabase()
            : base("logProtectorUS")
        {
            Database.Initialize(false);
        }
        
        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        ///                 before the model has been locked down and used to initialize the context.  The default
        ///                 implementation of this method does nothing, but it can be overridden in a derived class
        ///                 such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        ///                 is created.  The model for that context is then cached and is for all further instances of
        ///                 the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///                 property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///                 More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///                 classes directly.
        /// </remarks>

        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.Add(new AuditLogDetailMap());
            modelBuilder.Configurations.Add(new AuditLogMap());
            modelBuilder.Configurations.Add(new LogMap());
        }

        /// <summary>
        /// This method is called for commit changes in Log and log data changes.
        ///                 How it work: if the class implements the interface IAuditableEntity, 
        ///                 that have the properties CreatedBy and CreatedDate whe go put the actualy date and the current
        ///                 logged user
        /// </summary>
        /// <remarks>
        /// For this work, we need to set Thread.CurrentPrincipal.Identity.Name or change the change the implementation 
        /// to get the current user otherwise.
        /// </remarks>
        /// <returns></returns>
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        /// <summary>
        ///     Get all logs for the given model type
        /// </summary>
        /// <typeparam name="TEntity">Type of domain model</typeparam>
        /// <returns></returns>
        public IQueryable<AuditLog> GetLogs<TEntity>()
        {
            IEnumerable<string> entityTypeNames = EntityTypeNames<TEntity>();
            return this.AuditLog.Where(x => entityTypeNames.Contains(x.TypeFullName));

        }

        /// <summary>
        ///     Get all logs for the given entity name
        /// </summary>
        /// <param name="entityName">full name of entity</param>
        /// <returns></returns>
        public IQueryable<AuditLog> GetLogs(string entityName)
        {
            return this.AuditLog.Where(x => x.TypeFullName == entityName);
        }

        /// <summary>
        ///     Get all logs for the given model type for a specific record
        /// </summary>
        /// <typeparam name="TEntity">Type of domain model</typeparam>
        /// <param name="primaryKey">primary key of record</param>
        /// <returns></returns>
        public IQueryable<AuditLog> GetLogs<TEntity>(object primaryKey)
        {
            string key = primaryKey.ToString();
            IEnumerable<string> entityTypeNames = EntityTypeNames<TEntity>();

            return this.AuditLog.Where(x => entityTypeNames.Contains(x.TypeFullName) && x.RecordId == key);

        }

        /// <summary>
        ///     Get all logs for the given entity name for a specific record
        /// </summary>
        /// <param name="entityName">full name of entity</param>
        /// <param name="primaryKey">primary key of record</param>
        /// <returns></returns>
        public IQueryable<AuditLog> GetLogs(string entityName, object primaryKey)
        {
            string key = primaryKey.ToString();
            return this.AuditLog.Where(x => x.TypeFullName == entityName && x.RecordId == key);

        }

        private IEnumerable<string> EntityTypeNames<TEntity>()
        {
            Type entityType = typeof(TEntity);
            return typeof(TEntity).Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(entityType) || t.FullName == entityType.FullName).Select(m => m.FullName);
        }
    }
}