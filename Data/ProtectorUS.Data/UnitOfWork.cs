﻿using System.Data.Entity.Validation;
using System.Transactions;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace ProtectorUS.Data
{

    using Model.Repositories;
    using Model;
    using DataLogger;
    using System;
    using Exceptions;
    using Diagnostics.Logging;  
    using System.Diagnostics;
    using System.Data.SqlClient;
    /// <summary>
    /// Implements the <see cref="IUnitOfWork{T}"/> interface.
    /// </summary>

    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly IConnectionFactory<ProtectorUSDatabase> _connectionFactory;
        private ProtectorUSDatabase _context;
        private readonly IConnectionFactory<LogProtectorUSDatabase> _logConnectionFactory;
        private LogProtectorUSDatabase _logcontext;
        private bool disposed = false;
        public ILogger Logger { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="connectionFactory">the database connection factory.</param>
        public UnitOfWork(IConnectionFactory<ProtectorUSDatabase> connectionFactory, IConnectionFactory<LogProtectorUSDatabase> logConnectionFactory, ILogger logger)
        {
            this._connectionFactory = connectionFactory;
            this._logConnectionFactory = logConnectionFactory;
            this.Logger = logger;
            GlobalTrackingConfig.Enabled = true;
            GlobalTrackingConfig.TrackEmptyPropertiesOnAdditionAndDeletion = false;
            GlobalTrackingConfig.DisconnectedContext = true;
            GlobalTrackingConfig.SetSoftDeletableCriteria<ISoftDeletable>(x => x.IsDeleted);
        }

        /// <summary>
        /// Gets or sets the database context.
        /// </summary>
        protected ProtectorUSDatabase ProtectorUSDatabase
        {
            get { return this._context ?? (this._context = this._connectionFactory.Get()); }
        }

        /// <summary>
        /// Gets or sets the database context.
        /// </summary>
        protected LogProtectorUSDatabase LogDatabaseContext
        {
            get { return this._logcontext ?? (this._logcontext = this._logConnectionFactory.Get()); }
        }


        /// <summary>
        /// Commit all context changes to database and have two log levels, made in 3 steps.
        /// Attempt to understand the log flow:
        /// Log in 2 levels (Log4net and EF logDatabase context) by 3 steps
        /// Step 1: with Log4net reported that an entry will be logged(save in Log table)
        /// Step 2: queue a Task(called SaveLog) that asynchronously will be log changes or add (or both) in logDatabase (AuditLog and AuditLogDetails)
        /// Step 3: with Log4net reported that an entry has been terminated to log(save in Log table)
        /// Find in Log table, for each Log entry you will meet a "pair" like this: 
        /// "Attempt to log thread 18" and  "Finish log thread 18"
        /// Its easy to know when a record not been logged, if an error occurred in step 2 and 
        /// the entry not been saved in logDatabase finding in Log table you will only meet the attempt to log ("Attempt to log thread 18") 
        /// and you will not meet the matching record "Finish log 18". 
        /// </summary>
        public void Commit()
        {
            try
            {
                var logTracker = new LogTracker(Logger.UserName, ProtectorUSDatabase, _logConnectionFactory);

                BeforeSave(logTracker);

                // Call the original SaveChanges(), which will save both the changes made and the audit records...Note that added entry auditing is still remaining.
                this.ProtectorUSDatabase.SaveChanges();
               

                AfterSave(logTracker);

            }
            catch (DbEntityValidationException ex)
            {
                this.ProtectorUSDatabase.RollBackChanges();
                string[] validationErrors = ex.EntityValidationErrors.ToNonNullList()
                     .SelectMany(x => x.ValidationErrors)
                     .Select(x => x.ErrorMessage).ToArray();
                throw new DbValidationException(ex, validationErrors);
            }
            catch (Exception ex)
            {
                this.ProtectorUSDatabase.RollBackChanges();
                var baseException = ex.GetBaseException();
                if (baseException is SqlException)
                {
                    ThrowSqlDbException(baseException as SqlException);
                }
                throw baseException;
            }
        }

        /// <summary>
        /// Commit all context changes to database and have two log levels, made in 3 steps.
        /// Attempt to understand the log flow:
        /// Log in 2 levels (Log4net and EF logDatabase context) by 3 steps
        /// Step 1: with Log4net reported that an entry will be logged(save in Log table)
        /// Step 2: queue a Task(called SaveLog) that asynchronously will be log changes or add (or both) in logDatabase (AuditLog and AuditLogDetails)
        /// Step 3: with Log4net reported that an entry has been terminated to log(save in Log table)
        /// Find in Log table, for each Log entry you will meet a "pair" like this: 
        /// "Attempt to log transaction 18" and  "Finish log transaction 18"
        /// Its easy to know when a record not been logged, if an error occurred in step 2 and 
        /// the entry not been saved in logDatabase finding in Log table you will only meet the attempt to log ("Attempt to log thread 18") 
        /// and you will not meet the matching record "Finish log 18". 
        /// </summary>
        public async Task CommitAsync()
        {
            try
            {
                var logTracker = new LogTracker(Logger.UserName, ProtectorUSDatabase, _logConnectionFactory);

                BeforeSave(logTracker);

                // Call the original SaveChanges(), which will save both the changes made and the audit records...Note that added entry auditing is still remaining.
                var commit = await this.ProtectorUSDatabase.SaveChangesAsync();

                AfterSave(logTracker);


            }
            catch (DbEntityValidationException ex)
            {
                this.ProtectorUSDatabase.RollBackChanges();
                string[] validationErrors = ex.EntityValidationErrors.ToNonNullList()
                     .SelectMany(x => x.ValidationErrors)
                     .Select(x => x.ErrorMessage).ToArray();
                throw new DbValidationException(ex, validationErrors);
            }
            catch (Exception ex)
            {
                this.ProtectorUSDatabase.RollBackChanges();
                var baseException = ex.GetBaseException();
                if (baseException is SqlException)
                {
                    ThrowSqlDbException(baseException as SqlException);
                }
                throw baseException;
            }
        }

        /// <summary>
        /// Actions to be executed after EF Save Changes or EF Save Changes Async
        /// </summary>
        /// <param name="logTracker"></param>
        private void AfterSave(LogTracker logTracker)
        {
            logTracker.AddAdditionsLogs();

            if (!logTracker.HasAddsOrChanges())
                return;

            var transtionIdentify = Guid.NewGuid().ToString("N");

            var logInfo = new LogInfo($"Attempt to log transaction {transtionIdentify}", transtionIdentify, logTracker.Logs.ToList());
            Logger.LogData(logInfo);

            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveLog), logInfo);
        }

        /// <summary>
        /// Actions to be executed Before EF Save Changes or EF Save Changes Async
        /// </summary>
        /// <param name="logTracker"></param>
        private void BeforeSave(LogTracker logTracker)
        {
            logTracker.AddModificationsLogs();

            ProtectorUSDatabase.SetAuditableFields();
        }

        private void ThrowSqlDbException(SqlException sqlException)
        {
            if (sqlException.Number == 2601)//uniqueness
            {
                string tableName = sqlException.Message.Match("dbo.", "'");
                string uniqueValue = sqlException.Message.Match("unique_", "_");
                throw new SqlDbException(e => e.EL020, tableName, uniqueValue);//There’s already a {entity} with that {value}
            }
            else if (sqlException.Number == 547)//FK violation
            {
                string tableName = sqlException.Message.Match("table \"dbo.", "\"");
                throw new SqlDbException(e => e.EL022, tableName);//There’s not a {entity} with that id.
            }
            throw sqlException;
        }

        /// <summary>
        /// Save log entity values to logDatabase and 
        /// This method will be queued in a Thread Pool for execute asynchronously
        /// </summary>
        /// <param name="info"></param>
        private void SaveLog(object info)
        {
            LogInfo logInfo = info as LogInfo;
            logInfo.Title = $"Finish log transaction { logInfo.TransactionIdentify}";
            using (ITrackerContext logDB = new LogProtectorUSDatabase())
            {
                logDB.AuditLog.AddRange(logInfo.Logs);
                logDB.SaveChanges();
            }
            Logger.FinishLogData(logInfo.Title);
        }
     
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if(_context != null)
                    _context.Dispose();
                    if (_logcontext != null)
                        _logcontext.Dispose();
                }
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
