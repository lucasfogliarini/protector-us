﻿using ProtectorUS.Model;
using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_9_update_permissions : BaseSeed
    {
        public seed_9_update_permissions(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            UpdatePermissions();
        }
    }
}
