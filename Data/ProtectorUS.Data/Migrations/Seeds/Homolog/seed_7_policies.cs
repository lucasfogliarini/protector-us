﻿using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_7_policies : FakeSeed
    {
        public seed_7_policies(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            SeedAddresses();
            SeedPolicies();
        }
    }
}
