﻿using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_8_campaigns: FakeSeed
    {
        public seed_8_campaigns(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            SeedCampaigns();
        }
    }
}
