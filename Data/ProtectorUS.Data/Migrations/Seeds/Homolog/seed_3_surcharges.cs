﻿using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_3_surcharges : SurchargeSeed
    {
        public seed_3_surcharges(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            SeedStateSurcharges();
            SeedKentuckySurcharges();
        }
    }
}
