﻿using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_5_brokerages : FakeSeed
    {
        public seed_5_brokerages(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {            
            SeedBrokerages();
        }
    }
}
