﻿using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_6_users : FakeSeed
    {
        public seed_6_users(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            SeedUsers();
        }
    }
}
