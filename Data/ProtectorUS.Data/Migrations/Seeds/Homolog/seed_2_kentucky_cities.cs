﻿using ProtectorUS.Model;
using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds.Homolog
{
    public class seed_2_kentucky_cities : SurchargeSeed
    {
        public seed_2_kentucky_cities(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            SeedKentuckyCitiesTaxed();
        }
    }
}
