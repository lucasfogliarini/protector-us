﻿using System.Data.Entity;
namespace ProtectorUS.Data.Migrations.Seeds.Production
{
    public class seed_4_create_ratingplans : RatingPlanSeed
    {
        public seed_4_create_ratingplans(DbContext dbContext) : base(dbContext)
        {
        }
        public override void Up()
        {
            Engineers();
            Accountants();
        }
    }
}