﻿using ProtectorUS.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds
{
    using DocumentDB.Repositories;
    using System.Linq;
    using System.Threading.Tasks;
    using RatingPlanDoc = Model.DocumentDB.RatingPlan;
    public abstract class RatingPlanSeed : Seed
    {
        readonly RatingPlanFormRepository ratingPlanFormDocDbRepository;
        public RatingPlanSeed(DbContext dbContext) : base(dbContext)
        {
            ratingPlanFormDocDbRepository = new RatingPlanFormRepository(new DocumentDB.ConnectionFactory());
        }
        public RatingPlanDoc.RatingPlanForm CreateCommonRatingPlanForm(long formId, int version, string stateName, long productId)
        {
            #region Common Fields

            string totalClaimsLosses = "totalClaimsLosses";

            var commonFields = new List<RatingPlanDoc.Field>()
            {                
                #region Business since
                new RatingPlanDoc.Field()
                {
                    label = "My company has been in business since",
                    name = AccountantsComputer.businessManagementField,
                    type = RatingPlanDoc.Field.monthPicker,
                    placeholder = "Click to select",
                    tooltip = "Please indicate the month and year the company opened for business. Use the MM-YYYY format.",
                    invalidMessage = "You must select a date on the past",
                    classes = "pattern-semi-medium"
                },

                #endregion
                #region Professional Society
                new RatingPlanDoc.Field()
                {
                    labelAfter = "a member of a professional society.",
                    name = AccountantsComputer.professionalMembershipsField,
                    type = RatingPlanDoc.Field.select,
                    classes = "pattern-almost-small",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I am","1", -0.1m ),
                        new RatingPlanDoc.Option("I'm not","0", 0)
                    }
                },
                #endregion
                #region Revenue
  
                new RatingPlanDoc.Field()
                {
                    label = "My business revenue in the last 12 months was approximately $",
                    name = RatingPlanComputer.revenueField,
                    type = RatingPlanDoc.Field.money,
                    placeholder = "Enter amount",
                    tooltip = "Provide your company’s estimated revenue for the previous 12 months."
                },

                #endregion
                #region Written Contracts
                //new RatingPlanDoc.Field()
                //{
                //    name = AccountantsComputer.writtenContractsField,
                //    type = RatingPlanDoc.Field.select,
                //    options = new List<RatingPlanDoc.Option>()
                //    {
                //        new RatingPlanDoc.Option("At Least","atleast", 1),
                //        new RatingPlanDoc.Option("Less Than","lessthan") { isDeclined = true }
                //    }
                //},
                #endregion
                #region Risk Management
                new RatingPlanDoc.Field()
                {
                    label = "Either my staff or I",
                    labelAfter = "attended a loss control seminar or completed a loss control course within the last three years",
                    name = AccountantsComputer.riskManagementField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "Please, inform if either you or members of your staff have attended a loss control seminar or course in the past three years.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("have","1", -0.05m),
                        new RatingPlanDoc.Option("have not","0", 0)
                    }
                },
                #endregion
                #region From Services
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.revenueFromServicesField,
                    type = RatingPlanDoc.Field.select,
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do","1"){ isDeclined = true },
                        new RatingPlanDoc.Option("I do not","0", 1)
                    }
                },
                #endregion
                #region From Segments
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.revenueFromSegmentsField,
                    type = RatingPlanDoc.Field.select,
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("At Least","atleast", 1),
                        new RatingPlanDoc.Option("Less Than","lessthan") { isDeclined = true }
                    }
                },
                #endregion
                #region Existing Policy
                new RatingPlanDoc.Field()
                {
                    name = RatingPlanComputer.existingPolicyField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "Tell us if you have professional liability coverage at this time so we can offer you proper retroactive coverage.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do", "1"),
                        new RatingPlanDoc.Option("I don't", "0")
                    }
                },
                new RatingPlanDoc.Field()
                {
                    label = "My liability insurer is",
                    name = "insuranceCompany",
                    type = RatingPlanDoc.Field.text,
                    placeholder = "Enter insurer name",
                    tooltip = "Identify the insurance company that has underwritten your current policy.",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(RatingPlanComputer.existingPolicyField, "1") { Operator = RatingPlanDoc.Condition.EqualsOperator }
                    }
                },

                new RatingPlanDoc.Field()
                {
                    label = "The retroactive date on the current policy is",
                    name = RatingPlanComputer.retroactiveDateField,
                    type = RatingPlanDoc.Field.datePickerRetroactive,
                    tooltip = "We need to know the date your current policy went into effect, sometimes known as the retroactive date. Use the MM-DD-YYYY format.",
                    invalidMessage = "You must select a date on the past",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(RatingPlanComputer.existingPolicyField, "1") { Operator = RatingPlanDoc.Condition.EqualsOperator }
                    }
                },

                #endregion
                #region Claims
                new RatingPlanDoc.Field()
                {
                    label = "I had",
                    labelAfter = "claims in the last 5 years.",
                    name = EngineersComputer.claimsLastFiveField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "Indicate how many claims your company has made against your policy in the past 5 years.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("0","0"),
                        new RatingPlanDoc.Option("1","1") { isDeclined = true },
                        new RatingPlanDoc.Option("2","2") { isDeclined = true },
                        new RatingPlanDoc.Option("3+","3") { isDeclined = true },
                    }
                },
                new RatingPlanDoc.Field()
                {
                    label = "These claims resulted in total losses of $",
                    name = totalClaimsLosses,
                    type = RatingPlanDoc.Field.money,
                    placeholder = "Enter amount",
                    tooltip = "Please indicate the approximate total cost of any claims made against your current policy in the past 5 years.",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(EngineersComputer.claimsLastFiveField, "0") {Operator = RatingPlanDoc.Condition.DifferentOperator }
                    }
                },
                
                #endregion                                
                #region Coverage
                new RatingPlanDoc.Field()
                {
                    label = "I'd like Protector liability coverage in the amount of $",
                    name = RatingPlanComputer.coverageField,
                    type = RatingPlanDoc.Field.select,
                    placeholder = "Select dollar amount",
                    tooltip = "Please select the dollar amount of liability coverage you’re requesting.",
                },

                #endregion
                #region Deductible

                new RatingPlanDoc.Field()
                {
                    label = "With a deductible of $",
                    name = RatingPlanComputer.deductibleField,
                    type = RatingPlanDoc.Field.select,
                    placeholder = "Select dollar amount",
                    tooltip = "Please select the dollar amount of the deductible you would like for the policy you’re requesting."
                },

                #endregion
                #region Starting On

                new RatingPlanDoc.Field()
                {
                    label = "I’d like my coverage to begin on",
                    name = RatingPlanComputer.startingDateField,
                    type = RatingPlanDoc.Field.datePickerLimited,
                    tooltip = "Please select the date you'd like your new policy to go in effect. You may choose up to 30 days, including today. Use the MM-DD-YYYY format.",
                    invalidMessage = "You must choose up to 30 days from today."
                },

                #endregion
            };


            #endregion
            #region Common RatingPlanForm

            var ratingPlanForm = new RatingPlanDoc.RatingPlanForm()
            {
                formId = formId,
                version = version,
                rounding = true,
                maxRevenue = 2000000,
                basePremium = new RatingPlanDoc.BasePremium(),
                businessManagement = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(5, -0.1m),
                    new RatingPlanDoc.YearFactor(10, -0.15m),
                },
                scheduleModifications = new RatingPlanDoc.ScheduleModifications()
                {
                    min = -0.25m,
                    max = 0.25m,
                    riskCaracteristics = new List<RatingPlanDoc.RiskCaracteristic>()
                    {
                        new RatingPlanDoc.RiskCaracteristic(EngineersComputer.professionalMembershipsField , -0.25m, 0.25m),
                        new RatingPlanDoc.RiskCaracteristic(EngineersComputer.businessManagementField, -0.25m, 0.25m),
                    }
                },
                fields = commonFields
            };

            #endregion
            
            #region Accountants
            if (productId == Product.AccountantsId)
            {
                #region Factors
                ratingPlanForm.id = $"{stateName}-accountants-{version}";
                ratingPlanForm.minPremium = 500;
                ratingPlanForm.basePremium.incremetalRate = 1000;
                ratingPlanForm.priorActsCoverages = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(0, 1),
                    new RatingPlanDoc.YearFactor(1, 1.48m),
                    new RatingPlanDoc.YearFactor(2, 1.66m),
                    new RatingPlanDoc.YearFactor(3, 1.78m),
                    new RatingPlanDoc.YearFactor(4, 1.86m),
                    new RatingPlanDoc.YearFactor(5, 1.92m),
                    new RatingPlanDoc.YearFactor(6, 1.97m),
                    new RatingPlanDoc.YearFactor(7, 2),
                };
                ratingPlanForm.revenueStaffCredit = new RatingPlanDoc.RevenueStaffCredit(200)
                {
                    credits = new List<RatingPlanDoc.CreditPerStaff>()
                    {
                        new RatingPlanDoc.CreditPerStaff(50000, 0.85m),
                        new RatingPlanDoc.CreditPerStaff(100000, 0.95m),
                        new RatingPlanDoc.CreditPerStaff(125000, 0.85m),
                        new RatingPlanDoc.CreditPerStaff(150000, 0.90m),
                    }
                };
                ratingPlanForm.defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
                {
                    minPremium = 650,
                    factors = new List<RatingPlanDoc.DefenseOutsideLimitFactor>()
                    {
                        new RatingPlanDoc.DefenseOutsideLimitFactor("100000", 1.14m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("250000", 1.12m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("500000", 1.1m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("1000000", 1.09m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("2000000", 1.08m),
                    }
                };
                ratingPlanForm.experienceModifications = new List<RatingPlanDoc.ExperienceModification>()
                {
                    new RatingPlanDoc.ExperienceModification(100000)
                    {
                        items = new List<RatingPlanDoc.ExperienceModificationFactor>()
                        {
                            new RatingPlanDoc.ExperienceModificationFactor(4, 0, 0),
                            new RatingPlanDoc.ExperienceModificationFactor(5, 0, -0.05m),
                        }
                    },
                    new RatingPlanDoc.ExperienceModification(500000)
                    {
                        items = new List<RatingPlanDoc.ExperienceModificationFactor>()
                        {
                            new RatingPlanDoc.ExperienceModificationFactor(4, 0, 0),
                            new RatingPlanDoc.ExperienceModificationFactor(5, 0, -0.075m),
                        }
                    },
                    new RatingPlanDoc.ExperienceModification(1000000)
                    {
                        items = new List<RatingPlanDoc.ExperienceModificationFactor>()
                        {
                            new RatingPlanDoc.ExperienceModificationFactor(4, 0, 0),
                            new RatingPlanDoc.ExperienceModificationFactor(5, 0, -0.1m),
                        }
                    },
                    new RatingPlanDoc.ExperienceModification(2000000)
                    {
                        items = new List<RatingPlanDoc.ExperienceModificationFactor>()
                        {
                            new RatingPlanDoc.ExperienceModificationFactor(4, 0, -0.03m),
                            new RatingPlanDoc.ExperienceModificationFactor(5, 0, -0.125m),
                        }
                    },
                    new RatingPlanDoc.ExperienceModification(99999999999)
                    {
                        items = new List<RatingPlanDoc.ExperienceModificationFactor>()
                        {
                            new RatingPlanDoc.ExperienceModificationFactor(4, 0, -0.05m),
                            new RatingPlanDoc.ExperienceModificationFactor(5, 0, -0.15m),
                        }
                    },
                };

                #endregion

                #region Staff Number
                var staffNumberField = new RatingPlanDoc.Field()
                {
                    label = "The total staff at my company, including owners, partners, officers and CPAs is",
                    name = AccountantsComputer.staffNumberField,
                    type = RatingPlanDoc.Field.number,
                    placeholder = "Enter a number",
                    tooltip = "Provide the exact number of company employees. If you’re an independent freelancer, enter 1."
                };
                ratingPlanForm.InsertAfter(staffNumberField, AccountantsComputer.businessManagementField);
                #endregion
                #region Professional Society
                var professionalMembershipsField = ratingPlanForm.GetField(AccountantsComputer.professionalMembershipsField);
                professionalMembershipsField.tooltip = "Tell us if you’re registered with any professional Accountants society.";
                #endregion
                #region Written Contracts (removed)
                //var writtenContractsField = ratingPlanForm.GetField(AccountantsComputer.writtenContractsField);
                //writtenContractsField.labelAfter = "75% of my business is secured through engagement letters in the last 12 months.";
                //writtenContractsField.tooltip = "Please tell us whether your company has secured more or less than 75% of its business through engagement letters.";
                #endregion
                #region From Services
                var fromServicesField = ratingPlanForm.GetField(AccountantsComputer.revenueFromServicesField);
                fromServicesField.labelAfter = "provide any of the following services: attorney, audits of SEC registrants, or audits of private firms with revenues or assets greater than $25M.";
                fromServicesField.tooltip = "Tell us if your company has provided services to attorneys, or conducted audits of SEC registrants or private firms with revenues or assets greater than $25M. This may affect the coverage we can offer you.";
                #endregion
                #region From Segments (practice)
                var fromSegmentsField = ratingPlanForm.GetField(AccountantsComputer.revenueFromSegmentsField);
                fromSegmentsField.labelAfter = "75% of my business revenue was derived from: Bookkeeping, Compilation, Payroll, Review, or Taxes in the past 12 months.";
                fromSegmentsField.tooltip = "Tell us if more than 75% of your company’s revenue was derived from any of the listed industries or specialties in the past 12 months. This may affect the coverage we can offer you.";
                fromSegmentsField.options = new List<RatingPlanDoc.Option>()
                {
                    new RatingPlanDoc.Option("At Least","atleast", -0.1m),
                    new RatingPlanDoc.Option("Less Than","lessthan") { isDeclined = true }
                };
                #endregion
                #region From Industries (clients)
                var revenueFromIndustriesField = new RatingPlanDoc.Field()
                {
                    label = "Please indicate if any of the following industries represented at least 10% of your revenue in the last 12 months:",
                    name = AccountantsComputer.revenueFromIndustriesField,
                    type = RatingPlanDoc.Field.checkbox,
                    tooltip = "Group your clients by industry, then tell us which ones generated more than 10 % of your company’s revenue in the past 12 months.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("Construction","construction", 0.15m),
                        new RatingPlanDoc.Option("Entertainment","entertainment", 0.35m),
                        new RatingPlanDoc.Option("Factoring Companies","factoringcompanies", 0.15m),
                        new RatingPlanDoc.Option("Financial Institutions","financialinstitutions", 0.5m),
                        new RatingPlanDoc.Option("Health Care Orgs","healthcareorgs", 0.15m),
                        new RatingPlanDoc.Option("Insurance Companies","insurancecompanies", 0.35m),
                        new RatingPlanDoc.Option("Pension Funds","pensionfunds", 0.35m),
                        new RatingPlanDoc.Option("Professional Athletes","professionalathletes", 0.35m),
                        new RatingPlanDoc.Option("Real Estate Developer","realestatedeveloper", 0.25m),
                        new RatingPlanDoc.Option("Unions","unions", 0.5m),
                    }
                };
                ratingPlanForm.InsertAfter(revenueFromIndustriesField, AccountantsComputer.revenueFromSegmentsField);

                #endregion
                #region Existing Policy
                ratingPlanForm.GetField(RatingPlanComputer.existingPolicyField).labelAfter = "have Accountants Professional Liability coverage currently in force.";
                #endregion
                #region Coverage
                ratingPlanForm.GetField(RatingPlanComputer.coverageField).options = new List<RatingPlanDoc.Option>()
                {
                    new RatingPlanDoc.Option("100,000","100000", 1),
                    new RatingPlanDoc.Option("250,000","250000", 1.35m),
                    new RatingPlanDoc.Option("500,000","500000", 1.7m),
                    new RatingPlanDoc.Option("1,000,000","1000000", 2.15m),
                    new RatingPlanDoc.Option("2,000,000","2000000", 2.75m)
                };
                #endregion
                #region Deductible
                ratingPlanForm.GetField(RatingPlanComputer.deductibleField).options = new List<RatingPlanDoc.Option>()
                {
                    new RatingPlanDoc.Option("500","500", 0.15m),
                    new RatingPlanDoc.Option("1,000","1000", 0.03m),
                    new RatingPlanDoc.Option("2,500","2500", 0),
                    new RatingPlanDoc.Option("5,000","5000", -0.05m),
                    new RatingPlanDoc.Option("7,500","7500", -0.07m),
                    new RatingPlanDoc.Option("10,000","10000", -0.09m)
                };
                #endregion
                #region Defense Outside Limit
                var defenseOutsideLimitField = new RatingPlanDoc.Field()
                {
                    active = false,
                    label = "I",
                    labelAfter = "want an additional defense limit added to my policy for claim expenses.",
                    name = AccountantsComputer.defenseOutsideLimitField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "In order to cover potential defense expenses, you may choose an additional limit equal to your coverage limit.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("do", "1"),
                        new RatingPlanDoc.Option("don't", "0"),
                    }
                };
                ratingPlanForm.InsertAfter(defenseOutsideLimitField, RatingPlanComputer.startingDateField);

                #endregion
            }

            #endregion
            #region Engineers
            else if (productId == Product.ArchitectectsEngineersId)
            {
                #region Factors
                ratingPlanForm.id = $"{stateName}-engineers-{version}";
                ratingPlanForm.minPremium = 1400;
                ratingPlanForm.basePremium.incremetalRate = 100;
                ratingPlanForm.priorActsCoverages = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(0, 0.8m),
                    new RatingPlanDoc.YearFactor(1, 0.9m),
                    new RatingPlanDoc.YearFactor(2, 0.95m),
                    new RatingPlanDoc.YearFactor(3, 0.975m),
                    new RatingPlanDoc.YearFactor(4, 1),
                };
                ratingPlanForm.firstDollarDefense = new RatingPlanDoc.FirstDollarDefense()
                {
                    minEndorsementPremium = 300,
                    factors = new List<RatingPlanDoc.FirstDollarDefenseFactor>()
                    {
                        new RatingPlanDoc.FirstDollarDefenseFactor("0", 0.0m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("1000", 0.03m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("2500", 0.05m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("5000", 0.07m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("7500", 0.08m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("10000", 0.09m),
                    }
                };
                #endregion

                #region Professional Society
                var professionalMembershipsField = ratingPlanForm.GetField(EngineersComputer.professionalMembershipsField);
                professionalMembershipsField.tooltip = "Tell us if you’re registered with any professional Architects or Engineers society.";
                #endregion
                #region Written Contracts (removed)
                //var writtenContractsField = ratingPlanForm.GetField(EngineersComputer.writtenContractsField);
                //writtenContractsField.labelAfter = "75% of my business is secured through written contracts in the last 12 months.";
                //writtenContractsField.tooltip = "Please tell us whether your company has secured more or less than 75% of its business through written contracts.";
                #endregion
                #region From Services
                var fromServicesField = ratingPlanForm.GetField(EngineersComputer.revenueFromServicesField);
                fromServicesField.labelAfter = "offer services in : Aerospace, Geotechnical, Marine, Nuclear or Petrochemical engineering; Product design, Real estate development or superfund / environmental remediation work.";
                fromServicesField.tooltip = "Tell us if your company regularly provides services in any of the industries or specialties listed. This may affect the coverage we can offer you.";
                #endregion
                #region From Segments
                var fromSegmentsField = ratingPlanForm.GetField(EngineersComputer.revenueFromSegmentsField);
                fromSegmentsField.labelAfter = "75% of my business revenue was derived from: Acoustical, Architecture, Civil, Electrical, Communications, Forensic, Mechanical, Process or Traffic engineering; Landscape or Golf Course architecture, Interior design or unlicensed design such as kitchen & bath, exhibit & trade show, lighting, etc. in the last 12 months.";
                fromSegmentsField.tooltip = "Tell us if more than 75% of your company’s revenue was derived from any of the listed industries or specialties in the past 12 months.This may affect the coverage we can offer you.";
                #endregion
                #region From HVAC
                var revenueFromHVACField = new RatingPlanDoc.Field()
                {
                    labelAfter = "provide Structural or HVAC services that account for over 50% of my revenue.",
                    name = EngineersComputer.revenueFromHVACField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "Please tell us if more than 50% of your business revenue was derived from Structural or HVAC services.This may affect the coverage we can offer you.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do", "1", 1.75m),
                        new RatingPlanDoc.Option("I don't", "0"),
                    }
                };
                ratingPlanForm.InsertAfter(revenueFromHVACField, EngineersComputer.revenueFromSegmentsField);

                #endregion
                #region Existing Policy
                ratingPlanForm.GetField(RatingPlanComputer.existingPolicyField).labelAfter = "have Architects and Engineers Professional Liability coverage currently in force.";
                #endregion
                #region Coverage
                ratingPlanForm.GetField(RatingPlanComputer.coverageField).options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("100,000","100000", 1),
                        new RatingPlanDoc.Option("250,000","250000", 1.5m),
                        new RatingPlanDoc.Option("500,000","500000", 2),
                        new RatingPlanDoc.Option("750,000","750000", 2.2m),
                        new RatingPlanDoc.Option("1,000,000","1000000", 2.35m),
                        new RatingPlanDoc.Option("2,000,000","2000000", 2.8m),
                    };
                #endregion
                #region Deductible
                ratingPlanForm.GetField(RatingPlanComputer.deductibleField).options = new List<RatingPlanDoc.Option>()
                {
                    //new RatingPlanDoc.Option("0","0", 1.21m) {
                    //    conditionals = new List<RatingPlanDoc.Condition>() {
                    //        new RatingPlanDoc.Condition(RatingPlanComputer.coverageField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                    //    }
                    //},
                    new RatingPlanDoc.Option("1,000","1000", 0.14m),
                    new RatingPlanDoc.Option("2,500","2500", 0.07m),
                    new RatingPlanDoc.Option("5,000","5000", 0.03m),
                    new RatingPlanDoc.Option("7,500","7500", -0.01m),
                    new RatingPlanDoc.Option("10,000","10000", -0.04m),
                };
                #endregion
                #region First Dollar Defense

                var firstDollarDefenseField = new RatingPlanDoc.Field()
                {
                    label = "I",
                    labelAfter = "want additional First Dollar Defense added to my policy to cover potential claim expenses.",
                    name = EngineersComputer.firstDollarDefenseField,
                    type = RatingPlanDoc.Field.select,
                    tooltip = "In order to cover potential defense expenses, you may choose an additional First Dollar Defense limit equal to your coverage limit.",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("do","1"),
                        new RatingPlanDoc.Option("don't","0"),
                    }
                };
                ratingPlanForm.InsertAfter(firstDollarDefenseField, RatingPlanComputer.startingDateField);

                #endregion                
            }
            #endregion

            return ratingPlanForm;
        }

        public void Engineers()
        {
            RatingPlanDoc.RatingPlanForm ratingPlanForm;
            RatingPlanDoc.Field field;
            RatingPlanFormVersion ratingPlanFormVersion;
            string[] stateAbbreviations;
            HashSet<string> addedStates = new HashSet<string>();
            var ratingPlanFormVersionSet = DbContext.Set<RatingPlanFormVersion>()
                .IncludeMultiple(r => r.RatingPlanForm, r => r.RatingPlanForm.State)
                .Where(r => r.RatingPlanForm.ProductId == Product.ArchitectectsEngineersId);

            #region Alabama
            string alabamaAbbr = "AL";
            addedStates.Add(alabamaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == alabamaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(100000, 1650, 0),
                    new RatingPlanDoc.BasePremiumRange(250000, 1650, 1.7m),
                    new RatingPlanDoc.BasePremiumRange(500000, 4206, 1.1m),
                    new RatingPlanDoc.BasePremiumRange(750000, 6966, 0.5m),
                    new RatingPlanDoc.BasePremiumRange(1000000, 8226, 0.48m),
                    new RatingPlanDoc.BasePremiumRange(1500000, 9426, 0.38m),
                    new RatingPlanDoc.BasePremiumRange(2000000, 11346, 0.38m),
                    new RatingPlanDoc.BasePremiumRange(999999990, 13266, 0.38m),
                };
                #region Deductible
                field = ratingPlanForm.GetField(RatingPlanComputer.deductibleField);
                field.GetOption("1000").factor = 1.14m;
                field.GetOption("2500").factor = 1.07m;
                field.GetOption("5000").factor = 1.03m;
                field.GetOption("7500").factor = 0.99m;
                field.GetOption("10000").factor = 0.96m;
                #endregion
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }

            #endregion

            #region Arkansas, Arizona, Colorado, Connecticut, District of Columbia, Delaware, Georgia, Idaho, Ilinois, Indiana, Iowa, Kansas, Kentucky, Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, North Carolina, North Dakota, Ohio, Oklahoma, Oregon, Rhode Island, South Carolina, South Dakota, Tennesse, Texas, Utah,  Vermont, Virginia, West Virginia, Wisconsin, Wyoming

            stateAbbreviations = new string[] { "AR", "AZ", "CO", "CT", "DC", "DE", "GA", "ID", "IL", "IN", "IA", "KS", "KY", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NC", "ND", "OH", "OK", "OR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WV", "WI", "WY" };
            addedStates.AddRange(stateAbbreviations, true);

            foreach (var stateAbbrev in stateAbbreviations)
            {
                ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == stateAbbrev);
                if (ratingPlanFormVersion != null)
                {
                    ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                    ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                    {
                        new RatingPlanDoc.BasePremiumRange(100000, 1375, 0),
                        new RatingPlanDoc.BasePremiumRange(250000, 1375, 1.42m),
                        new RatingPlanDoc.BasePremiumRange(500000, 3505, 0.92m),
                        new RatingPlanDoc.BasePremiumRange(750000, 5805, 0.42m),
                        new RatingPlanDoc.BasePremiumRange(1000000, 6855, 0.4m),
                        new RatingPlanDoc.BasePremiumRange(1500000, 7855, 0.32m),
                        new RatingPlanDoc.BasePremiumRange(2000000, 9455, 0.32m),
                        new RatingPlanDoc.BasePremiumRange(999999990, 11055, 0.32m),
                    };
                    ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
                }
            }

            #endregion

            #region California
            string califorinaAbbr = "CA";
            addedStates.Add(califorinaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == califorinaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                    {
                        new RatingPlanDoc.BasePremiumRange(100000, 1400, 0),
                        new RatingPlanDoc.BasePremiumRange(250000, 1400, 1.71m),
                        new RatingPlanDoc.BasePremiumRange(500000, 3965, 1.11m),
                        new RatingPlanDoc.BasePremiumRange(750000, 6740, 0.506m),
                        new RatingPlanDoc.BasePremiumRange(1000000, 8005, 0.484m),
                        new RatingPlanDoc.BasePremiumRange(1500000, 9215, 0.385m),
                        new RatingPlanDoc.BasePremiumRange(2000000, 11140, 0.385m),
                        new RatingPlanDoc.BasePremiumRange(999999990, 13065, 0.385m),
                    };
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

            #region Florida
            string floridaAbbr = "FL";
            addedStates.Add(floridaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == floridaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.minPremium = 750;
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(100000, 1441, 0),
                    new RatingPlanDoc.BasePremiumRange(250000, 1441, 1.49m),
                    new RatingPlanDoc.BasePremiumRange(500000, 3679, 0.96m),
                    new RatingPlanDoc.BasePremiumRange(750000, 6079, 0.44m),
                    new RatingPlanDoc.BasePremiumRange(1000000, 7179, 0.42m),
                    new RatingPlanDoc.BasePremiumRange(1500000, 8229, 0.34m),
                    new RatingPlanDoc.BasePremiumRange(2000000, 9929, 0.34m),
                    new RatingPlanDoc.BasePremiumRange(999999990, 11629, 0.34m),
                };
                #region Coverage
                field = ratingPlanForm.GetField(RatingPlanComputer.coverageField);
                field.GetOption("100000").factor = 1;
                field.GetOption("250000").factor = 1.5m;
                field.GetOption("500000").factor = 2;
                field.GetOption("750000").factor = 2.3m;
                field.GetOption("1000000").factor = 2.5m;
                field.GetOption("2000000").factor = 2.91m;
                #endregion
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

            #region New York
            string nyAbbr = "NY";
            addedStates.Add(nyAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == nyAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(100000, 1316, 0),
                    new RatingPlanDoc.BasePremiumRange(250000, 1316, 1.357m),
                    new RatingPlanDoc.BasePremiumRange(500000, 3351, 0.875m),
                    new RatingPlanDoc.BasePremiumRange(750000, 5538, 0.4m),
                    new RatingPlanDoc.BasePremiumRange(1000000, 6538, 0.385m),
                    new RatingPlanDoc.BasePremiumRange(1500000, 7500, 0.307m),
                    new RatingPlanDoc.BasePremiumRange(2000000, 9035, 0.307m),
                    new RatingPlanDoc.BasePremiumRange(999999990, 10570, 0.307m),
                };
                #region Deductible
                field = ratingPlanForm.GetField(RatingPlanComputer.deductibleField);
                field.GetOption("1000").factor = 1.14m;
                field.GetOption("2500").factor = 1.07m;
                field.GetOption("5000").factor = 1.03m;
                field.GetOption("7500").factor = 0.101m;
                field.GetOption("10000").factor = 0.96m;
                #endregion
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

            #region Pennsylvania
            string pennsylvaniaAbbr = "PA";
            addedStates.Add(pennsylvaniaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == pennsylvaniaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(100000, 1650, 0),
                    new RatingPlanDoc.BasePremiumRange(250000, 1650, 1.7m),
                    new RatingPlanDoc.BasePremiumRange(500000, 4206, 1.1m),
                    new RatingPlanDoc.BasePremiumRange(750000, 6966, 0.5m),
                    new RatingPlanDoc.BasePremiumRange(1000000, 8226, 0.48m),
                    new RatingPlanDoc.BasePremiumRange(1500000, 9426, 0.38m),
                    new RatingPlanDoc.BasePremiumRange(2000000, 11346, 0.38m),
                    new RatingPlanDoc.BasePremiumRange(999999990, 13266, 0.38m),
                };
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

            #region Washington
            string washingtonAbbr = "WA";
            addedStates.Add(washingtonAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == washingtonAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.ArchitectectsEngineersId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(100000, 1365, 0),
                    new RatingPlanDoc.BasePremiumRange(250000, 1365, 1.26m),
                    new RatingPlanDoc.BasePremiumRange(500000, 3255, 0.88m),
                    new RatingPlanDoc.BasePremiumRange(750000, 5455, 0.82m),
                    new RatingPlanDoc.BasePremiumRange(1000000, 7505, 0.76m),
                    new RatingPlanDoc.BasePremiumRange(1500000, 9405, 0.71m),
                    new RatingPlanDoc.BasePremiumRange(2000000, 12955, 0.67m),
                    new RatingPlanDoc.BasePremiumRange(2500000, 16305, 0.65m),
                    new RatingPlanDoc.BasePremiumRange(999999990, 19555, 0.6m),
                };
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

        }
        public void Accountants()
        {
            RatingPlanDoc.RatingPlanForm ratingPlanForm;
            RatingPlanDoc.Field field;
            RatingPlanFormVersion ratingPlanFormVersion;
            string[] stateAbbreviations;
            HashSet<string> addedStates = new HashSet<string>();
            var ratingPlanFormVersionSet = DbContext.Set<RatingPlanFormVersion>()
                .IncludeMultiple(r => r.RatingPlanForm, r => r.RatingPlanForm.State)
                .Where(r => r.RatingPlanForm.ProductId == Product.AccountantsId);

            #region Arizona, Massachussets, Maryland, New Jersey

            stateAbbreviations = new string[] { "AZ", "MA", "MD", "NJ" };
            addedStates.AddRange(stateAbbreviations, true);

            foreach (var stateAbbrev in stateAbbreviations)
            {
                ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == stateAbbrev);
                if (ratingPlanFormVersion != null)
                {
                    ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                    ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                    {
                        new RatingPlanDoc.BasePremiumRange(75000, 286, 0),
                        new RatingPlanDoc.BasePremiumRange(500000, 286, 3.82m),
                        new RatingPlanDoc.BasePremiumRange(750000, 1910, 2.87m),
                        new RatingPlanDoc.BasePremiumRange(9999999, 2628, 2.15m),
                    };
                    ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
                }
            }

            #endregion

            #region Alabama, Arkansas, Colorado, Connecticut, Delaware, Georgia, Idaho, Ilinois, Indiana, Iowa, Kansas, Kentucky, Michigan, Minessota, Missouri, Mississippi, Montana, Nebraska, Nevada, New Hampshire, New Mexico, North Carolina, North Dakota, Ohio, Oregon, Pennsylvania, Rhode Island, South Carolina, Soth Dakota, Tennesse, Utah, Vermont, West Virginia, Wisconsin, Wyoming

            stateAbbreviations = new string[] { "AL", "AR", "CO", "CT", "DE", "GA", "ID", "IL", "IN", "IA", "KS", "KY", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NM", "NC", "ND", "OH", "OR", "PA", "RI", "SC", "SD", "TN", "UT", "VT", "WV", "WI", "WY" };
            addedStates.AddRange(stateAbbreviations, true);

            foreach (var stateAbbrev in stateAbbreviations)
            {
                ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == stateAbbrev);
                if (ratingPlanFormVersion != null)
                {
                    ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                    ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                    {
                        new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                        new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                        new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                        new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
                    };
                    ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
                }
            }

            #endregion

            #region  District of Columbia, Maine, Virginia

            stateAbbreviations = new string[] { "DC", "ME", "VA" };
            addedStates.AddRange(stateAbbreviations, true);

            foreach (var stateAbbrev in stateAbbreviations)
            {
                ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == stateAbbrev);
                if (ratingPlanFormVersion != null)
                {
                    ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                    ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                    {
                        new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                        new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                        new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                        new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
                    };
                    #region Coverage
                    field = ratingPlanForm.GetField(RatingPlanComputer.coverageField);
                    field.GetOption("100000").factor = 1;
                    field.GetOption("250000").factor = 1.35m;
                    field.GetOption("500000").factor = 1.7m;
                    field.GetOption("1000000").factor = 2.15m;
                    field.GetOption("2000000").factor = 2.5m;
                    #endregion
                    ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
                }
            }

            #endregion

            #region  California

            string californiaAbbr = "CA";
            addedStates.Add(californiaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == californiaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.minPremium = 650;
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 338, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 338, 4.51m),
                    new RatingPlanDoc.BasePremiumRange(750000, 2255, 3.38m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 3100, 2.54m),
                };
                ratingPlanForm.revenueStaffCredit.minPerMember = 260;
                ratingPlanForm.defenseOutsideLimit.minPremium = 700;
                #region Coverage
                field = ratingPlanForm.GetField(RatingPlanComputer.coverageField);
                field.GetOption("100000").factor = 1;
                field.GetOption("250000").factor = 1.35m;
                field.GetOption("500000").factor = 1.7m;
                field.GetOption("1000000").factor = 2.15m;
                field.GetOption("2000000").factor = 2.5m;
                #endregion
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }


            #endregion

            #region  Florida

            string floridaAbbr = "FL";
            addedStates.Add(floridaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == floridaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.minPremium = 650;
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.60m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
                };
                ratingPlanForm.defenseOutsideLimit.minPremium = 700;


                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }

            #endregion

            #region  Oklahoma 

            string oklahomaAbbr = "OK";
            addedStates.Add(oklahomaAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == oklahomaAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
                };
                #region Coverage
                field = ratingPlanForm.GetField(RatingPlanComputer.coverageField);
                field.GetOption("100000").factor = 1;
                field.GetOption("250000").factor = 1.35m;
                field.GetOption("500000").factor = 1.7m;
                field.GetOption("1000000").factor = 2.15m;
                field.GetOption("2000000").factor = 2.5m;
                #endregion
                #region Deductible
                field = ratingPlanForm.GetField(RatingPlanComputer.deductibleField);
                //field.options.RemoveAll(o=>o.value == "500");
                #endregion

                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }

            #endregion

            #region New York
            string nyAbbr = "NY";
            addedStates.Add(nyAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == nyAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 204, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 204, 2.73m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1363, 2.04m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 1874, 1.53m),
                };
                #region Coverage
                field = ratingPlanForm.GetField(RatingPlanComputer.coverageField);
                field.GetOption("100000").factor = 1;
                field.GetOption("250000").factor = 1.35m;
                field.GetOption("500000").factor = 1.7m;
                field.GetOption("1000000").factor = 2.15m;
                field.GetOption("2000000").factor = 2.5m;
                #endregion

                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }
            #endregion

            #region  Texas 

            string taxasAbbr = "TX";
            addedStates.Add(taxasAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == taxasAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 251, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 251, 3.35m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1676, 2.52m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 2306, 1.89m),
                };
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }

            #endregion

            #region  Washington 

            string washingtonAbbr = "WA";
            addedStates.Add(washingtonAbbr, true);
            ratingPlanFormVersion = ratingPlanFormVersionSet.FirstOrDefault(r => r.RatingPlanForm.State.Abbreviation == washingtonAbbr);
            if (ratingPlanFormVersion != null)
            {
                ratingPlanForm = CreateCommonRatingPlanForm(ratingPlanFormVersion.Id, 1, ratingPlanFormVersion.RatingPlanForm.State.Name.Simplify(), Product.AccountantsId);
                ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 273, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 273, 3.64m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1820, 2.73m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 2503, 2.05m),
                };
                ratingPlanFormDocDbRepository.Upsert(ratingPlanForm);
            }

            #endregion

            //int accountantsCount = ratingPlanFormDocDbRepository.Count(e => e.id.Contains(Product.AccountantsAlias));
        }
    }
}
