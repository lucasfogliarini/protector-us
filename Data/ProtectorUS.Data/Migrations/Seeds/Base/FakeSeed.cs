﻿using ProtectorUS.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProtectorUS.Data.Migrations.Seeds
{
    public abstract class FakeSeed : Seed
    {
        public FakeSeed(DbContext dbContext) : base(dbContext)
        {
        }
        
        private class UserSeed
        {
            public UserSeed(long profileId, UserStatus status, string firstName, string lastName, long? brokerageId = null)
            {
                ProfileId = profileId;
                Status = status;
                BrokerageId = brokerageId.GetValueOrDefault();
                FirstName = firstName;
                LastName = lastName;
            }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public UserStatus Status { get; set; }
            public long ProfileId { get; set; }
            public long BrokerageId { get; set; }
        }

        public void SeedUsers()
        {
            var profileDbSet = DbContext.Set<Profile>();
            string brokerPassword = "brokeruser1";
            string managerUserPassword = "manageruser1";
            string insuredPassword = "insureduser1";
            List<ManagerUser> managerUsers = new List<ManagerUser>();
            List<Broker> brokerUsers = new List<Broker>();
            List<Insured> insuredUsers = new List<Insured>();
            var brokerages = DbContext.Set<Brokerage>().ToList();
            var random = new Random();


            #region Broker Masters
            foreach (Brokerage b in brokerages)
            {
                brokerUsers.Add(new Broker($"protector-us+{b.Id}@trin.ca", brokerPassword)
                {
                    Status = UserStatus.Active,
                    BrokerageId = b.Id,
                    FirstName = b.Name,
                    LastName = "Master",
                    Profiles = new List<Profile>()
                    {
                        profileDbSet.Find(Profile.Ids.MasterBroker)
                    }
                });
            }
            #endregion

            #region users
            List<UserSeed> users = new List<UserSeed>();
            users.Add(new UserSeed(Profile.Ids.AdminManagerUser, UserStatus.Active, "Ethan", "Wilhelm"));
            users.Add(new UserSeed(Profile.Ids.DirectorManagerUser, UserStatus.Active, "Zane", "Linversar"));
            users.Add(new UserSeed(Profile.Ids.ManagerManagerUser, UserStatus.Active, "Barbara", "Belford"));
            users.Add(new UserSeed(Profile.Ids.UnderwriterManagerUser, UserStatus.Active, "John", "Brandiling"));
            users.Add(new UserSeed(Profile.Ids.ClaimsManagerUser, UserStatus.Active, "Roney", "Heane"));
            users.Add(new UserSeed(Profile.Ids.AssistantManagerUser, UserStatus.Active, "Justin", "Wilhelm"));


            for (int i = 1; i <= 2; i++)
            {
                long brokerageId = i;
                #region brokers
                users.Add(new UserSeed(Profile.Ids.MasterBroker, UserStatus.Active, "Troy", "Chavez", brokerageId));
                users.Add(new UserSeed(Profile.Ids.MasterBroker, UserStatus.Onboarding, "Barbara", "Anderson", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AdminBroker, UserStatus.Active, "Norma", "Busey", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AdminBroker, UserStatus.Onboarding, "Caleb", "Jones", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AdminBroker, UserStatus.Onboarding, "Kayla", "Heane", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AgentBroker, UserStatus.Active, "Robert", "Aikenhead", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AgentBroker, UserStatus.Onboarding, "John", "Terry", brokerageId));
                users.Add(new UserSeed(Profile.Ids.AgentBroker, UserStatus.Onboarding, "Gary", "Derek", brokerageId));
                #endregion

                #region insureds
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "Shari", "Little", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "Alicia", "Wright", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "Joyce", "Poynter", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "John", "Shuler", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "Andrew", "Henry", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Active, "David", "Williams", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Terry", "Wood", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Harvey", "Dagostino", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Derek", "Callahan", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Stephen", "Stewart", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Gary", "Miller", brokerageId));
                users.Add(new UserSeed(Profile.Ids.Insured, UserStatus.Onboarding, "Jay", "Cooper", brokerageId));
                #endregion
            }

            #endregion

            string emailName;
            Brokerage brokerage;
            foreach (UserSeed user in users)
            {
                var profile = profileDbSet.Find(user.ProfileId);
                var profiles = new List<Profile>() { profile };
                var profileName = profile.Name.Replace("ARGO", "").Simplify();
                emailName = $"{user.FirstName.Simplify()}_{profileName}";
                if (Profile.Ids.ManagerUserProfiles.Contains(user.ProfileId))
                {
                    managerUsers.Add(new ManagerUser($"{emailName}@protector-argo.com", managerUserPassword)
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Status = user.Status,
                        CompanyId = 1,
                        Profiles = profiles
                    });
                }
                else
                {
                    brokerage = brokerages.FirstOrDefault(b => b.Id == user.BrokerageId);
                    string email = $"{emailName}@{brokerage.Alias}.com";
                    if (Profile.Ids.BrokerProfiles.Contains(user.ProfileId))
                    {
                        brokerUsers.Add(new Broker(email, brokerPassword)
                        {
                            Status = user.Status,
                            BrokerageId = user.BrokerageId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Profiles = profiles
                        });
                    }
                    else
                    {
                        insuredUsers.Add(new Insured(email, insuredPassword)
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            BirthDate = DateTime.Now.AddYears(-random.Next(18, 50)),
                            Status = user.Status,
                            Profiles = profiles,
                            Phones = new List<Phone>()
                            {
                                new Phone
                                {
                                    Type = (PhoneType)random.Next(1,3),
                                    Number = "555999" + random.Next(1000, 9999)
                                }
                            }
                        });
                    }
                }
            }

            DbContext.Set<ManagerUser>().AddIfNotExists(
               managerUsers.ToArray()
            );
            DbContext.Set<Broker>().AddIfNotExists(
                brokerUsers.ToArray()
            );
            DbContext.Set<Insured>().AddIfNotExists(
                insuredUsers.ToArray()
            );
        }
        public void SeedPolicies()
        {
            var productConditions = DbContext.Set<ProductCondition>().Include("Product").Where(pc => pc.Active);
            var AEcondition = productConditions.FirstOrDefault(pc => pc.Product.Id == Product.ArchitectectsEngineersId);
            var APLcondition = productConditions.FirstOrDefault(pc => pc.Product.Id == Product.AccountantsId);

            var random = new Random();
            var insureds = DbContext.Set<Insured>().Where(i => i.Status == UserStatus.Active);
            var brokers = DbContext.Set<Broker>().Where(i => i.Status == UserStatus.Active);
            long[] addressIds = DbContext.Set<Address>().Select(r => r.Id).ToArray();
            long[] regionIds = DbContext.Set<Region>().Select(r => r.Id).ToArray();
            var orders = new List<Order>();
            bool pair;
            int sequentialNumber = PolicyNumber.sequentialMin;

            var coverages = DbContext.Set<Coverage>().ToList();
            foreach (var insured in insureds)
            {
                pair = insured.Id % 2 == 0;
                #region quotation
                var quotation = new Quotation()
                {
                    Insured = insured,
                    Business = new Business()
                    {
                        AddressId = addressIds.Shuffle().First(),
                        Type = (BusinessType)random.Next(1, 2),
                        Name = $"{insured.FirstName} {insured.LastName} Business."
                    },
                    Revenue = random.Next(100000, 2000000),
                    RatingPlanFormVersionId = random.Next(1, 80),
                    PolicyStartDate = DateTime.Now.AddDays(1),
                    BrokerId = brokers.FirstOrDefault().Id,
                    RegionId = regionIds.Shuffle().First(),
                    BrokerageProductId = 1,
                    ExpirationDate = DateTime.Now.AddDays(15),
                    ProponentEmail = insured.Email,
                    ProponentFirstName = insured.FirstName,
                    ProponentLastName = insured.LastName,
                    Hash = Guid.NewGuid(),
                    TaxAmount = random.Next(0, 50),
                    FeeAmount = random.Next(0, 50),
                    PolicyPremium = random.Next(2000, 5000),
                    RiskAnalysisJson = "",
                    IsDeclined = false
                };
                #endregion
                #region order
                var condition = pair ? AEcondition : APLcondition;
                sequentialNumber++;
                PolicyNumber policyNumber = new PolicyNumber(1, condition.Product.Code, sequentialNumber);

                orders.Add(new Order()
                {
                    Quotation = quotation,
                    TransactionCode = "STRIPE_TRANSACTION_CODE",
                    PaymentStatus = PaymentStatus.Approved,
                    TotalInstallments = 1,
                    PaymentMethodId = random.Next(1, 6),
                    OrderDate = DateTime.Now,
                    SurchargeAmount = quotation.SurchargeAmount,
                    BrokerageFeePercentage = 15,
                    ArgoFee = 85,
                    BrokerageFee = 15,
                    TotalCharged = 100 + quotation.SurchargeAmount,
                    Rounding = 0,
                    Discount = 0,
                    Policy = new Policy(policyNumber, 0)
                    {
                        PolicyPath = "policies/policy_sample.pdf",
                        Condition = condition,
                        Insured = quotation.Insured,
                        BrokerId = quotation.BrokerId,
                        Business = quotation.Business,
                        TotalPremium = quotation.PolicyPremium,
                        CoverageEachClaim = 1000000,
                        DeductibleEachClaim = 2500,
                        PeriodStart = quotation.PolicyStartDate,
                        PeriodEnd = quotation.PolicyStartDate.AddYears(1),
                        CertificateFileName = "policy_certificate_example.pdf",
                        Coverages = coverages.Where(c => c.ProductId == condition.ProductId).ToList()
                    }
                });
                #endregion
            }

            DbContext.Set<Order>().AddIfNotExists(
                orders.ToArray()
            );
        }
        public void SeedEndorsements()
        {
            DbContext.Set<Endorsement>().AddIfNotExists(
               new Endorsement(EndorsementType.Cancellation, 1, 100)
               {
                   Id = 1,
                   PolicyId = 1,
               }
            );
        }
        public void SeedCampaigns()
        {
            var brokers = DbContext.Set<Broker>().ToList();

            var broker1 = brokers.ElementAtOrDefault(1) ?? brokers.Last();
            var broker2 = brokers.ElementAtOrDefault(2) ?? brokers.Last();
            var broker3 = brokers.ElementAtOrDefault(3) ?? brokers.Last();
            var broker4 = brokers.ElementAtOrDefault(4) ?? brokers.Last();

            var emailMarketings = new List<EmailMarketing>()
            {
                new EmailMarketing
                {
                    Id = 1,
                    CoverId = EmailMarketingCoverId.Accountants1,
                    Body = "Test body text",
                    ButtonText = "Get a Quote",
                    CoverPath = "",
                    Title= "Test title",
                    BrokerId = broker1.Id,
                    WebpageProductId = 1,
                },
                new EmailMarketing
                {
                    Id = 2,
                    CoverId = EmailMarketingCoverId.Engineers1,
                    Body = "Test body text",
                    ButtonText = "Get a Quote",
                    CoverPath ="",
                    Title= "Test title",
                    BrokerId = broker2.Id,
                    WebpageProductId = 2,
                }
            };
            var brochures = new List<Brochure>()
            {
                new Brochure
                {
                    Id = 3,
                    FilePath = "a",
                    TemplateType = BrochureTemplateType.Accountants1,
                    BrokerId = broker3.Id,
                    WebpageProductId = 1,
                },
                new Brochure
                {
                    Id = 4,
                    FilePath = "b",
                    TemplateType = BrochureTemplateType.Engineers1,
                    BrokerId = broker4.Id,
                    WebpageProductId = 2,
                },
            };

            DbContext.Set<EmailMarketing>().AddIfNotExists(
               emailMarketings.ToArray()
            );
            DbContext.Set<Brochure>().AddIfNotExists(
               brochures.ToArray()
            );
        }
        public void SeedWebPageViews()
        {
            List<WebpageView> views = new List<WebpageView>();
            var random = new Random();
            for (int i = 1; i <= 50; i++)
            {
                var webpagaproductid = random.Next(1, 3);
                var acesstypeid = random.Next(1, 3);
                long? campaignid = null;
                if (acesstypeid.Equals(AccessType.Campaign))
                    campaignid = random.Next(1, 2);

                var factor = i * 10;
                var view = new WebpageView()
                {
                    Id = i,
                    Access = (AccessType)acesstypeid,
                    WebpageProductId = webpagaproductid,
                    BrokerageId = 1,
                    CampaignId = campaignid,
                    CreatedDate = DateTime.Now.AddDays(-random.Next(1, 80))
                };

                views.Add(view);
            }

            DbContext.Set<Model.WebpageView>().AddIfNotExists(
               views.ToArray()
            );
        }
        public void SeedAddresses()
        {
            long[] regionIds = DbContext.Set<Region>().Select(r => r.Id).ToArray();
            DbContext.Set<Address>().AddIfNotExists(
               new Address() { Id = 1, StreetLine1 = "17158 Blackburn Rd", RegionId = regionIds.Shuffle().First(), Latitude = 34.8087872d, Longitude = -87.0538833d },
               new Address() { Id = 2, StreetLine1 = "24828 Stinnett Hollow Rd", RegionId = regionIds.Shuffle().First(), Latitude = 34.908246d, Longitude = -87.1741235d },
               new Address() { Id = 3, StreetLine1 = "22469 Nick Davis Rd", RegionId = regionIds.Shuffle().First(), Latitude = 34.79902d, Longitude = -86.9157444d },
               new Address() { Id = 4, StreetLine1 = "539 W 28th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.751738d, Longitude = -74.0056217d },
               new Address() { Id = 5, StreetLine1 = "49 Ludlow St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7165125d, Longitude = -73.9913285d },
               new Address() { Id = 6, StreetLine1 = "135 E 16th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.734997d, Longitude = -73.9890333d },
               new Address() { Id = 7, StreetLine1 = "11 Stone St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7041706d, Longitude = -74.0144558d },
               new Address() { Id = 8, StreetLine1 = "150 Water St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7064327d, Longitude = -74.0082478d },
               new Address() { Id = 9, StreetLine1 = "5 Albany St", RegionId = regionIds.Shuffle().First(), Latitude = 40.709823d, Longitude = -74.0154817d },
               new Address() { Id = 10, StreetLine1 = "9 Waverly Pl", RegionId = regionIds.Shuffle().First(), Latitude = 40.7299926d, Longitude = -73.9962606d },
               new Address() { Id = 11, StreetLine1 = "28 Little W St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7060596d, Longitude = -74.0193974d },
               new Address() { Id = 12, StreetLine1 = "25 Nassau St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7080458d, Longitude = -74.0120924d },
               new Address() { Id = 13, StreetLine1 = "126 W 30th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7475378d, Longitude = -73.9929922d },
               new Address() { Id = 14, StreetLine1 = "67 Allen St", RegionId = regionIds.Shuffle().First(), Latitude = 42.7917448d, Longitude = -78.8290981d },
               new Address() { Id = 15, StreetLine1 = "310 Rutger St", RegionId = regionIds.Shuffle().First(), Latitude = 43.0960639d, Longitude = -75.2306967d },
               new Address() { Id = 16, StreetLine1 = "123 Elm St", RegionId = regionIds.Shuffle().First(), Latitude = 40.9338077d, Longitude = -73.890879d },
               new Address() { Id = 17, StreetLine1 = "S Mall Arterial", RegionId = regionIds.Shuffle().First(), Latitude = 42.6502948d, Longitude = -73.760872d },
               new Address() { Id = 18, StreetLine1 = "100 Telfer St", RegionId = regionIds.Shuffle().First(), Latitude = 42.7845639d, Longitude = -78.828622d },
               new Address() { Id = 19, StreetLine1 = "207 W 25th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.745888d, Longitude = -73.9974232d },
               new Address() { Id = 20, StreetLine1 = "11033 Big Spring Ln", RegionId = regionIds.Shuffle().First(), Latitude = 34.7662876d, Longitude = -87.1262121d },
               new Address() { Id = 21, StreetLine1 = "7 Albany St", RegionId = regionIds.Shuffle().First(), Latitude = 40.709823d, Longitude = -74.0154817d },
               new Address() { Id = 22, StreetLine1 = "13 Waverly Pl", RegionId = regionIds.Shuffle().First(), Latitude = 40.7299926d, Longitude = -73.9962606d },
               new Address() { Id = 23, StreetLine1 = "30 Little W St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7060596d, Longitude = -74.0193974d },
               new Address() { Id = 24, StreetLine1 = "22 Nassau St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7080458d, Longitude = -74.0120924d },
               new Address() { Id = 25, StreetLine1 = "120 W 30th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7475378d, Longitude = -73.9929922d },
               new Address() { Id = 26, StreetLine1 = "65 Allen St", RegionId = regionIds.Shuffle().First(), Latitude = 42.7917448d, Longitude = -78.8290981d },
               new Address() { Id = 27, StreetLine1 = "308 Rutger St", RegionId = regionIds.Shuffle().First(), Latitude = 43.0960639d, Longitude = -75.2306967d },
               new Address() { Id = 28, StreetLine1 = "22474 Nick Davis Rd", RegionId = regionIds.Shuffle().First(), Latitude = 34.79902d, Longitude = -86.9157444d },
               new Address() { Id = 29, StreetLine1 = "537 W 28th St", RegionId = regionIds.Shuffle().First(), Latitude = 40.751738d, Longitude = -74.0056217d },
               new Address() { Id = 30, StreetLine1 = "44 Ludlow St", RegionId = regionIds.Shuffle().First(), Latitude = 40.7165125d, Longitude = -73.9913285d }
            );
            DbContext.SaveChanges();
        }
        public Address CreateAddress(string streetLine1, string streetLine2, double lat, double lon, string zipCode, string cityName, long stateId)
        {
            var city = DbContext.Set<City>().FirstOrDefault(c => c.Name == cityName && c.StateId == stateId);
            city = city ?? new City() { Name = cityName, StateId = stateId };

            var region = DbContext.Set<Region>().FirstOrDefault(c => c.ZipCode == zipCode);
            region = region ?? new Region() { ZipCode = zipCode, City = city };
            return new Address()
            {
                StreetLine1 = streetLine1,
                StreetLine2 = streetLine2,
                Latitude = lat,
                Longitude = lon,
                Region = region
            };
        }
        public Brokerage CreateBrokerage(long id, string brokerageName, string logo, Address address, params BrokerageProduct[] products)
        {
            string brokerageNameSimplified = brokerageName.Simplify();
            return new Brokerage()
            {
                Id = id,
                ChargeAccountId = "acct_18XXMxGXFtORWAtN",
                ChargeAccountStatus = ChargeAccountStatus.Verified,
                Name = brokerageName,
                Alias = brokerageNameSimplified.Take(20),
                Email = $"contact@{brokerageNameSimplified}.com",
                Logo = logo,
                Website = $"www.{brokerageNameSimplified}.com",
                Status = BrokerageStatus.Active,
                Address = address,
                AgentNumber = "Z1001",
                Phones = new List<Phone>()
                {
                    new Phone()
                    {
                        Number = new Random().Next(55500000, 55599999).ToString(),
                        Type = PhoneType.Commercial
                    }
                },
                BrokerageProducts = products
            };
        }
        public void SeedBrokerages()
        {
            List<Brokerage> brokerages = new List<Brokerage>();
            long count = 0;
            #region Eagle Insurance
            brokerages.Add(CreateBrokerage(++count, "Eagle Insurance", "images/eagle.jpg",
                CreateAddress("1620 Westhaven Ln", null, 32.36092d, -86.297822d, "36106", "Montgomery", 1),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Financial Japan
            brokerages.Add(CreateBrokerage(++count, "Financial Japan", "images/financial_japan.jpg",
                CreateAddress("99 W Washington St", "1st Floor", 33.4482258d, -112.0750744d, "85003", "Phoenix", 3),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Orion Reach
            brokerages.Add(CreateBrokerage(++count, "Orion Reach", "images/orion.jpg",
                CreateAddress("228 Spring St", null, 34.5104687d, -93.0519613d, "71901", "Hot Springs", 4),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Bull Brokers
            brokerages.Add(CreateBrokerage(++count, "Bull Brokers", "images/bull_brokers.jpg",
                CreateAddress("662 27th St", null, 36.9836793d, -76.4194432d, "96612", "Oakland", 5),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Gates Insurance Inc.
            brokerages.Add(CreateBrokerage(++count, "Gates Insurance Inc.", "images/gates.jpg",
                CreateAddress("1200 Cherokee St", null, 39.73558999999999d, -104.991361d, "80204", "Denver", 6),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Mark Smith
            brokerages.Add(CreateBrokerage(++count, "Mark Smith", "images/marksmith.jpg",
                CreateAddress("40 Grand St", null, 41.7596897d, -72.68632699999999d, "06106", "Hartford", 7),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Fog Insurance
            brokerages.Add(CreateBrokerage(++count, "Fog Insurance", "images/fog_insurance.jpg",
                CreateAddress("203 Bates St NW", null, 38.9106032d, -77.01376669999999d, "20001", "Washington, D.C.", 8),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region MS Medina
            brokerages.Add(CreateBrokerage(++count, "MS Medina", "images/msmedina.jpg",
                CreateAddress("204 Chelteham Rd", null, 39.682737d, -75.764293d, "19711", "Newark", 9),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Mountain Insurance
            brokerages.Add(CreateBrokerage(++count, "Mountain Insurance", "images/mountain.jpg",
                CreateAddress("401 SW St Johns St", null, 30.1879615d, -82.6432959d, "32025", "Lake City", 10),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Cavalry Insurance Services
            brokerages.Add(CreateBrokerage(++count, "Cavalry Insurance Services", "images/cavalry.jpg",
                CreateAddress("36 Harris St NE", null, 33.760743d, -84.3856069d, "30303", "Atlanta", 11),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region One World Cover
            brokerages.Add(CreateBrokerage(++count, "One World Cover", "images/oneworld.jpg",
                CreateAddress("504 1st Ave E", "3rd Floor", 42.7252471d, -114.5114177, "83338", "Jerome", 13),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Action Insurance
            brokerages.Add(CreateBrokerage(++count, "Action Insurance", "images/action.jpg",
                CreateAddress("209 W Ash St", null, 39.7791686d, -89.6563092d, "62704", "Springfield", 14),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Eric Cartman Brokers
            brokerages.Add(CreateBrokerage(++count, "Eric Cartman Brokers", "images/ec.jpg",
                CreateAddress("68 W New York St", null, 39.7715516d, -86.159109d, "46204", "Indianapolis", 15),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Cliffton Insurance
            brokerages.Add(CreateBrokerage(++count, "Cliffton Insurance", "images/cliffton.jpg",
                CreateAddress("1902 1st Ave S", null, 42.5054234d, -94.171931d, "50501", "Fort Dodge", 16),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Horace Mann
            brokerages.Add(CreateBrokerage(++count, "Horace Mann", "images/horacemann.jpg",
                CreateAddress("238 N Mead St", null, 37.6892415d, -97.3284668d, "67202", "Wichita", 17),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Wanderwell
            brokerages.Add(CreateBrokerage(++count, "Wanderwell", "images/wanderwell.jpg",
                CreateAddress("301 Bolivar St", "Room 305", 38.0429042d, -84.50639620000001d, "40508", "Lexington", 18),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Louisiana Brokers United
            brokerages.Add(CreateBrokerage(++count, "Louisiana Brokers United", "images/louisiana.jpg",
                CreateAddress("141 Main St", null, 30.4513987d, -91.1901135d, "70801", "Baton Rouge", 19),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Freedom Insurance Services
            brokerages.Add(CreateBrokerage(++count, "Freedom Insurance Services", "images/freedom.jpg",
                CreateAddress("344 Woodford St", null, 43.6699138d, -70.2935853d, "04103", "Portland", 20),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Spiral Insurances
            brokerages.Add(CreateBrokerage(++count, "Spiral Insurances", "images/spiral.jpg",
                CreateAddress("1200 Liberty St", null, 39.4157845d, -79.4164797d, "21550", "Oakland", 21),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Brenda Nom
            brokerages.Add(CreateBrokerage(++count, "Brenda Nom", "images/brendanom.jpg",
                CreateAddress("704 Park St", null, 41.7583872d, -72.6888616d, "06106", "Hartford", 22),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region White Shield
            brokerages.Add(CreateBrokerage(++count, "White Shield", "images/whiteshield.jpg",
                CreateAddress("1716 Brainard St", null, 42.3417153d, -83.0757735d, "48208", "Detroit", 23),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Futura Insurance
            brokerages.Add(CreateBrokerage(++count, "Futura Insurance", "images/futura.jpg",
                CreateAddress("78 Mill St", null, 44.095938d, -91.74980309999999d, "55959", "Minnesota City", 24),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Kings Bridge
            brokerages.Add(CreateBrokerage(++count, "Kings Bridge", "images/kingsbridge.jpg",
                CreateAddress("263 S West St", null, 32.2981656d, -90.18442669999999d, "39201", "Jackson", 25),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Fields Insurance
            brokerages.Add(CreateBrokerage(++count, "Fields Insurance", "images/fields.jpg",
                CreateAddress("111 W Madison St", "Room 12", 37.6460947, -93.0925656d, "65622", "Buffalo", 26),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Wilkes Insurance
            brokerages.Add(CreateBrokerage(++count, "Wilkes Insurance", "images/wilkes.jpg",
                CreateAddress("100 N Montana Ave", null, 46.5853506d, -112.02034d, "59601", "Helena", 27),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Deland Gibson
            brokerages.Add(CreateBrokerage(++count, "Deland Gibson", "images/delandgibson.jpg",
                CreateAddress("2317 Dudley St", null, 40.825969d, -96.686526d, "68503", "Lincoln", 28),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Birla Sun
            brokerages.Add(CreateBrokerage(++count, "Birla Sun", "images/birla.jpg",
                CreateAddress("1920 W Bonanza Rd", null, 36.177628d, -115.167627d, "89106", "Las Vegas", 29),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Grange Insurance
            brokerages.Add(CreateBrokerage(++count, "Grange Insurance", "images/grange.jpg",
                CreateAddress("987 Gold St", "17th Floor", 42.95693929999999d, -71.4432127d, "03103", "Manchester", 30),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Class Insurance
            brokerages.Add(CreateBrokerage(++count, "Class Insurance", "images/class.jpg",
                CreateAddress("579 Grand St", null, 40.7172443d, -74.0602921d, "07304", "Jersey City", 31),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Chem Brokers
            brokerages.Add(CreateBrokerage(++count, "Chem Brokers", "images/chem.jpg",
                CreateAddress("3001 Montgomery Blvd NE", null, 35.1305785d, -106.5204528d, "87111", "Albuquerque", 32),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region WA Commercial
            brokerages.Add(CreateBrokerage(++count, "WA Commercial", "images/wa_insurance.jpg",
                CreateAddress("419 Broome St", null, 40.7210569d, -73.9988113, "10013", "New York", 33),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region USA Insurance
            brokerages.Add(CreateBrokerage(++count, "USA Insurance", "images/usa_insurance.jpg",
                CreateAddress("211 S Tryon St", null, 35.226059d, -80.844464d, "28202", "Charlotte", 34),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region HCC Brokers
            brokerages.Add(CreateBrokerage(++count, "HCC Brokers", "images/hcc.jpg",
                CreateAddress("1233 Redwood Dr", null, 47.96085129999999d, -97.36904760000002d, "58202", "Grand Forks", 35),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Lucas & Wright
            brokerages.Add(CreateBrokerage(++count, "Lucas & Wright", "images/lucas_insurance.jpg",
                CreateAddress("236 E Church St", null, 40.1101574d, -83.749286d, "43078", "Urbana", 36),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region AMS Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "AMS Insurance Brokers", "images/ams.jpg",
                CreateAddress("1400 E 33rd St", null, 35.6236662d, -97.46043019999999d, "73013", "Edmond", 37),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Marsh Down
            brokerages.Add(CreateBrokerage(++count, "Marsh Down", "images/marshdown.jpg",
                CreateAddress("1299 SW Broadway", null, 45.5153589d, -122.6817912d, "97205", "Portland", 38),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Integra Insurance Broker
            brokerages.Add(CreateBrokerage(++count, "Integra Insurance Broker", "images/integra.jpg",
                CreateAddress("21 Golf Dr", "3rd Floor", 40.2968601d, -76.87620130000001d, "17107", "Harrisburg", 39),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Chartered Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "Chartered Insurance Brokers", "images/chartered.jpg",
                CreateAddress("101 Division St", "Room 04", 41.9686902d, -71.4764742d, "02838", "Lincoln", 40),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region MGA Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "MGA Insurance Brokers", "images/mga.jpg",
                CreateAddress("1912 Baxter St", null, 32.852407d, -79.9652019d, "29405", "North Charleston", 41),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Trusted Choice
            brokerages.Add(CreateBrokerage(++count, "Trusted Choice", "images/trusted.jpg",
                CreateAddress("12 E Rice St", "2nd Floor", 43.572638d, -96.67485139999999d, "57103", "Sioux Falls", 42),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Thomas Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "Thomas Insurance Brokers", "images/thomas.jpg",
                CreateAddress("803 E 10th St", null, 35.040237d, -85.29619500000001d, "37403", "Chattanooga", 43),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Lions Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "Lions Insurance Brokers", "images/lions.jpg",
                CreateAddress("178 E Houston St", null, 29.4264499d, -98.4922138d, "78205", "San Antonio", 44),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Whitbread
            brokerages.Add(CreateBrokerage(++count, "Whitbread", "images/whitbread.jpg",
                CreateAddress("80 W Lakeview Rd", null, 40.3420713d, -111.7277632d, "84042", "Lindon", 45),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Henshalls Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "Henshalls Insurance Brokers", "images/henshalls.jpg",
                CreateAddress("56 Elm St", null, 44.2616843d, -72.57543489999999d, "05681", "Montpelier", 46),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Swinton Insurance
            brokerages.Add(CreateBrokerage(++count, "Swinton Insurance", "images/swinton.jpg",
                CreateAddress("101 N 8th St", null, 37.5397484d, -77.4361024d, "23219", "Richmond", 47),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Platinum Insurance Brokers
            brokerages.Add(CreateBrokerage(++count, "Platinum Insurance Brokers", "images/platinum.jpg",
                CreateAddress("780 Pakway Blvd", null, 47.314547d, -119.560034d, "98823", "Washington", 48),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Champion
            brokerages.Add(CreateBrokerage(++count, "Champion", "images/champion.jpg",
                CreateAddress("499 Summers St", null, 38.3528487d, -81.631428d, "25301", "Charleston", 49),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Target Brokers
            brokerages.Add(CreateBrokerage(++count, "Target Brokers", "images/target.jpg",
                CreateAddress("326 E Mason St", "10th Floor", 43.0403072d, -87.9070909d, "53202", "Milwaukee", 50),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Premier Brokers
            brokerages.Add(CreateBrokerage(++count, "Premier Brokers", "images/premier.jpg",
                CreateAddress("1500 W 24th St", "2nd Floor", 40.6993697d, -99.1009878d, "82002", "Cheyenne", 51),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            DbContext.Set<Brokerage>().AddIfNotExists(
               brokerages.ToArray()
            );
            DbContext.SaveChanges();//necessary to create the address tree

            brokerages.Clear();

            #region Phoenix Insurance
            brokerages.Add(CreateBrokerage(++count, "Phoenix Insurance", "images/phoenix.jpg",
                CreateAddress("94 Warren St", "7th Floor", 40.7156065d, -74.0107821d, "10007", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Thorn Circle
            brokerages.Add(CreateBrokerage(++count, "Thorn Circle", "images/thorncircle.jpg",
                CreateAddress("93 Water St", null, 40.7046181d, -74.0081666d, "10005", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Water Drop Brokers
            brokerages.Add(CreateBrokerage(++count, "Water Drop Brokers", "images/waterdrop.jpg",
                CreateAddress("320 E 10th St", "Room 201", 40.7273515d, -73.9810837d, "10009", "New York", 33),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Nobilis Insurances
            brokerages.Add(CreateBrokerage(++count, "Nobilis Insurances", "images/nobilis.jpg",
                CreateAddress("182 Bleecker St", null, 40.728893d, -74.001086d, "10012", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Starwhale 
            brokerages.Add(CreateBrokerage(++count, "Starwhale ", "images/starwhale.jpg",
                CreateAddress("110 Jewel Ave", null, 40.723948, -73.843779, "11375", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Hi-Lo Brokerage
            brokerages.Add(CreateBrokerage(++count, "Hi-Lo Brokerage", "images/hilo.jpg",
                CreateAddress("791 8th Ave", null, 40.761268d, -73.987253d, "10019", "New York", 33),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 10)
            ));
            #endregion
            #region Klöpfer Holzhandel
            brokerages.Add(CreateBrokerage(++count, "Klöpfer Holzhandel", "images/klopfer.jpg",
                CreateAddress("51 Cooper Square", null, 40.7285044d, -73.9904112d, "10003", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(false, Product.AccountantsId, 10)
            ));
            #endregion
            #region Eaton Brokers
            brokerages.Add(CreateBrokerage(++count, "Eaton Brokers", "images/eaton.jpg",
                CreateAddress("1305 St Nicholas Ave", "3rd Floor", 40.8458023d, -73.9365435d, "10033", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 15)
            ));
            #endregion
            #region Saturn Insurances
            brokerages.Add(CreateBrokerage(++count, "Saturn Insurances", "images/saturn.jpg",
                CreateAddress("170 East End Ave", null, 40.7760661d, -73.94438959999999d, "10128", "New York", 33),
                new BrokerageProduct(true, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 15)
            ));
            #endregion
            #region Galaxy Brokers
            brokerages.Add(CreateBrokerage(++count, "Galaxy Brokers", "images/galaxy.jpg",
                CreateAddress("43 Hunterspoint Ave", "Room 101", 40.7399928d, -73.93490419999999d, "11101", "Long Island City", 33),
                new BrokerageProduct(false, Product.ArchitectectsEngineersId, 15),
                new BrokerageProduct(true, Product.AccountantsId, 15)
            ));
            #endregion

            DbContext.Set<Brokerage>().AddIfNotExists(
               brokerages.ToArray()
            );
        }
    }
}
