﻿using ProtectorUS.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProtectorUS.Data.Migrations.Seeds
{
    using System.Data.Entity.Migrations;
    using RatingPlanDoc = Model.DocumentDB.RatingPlan;
    public class BaseSeed : Seed
    {
        public BaseSeed(DbContext dbContext) : base(dbContext)
        {
        }

        public override void Up()
        {
            SeedCountries();
            SeedStates();
            SeedCarriers();
            UpdateStates();
            SeedCancelReasons();
            SeedProducts();
            SeedProductConditions();
            SeedRatingPlanForms();
            SeedCoverages();
            SeedPaymentMethods();
            SeedCompanies();
            SeedProfilesAndPermissions();
            SeedAppClients();
            SeedEmailTemplates();            
        }
        public void SeedCountries()
        {
            DbContext.Set<Country>().AddIfNotExists(
               new Country() { Id = 1, Name = "United States", Abbreviation = "US" }
            );
        }

        internal const long kentuckyId = 18;
        internal const long westVirginiaId = 49;
        internal const long newJerseyId = 31;
        private State[] GetStates()
        {
            return new State[]
            {
                new State() { Id = 1, Name = "Alabama", CountryId = 1, Abbreviation = "AL" },
                new State() { Id = 2, Name = "Alaska", CountryId = 1, Abbreviation = "AK" },
                new State() { Id = 3, Name = "Arizona", CountryId = 1, Abbreviation = "AZ" },
                new State() { Id = 4, Name = "Arkansas", CountryId = 1, Abbreviation = "AR" },
                new State() { Id = 5, Name = "California", CountryId = 1, Abbreviation = "CA" },
                new State() { Id = 6, Name = "Colorado", CountryId = 1, Abbreviation = "CO" },
                new State() { Id = 7, Name = "Connecticut", CountryId = 1, Abbreviation = "CT" },
                new State() { Id = 8, Name = "District of Columbia", CountryId = 1, Abbreviation = "DC" },
                new State() { Id = 9, Name = "Delaware", CountryId = 1, Abbreviation = "DE" },
                new State() { Id = 10, Name = "Florida", CountryId = 1, Abbreviation = "FL" },
                new State() { Id = 11, Name = "Georgia", CountryId = 1, Abbreviation = "GA" },
                new State() { Id = 12, Name = "Hawaii", CountryId = 1, Abbreviation = "HI" },
                new State() { Id = 13, Name = "Idaho", CountryId = 1, Abbreviation = "ID" },
                new State() { Id = 14, Name = "Illinois", CountryId = 1, Abbreviation = "IL" },
                new State() { Id = 15, Name = "Indiana", CountryId = 1, Abbreviation = "IN" },
                new State() { Id = 16, Name = "Iowa", CountryId = 1, Abbreviation = "IA" },
                new State() { Id = 17, Name = "Kansas", CountryId = 1, Abbreviation = "KS" },
                new State() { Id = kentuckyId, Name = "Kentucky", CountryId = 1, Abbreviation = "KY" },
                new State() { Id = 19, Name = "Louisiana", CountryId = 1, Abbreviation = "LA" },
                new State() { Id = 20, Name = "Maine", CountryId = 1, Abbreviation = "ME" },
                new State() { Id = 21, Name = "Maryland", CountryId = 1, Abbreviation = "MD" },
                new State() { Id = 22, Name = "Massachusetts", CountryId = 1, Abbreviation = "MA" },
                new State() { Id = 23, Name = "Michigan", CountryId = 1, Abbreviation = "MI" },
                new State() { Id = 24, Name = "Minnesota", CountryId = 1, Abbreviation = "MN" },
                new State() { Id = 25, Name = "Mississippi", CountryId = 1, Abbreviation = "MS" },
                new State() { Id = 26, Name = "Missouri", CountryId = 1, Abbreviation = "MO" },
                new State() { Id = 27, Name = "Montana", CountryId = 1, Abbreviation = "MT" },
                new State() { Id = 28, Name = "Nebraska", CountryId = 1, Abbreviation = "NE" },
                new State() { Id = 29, Name = "Nevada", CountryId = 1, Abbreviation = "NV" },
                new State() { Id = 30, Name = "New Hampshire", CountryId = 1, Abbreviation = "NH" },
                new State() { Id = newJerseyId, Name = "New Jersey", CountryId = 1, Abbreviation = "NJ" },
                new State() { Id = 32, Name = "New Mexico", CountryId = 1, Abbreviation = "NM" },
                new State() { Id = 33, Name = "New York", CountryId = 1, Abbreviation = "NY" },
                new State() { Id = 34, Name = "North Carolina", CountryId = 1, Abbreviation = "NC" },
                new State() { Id = 35, Name = "North Dakota", CountryId = 1, Abbreviation = "ND" },
                new State() { Id = 36, Name = "Ohio", CountryId = 1, Abbreviation = "OH" },
                new State() { Id = 37, Name = "Oklahoma", CountryId = 1, Abbreviation = "OK" },
                new State() { Id = 38, Name = "Oregon", CountryId = 1, Abbreviation = "OR" },
                new State() { Id = 39, Name = "Pennsylvania", CountryId = 1, Abbreviation = "PA" },
                new State() { Id = 40, Name = "Rhode Island", CountryId = 1, Abbreviation = "RI" },
                new State() { Id = 41, Name = "South Carolina", CountryId = 1, Abbreviation = "SC" },
                new State() { Id = 42, Name = "South Dakota", CountryId = 1, Abbreviation = "SD" },
                new State() { Id = 43, Name = "Tennessee", CountryId = 1, Abbreviation = "TN" },
                new State() { Id = 44, Name = "Texas", CountryId = 1, Abbreviation = "TX" },
                new State() { Id = 45, Name = "Utah", CountryId = 1, Abbreviation = "UT" },
                new State() { Id = 46, Name = "Vermont", CountryId = 1, Abbreviation = "VT" },
                new State() { Id = 47, Name = "Virginia", CountryId = 1, Abbreviation = "VA" },
                new State() { Id = 48, Name = "Washington", CountryId = 1, Abbreviation = "WA" },
                new State() { Id = westVirginiaId, Name = "West Virginia", CountryId = 1, Abbreviation = "WV" },
                new State() { Id = 50, Name = "Wisconsin", CountryId = 1, Abbreviation = "WI" },
                new State() { Id = 51, Name = "Wyoming", CountryId = 1, Abbreviation = "WY" }
            };
        }
        public void SeedStates()
        {
            DbContext.Set<State>().AddIfNotExists(
                GetStates()
            );
        }
        /// <summary>
        /// Splitted, because Carrier has circular dependency with State.
        /// </summary>
        public void UpdateStates()
        {
            var states = GetStates();
            states.FirstOrDefault(s => s.Abbreviation == "AL").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "AZ").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "AR").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "CA").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "CO").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "CT").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "DC").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "DE").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "FL").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "GA").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "ID").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "IL").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "IN").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "IA").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "KS").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "KY").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "ME").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "MD").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "MA").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "MI").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "MN").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "MS").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "MO").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "MT").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "NE").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "NV").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "NH").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "NJ").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "NM").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "NY").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "NC").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "ND").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "OH").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "OK").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "OR").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "PA").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "RI").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "SC").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "SD").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "TN").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "TX").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "UT").CarrierId = 2;
            states.FirstOrDefault(s => s.Abbreviation == "VT").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "VA").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "WA").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "WV").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "WI").CarrierId = 1;
            states.FirstOrDefault(s => s.Abbreviation == "WY").CarrierId = 1;
            DbContext.Set<State>().AddOrUpdate(s => s.Id,
                states
           );
        }
        public void SeedRatingPlanForms()
        {
            DbContext.SaveChanges();
            var products = DbContext.Set<Product>();
            var states = DbContext.Set<State>();
            var ratingPlanForms = new List<RatingPlanForm>();
            long count = 1;
            foreach (Product product in products)
            {
                foreach (State state in states)
                {
                    var ratingPlanForm = new RatingPlanForm()
                    {
                        Id = count++,
                        Active = HasRatingPlanActive(state),
                        StateId = state.Id,
                        ProductId = product.Id,
                    };
                    ratingPlanForm.AddNewVersion(1);
                    ratingPlanForms.Add(ratingPlanForm);
                }
            }
            DbContext.Set<RatingPlanForm>().AddIfNotExists(
               ratingPlanForms.ToArray()
            );
        }
        public void SeedProductConditions()
        {
            DbContext.SaveChanges();
            var products = DbContext.Set<Product>();
            var states = DbContext.Set<State>();
            var productConditions = new List<ProductCondition>();
            long count = 1;
            foreach (Product product in products)
            {
                foreach (State state in states)
                {
                    var productCondition = new ProductCondition()
                    {
                        Id = count++,
                        Active = true,
                        StateId = state.Id,
                        ProductId = product.Id,
                        FilePath = ""
                    };
                    productConditions.Add(productCondition);
                }
            }
            DbContext.Set<ProductCondition>().AddIfNotExists(
               productConditions.ToArray()
            );
        }
        private bool HasRatingPlanActive(State state)
        {
            switch (state.Abbreviation)
            {
                case "AK":
                case "HI":
                case "LA":
                    return false;
            }
            return true;
        }
        public void SeedProfilesAndPermissions()
        {
            DbContext.Set<PermissionGroup>().AddIfNotExists(
                new PermissionGroup() { Id = Profile.ManagerUserPermissions.GroupId, Name = "ARGO Manager" },
                new PermissionGroup() { Id = Profile.BrokerPermissions.GroupId, Name = "Broker Center" }
            );

            var dbProfile = DbContext.Set<Profile>().Include("Permissions");
            var adminManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.AdminManagerUser) ?? Profile.Profiles.AdminManagerUser;
            var directorManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.DirectorManagerUser) ?? Profile.Profiles.DirectorManagerUser;
            var managerManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.ManagerManagerUser) ?? Profile.Profiles.ManagerManagerUser;
            var underwriterManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.UnderwriterManagerUser) ?? Profile.Profiles.UnderwriterManagerUser;
            var claimsManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.ClaimsManagerUser) ?? Profile.Profiles.ClaimsManagerUser;
            var assitantManagerUser = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.AssistantManagerUser) ?? Profile.Profiles.AssistantManagerUser;

            var masterBroker = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.MasterBroker) ?? Profile.Profiles.MasterBroker;
            var adminBroker = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.AdminBroker) ?? Profile.Profiles.AdminBroker;
            var commonBroker = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.AgentBroker) ?? Profile.Profiles.AgentBroker;

            var insured = dbProfile.FirstOrDefault(p => p.Id == Profile.Ids.Insured) ?? Profile.Profiles.Insured;

            var profiles = new List<Profile>()
            {
                adminManagerUser,
                directorManagerUser,
                managerManagerUser,
                underwriterManagerUser,
                claimsManagerUser,
                assitantManagerUser,
                masterBroker,
                adminBroker,
                commonBroker,
                insured
            };

            UpdatePermissions(profiles);
        }
        public void UpdatePermissions(IEnumerable<Profile> profiles = null)
        {
            profiles = profiles ?? DbContext.Set<Profile>().Include("Permissions").ToList();
            DbSet<Permission> dbPermissions = DbContext.Set<Permission>();
            var currentPermissions = new List<Permission>();
            Func<Permission, Permission, bool> predicate = (n, i) => n.Key == i.Key && n.GroupId == i.GroupId;
            foreach (var profile in profiles)
            {
                var permissions = Profile.GetPermissions(profile.Id, dbPermissions);
                if (permissions.HasItem())
                {
                    profile.Permissions.Clear();
                    profile.Permissions.AddRange(permissions);
                    currentPermissions.AddRangeIfNot(profile.Permissions, predicate);
                }
            }

            var deletedPermissions = dbPermissions.Outer(currentPermissions, (l, r) => l.Id == r.Id);
            dbPermissions.RemoveRange(deletedPermissions);

            foreach (Profile profile in profiles)
            {
                var profileEntry = DbContext.Entry(profile);
                if (profileEntry.State == EntityState.Detached)
                {
                    profileEntry.State = EntityState.Added;
                }
                else
                {
                    foreach (var permission in profile.Permissions)
                    {
                        var permissionEntry = DbContext.Entry(permission);
                        if (permissionEntry.State == EntityState.Added)
                        {
                            profileEntry.State = EntityState.Modified;
                            break;
                        }
                    }
                }
            }
        }
        public void SeedProducts()
        {
            DbContext.Set<Product>().AddIfNotExists(
               new Product() { Id = Product.ArchitectectsEngineersId, Name = "Architects & Engineers", Slogan = "Professional Liability", Code = "PAE", Alias = Product.ArchitectectsEngineersAlias, Abbreviation = "A&E", Active = true, Icon = "" },
               new Product() { Id = Product.AccountantsId, Name = "Accountants", Slogan = "Professional Liability", Code = "PAC", Alias = Product.AccountantsAlias, Abbreviation = "APL", Active = true, Icon = "" }
            );
        }
        public void SeedCoverages()
        {
            //Architects & Engineers Coverages
            DbContext.Set<Coverage>().AddIfNotExists(
               new Coverage(1, "EXPANDABLE LIMIT")
               {
                   ProductId = Product.ArchitectectsEngineersId,
                   Description = "You can purchase limits of liability for $250,000 to $2,000,000 and can increase those limits mid-term if you take on larger projects. If you need limits greater than $2,000,000, or project specific limits, your policy can be transferred to an Underwriter who may be able to meet those needs as well."
               },
                new Coverage(2, "BODILY INJURY & PROPERTY DAMAGE")
                {
                    ProductId = Product.ArchitectectsEngineersId,
                    Description = "You're protected for a wide range of events including workers injured on the job or the presence of mold, asbestos, pollutants, or other damage to property."
                },
                new Coverage(3, "FLEXIBILITY CLAIMS")
                {
                    ProductId = Product.ArchitectectsEngineersId,
                    Description = "Protector allows for an extended reporting period to file a claim should anyone covered by the policy die or is rendered disabled during the policy period."
                },
                new Coverage(4, "COVERAGE FOR MULTIPLE PROFESSIONS")
                {
                    ProductId = Product.ArchitectectsEngineersId,
                    Description = "Protector Architects & Engineers provides liability coverage for a broad range of professional disciplines related to building design and construction, including (but not limited to): Architects, Engineers, Interior, Designers, Property Surveyors, Construction Managers, and many more."
                },
                new Coverage(5, "LEGAL PROTECTION")
                {
                    ProductId = Product.ArchitectectsEngineersId,
                    Description = "In the event of legal action against you or your firm, we will immediately mount a defense. Even if the claim appears to lack grounds, or there is no liability on your part, we will appoint an attorney to protect your interests until you are cleared of liability or the case is settled."
                }
            );

            //Accountants Coverages
            DbContext.Set<Coverage>().AddIfNotExists(
               new Coverage(6, "COVERAGE ACROSS A RANGE OF FINANCIAL PROFESSIONS")
               {
                   ProductId = Product.AccountantsId,
                   Description = "Protector Accountants covers a broad spectrum of professionals in financial services and related disciplines including (but not limited to): Accountant, Bookkeeper, Tax preparer, Investment advisor, Enrolled agent, Notary, Personal Fiduciary, and more."
               },
               new Coverage(7, "LEGAL PROTECTION")
               {
                   ProductId = Product.AccountantsId,
                   Description = "In the event of legal action against you or your firm, we will immediately mount a defense. Even if the claim appears to lack grounds, or there is no liability on your part, we will appoint an attorney to protect your interests until you are cleared of liability or the case is settled."
               },
               new Coverage(8, "DATA BREACH")
               {
                   ProductId = Product.AccountantsId,
                   Description = "Financial professionals are changed with maintaining the confidentiality of a vast amount of sensitive client information. Losing control of that data, whether the result of a vírus, accidental dissemination, or an unauthorized external breach, can not only damage a firm's reputation, but can cripple it with ensuing damages. Protector covers the defense you firm will need in the event of legal action."
               },
               new Coverage(9, "CLIENT REFUND")
               {
                   ProductId = Product.AccountantsId,
                   Description = "Whether in preparing taxes or providing advice, errors can, and often do, happen. Should a covered professional make an error or omission that results in additional taxes, penalties or interest levied on a client of the insured, we will cover legal expenses, and reimburse the client for those additional costs."
               }
           );
        }
        public void SeedCancelReasons()
        {
            DbContext.Set<CancelReason>().AddIfNotExists(
              new CancelReason() { Id = 1, Description = "There's an error in my policy" },
              new CancelReason() { Id = 2, Description = "I'm unsatisfied with the product" },
              new CancelReason() { Id = 3, Description = "I don't need an insurance anymore" },
              new CancelReason() { Id = 4, Description = "Other reason" }
           );
        }
        public void SeedPaymentMethods()
        {
            int masterCardId = (int)CreditCardBrand.MasterCard;
            int visaId = (int)CreditCardBrand.Visa;
            int americanExpressId = (int)CreditCardBrand.AmericanExpress;
            int discoverId = (int)CreditCardBrand.Discover;
            int dinersClubId = (int)CreditCardBrand.DinersClub;
            int jcbId = (int)CreditCardBrand.JCB;

            DbContext.Set<PaymentMethod>().AddIfNotExists(
               new PaymentMethod() { Id = masterCardId, Name = "MasterCard", Icon = "" },
               new PaymentMethod() { Id = visaId, Name = "Visa", Icon = "" },
               new PaymentMethod() { Id = americanExpressId, Name = "American Express", Icon = "" },
               new PaymentMethod() { Id = discoverId, Name = "Discover", Icon = "" },
               new PaymentMethod() { Id = dinersClubId, Name = "Diners Club", Icon = "" },
               new PaymentMethod() { Id = jcbId, Name = "JCB", Icon = "" }
            );
            DbContext.Set<ProductPaymentMethod>().AddIfNotExists(
               new ProductPaymentMethod() { Id = 1, ProductId = 1, PaymentMethodId = 1, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 2, ProductId = 1, PaymentMethodId = 2, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 3, ProductId = 1, PaymentMethodId = 3, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 4, ProductId = 1, PaymentMethodId = 4, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 5, ProductId = 1, PaymentMethodId = 5, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 6, ProductId = 1, PaymentMethodId = 6, TotalInstallments = 1 },

               new ProductPaymentMethod() { Id = 7, ProductId = 2, PaymentMethodId = 1, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 8, ProductId = 2, PaymentMethodId = 2, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 9, ProductId = 2, PaymentMethodId = 3, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 10, ProductId = 2, PaymentMethodId = 4, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 11, ProductId = 2, PaymentMethodId = 5, TotalInstallments = 1 },
               new ProductPaymentMethod() { Id = 12, ProductId = 2, PaymentMethodId = 6, TotalInstallments = 1 }
            );
        }
        public void SeedAppClients()
        {
            string universalToken = AppClient.GenerateToken();
            DbContext.Set<AppClient>().AddIfNotExists(
                new AppClient(universalToken) { Id = 1, Name = "Web App", ClientId = "web", Active = true, AllowedOrigin = "*", RefreshTokenLifeTime = 7200, CurrentVersion = "1.0", ReviewVersion = "1.0", InReview = false },
                new AppClient(universalToken) { Id = 2, Name = "Android App", ClientId = "android", Active = true, AllowedOrigin = "*", RefreshTokenLifeTime = 7200, CurrentVersion = "1.0", ReviewVersion = "1.0", InReview = false },
                new AppClient(universalToken) { Id = 3, Name = "IOS App", ClientId = "ios", Active = true, AllowedOrigin = "*", RefreshTokenLifeTime = 7200, CurrentVersion = "1.0", ReviewVersion = "1.0", InReview = false }
            );
        }
        public void SeedCompanies()
        {
            DbContext.Set<Company>().AddIfNotExists(
                new Company() { Id = 1, Name = "ALTERIS" },
                new Company() { Id = 2, Name = "ARGOGLOBAL" },
                new Company() { Id = 3, Name = "ARGO INSURANCE" },
                new Company() { Id = 4, Name = "ARGO PRO" },
                new Company() { Id = 5, Name = "ARGO RE" },
                new Company() { Id = 6, Name = "ARGO SEGUROS" },
                new Company() { Id = 7, Name = "ARGO SURETY" },
                new Company() { Id = 8, Name = "ARIS" },
                new Company() { Id = 9, Name = "COLONY SPECIALTY" },
                new Company() { Id = 10, Name = "ROCKWOOD" },
                new Company() { Id = 11, Name = "TRIDENT" }
            );
        }
        public void SeedCarriers()
        {
            DbContext.Set<Carrier>().AddIfNotExists(
                new Carrier()
                {
                    Id = 1,
                    Name = "Argonaut Insurance Company",
                    Address = new Address()
                    {
                        StreetLine1 = "225 W. Washington Street",
                        StreetLine2 = "24th Floor",
                        Latitude = 41.8828438,
                        Longitude = -87.63498210000002,
                        Region = new Region()
                        {
                            ZipCode = "60606",
                            City = new City()
                            {
                                Name = "Chicago",
                                StateId = 14//illinois
                            }
                        }
                    }
                },
                new Carrier()
                {
                    Id = 2,
                    Name = "Colony Specialty Insurance Company",
                    Address = new Address()
                    {
                        StreetLine1 = "8720 Stony Point Parkway",
                        StreetLine2 = "Suite 400",
                        Latitude = 37.550683,
                        Longitude = -77.563248,
                        Region = new Region()
                        {
                            ZipCode = "23235",
                            City = new City()
                            {
                                Name = "Richmond",
                                StateId = 47,//virginia
                            }
                        }
                    }
                }
            );
            DbContext.SaveChanges();
        }
        public void SeedEmailTemplates()
        {
            string noReplyArgo = "no-reply@argo-protector.com";
            string displayNameArgo = "Protector";
            long count = 1;
            DbContext.Set<EmailTemplate>().AddIfNotExists(
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.Quotation.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Your Insurance Quote",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.ContactBroker.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "You Have a New Contact",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.Activation.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Your Policy Information",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.WelcomeManagerUser.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Welcome to Protector",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.WelcomeInsured.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Welcome to Protector",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.WelcomeBroker.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Welcome to Protector",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.WelcomeBrokerage.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Welcome to Protector",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.FnolClaim.BrokerId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A new claim was reported",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.FnolClaim.InsuredId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A new claim was reported",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.FnolClaim.ArgoId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A new claim was reported",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.ClaimStatus.BrokerId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A claim has changed status",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.ClaimStatus.ArgoId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A claim has changed status",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.ClaimStatus.InsuredId,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "A claim has changed status",
                },
                new EmailTemplate()
                {
                    Id = count++,
                    TemplateId = EmailTemplate.ForgotPassword.Id,
                    From = noReplyArgo,
                    FromName = displayNameArgo,
                    Subject = "Protector - Reset your password",
                }
            );
        }
    }
}
