﻿using System;
using System.Data.Entity;

namespace ProtectorUS.Data.Migrations.Seeds
{
    public abstract class Seed
    {
        public Seed(DbContext dbContext)
        {
            DbContext = dbContext;
        }
        public abstract void Up();
        protected DbContext DbContext { get; private set; }
        public static Seed CreateInstance(Type seedType, DbContext dbContext)
        {
            return Activator.CreateInstance(seedType, dbContext) as Seed;
        }
    }
}
