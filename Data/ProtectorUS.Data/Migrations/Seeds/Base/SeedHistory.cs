﻿using ProtectorUS.Model;
using System;

namespace ProtectorUS.Data.Migrations.Seeds
{
    public class SeedHistory : BaseEntity
    {
        public SeedHistory(Seed seed)
        {
            SeedId = seed.GetType().Name;
            ContextKey = seed.GetType().Namespace;
            RunDate = DateTime.Now;
        }
        public string SeedId { get; private set; }
        public string ContextKey { get; private set; }
        public DateTime RunDate { get; private set; }
    }
}
