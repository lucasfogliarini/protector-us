﻿using ProtectorUS.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProtectorUS.Data.Migrations.Seeds
{
    public abstract class SurchargeSeed : Seed
    {
        public SurchargeSeed(DbContext dbContext) : base(dbContext)
        {
        }

        public void SeedStateSurcharges()
        {
            DbContext.Set<SurchargePercentage>().AddIfNotExists(
              new SurchargePercentage()
              {
                  Id = 1,
                  Percentage = 0.0055m,
                  Surcharge = new StateSurcharge()
                  {
                      StateId = BaseSeed.westVirginiaId,
                      Code = "WVFC",
                      Description = "WV Fire & Casualty Surcharge",
                      Type = SurchargeType.Fee,
                  }
              },
              new SurchargePercentage()
              {
                  Id = 2,
                  Percentage = 0.0007m,
                  Surcharge = new StateSurcharge()
                  {
                      StateId = BaseSeed.newJerseyId,
                      Code = "LIGA",
                      Description = "NJ PLIGA",
                      Type = SurchargeType.Fee,
                  }
              },
              new SurchargePercentage()
              {
                  Id = 3,
                  Percentage = 0.018m,
                  Surcharge = new StateSurcharge()
                  {
                      StateId = BaseSeed.kentuckyId,
                      Code = "KYMTHLY",
                      Description = "KY Monthly Premium Surcharge",
                      Type = SurchargeType.Fee,
                  }
              }
           );
        }
        public void SeedKentuckyCitiesTaxed()
        {
            var citiesTaxed = GetKentuckyCitiesTaxed();
            List<City> cities = new List<City>();
            foreach (CityTax cityTax in citiesTaxed)
            {
                City city = new City()
                {
                    Id = cityTax.CityId,
                    Name = cityTax.Name,
                    StateId = BaseSeed.kentuckyId
                };
                cities.Add(city);
            }
            DbContext.Set<City>().AddIfNotExists(
               cities.ToArray()
            );
        }
        public void SeedKentuckySurcharges()
        {
            var citiesTaxed = GetKentuckyCitiesTaxed();

            long countySurchargeCount = 1;
            List<CountySurcharge> countySurcharges = new List<CountySurcharge>();
            foreach (var cityTaxed in citiesTaxed)
            {
                countySurchargeCount++;
                CountySurcharge countySurcharge;
                if (cityTaxed.County == null)//Taxed by City
                {
                    countySurcharge = CreateCountySurcharge(countySurchargeCount, countySurchargeCount.ToString(), cityTaxed.Name, cityTaxed.Min, cityTaxed.Tax);
                    countySurcharges.Add(countySurcharge);
                }
                else//Taxed by County
                {
                    string countyCode = cityTaxed.County.Code.ToString();
                    countySurcharge = countySurcharges.FirstOrDefault(cs => cs.Code == countyCode);
                    if (countySurcharge == null)
                    {
                        countySurcharge = CreateCountySurcharge(countySurchargeCount, countyCode, cityTaxed.County.Name, cityTaxed.County.Min, cityTaxed.County.Tax);
                        countySurcharges.Add(countySurcharge);
                    }
                }
                var city = DbContext.Set<City>().Find(cityTaxed.CityId);
                countySurcharge.Cities.Add(city);
            }

            DbContext.Set<CountySurcharge>().AddIfNotExists(
               countySurcharges.ToArray()
            );
        }

        private IEnumerable<CityTax> GetKentuckyCitiesTaxed()
        {
            #region Counties and Cities
            //* cities taxed only by counties

            //Anderson County
            //Lawrenceburg

            //Bell County
            //Middlesboro, Pineville

            //Breckinridge County
            //Cloverport, Hardinsburg, Irvington

            //Campbell County
            //Alexandria, Bellevue, California, Cold Spring, Crestview, Dayton, Fort Thomas, Highland Heights,
            //Melbourne, Mentor, Newport, Silver Grove, Southgate, Wilder, Woodlawn

            //Carter County
            //Grayson, Olive Hill

            //Casey County
            //Liberty

            //Clark County
            //Winchester

            //Crittenden County
            //Dycusburg*, Marion

            //Daviess County
            //Owensboro, Whitesville

            //Elliott County
            //Sandy Hook

            //Fleming County
            //Ewing, Flemingsburg

            //Franklin County
            //Frankfort

            //Fulton County
            //Fulton*, Hickman

            //Green County
            //Greensburg

            //Henderson County
            //Corydon, Henderson, Robards

            //Hopkins County
            //Earlington, Hanson, Madisonville, Mortons Gap, Nebo, Nortonville, St. Charles, White Plains, Dawson Springs

            //Jackson County
            //McKee

            //Kenton County
            //Bromley, Covington, Crescent Springs, Crestview Hills*, Edgewood*, Elsmere, Erlanger, Fairview*, Fort Mitchell, Fort Wright, Independence*, 
            //Kenton Vale*, Lakeside Park, Ludlow, Park Hills, Ryland Heights, Taylor Mill, Villa Hills

            //Lewis County
            //Concord*, Vanceburg

            //Mason County
            //Dover, Maysville, Sardis*

            //Meade County
            //Brandenburg, Ekron, Muldraugh

            //Oldham County
            //Crestwood, Goshen, La Grange*, Orchard Grass Hills, Pewee Valley, River Bluff

            //Owen County
            //Gratz*, Monterey*, Owenton*

            //Owsley County
            //Booneville*

            //Powell County
            //Clay City, Stanton

            //Pulaski County
            //Burnside, Ferguson*, Science Hill, Somerset, Eubank*

            //Robertson County
            //Mount Olivet*, Sardis*

            //Rockcastle County
            //Brodhead, Livingston, Mount Vernon

            //Spencer County
            //Taylorsville

            //Todd County
            //Allensville*, Elkton, Guthrie, Trenton

            //Trigg County
            //Cadiz

            //Trimble County
            //Bedford, Milton

            //Washington County
            //Mackville*, Springfield, Willisburg*

            //Wayne County
            //Monticello
            #endregion

            #region Taxed by City
            List<CityTax> cityTaxedByCity = new List<CityTax>()
            {
                new CityTax() { Tax=0, Name = "Adairville" },
                new CityTax() { Tax=0.1m, Name = "Albany" },
                new CityTax() { Tax=0.1m, Name = "Alexandria", Min = 5 },//campbell
                new CityTax() { Tax=0.06m, Name = "Allen" },
                new CityTax() { Tax=0.05m, Name = "Anchorage" },
                new CityTax() { Tax=0.08m, Name = "Arlington", Min = 25 },
                new CityTax() { Tax=0.1m, Name = "Ashland" },
                new CityTax() { Tax=0.06m, Name = "Auburn" },
                new CityTax() { Tax=0.09m, Name = "Audubon Park" },
                new CityTax() { Tax=0.05m, Name = "Augusta" },
                new CityTax() { Tax=0.05m, Name = "Bancroft" },
                new CityTax() { Tax=0.05m, Name = "Barbourmeade" },
                new CityTax() { Tax=0.09m, Name = "Barbourville" },
                new CityTax() { Tax=0.09m, Name = "Bardwell" },
                new CityTax() { Tax=0.03m, Name = "Barlow" },// or 7
                new CityTax() { Tax=0.08m, Name = "Beattyville" },
                new CityTax() { Tax=0.069m, Name = "Beaver Dam" },
                new CityTax() { Tax=0.11m, Name = "Bedford", Min = 5 },//Trimble
                new CityTax() { Tax=0.05m, Name = "Beechwood Village" },
                new CityTax() { Tax=0.05m, Name = "Bellemeade" },
                new CityTax() { Tax=0.1m, Name = "Bellevue" },//Campbell
                new CityTax() { Tax=0.05m, Name = "Bellewood" },
                new CityTax() { Tax=0.075m, Name = "Benton" },
                new CityTax() { Tax=0.06m, Name = "Berea" },
                new CityTax() { Tax=0.0725m, Name = "Berry" },
                new CityTax() { Tax=0.05m, Name = "Blue Ridge Manor" },
                new CityTax() { Tax=0.08m, Name ="Bonnieville" },
                new CityTax() { Tax=0.07m, Name = "Bowling Green", Min = 5 },
                new CityTax() { Tax=0.03m, Name = "Bradfordsville" },
                new CityTax() { Tax=0.05m, Name = "Brandenburg" },//Meade
                new CityTax() { Tax=0.05m, Name = "Briarwood" },
                new CityTax() { Tax=0.06m, Name = "Brodhead" },//Rockcastle
                new CityTax() { Tax=0.05m, Name = "Broeck Pointe" },
                new CityTax() { Tax=0.1m, Name = "Bromley" },//kenton
                new CityTax() { Tax=0.07m, Name = "Brooksville" },
                new CityTax() { Tax=0.05m, Name = "Brownsboro Farm" },
                new CityTax() { Tax=0.05m, Name = "Brownsboro Village" },
                new CityTax() { Tax=0.07m, Name = "Brownsville" },
                new CityTax() { Tax=0.1m, Name = "Burgin" },
                new CityTax() { Tax=0.1m, Name = "Burkesville" },
                new CityTax() { Tax=0.08m, Name = "Burnside", Min = 5 },//pulaski
                new CityTax() { Tax=0.1m, Name = "Butler" },
                new CityTax() { Tax=0.06m, Name = "Cadiz" },//trigg
                new CityTax() { Tax=0.12m, Name = "Calhoun" },
                new CityTax() { Tax=0.1m, Name = "California" },//campbell
                new CityTax() { Tax=0.07m, Name = "Calvert City" },
                new CityTax() { Tax=0.05m, Name = "Cambridge" },
                new CityTax() { Tax=0.1m, Name = "Campbellsburg", Min = 5 },
                new CityTax() { Tax=0.1m, Name = "Campbellsville" },
                new CityTax() { Tax=0.08m, Name = "Campton" },
                new CityTax() { Tax=0, Name = "Caneyville", Min = 5 },
                new CityTax() { Tax=0.075m, Name = "Carlisle", Min = 5 },
                new CityTax() { Tax=0.08m, Name = "Carrollton" },
                new CityTax() { Tax=0.14m, Name = "Catlettsburg", Min = 5 },
                new CityTax() { Tax=0, Name = "Cave City" },
                new CityTax() { Tax=0.06m, Name = "Centertown" },
                new CityTax() { Tax=0.08m, Name = "Central City" },
                new CityTax() { Tax=0.05m, Name = "Clarkson", Min = 5 },
                new CityTax() { Tax=0.08m, Name = "Clay" },//webster
                new CityTax() { Tax=0, Name = "Clay City" },//powell
                new CityTax() { Tax=0.09m, Name = "Cloverport", Min = 5 },//breckinridge
                new CityTax() { Tax=0.4m, Name = "Coal Run Village" },
                new CityTax() { Tax=0.1m, Name = "Cold Spring" },//campbell
                new CityTax() { Tax=0.05m, Name = "Coldstream" },
                new CityTax() { Tax=0.1m, Name = "Columbia" },
                new CityTax() { Tax=0.09m, Name = "Corbin" },
                new CityTax() { Tax=0.1m, Name = "Corinth" },
                new CityTax() { Tax=0.12m, Name = "Corydon" },//henderson
                new CityTax() { Tax=0.12m, Name = "Covington" },//kenton
                new CityTax() { Tax=0.09m, Name = "Crab Orchard" },
                new CityTax() { Tax=0.058m, Name = "Creekside" },
                new CityTax() { Tax=0.05m, Name = "Crescent Springs" },//kenton
                new CityTax() { Tax=0.1m, Name = "Crestview" },//campbell
                new CityTax() { Tax=0.1m, Name = "Crestwood" },//oldham
                new CityTax() { Tax=0.09m, Name = "Crittenden" },
                new CityTax() { Tax=0.1m, Name = "Crofton" },
                new CityTax() { Tax=0.05m, Name = "Crossgate" },
                new CityTax() { Tax=0.08m, Name = "Cumberland" },
                new CityTax() { Tax=0.06m, Name = "Cynthiana" },
                new CityTax() { Tax=0.08m, Name = "Danville" },
                new CityTax() { Tax=0.1m, Name = "Dawson Springs" },//hopkins
                new CityTax() { Tax=0.11m, Name = "Dayton" },//campbell
                new CityTax() { Tax=0.05m, Name = "Dixon" },
                new CityTax() { Tax=0.05m, Name = "Douglass Hills" },
                new CityTax() { Tax=0.07m, Name = "Dover" },//mason
                new CityTax() { Tax=0.05m, Name = "Druid Hills" },
                new CityTax() { Tax=0.085m, Name = "Dry Ridge" },
                new CityTax() { Tax=0.1m, Name = "Earlington" },//hopkins
                new CityTax() { Tax=0.1m, Name = "Eddyville", Min = 5 },
                new CityTax() { Tax=0.07m, Name = "Edmonton" },
                new CityTax() { Tax=0.03m, Name = "Ekron" },//meade
                new CityTax() { Tax=0.08m, Name = "Elizabethtown" },
                new CityTax() { Tax=0.03m, Name = "Elkhorn City" },
                new CityTax() { Tax=0.08m, Name = "Elkton", Min = 10 },//todd
                new CityTax() { Tax=0.1m, Name = "Elsmere" },//kenton
                new CityTax() { Tax=0.1m, Name = "Eminence" },
                new CityTax() { Tax=0.1m, Name = "Erlanger" },//kenton
                new CityTax() { Tax=0.1m, Name = "Evarts" },
                new CityTax() { Tax=0.06m, Name = "Ewing" },//fleming
                new CityTax() { Tax=0.1m, Name = "Falmouth" },
                new CityTax() { Tax=0.05m, Name = "Fincastle" },
                new CityTax() { Tax=0.08m, Name = "Flatwoods", Min = 5 },
                new CityTax() { Tax=0.05m, Name = "Fleming-Neon" },
                new CityTax() { Tax=0.06m, Name = "Flemingsburg", Min = 10 },//fleming
                new CityTax() { Tax=0.08m, Name = "Florence" },
                new CityTax() { Tax=0.08m, Name = "Fordsville" },
                new CityTax() { Tax=0.05m, Name = "Forest Hills" },
                new CityTax() { Tax=0.07m, Name = "Fort Mitchell" },//kenton
                new CityTax() { Tax=0.1m, Name = "Fort Thomas" },//campbell
                new CityTax() { Tax=0.08m, Name = "Fort Wright" },//kenton
                new CityTax() { Tax=0.05m, Name = "Fountain Run" },
                new CityTax() { Tax=0.05m, Name = "Fox Chase" },//bullitt
                new CityTax() { Tax=0.06m, Name = "Frankfort" },//franklin
                new CityTax() { Tax=0.09m, Name = "Franklin" },
                new CityTax() { Tax=0.06m, Name = "Fredonia" },
                new CityTax() { Tax=0.05m, Name = "Frenchburg" },//menifee
                new CityTax() { Tax=0.07m, Name = "Gamaliel" },
                new CityTax() { Tax=0.05m, Name = "Georgetown" },
                new CityTax() { Tax=0.05m, Name = "Ghent" },
                new CityTax() { Tax=0.05m, Name = "Glasgow" },
                new CityTax() { Tax=0.06m, Name = "Glencoe" },
                new CityTax() { Tax=0.05m, Name = "Glenview" },
                new CityTax() { Tax=0.05m, Name = "Glenview Hills" },
                new CityTax() { Tax=0, Name = "Glenview Manor" },
                new CityTax() { Tax=0.1m, Name = "Goose Creek" },
                new CityTax() { Tax=0.1m, Name = "Goshen" },//oldham
                new CityTax() { Tax=0.05m, Name = "Graymoor-Devondale" },
                new CityTax() { Tax=0.105m, Name = "Grayson" },//carter
                new CityTax() { Tax=0.0575m, Name = "Green Spring" },
                new CityTax() { Tax=0.1m, Name = "Greensburg" },//green
                new CityTax() { Tax=0, Name = "Greenup", Min = 5 },
                new CityTax() { Tax=0.11m, Name = "Greenville" },
                new CityTax() { Tax=0.08m, Name = "Guthrie" },//todd
                new CityTax() { Tax=0.1m, Name = "Hanson" },//hopkins
                new CityTax() { Tax=0.06m, Name = "Hardinsburg" },//breckinridge
                new CityTax() { Tax=0.08m, Name = "Harlan" },
                new CityTax() { Tax=0.1m, Name = "Harrodsburg" },
                new CityTax() { Tax=0.1m, Name = "Hartford" },
                new CityTax() { Tax=0.08m, Name = "Hawesville" },
                new CityTax() { Tax=0.05m, Name = "Hazard" },
                new CityTax() { Tax=0.03m, Name = "Hebron Estates" },//bullitt
                new CityTax() { Tax=0.1m, Name = "Henderson", Min = 5 },//henderson
                new CityTax() { Tax=0.05m, Name = "Heritage Creek" },
                new CityTax() { Tax=0.03m, Name = "Hickman" },//fulton
                new CityTax() { Tax=0.05m, Name = "Hickory Hill" },
                new CityTax() { Tax=0.1m, Name = "Highland Heights" },//campbell
                new CityTax() { Tax=0.05m, Name = "Hills And Dales" },
                new CityTax() { Tax=0.07m, Name = "Hillview" },//bullitt
                new CityTax() { Tax=0.03m, Name = "Hindman" },
                new CityTax() { Tax=0.1m, Name = "Hodgenville" },
                new CityTax() { Tax=0.05m, Name = "Hollow Creek" },
                new CityTax() { Tax=0.075m, Name = "Hopkinsville" },
                new CityTax() { Tax=0.08m, Name = "Horse Cave" },
                new CityTax() { Tax=0.07m, Name = "Houston Acres" },
                new CityTax() { Tax=0.05m, Name = "Hunters Hollow" },//bullitt
                new CityTax() { Tax=0.08m, Name = "Hurstbourne" },
                new CityTax() { Tax=0.05m, Name = "Hurstbourne Acres" },
                new CityTax() { Tax=0.06m, Name = "Hustonville" },
                new CityTax() { Tax=0.04m, Name = "Hyden" },
                new CityTax() { Tax=0.05m, Name = "Indian Hills" },
                new CityTax() { Tax=0.1m, Name = "Irvine", Min=5 },
                new CityTax() { Tax=0.075m, Name = "Irvington" },//breckinridge
                new CityTax() { Tax=0.08m, Name = "Island" },
                new CityTax() { Tax=0.085m, Name = "Jackson" },
                new CityTax() { Tax=0.1m, Name = "Jamestown" },
                new CityTax() { Tax=0.05m, Name = "Jeffersontown" },
                new CityTax() { Tax=0, Name = "Jeffersonville", Min = 5 },
                new CityTax() { Tax=0.06m, Name = "Jenkins" },
                new CityTax() { Tax=0.12m, Name = "Junction City" },
                new CityTax() { Tax=0.05m, Name = "Kingsley" },
                new CityTax() { Tax=0, Name = "Kuttawa", Min=5 },
                new CityTax() { Tax=0.03m, Name = "La Center", Min=25 },
                new CityTax() { Tax=0.07m, Name = "Lafayette" },
                new CityTax() { Tax=0.1m, Name = "Lagrange" },
                new CityTax() { Tax=0.08m, Name = "Lakeside Park" },//kenton
                new CityTax() { Tax=0.1m, Name = "Lancaster" },
                new CityTax() { Tax=0.05m, Name = "Langdon Place" },
                new CityTax() { Tax=0.08m, Name = "Lawrenceburg" },//anderson
                new CityTax() { Tax=0.1m, Name = "Lebanon" },
                new CityTax() { Tax=0.05m, Name = "Lebanon Junction" },//bullitt
                new CityTax() { Tax=0.08m, Name = "Leitchfield", Min=5 },
                new CityTax() { Tax=0, Name = "Lewisburg", Min=5 },
                new CityTax() { Tax=0.05m, Name = "Lexington", Min=5 },
                new CityTax() { Tax=0.08m, Name = "Liberty" },//casey
                new CityTax() { Tax=0.05m, Name = "Lincolnshire" },
                new CityTax() { Tax=0.1m, Name = "Livermore" },
                new CityTax() { Tax=0.07m, Name = "Livingston" },//rockcastle
                new CityTax() { Tax=0.07m, Name = "London", Min=5 },
                new CityTax() { Tax=0.03m, Name = "Loretto", Min=5 },
                new CityTax() { Tax=0.08m, Name = "Louisa", Min=25 },
                new CityTax() { Tax=0.05m, Name = "Louisville" },
                new CityTax() { Tax=0.092m, Name = "Loyall" },
                new CityTax() { Tax=0.1m, Name = "Ludlow" },//kenton
                new CityTax() { Tax=0.08m, Name = "Lynch" },
                new CityTax() { Tax=0.05m, Name = "Lyndon" },
                new CityTax() { Tax=0.05m, Name = "Lynnview" },
                new CityTax() { Tax=0.1m, Name = "Madisonville" },//hopkins
                new CityTax() { Tax=0.4m, Name = "Manchester" },
                new CityTax() { Tax=0.05m, Name = "Manor Creek" },
                new CityTax() { Tax=0.4m, Name = "Marion" },//crittenden
                new CityTax() { Tax=0.09m, Name = "Martin" },
                new CityTax() { Tax=0.0575m, Name = "Maryhill Estates" },
                new CityTax() { Tax=0.09m, Name = "Mayfield", Min=5},
                new CityTax() { Tax=0.07m, Name = "Maysville" },//mason
                new CityTax() { Tax=0.05m, Name = "Mc Henry" },
                new CityTax() { Tax=0.1m, Name = "McKee" },//jackson
                new CityTax() { Tax=0.05m, Name = "Meadow Vale" },
                new CityTax() { Tax=0.05m, Name = "Meadowbrook Farm" },
                new CityTax() { Tax=0.05m, Name = "Meadowview Estates" },
                new CityTax() { Tax=0.1m, Name = "Melbourne", Min=5 },//campbell
                new CityTax() { Tax=0.1m, Name = "Mentor" },//campbell
                new CityTax() { Tax=0.03m, Name = "Middlesboro" },
                new CityTax() { Tax=0.05m, Name = "Middletown" },
                new CityTax() { Tax=0.05m, Name = "Midway" },
                new CityTax() { Tax=0.12m, Name = "Millersburg", Min=5 },
                new CityTax() { Tax=0.08m, Name = "Milton" },//trimble
                new CityTax() { Tax=0.05m, Name = "Mockingbird Valley" },
                new CityTax() { Tax=0.06m, Name = "Monticello", Min=5 },
                new CityTax() { Tax=0, Name = "Morehead" },
                new CityTax() { Tax=0.07m, Name = "Morganfield" },
                new CityTax() { Tax=0.08m, Name = "Morgantown" },
                new CityTax() { Tax=0.1m, Name = "Mortons Gap" },//hopkins
                new CityTax() { Tax=0.1m, Name = "Mount Sterling" },
                new CityTax() { Tax=0.4m, Name = "Mount Vernon"  },//rockcastle
                new CityTax() { Tax=0.045m, Name = "Mount Washington" },//bullitt
                new CityTax() { Tax=0.095m, Name = "Muldraugh", Min=5 },//meade
                new CityTax() { Tax=0.092m, Name = "Munfordville" },
                new CityTax() { Tax=0.095m, Name = "Murray" },
                new CityTax() { Tax=0.05m, Name = "Murray Hill" },
                new CityTax() { Tax=0.1m, Name = "Nebo" },//hopkins
                new CityTax() { Tax=0.12m, Name = "New Castle" },
                new CityTax() { Tax=0.08m, Name = "New Haven", Min=5 },
                new CityTax() { Tax=0.1m, Name = "Newport" },//campbell
                new CityTax() { Tax=0.09m, Name = "Nicholasville" },
                new CityTax() { Tax=0.05m, Name = "Norbourne Estates" },
                new CityTax() { Tax=0.09m, Name = "North Middletown" },
                new CityTax() { Tax=0.05m, Name = "Northfield" },
                new CityTax() { Tax=0.1m, Name = "Nortonville" },//hopkins
                new CityTax() { Tax=0.05m, Name = "Norwood" },
                new CityTax() { Tax=0.1m, Name = "Oak Grove" },
                new CityTax() { Tax=0.05m, Name = "Oakland" },
                new CityTax() { Tax=0.05m, Name = "Old Brownsboro Place" },
                new CityTax() { Tax=0.09m, Name = "Olive Hill", Min=5 },//carter
                new CityTax() { Tax=0.1m, Name = "Orchard Grass Hills" },//oldham
                new CityTax() { Tax=0.08m, Name = "Owensboro" },//daviess
                new CityTax() { Tax=0, Name = "Owingsville", Min=5 },
                new CityTax() { Tax=0.06m, Name = "Paducah" },
                new CityTax() { Tax=0, Name = "Paintsville" },
                new CityTax() { Tax=0.1m, Name = "Paris" },
                new CityTax() { Tax=0.08m, Name = "Park City" },
                new CityTax() { Tax=0.1m, Name = "Park Hills" },//kenton
                new CityTax() { Tax=0.05m, Name = "Parkway Village" },
                new CityTax() { Tax=0.055m, Name = "Pembroke" },
                new CityTax() { Tax=0, Name = "Perryville", Min=5 },
                new CityTax() { Tax=0.1m, Name = "Pewee Valley" },//oldham
                new CityTax() { Tax=0.08m, Name = "Pineville" },
                new CityTax() { Tax=0.05m, Name = "Pioneer Village" },//bullitt
                new CityTax() { Tax=0.05m, Name = "Plantation" },
                new CityTax() { Tax=0.1m, Name = "Pleasureville", Min=5 },
                new CityTax() { Tax=0.05m, Name = "Plum Springs" },
                new CityTax() { Tax=0.09m, Name = "Powderly" },
                new CityTax() { Tax=0.08m, Name = "Prestonsburg" },
                new CityTax() { Tax=0.06m, Name = "Prestonville" },
                new CityTax() { Tax=0.08m, Name = "Princeton" },
                new CityTax() { Tax=0.07m, Name = "Prospect" },
                new CityTax() { Tax=0.075m, Name = "Providence" },
                new CityTax() { Tax=0.12m, Name = "Raceland" },
                new CityTax() { Tax=0.11m, Name = "Radcliff" },
                new CityTax() { Tax=0.1m, Name = "Ravenna" },
                new CityTax() { Tax=0.05m, Name = "Richlawn" },
                new CityTax() { Tax=0.08m, Name = "Richmond" },
                new CityTax() { Tax=0.1m, Name = "River Bluff" },//oldham
                new CityTax() { Tax=0.05m, Name = "Riverwood" },
                new CityTax() { Tax=0.08m, Name = "Robards" },//henderson
                new CityTax() { Tax=0.06m, Name = "Rockport" },
                new CityTax() { Tax=0.05m, Name = "Rolling Fields" },
                new CityTax() { Tax=0.05m, Name = "Rolling Hills" },
                new CityTax() { Tax=0.1m, Name = "Russell Springs" },
                new CityTax() { Tax=0.07m, Name = "Russellville" },
                new CityTax() { Tax=0.07m, Name = "Ryland Heights" },//kenton
                new CityTax() { Tax=0.08m, Name = "Sacramento" },
                new CityTax() { Tax=0.1m, Name = "Sadieville", Min=5 },
                new CityTax() { Tax=0, Name = "Salyersville" },
                new CityTax() { Tax=0.09m, Name = "Sandy Hook" },//elliott
                new CityTax() { Tax=0.05m, Name = "Science Hill" },//pulaski
                new CityTax() { Tax=0.06m, Name = "Scottsville" },
                new CityTax() { Tax=0.07m, Name = "Sebree", Min=5 },
                new CityTax() { Tax=0.05m, Name = "Seneca Gardens" },
                new CityTax() { Tax=0.05m, Name = "Shelbyville" },
                new CityTax() { Tax=0.05m, Name = "Shepherdsville" },//bullitt
                new CityTax() { Tax=0.1m, Name = "Shively" },
                new CityTax() { Tax=0.1m, Name = "Silver Grove" },//campbell
                new CityTax() { Tax=0.05m, Name = "Simpsonville" },
                new CityTax() { Tax=0.05m, Name = "Slaughters" },
                new CityTax() { Tax=0.065m, Name = "Smithfield" },
                new CityTax() { Tax=0, Name = "Smithland" },
                new CityTax() { Tax=0.07m, Name = "Smiths Grove" },
                new CityTax() { Tax=0.06m, Name = "Somerset", Min=5 },//pulaski
                new CityTax() { Tax=0.06m, Name = "South Shore" },
                new CityTax() { Tax=0.1m, Name = "Southgate" },//campbell
                new CityTax() { Tax=0.06m, Name = "Sparta" },
                new CityTax() { Tax=0.05m, Name = "Spring Valley" },
                new CityTax() { Tax=0.06m, Name = "Springfield" },//washington
                new CityTax() { Tax=0.1m, Name = "St. Charles", Min=25 },//hopkins
                new CityTax() { Tax=0.05m, Name = "St. Matthews" },
                new CityTax() { Tax=0.05m, Name = "St. Regis Park" },
                new CityTax() { Tax=0.06m, Name = "Stamping Ground" },
                new CityTax() { Tax=0.09m, Name = "Stanford" },
                new CityTax() { Tax=0.05m, Name = "Stanton" },//powell
                new CityTax() { Tax=0.07m, Name = "Strathmoor Manor" },
                new CityTax() { Tax=0.05m, Name = "Strathmoor Village" },
                new CityTax() { Tax=0.12m, Name = "Sturgis" },
                new CityTax() { Tax=0.08m, Name = "Sycamore" },
                new CityTax() { Tax=0.08m, Name = "Taylor Mill" },//kenton
                new CityTax() { Tax=0.07m, Name = "Taylorsville", Min=5 },//spencer
                new CityTax() { Tax=0, Name = "Ten Broeck" },
                new CityTax() { Tax=0.05m, Name = "Thornhill" },
                new CityTax() { Tax=0.05m, Name = "Tompkinsville" },
                new CityTax() { Tax=0.06m, Name = "Trenton" },//todd
                new CityTax() { Tax=0, Name = "Uniontown", Min=5 },
                new CityTax() { Tax=0.06m, Name = "Vanceburg" },//lewis 
                new CityTax() { Tax=0.09m, Name = "Versailles" },
                new CityTax() { Tax=0.1m, Name = "Vicco" },
                new CityTax() { Tax=0.05m, Name = "Villa Hills" },
                new CityTax() { Tax=0.1m, Name = "Vine Grove" },
                new CityTax() { Tax=0.05m, Name = "Walton" },
                new CityTax() { Tax=0, Name = "Warsaw" },
                new CityTax() { Tax=0.05m, Name = "Watterson Park" },
                new CityTax() { Tax=0.08m, Name = "Wayland" },
                new CityTax() { Tax=0.05m, Name = "Wellington" },
                new CityTax() { Tax=0.1m, Name = "West Buechel" },
                new CityTax() { Tax=0.1m, Name = "West liberty" },
                new CityTax() { Tax=0.12m, Name = "West Point" },
                new CityTax() { Tax=0.06m, Name = "Westwood" },
                new CityTax() { Tax=0.08m, Name = "Wheatcroft" },
                new CityTax() { Tax=0.05m, Name = "Wheelwright" },
                new CityTax() { Tax=0.1m, Name = "White Plains", Min=25 },
                new CityTax() { Tax=0.05m, Name = "Whitesburg", Min=5 },
                new CityTax() { Tax=0, Name = "Whitesville" },
                new CityTax() { Tax=0.064m, Name = "Wickliffe" },
                new CityTax() { Tax=0.1m, Name = "Wilder" },
                new CityTax() { Tax=0.05m, Name = "Wildwood" },
                new CityTax() { Tax=0.06m, Name = "Williamsburg" },
                new CityTax() { Tax=0.0675m, Name = "Williamstown" },
                new CityTax() { Tax=0.07m, Name = "Wilmore" },
                new CityTax() { Tax=0.08m, Name = "Winchester" },
                new CityTax() { Tax=0.05m, Name = "Windy Hills" },
                new CityTax() { Tax=0.05m, Name = "Wingo" },
                new CityTax() { Tax=0.07m, Name = "Woodburn" },
                new CityTax() { Tax=0.05m, Name = "Woodbury" },
                new CityTax() { Tax=0.08m, Name = "Woodland Hills" },
                new CityTax() { Tax=0.1m, Name = "Woodlawn" },
                new CityTax() { Tax=0.05m, Name = "Woodlawn Park" },
                new CityTax() { Tax=0.08m, Name = "Worthington" },
                new CityTax() { Tax=0.06m, Name = "Worthington Hills" },
                new CityTax() { Tax=0.05m, Name = "Worthville" },
                new CityTax() { Tax=0.08m, Name = "Wurtland" },
            };

            #endregion

            #region Taxed by Counties

            #region Counties

            CountyTax crittenden = new CountyTax() { Name = "Crittenden County", Code = 1007, Tax = 0.02m };
            CountyTax fulton = new CountyTax() { Name = "Fulton County", Code = 0887, Tax = 0.03m, Min = 10 };
            CountyTax kenton = new CountyTax() { Name = "Kenton County", Code = 1000, Tax = 0.08m, Min = 5 };
            CountyTax lewis = new CountyTax() { Name = "Lewis County", Code = 0917, Tax = 0.06m };
            CountyTax mason = new CountyTax() { Name = "Mason County", Code = 0930, Tax = 0.07m };
            CountyTax owen = new CountyTax() { Name = "Owen County", Code = 0943, Tax = 0.075m };
            CountyTax owsley = new CountyTax() { Name = "Owsley County", Code = 0944, Tax = 0.07m };
            CountyTax oldham = new CountyTax() { Name = "Oldham County", Code = 0942, Tax = 0.1m };
            CountyTax pulaski = new CountyTax() { Name = "Pulaski County", Code = 0949, Tax = 0.055m, Min = 5 };
            CountyTax robertson = new CountyTax() { Name = "Robertson County", Code = 1024, Tax = 0.05m };
            CountyTax todd = new CountyTax() { Name = "Todd County", Code = 1001, Tax = 0.06m };
            CountyTax washington = new CountyTax() { Name = "Washington County", Code = 0964, Tax = 0.04m, Min = 10 };

            #region useless
            CountyTax anderson = new CountyTax() { Name = "Anderson County", Code = 0852, Tax = 0.04m };//useless
            CountyTax breckinridge = new CountyTax() { Name = "Breckinridge County", Code = 0863, Tax = 0.05m };//useless
            CountyTax bullitt = new CountyTax() { Name = "Bullitt County", Code = 0864, Tax = 0.05m };//useless
            CountyTax campbell = new CountyTax() { Name = "Campbell County", Code = 0868, Tax = 0.1m };//useless
            CountyTax carter = new CountyTax() { Name = "Carter County", Code = 0870, Tax = 0.09m };//useless
            CountyTax casey = new CountyTax() { Name = "Casey County", Code = 0872, Tax = 0.06m };//useless
            CountyTax clark = new CountyTax() { Name = "Clark County", Code = 0994, Tax = 0.04m };//useless
            CountyTax daviess = new CountyTax() { Name = "Daviess County", Code = 0879, Tax = 0 };//useless
            CountyTax elliott = new CountyTax() { Name = "Elliott County", Code = 0999, Tax = 0.1m };//useless
            CountyTax fleming = new CountyTax() { Name = "Fleming County", Code = 1005, Tax = 0.06m };//useless
            CountyTax franklin = new CountyTax() { Name = "Franklin County", Code = 0886, Tax = 0.06m };//useless
            CountyTax green = new CountyTax() { Name = "Green County", Code = 1025, Tax = 0.03m };//useless
            CountyTax henderson = new CountyTax() { Name = "Henderson County", Code = 0900, Tax = 0.975m };//useless
            CountyTax hopkins = new CountyTax() { Name = "Hopkins County", Code = 0903, Tax = 0.1m };//useless
            CountyTax jackson = new CountyTax() { Name = "Jackson County", Code = 0904, Tax = 0.05m };//useless
            CountyTax meade = new CountyTax() { Name = "Meade County", Code = 0931, Tax = 0.05m };//useless
            CountyTax menifee = new CountyTax() { Name = "Menifee County", Code = 0932, Tax = 0.06m };//useless
            CountyTax morgon = new CountyTax() { Name = "Morgon County", Code = 0937, Tax = 0.045m };//useless
            CountyTax powell = new CountyTax() { Name = "Powell County", Code = 1013, Tax = 0.05m };//useless
            CountyTax rockcastle = new CountyTax() { Name = "Rockcastle County", Code = 0951, Tax = 0.04m };//useless
            CountyTax spencer = new CountyTax() { Name = "Spencer County", Code = 1006, Tax = 0.03m };//useless
            CountyTax trigg = new CountyTax() { Name = "Trigg County", Code = 0960, Tax = 0.06m };//useless
            CountyTax trimble = new CountyTax() { Name = "Trimble County", Code = 1023, Tax = 0.1m, Min = 5 };//useless
            CountyTax wayne = new CountyTax() { Name = "Wayne County", Code = 0965, Tax = 0.05m };//useless
            #endregion

            #endregion

            List<CityTax> citiesTaxedByCounties = new List<CityTax>()
            {
                new CityTax() { Name = "Dycusburg", County = crittenden },//Crittenden County
                new CityTax() { Name = "Fulton", County = fulton },//Fulton County
                new CityTax() { Name = "Crestview Hills", County = kenton },//Kenton County
                new CityTax() { Name = "Edgewood", County = kenton },//Kenton County
                new CityTax() { Name = "Fairview", County = kenton },//Kenton County
                new CityTax() { Name = "Independence", County = kenton },//Kenton County
                new CityTax() { Name = "Kenton Vale", County = kenton },//Kenton County
                new CityTax() { Name = "Concord", County = lewis },//Lewis County
                new CityTax() { Name = "Sardis", County = mason },//Mason Edgewood and Robertson County      ?
                new CityTax() { Name = "Germantown", County = mason },//Mason Edgewood and Bracken  County      ?
                new CityTax() { Name = "La Grange", County = oldham },//Oldham County
                new CityTax() { Name = "Gratz", County = owen },//Owen County
                new CityTax() { Name = "Monterey", County = owen },//Owen County
                new CityTax() { Name = "Owenton", County = owen },//Owen County
                new CityTax() { Name = "Booneville", County = owsley },//Owsley County
                new CityTax() { Name = "Ferguson", County = pulaski },//Pulaski County
                new CityTax() { Name = "Eubank", County = pulaski },//Pulaski County
                new CityTax() { Name = "Mount Olivet", County = robertson },//Robertson County
                new CityTax() { Name = "Allensville", County = todd },//Todd County
                new CityTax() { Name = "Mackville", County = washington },//Washington County
                new CityTax() { Name = "Willisburg", County = washington },//Washington County
            };
            #endregion

            var citiesTaxed = cityTaxedByCity.Concat(citiesTaxedByCounties);
            var setCity = DbContext.Set<City>();
            long? firstIdKentucky = setCity.FirstOrDefault(c => c.StateId == BaseSeed.kentuckyId)?.Id;
            long cityCount = firstIdKentucky ?? setCity.Max(c => c.Id) + 1;
            foreach (var city in citiesTaxed)
            {
                city.CityId = cityCount++;
            }
            return citiesTaxed;
        }
        private CountySurcharge CreateCountySurcharge(long id, string code, string description, decimal minTax, decimal percentage)
        {
            return new CountySurcharge()
            {
                Id = id,
                Code = code,
                Description = description,
                Type = SurchargeType.Tax,
                MinTax = minTax,
                Cities = new List<City>(),
                Percentages = new List<SurchargePercentage>()
                {
                    new SurchargePercentage()
                    {
                        Percentage = percentage
                    }
                }
            };
        }
    }

    public class CityTax : CountyTax
    {
        public long CityId { get; set; }
        public CountyTax County { get; set; }
    }

    public class CountyTax
    {
        public string Name { get; set; }
        public int Code { get; set; }
        public decimal Tax { get; set; }
        public decimal Min { get; set; }
    }
}
