namespace ProtectorUS.Data.Migrations
{
    using Model;
    using System;
    using System.Data.Entity.Migrations;

    public partial class create_unique_softdeletable : DbMigration
    {
        public override void Up()
        {
            UniqueKeySoftDeletable.CreateIndexes((sql) => Sql(sql));
        }
        
        public override void Down()
        {
        }
    }
}
