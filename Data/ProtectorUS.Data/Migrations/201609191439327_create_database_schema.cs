namespace ProtectorUS.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class create_database_schema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StreetLine1 = c.String(),
                        StreetLine2 = c.String(),
                        RegionId = c.Long(nullable: false),
                        Latitude = c.Double(),
                        Longitude = c.Double(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ZipCode = c.String(nullable: false, maxLength: 20),
                        CityId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId)
                .Index(t => new { t.ZipCode, t.CityId }, unique: true, name: "unique_ZipCode_CityId_index");
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                        StateId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => new { t.Name, t.StateId }, unique: true, name: "unique_Name_StateId_index");
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                        Abbreviation = c.String(nullable: false, maxLength: 400),
                        CountryId = c.Long(nullable: false),
                        CarrierId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carrier", t => t.CarrierId)
                .ForeignKey("dbo.Country", t => t.CountryId)
                .Index(t => t.Name, unique: true, name: "unique_Name_index")
                .Index(t => t.Abbreviation, unique: true, name: "unique_Abbreviation_index")
                .Index(t => t.CountryId)
                .Index(t => t.CarrierId);
            
            CreateTable(
                "dbo.Carrier",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        AddressId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                        Abbreviation = c.String(nullable: false, maxLength: 400),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "unique_Name_index")
                .Index(t => t.Abbreviation, unique: true, name: "unique_Abbreviation_index");
            
            CreateTable(
                "dbo.Brokerage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Alias = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 50),
                        Email = c.String(nullable: false),
                        Cover = c.String(),
                        Status = c.Int(nullable: false),
                        ChargeAccountStatus = c.Int(nullable: false),
                        Website = c.String(),
                        BrokerageGroupId = c.Long(),
                        AddressId = c.Long(nullable: false),
                        ChargeAccountId = c.String(),
                        AgentNumber = c.String(),
                        Logo = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId)
                .ForeignKey("dbo.BrokerageGroup", t => t.BrokerageGroupId)
                .Index(t => t.BrokerageGroupId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.BrokerageGroup",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "unique_Name_index");
            
            CreateTable(
                "dbo.BrokerageProduct",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ActiveARGO = c.Boolean(nullable: false),
                        ActiveBrokerage = c.Boolean(nullable: false),
                        ProductId = c.Long(nullable: false),
                        BrokerageId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .ForeignKey("dbo.Brokerage", t => t.BrokerageId)
                .Index(t => t.ProductId)
                .Index(t => t.BrokerageId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Slogan = c.String(nullable: false, maxLength: 50),
                        Code = c.String(nullable: false),
                        Alias = c.String(nullable: false, maxLength: 30),
                        Abbreviation = c.String(nullable: false, maxLength: 5),
                        Icon = c.String(),
                        Active = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Declined",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ContractType = c.Int(nullable: false),
                        InsuredId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.InsuredId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.InsuredId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.DeclinedBlock",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BlockReason = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ManagerUserId = c.Long(),
                        DeclinedId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Declined", t => t.DeclinedId)
                .ForeignKey("dbo.Person", t => t.ManagerUserId)
                .Index(t => t.ManagerUserId)
                .Index(t => t.DeclinedId);
            
            CreateTable(
                "dbo.DeclinedRule",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        ContractType = c.Int(nullable: false),
                        Reason = c.String(nullable: false),
                        ProductId = c.Long(nullable: false),
                        PropetyName = c.String(nullable: false),
                        Operator = c.String(nullable: false),
                        TargetValue = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.DeclinedUnblock",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UnblockReason = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ManagerUserId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeclinedBlock", t => t.Id)
                .ForeignKey("dbo.Person", t => t.ManagerUserId)
                .Index(t => t.Id)
                .Index(t => t.ManagerUserId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 250),
                        Picture = c.String(),
                        BirthDate = c.DateTime(),
                        SocialTitle = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Status = c.Int(),
                        Password = c.String(),
                        FacebookId = c.String(),
                        LinkedinId = c.String(),
                        CompanyId = c.Long(),
                        BrokerageId = c.Long(),
                        AcceptedTerms = c.Boolean(),
                        PersonType = c.String(maxLength: 128),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId)
                .ForeignKey("dbo.Brokerage", t => t.BrokerageId)
                .Index(t => t.CompanyId)
                .Index(t => t.BrokerageId);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DeviceId = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        UserId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ExceptedPermission",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        Allowed = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => new { t.Id, t.UserId })
                .ForeignKey("dbo.Permission", t => t.Id, cascadeDelete: true)
                .ForeignKey("dbo.Person", t => t.UserId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ActionApi = c.String(nullable: false, maxLength: 400),
                        Order = c.Decimal(precision: 18, scale: 2),
                        GroupId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PermissionGroup", t => t.GroupId)
                .Index(t => new { t.ActionApi, t.GroupId }, unique: true, name: "unique_ActionApi_GroupId_index");
            
            CreateTable(
                "dbo.PermissionGroup",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                        Description = c.String(),
                        Order = c.Long(nullable: false),
                        GroupId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PermissionGroup", t => t.GroupId)
                .Index(t => t.Name, unique: true, name: "unique_Name_index")
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.Phone",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Profile",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Campaign",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        WebpageProductId = c.Long(nullable: false),
                        BrokerId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        FilePath = c.String(),
                        TemplateType = c.Int(),
                        CoverPath = c.String(),
                        CoverId = c.Int(),
                        Title = c.String(),
                        Body = c.String(),
                        ButtonText = c.String(),
                        CampaignType = c.String(maxLength: 128),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.BrokerId)
                .ForeignKey("dbo.WebpageProduct", t => t.WebpageProductId)
                .Index(t => t.WebpageProductId)
                .Index(t => t.BrokerId);
            
            CreateTable(
                "dbo.WebpageProduct",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Active = c.Boolean(nullable: false),
                        Alias = c.String(nullable: false, maxLength: 35),
                        Logo = c.String(),
                        Cover = c.String(),
                        Default = c.Boolean(nullable: false),
                        BrokerageComission = c.Decimal(nullable: false, precision: 9, scale: 6),
                        Description = c.String(),
                        BrokerageProductId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BrokerageProduct", t => t.BrokerageProductId)
                .Index(t => new { t.Alias, t.BrokerageProductId }, unique: true, name: "unique_Alias_BrokerageProductId_index");
            
            CreateTable(
                "dbo.WebpageView",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Access = c.Int(nullable: false),
                        CampaignId = c.Long(),
                        BrokerageId = c.Long(nullable: false),
                        WebpageProductId = c.Long(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brokerage", t => t.BrokerageId)
                .ForeignKey("dbo.Campaign", t => t.CampaignId)
                .ForeignKey("dbo.WebpageProduct", t => t.WebpageProductId)
                .Index(t => t.CampaignId)
                .Index(t => t.BrokerageId)
                .Index(t => t.WebpageProductId);
            
            CreateTable(
                "dbo.Policy",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Number = c.String(nullable: false, maxLength: 20),
                        SequentialNumber = c.Int(nullable: false),
                        PolicyPath = c.String(),
                        CertificatePath = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                        PeriodStart = c.DateTime(nullable: false),
                        PeriodEnd = c.DateTime(nullable: false),
                        ConditionsId = c.Long(nullable: false),
                        InsuredId = c.Long(nullable: false),
                        BrokerId = c.Long(nullable: false),
                        Canceled = c.Boolean(nullable: false),
                        BusinessId = c.Long(),
                        InsuredTimezone = c.Int(nullable: false),
                        CoverageEachClaim = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CoverageAggregate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeductibleEachClaim = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeductibleAggregate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPremium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.BrokerId)
                .ForeignKey("dbo.Business", t => t.BusinessId)
                .ForeignKey("dbo.ProductCondition", t => t.ConditionsId)
                .ForeignKey("dbo.Person", t => t.InsuredId)
                .ForeignKey("dbo.Order", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.ConditionsId)
                .Index(t => t.InsuredId)
                .Index(t => t.BrokerId)
                .Index(t => t.BusinessId);
            
            CreateTable(
                "dbo.Business",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                        AddressId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Claim",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 20),
                        SequentialNumber = c.Int(nullable: false),
                        OccurrenceDate = c.DateTime(nullable: false),
                        ClaimDate = c.DateTime(nullable: false),
                        PriorNotification = c.Boolean(nullable: false),
                        DescriptionFacts = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        LossReserve = c.Decimal(precision: 18, scale: 2),
                        ALAEReserve = c.Decimal(precision: 18, scale: 2),
                        TotalPaid = c.Decimal(precision: 18, scale: 2),
                        PolicyId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .Index(t => t.PolicyId);
            
            CreateTable(
                "dbo.ClaimDocument",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Path = c.String(),
                        ClaimId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Claim", t => t.ClaimId)
                .Index(t => t.ClaimId);
            
            CreateTable(
                "dbo.ThirdParty",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        ClaimId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Claim", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.ProductCondition",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        StateId = c.Long(nullable: false),
                        Description = c.String(),
                        FilePath = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.ProductId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.Coverage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Key = c.String(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 2000),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Endorsement",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PolicyId = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        ReasonId = c.Long(nullable: false),
                        RequesterId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .ForeignKey("dbo.Reason", t => t.ReasonId)
                .ForeignKey("dbo.Person", t => t.RequesterId)
                .Index(t => t.PolicyId)
                .Index(t => t.ReasonId)
                .Index(t => t.RequesterId);
            
            CreateTable(
                "dbo.Reason",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        ReasonType = c.String(maxLength: 128),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TransactionCode = c.String(nullable: false),
                        PaymentStatus = c.Int(nullable: false),
                        TotalInstallments = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        QuotationId = c.Long(nullable: false),
                        PaymentMethodId = c.Long(nullable: false),
                        ArgoFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BrokerageFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BrokerageFeePercentage = c.Decimal(nullable: false, precision: 9, scale: 6),
                        Rounding = c.Decimal(nullable: false, precision: 5, scale: 4),
                        TotalCharged = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SurchargeAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StripeFee = c.Decimal(precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentMethod", t => t.PaymentMethodId)
                .ForeignKey("dbo.Quotation", t => t.QuotationId)
                .Index(t => t.QuotationId)
                .Index(t => t.PaymentMethodId);
            
            CreateTable(
                "dbo.PaymentMethod",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Icon = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Quotation",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Hash = c.Guid(nullable: false),
                        PolicyPremium = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FeeAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeclined = c.Boolean(nullable: false),
                        PolicyStartDate = c.DateTime(nullable: false),
                        Rounding = c.Decimal(nullable: false, precision: 5, scale: 4),
                        Revenue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        ProponentSocialTitle = c.Int(),
                        ProponentFirstName = c.String(),
                        ProponentLastName = c.String(),
                        ProponentEmail = c.String(),
                        ExpirationDate = c.DateTime(nullable: false),
                        RatingPlanFormVersionId = c.Long(nullable: false),
                        BrokerId = c.Long(nullable: false),
                        InsuredId = c.Long(),
                        RegionId = c.Long(nullable: false),
                        CampaignId = c.Long(),
                        BusinessId = c.Long(),
                        InsuredTimezone = c.Int(nullable: false),
                        BrokerageProductId = c.Long(nullable: false),
                        RiskAnalysisJson = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.BrokerId)
                .ForeignKey("dbo.BrokerageProduct", t => t.BrokerageProductId)
                .ForeignKey("dbo.Business", t => t.BusinessId)
                .ForeignKey("dbo.Campaign", t => t.CampaignId)
                .ForeignKey("dbo.Person", t => t.InsuredId)
                .ForeignKey("dbo.RatingPlanFormVersion", t => t.RatingPlanFormVersionId)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .Index(t => t.RatingPlanFormVersionId)
                .Index(t => t.BrokerId)
                .Index(t => t.InsuredId)
                .Index(t => t.RegionId)
                .Index(t => t.CampaignId)
                .Index(t => t.BusinessId)
                .Index(t => t.BrokerageProductId);
            
            CreateTable(
                "dbo.DeclineReason",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuotationId = c.Long(nullable: false),
                        QuestionFieldName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Quotation", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.QuotationDelivery",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuotationId = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Email = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Quotation", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.RatingPlanFormVersion",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Version = c.Int(nullable: false),
                        RatingPlanFormId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RatingPlanForm", t => t.RatingPlanFormId)
                .Index(t => new { t.RatingPlanFormId, t.Version }, unique: true, name: "unique_RatingPlanFormId_Version_index");
            
            CreateTable(
                "dbo.RatingPlanForm",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StateId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => new { t.StateId, t.ProductId }, unique: true, name: "unique_StateId_ProductId_index");
            
            CreateTable(
                "dbo.SurchargePercentage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Percentage = c.Decimal(nullable: false, precision: 9, scale: 6),
                        SurchargeId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Surcharge", t => t.SurchargeId)
                .Index(t => t.SurchargeId);
            
            CreateTable(
                "dbo.Surcharge",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 400),
                        Description = c.String(),
                        MinTax = c.Decimal(precision: 18, scale: 2),
                        Type = c.Int(nullable: false),
                        StateId = c.Long(),
                        SurchargeType = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.Code, unique: true, name: "unique_Code_index")
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.ProductPaymentMethod",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        PaymentMethodId = c.Long(nullable: false),
                        TotalInstallments = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentMethod", t => t.PaymentMethodId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.PaymentMethodId);
            
            CreateTable(
                "dbo.AppClient",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientId = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        RefreshTokenLifeTime = c.Int(nullable: false),
                        AllowedOrigin = c.String(nullable: false),
                        CurrentVersion = c.String(nullable: false),
                        ReviewVersion = c.String(nullable: false),
                        Token = c.String(nullable: false),
                        InReview = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RefreshToken",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RefreshTokenId = c.String(nullable: false),
                        IdentityName = c.String(nullable: false),
                        AppClientId = c.String(nullable: false),
                        IssuedDate = c.DateTime(),
                        ExpiresDate = c.DateTime(),
                        ProtectedTicket = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Message = c.String(nullable: false),
                        ActorId = c.Long(),
                        ActorType = c.Int(),
                        SourceId = c.Long(),
                        SourceType = c.Int(),
                        DestinationId = c.Long(),
                        DestinationType = c.Int(),
                        Type = c.Int(),
                        ActivityType = c.Int(),
                        EventType = c.Int(),
                        AppType = c.Int(),
                        Sent = c.Boolean(nullable: false),
                        SendDate = c.DateTime(),
                        Read = c.Boolean(nullable: false),
                        ReadDate = c.DateTime(),
                        Answered = c.Boolean(),
                        ExpiresOn = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.__SeedHistory",
                c => new
                    {
                        SeedId = c.String(nullable: false, maxLength: 128),
                        ContextKey = c.String(nullable: false),
                        RunDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SeedId);
            
            CreateTable(
                "dbo.NewsLetter",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 150),
                        Origin = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailTemplate",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TemplateId = c.String(nullable: false, maxLength: 400),
                        Name = c.String(),
                        From = c.String(nullable: false),
                        FromName = c.String(nullable: false),
                        Subject = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TemplateId, unique: true, name: "unique_TemplateId_index");
            
            CreateTable(
                "dbo.AcordTransaction",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PolicyId = c.Long(nullable: false),
                        Xml = c.String(),
                        Token = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Policy", t => t.PolicyId)
                .Index(t => t.PolicyId);
            
            CreateTable(
                "dbo.DeclinedBlockRule",
                c => new
                    {
                        DeclinedBlockId = c.Long(nullable: false),
                        DeclinedRuleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.DeclinedBlockId, t.DeclinedRuleId })
                .ForeignKey("dbo.DeclinedBlock", t => t.DeclinedBlockId, cascadeDelete: true)
                .ForeignKey("dbo.DeclinedRule", t => t.DeclinedRuleId, cascadeDelete: true)
                .Index(t => t.DeclinedBlockId)
                .Index(t => t.DeclinedRuleId);
            
            CreateTable(
                "dbo.ProfilePermission",
                c => new
                    {
                        ProfileId = c.Long(nullable: false),
                        PermissionId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProfileId, t.PermissionId })
                .ForeignKey("dbo.Profile", t => t.ProfileId, cascadeDelete: true)
                .ForeignKey("dbo.Permission", t => t.PermissionId, cascadeDelete: true)
                .Index(t => t.ProfileId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Long(nullable: false),
                        ProfileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ProfileId })
                .ForeignKey("dbo.Person", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Profile", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.PolicyCoverage",
                c => new
                    {
                        PolicyId = c.Long(nullable: false),
                        CoverageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.PolicyId, t.CoverageId })
                .ForeignKey("dbo.Policy", t => t.PolicyId, cascadeDelete: true)
                .ForeignKey("dbo.Coverage", t => t.CoverageId, cascadeDelete: true)
                .Index(t => t.PolicyId)
                .Index(t => t.CoverageId);
            
            CreateTable(
                "dbo.SurchargeCity",
                c => new
                    {
                        CountySurchargeId = c.Long(nullable: false),
                        CityId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.CountySurchargeId, t.CityId })
                .ForeignKey("dbo.Surcharge", t => t.CountySurchargeId, cascadeDelete: true)
                .ForeignKey("dbo.City", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CountySurchargeId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.QuotationSurcharge",
                c => new
                    {
                        QuotationId = c.Long(nullable: false),
                        SurchargePercentageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuotationId, t.SurchargePercentageId })
                .ForeignKey("dbo.Quotation", t => t.QuotationId, cascadeDelete: true)
                .ForeignKey("dbo.SurchargePercentage", t => t.SurchargePercentageId, cascadeDelete: true)
                .Index(t => t.QuotationId)
                .Index(t => t.SurchargePercentageId);
            
            CreateTable(
                "dbo.BrokeragePhone",
                c => new
                    {
                        BrokerageId = c.Long(nullable: false),
                        PhoneId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BrokerageId, t.PhoneId })
                .ForeignKey("dbo.Brokerage", t => t.BrokerageId, cascadeDelete: true)
                .ForeignKey("dbo.Phone", t => t.PhoneId, cascadeDelete: true)
                .Index(t => t.BrokerageId)
                .Index(t => t.PhoneId);
            
            CreateTable(
                "dbo.PersonAddress",
                c => new
                    {
                        PersonId = c.Long(nullable: false),
                        AddressId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.PersonId, t.AddressId })
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.Address", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.PersonPhone",
                c => new
                    {
                        PersonId = c.Long(nullable: false),
                        PhoneId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.PersonId, t.PhoneId })
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.Phone", t => t.PhoneId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.PhoneId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AcordTransaction", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.PersonPhone", "PhoneId", "dbo.Phone");
            DropForeignKey("dbo.PersonPhone", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonAddress", "AddressId", "dbo.Address");
            DropForeignKey("dbo.PersonAddress", "PersonId", "dbo.Person");
            DropForeignKey("dbo.BrokeragePhone", "PhoneId", "dbo.Phone");
            DropForeignKey("dbo.BrokeragePhone", "BrokerageId", "dbo.Brokerage");
            DropForeignKey("dbo.BrokerageProduct", "BrokerageId", "dbo.Brokerage");
            DropForeignKey("dbo.WebpageProduct", "BrokerageProductId", "dbo.BrokerageProduct");
            DropForeignKey("dbo.BrokerageProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ProductPaymentMethod", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ProductPaymentMethod", "PaymentMethodId", "dbo.PaymentMethod");
            DropForeignKey("dbo.Declined", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Declined", "InsuredId", "dbo.Person");
            DropForeignKey("dbo.DeclinedBlock", "ManagerUserId", "dbo.Person");
            DropForeignKey("dbo.DeclinedUnblock", "ManagerUserId", "dbo.Person");
            DropForeignKey("dbo.Policy", "Id", "dbo.Order");
            DropForeignKey("dbo.Order", "QuotationId", "dbo.Quotation");
            DropForeignKey("dbo.QuotationSurcharge", "SurchargePercentageId", "dbo.SurchargePercentage");
            DropForeignKey("dbo.QuotationSurcharge", "QuotationId", "dbo.Quotation");
            DropForeignKey("dbo.Surcharge", "StateId", "dbo.State");
            DropForeignKey("dbo.SurchargeCity", "CityId", "dbo.City");
            DropForeignKey("dbo.SurchargeCity", "CountySurchargeId", "dbo.Surcharge");
            DropForeignKey("dbo.SurchargePercentage", "SurchargeId", "dbo.Surcharge");
            DropForeignKey("dbo.Quotation", "RegionId", "dbo.Region");
            DropForeignKey("dbo.Quotation", "RatingPlanFormVersionId", "dbo.RatingPlanFormVersion");
            DropForeignKey("dbo.RatingPlanForm", "StateId", "dbo.State");
            DropForeignKey("dbo.RatingPlanFormVersion", "RatingPlanFormId", "dbo.RatingPlanForm");
            DropForeignKey("dbo.RatingPlanForm", "ProductId", "dbo.Product");
            DropForeignKey("dbo.QuotationDelivery", "QuotationId", "dbo.Quotation");
            DropForeignKey("dbo.Quotation", "InsuredId", "dbo.Person");
            DropForeignKey("dbo.DeclineReason", "QuotationId", "dbo.Quotation");
            DropForeignKey("dbo.Quotation", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.Quotation", "BusinessId", "dbo.Business");
            DropForeignKey("dbo.Quotation", "BrokerageProductId", "dbo.BrokerageProduct");
            DropForeignKey("dbo.Quotation", "BrokerId", "dbo.Person");
            DropForeignKey("dbo.Order", "PaymentMethodId", "dbo.PaymentMethod");
            DropForeignKey("dbo.Policy", "InsuredId", "dbo.Person");
            DropForeignKey("dbo.Endorsement", "RequesterId", "dbo.Person");
            DropForeignKey("dbo.Endorsement", "ReasonId", "dbo.Reason");
            DropForeignKey("dbo.Endorsement", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.PolicyCoverage", "CoverageId", "dbo.Coverage");
            DropForeignKey("dbo.PolicyCoverage", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.Coverage", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Policy", "ConditionsId", "dbo.ProductCondition");
            DropForeignKey("dbo.ProductCondition", "StateId", "dbo.State");
            DropForeignKey("dbo.ProductCondition", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ThirdParty", "Id", "dbo.Claim");
            DropForeignKey("dbo.Claim", "PolicyId", "dbo.Policy");
            DropForeignKey("dbo.ClaimDocument", "ClaimId", "dbo.Claim");
            DropForeignKey("dbo.Policy", "BusinessId", "dbo.Business");
            DropForeignKey("dbo.Business", "AddressId", "dbo.Address");
            DropForeignKey("dbo.Policy", "BrokerId", "dbo.Person");
            DropForeignKey("dbo.Campaign", "WebpageProductId", "dbo.WebpageProduct");
            DropForeignKey("dbo.WebpageView", "WebpageProductId", "dbo.WebpageProduct");
            DropForeignKey("dbo.WebpageView", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.WebpageView", "BrokerageId", "dbo.Brokerage");
            DropForeignKey("dbo.Campaign", "BrokerId", "dbo.Person");
            DropForeignKey("dbo.Person", "BrokerageId", "dbo.Brokerage");
            DropForeignKey("dbo.UserProfile", "ProfileId", "dbo.Profile");
            DropForeignKey("dbo.UserProfile", "UserId", "dbo.Person");
            DropForeignKey("dbo.ProfilePermission", "PermissionId", "dbo.Permission");
            DropForeignKey("dbo.ProfilePermission", "ProfileId", "dbo.Profile");
            DropForeignKey("dbo.ExceptedPermission", "UserId", "dbo.Person");
            DropForeignKey("dbo.ExceptedPermission", "Id", "dbo.Permission");
            DropForeignKey("dbo.Permission", "GroupId", "dbo.PermissionGroup");
            DropForeignKey("dbo.PermissionGroup", "GroupId", "dbo.PermissionGroup");
            DropForeignKey("dbo.Device", "UserId", "dbo.Person");
            DropForeignKey("dbo.Person", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.DeclinedUnblock", "Id", "dbo.DeclinedBlock");
            DropForeignKey("dbo.DeclinedBlockRule", "DeclinedRuleId", "dbo.DeclinedRule");
            DropForeignKey("dbo.DeclinedBlockRule", "DeclinedBlockId", "dbo.DeclinedBlock");
            DropForeignKey("dbo.DeclinedRule", "ProductId", "dbo.Product");
            DropForeignKey("dbo.DeclinedBlock", "DeclinedId", "dbo.Declined");
            DropForeignKey("dbo.Brokerage", "BrokerageGroupId", "dbo.BrokerageGroup");
            DropForeignKey("dbo.Brokerage", "AddressId", "dbo.Address");
            DropForeignKey("dbo.Address", "RegionId", "dbo.Region");
            DropForeignKey("dbo.Region", "CityId", "dbo.City");
            DropForeignKey("dbo.City", "StateId", "dbo.State");
            DropForeignKey("dbo.State", "CountryId", "dbo.Country");
            DropForeignKey("dbo.State", "CarrierId", "dbo.Carrier");
            DropForeignKey("dbo.Carrier", "AddressId", "dbo.Address");
            DropIndex("dbo.PersonPhone", new[] { "PhoneId" });
            DropIndex("dbo.PersonPhone", new[] { "PersonId" });
            DropIndex("dbo.PersonAddress", new[] { "AddressId" });
            DropIndex("dbo.PersonAddress", new[] { "PersonId" });
            DropIndex("dbo.BrokeragePhone", new[] { "PhoneId" });
            DropIndex("dbo.BrokeragePhone", new[] { "BrokerageId" });
            DropIndex("dbo.QuotationSurcharge", new[] { "SurchargePercentageId" });
            DropIndex("dbo.QuotationSurcharge", new[] { "QuotationId" });
            DropIndex("dbo.SurchargeCity", new[] { "CityId" });
            DropIndex("dbo.SurchargeCity", new[] { "CountySurchargeId" });
            DropIndex("dbo.PolicyCoverage", new[] { "CoverageId" });
            DropIndex("dbo.PolicyCoverage", new[] { "PolicyId" });
            DropIndex("dbo.UserProfile", new[] { "ProfileId" });
            DropIndex("dbo.UserProfile", new[] { "UserId" });
            DropIndex("dbo.ProfilePermission", new[] { "PermissionId" });
            DropIndex("dbo.ProfilePermission", new[] { "ProfileId" });
            DropIndex("dbo.DeclinedBlockRule", new[] { "DeclinedRuleId" });
            DropIndex("dbo.DeclinedBlockRule", new[] { "DeclinedBlockId" });
            DropIndex("dbo.AcordTransaction", new[] { "PolicyId" });
            DropIndex("dbo.EmailTemplate", "unique_TemplateId_index");
            DropIndex("dbo.ProductPaymentMethod", new[] { "PaymentMethodId" });
            DropIndex("dbo.ProductPaymentMethod", new[] { "ProductId" });
            DropIndex("dbo.Surcharge", new[] { "StateId" });
            DropIndex("dbo.Surcharge", "unique_Code_index");
            DropIndex("dbo.SurchargePercentage", new[] { "SurchargeId" });
            DropIndex("dbo.RatingPlanForm", "unique_StateId_ProductId_index");
            DropIndex("dbo.RatingPlanFormVersion", "unique_RatingPlanFormId_Version_index");
            DropIndex("dbo.QuotationDelivery", new[] { "QuotationId" });
            DropIndex("dbo.DeclineReason", new[] { "QuotationId" });
            DropIndex("dbo.Quotation", new[] { "BrokerageProductId" });
            DropIndex("dbo.Quotation", new[] { "BusinessId" });
            DropIndex("dbo.Quotation", new[] { "CampaignId" });
            DropIndex("dbo.Quotation", new[] { "RegionId" });
            DropIndex("dbo.Quotation", new[] { "InsuredId" });
            DropIndex("dbo.Quotation", new[] { "BrokerId" });
            DropIndex("dbo.Quotation", new[] { "RatingPlanFormVersionId" });
            DropIndex("dbo.Order", new[] { "PaymentMethodId" });
            DropIndex("dbo.Order", new[] { "QuotationId" });
            DropIndex("dbo.Endorsement", new[] { "RequesterId" });
            DropIndex("dbo.Endorsement", new[] { "ReasonId" });
            DropIndex("dbo.Endorsement", new[] { "PolicyId" });
            DropIndex("dbo.Coverage", new[] { "ProductId" });
            DropIndex("dbo.ProductCondition", new[] { "StateId" });
            DropIndex("dbo.ProductCondition", new[] { "ProductId" });
            DropIndex("dbo.ThirdParty", new[] { "Id" });
            DropIndex("dbo.ClaimDocument", new[] { "ClaimId" });
            DropIndex("dbo.Claim", new[] { "PolicyId" });
            DropIndex("dbo.Business", new[] { "AddressId" });
            DropIndex("dbo.Policy", new[] { "BusinessId" });
            DropIndex("dbo.Policy", new[] { "BrokerId" });
            DropIndex("dbo.Policy", new[] { "InsuredId" });
            DropIndex("dbo.Policy", new[] { "ConditionsId" });
            DropIndex("dbo.Policy", new[] { "Id" });
            DropIndex("dbo.WebpageView", new[] { "WebpageProductId" });
            DropIndex("dbo.WebpageView", new[] { "BrokerageId" });
            DropIndex("dbo.WebpageView", new[] { "CampaignId" });
            DropIndex("dbo.WebpageProduct", "unique_Alias_BrokerageProductId_index");
            DropIndex("dbo.Campaign", new[] { "BrokerId" });
            DropIndex("dbo.Campaign", new[] { "WebpageProductId" });
            DropIndex("dbo.PermissionGroup", new[] { "GroupId" });
            DropIndex("dbo.PermissionGroup", "unique_Name_index");
            DropIndex("dbo.Permission", "unique_ActionApi_GroupId_index");
            DropIndex("dbo.ExceptedPermission", new[] { "UserId" });
            DropIndex("dbo.ExceptedPermission", new[] { "Id" });
            DropIndex("dbo.Device", new[] { "UserId" });
            DropIndex("dbo.Person", new[] { "BrokerageId" });
            DropIndex("dbo.Person", new[] { "CompanyId" });
            DropIndex("dbo.DeclinedUnblock", new[] { "ManagerUserId" });
            DropIndex("dbo.DeclinedUnblock", new[] { "Id" });
            DropIndex("dbo.DeclinedRule", new[] { "ProductId" });
            DropIndex("dbo.DeclinedBlock", new[] { "DeclinedId" });
            DropIndex("dbo.DeclinedBlock", new[] { "ManagerUserId" });
            DropIndex("dbo.Declined", new[] { "ProductId" });
            DropIndex("dbo.Declined", new[] { "InsuredId" });
            DropIndex("dbo.BrokerageProduct", new[] { "BrokerageId" });
            DropIndex("dbo.BrokerageProduct", new[] { "ProductId" });
            DropIndex("dbo.BrokerageGroup", "unique_Name_index");
            DropIndex("dbo.Brokerage", new[] { "AddressId" });
            DropIndex("dbo.Brokerage", new[] { "BrokerageGroupId" });
            DropIndex("dbo.Country", "unique_Abbreviation_index");
            DropIndex("dbo.Country", "unique_Name_index");
            DropIndex("dbo.Carrier", new[] { "AddressId" });
            DropIndex("dbo.State", new[] { "CarrierId" });
            DropIndex("dbo.State", new[] { "CountryId" });
            DropIndex("dbo.State", "unique_Abbreviation_index");
            DropIndex("dbo.State", "unique_Name_index");
            DropIndex("dbo.City", "unique_Name_StateId_index");
            DropIndex("dbo.Region", "unique_ZipCode_CityId_index");
            DropIndex("dbo.Address", new[] { "RegionId" });
            DropTable("dbo.PersonPhone");
            DropTable("dbo.PersonAddress");
            DropTable("dbo.BrokeragePhone");
            DropTable("dbo.QuotationSurcharge");
            DropTable("dbo.SurchargeCity");
            DropTable("dbo.PolicyCoverage");
            DropTable("dbo.UserProfile");
            DropTable("dbo.ProfilePermission");
            DropTable("dbo.DeclinedBlockRule");
            DropTable("dbo.AcordTransaction");
            DropTable("dbo.EmailTemplate",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.NewsLetter",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.__SeedHistory");
            DropTable("dbo.Notification",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.RefreshToken",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.AppClient",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.ProductPaymentMethod",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Surcharge");
            DropTable("dbo.SurchargePercentage",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.RatingPlanForm",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.RatingPlanFormVersion",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.QuotationDelivery",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.DeclineReason",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Quotation",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.PaymentMethod",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Order",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Reason",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Endorsement",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Coverage",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.ProductCondition",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.ThirdParty",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.ClaimDocument",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Claim",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Business",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Policy",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.WebpageView",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.WebpageProduct",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Campaign",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Profile",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Phone");
            DropTable("dbo.PermissionGroup");
            DropTable("dbo.Permission",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.ExceptedPermission",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Device",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Company",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Person",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.DeclinedUnblock",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.DeclinedRule",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.DeclinedBlock",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Declined",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Product",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.BrokerageProduct",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.BrokerageGroup");
            DropTable("dbo.Brokerage",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.Country");
            DropTable("dbo.Carrier",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
            DropTable("dbo.State");
            DropTable("dbo.City");
            DropTable("dbo.Region");
            DropTable("dbo.Address",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "IsDeleted" },
                });
        }
    }
}
