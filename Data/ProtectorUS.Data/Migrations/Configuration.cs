namespace ProtectorUS.Data.Migrations
{
    using Data;
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<ProtectorUSDatabase>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}