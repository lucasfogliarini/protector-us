﻿using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using InteractivePreGeneratedViews;
using System.Data.Entity;

namespace ProtectorUS.Data
{
    /// <summary>
    /// Database context connection factory.
    /// </summary>
    /// <typeparam name="TDbContext"></typeparam>
    public abstract class DbConnectionFactory<TDbContext> :
        IConnectionFactory<TDbContext>,
        IDbConnectionFactory,
        IDisposable
        where TDbContext : DbContext, new()
    {
        TDbContext _context;
        readonly Configuration.ProtectorUSConfiguration ProtectorUSConfiguration = Configuration.ProtectorUSConfiguration.GetConfig();

        /// <summary>
        /// Gets the current database context. If its the first access, creates one and return it.
        /// </summary>
        /// <returns>an instance of <see cref="ProtectorUSDatabase"/>.</returns>
        public TDbContext Get()
        {
            if(_context == null)
            {
                _context = new TDbContext();
                //InteractiveViews.SetViewCacheFactory(
                //    _context, new SqlServerViewCacheFactory(_context.Database.Connection.ConnectionString));
            }
            return _context;
        }

        /// <summary>
        /// Creates a connection based on the given database name or connection string.
        /// </summary>
        /// <param name="nameOrConnectionString">The database name or connection string.</param>
        /// <returns>
        /// An initialized DbConnection.
        /// </returns>
        public DbConnection CreateConnection(string nameOrConnectionString)
        {
            return new SqlConnection(this.GetconnectionString());
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            if (this._context == null)
                return;


            this._context.Dispose();
        }

        /// <summary>
        /// Gets the application's database connection string.
        /// </summary>
        /// <returns>the connection string.</returns>
        private string GetconnectionString()
        {
            var builder = new SqlConnectionStringBuilder()
            {
                DataSource = ProtectorUSConfiguration.Database.DataSource,
                InitialCatalog = ProtectorUSConfiguration.Database.Database,
                UserID = ProtectorUSConfiguration.Database.Username,
                Password = ProtectorUSConfiguration.Database.Password,
                MultipleActiveResultSets = true
            };

            return builder.ToString();
        }
    }
}
