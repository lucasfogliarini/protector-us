﻿using System.Data.Entity;

namespace ProtectorUS.Data
{
    
    /// <summary>
    /// Defines the behavior for produce database connections.
    /// </summary>
    public interface IConnectionFactory<TDbContext> where TDbContext : DbContext
    {
        /// <summary>
        /// Gets the current database context. If its the first access, creates one and return it.
        /// </summary>
        /// <returns>an instance of <typeparamref name="TDbContext"/>.</returns>
        TDbContext Get();
    }
}