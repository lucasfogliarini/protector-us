﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class PaymentMethodMap : AuditableEntityMap<PaymentMethod,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(e => e.Icon).HasColumnName("Icon").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("PaymentMethod");
        }
    }
}
