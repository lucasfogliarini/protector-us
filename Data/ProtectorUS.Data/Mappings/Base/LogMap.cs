﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="Log"/> entity mapper.
    /// </summary>
    public class LogMap : Map<Log>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Level).HasColumnName("Level").IsRequired();
            Property(e => e.Logger).HasColumnName("Logger").IsRequired();
            Property(e => e.Message).HasColumnName("Message").IsRequired();
            Property(e => e.UserId).HasColumnName("UserId").IsRequired();
            Property(e => e.UserName).HasColumnName("UserName").IsRequired();
            Property(e => e.Exception).HasColumnName("Exception").IsRequired();
            Property(e => e.Date).HasColumnName("Date").IsRequired();
            Property(e => e.AdditionalInfo).HasColumnName("AdditionalInfo").IsOptional().HasColumnType("text");
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("Log");
        }
    }
}
