﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class AuditLogDetailMap : Map<AuditLogDetail>  
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.NewValue).HasColumnName("NewValue");
            Property(e => e.OriginalValue).HasColumnName("OriginalValue");
            Property(e => e.PropertyName).HasColumnName("PropertyName");
        }

        /// Maps the entity's relationships.

        protected override void MapRelationships()
        {
            HasRequired(x => x.AuditLog).WithMany(x => x.LogDetails).HasForeignKey(x => x.AuditLogId);
        }

        protected override void MapTable()
        {
            ToTable("AuditLogDetail");
        }
    }
}
