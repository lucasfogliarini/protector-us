﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class AuditLogMap : Map<AuditLog> 
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.EventDateUTC).HasColumnName("EventDateUTC");
            Property(e => e.EventType).HasColumnName("EventType");
            Property(e => e.RecordId).HasColumnName("RecordId");
            Property(e => e.TypeFullName).HasColumnName("TypeFullName");
            Property(e => e.UserName).HasColumnName("UserName");
        }

        /// Maps the entity's relationships.

        protected override void MapRelationships()
        {
            HasMany(x => x.LogDetails);
        }

        protected override void MapTable()
        {
            ToTable("AuditLog");
        }
    }
}
