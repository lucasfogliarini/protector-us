﻿using System;
using ProtectorUS.Model;

namespace ProtectorUS.Data.Mappings
{
    public abstract class AuditableEntityMap<TEntity,TId> : Map<TEntity>  where TEntity : AuditableEntity<TId>
    {
        protected AuditableEntityMap()
        {
            MapAuditableProperties();
        }

        private void MapAuditableProperties()
        {
            Property(e => e.CreatedBy).HasColumnName("CreatedBy").IsRequired();
            Property(e => e.CreatedDate).HasColumnName("CreatedDate").IsRequired();
            Property(e => e.UpdatedBy).HasColumnName("UpdatedBy");
            Property(e => e.UpdatedDate).HasColumnName("UpdatedDate");
        }
    }
}
