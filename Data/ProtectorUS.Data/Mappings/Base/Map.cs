﻿using System;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace ProtectorUS.Data.Mappings
{
    /// <summary>
    /// Base entities model map.
    /// </summary>
    /// <typeparam name="T">the entity type to map.</typeparam>
    public abstract class Map<T> :
        System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of <see cref="Map{T}"/> class.
        /// </summary>
        protected Map()
        {
            this.MapKey();
            this.MapProperties();
            this.MapRelationships();
            this.MapTable();
        }

        /// <summary>
        /// Maps the entity's unique identifier.
        /// </summary>
        protected virtual void MapKey() { }

        /// <summary>
        /// Maps the entity's properties.
        /// </summary>
        protected virtual void MapProperties() { }

        /// <summary>
        /// Maps the entity's relationships.
        /// </summary>
        protected virtual void MapRelationships() { }

        /// <summary>
        /// Maps the entity's table.
        /// </summary>
        protected virtual void MapTable() { }
        
        public string IndexUniqueMultiple<TProperty1, TProperty2>(Expression<Func<T, TProperty1>> firstProperty, Expression<Func<T, TProperty2>> secondProperty, bool maxLengthMinimum = false)
        {
            string firstPropertyName = (firstProperty.Body as MemberExpression).Member.Name;
            string secondPropertyName = (secondProperty.Body as MemberExpression).Member.Name;
            string uniqueIndexName = $"unique_{firstPropertyName}_{secondPropertyName}_index";
            IndexUniqueMultiple(GetPropertyConfiguration(firstProperty), GetPropertyConfiguration(secondProperty), uniqueIndexName, maxLengthMinimum);
            return uniqueIndexName;
        }

        private PrimitivePropertyConfiguration GetPropertyConfiguration<TProperty>(Expression<Func<T, TProperty>> property)
        {
            if (typeof(TProperty) == typeof(string)) return Property(property as Expression<Func<T, string>>);
            if (typeof(TProperty) == typeof(long)) return Property(property as Expression<Func<T, long>>);
            if (typeof(TProperty) == typeof(int)) return Property(property as Expression<Func<T, int>>);
            return null;
        }
        private void IndexUniqueMultiple(PrimitivePropertyConfiguration firstProperty, PrimitivePropertyConfiguration secondProperty, string uniqueIndexName, bool maxLengthMinimum = false)
        {
            if (maxLengthMinimum)
            {
                if (firstProperty is StringPropertyConfiguration)
                {
                    (firstProperty as StringPropertyConfiguration).HasMaxLength(400);
                }
                if (secondProperty is StringPropertyConfiguration)
                {
                    (secondProperty as StringPropertyConfiguration).HasMaxLength(400);
                }
            }
            IndexUnique(firstProperty, uniqueIndexName, 1);
            IndexUnique(secondProperty, uniqueIndexName, 2);
        }
        public string IndexUnique(Expression<Func<T, string>> property, bool maxLengthMinimum = true)
        {
            var propertyConfiguration = Property(property);
            if (maxLengthMinimum)
            {
                propertyConfiguration.HasMaxLength(400);
            }
            string propertyName = (property.Body as MemberExpression).Member.Name;
            string uniqueIndexName = $"unique_{propertyName}_index";
            IndexUnique(propertyConfiguration, uniqueIndexName, 1);
            return uniqueIndexName;
        }
        private void IndexUnique(PrimitivePropertyConfiguration propertyConfiguration, string indexName, int order)
        {
            propertyConfiguration.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute(indexName) { IsUnique = true, Order = order }));
        }
    }
}
