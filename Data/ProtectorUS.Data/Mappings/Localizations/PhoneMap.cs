﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class PhoneMap : Map<Phone>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Number).HasColumnName("Number").IsRequired();
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.Description).HasColumnName("Description");
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("Phone");
        }
    }
}
