﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CityMap : Map<City>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            IndexUniqueMultiple(e => e.Name, e => e.StateId, true);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.State).WithMany(e=>e.Cities).HasForeignKey(e=>e.StateId);
            HasMany(e => e.Regions);
        }
        protected override void MapTable()
        {
            ToTable("City");
        }
    }
}
