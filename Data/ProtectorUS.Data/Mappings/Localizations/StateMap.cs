﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class StateMap : Map<State>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.Abbreviation).HasColumnName("Abbreviation").IsRequired();
            IndexUnique(e => e.Name);
            IndexUnique(e => e.Abbreviation);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Country).WithMany(e=>e.States).HasForeignKey(e=>e.CountryId);
            HasOptional(e => e.Carrier).WithMany(e => e.States).HasForeignKey(e => e.CarrierId);
            HasMany(e => e.Cities);
        }
        protected override void MapTable()
        {
            ToTable("State");
        }
    }
}
