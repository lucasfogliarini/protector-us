﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class RegionMap : Map<Region>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.ZipCode).HasColumnName("ZipCode").IsRequired().HasMaxLength(20);
            IndexUniqueMultiple(e => e.ZipCode, e => e.CityId);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.City).WithMany(e=>e.Regions).HasForeignKey(e=>e.CityId);
        }
        protected override void MapTable()
        {
            ToTable("Region");
        }
    }
}
