﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class AddressMap : Map<Address>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.StreetLine1).HasColumnName("StreetLine1");
            Property(e => e.StreetLine2).HasColumnName("StreetLine2");
            Property(e => e.Latitude).HasColumnName("Latitude");
            Property(e => e.Longitude).HasColumnName("Longitude");
            Ignore(e => e.StreetFull);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Region).WithMany().HasForeignKey(e=>e.RegionId);
        }
        protected override void MapTable()
        {
            ToTable("Address");
        }
    }
}
