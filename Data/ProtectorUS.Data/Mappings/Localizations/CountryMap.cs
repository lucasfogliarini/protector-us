﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CountryMap : Map<Country>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.Abbreviation).HasColumnName("Abbreviation").IsRequired();
            IndexUnique(e => e.Name);
            IndexUnique(e => e.Abbreviation);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.States);
        }
        protected override void MapTable()
        {
            ToTable("Country");
        }
    }
}
