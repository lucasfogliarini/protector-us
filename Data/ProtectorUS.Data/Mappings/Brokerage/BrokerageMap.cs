﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BrokerageMap : AuditableEntityMap<Brokerage,long>
    {
        protected override void MapKey()
        {
            HasKey(e=>e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Alias).HasColumnName("Alias").IsRequired().HasMaxLength(Brokerage.AliasMaxLength);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(Brokerage.NameMaxLength);
            Property(e => e.Status).HasColumnName("Status").IsRequired();
            Property(e => e.ChargeAccountStatus).HasColumnName("ChargeAccountStatus").IsRequired();
            Property(e => e.Email).HasColumnName("Email").IsRequired();
            Property(e => e.Logo).HasColumnName("Logo");
            Property(e => e.Cover).HasColumnName("Cover");
            Property(e => e.Website).HasColumnName("Website");
            Property(e => e.ChargeAccountId).HasColumnName("ChargeAccountId");
            Property(e => e.AgentNumber).HasColumnName("AgentNumber");

        }
        protected override void MapRelationships()
        {
            HasOptional(e => e.BrokerageGroup).WithMany(e=>e.Brokerages).HasForeignKey(e=>e.BrokerageGroupId);
            HasMany(e => e.Brokers);
            HasMany(e => e.Phones)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("BrokerageId");
                    cs.MapRightKey("PhoneId");
                    cs.ToTable("BrokeragePhone");
                });
            HasMany(e => e.BrokerageProducts).WithRequired(e => e.Brokerage).HasForeignKey(e => e.BrokerageId);
            HasRequired(e => e.Address).WithMany().HasForeignKey(e => e.AddressId);
        }

        protected override void MapTable()
        {
            ToTable("Brokerage");
        }
    }
}
