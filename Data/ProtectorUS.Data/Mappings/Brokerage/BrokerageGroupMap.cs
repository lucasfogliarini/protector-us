﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BrokerageGroupMap : Map<BrokerageGroup>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            IndexUnique(e => e.Name);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Brokerages);
        }
        protected override void MapTable()
        {
            ToTable("BrokerageGroup");
        }
    }
}
