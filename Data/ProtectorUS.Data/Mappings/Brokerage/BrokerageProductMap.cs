﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BrokerageProductMap : AuditableEntityMap<BrokerageProduct,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.ActiveARGO).HasColumnName("ActiveARGO").IsRequired();
            Property(e => e.ActiveBrokerage).HasColumnName("ActiveBrokerage").IsRequired();
            Ignore(e => e.Active);
            Ignore(e => e.BrokerageComission);
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Product).WithMany(x => x.BrokerageProducts).HasForeignKey(x => x.ProductId);
            HasRequired(x => x.Brokerage).WithMany(x => x.BrokerageProducts).HasForeignKey(x => x.BrokerageId);
            HasMany(e => e.WebpageProducts).WithRequired(e => e.BrokerageProduct).HasForeignKey(e => e.BrokerageProductId);
        }
        protected override void MapTable()
        {
            ToTable("BrokerageProduct");
        }
    }
}
