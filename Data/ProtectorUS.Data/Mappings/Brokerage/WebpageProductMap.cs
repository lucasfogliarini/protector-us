﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    using Model.FloatingPoint;
    public class WebpageProductMap : AuditableEntityMap<WebpageProduct,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Alias).HasColumnName("Alias").IsRequired().HasMaxLength(WebpageProduct.AliasMaxLength);
            Property(e => e.Active).HasColumnName("Active").IsRequired();
            Property(e => e.Default).HasColumnName("Default").IsRequired();
            Property(e => e.Logo).HasColumnName("Logo");
            Property(e => e.Cover).HasColumnName("Cover");
            Property(e => e.Description).HasColumnName("Description");
            IndexUniqueMultiple(e => e.Alias, e => e.BrokerageProductId);
            Property(e => e.BrokerageComission).HasColumnName("BrokerageComission").HasPrecision(Percentage.precision, Percentage.scale).IsRequired();
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Campaigns);
            HasRequired(e => e.BrokerageProduct).WithMany(e => e.WebpageProducts).HasForeignKey(e => e.BrokerageProductId);
        }
        protected override void MapTable()
        {
            ToTable("WebpageProduct");
        }
    }
}
