﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class WebpageViewMap : AuditableEntityMap<WebpageView,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Access).HasColumnName("Access").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasOptional(x => x.WebpageProduct).WithMany(x => x.Views).HasForeignKey(x => x.WebpageProductId);
            HasOptional(x => x.Campaign).WithMany().HasForeignKey(x => x.CampaignId);
        }
        protected override void MapTable()
        {
            ToTable("WebpageView");
        }
    }
}
