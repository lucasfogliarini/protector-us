﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class AppClientMap : AuditableEntityMap<AppClient, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.ClientId).HasColumnName("ClientId").IsRequired().HasMaxLength(20);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(e => e.Active).HasColumnName("Active").IsRequired();
            Property(e => e.RefreshTokenLifeTime).HasColumnName("RefreshTokenLifeTime").IsRequired();
            Property(e => e.AllowedOrigin).HasColumnName("AllowedOrigin").IsRequired();
            Property(e => e.CurrentVersion).HasColumnName("CurrentVersion").IsRequired();
            Property(e => e.ReviewVersion).HasColumnName("ReviewVersion").IsRequired();
            Property(e => e.InReview).HasColumnName("InReview").IsRequired();
            Property(e => e.Token).HasColumnName("Token").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("AppClient");
        }
    }
}
