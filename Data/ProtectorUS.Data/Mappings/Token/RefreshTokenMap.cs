﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class RefreshTokenMap : AuditableEntityMap<RefreshToken, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.RefreshTokenId).HasColumnName("RefreshTokenId").IsRequired();
            Property(e => e.AppClientId).HasColumnName("AppClientId").IsRequired();
            Property(e => e.IdentityName).HasColumnName("IdentityName").IsRequired();
            Property(e => e.ProtectedTicket).HasColumnName("ProtectedTicket").IsRequired();
            Property(e => e.IssuedDate).HasColumnName("IssuedDate");
            Property(e => e.ExpiresDate).HasColumnName("ExpiresDate");
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("RefreshToken");
        }
    }
}
