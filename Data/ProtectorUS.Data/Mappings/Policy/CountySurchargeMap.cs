﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CountySurchargeMap : Map<CountySurcharge>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Surcharge.SurchargeType).HasValue("County"));
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Cities)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("CountySurchargeId");
                    cs.MapRightKey("CityId");
                    cs.ToTable("SurchargeCity");
                });
        }
        protected override void MapTable()
        {
        }
    }
}
