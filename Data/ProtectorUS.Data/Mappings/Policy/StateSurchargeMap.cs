﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class StateSurchargeMap : Map<StateSurcharge>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Surcharge.SurchargeType).HasValue("State"));
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.State).WithMany().HasForeignKey(e => e.StateId);
        }
        protected override void MapTable()
        {
        }
    }
}
