﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    using Model.FloatingPoint;
    public class PolicyMap : AuditableEntityMap<Policy,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(e => e.Number).HasColumnName("Number").IsRequired().HasMaxLength(20);
            Property(e => e.SequentialNumber).HasColumnName("SequentialNumber").IsRequired();
            Property(e => e.PolicyPath).HasColumnName("PolicyPath");
            Property(e => e.CertificatePath).HasColumnName("CertificatePath").IsRequired();            
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.PeriodStart).HasColumnName("PeriodStart").IsRequired();
            Property(e => e.PeriodEnd).HasColumnName("PeriodEnd").IsRequired();
            Property(e => e.Canceled).HasColumnName("Canceled").IsRequired();
            Property(e => e.InsuredTimezone).HasColumnName("InsuredTimezone").IsRequired();
            Property(e => e.TotalPremium).HasColumnName("TotalPremium").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.CoverageEachClaim).HasColumnName("CoverageEachClaim").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.CoverageAggregate).HasColumnName("CoverageAggregate").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.DeductibleEachClaim).HasColumnName("DeductibleEachClaim").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.DeductibleAggregate).HasColumnName("DeductibleAggregate").HasPrecision(Money.precision, Money.scale).IsRequired();
            Ignore(e => e.Status);
            Ignore(e => e.PeriodRenewal);
            Ignore(e => e.CertificateFileName);
        }
        protected override void MapRelationships()
        {
            HasOptional(x => x.Business).WithMany().HasForeignKey(x => x.BusinessId);
            HasRequired(x => x.Insured).WithMany(x => x.Policies).HasForeignKey(x => x.InsuredId);
            HasRequired(x => x.Broker).WithMany(x => x.Policies).HasForeignKey(x => x.BrokerId);
            HasRequired(x => x.Condition).WithMany().HasForeignKey(x => x.ConditionsId);
            HasMany(x => x.Claims);
            HasMany(x => x.Endorsements);
            HasRequired(e => e.Order).WithOptional(e => e.Policy);
            HasMany(e => e.Coverages)
            .WithMany()
            .Map(cs =>
            {
                cs.MapLeftKey("PolicyId");
                cs.MapRightKey("CoverageId");
                cs.ToTable("PolicyCoverage");
            });
        }
        protected override void MapTable()
        {
            ToTable("Policy");
        }
    }
}
