﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    using Model.FloatingPoint;
    public class OrderMap : AuditableEntityMap<Order,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.TransactionCode).HasColumnName("TransactionCode").IsRequired();
            Property(e => e.PaymentStatus).HasColumnName("PaymentStatus").IsRequired();
            Property(e => e.TotalInstallments).HasColumnName("TotalInstallments").IsRequired();
            Property(e => e.OrderDate).HasColumnName("OrderDate").IsRequired();
            Property(e => e.Rounding).HasColumnName("Rounding").HasPrecision(Rounding.precision, Rounding.scale).IsRequired();
            Property(e => e.Discount).HasColumnName("Discount").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.ArgoFee).HasColumnName("ArgoFee").HasPrecision(Money.precision,Money.scale).IsRequired();
            Property(e => e.BrokerageFee).HasColumnName("BrokerageFee").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.BrokerageFeePercentage).HasColumnName("BrokerageFeePercentage").HasPrecision(Percentage.precision, Percentage.scale).IsRequired();
            Property(e => e.TotalCharged).HasColumnName("TotalCharged").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.SurchargeAmount).HasColumnName("SurchargeAmount").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.StripeFee).HasColumnName("StripeFee").HasPrecision(Money.precision, Money.scale);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Quotation).WithMany(e=>e.Orders).HasForeignKey(e=>e.QuotationId);
            HasRequired(e => e.PaymentMethod).WithMany().HasForeignKey(e => e.PaymentMethodId);
        }
        protected override void MapTable()
        {
            ToTable(typeof(Order).Name);
        }
    }
}
