﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class AcordTransactionMap : Map<AcordTransaction>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            Property(e => e.Xml).HasColumnName("Xml");
            Property(e => e.CreatedDate).HasColumnName("CreatedDate").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Policy).WithMany().HasForeignKey(e => e.PolicyId);
        }
        protected override void MapTable()
        {
            ToTable("AcordTransaction");
        }
    }
}
