﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    using System.ComponentModel.DataAnnotations.Schema;
    public class SurchargeMap : Map<Surcharge>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Code).HasColumnName("Code").IsRequired();
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.MinTax).HasColumnName("MinTax");
            Property(e => e.Description).HasColumnName("Description");
            IndexUnique(e => e.Code);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Percentages).WithRequired(e=>e.Surcharge).HasForeignKey(e=>e.SurchargeId);
        }
        protected override void MapTable()
        {
            ToTable(typeof(Surcharge).Name);
        }
    }
}
