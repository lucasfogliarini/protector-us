﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    using Model.FloatingPoint;
    using System.ComponentModel.DataAnnotations.Schema;
    public class SurchargePercentageMap : AuditableEntityMap<SurchargePercentage, long>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Percentage).HasColumnName("Percentage").HasPrecision(Percentage.precision, Percentage.scale).IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Surcharge).WithMany(e=>e.Percentages).HasForeignKey(e => e.SurchargeId);
        }
        protected override void MapTable()
        {
            ToTable(typeof(SurchargePercentage).Name);
        }
    }
}
