﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="ClaimDocument"/> entity mapper.
    /// </summary>
    public class ClaimDocumentMap : AuditableEntityMap<ClaimDocument, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.Path).HasColumnName("Path").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Claim).WithMany(x => x.ClaimDocuments).HasForeignKey(x => x.ClaimId);
        }
        protected override void MapTable()
        {
            ToTable("ClaimDocument");
        }
    }
}
