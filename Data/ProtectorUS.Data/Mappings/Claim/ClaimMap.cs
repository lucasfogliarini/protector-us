﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="Claim"/> entity mapper.
    /// </summary>
    public class ClaimMap : AuditableEntityMap<Claim, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.OccurrenceDate).HasColumnName("OccurrenceDate").IsRequired();
            Property(e => e.Number).HasColumnName("Number").IsRequired().HasMaxLength(20);
            Property(e => e.ClaimDate).HasColumnName("ClaimDate").IsRequired();
            Property(e => e.DescriptionFacts).HasColumnName("DescriptionFacts").IsRequired();
            Property(e => e.PriorNotification).HasColumnName("PriorNotification").IsRequired();
            Property(e => e.TotalPaid).HasColumnName("TotalPaid").IsOptional();
            Property(e => e.LossReserve).HasColumnName("LossReserve").IsOptional();
            Property(e => e.ALAEReserve).HasColumnName("ALAEReserve").IsOptional();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Policy).WithMany(x => x.Claims).HasForeignKey(x => x.PolicyId);
            HasMany(e => e.ClaimDocuments);
            HasOptional(e => e.ThirdParty).WithRequired(e => e.Claim);
        }
        protected override void MapTable()
        {
            ToTable("Claim");
        }
    }
}
