﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="ThirdParty"/> entity mapper.
    /// </summary>
    public class ThirdPartyMap : AuditableEntityMap<ThirdParty, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.Email).HasColumnName("Email").IsRequired();
            Property(e => e.Phone).HasColumnName("Phone").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Claim).WithOptional(e => e.ThirdParty);
        }
        protected override void MapTable()
        {
            ToTable("ThirdParty");
        }
    }
}
