﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    using System.ComponentModel.DataAnnotations.Schema;
    public class EmailTemplateMap : AuditableEntityMap<EmailTemplate,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.TemplateId).HasColumnName("TemplateId").IsRequired();
            Property(e => e.Name).HasColumnName("Name");
            Property(e => e.From).HasColumnName("From").IsRequired();
            Property(e => e.FromName).HasColumnName("FromName").IsRequired();
            Property(e => e.Subject).HasColumnName("Subject").IsRequired();
            IndexUnique(e => e.TemplateId);
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("EmailTemplate");
        }
    }
}
