﻿using System.ComponentModel.DataAnnotations.Schema;
namespace ProtectorUS.Data.Mappings
{

    using Model;

    public class NotificationMap : Map<Notification>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(e => e.Title).HasColumnName("Title").IsRequired();
            Property(e => e.Description).HasColumnName("Description").IsRequired();
            Property(e => e.Message).HasColumnName("Message").IsRequired();
            Property(e => e.ActorId).HasColumnName("ActorId").IsOptional();
            Property(e => e.ActorType).HasColumnName("ActorType").IsOptional();
            Property(e => e.SourceId).HasColumnName("SourceId").IsOptional();
            Property(e => e.SourceType).HasColumnName("SourceType").IsOptional();
            Property(e => e.DestinationId).HasColumnName("DestinationId").IsOptional();
            Property(e => e.DestinationType).HasColumnName("DestinationType").IsOptional();
            Property(e => e.Type).HasColumnName("Type").IsOptional();
            Property(e => e.ActivityType).HasColumnName("ActivityType").IsOptional();
            Property(e => e.EventType).HasColumnName("EventType").IsOptional();
            Property(e => e.ExpiresOn).HasColumnName("ExpiresOn").IsOptional();
            Property(e => e.ReadDate).HasColumnName("ReadDate").IsOptional();
            Property(e => e.SendDate).HasColumnName("SendDate").IsOptional();
            Property(e => e.Answered).HasColumnName("Answered").IsOptional();
            Property(e => e.Sent).HasColumnName("Sent").IsRequired();
            Property(e => e.Read).HasColumnName("Read").IsRequired();
        }
        protected override void MapRelationships()
        {

        }
        protected override void MapTable()
        {
            ToTable("Notification");
        }
    }
}
