﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    using System.ComponentModel.DataAnnotations.Schema;
    public class RatingPlanFormMap : Map<RatingPlanForm>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Active).HasColumnName("Active").IsRequired();
            IndexUniqueMultiple(e => e.StateId, e => e.ProductId);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Product).WithMany().HasForeignKey(e => e.ProductId);
            HasRequired(e => e.State).WithMany().HasForeignKey(e => e.StateId);
            HasMany(e => e.RatingPlanFormVersions).WithRequired(e=>e.RatingPlanForm).HasForeignKey(e=>e.RatingPlanFormId);
        }
        protected override void MapTable()
        {
            ToTable("RatingPlanForm");
        }
    }
}
