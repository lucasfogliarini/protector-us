﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    using System.ComponentModel.DataAnnotations.Schema;
    public class RatingPlanFormVersionMap : AuditableEntityMap<RatingPlanFormVersion, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Version).HasColumnName("Version").IsRequired();
            IndexUniqueMultiple(e => e.RatingPlanFormId, e => e.Version);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.RatingPlanForm).WithMany().HasForeignKey(e => e.RatingPlanFormId);
        }
        protected override void MapTable()
        {
            ToTable("RatingPlanFormVersion");
        }
    }
}
