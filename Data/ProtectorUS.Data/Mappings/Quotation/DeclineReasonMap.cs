﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="DeclineReason"/> entity mapper.
    /// </summary>
    public class DeclineReasonMap : AuditableEntityMap<DeclineReason, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.QuotationId).HasColumnName("RatingPlanFormVersionId").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Quotation).WithMany().HasForeignKey(x=> x.QuotationId);
        }
        protected override void MapTable()
        {
            ToTable("DeclineReason");
        }
    }
}
