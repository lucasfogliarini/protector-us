﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="QuotationDelivery"/> entity mapper.
    /// </summary>
    public class QuotationDeliveryMap : AuditableEntityMap<QuotationDelivery,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.QuotationId).HasColumnName("QuotationId").IsRequired();
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.Date).HasColumnName("Date").IsRequired();
            Property(e => e.Email).HasColumnName("Email").IsOptional();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Quotation).WithMany(x => x.QuotationDeliveries).HasForeignKey(x => x.QuotationId);
        }
        protected override void MapTable()
        {
            ToTable("QuotationDelivery");
        }
    }
}
