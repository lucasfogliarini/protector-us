﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    using Model.FloatingPoint;    

    /// <summary>
    /// <see cref="Quotation"/> entity mapper.
    /// </summary>
    public class QuotationMap : AuditableEntityMap<Quotation,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Hash).HasColumnName("Hash").IsRequired();
            Property(e => e.RiskAnalysisJson).HasColumnName("RiskAnalysisJson").IsRequired();
            Property(e => e.PolicyStartDate).HasColumnName("PolicyStartDate").IsRequired();
            Property(e => e.IsDeclined).HasColumnName("IsDeclined").IsRequired();
            Property(e => e.Status).HasColumnName("Status").IsRequired();
            Property(e => e.ExpirationDate).HasColumnName("ExpirationDate").IsRequired();
            Property(e => e.ProponentFirstName).HasColumnName("ProponentFirstName");
            Property(e => e.ProponentLastName).HasColumnName("ProponentLastName");
            Property(e => e.ProponentSocialTitle).HasColumnName("ProponentSocialTitle");
            Property(e => e.ProponentEmail).HasColumnName("ProponentEmail");
            Property(e => e.InsuredTimezone).HasColumnName("InsuredTimezone").IsRequired();
            Property(e => e.Revenue).HasColumnName("Revenue").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.PolicyPremium).HasColumnName("PolicyPremium").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.Rounding).HasColumnName("Rounding").HasPrecision(Rounding.precision, Rounding.scale).IsRequired();
            Property(e => e.TaxAmount).HasColumnName("TaxAmount").HasPrecision(Money.precision, Money.scale).IsRequired();
            Property(e => e.FeeAmount).HasColumnName("FeeAmount").HasPrecision(Money.precision, Money.scale).IsRequired();
            Ignore(e => e.Total);
            Ignore(e => e.SurchargeAmount);
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.RatingPlanFormVersion).WithMany().HasForeignKey(e => e.RatingPlanFormVersionId);
            HasRequired(x => x.Broker).WithMany(x => x.Quotations).HasForeignKey(x => x.BrokerId);
            HasRequired(x => x.Region).WithMany().HasForeignKey(x => x.RegionId);
            HasRequired(x => x.BrokerageProduct).WithMany().HasForeignKey(x => x.BrokerageProductId);
            HasOptional(x => x.Campaign).WithMany().HasForeignKey(x => x.CampaignId);
            HasOptional(x => x.Business).WithMany().HasForeignKey(x => x.BusinessId);
            HasOptional(x => x.Insured).WithMany().HasForeignKey(x => x.InsuredId);
            HasMany(x => x.QuotationDeliveries);
            HasMany(e => e.Surcharges)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("QuotationId");
                    cs.MapRightKey("SurchargePercentageId");
                    cs.ToTable("QuotationSurcharge");
                });
            HasMany(e => e.DeclineReasons);
        }
        protected override void MapTable()
        {
            ToTable("Quotation");
        }
    }
}
