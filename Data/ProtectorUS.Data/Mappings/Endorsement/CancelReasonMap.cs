﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CancelReasonMap : Map<CancelReason>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires("ReasonType").HasValue("Cancel"));
        }
        protected override void MapTable()
        {
        }
    }
}
