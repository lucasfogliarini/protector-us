﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="Endorsement"/> entity mapper.
    /// </summary>
    public class EndorsementMap : AuditableEntityMap<Endorsement,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.Type).HasColumnName("Type").IsRequired();
            Property(e => e.Status).HasColumnName("Status").IsRequired();
            Ignore(e => e.Product);
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Policy).WithMany(x => x.Endorsements).HasForeignKey(x => x.PolicyId);
            HasRequired(x => x.Reason).WithMany().HasForeignKey(x => x.ReasonId);
            HasRequired(x => x.Requester).WithMany().HasForeignKey(x => x.RequesterId);
        }
        protected override void MapTable()
        {
            ToTable("Endorsement");
        }
    }
}
