﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class ReasonMap : AuditableEntityMap<Reason, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Description).HasColumnName("Description").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("Reason");
        }
    }
}
