﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class PersonMap : AuditableEntityMap<Person,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Email).HasColumnName("Email").IsRequired().HasMaxLength(Person.EmailMaxLength);
            Property(e => e.FirstName).HasColumnName("FirstName").HasMaxLength(Person.FirstNameMaxLength);
            Property(e => e.LastName).HasColumnName("LastName").HasMaxLength(Person.LastNameMaxLength);
            Property(e => e.Picture).HasColumnName("Picture");
            Property(e => e.BirthDate).HasColumnName("BirthDate");
            Property(e => e.SocialTitle).HasColumnName("SocialTitle");
            Ignore(e => e.Gender);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Phones)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("PersonId");
                    cs.MapRightKey("PhoneId");
                    cs.ToTable("PersonPhone");
                });
            HasMany(e => e.Addresses)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("PersonId");
                    cs.MapRightKey("AddressId");
                    cs.ToTable("PersonAddress");
                });
        }
        protected override void MapTable()
        {
            ToTable("Person");
        }
    }
}
