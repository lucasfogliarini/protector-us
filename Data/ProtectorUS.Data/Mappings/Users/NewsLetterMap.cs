﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class NewsLetterMap : AuditableEntityMap<NewsLetter,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Email).HasColumnName("Email").IsRequired().HasMaxLength(150);
            Property(e => e.Origin).HasColumnName("Origin");
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("NewsLetter");
        }
    }
}
