﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class ManagerUserMap : Map<ManagerUser>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Person.PersonType).HasValue("ManagerUser"));
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Company).WithMany(e=>e.ManagerUsers).HasForeignKey(e=>e.CompanyId);
        }
        protected override void MapTable()
        {
        }
    }
}
