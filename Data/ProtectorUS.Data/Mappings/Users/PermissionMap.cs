﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class PermissionMap : AuditableEntityMap<Permission,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.ActionApi).HasColumnName("ActionApi").IsRequired();
            Property(e => e.Order).HasColumnName("Order");
            IndexUniqueMultiple(e => e.ActionApi, e => e.GroupId, true);
            Ignore(e => e.Key);
        }

        protected override void MapRelationships()
        {
            HasRequired(e => e.Group).WithMany(e => e.Permissions).HasForeignKey(e => e.GroupId);
        }

        protected override void MapTable()
        {
            ToTable("Permission");
        }
    }
}
