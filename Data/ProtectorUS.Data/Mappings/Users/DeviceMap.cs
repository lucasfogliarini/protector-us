﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class DeviceMap : Map<Device>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.DeviceId).HasColumnName("DeviceId").IsRequired();
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.Active).HasColumnName("Active").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.User).WithMany().HasForeignKey(e => e.UserId);
        }
        protected override void MapTable()
        {
            ToTable("Device");
        }
    }
}
