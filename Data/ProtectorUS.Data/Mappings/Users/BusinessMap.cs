﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BusinessMap : AuditableEntityMap<Business,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(e => e.Type).HasColumnName("Type").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Address).WithMany().HasForeignKey(e=>e.AddressId);
        }
        protected override void MapTable()
        {
            ToTable("Business");
        }
    }
}
