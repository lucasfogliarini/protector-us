﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class ExceptedPermissionMap : AuditableEntityMap<ExceptedPermission,long>
    {
        protected override void MapKey()
        {
            HasKey(e => new { e.Id, e.UserId });
        }

        protected override void MapProperties()
        {
            Property(e => e.Allowed).HasColumnName("Allowed").IsRequired();
        }

        protected override void MapRelationships()
        {
            HasRequired(e => e.Permission).WithMany().HasForeignKey(e => e.Id).WillCascadeOnDelete();
            HasRequired(e => e.User).WithMany(e=>e.ExceptedPermissions).HasForeignKey(e => e.UserId).WillCascadeOnDelete();
        }

        protected override void MapTable()
        {
            ToTable("ExceptedPermission");
        }
    }
}
