﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class ProfileMap : AuditableEntityMap<Profile,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(100);
            Property(e => e.Description).HasColumnName("Description");
        }

        protected override void MapRelationships()
        {
            HasMany(e => e.Permissions)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("ProfileId");
                    cs.MapRightKey("PermissionId");
                    cs.ToTable("ProfilePermission");
                });
        }

        protected override void MapTable()
        {
            ToTable("Profile");
        }
    }
}
