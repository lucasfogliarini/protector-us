﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BrokerMap : Map<Broker>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Person.PersonType).HasValue("Broker"));
        }
        protected override void MapRelationships()
        {
            HasRequired(e => e.Brokerage).WithMany(e=>e.Brokers).HasForeignKey(e=>e.BrokerageId);
            HasMany(e => e.Quotations);
        }
        protected override void MapTable()
        {
        }
    }
}
