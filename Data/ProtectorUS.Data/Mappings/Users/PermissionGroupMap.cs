﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class PermissionGroupMap : Map<PermissionGroup>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }

        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired();
            Property(e => e.Order).HasColumnName("Order").IsRequired();
            Property(e => e.Description).HasColumnName("Description");
            IndexUnique(e => e.Name);
        }

        protected override void MapRelationships()
        {
            HasOptional(e => e.Group).WithMany(e => e.Groups).HasForeignKey(e => e.GroupId);
            HasMany(e => e.Groups);
        }

        protected override void MapTable()
        {
            ToTable("PermissionGroup");
        }
    }
}
