﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class UserMap : Map<User>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Person.PersonType).HasValue("User"));
            Property(e => e.Status).HasColumnName("Status").IsRequired();
            Property(e => e.Password).HasColumnName("Password").IsRequired();
            Property(e => e.FacebookId).HasColumnName("FacebookId");
            Property(e => e.LinkedinId).HasColumnName("LinkedinId");
            Ignore(e => e.PasswordPlain);
            Ignore(e => e.LastChange);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.ExceptedPermissions).WithRequired(e=>e.User).HasForeignKey(e=>e.UserId);
            HasMany(e => e.Devices).WithRequired(e => e.User).HasForeignKey(e => e.UserId);
            HasMany(e => e.Profiles)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("UserId");
                    cs.MapRightKey("ProfileId");
                    cs.ToTable("UserProfile");
                });
        }
        protected override void MapTable()
        {
        }
    }
}
