﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class InsuredMap : Map<Insured>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Person.PersonType).HasValue("Insured"));
            Property(e => e.AcceptedTerms).HasColumnName("AcceptedTerms").IsOptional();
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.Declineds);
            HasMany(e => e.Policies);
        }
        protected override void MapTable()
        {
        }
    }
}
