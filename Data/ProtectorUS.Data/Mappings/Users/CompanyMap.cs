﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CompanyMap : AuditableEntityMap<Company,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(100);
        }
        protected override void MapRelationships()
        {
            HasMany(e => e.ManagerUsers);
        }
        protected override void MapTable()
        {
            ToTable("Company");
        }
    }
}
