﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CampaignMap : AuditableEntityMap<Campaign,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.WebpageProduct).WithMany(x => x.Campaigns).HasForeignKey(x => x.WebpageProductId);
            HasRequired(x => x.Broker).WithMany(x => x.Campaigns).HasForeignKey(x => x.BrokerId);
        }
        protected override void MapTable()
        {
            ToTable("Campaign");
        }
    }
}
