﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class BrochureMap : AuditableEntityMap<Brochure, long>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Campaign.CampaignType).HasValue("Brochure"));
            Property(e => e.FilePath).HasColumnName("FilePath").IsRequired();
            Property(e => e.TemplateType).HasColumnName("TemplateType").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
        }
    }
}
