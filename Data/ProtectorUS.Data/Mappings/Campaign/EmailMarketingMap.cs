﻿namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class EmailMarketingMap : AuditableEntityMap<EmailMarketing,long>
    {
        protected override void MapKey()
        {
        }
        protected override void MapProperties()
        {
            Map(e => e.Requires(Campaign.CampaignType).HasValue("EmailMarketing"));
            Property(e => e.CoverId).HasColumnName("CoverId").IsRequired();
            Property(e => e.CoverPath).HasColumnName("CoverPath").IsRequired();
            Property(e => e.Title).HasColumnName("Title").IsRequired();
            Property(e => e.Body).HasColumnName("Body").IsRequired();
            Property(e => e.ButtonText).HasColumnName("ButtonText").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
        }
    }
}
