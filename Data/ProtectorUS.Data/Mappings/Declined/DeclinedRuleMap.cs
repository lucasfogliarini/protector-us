﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    /// <summary>
    /// <see cref="DeclinedRule"/> entity mapper.
    /// </summary>
    public class DeclinedRuleMap : AuditableEntityMap<DeclinedRule, long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.PropetyName).HasColumnName("PropetyName").IsRequired();
            Property(e => e.Operator).HasColumnName("Operator").IsRequired();
            Property(e => e.TargetValue).HasColumnName("TargetValue").IsRequired();
            Property(e => e.ContractType).HasColumnName("ContractType").IsRequired();
            Property(e => e.Description).HasColumnName("Description").IsRequired();
            Property(e => e.Reason).HasColumnName("Reason").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Product).WithMany().HasForeignKey(x => x.ProductId);
        }
        protected override void MapTable()
        {
        }
    }
}
