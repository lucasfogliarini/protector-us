﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    /// <summary>
    /// <see cref="DeclinedUnblock"/> entity mapper.
    /// </summary>
    public class DeclinedUnblockMap : AuditableEntityMap<DeclinedUnblock,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(e => e.UnblockReason).HasColumnName("UnblockReason").IsRequired();
            Property(e => e.Date).HasColumnName("Date").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.ManagerUser).WithMany().HasForeignKey(x => x.ManagerUserId);
            HasRequired(e => e.DeclinedBlock).WithOptional(e => e.DeclinedUnblock);
        }
        protected override void MapTable()
        {
            ToTable("DeclinedUnblock");
        }
    }
}
