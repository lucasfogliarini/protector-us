﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="Declined"/> entity mapper.
    /// </summary>
    public class DeclinedMap : AuditableEntityMap<Declined,long>
    {
        protected override void MapKey()
        {
            HasKey(eblock => eblock.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.ContractType).HasColumnName("ContractType").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Insured).WithMany(x => x.Declineds).HasForeignKey(x => x.InsuredId);
            HasRequired(x => x.Product).WithMany(x => x.Declineds).HasForeignKey(x => x.ProductId);
            HasMany(e => e.DeclinedBlocks);
        }
        protected override void MapTable()
        {
            ToTable("Declined");
        }
    }
}
