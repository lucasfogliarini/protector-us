﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="DeclinedBlock"/> entity mapper.
    /// </summary>
    public class DeclinedBlockMap : AuditableEntityMap<DeclinedBlock,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(e => e.BlockReason).HasColumnName("BlockReason").IsRequired();
            Property(e => e.Date).HasColumnName("Date").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Declined).WithMany(e=>e.DeclinedBlocks).HasForeignKey(e => e.DeclinedId);
            HasOptional(x => x.ManagerUser).WithMany().HasForeignKey(e=>e.ManagerUserId);
            HasMany(e => e.DeclinedRules)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("DeclinedBlockId");
                    cs.MapRightKey("DeclinedRuleId");
                    cs.ToTable("DeclinedBlockRule");
                });
        }
        protected override void MapTable()
        {
            ToTable("DeclinedBlock");
        }
    }
}
