﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CarrierMap : AuditableEntityMap<Carrier,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(2000);
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Address).WithMany().HasForeignKey(x => x.AddressId);
            HasMany(x => x.States);
        }
        protected override void MapTable()
        {
            ToTable("Carrier");
        }
    }
}
