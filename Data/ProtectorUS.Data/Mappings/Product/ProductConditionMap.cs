﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="ProductCondition"/> entity mapper.
    /// </summary>
    public class ProductConditionMap : AuditableEntityMap<ProductCondition,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Active).HasColumnName("Active").IsRequired();
            Property(e => e.Description).HasColumnName("Description");
            Property(e => e.FilePath).HasColumnName("FilePath").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Product).WithMany().HasForeignKey(x => x.ProductId);
            HasRequired(f => f.State).WithMany().HasForeignKey(f => f.StateId);
        }
        protected override void MapTable()
        {
            ToTable("ProductCondition");
        }
    }
}
