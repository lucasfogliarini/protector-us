﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class ProductMap : AuditableEntityMap<Product,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(e => e.Slogan).HasColumnName("Slogan").IsRequired().HasMaxLength(50);
            Property(e => e.Alias).HasColumnName("Alias").IsRequired().HasMaxLength(30);
            Property(e => e.Abbreviation).HasColumnName("Abbreviation").IsRequired().HasMaxLength(5);
            Property(e => e.Code).HasColumnName("Code").IsRequired();
            Property(e => e.Icon).HasColumnName("Icon");
            Property(e => e.Active).HasColumnName("Active").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasMany(x => x.ProductPaymentMethods);
            HasMany(x => x.Declineds);
            HasMany(e => e.BrokerageProducts).WithRequired(e => e.Product).HasForeignKey(e => e.ProductId);
        }
        protected override void MapTable()
        {
            ToTable("Product");
        }
    }
}
