﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;
    public class CoverageMap : AuditableEntityMap<Coverage,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.Title).HasColumnName("Title").IsRequired().HasMaxLength(50);
            Property(e => e.Key).HasColumnName("Key").IsRequired();
            Property(e => e.Description).HasColumnName("Description").IsRequired().HasMaxLength(2000);
            Property(e => e.DateStart).HasColumnName("DateStart").IsRequired();
            Property(e => e.DateEnd).HasColumnName("DateEnd").IsOptional();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.Product).WithMany().HasForeignKey(x => x.ProductId).WillCascadeOnDelete();
        }
        protected override void MapTable()
        {
            ToTable("Coverage");
        }
    }
}
