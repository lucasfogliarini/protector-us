﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectorUS.Data.Mappings
{
    using Model;

    /// <summary>
    /// <see cref="ProductPaymentMethod"/> entity mapper.
    /// </summary>
    public class ProductPaymentMethodMap : AuditableEntityMap<ProductPaymentMethod,long>
    {
        protected override void MapKey()
        {
            HasKey(e => e.Id);
        }
        protected override void MapProperties()
        {
            Property(e => e.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(e => e.TotalInstallments).HasColumnName("TotalInstallments").IsRequired();
        }
        protected override void MapRelationships()
        {
            HasRequired(x => x.PaymentMethod).WithMany().HasForeignKey(x => x.PaymentMethodId).WillCascadeOnDelete();
            HasRequired(x => x.Product).WithMany(x => x.ProductPaymentMethods).HasForeignKey(x => x.ProductId).WillCascadeOnDelete();
        }
        protected override void MapTable()
        {
            ToTable("ProductPaymentMethod");
        }
    }
}
