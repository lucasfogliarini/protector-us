﻿using ProtectorUS.Data.Migrations.Seeds;

namespace ProtectorUS.Data.Mappings
{
    public class SeedHistoryMap : Map<SeedHistory>
    {
        protected override void MapKey()
        {
            HasKey(e => e.SeedId);
        }
        protected override void MapProperties()
        {
            Property(e => e.SeedId).HasColumnName("SeedId").IsRequired();
            Property(e => e.ContextKey).HasColumnName("ContextKey").IsRequired();
            Property(e => e.RunDate).HasColumnName("RunDate").IsRequired();
        }
        protected override void MapRelationships()
        {
        }
        protected override void MapTable()
        {
            ToTable("__SeedHistory");
        }
    }
}
