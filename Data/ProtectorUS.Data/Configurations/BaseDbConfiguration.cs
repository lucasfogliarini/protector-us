﻿using EFCache;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;

namespace ProtectorUS.Data
{
    public class BaseDbConfiguration : DbConfiguration
    {
        public BaseDbConfiguration()
        {
            //var transactionHandler = new CacheTransactionHandler(new InMemoryCache());

            //AddInterceptor(transactionHandler);

            //var cachingPolicy = new CachingPolicy();

            //Loaded +=
            //  (sender, args) => args.ReplaceService<DbProviderServices>(
            //    (s, _) => new CachingProviderServices(s, transactionHandler, cachingPolicy));

            // Handling of Transaction Commit Failures https://msdn.microsoft.com/en-us/dn630221 and Connection Resiliency / Retry Logic Execution Strategies SqlAzureExecutionStrategy
            SetTransactionHandler(SqlProviderServices.ProviderInvariantName, () => new CommitFailureHandler());
            SetExecutionStrategy("System.Data.SqlClient", () => SuspendExecutionStrategy ? (IDbExecutionStrategy)new DefaultExecutionStrategy() : new SqlAzureExecutionStrategy(2, TimeSpan.FromSeconds(5)));
            AddInterceptor(new TransientFailureCausingCommandInterceptor());
        }

        public static bool SuspendExecutionStrategy
        {
            get { return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? false; }
            set { CallContext.LogicalSetData("SuspendExecutionStrategy", value); }
        }
    }
}
