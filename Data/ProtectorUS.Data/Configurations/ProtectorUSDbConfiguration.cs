﻿namespace ProtectorUS.Data
{
    public class ProtectorUSDbConfiguration : BaseDbConfiguration
    {
        public ProtectorUSDbConfiguration() : base()
        {
           SetDatabaseInitializer(new ProtectorUSMigratorInitializer());
           AddInterceptor(new SoftDeleteInterceptor());
        }
    }
}