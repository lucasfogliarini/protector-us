﻿using InteractivePreGeneratedViews;
using ProtectorUS.Configuration;
using System;
using ProtectorUS.Data.Migrations.Seeds;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Diagnostics;

namespace ProtectorUS.Data
{
    public abstract class DbMigratorInitializer<TMigrationsConfiguration, TDbContext> : IDatabaseInitializer<TDbContext> 
        where TDbContext: DbContext
        where TMigrationsConfiguration : DbMigrationsConfiguration<TDbContext>, new()
    {
        public void InitializeDatabase(TDbContext context)
        {
            DbContext = context;
            DbMigrationsConfiguration = new TMigrationsConfiguration();
            InteractiveViews.SetViewCacheFactory(
                context, new SqlServerViewCacheFactory(context.Database.Connection.ConnectionString));

            Environment = ProtectorUSConfiguration.GetConfig().Env;
            OnInitialization();
            DbMigrator migrator = new DbMigrator(DbMigrationsConfiguration);
            DbContext.Database.CommandTimeout = migrator.Configuration.CommandTimeout = DbContext.Database.Connection.ConnectionTimeout;
            migrator.Update();
            switch (Environment)
            {
                case ProtectorUSEnvironment.Development:
                    SeedDevelopment();
                    break;
                case ProtectorUSEnvironment.Test:
                    SeedTest();
                    break;
                case ProtectorUSEnvironment.Homolog:
                    SeedHomolog();
                    break;
                case ProtectorUSEnvironment.UAT:
                    SeedUAT();
                    break;
                case ProtectorUSEnvironment.Production:
                    SeedProduction();
                    break;
            }
            DbContext.SaveChanges();            
        }
        protected virtual void OnInitialization()
        {
        }
        public TMigrationsConfiguration DbMigrationsConfiguration { get; private set; }
        public TDbContext DbContext { get; private set; }
        protected ProtectorUSEnvironment Environment { get; set; }
        protected virtual void SeedTest() { }
        protected virtual void SeedDevelopment()
        {
            SeedHistory(nameof(ProtectorUSEnvironment.Homolog));
        }
        protected virtual void SeedHomolog()
        {
            SeedHistory(nameof(ProtectorUSEnvironment.Homolog));
        }
        protected virtual void SeedUAT()
        {
            SeedHistory(nameof(ProtectorUSEnvironment.Homolog));
        }
        protected virtual void SeedProduction()
        {
            SeedHistory(nameof(ProtectorUSEnvironment.Production));
        }
        private void SeedHistory(string environmentNameSpace)
        {
            var configurationType = DbMigrationsConfiguration.GetType();
            var seedTypes = configurationType.Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Seed)) && t.Namespace == $"{configurationType.Namespace}.Seeds.{environmentNameSpace}").OrderBy(t=>t.Name);
            foreach (var seedType in seedTypes)
            {
                Seed seed = Seed.CreateInstance(seedType, DbContext);
                var setSeedHistory = DbContext.Set<SeedHistory>();
                var seedHistory = new SeedHistory(seed);
                if (!setSeedHistory.Any(s => s.SeedId == seedHistory.SeedId))
                {
                    seed.Up();
                    DbContext.SaveChanges();
                    setSeedHistory.Add(seedHistory);
                    DbContext.SaveChanges();
                }
            }
        }
        public static double Measure(Action action, int repetitions, bool print = true)
        {
            long[] times = new long[repetitions];

            for (int i = 0; i < repetitions; i++)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();

                action();

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            /// Sort the elapsed times for all test runs
            Array.Sort(times);

            // Calculate the total times discarding
            // the 5% min and 5% max test times
            long totalTime = 0;
            int discardCount = (int)Math.Round(repetitions * 0.05);
            int count = repetitions - discardCount;
            for (int i = discardCount; i < count; i++)
            {
                totalTime += times[i];
            }

            double averageTime = ((double)totalTime) / (count - discardCount);

            if (print)
                Debug.WriteLine($"The action execute {repetitions} times and takes {averageTime} to complete");

            return averageTime;
        }
    }
}
