﻿using System;

namespace ProtectorUS.Data
{
    using Migrations;
    using Migrations.Seeds;
    using Model;
    using System.Collections.Generic;
    using System.Linq;

    public class ProtectorUSMigratorInitializer : DbMigratorInitializer<Configuration, ProtectorUSDatabase>
    {
        BaseSeed baseSeed;
        protected override void OnInitialization()
        {
            baseSeed = new BaseSeed(DbContext);
        }
        protected override void SeedTest()
        {
            DbContext.Clear();
            baseSeed.SeedCountries();
            baseSeed.SeedStates();
            baseSeed.SeedCancelReasons();
            baseSeed.SeedProducts();
            baseSeed.SeedCoverages();
            baseSeed.SeedPaymentMethods();
            baseSeed.SeedCompanies();
            baseSeed.SeedProfilesAndPermissions();
            baseSeed.SeedAppClients();

            SeedCitiesTest();
            SeedRegionsTest();
            SeedAddressesTest();
            SeedBrokeragesTest();
            SeedProductConditionsTest();
            SeedUsersTest();
            SeedRatingPlanFormsTest();
            SeedQuotationsTest();
        }

        #region SeedsTest

        private void SeedCitiesTest()
        {
            DbContext.Set<City>().AddRange(
                new City[]
        {
               new City() { Id = 1, Name = "Athens", StateId = 1 },
               new City() { Id = 2, Name = "Nova York", StateId = 32 },
               new City() { Id = 3, Name = "Buffalo", StateId = 32 },
               new City() { Id = 4, Name = "Utica", StateId = 32 },
               new City() { Id = 5, Name = "Yonkers", StateId = 32 },
               new City() { Id = 6, Name = "Albany", StateId = 32 }
                }               
            );
        }
        private void SeedRegionsTest()
        {
            DbContext.Set<Region>().AddRange(
                new Region[]
        {
                new Region() { Id = 1, ZipCode = "35611", CityId = 1 },
                new Region() { Id = 2, ZipCode = "35614", CityId = 1 },
                new Region() { Id = 3, ZipCode = "35613", CityId = 1 },
                new Region() { Id = 4, ZipCode = "10001", CityId = 2 },
                new Region() { Id = 5, ZipCode = "10002", CityId = 2 },
                new Region() { Id = 6, ZipCode = "10003", CityId = 2 },
                new Region() { Id = 7, ZipCode = "10004", CityId = 2 },
                new Region() { Id = 8, ZipCode = "10005", CityId = 2 },
                new Region() { Id = 9, ZipCode = "10006", CityId = 2 },
                new Region() { Id = 10, ZipCode = "14219", CityId = 3 },
                new Region() { Id = 11, ZipCode = "13501", CityId = 4 },
                new Region() { Id = 12, ZipCode = "10701", CityId = 5 },
                new Region() { Id = 13, ZipCode = "12201", CityId = 6 }
                }
            );
            DbContext.SaveChanges();
        }
        private void SeedAddressesTest()
        {
            DbContext.Set<Address>().AddRange(
                new Address[]
                {
                       new Address() { Id = 1, StreetLine1 = "17158 Blackburn Rd", RegionId = 1, Latitude = 34.8087872d, Longitude = -87.0538833d },
                       new Address() { Id = 2, StreetLine1 = "24828 Stinnett Hollow Rd", RegionId = 2, Latitude = 34.908246d, Longitude = -87.1741235d },
                       new Address() { Id = 3, StreetLine1 = "22469 Nick Davis Rd", RegionId = 3, Latitude = 34.79902d, Longitude = -86.9157444d },
                       new Address() { Id = 4, StreetLine1 = "539 W 28th St", RegionId = 4, Latitude = 40.751738d, Longitude = -74.0056217d },
                       new Address() { Id = 5, StreetLine1 = "49 Ludlow St", RegionId = 5, Latitude = 40.7165125d, Longitude = -73.9913285d },
                       new Address() { Id = 6, StreetLine1 = "135 E 16th St", RegionId = 6, Latitude = 40.734997d, Longitude = -73.9890333d },
                       new Address() { Id = 7, StreetLine1 = "11 Stone St", RegionId = 7, Latitude = 40.7041706d, Longitude = -74.0144558d },
                       new Address() { Id = 8, StreetLine1 = "150 Water St", RegionId = 8, Latitude = 40.7064327d, Longitude = -74.0082478d },
                       new Address() { Id = 9, StreetLine1 = "5 Albany St", RegionId = 9, Latitude = 40.709823d, Longitude = -74.0154817d },
                       new Address() { Id = 10, StreetLine1 = "9 Waverly Pl", RegionId = 6, Latitude = 40.7299926d, Longitude = -73.9962606d },
                       new Address() { Id = 11, StreetLine1 = "28 Little W St", RegionId = 7, Latitude = 40.7060596d, Longitude = -74.0193974d },
                       new Address() { Id = 12, StreetLine1 = "25 Nassau St", RegionId = 8, Latitude = 40.7080458d, Longitude = -74.0120924d },
                       new Address() { Id = 13, StreetLine1 = "126 W 30th St", RegionId = 4, Latitude = 40.7475378d, Longitude = -73.9929922d },
                       new Address() { Id = 14, StreetLine1 = "67 Allen St", RegionId = 10, Latitude = 42.7917448d, Longitude = -78.8290981d },
                       new Address() { Id = 15, StreetLine1 = "310 Rutger St", RegionId = 11, Latitude = 43.0960639d, Longitude = -75.2306967d },
                       new Address() { Id = 16, StreetLine1 = "123 Elm St", RegionId = 12, Latitude = 40.9338077d, Longitude = -73.890879d },
                       new Address() { Id = 17, StreetLine1 = "S Mall Arterial", RegionId = 13, Latitude = 42.6502948d, Longitude = -73.760872d },
                       new Address() { Id = 18, StreetLine1 = "100 Telfer St", RegionId = 11, Latitude = 42.7845639d, Longitude = -78.828622d },
                       new Address() { Id = 19, StreetLine1 = "207 W 25th St", RegionId = 4, Latitude = 40.745888d, Longitude = -73.9974232d },
                       new Address() { Id = 20, StreetLine1 = "11033 Big Spring Ln", RegionId = 1, Latitude = 34.7662876d, Longitude = -87.1262121d },
                       new Address() { Id = 21, StreetLine1 = "7 Albany St", RegionId = 9, Latitude = 40.709823d, Longitude = -74.0154817d },
                       new Address() { Id = 22, StreetLine1 = "13 Waverly Pl", RegionId = 6, Latitude = 40.7299926d, Longitude = -73.9962606d },
                       new Address() { Id = 23, StreetLine1 = "30 Little W St", RegionId = 7, Latitude = 40.7060596d, Longitude = -74.0193974d },
                       new Address() { Id = 24, StreetLine1 = "22 Nassau St", RegionId = 8, Latitude = 40.7080458d, Longitude = -74.0120924d },
                       new Address() { Id = 25, StreetLine1 = "120 W 30th St", RegionId = 4, Latitude = 40.7475378d, Longitude = -73.9929922d },
                       new Address() { Id = 26, StreetLine1 = "65 Allen St", RegionId = 10, Latitude = 42.7917448d, Longitude = -78.8290981d },
                       new Address() { Id = 27, StreetLine1 = "308 Rutger St", RegionId = 11, Latitude = 43.0960639d, Longitude = -75.2306967d },
                       new Address() { Id = 28, StreetLine1 = "22474 Nick Davis Rd", RegionId = 3, Latitude = 34.79902d, Longitude = -86.9157444d },
                       new Address() { Id = 29, StreetLine1 = "537 W 28th St", RegionId = 4, Latitude = 40.751738d, Longitude = -74.0056217d },
                       new Address() { Id = 30, StreetLine1 = "44 Ludlow St", RegionId = 5, Latitude = 40.7165125d, Longitude = -73.9913285d }
                }
            );
        }
        private void SeedBrokeragesTest()
            {
            #region Brokerage1
            var brokerage1 = new Brokerage()
            {
                Id = 1,
                Name = "Brokerage1",
                Alias = "brokerage1",
                Email = "master@brokerage1.com",
                Logo = "assets/images/broker-logo-teste1.png",
                Cover = "assets/images/broker-cover-teste1.png",
                Website = "www.brokerage1.com.br",
                AddressId = 1,
                Status = BrokerageStatus.Active,
                Phones = new List<Phone>()
                {
                    new Phone()
                    {
                        Id = 1,
                        Description = "",
                        Number = "8888-5885",
                        Type = PhoneType.Commercial
                    }
                },
                BrokerageProducts = new List<BrokerageProduct>()
                {
                    new BrokerageProduct()
                    {
                        ActiveARGO = true,
                        ProductId = 1,
                        WebpageProducts = new List<WebpageProduct>()
                        {
                            new WebpageProduct() { Active = true, Alias = "", BrokerageComission = 10, Default = true }
                        }
                    },
                    new BrokerageProduct()
                    {
                        ActiveARGO = true,
                        ProductId = 2,
                        WebpageProducts = new List<WebpageProduct>()
                        {
                            new WebpageProduct() { Active = true, Alias = "", BrokerageComission = 20, Default = true }
                        }
                    }
                }
            };
            #endregion
            #region Brokerage2
            var brokerage2 = new Brokerage()
            {
                Id = 2,
                Name = "Brokerage2",
                Alias = "brokerage2",
                Email = "master@brokerage2.com",
                Logo = "assets/images/broker-logo-teste2.png",
                Cover = "assets/images/broker-cover-teste2.png",
                Website = "www.brokerage2.com.br",
                AddressId = 2,
                Status = BrokerageStatus.Active,
                Phones = new List<Phone>()
                {
                    new Phone()
                    {
                        Id = 2,
                        Description = "",
                        Number = "6666-4485",
                        Type =PhoneType.Commercial
                    }
                },
                BrokerageProducts = new List<BrokerageProduct>()
                {
                    new BrokerageProduct()
                    {
                        ActiveARGO = true,
                        ProductId = 1,
                        WebpageProducts = new List<WebpageProduct>()
                        {
                            new WebpageProduct() { Active = true, Alias = "", BrokerageComission = 30, Default = true }
                        }
                    }
                }
            };
            #endregion
            #region Brokerage3
            var brokerage3 = new Brokerage()
            {
                Id = 3,
                Name = "Brokerage3",
                Alias = "brokerage3",
                Email = "master@brokerage3.com",
                Logo = "assets/images/broker-logo-teste3.png",
                Cover = "assets/images/broker-cover-teste3.png",
                Website = "www.brokerage3.com.br",
                AddressId = 3,
                Status = BrokerageStatus.Active,
                Phones = new List<Phone>()
                {
                    new Phone()
                    {
                        Id = 3,
                        Description = "",
                        Number = "9444-4335",
                        Type =PhoneType.Commercial
                    }
                },
                BrokerageProducts = new List<BrokerageProduct>()
                {
                    new BrokerageProduct()
                    {
                        ActiveARGO = true,
                        ProductId = 2,
                        WebpageProducts = new List<WebpageProduct>()
                        {
                            new WebpageProduct() { Active = true, Alias = "", BrokerageComission = 40, Default = true }
                        }
                    }
                }
            };
            #endregion
            #region Brokerage4
            var brokerage4 = new Brokerage()
            {
                Id = 4,
                Name = "Brokerage4",
                Alias = "brokerage4",
                Email = "master@brokerage4.com",
                Logo = "assets/images/broker-logo-teste4.png",
                Cover = "assets/images/broker-cover-teste4.png",
                Website = "www.brokerage4.com.br",
                AddressId = 4,
                Status = BrokerageStatus.Active,
                Phones = new List<Phone>()
                {
                    new Phone()
                    {
                        Id = 4,
                        Description = "",
                        Number = "5655-4335",
                        Type =PhoneType.Commercial
                    }
                },
                BrokerageProducts = new List<BrokerageProduct>()
                {
                    new BrokerageProduct()
                    {
                        ActiveARGO = true,
                        ProductId = 2,
                        WebpageProducts = new List<WebpageProduct>()
                        {
                            new WebpageProduct() { Active = true, Alias = "", BrokerageComission = 40, Default = true }
                        }
                    }
                }
            };
            #endregion

            DbContext.Set<Brokerage>().AddRange(
                new Brokerage[]
                {
               brokerage1,
               brokerage2,
               brokerage3,
               brokerage4
                }
            );
            DbContext.SaveChanges();
        }

        long brokerFirstId;
        long brokerLastId;
        long insuredFirstId;
        long insuredLastId;
        private void SeedUsersTest()
        {
            var profileDbSet = DbContext.Set<Profile>();
            long currentUserId = 0;
            #region manager users
            var managerUsers = new List<ManagerUser>();
            string[] firstnames = { "John", "Marcos", "Daniel", "Gustavo", "Pedro", "Brad", "Wilson", "Leonardo", "Augusto", "Valderama", "Bob", "Julio", "Kahue", "Guilherme", "Adair", "Vitor", "Roney", "Anderson", "Diego", "Wagner", "Justin", "Alison" };
            string[] lastnames = { "Travolta", "Benetton", "Smith", "Aguiar", "Polem", "Lion", "Will", "De Narnia", "Polonio", "Brandiling", "Burnquist", "do Monte", "Costa", "Alvarenga", "Linversar", "Belford", "Bonamir", "Spider", "Dalagnol", "Moura", "Bieber", "Oliveira" };

            for (int i = 1; i <= Profile.Ids.ManagerUserProfiles.Length; i++)
            {
                var profile = profileDbSet.Find(i);
                currentUserId++;
                var profileName = profile.Name.Replace("ARGO", "").Simplify();
                var manageruser = new ManagerUser($"{profileName}@manageruser.com", "manageruser")
                {
                    Id = currentUserId,
                    FirstName = firstnames[i-1],
                    LastName = lastnames[i-1],
                    Status = UserStatus.Active,
                    CompanyId = 1,
                    Profiles = new List<Profile>()
                    {
                        profileDbSet.Find(profile.Id)
                    }
                };
                managerUsers.Add(manageruser);
            }
            DbContext.Set<ManagerUser>().AddRange(
               managerUsers.ToArray()
            );
            #endregion

            #region brokers
            var brokers = new List<Broker>();
            long firstBrokerProfileId = Profile.Ids.BrokerProfiles[0];
            long lastBrokerProfileId = Profile.Ids.BrokerProfiles.Last();
            long brokerageId = 1;
            brokerFirstId = currentUserId + 1;
            string[] firstnames2 = { "Bob", "Julio", "Kahue", "Guilherme", "Adair", "Vitor", "Roney", "Anderson", "Diego", "Wagner", "Justin", "Alison", "Wilson", "Leonardo", "Augusto", };
            string[] lastnames2 = { "Burnquist", "do Monte", "Costa", "Alvarenga", "Linversar", "Belford", "Bonamir", "Spider", "Dalagnol", "Moura", "Bieber", "Oliveira","Brandiling", "Burnquist", "do Monte", };

            for (int i = 1; i <= 12; i++)
            {
                if (firstBrokerProfileId > lastBrokerProfileId)
                {
                    firstBrokerProfileId = Profile.Ids.BrokerProfiles[0];
                    brokerageId++;
                }
                var profile = profileDbSet.Find(firstBrokerProfileId);
                currentUserId++;
                var profileName = profile.Name.Simplify();
                var email = firstnames2[i - 1] == "Kahue" ? "kahue@trin.ca" : $"{profileName}@brokerage{brokerageId}.com";
                var status = firstnames2[i - 1] == "Kahue" ? UserStatus.Onboarding : UserStatus.Active;
                var broker = new Broker(email, "broker")
                {
                    Id = currentUserId,
                    Status = status,
                    BrokerageId = brokerageId,
                    FirstName = firstnames2[i-1],
                    LastName = lastnames2[i-1],
                    Profiles = new List<Profile>()
                    {
                        profileDbSet.Find(profile.Id)
                    }
                };
                brokers.Add(broker);
                firstBrokerProfileId++;
            }
            brokerLastId = currentUserId;

            DbContext.Set<Broker>().AddRange(
               brokers.ToArray()
            );

            #endregion

            #region insureds
            currentUserId++;
            insuredFirstId = currentUserId;
            #region insured1
            var insured = new Insured("insured1@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Tom",
                LastName = "Hanks",
                BirthDate = DateTime.Now.AddYears(-40),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 14, Type = PhoneType.Commercial, Number = "1-800-706-1234", Description = "" } },
                Picture = "Upload/pictures/insureds/insured02.jpg",
                Addresses = new List<Address>() { new Address() { Id = 31, StreetLine1 = "535 W 28th St", RegionId = 4, Latitude = 40.751738d, Longitude = -74.0056217d } },
                SocialTitle = SocialTitle.Mister
            };

            insured.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured2
            currentUserId++;
            var insured2 = new Insured("insured2@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Paul",
                LastName = "Jhonson",
                BirthDate = DateTime.Now.AddYears(-45),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 5, Type = PhoneType.Residential, Number = "1-443-706-1234", Description = "" } },
                Picture = "Upload/pictures/insureds/insured04.jpg",
                Addresses = new List<Address>() { new Address() { Id = 32, StreetLine1 = "89 Ludlow St", RegionId = 5, Latitude = 40.7165125d, Longitude = -73.9913285d } },
                SocialTitle = SocialTitle.Mister
            };

            insured2.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured3
            currentUserId++;
            var insured3 = new Insured("insured3@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Raul",
                LastName = "Ganzol",
                BirthDate = DateTime.Now.AddYears(-29),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 6, Type = PhoneType.Residential, Number = "1-446-585-1554", Description = "" } },
                Picture = "Upload/pictures/insureds/insured05.jpg",
                Addresses = new List<Address>() { new Address() { Id = 33, StreetLine1 = "132 E 16th St", RegionId = 6, Latitude = 40.734997d, Longitude = -73.9890333d } },
                SocialTitle = SocialTitle.Mister
            };

            insured3.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured4
            currentUserId++;
            var insured4 = new Insured("insured4@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "John",
                LastName = "Zika",
                BirthDate = DateTime.Now.AddYears(-59),
                Status = UserStatus.Inactive,
                Phones = new List<Phone>() { new Phone { Id = 7, Type = PhoneType.Residential, Number = "1-433-222-1114", Description = "" } },
                Picture = "Upload/pictures/insureds/insured06.jpg",
                Addresses = new List<Address>() { new Address() { Id = 34, StreetLine1 = "15 Stone St", RegionId = 7, Latitude = 40.7041706d, Longitude = -74.0144558d } },
                SocialTitle = SocialTitle.Mister
            };

            insured4.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured5
            currentUserId++;
            var insured5 = new Insured("insured5@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Lebron",
                LastName = "James",
                BirthDate = DateTime.Now.AddYears(-39),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 8, Type = PhoneType.Cell, Number = "1-888-552-1334", Description = "" } },
                Picture = "Upload/pictures/insureds/insured07.jpg",
                Addresses = new List<Address>() { new Address() { Id = 35, StreetLine1 = "155 Water St", RegionId = 8, Latitude = 40.7064327d, Longitude = -74.0082478d } },
                SocialTitle = SocialTitle.Mister
            };

            insured5.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured6
            currentUserId++;
            var insured6 = new Insured("insured6@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Michel",
                LastName = "Jackson",
                BirthDate = DateTime.Now.AddYears(-27),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 9, Type = PhoneType.Cell, Number = "1-888-552-1334", Description = "" } },
                Picture = "Upload/pictures/insureds/insured08.jpg",
                Addresses = new List<Address>() { new Address() { Id = 36, StreetLine1 = "152 Water St", RegionId = 8, Latitude = 40.7064327d, Longitude = -74.0082478d } },
                SocialTitle = SocialTitle.Mister
            };

            insured6.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured7
            currentUserId++;
            var insured7 = new Insured("insured7@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Amy",
                LastName = "Winehouse",
                BirthDate = DateTime.Now.AddYears(-27),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 10, Type = PhoneType.Residential, Number = "1-338-223-1334", Description = "" } },
                Picture = "Upload/pictures/insureds/insured09.jpg",
                Addresses = new List<Address>() { new Address() { Id = 37, StreetLine1 = "134 Water St", RegionId = 8, Latitude = 40.7064327d, Longitude = -74.0082478d } },
                SocialTitle = SocialTitle.Miss
            };

            insured7.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured8
            currentUserId++;
            var insured8 = new Insured("insured8@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Chapolin",
                LastName = "Colorado",
                BirthDate = DateTime.Now.AddYears(-22),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 11, Type = PhoneType.Cell, Number = "1-889-996-6775", Description = "" } },
                Picture = "Upload/pictures/insureds/insured10.jpg",
                Addresses = new List<Address>() { new Address() { Id = 38, StreetLine1 = "25 Little W St", RegionId = 7, Latitude = 40.7060596d, Longitude = -74.0193974d } },
                SocialTitle = SocialTitle.Miss
            };

            insured8.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured9
            currentUserId++;
            var insured9 = new Insured("insured9@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Elvis",
                LastName = "Presley",
                BirthDate = DateTime.Now.AddYears(-67),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 12, Type = PhoneType.Cell, Number = "1-889-996-6466", Description = "" } },
                Picture = "Upload/pictures/insureds/insured11.jpg",
                Addresses = new List<Address>() { new Address() { Id = 39, StreetLine1 = "29 Little W St", RegionId = 7, Latitude = 40.7060596d, Longitude = -74.0193974d } },
                SocialTitle = SocialTitle.Mister
            };

            insured9.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            #region insured10
            currentUserId++;
            var insured10 = new Insured("insured10@insured.com", "insured")
            {
                Id = currentUserId,
                FirstName = "Lady",
                LastName = "Gaga",
                BirthDate = DateTime.Now.AddYears(-34),
                Status = UserStatus.Active,
                Phones = new List<Phone>() { new Phone { Id = 13, Type = PhoneType.Cell, Number = "1-995-342-6466", Description = "" } },
                Picture = "Upload/pictures/insureds/insured12.jpg",
                Addresses = new List<Address>() { new Address() { Id = 40, StreetLine1 = "124 W 30th St", RegionId = 4, Latitude = 40.7475378d, Longitude = -73.9929922d } },
                SocialTitle = SocialTitle.Mister
            };

            insured10.Profiles = new List<Profile>()
            {
                profileDbSet.Find(Profile.Ids.Insured)
            };
            #endregion
            insuredLastId = currentUserId;
            DbContext.Set<Insured>().AddRange(
                new Insured[]
                {
               insured,
               insured2,
               insured3,
               insured4,
               insured5,
               insured6,
               insured7,
               insured8,
               insured9,
               insured10
                }               
            );
            #endregion

            #region ExceptedPermissions
            
            DbContext.Set<ExceptedPermission>().AddRange(
                new ExceptedPermission[]
                {
               new ExceptedPermission() { Id = 10, Allowed = false, UserId = 3 },
               new ExceptedPermission() { Id = 4, Allowed = true, UserId = 2 }
                }
            );
            #endregion

        }
        private void SeedRatingPlanFormsTest()
        {
            var ratingPlanForm = new RatingPlanForm()
            {
                StateId = 1,
                ProductId = 1,
            };
            ratingPlanForm.AddNewVersion(1);
            DbContext.Set<RatingPlanForm>().Add(
               ratingPlanForm
            );
        }
        private void SeedQuotationsTest()
        {
            DbContext.SaveChanges();
            #region RiskAnalysis Fields
            List<RiskAnalysisField> riskAnalysisFields = new List<RiskAnalysisField>()
            {
                new RiskAnalysisField("revenue", "110000"),
                new RiskAnalysisField("existingPolicyStartDate","01/25/2015"),
                new RiskAnalysisField("coverageLimit", "230000"),

                new RiskAnalysisField("existingPolicy", "dont"),
                new RiskAnalysisField("priorActsIssuer", "wasnot"),
                new RiskAnalysisField("priorInsuranceProvider", "priorInsuranceProvider1"),
                new RiskAnalysisField("coverage", "100000", 1),
                new RiskAnalysisField("startingMonth", "9"),
                new RiskAnalysisField("startingDay", "25"),
                new RiskAnalysisField("engagementLetters", "atleast",1),
                new RiskAnalysisField("claimsLastFive", "ihadtwo", 2),
                new RiskAnalysisField("totalClaims", "0"),
                new RiskAnalysisField("exposure", "ihave", 1),
            };
            #endregion
            decimal total = 2575.040m;
            List<Quotation> quotations = new List<Quotation>();
            var random = new Random();
            for (int i = 1; i <= 10; i++)
            {
                string insured = "insured" + i;
                Guid hash;
                switch (i)
                {
                    case 1:
                        hash = Guid.Parse("b2d5116b-2fb7-481d-8a6f-26ef632a36e1");
                        break;
                    case 2:
                        hash = Guid.Parse("96F9CBC1-FF84-45D0-8419-3E2EC98E3E93");
                        break;
                    default:
                        hash = Guid.NewGuid();
                        break;
                }
                var quotation = new Quotation()
                {
                    Id = i,
                    RatingPlanFormVersionId = 1,
                    PolicyStartDate = DateTime.Now.AddDays(1),
                    InsuredId = insuredFirstId + i - 1,
                    BrokerId = random.Next((int)brokerFirstId, (int)brokerLastId),
                    Business = new Business() { Name = "Business" + 1, Type = BusinessType.Firm, AddressId = i },
                    RegionId = i,
                    BrokerageProductId = 1,
                    ExpirationDate = DateTime.Now.AddDays(15),
                    ProponentEmail = $"{insured}@proponent.com",
                    ProponentSocialTitle = SocialTitle.Miss,
                    ProponentFirstName = insured,
                    ProponentLastName = "LastName " + insured,
                    Hash = hash,
                    TaxAmount = random.Next(0, 50),
                    FeeAmount = random.Next(0, 50),
                    PolicyPremium = total + i
                };
                quotation.SetRiskAnalysis(new RiskAnalysis(riskAnalysisFields));
                quotations.Add(quotation);
            }

            DbContext.Set<Quotation>().AddRange(
               quotations.ToArray()
            );
        }
        private void SeedProductConditionsTest()
        {
            DbContext.Set<ProductCondition>().AddRange(
                new ProductCondition[]
                {
                    new ProductCondition() { Id = 1, ProductId = 1, StateId = 1, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 2, ProductId = 1, StateId = 32, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 3, ProductId = 1, StateId = 43, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 4, ProductId = 1, StateId = 20, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 5, ProductId = 1, StateId = 21, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 6, ProductId = 1, StateId = 11, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 7, ProductId = 1, StateId = 12, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 8, ProductId = 2, StateId = 1, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 9, ProductId = 2, StateId = 32, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 10, ProductId = 2, StateId = 43, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 11, ProductId = 2, StateId = 20, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 12, ProductId = 2, StateId = 21, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 13, ProductId = 2, StateId = 11, Active = true, FilePath = "ConditionsAlabama.pdf" },
                    new ProductCondition() { Id = 14, ProductId = 2, StateId = 12, Active = true, FilePath = "ConditionsAlabama.pdf" }
                }               
            );
        }
        private void SeedQuotationsDeliveryTest()
        {
            List<QuotationDelivery> quotationsDelivery = new List<QuotationDelivery>();
            var random = new Random();
            for (int i = 1; i <= 100; i++)
            {
                string insured = "insured" + i;
                int quotationId = random.Next(1, 10);
                quotationsDelivery.Add(new QuotationDelivery()
                {
                    Id = i,
                    Date = DateTime.Now,
                    QuotationId = quotationId,
                    Email = "sendmail" + i + "@gmail.com",
                    Type = QuotationDeliveryType.Mail
                });
            }
            DbContext.Set<QuotationDelivery>().AddRange(
               quotationsDelivery.ToArray()
            );
        }
        private void SeedPoliciesTest()
        {
            //Where Condition 1 to 7 product 1, 8 to 14 product 2.            
            var AEcondition = DbContext.Set<ProductCondition>().Include("Product").FirstOrDefault(pc=> pc.Product.Id == 1 && pc.Active);
            var APLcondition = DbContext.Set<ProductCondition>().Include("Product").FirstOrDefault(pc => pc.Product.Id == 2 && pc.Active);

            var random = new Random();
            var policies = new List<Policy>();
            int sequential = PolicyNumber.sequentialMin;
            for (int i = 1; i <= 50; i++)
            {
                int idInsured = random.Next((int)insuredFirstId, (int)insuredLastId);
                int days = -1 * i;
                bool pair = random.Next(1,2) == 1;
                int factor = i * 15;
                var condition = pair ? AEcondition : APLcondition;
                PolicyNumber policyNumber = new PolicyNumber(pair ? 1 : 2, condition.Product.Code, sequential++);
                var policy = new Policy(policyNumber, 0)
                {
                    Id = i,
                    Condition = condition,
                    CreatedDate = DateTime.Now.AddDays(days),
                    PeriodStart = DateTime.Now.AddDays(days),
                    PeriodEnd = DateTime.Now.AddDays(days).AddYears(1),
                    BusinessId = idInsured-18,
                    BrokerId = random.Next((int)brokerFirstId,(int)brokerLastId),
                    InsuredId = idInsured,
                    TotalPremium = factor + 3220.56m,
                };
                policies.Add(policy);
            }

            var orders = new List<Order>();
            foreach (var policy in policies)
            {
                orders.Add(new Order()
                {
                    Id = policy.Id,
                    QuotationId = random.Next(1, 10),
                    TransactionCode = "STRIPE8373663" + policy.Id,
                    PaymentStatus = PaymentStatus.Approved,
                    TotalInstallments = 1,
                    PaymentMethodId = 2,
                    OrderDate = policy.CreatedDate,
                    Policy = policy
                });
            }
            var policy1 = policies.FirstOrDefault();
            orders.Add(new Order()
            {
                Id = orders.Count + 1,
                QuotationId = random.Next(1, 10),
                TransactionCode = "STRIPE83736630",
                PaymentStatus = PaymentStatus.Denied,
                TotalInstallments = 1,
                PaymentMethodId = 2,
                OrderDate = DateTime.Now.AddDays(-1),
                Policy = policy1
            });

            DbContext.Set<Order>().AddRange(
                orders.ToArray()
            );
        }
        private void SeedEndorsementsTest()
        {
            DbContext.Set<Endorsement>().Add(
               new Endorsement(EndorsementType.Cancellation, 1, insuredFirstId)
               {
                   Id = 1,
                   PolicyId = 1,
               }
            );
        }
        private void SeedClaimsTest()
        {
            List<Claim> claims = new List<Claim>();
            var random = new Random();
            int sequentialNumber = Claim.MinValue;
            for (int i = 1; i <= 15; i++)
            {
                var factor = i * 10;
                var claim = new Claim()
                {
                    Id = i,
                    PolicyId = i,
                    ClaimDate = DateTime.Now.AddMonths(-1 * i),
                    ThirdParty = new ThirdParty() { Email = $"thirdparty{i}@gmail.com", Name = "Third Party" + i, Phone = $"51 999 99 999{i}" },
                    DescriptionFacts = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi consectetur, ex ut blandit efficitur, lorem odio pretium lectus, sed tempus lacus ante eu turpis. Sed finibus viverra vestibulum. Nullam neque tortor, porttitor id fermentum vitae, semper eget nunc. Vivamus in nulla turpis. In in auctor augue. Proin et nisi mi. Quisque congue congue sapien, eget convallis velit ornare at. Duis consectetur condimentum turpis at dapibus. Phasellus et orci nulla.",
                    PriorNotification = true,
                    TotalPaid = factor + 1502.55M,
                    ALAEReserve = factor + 10000.00M,
                    LossReserve = factor + 1000.00M,
                    OccurrenceDate = DateTime.Now.AddMonths(-1 * i),
                    Status = (ClaimStatus)random.Next(0, 3)
                };
                claim.Numerate(sequentialNumber++);
                claims.Add(claim);
            }

            DbContext.Set<Model.Claim>().AddRange(
               claims.ToArray()
            );
        }
        private void SeedWebPageViewsTest()
        {
            List<WebpageView> views = new List<WebpageView>();
            var random = new Random();
            for (int i = 1; i <= 50; i++)
            {
                var webpagaproductid = random.Next(1, 3);
                var acesstypeid = random.Next(1, 3);
                long? campaignid = null;
                if (acesstypeid.Equals(AccessType.Campaign))
                    campaignid = random.Next(1, 2);
                var factor = i * 10;
                var view = new WebpageView()
                {
                    Id = i,
                    Access = (AccessType)acesstypeid,
                    WebpageProductId = webpagaproductid,
                    BrokerageId = 1,
                    CampaignId = campaignid,
                    CreatedDate = DateTime.Now.AddDays(- random.Next(1,80))
                };

                views.Add(view);
            }

            DbContext.Set<Model.WebpageView>().AddRange(
               views.ToArray()
            );
        }

        #endregion
    }
}
