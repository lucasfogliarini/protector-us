﻿namespace ProtectorUS.Data
{
    public class LogProtectorUSDbConfiguration : BaseDbConfiguration
    {
        public LogProtectorUSDbConfiguration() : base()
        {
            SetDatabaseInitializer(new LogProtectorUSMigratorInitializer());
        }
    }
}