namespace ProtectorUS.Data.LogMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnAddInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Log", "UserName", c => c.String(nullable: false));
            AddColumn("dbo.Log", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.Log", "AdditionalInfo", c => c.String());
            DropColumn("dbo.Log", "User");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Log", "User", c => c.String(nullable: false));
            DropColumn("dbo.Log", "AdditionalInfo");
            DropColumn("dbo.Log", "UserId");
            DropColumn("dbo.Log", "UserName");
        }
    }
}
