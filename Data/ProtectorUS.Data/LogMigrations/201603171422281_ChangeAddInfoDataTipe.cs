namespace ProtectorUS.Data.LogMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAddInfoDataTipe : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Log", "AdditionalInfo", c => c.String(unicode: false, storeType: "text"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Log", "AdditionalInfo", c => c.String());
        }
    }
}
