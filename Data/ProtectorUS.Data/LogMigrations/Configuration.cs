namespace ProtectorUS.Data.LogMigrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<LogProtectorUSDatabase>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"LogMigrations";
        }
    }
}
