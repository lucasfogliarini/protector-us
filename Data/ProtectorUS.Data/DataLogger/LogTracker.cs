﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;


namespace ProtectorUS.Data.DataLogger
{
    using Model;
    using System.Collections.ObjectModel;
    public class LogTracker
    {
        private readonly IDbContext _dbContext;
        private readonly IConnectionFactory<LogProtectorUSDatabase> _logConnectionFactory;
        private readonly List<AuditLog> _logs = new List<AuditLog>();
        private readonly string _userName;
        public IEnumerable<AuditLog> Logs { get { return _logs.AsReadOnly(); } }
        private readonly IEnumerable<DbEntityEntry> _additions;
        private readonly IEnumerable<DbEntityEntry> _modifications;

        public LogTracker(string userName, IDbContext dbContext, IConnectionFactory<LogProtectorUSDatabase> logConnectionFactory)
        {
            _userName = userName;
            _dbContext = dbContext;
            this._logConnectionFactory = logConnectionFactory;
            _additions = _dbContext.GetAdditions();
            _modifications = _dbContext.GetModifications();
        }

        public void AddAdditionsLogs()
        {
            _logs.AddRange(AuditAdditions());
        }
        public void AddModificationsLogs()
        {
            _logs.AddRange(AuditModifications());
        }

        public bool HasAddsOrChanges()
        {
            return _logs.Any();
        }

        private IEnumerable<AuditLog> AuditModifications()
        {
            // Get all Deleted/Modified entities (not Unmodified or Detached or Added)
            foreach (DbEntityEntry ent in _modifications)
            {
                using (var auditer = new LogAuditor(ent))
                {
                    var eventType = GetEventType(ent);

                    AuditLog record = auditer.CreateLogRecord(_userName, eventType, _dbContext);

                    if (record != null && record.LogDetails.Any())
                    {
                        yield return record;
                    }
                }
            }
        }
        private IEnumerable<AuditLog> AuditAdditions()
        {
            foreach (DbEntityEntry ent in _additions)
            {
                using (var auditer = new LogAuditor(ent))
                {
                    AuditLog record = auditer.CreateLogRecord(_userName, EventType.Added, _dbContext);
                    if (record != null && record.LogDetails.Any())
                    {
                        yield return record;
                    }
                }
            }
        }
        private EventType GetEventType(DbEntityEntry entry)
        {
            var isSoftDeletable = GlobalTrackingConfig.SoftDeletableType?.IsInstanceOfType(entry.Entity);

            if (isSoftDeletable != null && isSoftDeletable.Value)
            {
                var previouslyDeleted = (bool)entry.OriginalValues[GlobalTrackingConfig.SoftDeletablePropertyName];
                //var nowDeleted = (bool)entry.CurrentValues[GlobalTrackingConfig.SoftDeletablePropertyName];
                var nowDeleted =  (entry.State == EntityState.Deleted);

                if (previouslyDeleted && !nowDeleted)
                {
                    return EventType.UnDeleted;
                }

                if (!previouslyDeleted && nowDeleted)
                {
                    return EventType.SoftDeleted;
                }
            }

            var eventType = entry.State == EntityState.Modified ? EventType.Modified : EventType.Deleted;
            return eventType;
        }
        private IEnumerable<string> EntityTypeNames<TEntity>()
        {
            Type entityType = typeof(TEntity);
            return typeof(TEntity).Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(entityType) || t.FullName == entityType.FullName).Select(m => m.FullName);
        }      
    }
}
