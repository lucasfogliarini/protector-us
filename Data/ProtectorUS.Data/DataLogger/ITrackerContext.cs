﻿using System;
using System.Data.Entity;
using System.Linq;

namespace ProtectorUS.Data
{
    using Model;
    public interface ITrackerContext : IDbContext
    {
        DbSet<AuditLog> AuditLog { get; set; }
        DbSet<AuditLogDetail> LogDetails { get; set; } 

        //event EventHandler<AuditLogGeneratedEventArgs> OnAuditLogGenerated;
        IQueryable<AuditLog> GetLogs(string entityFullName);
        IQueryable<AuditLog> GetLogs(string entityFullName, object primaryKey);
        IQueryable<AuditLog> GetLogs<TEntity>();
        IQueryable<AuditLog> GetLogs<TEntity>(object primaryKey);

    }

}
