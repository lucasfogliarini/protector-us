﻿using System;

namespace ProtectorUS.Data
{
    using Model;
    
    public class AuditLogGeneratedEventArgs : EventArgs
    {
        public AuditLogGeneratedEventArgs(AuditLog log)
        {
            Log = log;
        }

        public AuditLog Log { get; internal set; }

        public bool SkipSaving { get; set; } = false;
    }
}
