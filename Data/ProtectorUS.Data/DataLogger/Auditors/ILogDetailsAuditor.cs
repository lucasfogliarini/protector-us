﻿
using System.Collections.Generic;


namespace ProtectorUS.Data
{
    using Model;
    public interface ILogDetailsAuditor
    {
        IEnumerable<AuditLogDetail> CreateLogDetails();
    }
}
