﻿using System.Data.Entity.Infrastructure;

namespace ProtectorUS.Data.DataLogger
{
    using Model;
    internal class UnDeletedLogDetailsAudotor : ChangeLogDetailsAuditor
    {
        public UnDeletedLogDetailsAudotor(DbEntityEntry dbEntry, AuditLog log) : base(dbEntry, log)
        {
        }
    }
}