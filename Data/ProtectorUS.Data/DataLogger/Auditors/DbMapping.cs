﻿using EntityFramework.MappingAPI;
using EntityFramework.MappingAPI.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace ProtectorUS.Data.DataLogger.Auditors
{
    internal class DbMapping
    {
        private readonly IEntityMap _entityMap;
        private readonly Type _entityType;

        internal DbMapping(IDbContext context, Type entityType)
        {
            _entityType = entityType;
            _entityMap = (context as DbContext).Db(_entityType);
        }


        internal IEnumerable<PropertyConfiguerationKey> PrimaryKeys()
        {
            return _entityMap.Pks
                .Select(x => new PropertyConfiguerationKey(
                    x.PropertyName,
                    _entityType.FullName));
        }
    }
}
