﻿using System.Data.Entity.Infrastructure;
using System.Data.Entity;

namespace ProtectorUS.Data.DataLogger
{
    using Model;
    using Extensions;

    internal class AdditionLogDetailsAuditor : ChangeLogDetailsAuditor
    {
        public AdditionLogDetailsAuditor(DbEntityEntry dbEntry, AuditLog log) 
            : base(dbEntry, log)
        {

        }

        /// <summary>
        /// Treat unchanged entries as added entries when creating audit records.
        /// </summary>
        /// <returns></returns>
        protected internal override EntityState StateOfEntity()
        {
            if (DbEntry.State == EntityState.Unchanged)
            {
                return EntityState.Added;
            }

            return base.StateOfEntity();
        }

        /// <summary>
        /// Verify is the property value is changed
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected override bool IsValueChanged(string propertyName)
        {
            if (GlobalTrackingConfig.TrackEmptyPropertiesOnAdditionAndDeletion)
                return true;

            var propertyType = DbEntry.Entity.GetType().GetProperty(propertyName).PropertyType;
            object defaultValue = propertyType.DefaultValue();
            object currentValue = CurrentValue(propertyName);

            Comparator comparator = ComparatorFactory.GetComparator(propertyType);

            return !comparator.AreEqual(defaultValue, currentValue);
        }

        /// <summary>
        /// Get the original value of a property, in addiction is null.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected override object OriginalValue(string propertyName)
        {
            return null;
        }
    }
}