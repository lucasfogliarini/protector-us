﻿using System.Data.Entity.Infrastructure;

namespace ProtectorUS.Data.DataLogger
{
    using Model;
    internal class SoftDeletedLogDetailsAuditor : ChangeLogDetailsAuditor
    {
        public SoftDeletedLogDetailsAuditor(DbEntityEntry dbEntry, AuditLog log) : base(dbEntry, log)
        {
        }
    }
}