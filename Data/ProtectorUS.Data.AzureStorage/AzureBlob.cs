﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace ProtectorUS.Data.AzureStorage
{
    using Configuration;
    using Exceptions;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    public abstract class AzureBlob
    {
        public CloudBlobContainer Container { get; private set; }
        public string DirectoryName { get; private set; }
        private List<string> ValidFormats { get; set; } = new List<string>();
        /// <param name="containerName">Container names may only contain lowercase letters, numbers, and hyphens, and must begin with a letter or a number. The name can not contain two consecutive hyphens. The name must also be between 3 and 63 characters long.</param>
        public AzureBlob(string containerName, string directoryName, params FileType[] validFiles)
        {
            DirectoryName = directoryName;
            foreach (var fileType in validFiles)
            {
                ValidFormats.AddRange(fileType.Description().Split(','));
            }
            CloudStorageAccount storageAccount = ParseStorageAccount();
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            Container = blobClient.GetContainerReference(containerName);
            Container.CreateIfNotExists();
        }
        public static Uri GetEndpoint()
        {
            return ParseStorageAccount().BlobEndpoint;
        }
        private static CloudStorageAccount ParseStorageAccount()
        {
            var config = ProtectorUSConfiguration.GetConfig();
            string connectionString = config.AzureStorageAccount.Value;
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentRequiredException(e => e.EL014);
            return CloudStorageAccount.Parse(connectionString);
        }
        string GetBlobPath(string blobName)
        {
            return $"{DirectoryName}/{blobName}";
        }
        private CloudBlockBlob GetBlockBlob(string blobPath, bool directoryPrefix = false)
        {
            blobPath = directoryPrefix ? GetBlobPath(blobPath) : blobPath;
            CloudBlockBlob blockBlob = Container.GetBlockBlobReference(blobPath);
            if (!blockBlob.Exists())
                throw new NotFoundException(e => e.EL026, blobPath);
            return blockBlob;
        }

        #region Download
        public MemoryStream DownloadStream(string blobPath, bool directoryPrefix = false)
        {
            CloudBlockBlob blockBlob = GetBlockBlob(blobPath, directoryPrefix);
            MemoryStream memStream = new MemoryStream();
            blockBlob.DownloadToStream(memStream);
            return memStream;
        }
        public string DownloadText(string blobPath, bool directoryPrefix = false)
        {
            CloudBlockBlob blockBlob = GetBlockBlob(blobPath, directoryPrefix);
            return blockBlob.DownloadText();
        }
        #endregion

        #region Upload
        public async Task<string> UploadAsync(StreamContent streamContent, string fileName = null)
        {
            string contentFileName = streamContent.Headers.ContentDisposition.FileName.Trim('\"');
            string fileExtension = Path.GetExtension(contentFileName).ToLower();
            if (!ValidFormats.Contains(fileExtension.TrimStart('.')))
                throw new BadRequestException(x => x.EL034);
            Stream stream = await streamContent.ReadAsStreamAsync();
            fileName = fileName?? $"{stream.Length}_{Guid.NewGuid().ToString("n")}_{contentFileName}";

            string blobPath = GetBlobPath(fileName);
            CloudBlockBlob blockBlob = Container.GetBlockBlobReference(blobPath);
          
            await blockBlob.UploadFromStreamAsync(stream);
            return blobPath;
        }
        /// <summary>
        /// Upload blob ASYNCHRONOUSLY and return blobpath (directoryName + fileName)
        /// </summary>
        public string UploadAsync(byte[] buffer, string fileName)
        {
            string blobPath = GetBlobPath(fileName);
            CloudBlockBlob blockBlob = Container.GetBlockBlobReference(blobPath);

            blockBlob.UploadFromByteArrayAsync(buffer, 0, buffer.Length);
            return blobPath;
        }
        /// <summary>
        /// Upload blob SYNCHRONOUSLY and return blobpath (directoryName + fileName)
        /// </summary>
        public string Upload(byte[] buffer, string fileName)
        {
            string blobName = GetBlobPath(fileName);
            CloudBlockBlob blockBlob = Container.GetBlockBlobReference(blobName);

            blockBlob.UploadFromByteArray(buffer, 0, buffer.Length);
            return blobName;
        }
        #endregion
    }
}
