﻿using ProtectorUS.Model;

namespace ProtectorUS.Data.AzureStorage
{
    public class PolicyCertificateAzureBlob : AzureBlob
    {
        public PolicyCertificateAzureBlob() : base("public", Policy.policyCertificatesDirectory, FileType.Pdf)
        {           
        }
    }
}