﻿namespace ProtectorUS.Data.AzureStorage
{
    public class ImageAzureBlob : AzureBlob
    {
        public ImageAzureBlob() : base("public", "images", FileType.Image)
        {
        }
    }
}