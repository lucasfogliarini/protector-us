﻿namespace ProtectorUS.Data.AzureStorage
{
    public class ProductTermAzureBlob : AzureBlob
    {
        public ProductTermAzureBlob() : base("public", "product_terms", FileType.Pdf)
        {           
        }
    }
}