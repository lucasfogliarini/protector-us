﻿namespace ProtectorUS.Data.AzureStorage
{
    public class ClaimDocumentAzureBlob : AzureBlob
    {
        public ClaimDocumentAzureBlob() : base("private","claim_documents", FileType.Image, FileType.Pdf, FileType.Doc)
        {
        }
    }
}