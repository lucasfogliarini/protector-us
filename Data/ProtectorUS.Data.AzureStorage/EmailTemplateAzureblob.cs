﻿namespace ProtectorUS.Data.AzureStorage
{
    public class EmailTemplateAzureblob : AzureBlob
    {
        public EmailTemplateAzureblob() : base("public", "email", FileType.Any)
        {           
        }
    }
}