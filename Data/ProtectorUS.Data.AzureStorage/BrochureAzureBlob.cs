﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtectorUS.Data.AzureStorage
{
    public class BrochureAzureBlob : AzureBlob
    {
        public BrochureAzureBlob() : base("public", "brochures", FileType.Pdf)
        {
        }

    }
}
