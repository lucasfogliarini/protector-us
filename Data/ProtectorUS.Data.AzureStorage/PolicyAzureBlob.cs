﻿using ProtectorUS.Model;

namespace ProtectorUS.Data.AzureStorage
{
    public class PolicyAzureBlob : AzureBlob
    {
        public PolicyAzureBlob() : base("private", Policy.policiesDirectory, FileType.Pdf)
        {
        }
    }
}
