﻿using System;
using Microsoft.Azure.Documents.Client;

namespace ProtectorUS.Data.DocumentDB
{
    using Configuration;

    public class ConnectionFactory : IConnectionFactory
    {
        private readonly ProtectorUSConfiguration _config;
        public ConnectionFactory()
        {
            _config = ProtectorUSConfiguration.GetConfig();
        }

        private DocumentClient _connection;
        public DocumentClient DocumentClient()
        {
            return _connection ?? (_connection = new DocumentClient(new Uri(_config.DocumentDb.Url), _config.DocumentDb.Key));
        }
        public string DatabaseName { get { return _config.DocumentDb.Database; } }
    }
}
