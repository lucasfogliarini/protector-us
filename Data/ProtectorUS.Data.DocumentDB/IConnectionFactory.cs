﻿using Microsoft.Azure.Documents.Client;

namespace ProtectorUS.Data.DocumentDB
{
    public interface IConnectionFactory
    {
        DocumentClient DocumentClient();
        string DatabaseName { get; }
    }
}
