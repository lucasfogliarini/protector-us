﻿using System;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProtectorUS.Data.DocumentDB
{
    using Model.DocumentDB.Repositories;
    using System.Threading;
    public abstract class DocumentDBRepository<T> : IDocumentDBRepository<T>
    {
        private readonly IConnectionFactory _connectionFactory;

        public DocumentDBRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        protected abstract string CollectionName { get; set; }
        private Database _database;
        protected Database Database
        {
            get
            {
                if (_database == null)
                {
                    _database = DocumentClient.CreateDatabaseQuery()
                       .Where(db => db.Id == _connectionFactory.DatabaseName)
                       .AsEnumerable()
                       .FirstOrDefault();

                    if (_database == null)
                    {
                        var resourceResponse = DocumentClient.CreateDatabaseAsync(
                                       new Database
                                       {
                                           Id = _connectionFactory.DatabaseName
                                       });

                        _database = resourceResponse.Result;
                    }
                }                

                return _database;
            }
        }
        protected DocumentClient DocumentClient
        {
            get
            {
                return _connectionFactory.DocumentClient();
            }
        }
        private DocumentCollection _collection;
        protected DocumentCollection Collection
        {
            get
            {
                if (_collection == null)
                {
                    _collection = DocumentClient.CreateDocumentCollectionQuery(Database.CollectionsLink)
                                .Where(c => c.Id == CollectionName)
                                .AsEnumerable()
                                .FirstOrDefault();

                    if (_collection == null)
                    {
                        var resourceResponse = DocumentClient.CreateDocumentCollectionAsync(Database.CollectionsLink,
                                                new DocumentCollection
                                                {
                                                    Id = CollectionName
                                                });
                        _collection = resourceResponse.Result;
                    }
                }
                
                return _collection;
            }
        }        
        public T Find(Expression<Func<T, bool>> predicate)
        {
            return DocumentClient.CreateDocumentQuery<T>(Collection.DocumentsLink)
                        .Where(predicate)
                        .AsEnumerable()
                        .FirstOrDefault();
        }
        public IEnumerable<T> FindAll(Expression<Func<T, bool>> predicate)
        {
            return DocumentClient.CreateDocumentQuery<T>(Collection.DocumentsLink)
                .Where(predicate)
                .AsEnumerable();
        }
        public int Count(Expression<Func<T, bool>> predicate)
        {
            return DocumentClient.CreateDocumentQuery<T>(Collection.DocumentsLink)
                .Where(predicate)
                .AsEnumerable()
                .Count();
        }
        public virtual void Upsert(T item)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    DocumentClient.UpsertDocumentAsync(Collection.DocumentsLink, item).Wait();
                    break;
                }
                catch (DocumentClientException documentClientException)
                {
                    WaitToRetry(documentClientException);
                }
                catch (AggregateException aggregateException)
                {
                    var documentClientException = aggregateException.InnerException as DocumentClientException;
                    WaitToRetry(documentClientException);
                }
            }
        }
        //https://blogs.msdn.microsoft.com/bigdatasupport/2015/09/02/dealing-with-requestratetoolarge-errors-in-azure-documentdb-and-testing-performance/#gist25897506
        private void WaitToRetry(DocumentClientException documentClientException)
        {
            if (documentClientException != null)
            {
                var statusCode = (int)documentClientException.StatusCode;
                if (statusCode == 429 || statusCode == 503)
                {
                    Thread.Sleep(documentClientException.RetryAfter);
                    return;
                }
            }
            throw new Exceptions.SeedException("DocumentDb Upsert error.");
        }
    }
}
