﻿namespace ProtectorUS.Data.DocumentDB.Repositories
{
    using Model.DocumentDB.RatingPlan;
    using Model.DocumentDB.Repositories;

    public class RatingPlanFormRepository : DocumentDBRepository<RatingPlanForm>, IRatingPlanFormRepository
    {
        public RatingPlanFormRepository(IConnectionFactory connectionFactory) : base(connectionFactory) { }

        protected override string CollectionName { get; set; } = "ratingplanforms";

        public RatingPlanForm Find(long formid)
        {
            return Find(x => x.formId == formid);
        }
    }
}
