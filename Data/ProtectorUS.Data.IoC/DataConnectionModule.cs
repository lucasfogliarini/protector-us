﻿using Autofac;

namespace ProtectorUS.Data.IoC
{
    using Data;
    /// <summary>
    /// Group registration of console application's infrastructure services.
    /// </summary>
    public class DataConnectionModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <param name="builder">The builder through which components can be
        ///             registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProtectorUSConnectionFactory>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<LogProtectorUSConnectionFactory>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<DocumentDB.ConnectionFactory>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }
    }
}
