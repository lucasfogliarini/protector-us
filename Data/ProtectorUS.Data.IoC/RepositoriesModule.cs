﻿using Autofac;

namespace ProtectorUS.Data.IoC
{
    using Repositories;

    /// <summary>
    /// Dependency module for repositories registration.
    /// </summary>
    public class RepositoriesModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <param name="builder">The builder through which components can be
        ///             registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterType<BrokerageRepository>(builder);
            RegisterType<BrokerageGroupRepository>(builder);
            RegisterType<BrokerageProductRepository>(builder);
            RegisterType<AddressRepository>(builder);
            RegisterType<CityRepository>(builder);
            RegisterType<CountryRepository>(builder);
            RegisterType<PhoneRepository>(builder);
            RegisterType<RegionRepository>(builder);
            RegisterType<StateRepository>(builder);
            RegisterType<OrderRepository>(builder);
            RegisterType<PolicyRepository>(builder);
            RegisterType<PaymentMethodRepository>(builder);
            RegisterType<ProductRepository>(builder);
            RegisterType<ProductPaymentMethodRepository>(builder);
            RegisterType<RatingPlanFormVersionRepository>(builder);
            RegisterType<DocumentDB.Repositories.RatingPlanFormRepository>(builder);
            RegisterType<BrokerRepository>(builder);
            RegisterType<BusinessRepository>(builder);
            RegisterType<CompanyRepository>(builder);
            RegisterType<InsuredRepository>(builder);
            RegisterType<ManagerUserRepository>(builder);
            RegisterType<PermissionGroupRepository>(builder);
            RegisterType<ProfileRepository>(builder);
            RegisterType<UserRepository>(builder);
            RegisterType<CampaignRepository>(builder);
            RegisterType<LogRepository>(builder);
            RegisterType<ClaimRepository>(builder);
            RegisterType<ClaimDocumentRepository>(builder);
            RegisterType<ThirdPartyRepository>(builder);
            RegisterType<EndorsementRepository>(builder);
            RegisterType<WebpageProductRepository>(builder);
            RegisterType<QuotationRepository>(builder);
            RegisterType<QuotationDeliveryRepository>(builder);
            RegisterType<DeclinedRepository>(builder);
            RegisterType<DeclinedBlockRepository>(builder);
            RegisterType<DeclinedUnblockRepository>(builder);
            RegisterType<AppClientRepository>(builder);
            RegisterType<RefreshTokenRepository>(builder);
            RegisterType<CancelReasonRepository>(builder);
            RegisterType<WebpageViewRepository>(builder);
            RegisterType<ProductConditionRepository>(builder);
            RegisterType<NotificationRepository>(builder);
            RegisterType<NewsLetterRepository>(builder);
            RegisterType<EmailTemplateRepository>(builder);
            RegisterType<CoverageRepository>(builder);
            RegisterType<StateSurchargeRepository>(builder);
            RegisterType<CountySurchargeRepository>(builder);
            RegisterType<AcordTransactionRepository>(builder);
            RegisterType<DeclineReasonRepository>(builder);
        }

        private void RegisterType<T>(ContainerBuilder builder)
        {
            builder.RegisterType<T>().AsImplementedInterfaces().InstancePerRequest();
        }
    }
}
