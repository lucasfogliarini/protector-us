﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class RiskCaracteristic
    {
        public RiskCaracteristic(string name, decimal min, decimal max)
        {
            this.name = name;
            this.min = min;
            this.max = max;
        }
        public string name { get; set; }
        public decimal min { get; set; }
        public decimal max { get; set; }
    }
}