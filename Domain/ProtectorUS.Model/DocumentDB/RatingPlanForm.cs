﻿using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class RatingPlanForm
    {
        public string id { get; set; }
        public long formId { get; set; }
        public int version { get; set; }
        public BasePremium basePremium { get; set; }
        public RevenueStaffCredit revenueStaffCredit { get; set; }
        public IList<ExperienceModification> experienceModifications { get; set; }
        public IList<YearFactor> priorActsCoverages { get; set; }
        public IList<YearFactor> businessManagement { get; set; }
        public ScheduleModifications scheduleModifications { get; set; }
        public decimal maxRevenue { get; set; }
        public DefenseOutsideLimit defenseOutsideLimit { get; set; }
        public FirstDollarDefense firstDollarDefense { get; set; }
        public decimal minPremium { get; set; }
        public bool rounding { get; set; }
        public List<Field> fields { get; set; }
        public Field GetField(string fieldName)
        {
            return fields.FirstOrDefault(f => f.name?.ToLower() == fieldName?.ToLower());
        }
        public decimal GetFieldFactor(string fieldName, string optionValue)
        {
            return GetField(fieldName).GetOption(optionValue).factor;
        }
        public void InsertAfter(Field field, string fieldBeforeName)
        {
            var fieldBefore = GetField(fieldBeforeName);
            int fieldBeforeIndex = fields.IndexOf(fieldBefore);
            fields.Insert(fieldBeforeIndex + 1, field);
        }
    }
}
