﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class Condition
    {
        public Condition()
        {
        }
        public Condition(string _ref, string value)
        {
            Ref = _ref;
            this.value = value;
        }
        public string value { get; set; }

        [Newtonsoft.Json.JsonProperty("ref")]
        public string Ref { get; set; }
        [Newtonsoft.Json.JsonProperty("operator")]
        public string Operator { get; set; }
        internal bool IsRequired(string requiredValue)
        {
            int comparison = requiredValue.CompareTo(value);
            switch (Operator)
            {
                case EqualsOperator:
                    return comparison == 0;
                case DifferentOperator:
                    return comparison != 0;
                case GreaterAndEqualOperator:
                    return comparison > 0;
                case SmallerAndEqualOperator:
                    return comparison < 0;
            }
            return false;
        }

        public const string EqualsOperator = "equals";
        public const string DifferentOperator = "different";
        public const string GreaterAndEqualOperator = "greater";
        public const string SmallerAndEqualOperator = "smaller";
    }
}
