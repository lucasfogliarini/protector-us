﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class RevenueStaffCredit
    {
        public RevenueStaffCredit()
        {
        }
        public RevenueStaffCredit(decimal minPerMember)
        {
            this.minPerMember = minPerMember;
        }

        public decimal minPerMember { get; set; }
        public IList<CreditPerStaff> credits { get; set; }
    }
}