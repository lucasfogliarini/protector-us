﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class YearFactor
    {
        public YearFactor()
        {
        }
        public YearFactor(double years, decimal factor)
        {
            this.years = years;
            this.factor = factor;
        }
        public double years { get; set; }
        public decimal factor { get; set; }
    }
}