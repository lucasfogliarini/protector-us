﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class BasePremiumRange
    {
        public BasePremiumRange()
        {
        }
        public BasePremiumRange(decimal maxRevenue, decimal basePremium, decimal plus)
        {
            this.maxRevenue = maxRevenue;
            this.basePremium = basePremium;
            this.plus = plus;
        }
        public decimal maxRevenue { get; set; }
        public decimal basePremium { get; set; }
        public decimal plus { get; set; }
    }
}