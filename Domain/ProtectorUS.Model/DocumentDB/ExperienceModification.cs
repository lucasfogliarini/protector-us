﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class ExperienceModification
    {
        public ExperienceModification()
        {
        }
        public ExperienceModification(decimal maxRevenue)
        {
            this.maxRevenue = maxRevenue;
        }
        public decimal maxRevenue { get; set; }
        public IList<ExperienceModificationFactor> items { get; set; }
    }
}