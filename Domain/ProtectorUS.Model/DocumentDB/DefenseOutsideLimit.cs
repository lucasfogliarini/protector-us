﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class DefenseOutsideLimit
    {
        public decimal minPremium { get; set; }
        public IList<DefenseOutsideLimitFactor> factors { get; set; }
    }
    public class DefenseOutsideLimitFactor
    {
        public DefenseOutsideLimitFactor()
        {
        }
        public DefenseOutsideLimitFactor(string coverageValue, decimal factor)
        {
            this.coverageValue = coverageValue;
            this.factor = factor;
        }
        public string coverageValue { get; set; }
        public decimal factor { get; set; }
    }
}