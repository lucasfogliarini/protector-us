﻿using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class ScheduleModifications
    {
        public decimal min { get; set; }
        public decimal max { get; set; }
        public IList<RiskCaracteristic> riskCaracteristics { get; set; }
        public decimal GetFairFactor(string riskCaracteristicName, decimal factor)
        {
            var riskCaracteristic = riskCaracteristics.FirstOrDefault(r => r.name == riskCaracteristicName);
            return GetFairFactor(factor, riskCaracteristic.min, riskCaracteristic.max);
        }
        public static decimal GetFairFactor(decimal factor, decimal min, decimal max)
        {
            if (factor < min)
                return min;
            else if (factor > max)
                return max;
            return factor;
        }
    }
}