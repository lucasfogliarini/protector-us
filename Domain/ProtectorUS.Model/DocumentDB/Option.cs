﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class Option
    {
        public Option()
        {
        }
        public Option(string label, string value, decimal? factor = null)
        {
            this.label = label;
            this.value = value;
            this.factor = factor?? 1;
        }
        public string label { get; set; }
        public string value { get; set; }
        public bool isDeclined { get; set; }
        public decimal factor { get; set; }
        public IList<Condition> conditionals { get; set; }
    }
}
