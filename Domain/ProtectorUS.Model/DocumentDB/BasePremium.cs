﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class BasePremium
    {
        public decimal incremetalRate { get; set; }
        public IList<BasePremiumRange> ranges { get; set; }
    }
}