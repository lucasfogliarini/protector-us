﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class CreditPerStaff
    {
        public CreditPerStaff()
        {
        }
        public CreditPerStaff(decimal maxRevenuePerMember, decimal factor)
        {
            this.maxRevenuePerMember = maxRevenuePerMember;
            this.factor = factor;
        }
        public decimal maxRevenuePerMember { get; set; }
        public decimal factor { get; set; }
    }
}