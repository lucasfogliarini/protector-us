﻿using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class Field
    {
        public Field()
        {
        }
        public Field(string label, string type)
        {
            this.label = label;
            this.type = type;
        }
        public string label { get; set; }
        public bool active { get; set; } = true;
        public string labelAfter { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string placeholder { get; set; }
        public string tooltip { get; set; }
        public string classes { get; set; }
        public string invalidMessage { get; set; }
        public IList<Condition> conditionals { get; set; }

        #region text
        public int? minlength { get; set; }
        public int? maxlength { get; set; }
        #endregion

        #region select
        public IList<Option> options { get; set; }
        public Option GetOption(string value)
        {
            return options?.FirstOrDefault(o => o.value == value);
        }
        public static string[] SplitValue(string value)
        {
            string[] values = value.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
            return values.Select(v => v.Trim()).ToArray();
        }

        #endregion

        public bool Validate(string value)
        {
            if (type == "select")
                return GetOption(value) != null;
            else if (type == "checkbox")
            {
                string[] values = SplitValue(value);
                return values.All(v => options.Any(o => o.value == v));
            }
            return true;
        }

        public bool IsRequired(RiskAnalysis riskAnalysis)
        {
            if (conditionals != null)
            {
                foreach (Condition condition in conditionals)
                {
                    string requiredValue = riskAnalysis.GetFieldValue(condition.Ref);
                    if (!condition.IsRequired(requiredValue))
                        return false;
                }
            }            
            return true;
        }

        public const string text = "text";
        public const string number = "number";
        public const string select = "select";
        public const string checkbox = "checkbox";
        public const string monthPicker = "monthPicker";
        public const string datePickerRetroactive = "datePickerRetroactive";
        public const string datePickerLimited = "datePickerLimited";
        public const string datePicker = "datePicker";
        public const string money = "money";
    }
}