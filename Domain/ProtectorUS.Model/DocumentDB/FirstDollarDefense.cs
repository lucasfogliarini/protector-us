﻿using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class FirstDollarDefense
    {
        public decimal minEndorsementPremium { get; set; }
        public IList<FirstDollarDefenseFactor> factors { get; set; }
    }
    public class FirstDollarDefenseFactor
    {
        public FirstDollarDefenseFactor()
        {
        }
        public FirstDollarDefenseFactor(string deductibleValue, decimal factor)
        {
            this.deductibleValue = deductibleValue;
            this.factor = factor;
        }
        public string deductibleValue { get; set; }
        public decimal factor { get; set; }
    }
}