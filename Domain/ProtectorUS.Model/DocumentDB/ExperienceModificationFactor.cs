﻿namespace ProtectorUS.Model.DocumentDB.RatingPlan
{
    public class ExperienceModificationFactor
    {
        public ExperienceModificationFactor()
        {
        }
        public ExperienceModificationFactor(int minRetroactiveYear, int claims, decimal factor)
        {
            this.minRetroactiveYear = minRetroactiveYear;
            this.claims = claims;
            this.factor = factor;
        }
        public int minRetroactiveYear { get; set; }
        public int claims { get; set; }
        public decimal factor { get; set; }
    }
}