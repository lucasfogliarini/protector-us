﻿using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Text;

namespace ProtectorUS.Model.Validation
{
    public class ValidationManager : IValidationManager
    {
        readonly List<ValidationFailure> _errors = new List<ValidationFailure>();
 
        public List<ValidationFailure> Errors
        {
            get
            {
                return _errors;
            }
        }
        public bool HasErrors { get { return _errors.Count > 0; } }
        public void ThrowErrors()
        {
            if (HasErrors)
            {
                ValidationFailure[] errors = Errors.ToArray();
                Clear();
                throw new ValidationException(errors);
            }
        }
        public void Clear()
        {
            _errors.Clear();
        }

        public void ValidateAndThrowErrors<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Validate(entity);
            if (HasErrors) ThrowErrors();
        }

        /// <summary>
        /// Verify if a entity is valid and add the validation result in a validation manager
        /// </summary>
        /// <param name="entity"></param>
        public void Validate<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            entity = entity ?? System.Activator.CreateInstance(typeof(TEntity), true) as TEntity;
            IValidator validator = entity.GetValidator();
            ValidationResult validation = validator.Validate(entity);
            if (!validation.IsValid)
            {
                string entityName = entity.GetType().Name;
                foreach (var error in validation.Errors)
                {
                    error.ResourceName = entityName;
                }
                _errors.AddRange(validation.Errors);
            }
        }

        public void AddError(string errorMessage, string entityName)
        {
            var error = new ValidationFailure("", errorMessage);
            error.ResourceName = entityName;
            Errors.Add(error);
        }
    }
}
