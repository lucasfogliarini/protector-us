﻿using FluentValidation.Validators;
using System.Collections.Generic;

namespace FluentValidation
{
    internal class AtLeastValidator<T> : PropertyValidator
    {
        public int Length { get; private set; }
        /// <summary>
        /// Validate if collection has at least 1 item.
        /// </summary>
        public AtLeastValidator(int length) : base($"{typeof(T).Name} must contain at least {length} item(s)")
        {
            Length = length;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            IList<T> list = context.PropertyValue as IList<T>;
            return list?.Count >= Length;
        }
    }
}
