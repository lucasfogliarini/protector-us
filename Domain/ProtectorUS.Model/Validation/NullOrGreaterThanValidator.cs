﻿using FluentValidation.Validators;
using System;
using System.Collections.Generic;

namespace FluentValidation
{
    internal class NullOrGreaterThanValidator : GreaterThanValidator
    {
        public NullOrGreaterThanValidator(IComparable value) :base(value)
        {
        }

        public override bool IsValid(IComparable value, IComparable valueToCompare)
        {
            if (value == null)
            {
                return true;
            }
            return base.IsValid(value, valueToCompare);
        }
    }
}
