﻿using FluentValidation.Results;
using System.Collections.Generic;

namespace ProtectorUS.Model.Validation
{
    public interface IValidationManager
    {
        List<ValidationFailure> Errors { get; }
        bool HasErrors { get; }
        void Clear();
        void ThrowErrors();
        void Validate<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void ValidateAndThrowErrors<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void AddError(string errorMessage, string entityName);
    }
}