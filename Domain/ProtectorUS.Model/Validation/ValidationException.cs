﻿using FluentValidation.Results;
using ProtectorUS.Exceptions;
using System;
using System.Linq;

namespace ProtectorUS.Model.Validation
{
    [Serializable]
    public class ValidationException : BaseException
    {
        public ValidationException(ValidationFailure[] errors)
            : base(c => c.ES017, null, errors.Select(e=>e.ErrorMessage).ToArray())
        {
            Errors = errors;
        }
        public ValidationFailure[] Errors { get; private set; }
    }

}
