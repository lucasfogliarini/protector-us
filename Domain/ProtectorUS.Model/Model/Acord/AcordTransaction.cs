﻿using System;

namespace ProtectorUS.Model
{
    public class AcordTransaction: Entity<long>
    {
        public long PolicyId { get; set; }
        public Policy Policy { get; set; }
        public string Xml { get; set; }
        public string Token { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
