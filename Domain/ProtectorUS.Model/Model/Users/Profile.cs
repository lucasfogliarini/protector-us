﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProtectorUS.Model
{
    public class Profile : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Permission> Permissions { get; set; }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Name) };
            }
        }

        //caution with SeedUsers!
        public static class Ids
        {
            public const long AdminManagerUser = 1;
            public const long DirectorManagerUser = 2;
            public const long ManagerManagerUser = 3;
            public const long UnderwriterManagerUser = 4;
            public const long ClaimsManagerUser = 5;
            public const long AssistantManagerUser = 6;

            public const long MasterBroker = 7;
            public const long AdminBroker = 8;
            public const long AgentBroker = 9;

            public const long Insured = 10;


            public static readonly long[] ManagerUserProfiles = { AdminManagerUser, DirectorManagerUser, ManagerManagerUser, UnderwriterManagerUser, ClaimsManagerUser, AssistantManagerUser };
            public static readonly long[] BrokerProfiles = { MasterBroker, AdminBroker, AgentBroker };
        }

        #region Profiles and Permissions

        public static IEnumerable<Permission> GetPermissions(long profileId, DbSet<Permission> dbPermissions)
        {
            string[] argoManager_basicPermissions = ManagerUserPermissions.BasicPermissions;
            string[] argoManager_managerUsersRead = ManagerUserPermissions.ManagerUsersRead;
            string[] argoManager_brokeragesCreateUpdateDelete = ManagerUserPermissions.BrokeragesCreateUpdateDelete;
            string[] argoManager_underwritingFullAccess = ManagerUserPermissions.UnderwritingFullAccess;
            string[] argoManager_claimsChangeStatus = ManagerUserPermissions.ClaimsChangeStatus;
            string[] argoManager_claimsCreateDeleteManageDocuments = ManagerUserPermissions.ClaimsCreateDeleteManageDocuments;

            string[] brokerCenter_basicPermissions = BrokerPermissions.BasicPermissions;
            string[] brokerCenter_brokersCreateReadUpdate = BrokerPermissions.BrokersCreateReadUpdate;
            string[] brokerCenter_brokersDelete = BrokerPermissions.BrokersDelete;
            string[] brokerCenter_brokeragesOnbordingUpdate = BrokerPermissions.BrokeragesOnboardingUpdate;

            long groupId = ManagerUserPermissions.GroupId;
            IEnumerable<string> permissions = Enumerable.Empty<string>();
            switch (profileId)
            {
                case Ids.AdminManagerUser:
                    permissions = ManagerUserPermissions.AllPermissions;
                    break;
                case Ids.DirectorManagerUser:
                    permissions = argoManager_basicPermissions.Concat(argoManager_managerUsersRead)
                                                              .Concat(argoManager_brokeragesCreateUpdateDelete)
                                                              .Concat(argoManager_underwritingFullAccess)
                                                              .Concat(argoManager_claimsChangeStatus)
                                                              .Concat(argoManager_claimsCreateDeleteManageDocuments);
                    break;
                case Ids.ManagerManagerUser:
                    permissions = argoManager_basicPermissions.Concat(argoManager_brokeragesCreateUpdateDelete)
                                                              .Concat(argoManager_claimsCreateDeleteManageDocuments);
                    break;
                case Ids.UnderwriterManagerUser:
                    permissions = argoManager_basicPermissions.Concat(argoManager_underwritingFullAccess);
                    break;
                case Ids.ClaimsManagerUser:
                    permissions = argoManager_basicPermissions.Concat(argoManager_claimsCreateDeleteManageDocuments)
                                                              .Concat(argoManager_claimsChangeStatus);
                    break;
                case Ids.AssistantManagerUser:
                    permissions = argoManager_basicPermissions;
                    break;

                case Ids.MasterBroker:
                    groupId = BrokerPermissions.GroupId;
                    permissions = BrokerPermissions.AllPermissions;
                    break;
                case Ids.AdminBroker:
                    groupId = BrokerPermissions.GroupId;
                    permissions = brokerCenter_basicPermissions.Concat(brokerCenter_brokersCreateReadUpdate);
                    break;
                case Ids.AgentBroker:
                    groupId = BrokerPermissions.GroupId;
                    permissions = brokerCenter_basicPermissions;
                    break;
            }
            foreach (string actionApi in permissions)
            {
                Permission permission = dbPermissions.Local.FirstOrDefault(p => p.ActionApi == actionApi && p.GroupId == groupId);

                if (permission == null)
                {
                    permission = new Permission(actionApi, groupId);
                    dbPermissions.Add(permission);
                }
                yield return permission;
            }
        }

        public static class Profiles
        {
            public static readonly Profile AdminManagerUser = new Profile()
            {
                Id = Ids.AdminManagerUser,
                Name = "ARGO Admin",
                Permissions = new List<Permission>()
            };
            public static readonly Profile DirectorManagerUser = new Profile()
            {
                Id = Ids.DirectorManagerUser,
                Name = "ARGO Director",
                Permissions = new List<Permission>()
            };
            public static readonly Profile ManagerManagerUser = new Profile()
            {
                Id = Ids.ManagerManagerUser,
                Name = "ARGO Manager",
                Permissions = new List<Permission>()
            };
            public static readonly Profile UnderwriterManagerUser = new Profile()
            {
                Id = Ids.UnderwriterManagerUser,
                Name = "ARGO Underwriter",
                Permissions = new List<Permission>()
            };
            public static readonly Profile ClaimsManagerUser = new Profile()
            {
                Id = Ids.ClaimsManagerUser,
                Name = "ARGO Claims",
                Permissions = new List<Permission>()
            };
            public static readonly Profile AssistantManagerUser = new Profile()
            {
                Id = Ids.AssistantManagerUser,
                Name = "ARGO Assistant",
                Permissions = new List<Permission>()
            };
            public static readonly Profile MasterBroker = new Profile()
            {
                Id = Ids.MasterBroker,
                Name = "Master",
                Permissions = new List<Permission>()
            };
            public static readonly Profile AdminBroker = new Profile()
            {
                Id = Ids.AdminBroker,
                Name = "Admin",
                Permissions = new List<Permission>()
            };
            public static readonly Profile AgentBroker = new Profile()
            {
                Id = Ids.AgentBroker,
                Name = "Agent",
                Permissions = new List<Permission>()
            };

            public static readonly Profile Insured = new Profile()
            {
                Id = Ids.Insured,
                Name = "Insured",
                Permissions = new List<Permission>()
            };
        }
        public static class ManagerUserPermissions
        {
            public const long GroupId = 1;
            public static readonly string[] BasicPermissions = {
                "Brokerages.Get","Brokerages.Find","Brokerages.GetAll","Brokerages.Claims", "Brokerages.Insureds","Brokerages.Policies", "Brokerages.Webpages","Brokerages.GetMapPoints","Brokerages.GetTop","Brokerages.GetTotalTypes","Brokerages.GetTotalLastYear","Brokerages.EnableDisableProduct", "Brokerages.GetActiveInactiveBrokeragesAndAgents",
                "Claims.GetAll","Claims.Get","Claims.GetMapPoints","Claims.GetTotalInterval",
                "Insureds.FindAll","Insureds.GetAll","Insureds.Get","Insureds.Claims","Insureds.Endorsements","Insureds.GetMapPoints","Insureds.CountByStatus","Insureds.GetTotalLastYear",
                "Policies.FindAll","Policies.Get", "Policies.GetAll","Policies.Claims", "Policies.GetMapPoints","Policies.Endorsements","Policies.Orders","Policies.GetMapPoints","Policies.GetDashboardTypes","Policies.GetTotalInterval","Policies.GetTotalGeneral","Policies.GetRenewalsTotal","Policies.CountInEffect","Policies.GetTotalLastYear","Policies.RegenPolicy",
                "Products.GetAll",
                "Quotations.GetTotalStatus",
                "Quotations.GetDeclinesTotalByType",
                "Companies.GetAll"
            };
            public static readonly string[] BrokeragesCreateUpdateDelete = {
                "Brokerages.Create", "Brokerages.Update","Brokerages.Delete"
            };
            public static readonly string[] ManagerUsersRead = {
                "ManagerUsers.Get","ManagerUsers.GetAll"
            };
            public static readonly string[] ManagerUsersCreateUpdateDelete = {
                "ManagerUsers.Create","ManagerUsers.Update","ManagerUsers.Delete"
            };
            public static readonly string[] UnderwritingFullAccess = {
               "Products.Toggle", "Products.GetConditions","Products.AttachCondition","Endorsements.GetAll","Endorsements.Reply"
            };
            public static readonly string[] ClaimsCreateDeleteManageDocuments = {
                "Policies.CreateClaim","Claims.AttachDocuments","Claims.Delete","Claims.DeleteDocument"
            };
            public static readonly string[] ClaimsChangeStatus = {
                "Claims.ChangeStatus"
            };
            public static readonly string[] AllPermissions = BasicPermissions.Concat(BrokeragesCreateUpdateDelete)
                                                                            .Concat(ManagerUsersRead)
                                                                            .Concat(ManagerUsersCreateUpdateDelete)
                                                                            .Concat(UnderwritingFullAccess)
                                                                            .Concat(ClaimsCreateDeleteManageDocuments)
                                                                            .Concat(ClaimsChangeStatus).ToArray();
        }

        public static class BrokerPermissions
        {
            public const long GroupId = 2;
            public static readonly string[] BasicPermissions = {
                "Brokerage.Get","Brokerage.TestAlias",
                "EmailsMarketing.Create","EmailsMarketing.Get",
                "Campaigns.Create","Campaigns.Get","Campaigns.GetAll","Campaigns.GetTotalbyType","Campaigns.GetTotalbyTypeView","Campaigns.GetTotalbyTypePeriod","Campaigns.CreateEmailMarketing","Campaigns.CreateBrochure",
                "Claims.GetAll","Claims.Get","Claims.GetMapPoints","Claims.GetTotalInterval","Claims.ChangeStatus","Claims.AttachDocuments","Claims.DeleteDocument", "Claims.Update",
                "Insureds.FindAll","Insureds.Get","Insureds.GetAll","Insureds.Claims","Insureds.Endorsements","Insureds.GetMapPoints","Insureds.CountByStatus","Insureds.GetTotalLastYear","Insureds.Update",
                "Policies.FindAll","Policies.Claims","Policies.CreateClaim","Policies.GetAll","Policies.Get","Policies.Endorsements","Policies.Orders","Policies.GetMapPoints","Policies.GetDashboardTypes","Policies.GetTotalInterval","Policies.GetTotalGeneral","Policies.CountInEffect","Policies.GetTotalLastYear","Policies.GetRenewalsTotal","Policies.RegenPolicy",
                "WebpageViews.GetTotalInterval", "WebpageViews.GetTotalWeek", "WebpageViews.GetTotalWebBanner"
            };
            public static readonly string[] BrokersCreateReadUpdate = {
                "Brokers.Inactivate", "Brokers.GetAll","Brokers.Get", "Brokers.Create","Brokers.Update","Brokers.GetTotalStatus","Brokers.GetTotalLastMonth","Brokers.GetTotalLastYear"
            };
            public static readonly string[] BrokersDelete = {
                 "Brokers.Delete"
            };
            public static readonly string[] BrokeragesOnboardingUpdate = {
                 "Brokerage.Onboarding","Brokerage.Update","Brokerage.PutDefaultWebpage","Brokerage.ToggleProduct","Brokerage.CreateChargeAccount","Brokerage.UpdateChargeAccount","Brokerage.GetChargeAccount"
            };
            public static readonly string[] AllPermissions = BasicPermissions.Concat(BrokersCreateReadUpdate)
                                                                            .Concat(BrokersDelete)
                                                                            .Concat(BrokeragesOnboardingUpdate).ToArray();
        }

        #endregion
    }
}