﻿using System;
namespace ProtectorUS.Model
{
    public class Permission : AuditableEntity<long>
    {
        public string Name { get; set; }
        public string Key { get { return ActionApi.Simplify(); } }
        public string ActionApi { get; set; }
        public decimal? Order { get; set; }
        public long GroupId { get; set; }
        public PermissionGroup Group { get; set; }
        public Permission()
        {
        }
        public Permission(string actionApi, long groupId, decimal? order = null, string name = null)
        {
            ActionApi = actionApi;
            GroupId = groupId;
            Order = order;
            Name = name ?? Key;
        }
    }
}