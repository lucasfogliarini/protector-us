﻿namespace ProtectorUS.Model
{
    public class Device : AuditableEntity<long>
    {
        internal Device()
        {
        }
        public Device(string deviceId, string name, long userId)
        {
            DeviceId = deviceId;
            Name = name;
            UserId = userId;
            Active = true;
        }
        public string DeviceId { get; set; }

        public bool Active { get; set; }

        public string Name { get; set; }

        public User User { get; set; }

        public long UserId { get; set; }

    }
}