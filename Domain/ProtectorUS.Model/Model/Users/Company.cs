﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Company : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public string Name { get; set; }
        public IList<ManagerUser> ManagerUsers { get; set; }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Name) };
            }
        }
    }
}
