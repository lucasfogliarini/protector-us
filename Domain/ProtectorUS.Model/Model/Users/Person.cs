﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Person : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public Gender? Gender
        {
            get
            {
                switch (SocialTitle)
                {
                    case Model.SocialTitle.Mister:
                        return Model.Gender.Male;
                    case Model.SocialTitle.Miss:
                        return Model.Gender.Female;
                    case Model.SocialTitle.Mrs:
                        return Model.Gender.Male;
                    case Model.SocialTitle.Ms:
                        return Model.Gender.Female;
                    default:
                        return null;
                }
            }
        }
        public DateTime? BirthDate { get; set; }
        public SocialTitle? SocialTitle { get; set; }
        public IList<Phone> Phones { get; set; }
        public IList<Address> Addresses { get; set; }
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        public const int EmailMaxLength = 250;
        public const int FirstNameMaxLength = 50;
        public const int LastNameMaxLength = 50;

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { $"{nameof(Email)},{PersonType}" };
            }
        }
        public const string PersonType = "PersonType";
    }
}
