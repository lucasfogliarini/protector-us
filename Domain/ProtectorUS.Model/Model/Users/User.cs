﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    using Security.Cryptography;
    public class User : Person
    {
        internal User()
        {
        }
        public User(string email, string passwordPlain = null)
        {
            Email = email;
            GeneratePassword(passwordPlain);
        }
        public UserStatus Status { get; set; }
        public string Password { get; private set; }
        public string FacebookId { get; set; }
        public string LinkedinId { get; set; }
        public IList<Profile> Profiles { get; set; }
        public IList<Device> Devices { get; set; }
        public IList<ExceptedPermission> ExceptedPermissions { get; set; }
        public string PasswordPlain { get; private set; }
        public string LastChange
        {
            get
            {
                int totalchangesday = UpdatedDate.HasValue ? Convert.ToInt32(DateTime.Now.Subtract(UpdatedDate.Value).TotalDays) : 0;
                return totalchangesday == 0 ? "today" : totalchangesday.ToString() + (totalchangesday == 1 ? " day ago" : " days ago");
            }
        }
        public void GeneratePassword(string passwordPlain = null)
        {
            PasswordPlain = passwordPlain ?? Guid.NewGuid().ToString();
            Password = PBKDF2Hasher.GenerateHash(PasswordPlain);
        }
        public bool PasswordMatch(string passwordPlain)
        {
            return PBKDF2Hasher.Match(Password, passwordPlain);
        }

        public const int PasswordMinLength = 8;
        public const string PasswordPattern = "(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,50})";
        public const string PasswordPatternMessage = "Password must contain at least 8 characters, with at least one letter and one number.";
        public const string AnonymousUserPicture = "images/anonymous_user.png";

        public static class ClaimTypes
        {
            public const string IdUserAuthenticated = "IdUserAuthenticated";
            public const string client_id = "client_id";
            public const string permission = "permission";
        }
    }
}
