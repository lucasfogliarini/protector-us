﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class PermissionGroup : Entity<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long Order { get; set; }
        public long? GroupId { get; set; }
        public PermissionGroup Group { get; set; }
        public IList<PermissionGroup> Groups { get; set; }
        public IList<Permission> Permissions { get; set; }
    }
}