﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Broker : User
    {
        internal Broker()
        {
        }
        public Broker(string email, string passwordPlain = null) : base(email, passwordPlain)
        {
        }
        public long BrokerageId { get; set; }
        public Brokerage Brokerage { get; set; }
        public IList<Quotation> Quotations { get; set; }
        public IList<Campaign> Campaigns { get; set; }
        public IList<Policy> Policies { get; set; }
        public static new class ClaimTypes
        {
            public const string BrokerageId = "BrokerageId";
        }
    }
}
