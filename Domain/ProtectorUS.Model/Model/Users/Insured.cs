﻿using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model
{
    public class Insured : User
    {
        internal Insured()
        {
        }
        public Insured(string email, string passwordPlain = null) : base(email,passwordPlain)
        {
        }

        public IList<Policy> Policies { get; set; }
        public IList<Declined> Declineds { get; set; }
        public bool HasActivePolicy
        {
            get
            {
                return Policies == null ? false : Policies.Any(p => p.Status == PolicyStatus.InEffect || p.Status == PolicyStatus.OnHold);
            }
        }
        public IEnumerable<Business> Businesses
        {
            get
            {
                List<Business> businessDistinct = new List<Business>();
                var businesses = Policies?.Select(e => e.Business);
                if (businesses != null)
                {
                    foreach (var business in businesses)
                    {
                        if (!businessDistinct.Any(b => b.Id == business.Id))
                            businessDistinct.Add(business);
                    }
                }            
                return businessDistinct.AsReadOnly();
            }
        }

        public bool? AcceptedTerms { get; set; }
    }
}
