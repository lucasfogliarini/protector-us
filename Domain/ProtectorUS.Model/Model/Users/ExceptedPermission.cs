﻿namespace ProtectorUS.Model
{
    public class ExceptedPermission : AuditableEntity<long>
    {
        public Permission Permission { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool Allowed { get; set; }
    }
}