﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum SocialTitle
    {
        [Description("Mr.")]
        Mister = 1,
        [Description("Ms.")]
        Ms = 2,
        [Description("Miss.")]
        Miss = 3,
        [Description("Mrs.")]
        Mrs = 4,
    }
}