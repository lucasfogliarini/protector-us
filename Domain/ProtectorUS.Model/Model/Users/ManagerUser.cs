﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class ManagerUser : User
    {
        internal ManagerUser()
        {
        }
        public ManagerUser(string email, string passwordPlain = null) : base(email,passwordPlain)
        {
        }
        public long CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
