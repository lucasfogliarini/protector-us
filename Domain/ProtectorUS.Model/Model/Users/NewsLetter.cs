﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class NewsLetter : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public string Email { get; set; }
        public string Origin { get; set; }
        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Email) };
            }
        }
    }
}
