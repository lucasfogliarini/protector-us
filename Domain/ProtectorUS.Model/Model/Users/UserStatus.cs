﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum UserStatus
    {
        [Description("Inactive")]
        Inactive = 0,
        [Description("Active")]
        Active = 1,
        [Description("Onboarding")]
        Onboarding = 2,
    }
}
