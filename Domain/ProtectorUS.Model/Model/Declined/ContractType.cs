﻿/// <summary>
/// Determines de contract application types.
/// </summary>
public enum ContractType
{
    New = 0,
    Renewal = 1
};