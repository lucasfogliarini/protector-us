﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an declined from API.
    /// </summary>
    public class Declined : AuditableEntity<long>
    {
        public ContractType ContractType { get; set; }
        public long InsuredId { get; set; }
        public Insured Insured { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public IList<DeclinedBlock> DeclinedBlocks { get; set; }
    }
}
