﻿using System;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an declined unblock from API.
    /// </summary>
    public class DeclinedUnblock : AuditableEntity<long>
    {
        public string UnblockReason { get; set; }
        public DateTime Date { get; set; }
        public long ManagerUserId { get; set; }
        public ManagerUser ManagerUser { get; set; }
        public DeclinedBlock DeclinedBlock { get; set; }
    }

}
