﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an declined rule from API.
    /// </summary>
    public class DeclinedRule : ExpressionRule
    {
        public string Description { get; set; }
        public ContractType ContractType { get; set; }
        public string Reason { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public DeclinedRule(string _propetyName, string _operator, string _targetValue):base(_propetyName, _operator, _targetValue)
        {
        }
    }

}
