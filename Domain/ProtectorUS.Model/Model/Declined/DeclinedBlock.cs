﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an declined block from API.
    /// </summary>
    public class DeclinedBlock : AuditableEntity<long>
    {
        public string BlockReason { get; set; }
        public DateTime Date { get; set; }
        public long? ManagerUserId { get; set; }
        public ManagerUser ManagerUser { get; set; }
        public long DeclinedId { get; set; }
        public Declined Declined { get; set; }
        public IList<DeclinedRule> DeclinedRules { get; set; }
        public DeclinedUnblock DeclinedUnblock { get; set; }
    }

}
