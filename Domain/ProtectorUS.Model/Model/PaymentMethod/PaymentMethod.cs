﻿namespace ProtectorUS.Model
{
    public class PaymentMethod : AuditableEntity<long>, IUniqueKeySoftDeletable
    {

        public string Name { get; set; }
        public string Icon { get; set; }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[]{ nameof(Name) };
            }
        }
    }
}
