using System.Text.RegularExpressions;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an information credit card .
    /// </summary>
    public class CreditCardInfo
    {
        public string CardHoldName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Number { get; set; }
        public string Cvc { get; set; }
        public string ExpirationYear { get; set; }
        public string ExpirationMonth { get; set; }

        const string VisaPattern = "^4[0-9]{12}(?:[0-9]{3})?$";
        const string MasterCardPattern = "^5[1-5][0-9]{14}$";
        const string AmericanExpressPattern = "^3[47][0-9]{13}$";
        const string DinersClubPattern = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
        const string DiscoverPattern = "^6(?:011|5[0-9]{2})[0-9]{12}$";
        const string JCBPattern = @"^(?:2131|1800|35\d{3})\d{11}$";

        public static CreditCardBrand CreditCardBrand(string number = null)
        {
            if (Regex.Match(number, VisaPattern).Success)
            {
                return  Model.CreditCardBrand.Visa;
            }
            else if (Regex.Match(number, MasterCardPattern).Success)
            {
                return Model.CreditCardBrand.MasterCard;
            }
            else if (Regex.Match(number, AmericanExpressPattern).Success)
            {
                return Model.CreditCardBrand.AmericanExpress;
            }
            else if (Regex.Match(number, DiscoverPattern).Success)
            {
                return Model.CreditCardBrand.Discover;
            }
            else if (Regex.Match(number, DinersClubPattern).Success)
            {
                return Model.CreditCardBrand.DinersClub;
            }
            else if (Regex.Match(number, JCBPattern).Success)
            {
                return Model.CreditCardBrand.JCB;
            }
            return Model.CreditCardBrand.Invalid;
        }
    }
}
