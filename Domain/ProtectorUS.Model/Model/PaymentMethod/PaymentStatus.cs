﻿/// <summary>
/// Determines de Payment Status application types.
/// </summary>
public enum PaymentStatus
{
    Pending = 0,
    Approved = 1,
    Denied = 2
};