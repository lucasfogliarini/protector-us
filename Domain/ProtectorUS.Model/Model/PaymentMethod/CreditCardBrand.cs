﻿namespace ProtectorUS.Model
{
    public enum CreditCardBrand
    {
        Invalid = 0,
        MasterCard = 1,
        Visa = 2,
        AmericanExpress = 3,
        Discover = 4,
        DinersClub = 5,
        JCB = 6
    }
}
