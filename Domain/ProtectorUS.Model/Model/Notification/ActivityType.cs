﻿namespace ProtectorUS.Model
{
    public enum ActivityType
    {
        ClaimStatusChange,
        PolicyMustBeRenewal,
        BrokerageOnBoardingCompleted,
        AgentOnBoardingCompleted,
        PolicyCancelRequest,
    }
}
