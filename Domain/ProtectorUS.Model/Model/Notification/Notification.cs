﻿using System;

namespace ProtectorUS.Model
{
    public class Notification : AuditableEntity<long>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public long? ActorId { get; set; }
        public ActorType? ActorType { get; set; }
        public long? SourceId { get; set; }
        public SourceType? SourceType { get; set; }
        public long? DestinationId { get; set; }
        public DestinationType? DestinationType { get; set; }
        public NotificationType? Type { get; set; }
        public ActivityType? ActivityType { get; set; }
        public NotificationEventType EventType { get; set; }
        public ApplicationTypes? AppType { get; set; }
        public bool Sent { get; set; }
        public DateTime? SendDate { get; set; }
        public bool Read { get; set; }
        public DateTime? ReadDate { get; set; }
        public bool? Answered { get; set; }
        public DateTime? ExpiresOn { get; set; }

    }
}
