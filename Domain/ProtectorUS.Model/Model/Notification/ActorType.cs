﻿namespace ProtectorUS.Model
{
    public enum ActorType
    {
        Insured,
        Broker,
        ManagerUser,
        System
    }
}
