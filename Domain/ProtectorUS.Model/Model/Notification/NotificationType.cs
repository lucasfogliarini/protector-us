﻿namespace ProtectorUS.Model
{

    public enum NotificationType
    {
        ReadOnly,
        NeedsResponse
    }
}
