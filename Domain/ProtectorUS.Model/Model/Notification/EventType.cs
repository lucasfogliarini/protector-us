﻿namespace ProtectorUS.Model
{ 
    public enum NotificationEventType
    {
        //Dont't need a object as source
        Event,
        //Need a object as source
        EventAboutAnObject,
        //Need a object as source and an actor as agent (ex: the bkoker change the claim PR-00115 owned by Fulana de Tal
        EventAboutAnObjectWithAgent,
        //Need a object  as source, an object owner and an actor as agent (ex: the broker change the claim PR-00115 status owned by Fulana de Tal
        EventAboutAnObjectOwnedByWithAgent
    }
}
