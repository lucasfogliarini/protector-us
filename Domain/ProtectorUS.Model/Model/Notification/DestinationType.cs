﻿namespace ProtectorUS.Model
{
    public enum DestinationType
    {
        Insured,
        Broker,
        ManagerUser
    }
}
