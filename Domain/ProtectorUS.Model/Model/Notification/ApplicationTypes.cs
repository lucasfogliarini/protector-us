﻿using System.ComponentModel;

namespace ProtectorUS.Model {
    public enum ApplicationTypes
    {
        [Description("javascript")]
        JavaScript = 0,
        [Description("ios")]
        IOS = 1,
        [Description("android")]
        Android = 2
    };
}
