﻿namespace ProtectorUS.Model
{
    public enum SourceType
    {
        Claim,
        Policy,
        Agent,
        Broker
    }
}
