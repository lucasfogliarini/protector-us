﻿using System;

namespace ProtectorUS.Model
{
    public class AppClient : AuditableEntity<long>, ISwitch, IUniqueKeySoftDeletable
    {
        public AppClient()
        {
        }
        public AppClient(string token)
        {
            Token = token;
        }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int RefreshTokenLifeTime { get; set; }
        public string AllowedOrigin { get; set; }
        public string CurrentVersion { get; set; }
        public string ReviewVersion { get; set; }
        public string Token { get; private set; }
        public bool InReview { get; set; }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Name), nameof(ClientId) };
            }
        }

        public static string GenerateToken()
        {
            string guid = Guid.NewGuid().ToString("N");
            return guid.Substring(0,TokenMinLength);
        }

        public const string Android = "andoid";
        public const string IOS = "ios";
        public const string Web = "web";
        public const int TokenMinLength = 8;
    }
}
