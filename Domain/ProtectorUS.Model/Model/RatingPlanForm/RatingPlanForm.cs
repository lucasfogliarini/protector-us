﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class RatingPlanForm : AuditableEntity<long>, ISwitch
    {
        public long StateId { get; set; }
        public State State { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public bool Active { get; set; }
        public IList<RatingPlanFormVersion> RatingPlanFormVersions { get; internal set; }
        public void AddNewVersion(int version)
        {
            RatingPlanFormVersions = RatingPlanFormVersions ?? new List<RatingPlanFormVersion>();
            RatingPlanFormVersions.Add(new RatingPlanFormVersion(version));
        }

    }
}
