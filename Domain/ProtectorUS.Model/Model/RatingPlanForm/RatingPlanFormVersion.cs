﻿namespace ProtectorUS.Model
{
    public class RatingPlanFormVersion : AuditableEntity<long>
    {
        internal RatingPlanFormVersion()
        {
        }
        public RatingPlanFormVersion(int version)
        {
            Version = version;
        }
        public int Version { get; set; }
        public long RatingPlanFormId { get; set; }
        public RatingPlanForm RatingPlanForm { get; set; }

    }
}
