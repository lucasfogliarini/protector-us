﻿using ProtectorUS.Exceptions;
using ProtectorUS.Security.Cryptography;
using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    using DocumentDB.RatingPlan;
    using System.Linq;
    using DocumentDb = DocumentDB.RatingPlan;
    public abstract class RatingPlanComputer
    {
        public RatingPlanComputer(DocumentDb.RatingPlanForm ratingPlanForm, long productId, DateTime? policyStartDate = null)
        {
            RatingPlanForm = ratingPlanForm;
            ProductId = productId;
            PolicyStartDate = policyStartDate?? DateTime.Now;
        }
        protected RatingPlanComputer(long ratingPlanFormVersionId, long productId, long regionId, int insuredTimezone)
        {
            RatingPlanFormVersionId = ratingPlanFormVersionId;
            ProductId = productId;
            RegionId = regionId;
            InsuredTimezone = insuredTimezone;
        }
        public static RatingPlanComputer Get(string hashCode)
        {
            try
            {
                var textPlain = Rijndael.Decrypt(hashCode);
                long ratingPlanFormVersionId = Convert.ToInt64(textPlain.Split('#')[0]);
                long productId = Convert.ToInt64(textPlain.Split('#')[1]);
                long regionId = Convert.ToInt64(textPlain.Split('#')[2]);
                int insuredTimezone = Convert.ToInt32(textPlain.Split('#')[3]);
                switch (productId)
                {
                    case Product.ArchitectectsEngineersId:
                        return new EngineersComputer(ratingPlanFormVersionId, productId, regionId, insuredTimezone);
                    case Product.AccountantsId:
                        return new AccountantsComputer(ratingPlanFormVersionId, productId, regionId, insuredTimezone);
                }
                return null;
            }
            catch
            {
                throw new InvalidException(e => e.EL010);
            }
        }

        #region props
        protected decimal BasePremium { get; private set; }
        public long RatingPlanFormVersionId { get; private set; }
        public long ProductId { get; private set; }
        public long RegionId { get; private set; }
        public int InsuredTimezone { get; private set; }
        public DateTime InsuredTimeNow
        {
            get
            {
                return DateTime.UtcNow.AddSeconds(InsuredTimezone);
            }
        }
        public bool IsDeclined {
            get
            {
                return DeclineReasons != null ? DeclineReasons.Count > 0 : false; 
            }
        }
        public decimal Rounding { get; private set; }
        public DateTime PolicyStartDate { get; private set; }
        public DocumentDb.RatingPlanForm RatingPlanForm { get; set; }
        public RiskAnalysis RiskAnalysis { get; private set; }
        public decimal Revenue { get; protected set; }

        public IList<DeclineReason> DeclineReasons { get; set; }
        #endregion

        #region methods
        public string GenerateHash()
        {
            return GenerateHash(RatingPlanFormVersionId, ProductId, RegionId, InsuredTimezone);
        }
        public static string GenerateHash(long ratingPlanFormVersionId, long productId, long regionId, int insuredTimezone)
        {
            return Rijndael.Encrypt($"{ratingPlanFormVersionId}#{productId}#{regionId}#{insuredTimezone}");
        }
        protected virtual void BeforeValidate()
        {
        }
        public void Validate(IEnumerable<RiskAnalysisField> riskAnalysisFields)
        {
            try
            {
                RiskAnalysis = new RiskAnalysis(riskAnalysisFields);
                BeforeValidate();
                foreach (var ratingPlanField in RatingPlanForm.fields)
                {
                    var riskAnalysisField = RiskAnalysis.GetField(ratingPlanField.name);
                    if (riskAnalysisField != null)
                    {
                        if (!ratingPlanField.Validate(riskAnalysisField.Value))
                            throw new ArgumentInvalidException(e => e.EL012, ratingPlanField.name);

                        var option = ratingPlanField.GetOption(riskAnalysisField.Value);
                        if (option != null)
                        {
                            riskAnalysisField.Factor = option.factor;
                            riskAnalysisField.ValueLabel = option.label;
                        }
                    }
                    else if (ratingPlanField.IsRequired(RiskAnalysis))
                        throw new ArgumentInvalidException(e => e.EL012, ratingPlanField.name);
                }
                PolicyStartDate = RiskAnalysis.GetFieldValue<DateTime>(startingDateField);
                if (PolicyStartDate < InsuredTimeNow.Date)
                    throw new ArgumentInvalidException(e => e.ES003, startingDateField);
            }
            catch (ArgumentInvalidException ex)
            {
                throw ex;
            }
            catch
            {
                throw new ArgumentInvalidException(e => e.EL010);
            }
        }

        #endregion

        #region fields

        public const string revenueField = "revenue";
        public const string deductibleField = "deductible";
        public const string startingDateField = "startingDate";
        public const string coverageField = "coverage";
        public const string existingPolicyField = "existingPolicy";
        public const string retroactiveDateField = "retroactiveDate";

        #endregion

        #region calculation steps
        public const string basePremiumStep = "Base Premium";
        public const string priorActsCoverageStep = "Prior Acts Coverage";
        public const string coverageLimitStep = "Coverage Limit and Deductible";
        #endregion

        #region calculation
        public abstract decimal CalculatePolicyPremium();
        /// <summary>
        /// Calculate RateMod and insert to RiskAnalysis.MetaData
        /// </summary>
        protected void CalculateRateMod(decimal finalPremium)
        {
            if (BasePremium <= 0)
            {
                throw new ArgumentInvalidException(e => e.EL053);
            }
            decimal rateMod = finalPremium / BasePremium;
            RiskAnalysis.MetaData.Add(RiskAnalysis.rateModKey, rateMod.ToString("n"));
        }

        /// <summary>
        /// Round the premium and CalculateRateMod
        /// </summary>
        /// <returns>Final Premium</returns>
        protected decimal FinalizePremium(decimal currentPremium)
        {
            decimal finalPremium = RoundPremium(currentPremium);
            CalculateRateMod(finalPremium);
            VerifyMaxRevenueDecline();
            return finalPremium;
        }

        /// <summary>
        /// Add decline reasons to quote by question field name
        /// </summary>
        public virtual void VerifyMaxRevenueDecline()
        {
            if(Revenue > RatingPlanForm.maxRevenue)
            { 
                DeclineReasons = DeclineReasons ?? (DeclineReasons = new List<DeclineReason>());
                DeclineReasons.Add(new DeclineReason() { QuestionFieldName = revenueField });
            }
        }
        protected decimal Multiply(string calculationStep, decimal currentPremium, decimal factor)
        {
            currentPremium = currentPremium * SafeFactor(factor);
            RiskAnalysis.AddStep(calculationStep, currentPremium, factor, CalculationStepType.Factor);
            return currentPremium;
        }
        protected decimal Sum(string calculationStep, decimal currentPremium, decimal value)
        {
            currentPremium = currentPremium + value;
            RiskAnalysis.AddStep(calculationStep, currentPremium, value, CalculationStepType.Addition);
            return currentPremium;
        }
        public virtual decimal CalculateBasePremium(decimal revenue)
        {
            var basePremiumRanges = RatingPlanForm.basePremium.ranges.OrderBy(r => r.maxRevenue).ToList();
            BasePremiumRange basePremiumRange = basePremiumRanges.FirstOrDefault(b => revenue <= b.maxRevenue);
            basePremiumRange = basePremiumRange ?? basePremiumRanges.Last();
            BasePremium = basePremiumRange.basePremium;
            int prevMaxRevenueIndex = basePremiumRanges.IndexOf(basePremiumRange) - 1;
            if (prevMaxRevenueIndex >= 0)
            {
                decimal revenueExceeded = revenue - basePremiumRanges.ElementAt(prevMaxRevenueIndex).maxRevenue;
                decimal incrementalRate = GetIncrementalRate(revenueExceeded);
                BasePremium += incrementalRate * basePremiumRange.plus;
            }
            RiskAnalysis.AddStep(basePremiumStep, BasePremium, null, CalculationStepType.Base);
            return BasePremium;
        }
        /// <summary>
        /// i.e: for 3500 exceded with 1000 incrementalRate the value is 3. 
        /// </summary>
        private decimal GetIncrementalRate(decimal revenueExceeded)
        {
            decimal incrementalRate = revenueExceeded / RatingPlanForm.basePremium.incremetalRate;
            return Math.Floor(incrementalRate);
        }
        public virtual decimal CalculatePriorActsCoverage(decimal currentPremium, DateTime? retroactiveDate)
        {
            int retroactiveYears = GetRetroactiveYears(retroactiveDate);
            RiskAnalysis.MetaData.Add(RiskAnalysis.retroactiveYearsKey, retroactiveYears.ToString());
            var priorActsCoverage = RatingPlanForm.priorActsCoverages.FirstOrDefault(pc => Convert.ToDouble(pc.years) == retroactiveYears);
            if (priorActsCoverage == null)
            {
                priorActsCoverage = RatingPlanForm.priorActsCoverages.OrderBy(pc => pc.years).Last();
                if(retroactiveYears < priorActsCoverage.years)
                    priorActsCoverage.factor = 1;
            }
            return Multiply(priorActsCoverageStep, currentPremium, priorActsCoverage.factor);
        }
        public virtual decimal CalculateCoverage(decimal currentPremium, string coverageValue, string deductibleValue)
        {
            decimal coverageFactor = RatingPlanForm.GetFieldFactor(coverageField, coverageValue.Replace(",",""));
            decimal deductibleFactor = RatingPlanForm.GetFieldFactor(deductibleField, deductibleValue.Replace(",", ""));
            decimal factor = coverageFactor + deductibleFactor;
            return Multiply(coverageLimitStep, currentPremium, factor);
        }
        public virtual decimal ChecksMinimumPremium(decimal currentPremium)
        {
            return currentPremium > RatingPlanForm.minPremium ? currentPremium : RatingPlanForm.minPremium;
        }
        public decimal RoundPremium(decimal currentPremium)
        {
            decimal premiumRounding = RatingPlanForm.rounding ? Round(currentPremium) : currentPremium;
            Rounding = currentPremium - premiumRounding;
            return premiumRounding;
        }
        public decimal Round(decimal value)
        {
            return Math.Round(value, MidpointRounding.AwayFromZero);
        }
        public decimal CalculateCheckingDecline(decimal currentPremium, string fieldName, string value)
        {
            decimal factor = GetFactorCheckingDecline(fieldName, value);
            return Multiply(fieldName, currentPremium, factor);
        }

        protected decimal GetHighestFactor(string fieldName, string value)
        {
            string[] values = Field.SplitValue(value);
            if (value.Length == 0)
                return 0;
            var selectedValues = RatingPlanForm.GetField(fieldName).options.Where(o => values.Contains(o.value));
            decimal highestFactor = selectedValues.Max(o => o.factor);
            return highestFactor;
        }
        protected decimal GetFactorCheckingDecline(string fieldName, string value)
        {
            return CheckDecline(fieldName, value).factor;
        }

        /// <summary>
        /// Add decline reasons to quote by question field name
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected Option CheckDecline(string fieldName, string value)
        {
            Field field = RatingPlanForm.GetField(fieldName);
            Option option = field.GetOption(value);

            if (option != null && option.isDeclined)
            {
                DeclineReasons = DeclineReasons ?? (DeclineReasons = new List<DeclineReason>());
                DeclineReasons.Add(new DeclineReason()
                {
                    QuestionFieldName = fieldName
                });
            }
            return option;
        }

        protected Option GetOption(string fieldName, string value)
        {
            Field field = RatingPlanForm.GetField(fieldName);
            return field.GetOption(value);
        }
        protected int GetRetroactiveYears(DateTime? retroactiveDate)
        {
            int years = 0;
            if (retroactiveDate != null)
            {
                TimeSpan time = PolicyStartDate - retroactiveDate.GetValueOrDefault();
                double totalYears = time.TotalDays / 365.25;
                years = (int)Math.Round(totalYears);
            }
            if (years < 0)
                throw new ArgumentInvalidException(e => e.EL016, retroactiveDateField);
            return years;

        }
        protected decimal GetBusinessManagmentFactor(double businessYears)
        {
            var businessManagementOrdered = RatingPlanForm.businessManagement.OrderByDescending(i => i.years);
            foreach (var businessManagement in businessManagementOrdered)
            {
                if (businessYears > businessManagement.years)
                    return businessManagement.factor;
            }
            return 0;
        }
        protected decimal SafeFactor(decimal factor, decimal minimumValue = 0.5m)
        {
            return factor < minimumValue ? minimumValue : factor;
        }

        protected double GetBusinessYears(DateTime date)
        {
            TimeSpan time = PolicyStartDate - date;
            //decimal years = (DateTime.MinValue + time).Year - 1;// because we start at year 1 for the Gregorian calendar, we must subtract a year here.
            double years = time.TotalDays / 365.25d;
            return time.Ticks <= 0 ? -1 : years;
        }
        #endregion
    }
}
