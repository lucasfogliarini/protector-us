﻿using System;

namespace ProtectorUS.Model
{
    using DocumentDB.RatingPlan;
    using System.Collections.Generic;
    using System.Linq;
    public class AccountantsComputer : RatingPlanComputer
    {
        public AccountantsComputer(DocumentDB.RatingPlan.RatingPlanForm ratingPlanForm, DateTime? policyStartDate = null) :
            base(ratingPlanForm, Product.AccountantsId, policyStartDate)
        {
        }
        public AccountantsComputer(long ratingPlanFormVersionId, long productId, long regionId, int insuredTimezone) :
            base(ratingPlanFormVersionId, productId, regionId, insuredTimezone)
        {
        }

        #region fields
        public const string staffNumberField = "staffNumber";
        //public const string writtenContractsField = "writtenContracts";
        public const string riskManagementField = "riskManagement";
        public const string revenueFromServicesField = "revenueFromServices";
        public const string revenueFromIndustriesField = "revenueFromIndustries";//clients factor
        public const string revenueFromSegmentsField = "revenueFromSegments";//practice factor
        public const string claimsLastFiveField = "claimsLastFive";//experience modification factor
        public const string businessManagementField = "businessManagement";
        public const string professionalMembershipsField = "professionalMemberships";
        public const string defenseOutsideLimitField = "defenseOutsideLimit";
        #endregion

        #region calculation steps
        public const string staffCreditsStep = "Staff Credits";
        public const string premiumModificationStep = "Premium Modification";
        public const string scheduleModificationStep = "Schedule Modification";
        public const string defenseOutsideLimitStep = "Defense Outside Limit";
        #endregion

        protected override void BeforeValidate()
        {
            RiskAnalysis.AddField(defenseOutsideLimitField, "1");
        }
        public override decimal CalculatePolicyPremium()
        {
            #region fields
            Revenue = RiskAnalysis.GetFieldValue<decimal>(revenueField);
            string deductible = RiskAnalysis.GetFieldValue(deductibleField);
            string coverage = RiskAnalysis.GetFieldValue(coverageField);
            string claimsLastFiveValue = RiskAnalysis.GetFieldValue(claimsLastFiveField);
            string professionalMembershipsValue = RiskAnalysis.GetFieldValue(professionalMembershipsField);
            bool existingPolicy = RiskAnalysis.GetFieldValue<bool>(existingPolicyField);
            DateTime? retroactiveDate = null;
            if (existingPolicy)
                retroactiveDate = RiskAnalysis.GetFieldValue<DateTime>(retroactiveDateField);
            DateTime businessFoundationDate = RiskAnalysis.GetFieldValue<DateTime>(businessManagementField);
            string riskManagement = RiskAnalysis.GetFieldValue(riskManagementField);
            string revenueFromServices = RiskAnalysis.GetFieldValue(revenueFromServicesField);
            string revenueFromSegments = RiskAnalysis.GetFieldValue(revenueFromSegmentsField);
            string revenueFromIndustries = RiskAnalysis.GetFieldValue(revenueFromIndustriesField);
            int staffNumber = RiskAnalysis.GetFieldValue<int>(staffNumberField);
            bool defenseOutsideLimit = RiskAnalysis.GetFieldValue<bool>(defenseOutsideLimitField);
            #endregion

            //IsDeclined = Revenue > RatingPlanForm.maxRevenue;
            decimal premium = CalculateBasePremium(Revenue);
            premium = CalculateRevenueStaffCredits(premium, Revenue, staffNumber);
            premium = CalculatePriorActsCoverage(premium, retroactiveDate);
            premium = CalculateCheckingDecline(premium, revenueFromServicesField, revenueFromServices);
            premium = CalculatePremiumModification(premium, revenueFromIndustries, revenueFromSegments, riskManagement, claimsLastFiveValue, retroactiveDate);
            premium = CalculateCoverage(premium, coverage, deductible);
            premium = CalculateScheduleModifications(premium, businessFoundationDate, professionalMembershipsValue);
            premium = CalculateDefenseOutsideLimit(premium, coverage, defenseOutsideLimit);
            premium = ChecksMinimumPremium(premium, defenseOutsideLimit);
            return FinalizePremium(premium);
        }

        public decimal CalculateRevenueStaffCredits(decimal currentPremium, decimal revenue, int staffNumber)
        {
            decimal minPerMember = RatingPlanForm.revenueStaffCredit.minPerMember;
            decimal revenueStaffPremiumMinimum = minPerMember * staffNumber;
            if(currentPremium < revenueStaffPremiumMinimum)
                currentPremium = revenueStaffPremiumMinimum;

            decimal revenuePerMember = revenue / staffNumber;
            var revenueStaffCreditsOrdered = RatingPlanForm.revenueStaffCredit.credits.OrderBy(i => i.maxRevenuePerMember);
            decimal revenueStaffCreditFactor = 1;
            var revenueStaffCredit = revenueStaffCreditsOrdered.FirstOrDefault(sc => revenuePerMember <= sc.maxRevenuePerMember);
            if (revenueStaffCredit != null)
            {
                revenueStaffCreditFactor = revenueStaffCredit.factor;
            }
            return Multiply(staffCreditsStep, currentPremium, revenueStaffCreditFactor);
        }
        public decimal CalculatePremiumModification(decimal currentPremium, string revenueFromIndustriesValue, string revenueFromSegmentsValue, string riskManagementValue, string claimsLastFiveValue, DateTime? retroactiveDate)
        {
            decimal clientsFactor = GetHighestFactor(revenueFromIndustriesField, revenueFromIndustriesValue);
            decimal practiceFactor = GetFactorCheckingDecline(revenueFromSegmentsField, revenueFromSegmentsValue);
            decimal riskManagementFactor = GetOption(riskManagementField, riskManagementValue).factor;
            CheckDecline(claimsLastFiveField, claimsLastFiveValue);

            int claims = Convert.ToInt32(claimsLastFiveValue);
            int retroactiveYears = GetRetroactiveYears(retroactiveDate);
            decimal experienceModificationFactor = GetExperienceModificationFactor(claims, retroactiveYears);

            decimal premiumModificationFactor = clientsFactor + practiceFactor + riskManagementFactor + experienceModificationFactor;
            premiumModificationFactor += 1;
            return Multiply(premiumModificationStep, currentPremium, premiumModificationFactor);
        }
        public decimal CalculateScheduleModifications(decimal currentPremium, DateTime businessFoundationDate, string professionalMembershipsValue)
        {
            double businessYears = GetBusinessYears(businessFoundationDate);
            RiskAnalysis.MetaData.Add(RiskAnalysis.businessYearsKey, businessYears.ToString());
            decimal businessFactor = GetBusinessManagmentFactor(businessYears);
            businessFactor = RatingPlanForm.scheduleModifications.GetFairFactor(businessManagementField, businessFactor);

            decimal professionalMembershipsFactor = RatingPlanForm.GetFieldFactor(professionalMembershipsField, professionalMembershipsValue);
            professionalMembershipsFactor = RatingPlanForm.scheduleModifications.GetFairFactor(professionalMembershipsField, professionalMembershipsFactor);

            decimal scheduleModificationsFactor = businessFactor + professionalMembershipsFactor;
            scheduleModificationsFactor = ScheduleModifications.GetFairFactor(scheduleModificationsFactor, RatingPlanForm.scheduleModifications.min, RatingPlanForm.scheduleModifications.max);            
            scheduleModificationsFactor += 1;

            return Multiply(scheduleModificationStep, currentPremium, scheduleModificationsFactor);
        }
        public decimal CalculateDefenseOutsideLimit(decimal currentPremium, string coverageValue, bool defenseOutsideLimit)
        {
            decimal dolValue = 0;
            if (defenseOutsideLimit)
            {
                DefenseOutsideLimitFactor dolFactor = RatingPlanForm.defenseOutsideLimit.factors.FirstOrDefault(f => f.coverageValue == coverageValue);
                decimal defenseOutsideLimitFactor = SafeFactor(dolFactor.factor, 1);
                dolValue = currentPremium * (defenseOutsideLimitFactor - 1);
                dolValue = Round(dolValue);
                RiskAnalysis.OptionalCoverages.Add(defenseOutsideLimitField, dolValue);
            }
            return Sum(defenseOutsideLimitStep, currentPremium, dolValue);
        }
        public decimal ChecksMinimumPremium(decimal currentPremium, bool defenseOutsideLimit)
        {
            decimal minPremium = defenseOutsideLimit && RatingPlanForm.defenseOutsideLimit != null ? RatingPlanForm.defenseOutsideLimit.minPremium : RatingPlanForm.minPremium;
            return currentPremium > minPremium ? currentPremium : minPremium;
        }

        private decimal GetExperienceModificationFactor(int claims, int retroactiveYears)
        {
            IEnumerable<ExperienceModification> experienceModifications = RatingPlanForm.experienceModifications.OrderBy(r => r.maxRevenue);
            ExperienceModification experienceModification = experienceModifications.FirstOrDefault(b => Revenue <= b.maxRevenue);
            experienceModification = experienceModification ?? experienceModifications.Last();
            ExperienceModificationFactor experienceModificationFactor = experienceModification.items.OrderByDescending(i => i.minRetroactiveYear).FirstOrDefault(i => i.claims == claims && retroactiveYears >= i.minRetroactiveYear);
            return experienceModificationFactor == null ? 0 : experienceModificationFactor.factor;
        }
    }
}
