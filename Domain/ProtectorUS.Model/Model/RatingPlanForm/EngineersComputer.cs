﻿using ProtectorUS.Model.DocumentDB.RatingPlan;
using System;
using System.Linq;

namespace ProtectorUS.Model
{
    public class EngineersComputer : RatingPlanComputer
    {
        public EngineersComputer(DocumentDB.RatingPlan.RatingPlanForm ratingPlanForm, DateTime? policyStartDate = null) :
            base(ratingPlanForm, Product.ArchitectectsEngineersId, policyStartDate)
        {
        }
        public EngineersComputer(long ratingPlanFormVersionId, long productId, long regionId, int insuredTimezone) :
            base(ratingPlanFormVersionId, productId, regionId, insuredTimezone)
        {
        }

        #region fields
        public const string revenueFromHVACField = "revenueFromHVAC";
        //public const string writtenContractsField = "writtenContracts";
        public const string riskManagementField = "riskManagement";
        public const string revenueFromServicesField = "revenueFromServices";
        public const string businessManagementField = "businessManagement";
        public const string claimsLastFiveField = "claimsLastFive";
        public const string professionalMembershipsField = "professionalMemberships";
        public const string revenueFromSegmentsField = "revenueFromSegments";
        public const string firstDollarDefenseField = "firstDollarDefense";
        #endregion

        #region calculation steps
        public const string premiumModificationStep = "Premium Modification";
        public const string scheduleModificationStep = "Schedule Modification";
        public const string firstDollarDefenseStep = "First Dollar Defense";
        #endregion

        public override decimal CalculatePolicyPremium()
        {
            #region fields
            Revenue = RiskAnalysis.GetFieldValue<decimal>(revenueField);
            string deductible = RiskAnalysis.GetFieldValue(deductibleField);
            string coverage = RiskAnalysis.GetFieldValue(coverageField);
            string claimsLastFiveValue = RiskAnalysis.GetFieldValue(claimsLastFiveField);
            string professionalMembershipsValue = RiskAnalysis.GetFieldValue(professionalMembershipsField);
            bool existingPolicy = RiskAnalysis.GetFieldValue<bool>(existingPolicyField);
            DateTime? retroactiveDate = null;
            if (existingPolicy)
                retroactiveDate = RiskAnalysis.GetFieldValue<DateTime>(retroactiveDateField);
            DateTime businessFoundationDate = RiskAnalysis.GetFieldValue<DateTime>(businessManagementField);
            string riskManagement = RiskAnalysis.GetFieldValue(riskManagementField);
            string revenueFromServices = RiskAnalysis.GetFieldValue(revenueFromServicesField);
            string revenueFromSegments = RiskAnalysis.GetFieldValue(revenueFromSegmentsField);
            string revenueFromHVAC = RiskAnalysis.GetFieldValue(revenueFromHVACField);
            bool fdd = RiskAnalysis.GetFieldValue<bool>(firstDollarDefenseField);
            #endregion

            //IsDeclined = Revenue > RatingPlanForm.maxRevenue;
            decimal premium = CalculateBasePremium(Revenue);
            premium = CalculatePriorActsCoverage(premium, retroactiveDate);
            premium = CalculateCheckingDecline(premium, revenueFromHVACField, revenueFromHVAC);
            premium = CalculateCheckingDecline(premium, revenueFromServicesField, revenueFromServices);
            premium = CalculatePremiumModificationFactors(premium, revenueFromSegments);
            premium = CalculateScheduleModifications(premium, businessFoundationDate, professionalMembershipsValue);
            premium = CalculateCoverage(premium, coverage, deductible);
            premium = CalculateFirstDollarDefense(premium, deductible, fdd);
            premium = ChecksMinimumPremium(premium);
            return FinalizePremium(premium);
        }
        public decimal CalculatePremiumModificationFactors(decimal currentPremium, string revenueFromSegmentsValue)
        {
            currentPremium = CalculateCheckingDecline(currentPremium, revenueFromSegmentsField, revenueFromSegmentsValue);
            return currentPremium;
        }
        public decimal CalculateScheduleModifications(decimal currentPremium, DateTime businessFoundationDate, string professionalMembershipsValue)
        {
            double businessYears = GetBusinessYears(businessFoundationDate);
            RiskAnalysis.MetaData.Add(RiskAnalysis.businessYearsKey, businessYears.ToString());
            decimal businessFactor = GetBusinessManagmentFactor(businessYears);
            businessFactor = RatingPlanForm.scheduleModifications.GetFairFactor(businessManagementField, businessFactor);

            decimal professionalMembershipsFactor = RatingPlanForm.GetFieldFactor(professionalMembershipsField, professionalMembershipsValue);
            professionalMembershipsFactor = RatingPlanForm.scheduleModifications.GetFairFactor(professionalMembershipsField, professionalMembershipsFactor);

            decimal scheduleModificationsFactor = businessFactor + professionalMembershipsFactor;
            scheduleModificationsFactor = ScheduleModifications.GetFairFactor(scheduleModificationsFactor, RatingPlanForm.scheduleModifications.min, RatingPlanForm.scheduleModifications.max);
            scheduleModificationsFactor += 1;

            return Multiply(scheduleModificationStep, currentPremium, scheduleModificationsFactor);
        }
        public decimal CalculateFirstDollarDefense(decimal currentPremium, string deductibleValue, bool firstDollarDefense)
        {
            decimal firstDollarDefenseValue = 0;
            if (firstDollarDefense)
            {
                FirstDollarDefenseFactor fddFactor = RatingPlanForm.firstDollarDefense.factors.FirstOrDefault(f => f.deductibleValue == deductibleValue);
                firstDollarDefenseValue = currentPremium * fddFactor.factor;
                firstDollarDefenseValue = Round(firstDollarDefenseValue);
                firstDollarDefenseValue = firstDollarDefenseValue < RatingPlanForm.firstDollarDefense.minEndorsementPremium ? RatingPlanForm.firstDollarDefense.minEndorsementPremium : firstDollarDefenseValue;
                RiskAnalysis.OptionalCoverages.Add(firstDollarDefenseField, firstDollarDefenseValue);
            }
            return Sum(firstDollarDefenseStep, currentPremium, firstDollarDefenseValue);
        }
    }
}
