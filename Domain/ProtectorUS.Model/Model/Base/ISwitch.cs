﻿namespace ProtectorUS.Model
{
    public interface ISwitch
    {
        bool Active { get; set; }
    }
}
