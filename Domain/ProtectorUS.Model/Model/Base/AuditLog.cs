﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class AuditLog
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public DateTimeOffset EventDateUTC { get; set; }

        public EventType EventType { get; set; }
 
        public string TypeFullName { get; set; }

        public string RecordId { get; set; }

        public virtual ICollection<AuditLogDetail> LogDetails { get; set; } = new List<AuditLogDetail>();
    }
}
