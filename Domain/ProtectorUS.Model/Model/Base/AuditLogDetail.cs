﻿namespace ProtectorUS.Model
{
    public class AuditLogDetail
    {
        public long Id { get; set; }

        public string PropertyName { get; set; }

        public string OriginalValue { get; set; }

        public string NewValue { get; set; }

        public virtual long AuditLogId { get; set; }

        public virtual AuditLog AuditLog { get; set; }
    }
}