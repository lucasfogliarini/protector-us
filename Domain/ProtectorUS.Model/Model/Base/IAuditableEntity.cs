﻿using System;

namespace ProtectorUS.Model
{
    /// <summary>
    /// AuditableEntity it is the interface for auditable fields.
    /// </summary>
    public interface IAuditableEntity
    {
        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
    }
}
