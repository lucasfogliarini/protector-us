﻿namespace ProtectorUS.Model.FloatingPoint
{
    public class Rounding : IFloatingPoint
    {
        public const byte precision = 5;
        public const byte scale = 4;
    }
}
