﻿namespace ProtectorUS.Model.FloatingPoint
{
    public class Percentage : IFloatingPoint
    {
        public const byte precision = 9;
        public const byte scale = 6;
    }
}
