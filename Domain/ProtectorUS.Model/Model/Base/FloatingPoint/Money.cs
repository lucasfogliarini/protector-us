﻿namespace ProtectorUS.Model.FloatingPoint
{
    public class Money : IFloatingPoint
    {
        public const byte precision = 18;
        public const byte scale = 2;
    }
}
