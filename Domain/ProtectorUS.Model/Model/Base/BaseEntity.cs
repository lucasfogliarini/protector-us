﻿using FluentValidation;
using System;

namespace ProtectorUS.Model
{
    using Exceptions;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Validation;
    /// <summary>
    /// Generic base abstract entity.
    /// </summary>

    public abstract class BaseEntity
    {
        public virtual string EntityName { get { return this == null ? "" : this.GetType().Name; } }
        /// <summary>
        /// Get the instance of a validator by name convention e.g. ClassName+Validator
        /// </summary>
        /// <returns></returns>
        public IValidator GetValidator()
        {
            string typeName = $"{this.GetType().FullName}Validator";
            var type = Type.GetType(typeName);
            if (type == null)
                throw new ValidatorNotImplementedException(this.GetType().Name);
            return (IValidator)Activator.CreateInstance(type);
        }
    }
    public abstract class Entity<T> : BaseEntity, IEntity<T>
    {
        /// <summary>
        /// Gets or sets the generic unique identifier.
        /// </summary>
        public T Id { get; set; }
    }

    public static class BaseEntityExtensions
    {
        public static bool Has<TEntity, TPropertyList>(this TEntity entity, Func<TEntity, IEnumerable<TPropertyList>> propertyList) where TEntity : BaseEntity
        {
            IEnumerable<TPropertyList> property = propertyList(entity);
            return propertyList != null && property.Any();
        }
        public static void Add<TEntity, TPropertyList>(this TEntity entity, Expression<Func<TEntity, ICollection<TPropertyList>>> propertyList, TPropertyList item) where TEntity : BaseEntity
        {
            if (propertyList.Body is MemberExpression)
            {
                MemberExpression memberExpression = propertyList.Body as MemberExpression;
                PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;
                var propertyCollectionObj = propertyInfo.GetValue(entity);
                if (propertyCollectionObj == null) propertyInfo.SetValue(entity, new List<TPropertyList>());
                propertyCollectionObj = propertyInfo.GetValue(entity);
                if (propertyCollectionObj is ICollection<TPropertyList>)
                {
                    (propertyCollectionObj as ICollection<TPropertyList>).Add(item);
                }
            }
        }
    }
}
