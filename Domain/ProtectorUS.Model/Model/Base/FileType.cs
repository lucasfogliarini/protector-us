﻿using System.ComponentModel;
public enum FileType
{
    None = 0,
    [Description("*")]
    Any = 1,
    [Description("png,jpg,jpeg,gif")]
    Image = 2,
    [Description("pdf")]
    Pdf = 3,
    [Description("doc,docx,txt,rtf")]
    Doc = 4
};