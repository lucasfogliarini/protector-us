﻿using System;
using System.Linq.Expressions;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Class to build expressions
    /// </summary>
    public class ExpressionBuilder
    {
        /// <summary>
        /// Build a new expression to evaluate compiled expressions rules
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="r">Expression rule</param>
        /// <param name="param">Typed parameter</param>
        /// <returns></returns>
        public static Expression BuildExpr<T>(ExpressionRule r, ParameterExpression param)
        {
            var left = MemberExpression.Property(param, r.PropetyName);
            var tProp = typeof(T).GetProperty(r.PropetyName).PropertyType;
            ExpressionType tBinary;
            // is the operator a known .NET operator?
            if (ExpressionType.TryParse(r.Operator, out tBinary))
            {
                var right = Expression.Constant(Convert.ChangeType(r.TargetValue, tProp));
                // use a binary operation, e.g. 'Equal' -> 'u.Age == 15'
                return Expression.MakeBinary(tBinary, left, right);
            }
            else
            {
                var method = tProp.GetMethod(r.Operator);
                var tParam = method.GetParameters()[0].ParameterType;
                var right = Expression.Constant(Convert.ChangeType(r.TargetValue, tParam));
                // use a method call, e.g. 'Contains' -> 'u.Tags.Contains(some_tag)'
                return Expression.Call(left, method, right);
            }
        }
    }
}
