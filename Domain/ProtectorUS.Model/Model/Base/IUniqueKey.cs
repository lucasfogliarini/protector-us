﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model
{
    public interface IUniqueKeySoftDeletable : ISoftDeletable
    {
        /// <summary>
        /// unique columns: { "FooColumn", "Foo2Column" }
        /// aggregate unique columns: { "FooColumn, Foo2Column" }
        /// </summary>
        string[] UniqueColumns { get; }
    }

    public static class UniqueKeySoftDeletable
    {
        public static void CreateIndexes(Action<string> sql)
        {
            Type iUniqueKeyType = typeof(IUniqueKeySoftDeletable);
            IEnumerable<Type> uniqueKeyTypes = iUniqueKeyType.Assembly.GetTypes().Where(t => t.GetInterface(iUniqueKeyType.Name) != null && t.BaseType.GetInterface(iUniqueKeyType.Name) == null);
            foreach (Type type in uniqueKeyTypes)
            {
                IUniqueKeySoftDeletable uniqueKey = Activator.CreateInstance(type,true) as IUniqueKeySoftDeletable;
                foreach (string columnNames in uniqueKey.UniqueColumns)
                {
                    sql($"if exists (select * from sys.tables where name = '{type.Name}') CREATE UNIQUE NONCLUSTERED INDEX unique_{columnNames.Replace(',','_')}_index ON {type.Name}({columnNames}) WHERE {nameof(uniqueKey.IsDeleted)} = 0;");
                }
            }
        }
    }
}
