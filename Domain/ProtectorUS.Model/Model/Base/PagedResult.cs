﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Holds relevant information related to a page of a collection of information.
    /// </summary>
    public class PagedResult<TEntity>
    {
        /// <summary>
        /// A page of items.
        /// </summary>
        public IReadOnlyCollection<TEntity> Items { get; set; }

        /// <summary>
        /// Total number of items, regardless of page.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// The number of items that should be shown per page.
        /// </summary>
        public int ItemsPerPage { get; set; }
        public int Page { get; set; }
        public string Sort { get; set; }

        /// <summary>
        /// The total number of pages.
        /// </summary>
        public int TotalPages()
        {
            return (int)(Math.Ceiling((decimal)TotalItems / ItemsPerPage));
        }
    }

}
