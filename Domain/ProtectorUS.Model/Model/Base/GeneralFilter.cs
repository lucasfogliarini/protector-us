﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PeriodFilter
    {
        [Description("Today")]
        Today = 1,
        [Description("Last Week")]
        Week = 2,
        [Description("Last Month")]
        LastMonth = 3,
        [Description("Last Year")]
        LastYear = 4
    }

}
