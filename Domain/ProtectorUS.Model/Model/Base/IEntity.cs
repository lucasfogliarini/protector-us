﻿namespace ProtectorUS.Model
{
    /// <summary>
    /// Generic entity interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
