﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Auditable entity it is the base class from which you need to inherit to detect when changes have been made.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [SoftDelete("IsDeleted")]
    [TrackChanges]
    public abstract class AuditableEntity<T> : Entity<T>, IAuditableEntity, ISoftDeletable
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public void Delete()
        {
            IsDeleted = true;
        }
    }
}
