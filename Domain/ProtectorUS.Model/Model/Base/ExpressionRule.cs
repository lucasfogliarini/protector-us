﻿namespace ProtectorUS.Model
{

    /// <summary>
    /// Entity to storage and manage rules
    /// </summary>
    public class ExpressionRule : AuditableEntity<long>
    {
        /// <summary>
        /// Property name to be evaluate
        /// </summary>
        public string PropetyName { get; set; }

        /// <summary>
        /// Operator enumeration option basead on System.Linq.Expressions.ExpressionType https://msdn.microsoft.com/en-us/library/bb361179(v=vs.110).aspx
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Target value to apply operator to property
        /// </summary>
        public string TargetValue { get; set; }

        public ExpressionRule(string _propetyName, string _operator, string _targetValue)
        {
            this.PropetyName = _propetyName;
            this.Operator = _operator;
            this.TargetValue = _targetValue;
        }
    }

}
