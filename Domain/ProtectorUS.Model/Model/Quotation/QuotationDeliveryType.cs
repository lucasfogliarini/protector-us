﻿/// <summary>
/// Determines de Quotation Delivery application types.
/// </summary>
public enum QuotationDeliveryType
{
    Mail = 0,
    Print = 1
};