﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an quotation delivery from API.
    /// </summary>
    public class QuotationDelivery : AuditableEntity<long>
    {

        public long QuotationId { get; set; }
        public Quotation Quotation { get; set; }
        public QuotationDeliveryType Type { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }

    }
}
