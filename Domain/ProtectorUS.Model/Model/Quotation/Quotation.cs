﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an quotation from API.
    /// </summary>
    public class Quotation : AuditableEntity<long>
    {
        public Guid Hash { get; set; }
        public decimal Total { get { return PolicyPremium + SurchargeAmount; } }
        public decimal PolicyPremium { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal SurchargeAmount { get { return TaxAmount + FeeAmount; } }
        public IList<SurchargePercentage> Surcharges { get; set; }

        public bool IsDeclined { get; set; }
        public DateTime PolicyStartDate { get; set; }
        public decimal Rounding { get; set; }
        public decimal Revenue { get; set; }
        public ActivationStatus Status { get; set; }
        public SocialTitle? ProponentSocialTitle { get; set; }
        public string ProponentFirstName { get; set; }
        public string ProponentLastName { get; set; }
        public string ProponentEmail { get; set; }
        public DateTime ExpirationDate { get; set; }
        public long RatingPlanFormVersionId { get; set; }
        public RatingPlanFormVersion RatingPlanFormVersion { get; set; }
        public long BrokerId { get; set; }
        public Broker Broker { get; set; }
        public long? InsuredId { get; set; }
        public Insured Insured { get; set; }
        public long RegionId { get; set; }
        public Region Region { get; set; }
        public long? CampaignId { get; set; }
        public Campaign Campaign { get; set; }
        public long? BusinessId { get; set; }
        public Business Business { get; set; }
        /// <summary>
        /// in seconds
        /// </summary>
        public int InsuredTimezone { get; set; }
        public DateTime InsuredTimeNow
        {
            get
            {
                return DateTime.UtcNow.AddSeconds(InsuredTimezone);
            }
        }
        public long BrokerageProductId { get; set; }
        public BrokerageProduct BrokerageProduct { get; set; }
        public IList<QuotationDelivery> QuotationDeliveries { get; set; }
        public IList<Order> Orders { get; set; }
        public string RiskAnalysisJson { get; set; }
        public IList<DeclineReason> DeclineReasons { get; set; }
        public RiskAnalysis DeserializeRiskAnalysis()
        {
            if (RiskAnalysisJson == null) return null;
            return JsonConvert.DeserializeObject<RiskAnalysis>(RiskAnalysisJson);
        }
        public void SetRiskAnalysis(RiskAnalysis riskAnalysis)
        {
            RiskAnalysisJson = JsonConvert.SerializeObject(riskAnalysis, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
