﻿/// <summary>
/// Determines de Quotation Conatct types.
/// </summary>
public enum QuotationContactType
{
    Mail = 0,
    Phone = 1
};