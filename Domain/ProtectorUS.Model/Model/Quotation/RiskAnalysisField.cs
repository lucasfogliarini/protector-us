﻿namespace ProtectorUS.Model
{
    public class RiskAnalysisField
    {
        internal RiskAnalysisField()
        {
        }
        public RiskAnalysisField(string name, string value, decimal? factor = null)
        {
            Name = name;
            Value = value;
            Factor = factor;
        }
        public string Name { get; set; }
        public string Value { get; set; }
        public string ValueLabel { get; set; }
        public decimal? Factor { get; set; }
    }
}
