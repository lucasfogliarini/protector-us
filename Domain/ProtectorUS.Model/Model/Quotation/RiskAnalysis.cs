﻿using ProtectorUS.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model
{
    public class RiskAnalysis
    {
        internal RiskAnalysis()
        {
        }
        public RiskAnalysis(IEnumerable<RiskAnalysisField> fields)
        {
            Fields = fields.Where(f=>f != null).ToList();
        }
        public IList<RiskAnalysisField> Fields { get; set; }
        public Dictionary<string, string> MetaData { get; set; } = new Dictionary<string, string>();
        public IList<CalculationStep> CalculationSteps { get; set; } = new List<CalculationStep>();
        public Dictionary<string, decimal> OptionalCoverages { get; set; } = new Dictionary<string, decimal>();
        public decimal? GetCoverageAmount(string fieldName)
        {
            return OptionalCoverages.ContainsKey(fieldName) ? OptionalCoverages[fieldName] : default(decimal?);
        }
        public RiskAnalysisField GetField(string fieldName)
        {
            return Fields?.FirstOrDefault(f => f?.Name?.ToLower() == fieldName.ToLower());
        }
        public string GetFieldValue(string fieldName)
        {
            var field = GetField(fieldName);
            return field != null? field.Value: "";
        }
        public T GetFieldValue<T>(string fieldName)
        {
            try
            {
                string value = GetFieldValue(fieldName);
                value = value.Replace(',', '.');
                if (typeof(T).Name == typeof(bool).Name && (value == "1" || value == "0")) value = value == "0" ? "false" : "true";
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch
            {
                throw new ArgumentInvalidException(e => e.EL012, fieldName);
            }
        }        
        public void AddStep(string name, decimal premium, decimal? value, CalculationStepType type)
        {
            CalculationSteps.Add(new CalculationStep()
            {
                Name = name,
                Premium = premium,
                Value = value,
                Type = type
            });
        }
        public decimal? GetBasePremium()
        {
            return CalculationSteps?.FirstOrDefault(c => c.Type == CalculationStepType.Base)?.Premium;
        }
        public decimal GetRateMod()
        {
            string rateMod = MetaData.Get(rateModKey);
            return Convert.ToDecimal(rateMod);
        }
        public void AddField(string fieldName, string value)
        {
            Fields.Add(new RiskAnalysisField(fieldName,value));
        }

        public const string rateModKey = "rateMod";
        public const string businessYearsKey = "businessYears";
        public const string retroactiveYearsKey = "retroactiveYears";
    }
}
