﻿namespace ProtectorUS.Model
{
    public class CalculationStep
    {
        public string Name { get; set; }
        public decimal Premium { get; set; }
        public decimal? Value { get; set; }
        public CalculationStepType Type { get; set; }
    }
    public enum CalculationStepType
    {
        Base = 0,
        Factor = 1,
        Addition = 2,
    }
}
