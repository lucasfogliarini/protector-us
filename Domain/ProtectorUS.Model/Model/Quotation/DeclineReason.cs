﻿namespace ProtectorUS.Model
{
    public class DeclineReason: AuditableEntity<long>
    {
        public long QuotationId { get; set; }
        public Quotation Quotation { get; set; }
        public string QuestionFieldName  { get; set; }
    }
}
