﻿using System.ComponentModel;

public enum ActivationStatus
{
    [Description("Quoted")]
    Quoted = 0,
    //[Description("Details Completed")]
    //DetailsCompleted = 1,
    [Description("Activated")]
    Activated = 2
};