﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class ChargeTransactionParameters
    {
        public int Amount { get; set; }
        public int ApplicationFee { get; set; }
        public ChargeAccount ChargeAccount { get; set; }
        public CreditCardInfo CreditCard { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
    }
}
