﻿namespace ProtectorUS.Model
{
    public class ChargeTransaction
    {
        public PaymentStatus PaymentStatus { get; set; }
        public string TransactionCode { get; set; }

        public decimal StripeFee { get; set; }
    }
}
