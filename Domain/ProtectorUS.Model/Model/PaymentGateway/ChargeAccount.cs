﻿using System;

namespace ProtectorUS.Model
{
    public class ChargeAccount
    {
        public string Id { get; set; }
        public ChargeAccountStatus Status { get; set; }
        public bool CanCharge { get { return ChargesEnabled && TransfersEnabled; } }
        public bool TransfersEnabled { get; set; }
        public bool ChargesEnabled { get; set; }
        public ChargeAccountVerification Verification { get; set; }
    }
}
