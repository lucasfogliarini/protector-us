﻿using System;
using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PersonalType
    {
        [Description("individual")]
        Individual = 1,
        [Description("company")]
        Company = 2,
    }
}
