﻿using System;
using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum ChargeAccountStatus
    {
        [Description("noaccount")]
        NoAccount = 0,
        [Description("verified")]
        Verified = 1,
        [Description("unverified")]
        Unverified = 2,
        [Description("pending")]
        Pending = 3,
    }
}
