﻿using System;

namespace ProtectorUS.Model
{
    public class ChargeAccountVerification
    {
        public string DisabledReason { get; set; }
        public DateTime? DueBy { get; set; }
        public string[] FieldsNeeded { get; set; }
    }
}
