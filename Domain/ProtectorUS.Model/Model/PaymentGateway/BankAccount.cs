﻿namespace ProtectorUS.Model
{
    public class BankAccount : BaseEntity
    {
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public PersonalType AccountHolderType { get; set; } = PersonalType.Company;
    }
}
