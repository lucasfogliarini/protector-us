﻿using System;

namespace ProtectorUS.Model
{
    public class LegalEntity : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalIdNumber { get; set; }
        public string VerificationDocumentFileId { get; set; }
        public string SSNLast4 { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BusinessTaxId { get; set; }
        public Business Business { get; set; }
    }
}
