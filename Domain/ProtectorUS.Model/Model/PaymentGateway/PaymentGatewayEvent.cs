﻿using System;

namespace ProtectorUS.Model
{
    public class PaymentGatewayEvent
    {
        public DateTime? Created { get; set; }
        public dynamic Object { get; set; }
        public dynamic PreviousAttributes { get; set; }
        public bool LiveMode { get; set; }
        public string Type { get; set; }
        public string UserId { get; set; }
    }
}
