﻿
using System;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an claim document from API.
    /// </summary>
    public class ClaimDocument : AuditableEntity<long>
    {
        internal ClaimDocument() { }
        public ClaimDocument(string path)
        {
            Path = path;
        }
        public string Path { get; set; }
        public long ClaimId { get; set; }
        public Claim Claim { get; set; }
        public FileType Type
        {
            get
            {
                var extension = System.IO.Path.GetExtension(Path).TrimStart('.');
                return EnumExtensions.GetValue<FileType>(extension);
            }
        }
    }

}
