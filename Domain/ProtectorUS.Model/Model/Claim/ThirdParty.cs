﻿namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an manager user from API.
    /// </summary>
    public class ThirdParty : AuditableEntity<long>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Claim Claim { get; set; }
        public long ClaimId { get; set; }
    }
}
