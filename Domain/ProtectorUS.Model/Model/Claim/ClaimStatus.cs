﻿using System.ComponentModel;
/// <summary>
/// Determines de Claim Status application types.
/// </summary>
public enum ClaimStatus
{
    [Description("FNOL")]
    FNOL = 0,
    [Description("Analisys")]
    Analisys = 1,
    [Description("Payment")]
    Payment = 2,
    [Description("Closed")]
    Closed = 3
};