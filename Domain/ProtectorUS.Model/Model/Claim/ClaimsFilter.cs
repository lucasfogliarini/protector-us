﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum ClaimPeriodFilter
    {
        [Description("Today")]
        Today = 1,
        [Description("Last Week")]
        Week = 2,
        [Description("Last Month")]
        LastMonth = 3,
        [Description("Last Year")]
        LastYear = 4
    }
    public enum ClaimIntervalFilter
    {
        [Description("Today")]
        Today = 1,
        [Description("Month")]
        Month = 2,
        [Description("Year")]
        Year = 3
    }

}
