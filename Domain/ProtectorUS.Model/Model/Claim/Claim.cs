﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an clam from API.
    /// </summary>
    public class Claim : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public const int MinValue = 10000;
        public const int MaxValue = 99999;
        public string Number { get; private set; }
        public int SequentialNumber { get; private set; }
        public DateTime OccurrenceDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public bool PriorNotification { get; set; }
        public string DescriptionFacts { get; set; }
        public ClaimStatus Status { get; set; }
        public decimal? LossReserve { get; set; }
        public decimal? ALAEReserve { get; set; }
        public decimal? TotalPaid { get; set; }
        public IList<ClaimDocument> ClaimDocuments { get; set; }
        public ThirdParty ThirdParty { get; set; }
        public long PolicyId { get; set; }
        public Policy Policy { get; set; }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Number) };
            }
        }

        public static int NextSequentialNumber(int? lastSequentialNumber)
        {
            return lastSequentialNumber == null ? MinValue : lastSequentialNumber.GetValueOrDefault() + 1;
        }
        public void Numerate(int sequentialNumber = MinValue)
        {
            SequentialNumber = sequentialNumber;
            if (SequentialNumber < MinValue || SequentialNumber > MaxValue)
            {
                throw new Exceptions.ArgumentInvalidException(e => e.EL037, MinValue, MaxValue);
            }
            Number = "PR" + SequentialNumber;
        }
    }

}
