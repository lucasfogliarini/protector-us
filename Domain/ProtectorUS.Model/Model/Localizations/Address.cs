﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.Model
{
    public class Address : AuditableEntity<long>
    {
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public long RegionId { get; set; }
        public Region Region { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string StreetFull
        {
            get
            {
                string line2 = string.IsNullOrWhiteSpace(StreetLine2) ? "" : $" - {StreetLine2}";
                return $"{StreetLine1}{line2}";
            }            
        }
    }
}