﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Country : Entity<long>
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public IList<State> States { get; set; }
    }
}
