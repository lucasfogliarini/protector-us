﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class City : Entity<long>
    {
        public string Name { get; set; }
        public long StateId { get; set; }
        public State State { get; set; }
        public IList<Region> Regions { get; set; }
    }
}