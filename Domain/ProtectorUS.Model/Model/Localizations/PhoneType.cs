﻿namespace ProtectorUS.Model
{
    public enum PhoneType
    {
        Cell = 1,
        Residential = 2,
        Commercial = 3
    }
}
