﻿

namespace ProtectorUS.Model
{
    public class Phone : Entity<long>
    {
        public string Number { get; set; }
        public PhoneType Type { get; set; }
        public string Description { get; set; }
    }
}
