﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class State : Entity<long>
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public long CountryId { get; set; }
        public Country Country { get; set; }
        public IList<City> Cities { get; set; }
        public long? CarrierId { get; set; }
        public Carrier Carrier { get; set; }

        /// <summary>
        /// http://www.mapsofworld.com/usa/zipcodes/images/us-zip-code-map.jpg
        /// </summary>
        public static string[] GetRegionStates(string zipCode)
        {
            char prefixCode = zipCode[0];
            switch (prefixCode)
            {
                case '0':
                    return new string[] { "NH", "VT", "ME", "MA", "RI", "NJ", "CT" };
                case '1':
                    return new string[] { "NY", "PA", "DE" };
                case '2':
                    return new string[] { "WV", "VA", "NC", "SC", "DC", "MD" };
                case '3':
                    return new string[] { "TN", "MS", "AL", "GA", "FL" };
                case '4':
                    return new string[] { "MI", "IN", "OH", "KY" };
                case '5':
                    return new string[] { "MT", "ND", "SD", "MN", "IA", "WI" };
                case '6':
                    return new string[] { "NE", "KS", "MO", "IL"};
                case '7':
                    return new string[] { "OK", "TX", "LA", "AR" };
                case '8':
                    return new string[] { "ID", "WY", "NV", "UT", "CO", "AZ", "NM" };
                case '9':
                    return new string[] { "WA", "OR", "CA", "HI", "AK" };
            }
            return null;
        }
    }
}
