﻿namespace ProtectorUS.Model
{
    public class Region : Entity<long>
    {
        public string ZipCode { get; set; }
        public long CityId { get; set; }
        public City City { get; set; }
        public const int ZipCodeExactLength = 5;
    }
}
