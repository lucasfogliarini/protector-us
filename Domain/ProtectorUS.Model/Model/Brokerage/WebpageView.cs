﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class WebpageView : AuditableEntity<long>
    {
        public AccessType Access { get; set; }
        public long? CampaignId { get; set; }
        public Campaign Campaign { get; set; }
        public long BrokerageId { get; set; }
        public Brokerage Brokerage { get; set; }
        public long? WebpageProductId { get; set; }
        public WebpageProduct WebpageProduct { get; set; }
    }
}
