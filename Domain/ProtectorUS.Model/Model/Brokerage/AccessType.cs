﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum AccessType
    {
        [Description("Access Direct")]
        Direct = 1,
        [Description("Campaign")]
        Campaign = 2,
        [Description("Web Banners")]
        WebBanner = 3
    }
}
