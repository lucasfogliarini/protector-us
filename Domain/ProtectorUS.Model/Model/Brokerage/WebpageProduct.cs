﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class WebpageProduct : AuditableEntity<long>
    {
        public bool Active { get; set; }
        public const int AliasMaxLength = 35;
        public string Alias { get; set; }
        public string Logo { get; set; }
        public string Cover { get; set; }
        public bool Default { get; set; }
        public decimal BrokerageComission { get; set; }
        public string Description { get; set; }
        public long BrokerageProductId { get; set; }
        public BrokerageProduct BrokerageProduct { get; set; }
        public IList<Campaign> Campaigns { get; set; }
        public IList<WebpageView> Views { get; set; }
    }
}
