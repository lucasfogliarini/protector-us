﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class BrokerageGroup : Entity<long>
    {
        public string Name { get; set; }
        public IList<Brokerage> Brokerages { get; set; }
    }
}
