﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum BrokerageStatus
    {
        [Description("Inactive")]
        Inactive = 0,
        [Description("Active")]
        Active = 1,
        [Description("Onboarding")]
        Onboarding = 2
    }
}
