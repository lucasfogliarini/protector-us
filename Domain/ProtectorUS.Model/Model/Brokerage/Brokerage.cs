﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Brokerage : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public override string EntityName { get { return "Broker"; } }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Cover { get; set; }
        public BrokerageStatus Status { get; set; }
        public ChargeAccountStatus ChargeAccountStatus { get; set; }
        public string Website { get; set; }
        public long? BrokerageGroupId { get; set; }
        public BrokerageGroup BrokerageGroup { get; set; }
        public long AddressId { get; set; }
        public Address Address { get; set; }
        public IList<Broker> Brokers { get; set; }
        public IList<Phone> Phones { get; set; }
        public IList<BrokerageProduct> BrokerageProducts { get; set; }
        public string ChargeAccountId { get; set; }
        public string AgentNumber { get; set; }
        string _logo;
        public string Logo { get { return _logo ?? EmptyLogo; } set { _logo = value; } }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Name), nameof(Alias) };
            }
        }

        public const int AliasMaxLength = 20;
        public const int NameMaxLength = 50;
        public const string EmptyLogo = "images/brokerage_empty_logo.png";
    }
}
