﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Model
{
    public class BrokerageProduct : AuditableEntity<long>
    {
        public BrokerageProduct()
        {
        }
        public BrokerageProduct(bool active, long productId, decimal brokerageComission = 0)
        {
            CreateWebpage(active, productId, brokerageComission);
        }
        public bool ActiveARGO { get; set; }
        public bool ActiveBrokerage { get; set; }
        public bool? ActiveProduct { get { return Product?.Active; } }
        public bool Active { get { return ActiveARGO && ActiveBrokerage; } }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public long BrokerageId { get; set; }
        public Brokerage Brokerage { get; set; }
        public IList<WebpageProduct> WebpageProducts { get; set; }
        
        public decimal? BrokerageComission
        {
            get { return WebpageProducts?.FirstOrDefault(o => o.Default)?.BrokerageComission; }
        }

        public void CreateWebpage(bool active, long productId, decimal brokerageComission = 0, bool _default = true)
        {
            ActiveARGO = active;
            ActiveBrokerage = active;
            ProductId = productId;
            WebpageProducts = new List<WebpageProduct>()
            {
                new WebpageProduct()
                {
                    Active = active,
                    Alias = Guid.NewGuid().ToString("N"),
                    Default = _default,
                    BrokerageComission = brokerageComission
                }
            };
        }
    }
}