﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PolicyPeriodFilter
    {
        [Description("Today")]
        Today = 1,
        [Description("Last Week")]
        Week = 2,
        [Description("Last Month")]
        LastMonth = 3,
        [Description("Last 3 Month")]
        Last3Month = 4,
        [Description("Last 6 Month")]
        Last6Month = 5,
        [Description("Last Year")]
        LastYear = 6,
        [Description("All Time")]
        AllTime = 7
    }
    public enum PolicyIntervalFilter
    {
        [Description("Today")]
        Today = 1,
        [Description("Month")]
        Month = 2,
        [Description("Year")]
        Year = 3
    }

    public enum PolicyTypeView
    {
        [Description("Money")]
        Money = 1,
        [Description("Number")]
        Number = 2
    }
}
