﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Business : AuditableEntity<long>
    {
        public string Name { get; set; }
        public BusinessType Type { get; set; }
        public long AddressId { get; set; }
        public Address Address { get; set; }
    }
}
