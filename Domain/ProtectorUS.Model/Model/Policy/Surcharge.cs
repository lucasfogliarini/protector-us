﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Surcharge : Entity<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? MinTax { get; set; }
        public SurchargeType Type { get; set; }
        public IList<SurchargePercentage> Percentages { get; set; }

        public const string SurchargeType = "SurchargeType";
    }
}
