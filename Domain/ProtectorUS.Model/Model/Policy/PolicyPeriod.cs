﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PolicyPeriod
    {
        [Description("Renewal")]
        Renewal = 1,
        [Description("Not Renewal")]
        NotRenewal = 2
    }
}
