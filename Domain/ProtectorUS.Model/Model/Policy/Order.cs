﻿using System;

namespace ProtectorUS.Model
{
    public class Order : AuditableEntity<long>
    {
        public string TransactionCode { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public int TotalInstallments { get; set; }
        public DateTime OrderDate { get; set; }
        public Policy Policy { get; set; }
        public long QuotationId { get; set; }
        public Quotation Quotation { get; set; }
        public long PaymentMethodId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// PolicyPremium - BrokerageFee
        /// </summary>
        public decimal ArgoFee { get; set; }
        /// <summary>
        /// PolicyPremium * BrokerageFeePercent / 100
        /// </summary>
        public decimal BrokerageFee { get; set; }
        /// <summary>
        /// The percentage of the brokerage fee
        /// </summary>
        public decimal BrokerageFeePercentage { get; set; }
        /// <summary>
        /// Rounding value (-0.49 to 0.50)
        /// </summary>
        public decimal Rounding { get; set; }
        public decimal TotalCharged { get; set; }
        public decimal Discount { get; set; }
        public decimal SurchargeAmount { get; set; }

        /// <summary>
        /// Stripe - StripeFee
        /// </summary>
        public decimal? StripeFee { get; set; }
    }
}
