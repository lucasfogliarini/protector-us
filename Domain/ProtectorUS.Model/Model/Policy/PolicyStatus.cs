﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PolicyStatus
    {
        [Description("None")]
        None = 0,
        [Description("In Effect")]
        InEffect = 1,
        [Description("On Hold")]
        OnHold = 2,
        [Description("Expired")]
        Expired = 3,
        [Description("Canceled")]
        Canceled = 4
    }
}
