﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class CountySurcharge : Surcharge
    {
        public IList<City> Cities { get; set; }
    }
}
