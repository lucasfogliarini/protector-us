﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum SurchargeType
    {
        [Description("Tax")]
        Tax = 1,
        [Description("Fee")]
        Fee = 2
    }
}
