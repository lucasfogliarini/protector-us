﻿namespace ProtectorUS.Model
{
    public class StateSurcharge : Surcharge
    {
        public long StateId { get; set; }
        public State State { get; set; }
    }
}
