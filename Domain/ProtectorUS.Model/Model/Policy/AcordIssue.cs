﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ProtectorUS.Model
{
    [XmlRoot("Transactions")]
    public class AcordIssue
    {
        [XmlElement("Transaction")]
        public Transaction Transaction { get; set; }

    }

    public class Transaction
    {
        [XmlElement("TransactionId")]
        [Description("Unique for each policy")]
        public string TransactionId { get; set; }

        [XmlElement("TransactionType")]
        [Description("Static Value(Issue) - Type of Transaction")]
        public string TransactionType { get; set; }

        [XmlElement("EventType")]
        [Description("Static Value(Generic) - Type of Event")]
        public string EventType { get; set; }

        [XmlElement("TransactionStatus")]
        public string TransactionStatus { get; set; }

        [XmlElement("PolicyId")]
        public string PolicyId { get; set; }

        [XmlElement("SubmissionNumber")]
        public string SubmissionNumber { get; set; }

        [XmlElement("SubmissionId")]
        public string SubmissionId { get; set; }

        [XmlElement("BusinessUnit")]
        public string BusinessUnit { get; set; }

        [XmlElement("ProductVersionEffectiveDate")]
        public string ProductVersionEffectiveDate { get; set; }

        [XmlElement("ConfigurationProductCode")]
        public string ConfigurationProductCode { get; set; }

        [XmlElement("ConfigurationProductVersion")]
        public string ConfigurationProductVersion { get; set; }

        [XmlElement("IsRenewal")]
        public string IsRenewal { get; set; }

        [XmlElement("StripeFee")]
        public string StripeFee { get; set; }

        [XmlElement("ACORD")]
        public ACORD ACORD { get; set; }

    }

    public class ACORD
    {
        [XmlElement("SignonRs")]
        public SignonRs SignonRs { get; set; }

        [XmlElement("InsuranceSvcRs")]
        public InsuranceSvcRs InsuranceSvcRs { get; set; }

    }

    #region ACORD.SignonRs

    public class SignonRs
    {
        [XmlElement("CustId")]
        public CustId CustId { get; set; }

        [XmlElement("ClientDt")]
        public string ClientDt { get; set; }

        [XmlElement("CustLangPref")]
        public string CustLangPref { get; set; }

        [XmlElement("ClientApp")]
        public ClientApp ClientApp { get; set; }

        [XmlElement("ServerDt")]
        public string ServerDt { get; set; }

        [XmlElement("Language")]
        public string Language { get; set; }

    }

    public class ClientApp
    {
        [XmlElement("Org")]
        public string Org { get; set; }

        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

    }

    public class CustId
    {
        [XmlElement("SPName")]
        public string SPName { get; set; }

        [XmlElement("CustPermId")]
        public string CustPermId { get; set; }

        [XmlElement("CustLoginId")]
        public string CustLoginId { get; set; }

    }

    #endregion

    public class InsuranceSvcRs
    {
        [XmlElement("SPName")]
        public string SPName { get; set; }

        [XmlElement("CommlPkgPolicyAddRs")]
        public CommlPkgPolicyAddRs CommlPkgPolicyModRs { get; set; }

    }

    public class CommlPkgPolicyAddRs
    {
        [XmlElement("TransactionResponseDt")]
        public string TransactionResponseDt { get; set; }

        [XmlElement("CurCd")]
        public string CurCd { get; set; }

        [XmlElement("MsgStatus")]
        public MsgStatus MsgStatus { get; set; }

        [XmlElement("Producer")]
        public Producer Producer { get; set; }

        [XmlElement("InsuredOrPrincipal")]
        public InsuredOrPrincipal Insured { get; set; }

        [XmlElement("MiscParty")]
        public MiscParty MiscParty { get; set; }

        [XmlElement("CommlPolicy")]
        public CommlPolicy Policy { get; set; }

        [XmlElement("PriorCommlPolicy")]
        public PriorPolicy PriorPolicy { get; set; }

        [XmlElement("ErrorsAndOmissionsLineBusiness")]
        public ErrorsAndOmissionsLineBusiness ErrorsAndOmissionsLineBusiness { get; set; }
    }

    public class MsgStatus
    {
        [XmlElement("MsgStatusCd")]
        public string MsgStatusCd { get; set; }
    }


    #region Producer

    public class Producer
    {
        [XmlElement("ItemIdInfo")]
        public ProducerItemIdInfo ProducerItemIdInfo { get; set; }

        [XmlElement("GeneralPartyInfo")]
        public ProducerGeneralPartyInfo ProducerGeneralPartyInfo { get; set; }
    }

    public class ProducerItemIdInfo
    {
        [XmlElement("AgencyId")]
        public string AgencyId { get; set; }
    }

    public class ProducerGeneralPartyInfo
    {
        [XmlElement("NameInfo")]
        public ProducerNameInfo ProducerNameInfo { get; set; }

        [XmlElement("Addr")]
        public ProducerAddr ProducerAddr { get; set; }
    }

    public class ProducerNameInfo
    {
        [XmlElement("CommlName")]
        public ProducerCommlName ProducerCommlName { get; set; }
    }

    public class ProducerCommlName
    {
        [XmlElement("CommercialName")]
        public string CommercialName { get; set; }
    }

    public class ProducerAddr
    {
        [XmlElement("Addr1")]
        public string Addr1 { get; set; }

        [XmlElement("Addr2")]
        public string Addr2 { get; set; }

        [XmlElement("City")]
        public string City { get; set; }

        [XmlElement("StateProvCd")]
        public string StateProvCd { get; set; }

        [XmlElement("StateProv")]
        public string StateProv { get; set; }

        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement("CountryCd")]
        public string CountryCd { get; set; }

        [XmlElement("Country")]
        public string Country { get; set; }
    }

    #endregion

    #region Insured

    public class InsuredOrPrincipal
    {
        [XmlElement("ItemIdInfo")]
        public InsuredItemIdInfo InsuredItemIdInfo { get; set; }

        [XmlElement("GeneralPartyInfo")]
        public InsuredGeneralPartyInfo InsuredGeneralPartyInfo { get; set; }

        [XmlElement("InsuredOrPrincipalInfo")]
        public InsuredInfo InsuredInfo { get; set; }
    }

    public class InsuredItemIdInfo
    {
        [XmlElement("SystemId")]
        public string InsuredSystemId { get; set; }
    }

    public class InsuredGeneralPartyInfo
    {
        [XmlElement("NameInfo")]
        public InsuredNameInfo InsuredNameInfo { get; set; }

        [XmlElement("Addr")]
        public InsuredAddr InsuredAddr { get; set; }
    }

    public class InsuredNameInfo
    {
        [XmlElement("CommlName")]
        public InsuredCommlName InsuredCommlName { get; set; }

        [XmlElement("SupplementaryNameInfo")]
        public InsuredSupplementaryNameInfo InsuredSupplementaryNameInfo { get; set; }
    }

    public class InsuredCommlName
    {
        [XmlElement("CommercialName")]
        public string CommercialName { get; set; }
    }

    public class InsuredSupplementaryNameInfo
    {
        [XmlElement("SupplementaryNameCd")]
        public string SupplementaryNameCd { get; set; }

        [XmlElement("SupplementaryName")]
        public string SupplementaryName { get; set; }
    }

    public class InsuredAddr
    {
        [XmlElement("Addr1")]
        public string Addr1 { get; set; }

        [XmlElement("Addr2")]
        public string Addr2 { get; set; }

        [XmlElement("City")]
        public string City { get; set; }

        [XmlElement("StateProvCd")]
        public string StateProvCd { get; set; }

        [XmlElement("StateProv")]
        public string StateProv { get; set; }

        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement("CountryCd")]
        public string CountryCd { get; set; }

        [XmlElement("Country")]
        public string Country { get; set; }

        [XmlElement("County")]
        public string County { get; set; }
    }

    public class InsuredInfo
    {
        [XmlElement("InsuredOrPrincipalRoleCd")]
        public string InsuredOrPrincipalRoleCd { get; set; }

    }

    #endregion

    #region MiscParty

    public class MiscParty
    {
        [XmlElement("GeneralPartyInfo")]
        public MiscPartyGeneralPartyInfo MiscPartyGeneralPartyInfo { get; set; }

        [XmlElement("MiscPartyInfo")]
        public MiscPartyInfo MiscPartyInfo { get; set; }
    }

    public class MiscPartyGeneralPartyInfo
    {
        [XmlElement("NameInfo")]
        public MiscPartyNameInfo MiscPartyNameInfo { get; set; }

        [XmlElement("Addr")]
        public MiscPartyAddr MiscPartyAddr { get; set; }
    }

    public class MiscPartyNameInfo
    {
        [XmlElement("CommlName")]
        public MiscPartyCommlName MiscPartyCommlName { get; set; }

    }

    public class MiscPartyCommlName
    {
        [XmlElement("CommercialName")]
        public string CommercialName { get; set; }
    }

    public class MiscPartyAddr
    {
        [XmlElement("Addr1")]
        public string Addr1 { get; set; }

        [XmlElement("Addr2")]
        public string Addr2 { get; set; }

        [XmlElement("City")]
        public string City { get; set; }

        [XmlElement("StateProvCd")]
        public string StateProvCd { get; set; }

        [XmlElement("StateProv")]
        public string StateProv { get; set; }

        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement("CountryCd")]
        public string CountryCd { get; set; }

        [XmlElement("Country")]
        public string Country { get; set; }
    }

    public class MiscPartyInfo
    {
        [XmlElement("MiscPartyRoleCd")]
        public string MiscPartyRoleCd { get; set; }

    }

    #endregion

    #region Policy

    public class CommlPolicy
    {
        [XmlElement("PolicyNumber")]
        public string PolicyNumber { get; set; }

        [XmlElement("CompanyProgramCd")]
        public string PolicyCompanyProgramCd { get; set; }

        [XmlElement("LOBCd")]
        public string PolicyLOBCd { get; set; }

        [XmlElement("ControllingStateProvCd")]
        public string PolicyControllingStateProvCd { get; set; }

        [XmlElement("ContractTerm")]
        public PolicyContractTerm PolicyContractTerm { get; set; }

        [XmlElement("WrittenAmt")]
        public PolicyWrittenAmt PolicyWrittenAmt { get; set; }

        [XmlElement("MiscPremAmt")]
        public PolicyMiscPremAmt PolicyMiscPremAmt { get; set; }

        [XmlElement("AdditionalInterest")]
        public PolicyAdditionalInterest PolicyAdditionalInterest { get; set; }

        [XmlElement("CommlPolicySupplement")]
        public PolicySupplement PolicySupplement { get; set; }

        [XmlElement("CommissionafterContributionPct")]
        public string PolicyCommissionafterContributionPct { get; set; }

        [XmlElement("CreditOrSurcharge")]
        public PolicyCreditOrSurcharge[] PolicyCreditOrSurcharge { get; set; }

    }


    public class PolicyContractTerm
    {
        [XmlElement("EffectiveDt")]
        public string EffectiveDt { get; set; }

        [XmlElement("ExpirationDt")]
        public string ExpirationDt { get; set; }
    }


    public class PolicyWrittenAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }
    }


    public class PolicyMiscPremAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }
    }


    public class PolicyAdditionalInterest
    {
        [XmlElement("GeneralPartyInfo")]
        public PolicyGeneralPartyInfo PolicyGeneralPartyInfo { get; set; }

        [XmlElement("AdditionalInterestInfo")]
        public PolicyAdditionalInterestInfo PolicyAdditionalInterestInfo { get; set; }
    }

    public class PolicyGeneralPartyInfo
    {
        [XmlElement("NameInfo")]
        public PolicyNameInfo PolicyNameInfo { get; set; }

        [XmlElement("Communications")]
        public PolicyCommunications PolicyCommunications { get; set; }

        [XmlElement("GeneralPartyRoleCd")]
        public string PolicyGeneralPartyRoleCd { get; set; }
    }

    public class PolicyNameInfo
    {
        [XmlElement("PersonName")]
        public PolicyPersonName PolicyPersonName { get; set; }
    }

    public class PolicyPersonName
    {
        [XmlElement("Surname")]
        public string Surname { get; set; }

        [XmlElement("GivenName")]
        public string GivenName { get; set; }
    }

    public class PolicyCommunications
    {
        [XmlElement("PhoneInfo")]
        public PolicyPhoneInfo PolicyPhoneInfo { get; set; }

        [XmlElement("EmailInfo")]
        public PolicyEmailInfo PolicyEmailInfo { get; set; }
    }

    public class PolicyPhoneInfo
    {
        [XmlElement("PhoneNumber")]
        public string PolicyPhoneNumber { get; set; }
    }

    public class PolicyEmailInfo
    {
        [XmlElement("EmailAddr")]
        public string PolicyEmailAddr { get; set; }
    }

    public class PolicyAdditionalInterestInfo
    {
        [XmlElement("AccountNumberId")]
        public string AccountNumberId { get; set; }
    }


    public class PolicySupplement
    {
        [XmlElement("NYFreeTradeZone")]
        public string NYFreeTradeZone { get; set; }

        [XmlElement("NYFreeTradeZoneClassCd")]
        public string NYFreeTradeZoneClassCd { get; set; }
    }


    public class PolicyCreditOrSurcharge
    {
        [XmlElement("CreditSurchargeDt")]
        public string CreditSurchargeDt { get; set; }

        [XmlElement("CreditSurchargeCd")]
        public string CreditSurchargeCd { get; set; }

        [XmlElement("CreditSurchargeAmtDesc")]
        public string CreditSurchargeAmtDesc { get; set; }

        [XmlElement("NumericValue")]
        public PolicyNumericValue PolicyNumericValue { get; set; }

        [XmlElement("SecondaryCd")]
        public string SecondaryCd { get; set; }
    }

    public class PolicyNumericValue
    {
        [XmlElement("FormatInteger")]
        public string FormatInteger { get; set; }
    }

    #endregion

    #region PriorPolicy

    public class PriorPolicy
    {
        [XmlElement("ContractTerm")]
        public PriorPolicyContractTerm PriorPolicyContractTerm { get; set; }

        [XmlElement("FinalPremAmt")]
        public PriorPolicyFinalPremAmt FinalPremAmt { get; set; }
    }

    public class PriorPolicyContractTerm
    {
        [XmlElement("EffectiveDt")]
        public string EffectiveDt { get; set; }

        [XmlElement("ExpirationDt")]
        public string ExpirationDt { get; set; }
    }

    public class PriorPolicyFinalPremAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }
    }

    #endregion

    #region ErrorsAndOmissionsLineBusiness

    public class ErrorsAndOmissionsLineBusiness
    {
        [XmlElement("LOBCd")]
        public string LOBCd { get; set; }

        [XmlElement("CommlCoverage")]
        public EOCoverage[] Coverage { get; set; }
    }

    public class EOCoverage
    {
        [XmlElement("CoverageCd")]
        public string CoverageCd { get; set; }

        [XmlElement("CoverageDesc")]
        public string CoverageDesc { get; set; }

        [XmlElement("Limit")]
        public EOLimit[] EOLimit { get; set; }

        [XmlElement("Deductible")]
        public EODeductible[] EODeductible { get; set; }

        [XmlElement("WrittenAmt")]
        public EOWrittenAmt EOWrittenAmt { get; set; }

        [XmlElement("Option")]
        public EOOption[] EOOption { get; set; }

        [XmlElement("CommlCoverageSupplement")]
        public EOCoverageSupplement EOCoverageSupplement { get; set; }



    }

    public class EOLimit
    {
        [XmlElement("FormatCurrencyAmt")]
        public EOLimitFormatCurrencyAmt LimitFormatCurrencyAmt { get; set; }

        [XmlElement("LimitAppliesToCd")]
        public string EOLimitAppliesToCd { get; set; }
    }

    public class EOLimitFormatCurrencyAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }
    }

    public class EODeductible
    {
        [XmlElement("FormatCurrencyAmt")]
        public EODeductibleFormatCurrencyAmt DeductibleFormatCurrencyAmt { get; set; }

        [XmlElement("DeductibleAppliesToCd")]
        public string DeductibleAppliesToCd { get; set; }
    }
    public class EODeductibleFormatCurrencyAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }
    }

    public class EOWrittenAmt
    {
        [XmlElement("Amt")]
        public string Amt { get; set; }

    }

    public class EOOption
    {
        [XmlElement("OptionTypeCd")]
        public string EOOptionTypeCd { get; set; }

        [XmlElement("OptionCd")]
        public string EOOptionCd { get; set; }

        [XmlElement("OptionValue")]
        public string EOOptionValue { get; set; }

        [XmlElement("OptionValueDesc")]
        public string EOOptionValueDesc { get; set; }
    }

    public class EOCoverageSupplement
    {
        [XmlElement("ClaimsMadeInfo")]
        public EOClaimsMadeInfo EOClaimsMadeInfo { get; set; }
    }
    public class EOClaimsMadeInfo
    {
        [XmlElement("CurrentRetroactiveDt")]
        public string CurrentRetroactiveDt { get; set; }

        [XmlElement("ClaimsMadeInd")]
        public string ClaimsMadeInd { get; set; }
    }

    #endregion




}
