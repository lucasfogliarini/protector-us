﻿using ProtectorUS.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.IO;

namespace ProtectorUS.Model
{
    public class Policy : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        internal Policy()
        {
        }
        public Policy(PolicyNumber policyNumber, int insuredTimezone)
        {
            Numerate(policyNumber);
            CertificateFileName = $"{Number}_{Guid.NewGuid()}.pdf";
            InsuredTimezone = insuredTimezone;
        }
        public const string policyCertificatesDirectory = "policy_certificates";
        public const string policiesDirectory = "policies";
        public const string dateFormat = "MMM dd, yyyy";

        public Order Order { get; set; }
        public string Number { get; private set; }
        public int SequentialNumber { get; private set; }
        public string PolicyFileName
        {
            get { return $"{Number}.pdf"; }
        }
        public string PolicyPath { get; set; }
        public string CertificateFileName
        {
            get { return CertificatePath.Replace(policyCertificatesDirectory + "/", ""); }
            set { CertificatePath = $"{policyCertificatesDirectory}/{value}"; }
        }
        public string CertificatePath { get; private set; }
        public PolicyType Type { get; private set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public long ConditionsId { get; set; }
        public ProductCondition Condition { get; set; }
        public IList<Coverage> Coverages { get; set; }
        public long InsuredId { get; set; }
        public Insured Insured { get; set; }
        public long BrokerId { get; set; }
        public Broker Broker { get; set; }
        public bool Canceled { get; set; }
        public long? BusinessId { get; set; }
        public Business Business { get; set; }
        /// <summary>
        /// in seconds
        /// </summary>
        public int InsuredTimezone { get; private set; }
        public DateTime InsuredTimeNow
        {
            get
            {
                return DateTime.UtcNow.AddSeconds(InsuredTimezone);
            }
        }
        public IList<Claim> Claims { get; set; }
        public IList<Endorsement> Endorsements { get; set; }
        public decimal CoverageEachClaim { get; set; }
        public decimal CoverageAggregate { get; set; }
        public decimal DeductibleEachClaim { get; set; }
        public decimal DeductibleAggregate { get; set; }
        public decimal TotalPremium { get; set; }
        public PolicyStatus Status
        {
            get
            {
                if(Canceled)
                    return PolicyStatus.Canceled;
                if (InsuredTimeNow > PeriodStart && InsuredTimeNow < PeriodEnd)
                    return PolicyStatus.InEffect;
                else if (InsuredTimeNow > PeriodEnd)
                    return PolicyStatus.Expired;
                else if (InsuredTimeNow < PeriodStart)
                    return PolicyStatus.OnHold;
                return PolicyStatus.None;
            }

        }
        public PolicyPeriod PeriodRenewal
        {
            get
            {
                if (InsuredTimeNow > PeriodEnd && InsuredTimeNow < PeriodEnd.AddDays(15))
                    return PolicyPeriod.Renewal;
                else if (InsuredTimeNow < PeriodEnd && InsuredTimeNow > PeriodEnd.AddDays(-15))
                    return PolicyPeriod.Renewal;
                else
                    return PolicyPeriod.NotRenewal;
            }
        }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Number) };
            }
        }
        public void Numerate(PolicyNumber policyNumber)
        {
            SequentialNumber = policyNumber.SequentialNumber;
            Type = policyNumber.Type;
            Number = policyNumber.Number;
            if (SequentialNumber < PolicyNumber.sequentialMin || SequentialNumber > PolicyNumber.sequentialMax)
            {
                throw new Exceptions.ArgumentInvalidException(e=>e.EL037);
            }
        }
        public PolicyNumber GetPolicyNumber()
        {
            return new PolicyNumber(Number);
        }
        public string FileToken()
        {
            return PBKDF2Hasher.GenerateHash(Number);
        }
    }

    public class PolicyNumber
    {
        public const int sequentialMin = 10000;
        public const int sequentialMax = 99999;
        public PolicyNumber(int edition, string code, int sequentialNumber = sequentialMin)
        {
            Edition = edition;
            Code = code;
            SequentialNumber = sequentialNumber;
        }
        public PolicyNumber(string number)
        {
            try
            {
                //refact me plz
                string[] numberSplitted = number.Split('-');
                string numberSimplified = numberSplitted[0];
                Code = numberSimplified.Substring(0, 3);
                SequentialNumber = Convert.ToInt32(numberSimplified.Substring(3));
                Edition = Convert.ToInt32(numberSplitted[1]);
            }
            catch
            {
            }
        }
        public string Code { get; private set; }
        public string Number { get { return $"{NumberSimplified}-{Edition}"; } }
        public string NumberSimplified { get { return $"{Code}{SequentialNumber}"; } }
        public int SequentialNumber { get; set; }
        public int Edition { get; private set; }
        public PolicyType Type { get { return Edition == 0 ? PolicyType.New : PolicyType.Renewal; }  }
        public static int NextSequentialNumber(int? lastSequentialNumber)
        {
            return lastSequentialNumber == null ? sequentialMin : lastSequentialNumber.GetValueOrDefault() + 1;
        }
    }
}