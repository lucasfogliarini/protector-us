﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum PolicyType
    {
        [Description("New")]
        New = 1,
        [Description("Renewal")]
        Renewal =2
    }
}
