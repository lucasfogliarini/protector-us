﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum BusinessType
    {
        [Description("Individual")]
        Individual = 1,
        [Description("Firm")]
        Firm = 2,
    }
}
