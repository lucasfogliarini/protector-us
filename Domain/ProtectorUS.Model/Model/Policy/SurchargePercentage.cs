﻿namespace ProtectorUS.Model
{
    public class SurchargePercentage : AuditableEntity<long>
    {
        public decimal Percentage { get; set; }
        public long SurchargeId { get; set; }
        public Surcharge Surcharge { get; set; }
    }
}
