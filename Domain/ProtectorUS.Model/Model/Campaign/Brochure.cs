﻿namespace ProtectorUS.Model
{
    public class Brochure : Campaign
    {
        public string FilePath { get; set; }
        public BrochureTemplateType TemplateType { get; set; }
        public long ProductId
        {
            get
            {
                return (int)TemplateType;
            }
        }

        public const string brochureTemplatesFolder = "BrochureTemplates";
    }
}
