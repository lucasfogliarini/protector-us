﻿namespace ProtectorUS.Model
{
    public class Campaign : AuditableEntity<long>
    {
        public long WebpageProductId { get; set; }
        public WebpageProduct WebpageProduct { get; set; }
        public long BrokerId { get; set; }
        public Broker Broker { get; set; }
        public const string CampaignType = "CampaignType";
        public CampaignType Type
        {
            get
            {
                if (this is Brochure)
                    return Model.CampaignType.Brochure;
                return Model.CampaignType.EmailMarketing;
            }
        }
    }
}
