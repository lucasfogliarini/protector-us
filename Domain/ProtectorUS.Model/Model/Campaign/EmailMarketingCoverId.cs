﻿namespace ProtectorUS.Model
{
    public enum EmailMarketingCoverId
    {
        Accountants1 = 1,
        Accountants2 = 2,
        Accountants3 = 3,
        Engineers1 = 4,
        Engineers2 = 5,
        Engineers3 = 6,
    }
}