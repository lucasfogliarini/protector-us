﻿namespace ProtectorUS.Model
{
    public class EmailMarketing : Campaign
    {
        public string CoverPath { get; set; }
        public EmailMarketingCoverId CoverId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string ButtonText { get; set; }
        public long ProductId
        {
            get
            {
                return (int)CoverId <= 3 ? 2 : 1;
            }
        }        
    }
}
