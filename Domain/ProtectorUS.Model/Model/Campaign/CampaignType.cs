﻿namespace ProtectorUS.Model
{
    public enum CampaignType
    {
        EmailMarketing = 0,
        SocialMedia = 1,
        Brochure = 2
    }
}