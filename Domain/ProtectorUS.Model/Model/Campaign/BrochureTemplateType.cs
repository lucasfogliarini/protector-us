﻿using System.ComponentModel;

namespace ProtectorUS.Model
{
    public enum BrochureTemplateType
    {
        [Description("pae1.pdf")]
        Engineers1 = 1,
        [Description("pac1.pdf")]
        Accountants1 = 2,
    }
}