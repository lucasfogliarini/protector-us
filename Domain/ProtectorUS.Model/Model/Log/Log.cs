﻿using System;

namespace ProtectorUS.Model
{
    public class Log : Entity<long>
    {
        public DateTime Date { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string AdditionalInfo { get; set; }


    }
}
