﻿namespace ProtectorUS.Model
{
    public class EmailTemplate : AuditableEntity<long>
    {
        /// <summary>
        /// The blob folder path
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// Emailtemplate name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Email from address
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// Email from display name
        /// </summary>
        public string FromName { get; set; }
        /// <summary>
        /// Email subject
        /// </summary>
        public string Subject { get; set; }

        public const string DateFormat = "MMM dd, yyyy";

        #region Quotation and Activation
        public static class Quotation
        {
            public const string Id = "quotation";
            public const string protector_domain = "#protector_domain#";
            public const string brokerage_alias = "#brokerage_alias#";
            public const string brokerage_product_url = "#brokerage_product_url#";
            public const string brokerage_logo_path = "#brokerage_logo_path#";
            public const string brokerage_name = "#brokerage_name#";
            public const string brokerage_street = "#brokerage_street#";
            public const string brokerage_cityname = "#brokerage_cityname#";
            public const string brokerage_stateabbr = "#brokerage_stateabbr#";
            public const string brokerage_zipcode = "#brokerage_zipcode#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string proponent_firstname = "#proponent_firstname#";
            public const string proponent_lastname = "#proponent_lastname#";
            public const string product_name = "#product_name#";
            public const string quote_date = "#quote_date#";
            public const string coverage = "#coverage#";
            public const string deductible = "#deductible#";
            public const string business_cityname = "#business_cityname#";
            public const string business_stateabbr = "#business_stateabbr#";
            public const string business_zipcode = "#business_zipcode#";
            public const string total_premium = "#total_premium#";
            public const string quote_hash = "#quote_hash#";
            public const string product_alias = "#product_alias#";
        }
        public static class Activation
        {
            public const string Id = "activation";
            public const string protector_domain = "#protector_domain#";
            public const string brokerage_product_url = "#brokerage_product_url#";
            public const string brokerage_logo_path = "#brokerage_logo_path#";
            public const string brokerage_name = "#brokerage_name#";
            public const string brokerage_street = "#brokerage_street#";
            public const string brokerage_cityname = "#brokerage_cityname#";
            public const string brokerage_stateabbr = "#brokerage_stateabbr#";
            public const string brokerage_zipcode = "#brokerage_zipcode#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string insured_firstname = "#insured_firstname#";
            public const string insured_lastname = "#insured_lastname#";
            public const string product_name = "#product_name#";
            public const string policy_number = "#policy_number#";
            public const string coverage = "#coverage#";
            public const string deductible = "#deductible#";
            public const string policy_start = "#policy_start#";
            public const string policy_end = "#policy_end#";
            public const string business_address = "#business_address#";
            public const string business_cityname = "#business_cityname#";
            public const string business_stateabbr = "#business_stateabbr#";
            public const string business_zipcode = "#business_zipcode#";
            public const string total_premium = "#total_premium#";
            public const string policy_path = "#policy_certificate_path#";
        }
        public static class ContactBroker
        {
            public const string Id = "contact-broker";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string broker_firstname = "#broker_firstname#";
            public const string broker_lastname = "#broker_lastname#";
            public const string proponent_firstname = "#proponent_firstname#";
            public const string proponent_lastname = "#proponent_lastname#";
            public const string quote_date = "#quote_date#";
            public const string proponent_phone = "#proponent_phone#";
            public const string proponent_email = "#proponent_email#";
            public const string product_name = "#product_name#";
        }
        #endregion

        #region Welcome Protector
        public static class WelcomeManagerUser
        {
            public const string Id = "welcome-argo";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string manageruser_firstname = "#manageruser_firstname#";
            public const string manageruser_lastname = "#manageruser_lastname#";
            public const string manageruser_password = "#manageruser_password#";
            public const string manageruser_email = "#manageruser_email#";
        }
        public static class WelcomeInsured
        {
            public const string Id = "welcome-insured";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string insured_firstname = "#insured_firstname#";
            public const string insured_lastname = "#insured_lastname#";
            public const string insured_password = "#insured_password#";
            public const string insured_email = "#insured_email#";
        }
        public static class WelcomeBroker
        {
            public const string Id = "welcome-broker";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string brokerage_name = "#brokerage_name#";
            public const string broker_firstname = "#broker_firstname#";
            public const string broker_lastname = "#broker_lastname#";
            public const string broker_password = "#broker_password#";
            public const string broker_email = "#broker_email#";
        }

        public static class WelcomeBrokerage
        {
            public const string Id = "welcome-brokerage";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string broker_password = "#broker_password#";
            public const string broker_email = "#broker_email#";
        }
        #endregion

        #region Claims
        public static class FnolClaim
        {
            public const string BrokerId = "fnol-claim-broker";
            public const string ArgoId = "fnol-claim-argo";
            public const string InsuredId = "fnol-claim-insured";

            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            #region Broker
            public const string receiver_socialtitle = "#receiver_socialtitle#";
            public const string receiver_lastname = "#receiver_lastname#";
            public const string receiver_firstname = "#receiver_firstname#";
            public const string insured_firstname = "#insured_firstname#";
            public const string insured_lastname = "#insured_lastname#";
            public const string insured_business_name = "#insured_business_name#";
            public const string policy_number = "#policy_number#";
            public const string product_name = "#product_name#";
            public const string claim_date = "#claim_date#";
            public const string claim_number = "#claim_number#";
            public const string claim_id = "#claim_id#";
            public const string brokerage_name = "#brokerage_name#";
            public const string broker_firstname = "#broker_firstname#";
            public const string broker_lastname = "#broker_lastname#";
            #endregion
            #region Argo
            //"#insured_firstname#";
            //"#insured_lastname#";
            //"#policy_number#";
            //"#product_name#";
            //"#claim_date#";
            //"#claim_number#";
            //"#brokerage_name#";
            //"#broker_firstname#";
            //"#broker_lastname#";
            //"#claim_id#";
            #endregion
            #region Insured
            //"#receiver_firstname#";
            //"#receiver_lastname#";
            //"#receiver_socialtitle#";
            //"#claim_date#";
            //"#claim_number#";
            //"#product_name#";
            //"#brokerage_name#";
            //"#claim_id#";
            #endregion
        }

        public static class ClaimStatus
        {
            public const string BrokerId = "claim-status-broker";
            public const string ArgoId = "claim-status-argo";
            public const string InsuredId = "claim-status-insured";

            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";
            public const string receiver_firstname = "#receiver_firstname#";
            public const string receiver_lastname = "#receiver_lastname#";
            public const string claim_date = "#claim_date#";
            public const string claim_number = "#claim_number#";
            public const string claim_id = "#claim_id#";
            public const string product_name = "#product_name#";
            public const string brokerage_name = "#brokerage_name#";
        }

        #endregion

        public static class EmailMarketingCampaign
        {
            public const string emailmarketingTemplateCovers = "emailmarketing-template-covers";
            public const string accountantsId = "emailmarketing-accountants";
            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";

            public const string cover_path = "#cover_path#";
            public const string product_name = "#product_name#";
            public const string call_to_action_link = "#call_to_action_link#";
            public const string button_text = "#button_text#";
            public const string body_text = "#body_text#";
        }

        public static class ForgotPassword
        {
            public const string Id = "forgot-password";

            public const string protector_domain = "#protector_domain#";
            public const string blob_template_folder = "#blob_template_folder#";

            public const string protector_app = "#protector_app#";
            public const string user_firstname = "#user_firstname#";
            public const string user_lastname = "#user_lastname#";
            public const string user_id = "#user_id#";
            public const string token = "#token#";
        }
    }
}
