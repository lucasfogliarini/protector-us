﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Carrier : AuditableEntity<long>
    {
        public string  Name { get; set; }

        public Address Address { get; set; }

        public long AddressId { get; set; }

        public IList<State> States { get; set; }
    }
}
