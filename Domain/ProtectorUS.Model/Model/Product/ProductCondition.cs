﻿
namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents the Product Condition .
    /// </summary>
    public class ProductCondition : AuditableEntity<long>
    {
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public long StateId { get; set; }
        public State State { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public bool Active { get; set; }
    }
}
