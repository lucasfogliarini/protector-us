﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model
{
    public class Product : AuditableEntity<long>, IUniqueKeySoftDeletable
    {
        public string Name { get; set; }
        public string Slogan { get; set; }
        public string Code { get; set; }
        public string Alias { get; set; }
        public string Abbreviation { get; set; }
        public string Icon { get; set; }
        public bool Active { get; set; }
        public IList<ProductPaymentMethod> ProductPaymentMethods { get; set; }
        public IList<Declined> Declineds { get; set; }
        public IList<BrokerageProduct> BrokerageProducts { get; set; }
        public static string GetProductAlias(long productId)
        {
             return productId == ArchitectectsEngineersId ? ArchitectectsEngineersAlias : AccountantsAlias;
        }

        string[] IUniqueKeySoftDeletable.UniqueColumns
        {
            get
            {
                return new[] { nameof(Name), nameof(Alias), nameof(Abbreviation) };
            }
        }
        public const string AccountantsAlias = "accountants";
        public const string ArchitectectsEngineersAlias = "architects-and-engineers";

        public const long ArchitectectsEngineersId = 1;
        public const long AccountantsId = 2;
    }
}
