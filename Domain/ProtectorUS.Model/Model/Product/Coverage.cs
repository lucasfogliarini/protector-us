﻿using System;

namespace ProtectorUS.Model
{
    public class Coverage : AuditableEntity<long>
    {
        internal Coverage()
        {
        }
        public Coverage(long id, string title, DateTime? dateStart = null)
        {
            Id = id;
            Title = title;
            Key = title.Simplify();
            DateStart = dateStart ?? DateTime.Now;
        }
        public string Key { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
    }
}
