﻿using System.Collections.Generic;

namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an product payment method from API.
    /// </summary>
    public class ProductPaymentMethod : AuditableEntity<long>
    {

        public long ProductId { get; set; }
        public Product Product { get; set; }
        public long PaymentMethodId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public int TotalInstallments { get; set; }

    }
}
