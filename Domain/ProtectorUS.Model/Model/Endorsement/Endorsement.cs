﻿namespace ProtectorUS.Model
{
    /// <summary>
    /// Represents an endorsement.
    /// </summary>
    public class Endorsement : AuditableEntity<long>
    {
        internal Endorsement() { }
        public Endorsement(EndorsementType type, long reasonId, long requesterId)
        {
            Type = type;
            ReasonId = reasonId;
            Status = EndorsementStatus.Pending;
            RequesterId = requesterId;
        }
        public long PolicyId { get; set; }
        public Policy Policy { get; set; }
        public EndorsementType Type { get; set; }
        public long ReasonId { get; set; }
        public Reason Reason { get; set; }
        public long RequesterId { get; set; }
        public User Requester { get; set; }
        public EndorsementStatus Status { get; set; }
        public Product Product { get { return Policy?.Condition?.Product; }  }
    }
}
