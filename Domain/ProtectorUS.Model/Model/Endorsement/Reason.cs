﻿namespace ProtectorUS.Model
{
    public class Reason : AuditableEntity<long>
    {
        public string Description { get; set; }
    }
}
