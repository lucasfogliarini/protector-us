﻿namespace ProtectorUS.Model
{
    public enum EndorsementStatus
    {
        Denied = 0,
        Approved = 1,
        Pending = 2,
    };
}
