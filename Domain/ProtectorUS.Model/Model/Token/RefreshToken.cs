﻿using System;

namespace ProtectorUS.Model
{
    public class RefreshToken : AuditableEntity<long>
    {
        public string RefreshTokenId { get; set; }
        public string IdentityName { get; set; }
        public string AppClientId { get; set; }
        public DateTime? IssuedDate { get; set; }
        public DateTime? ExpiresDate { get; set; }
        public string ProtectedTicket { get; set; }
    }
}
