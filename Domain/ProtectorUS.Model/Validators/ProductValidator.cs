﻿using FluentValidation;
using System;

namespace ProtectorUS.Model
{
    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(e => e.Abbreviation).NotEmpty();
            RuleFor(e => e.Alias).NotEmpty();
            RuleFor(e => e.Code).NotEmpty();
            RuleFor(e => e.Name).NotEmpty();
        }
    }
}
