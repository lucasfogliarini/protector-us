﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class BrokerageValidator : AbstractValidator<Brokerage>
    {
        public BrokerageValidator()
        {
            RuleFor(b => b.Name).NotEmpty().Length(0, Brokerage.NameMaxLength);
            RuleFor(b => b.Alias).NotEmpty().Length(0, Brokerage.AliasMaxLength);
            RuleFor(b => b.Email).NotEmpty().EmailAddress();
            RuleFor(b => b.Address).NotEmpty();
            When(b => b.Address != null, () =>
            {
                RuleFor(b => b.Address.Latitude).NotEmpty();
                RuleFor(b => b.Address.Longitude).NotEmpty();
            });
            When(b => b.Id == 0, () =>
            {
                RuleFor(b => b.Brokers).AtLeast(1);
                RuleFor(b => b.BrokerageProducts).AtLeast(1);
            });
            When(b => b.Id > 0, () =>
            {
                //RuleFor(b => b.Logo).NotEmpty(); nextversion
            });
        }
    }
}
