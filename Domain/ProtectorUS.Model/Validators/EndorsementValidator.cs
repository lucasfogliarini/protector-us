﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class EndorsementValidator : AbstractValidator<Endorsement>
    {
        public EndorsementValidator()
        {
            RuleFor(e => e.PolicyId).NotEmpty();
            RuleFor(e => e.ReasonId).NotEmpty();
        }
    }
}
