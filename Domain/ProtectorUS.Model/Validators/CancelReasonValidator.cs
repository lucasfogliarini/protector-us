﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class CancelReasonValidator : AbstractValidator<CancelReason>
    {
        public CancelReasonValidator()
        {
            RuleFor(e => e.Description).NotEmpty();
        }
    }
}
