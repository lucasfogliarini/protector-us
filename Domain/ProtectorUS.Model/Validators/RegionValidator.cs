﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class RegionValidator : AbstractValidator<Region>
    {
        public RegionValidator()
        {
            RuleFor(e => e.ZipCode).NotEmpty().Length(Region.ZipCodeExactLength);
        }
    }
}
