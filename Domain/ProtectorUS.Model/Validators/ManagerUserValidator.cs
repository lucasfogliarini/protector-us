﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class ManagerUserValidator : UserValidator<ManagerUser>
    {
        public ManagerUserValidator()
        {
            RuleFor(b => b.FirstName).NotEmpty();
            RuleFor(b => b.CompanyId).NotEmpty();
        }
    }
}
