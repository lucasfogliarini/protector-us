﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class PolicyValidator : AbstractValidator<Policy>
    {
        public PolicyValidator()
        {
            RuleFor(e => e.Number).NotEmpty();
            RuleFor(e => e.TotalPremium).GreaterThan(0);
            RuleFor(b => b.PeriodStart).GreaterThanOrEqualTo(e=>e.InsuredTimeNow.Date);
            RuleFor(b => b.PeriodEnd).Equal(e => e.PeriodStart.AddYears(1));
            When(e => e.Id == 0, () =>
            {
                RuleFor(e => e.Insured).NotNull();
                RuleFor(e => e.Business).NotNull();
                When(e => e.Business != null, () =>
                {
                    RuleFor(e => e.Business.Address).NotNull();
                    When(e => e.Business.Address != null, () =>
                    {
                        RuleFor(e => e.Business.Address.StreetLine1).NotEmpty();
                        RuleFor(e => e.Business.Address.Latitude).NotEqual(0);
                        RuleFor(e => e.Business.Address.Longitude).NotEqual(0);
                    });
                });
            });
        }
    }
}
