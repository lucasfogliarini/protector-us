﻿using FluentValidation;
using ProtectorUS.Communication;
using ProtectorUS.Model.Validation;

namespace ProtectorUS.Model
{
    public abstract class UserValidator<TUser> : AbstractValidator<TUser> where TUser : User
    {
        public UserValidator()
        {
            RuleFor(u => u.Email).EmailAddress().NotEmpty();
            RuleFor(u => u.Password).Length(User.PasswordMinLength, int.MaxValue).NotEmpty();
            When(u => u.Id == 0, () =>
            {
                RuleFor(u => u.Status).Must(status => status == UserStatus.Onboarding);
                RuleFor(u => u.Profiles).AtLeast(1);
            });
            When(u => u.PasswordPlain != null, () =>
            {
                RuleFor(u => u.PasswordPlain).Matches(User.PasswordPattern).WithMessage(User.PasswordPatternMessage);
            });
        }
    }

    public static class ValidationManagerExtensions
    {
        public static void AddExistingUserError<TUser>(this IValidationManager validationManager, TUser user) where TUser : User
        {
            validationManager.AddExistingUserError<TUser>(user.Email);
        }
        public static void AddExistingUserError<TUser>(this IValidationManager validationManager, string existingEmail) where TUser : User
        {
            string userType = typeof(TUser).Name;
            validationManager.AddError(new MessageCode(x => x.ES032, userType, existingEmail).Message, userType);
        }
    }
}
