﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class AppClientValidator : AbstractValidator<AppClient>
    {
        public AppClientValidator()
        {
            RuleFor(e => e.Token).Length(AppClient.TokenMinLength, 20);
            RuleFor(e => e.Name).NotEmpty();
            RuleFor(e => e.ClientId).NotEmpty();
            RuleFor(e => e.CurrentVersion).NotEmpty();
            RuleFor(e => e.ReviewVersion).NotNull();
            RuleFor(e => e.AllowedOrigin).NotNull();
        }
    }
}
