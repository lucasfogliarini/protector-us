﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(e => e.PaymentMethodId).GreaterThan(0);
            RuleFor(e => e.Quotation).NotNull();
            RuleFor(e => e.TotalInstallments).GreaterThan(0);
            RuleFor(e => e.ArgoFee).GreaterThan(0);
            RuleFor(e => e.SurchargeAmount).GreaterThanOrEqualTo(0);
            RuleFor(e => e.BrokerageFee).GreaterThanOrEqualTo(0);
            RuleFor(e => e.TotalCharged).GreaterThan(0);
            When(e => e.PaymentStatus != PaymentStatus.Pending, () =>
            {
                RuleFor(e => e.TransactionCode).NotEmpty();
            });
        }
    }
}
