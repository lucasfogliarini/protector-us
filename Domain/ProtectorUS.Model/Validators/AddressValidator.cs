﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class AddressValidator : AbstractValidator<Address>
    {
        public AddressValidator()
        {
            RuleFor(e => e.StreetLine1).NotEmpty();
        }
    }
}
