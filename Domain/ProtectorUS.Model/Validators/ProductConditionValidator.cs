﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class ProductConditionValidator : AbstractValidator<ProductCondition>
    {
        public ProductConditionValidator()
        {
            RuleFor(e => e.FilePath).NotEmpty();
            RuleFor(e => e.ProductId).NotEmpty();
            RuleFor(e => e.StateId).NotEmpty();
            RuleFor(e => e.Active).Must(a => a);
        }
    }
}
