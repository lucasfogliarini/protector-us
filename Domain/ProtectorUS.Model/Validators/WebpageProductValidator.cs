﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class WebpageProductValidator : AbstractValidator<WebpageProduct>
    {
        public WebpageProductValidator()
        {
            RuleFor(b => b.Alias).NotEmpty().Length(0, WebpageProduct.AliasMaxLength);
            RuleFor(b => b.BrokerageProduct).NotEmpty();
        }
    }
}
