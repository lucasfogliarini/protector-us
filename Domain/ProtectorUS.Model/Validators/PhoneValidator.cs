﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class PhoneValidator : AbstractValidator<Phone>
    {
        public PhoneValidator()
        {
            RuleFor(e => e.Number).NotEmpty();
            RuleFor(e => e.Type).IsInEnum();
        }
    }
}
