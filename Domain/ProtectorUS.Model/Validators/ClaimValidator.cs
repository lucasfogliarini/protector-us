﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class ClaimValidator : AbstractValidator<Claim>
    {
        public ClaimValidator()
        {
            RuleFor(b => b.Number).NotEmpty();
            RuleFor(b => b.PolicyId).NotEmpty();
            RuleFor(b => b.ClaimDate).GreaterThanOrEqualTo(e => e.OccurrenceDate);
            RuleFor(b => b.OccurrenceDate).LessThanOrEqualTo(System.DateTime.Now);
            RuleFor(b => b.DescriptionFacts).NotEmpty();
        }
    }
}
