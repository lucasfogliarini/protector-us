﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class BankAccountValidator : AbstractValidator<BankAccount>
    {
        public BankAccountValidator()
        {
            RuleFor(e => e.AccountNumber).Length(12).NotEmpty();
            RuleFor(e => e.RoutingNumber).NotEmpty();
        }
    }
}
