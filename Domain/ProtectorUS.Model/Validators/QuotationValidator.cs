﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class QuotationValidator : AbstractValidator<Quotation>
    {
        public QuotationValidator()
        {
            RuleFor(b => b.BrokerId).GreaterThan(0);
            RuleFor(b => b.BrokerageProductId).GreaterThan(0);
            RuleFor(b => b.RatingPlanFormVersionId).GreaterThan(0);
            RuleFor(b => b.RegionId).GreaterThan(0);
            RuleFor(b => b.InsuredId).NullOrGreaterThan(0);
            RuleFor(b => b.CampaignId).NullOrGreaterThan(0);
            RuleFor(b => b.RiskAnalysisJson).NotEmpty();
            RuleFor(b => b.ProponentEmail).EmailAddress();
            When(e => e.Id == 0, () =>
            {
                RuleFor(b => b.PolicyStartDate).GreaterThanOrEqualTo(e => e.InsuredTimeNow.Date).LessThanOrEqualTo(e => e.InsuredTimeNow.Date.AddDays(30));
            });
        }
    }
}
