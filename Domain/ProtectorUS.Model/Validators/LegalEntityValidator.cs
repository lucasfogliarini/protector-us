﻿using FluentValidation;
using System;

namespace ProtectorUS.Model
{
    public class LegalEntityValidator : AbstractValidator<LegalEntity>
    {
        public LegalEntityValidator()
        {
            RuleFor(e => e.FirstName).NotEmpty();
            RuleFor(e => e.LastName).NotEmpty();
            RuleFor(e => e.BusinessTaxId).NotEmpty();
            RuleFor(e => e.SSNLast4).Length(4).NotEmpty();
            RuleFor(e => e.PersonalIdNumber).Length(9);
            RuleFor(e => e.BirthDate).LessThan(DateTime.Now);
            RuleFor(e => e.Business).NotNull();
            When(e => e.Business != null, () =>
            {
                RuleFor(e => e.Business.Name).NotEmpty();
                RuleFor(e => e.Business.Address).NotNull();
                When(e => e.Business.Address != null, () =>
                {
                    RuleFor(e => e.Business.Address.StreetLine1).NotEmpty();
                    RuleFor(e => e.Business.Address.StreetLine2);
                });
            });
        }
    }
}
