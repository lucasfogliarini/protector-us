﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class EmailMarketingValidator : AbstractValidator<EmailMarketing>
    {
        public EmailMarketingValidator()
        {
            RuleFor(e => e.BrokerId).GreaterThan(0);
            RuleFor(e => e.WebpageProductId).GreaterThan(0);
            RuleFor(e => e.CoverId).IsInEnum();
            //RuleFor(e => e.Title).NotEmpty();
            //RuleFor(e => e.Body).NotEmpty();
            RuleFor(e => e.ButtonText).NotEmpty();
        }
    }
}
