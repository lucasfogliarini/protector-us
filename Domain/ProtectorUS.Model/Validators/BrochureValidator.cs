﻿using FluentValidation;

namespace ProtectorUS.Model
{
    public class BrochureValidator : AbstractValidator<Brochure>
    {
        public BrochureValidator()
        {
            RuleFor(e => e.TemplateType).IsInEnum();
            RuleFor(e => e.BrokerId).GreaterThan(0);
            RuleFor(e => e.WebpageProductId).GreaterThan(0);
        }
    }
}
