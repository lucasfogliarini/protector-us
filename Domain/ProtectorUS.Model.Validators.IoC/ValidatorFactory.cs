﻿using Autofac;
using FluentValidation;
using System;


namespace ProtectorUS.Model.Validators
{
    public class ValidatorFactory : ValidatorFactoryBase
    {
        private readonly IComponentContext _context;

        public ValidatorFactory(IComponentContext context)
        {
            _context = context;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return _context.Resolve(validatorType) as IValidator;
        }
    }
}
