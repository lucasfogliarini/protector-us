﻿using Autofac;
using FluentValidation;
using ProtectorUS.Model.Validation;

namespace ProtectorUS.Model.Validators.IoC
{
    public class ValidatorsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register the simulator, also a singleton and dependent on 
            builder.RegisterType<ValidationManager>().AsSelf().As<IValidationManager>().SingleInstance();
            //Register teh validator factory
            builder.RegisterType<ValidatorFactory>().As<IValidatorFactory>().InstancePerRequest();
        }
    }
}
