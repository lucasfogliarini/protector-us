﻿using FluentValidation;

namespace ProtectorUS.Model.Validators
{
    public class AddressValidator : AbstractValidator<Address>
    {
        public AddressValidator()
        {
            RuleFor(address => address.StreetLine1).NotEmpty();
        }
    }
}
