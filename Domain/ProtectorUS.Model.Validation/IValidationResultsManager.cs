﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Validation
{
    public interface IValidationResultsManager
    {
        bool HasError { get; }
        List<ValidationResult> ValidationResults { get; set; }
        void AddValidationResultNotValid(string errorMessage, string entityName = "");
        void AddValidationResultValid(string entityName, string propertyName);
        void ClearValidationResults();
        void ThrowValidationError();
        void Validate(object obj);
    }
}