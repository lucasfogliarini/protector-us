﻿using ProtectorUS.Exceptions;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProtectorUS.Model.Validation
{

    [Serializable]
    public class ValidationException : Exception
    {
        public List<ValidationResult> ValidationResults { get; private set; }

        public ValidationException(string message, List<ValidationResult> listValidationResults)
            : base(message)
        {
            ValidationResults = listValidationResults;
        }

        public ValidationException(string message)
            : base(message)
        {
            ValidationResults = new List<ValidationResult>();
        }
    }

}