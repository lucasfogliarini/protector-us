﻿using FluentValidation;
using ProtectorUS.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProtectorUS.Model.Validation
{
    public class ValidationResultsManager : IValidationResultsManager
    {
        private static ValidationResultsManager instance;

        public ValidationResultsManager()
        {
            ValidationResults = new List<ValidationResult>();
            Errors = new List<ValidationResult>();
        }

        public static ValidationResultsManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ValidationResultsManager();
                }
                return instance;
            }
        }

        public List<ValidationResult> Errors { get; set; }
        public List<ValidationResult> ValidationResults { get; set; }

        public bool HasError { get { return ValidationResults.Count > 0; } }

        public virtual void ThrowValidationError()
        {
            if (ValidationResults.Count == 0)
                return;

            StringBuilder validationMessage = new StringBuilder();

            List<ValidationResult> errors = new List<ValidationResult>();

            foreach (ValidationResult vr in ValidationResults)
            {
                if (vr.IsValid)
                    continue;

                errors.Add(vr);

                if (validationMessage.Length > 0)
                    validationMessage.AppendLine();

                if (vr.EntityName != "")
                    validationMessage.Append(vr.EntityName + " - ");
                if (vr.PropertyName != "")
                    validationMessage.Append(vr.PropertyName + " - ");

                validationMessage.Append(vr.ErrorMessage);
            }

            if (errors.Count > 0)
            {
                ClearValidationResults();
                throw new ValidationException(validationMessage.ToString(), errors);
            }
                
        }

        public virtual void AddValidationResultNotValid(string errorMessage, string entityName = "")
        {
            ValidationResults.Add(new ValidationResult(
                false, errorMessage, entityName));
        }

        public virtual void AddValidationResultValid(string entityName, string propertyName)
        {
            ValidationResults.Add(new ValidationResult(
                true, "", entityName, propertyName));
        }

        public virtual void ClearValidationResults()
        {
            ValidationResults = new List<ValidationResult>();
        }

        /// <summary>
        /// Verify if a entity is valid and add the validation result in a validation manager
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="_validationManager"></param>
        public virtual void Validate(object obj)
        {
            var validator = GetValidator(obj);
            var result = validator.Validate(obj);
            if (!result.IsValid)
            {
                foreach (var error in result.Errors)
                {
                    this.AddValidationResultNotValid(error.ErrorMessage, this.GetType().Name);
                }
            }
        }

        /// <summary>
        /// Get the instance of a validator by name convention e.g. ClassName+Validator
        /// </summary>
        /// <returns></returns>
        public IValidator GetValidator(object obj)
        {
            string typeName = $"{obj.GetType().FullName}Validator";
            var type = Type.GetType(typeName);

            var instance = (IValidator)Activator.CreateInstance(type);
            if (instance == null)
                throw new ValidatorNotImplementedException(obj.GetType().Name);

            return instance;
        }

    }
}