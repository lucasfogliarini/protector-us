﻿namespace ProtectorUS.Model.Validation
{
    public class ValidationResult
    {
        public bool IsValid { get; private set; }
        public string ErrorMessage { get; private set; }
        public string PropertyName { get; private set; }
        public string EntityName { get; private set; }

        public ValidationResult(bool isValid, string errorMessage,
            string entityName, string propertyName)
        {
            IsValid = isValid;
            ErrorMessage = errorMessage;
            PropertyName = propertyName;
            EntityName = entityName;
        }

        public ValidationResult(bool isValid, string errorMessage, string entityName)
        {
            IsValid = isValid;
            ErrorMessage = errorMessage;
            EntityName = entityName;
            PropertyName = string.Empty;
        }

        public ValidationResult(bool isValid, string errorMessage)
        {
            IsValid = isValid;
            ErrorMessage = errorMessage;
            EntityName = string.Empty;
            PropertyName = string.Empty;
        }
    }
}
