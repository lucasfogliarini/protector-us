﻿using Newtonsoft.Json;

namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanPriorActsCoverage
    {
        [JsonProperty("years")]
        public decimal Years { get; set; }

        [JsonProperty("value")]
        public decimal Value { get; set; }
    }
}