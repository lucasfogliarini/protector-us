﻿namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanOption
    {
        public string Value { get; set; }

        public decimal Factor { get; set; }
    }
}
