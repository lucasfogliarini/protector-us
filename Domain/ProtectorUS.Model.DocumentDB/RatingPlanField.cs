﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanField
    {
        public string Type { get; set; }

        public string Order { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("maxlength", NullValueHandling = NullValueHandling.Ignore)]
        public string MaxLength { get; set; }

        [JsonProperty("placeholder", NullValueHandling = NullValueHandling.Ignore)]
        public string Placeholder { get; set; }

        [JsonProperty("conditionals", NullValueHandling = NullValueHandling.Ignore)]
        public IList<RatingPlanCondition> Conditionals { get; set; }

        [JsonProperty("labels", NullValueHandling = NullValueHandling.Ignore)]
        public IList<RatingPlanLabel> Labels { get; set; }

        public IList<RatingPlanOption> Options { get; set; }

        [JsonProperty("validations", NullValueHandling = NullValueHandling.Ignore)]
        public IList<RatingPlanValidation> Validations { get; set; }
    }
}