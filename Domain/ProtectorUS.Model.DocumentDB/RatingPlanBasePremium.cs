﻿using Newtonsoft.Json;

namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanBasePremium
    {
        [JsonProperty("min")]
        public decimal Min { get; set; }

        [JsonProperty("max")]
        public decimal Max { get; set; }

        [JsonProperty("base")]
        public decimal Base { get; set; }

        [JsonProperty("incremetalplus")]
        public decimal IncremetalPlus { get; set; }

        [JsonProperty("incremetalexcess")]
        public decimal IncremetalExcess { get; set; }
    }
}