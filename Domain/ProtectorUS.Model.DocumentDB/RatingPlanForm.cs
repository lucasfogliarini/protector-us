﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanForm
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("formid")]
        public long FormId { get; set; }

        [JsonProperty("basepremium")]
        public IList<RatingPlanBasePremium> BasesPremium { get; set; }

        [JsonProperty("prioractscoverage")]
        public IList<RatingPlanPriorActsCoverage> PriorActsCoverage { get; set; }
        
        [JsonProperty("tax")]
        public decimal Tax { get; set; }

        [JsonProperty("policycost")]
        public decimal PolicyCost { get; set; }

        [JsonProperty("minvalue")]
        public decimal MinValue { get; set; }

        [JsonProperty("rounding")]
        public bool Rounding { get; set; }

        [JsonProperty("fields")]
        public IList<RatingPlanField> Fields { get; set; }
    }
}
