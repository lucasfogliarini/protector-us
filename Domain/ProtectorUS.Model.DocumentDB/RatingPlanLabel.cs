﻿using Newtonsoft.Json;

namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanLabel
    {
        [JsonProperty("textbefore", NullValueHandling = NullValueHandling.Ignore)]
        public string TextBefore { get; set; }

        [JsonProperty("textafter", NullValueHandling = NullValueHandling.Ignore)]
        public string TextAfter { get; set; }

        public string Conditional { get; set; }
    }
}
