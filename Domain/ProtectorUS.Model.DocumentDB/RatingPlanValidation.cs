﻿namespace ProtectorUS.Model.DocumentDB
{
    public class RatingPlanValidation
    {
        public string Operation { get; set; }

        public string Value { get; set; }

        public string Message { get; set; }
    }
}
