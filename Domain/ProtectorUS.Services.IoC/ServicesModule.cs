﻿using Autofac;

namespace ProtectorUS.Services.IoC
{
    using Authentication;
    
    /// <summary>
    /// Group registration of domain services.
    /// </summary>
    public class ServicesModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterType<RatingPlanFormService>(builder);
            RegisterType<AuthenticationService>(builder);
			RegisterType<LocalizationService>(builder);
            RegisterType<BrokerageService>(builder);
            RegisterType<BrokerService>(builder);
            RegisterType<InsuredService>(builder);
            RegisterType<OrderService>(builder);
            RegisterType<QuotationService>(builder);
            RegisterType<ClaimService>(builder);
            RegisterType<PolicyService>(builder);
            RegisterType<EndorsementService>(builder);
            RegisterType<CampaignService>(builder);
            RegisterType<ManagerUserService>(builder);
            RegisterType<ProductService>(builder);
            RegisterType<CancelReasonService>(builder);
            RegisterType<WebpageViewService>(builder);
            RegisterType<NotificationService>(builder);
            RegisterType<AppClientService>(builder);
            RegisterType<BlobService>(builder);
            RegisterType<NewsLetterService>(builder);
            RegisterType<EmailTemplateService>(builder);
            RegisterType<PdfReplacerService>(builder);
            RegisterType<AcordService>(builder);
            RegisterType<PaymentGatewayService>(builder);
            RegisterType<CompanyService>(builder);
            RegisterType<EmailMarketingService>(builder);
            RegisterType<BrochureService>(builder);
        }

        private void RegisterType<T>(ContainerBuilder builder)
        {
            builder.RegisterType<T>().AsImplementedInterfaces().InstancePerRequest();
        }
    }
}
