﻿using System;

namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(LegalEntity))]
    public class InputLegalEntityDTO : IInputDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalIdNumber { get; set; }
        public string VerificationDocumentFileId { get; set; }
        public string SSNLast4 { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BusinessTaxId { get; set; }
        public ManagedAccountBusinessDTO Business { get; set; }
    }
}
