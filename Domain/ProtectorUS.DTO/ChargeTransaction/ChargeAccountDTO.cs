﻿namespace ProtectorUS.DTO
{
    using Model;
    public class ChargeAccountDTO : IInputDTO
    {
        public InputLegalEntityDTO LegalEntity { get; set; }
        public BankAccount BankAccount { get; set; }
        public bool? AcceptTOS { get; set; }
    }
}
