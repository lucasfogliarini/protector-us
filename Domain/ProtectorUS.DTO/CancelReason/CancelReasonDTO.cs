﻿namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(CancelReason))]
    public class InputCancelReasonDTO : IOutputDTO
    {
        public string Description { get; set; }
    }

    [AutoMapFrom(typeof(CancelReason))]
    public class OutputCancelReasonDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }

}
