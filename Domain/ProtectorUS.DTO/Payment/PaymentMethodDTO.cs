﻿namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(PaymentMethod))]
    public class InputPaymentMethodDTO : IInputDTO
    {
        public string Name { get; set; }
    }

    [AutoMapFrom(typeof(PaymentMethod))]
    public class OutputPaymentMethodDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string Name { get; set; }
        public string Icon { get; set; }
    }
}
