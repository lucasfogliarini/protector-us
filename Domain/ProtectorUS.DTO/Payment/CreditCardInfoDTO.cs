﻿using ProtectorUS.Model;
using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    [AutoMapTo(typeof(CreditCardInfo))]
    public class CreditCardInfoDTO : IInputDTO, IOutputDTO
    {
        public string CardHoldName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required, StringLength(5,MinimumLength = 5)]
        public string ZipCode { get; set; }
        [Required]
        public string Number { get; set; }
        [Required]
        public string Cvc { get; set; }
        [Required]
        public string ExpirationYear { get; set; }
        [Required]
        public string ExpirationMonth { get; set; }
    }
}
