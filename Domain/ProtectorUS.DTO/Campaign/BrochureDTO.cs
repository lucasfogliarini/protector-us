﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{

    using Model;
    using Newtonsoft.Json;
    [AutoMapTo(typeof(Brochure))]
    public class InputBrochureDTO : IInputDTO
    {
        [Required]
        public BrochureTemplateType TemplateType { get; set; }
    }
}
