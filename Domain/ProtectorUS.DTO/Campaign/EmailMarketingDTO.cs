﻿using ProtectorUS.Model;

namespace ProtectorUS.DTO
{
    [AutoMapTo(typeof(EmailMarketing))]
    public class EmailMarketingDTO : IInputDTO
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string ButtonText { get; set; }
        public EmailMarketingCoverId CoverId { get; set; }
    }
    public class OutputEmailMarketingDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
    }
}
