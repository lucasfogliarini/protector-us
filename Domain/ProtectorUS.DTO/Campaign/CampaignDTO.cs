﻿using System;

namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapFrom(typeof(Campaign))]
    public class OutputCampaignItemDTO : IOutputDTO
    {
        public long Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public WebpageProductListItemDTO WebpageProduct { get; set; }
        public decimal Views { get; set; }
        public decimal Bound { get; set; }
        public CampaignType Type { get; set; }
        public string FilePath { get; set; }
    }
}
