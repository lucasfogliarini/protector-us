﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTwoWay(typeof(Reason))]
    public class ReasonDTO : IOutputDTO, IInputDTO
    {
        public string Description { get; set; }
    }
}
