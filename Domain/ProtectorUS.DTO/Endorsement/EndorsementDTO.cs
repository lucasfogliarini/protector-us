﻿namespace ProtectorUS.DTO
{
    using Model;
    using System;
    [AutoMapTo(typeof(Endorsement))]
    public class InputEndorsementDTO : IInputDTO
    {
        public long PolicyId { get; set; }
        public long ReasonId { get; set; }
        public EndorsementType Type { get; set; }
        public EndorsementStatus Status { get; set; }
    }

    [AutoMapFrom(typeof(Endorsement))]
    public class OutputEndorsementDTO : IOutputDTO
    {
        public long Id { get; set; }
        string _createdDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createdDate) ? DateTime.Parse(_createdDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createdDate = value; } }
        public long PolicyId { get; set; }
        public EndorsementType Type { get; set; }
        public ReasonDTO Reason { get; set; }
        public EndorsementStatus Status { get; set; }
        public OutputProductBaseDTO Product { get; set; }
    }
}
