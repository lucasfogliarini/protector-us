﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapFrom(typeof(Permission))]
    public class OutputPermissionDTO : AuditableEntity<long>, IOutputDTO
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string ActionApi { get; set; }
        public decimal Order { get; set; }
    }
}