﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public abstract class UserPictureDTO
    {
        string _picture;
        public string Picture { get { return (string.IsNullOrEmpty(_picture) ? User.AnonymousUserPicture : _picture); } set { _picture = value; } }
    }
    public abstract class UserBaseDTO : UserPictureDTO
    {
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public SocialTitle? SocialTitle { get; set; }
    }

    public abstract class InputUserDTO : UserBaseDTO, IInputDTO
    {
        [Required]
        public override string FirstName { get; set; }
        public override string LastName { get; set; }
    }
    
    public abstract class UserInsertDTO : InputUserDTO
    {
        [Required]
        public override string Email { get; set; }
    }

    public abstract class UserUpdateDTO : InputUserDTO
    {
        public UserStatus? Status { get; set; }
    }
    
    public abstract class UserSelfUpdateDTO : InputUserDTO
    {
        public string Password { get; set; }
        [Required]
        public override string Email { get; set; }
    }

    public interface IOutputUserDTO : IOutputDTO
    {
    }
    
    public abstract class OutputUserDTO : UserBaseDTO, IOutputUserDTO
    {
        public long Id { get; set; }
        public Gender? Gender { get; set; }
        public UserStatus Status { get; set; }
        public string Location { get { return Addresses?.FirstOrDefault()?.Region?.City?.Name; } }
        public IList<OutputAddressDTO> Addresses { get; set; }
        public IList<PhoneDTO> Phones { get; set; }
        string _createdDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createdDate) ? DateTime.Parse(_createdDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createdDate = value; } }       
    }

    [AutoMapFrom(typeof(User))]
    public class OutputUserLoginDTO : UserPictureDTO, IOutputUserDTO
    {
        public string Name { get { return FirstName + " " + LastName; } }
        public UserStatus Status { get; set; }
        [JsonIgnore]
        public string FirstName { get; set; }
        [JsonIgnore]
        public string LastName { get; set; }
    }

    [AutoMapFrom(typeof(User))]
    public class UserNameDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}
