﻿using ProtectorUS.Model;
using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    [AutoMapTo(typeof(NewsLetter))]
    public class InputNewsLetterDTO : IInputDTO
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Origin { get; set; }
    }

    [AutoMapFrom(typeof(NewsLetter))]
    public class OutputNewsLetterDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string Email { get; set; }
        public string Origin { get; set; }
    }

}