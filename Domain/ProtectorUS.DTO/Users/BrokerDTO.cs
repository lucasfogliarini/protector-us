﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;


    [AutoMapTo(typeof(Broker))]
    public class BrokerInsertDTO : UserInsertDTO
    {
        public long ProfileId { get; set; }
    }
    [AutoMapTo(typeof(Broker))]
    public class BrokerUpdateDTO : UserUpdateDTO
    {
    }

    [AutoMapTo(typeof(Broker))]
    public class BrokerSelfUpdateDTO : UserSelfUpdateDTO
    {
    }

    [AutoMapFrom(typeof(Broker))]
    public class OutputBrokerDTO : OutputUserDTO
    {
        public OutputBrokerageDTO Brokerage { get; set; }
        public IReadOnlyCollection<OutputPolicyItemDTO> Policies { get; set; }
    }

    [AutoMapFrom(typeof(Broker))]
    public class OutputMyProtectorMobileBrokerDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string BrokerageName  { get; set; }
    }

    [AutoMapFrom(typeof(Broker))]
    public class OutputProfileBrokerDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SocialTitle? SocialTitle { get; set; }
        public string Profile { get { return Profiles?.FirstOrDefault()?.Name; } }
        string _birthDate;
        public string BirthDate { get { return (!string.IsNullOrEmpty(_birthDate) ? DateTime.Parse(_birthDate).ToString("MMMM dd, yyyy") : string.Empty); } set { _birthDate = value; } }
        public string Email { get; set; }
        public bool Onboarding { get { return Status == UserStatus.Onboarding; } }
        [JsonIgnore]
        public UserStatus Status { get; set; }
        public string LastChange { get; set; }
        public string BrokerageName { get; set; }
        public string BrokerageAddressRegionZipCode { get; set; }
        public string BrokerageAddressStreetLine1 { get; set; }
        public string BrokerageAddressStreetLine2 { get; set; }
        public ChargeAccountStatus BrokerageChargeAccountStatus { get; set; }        
        [JsonIgnore]
        public IList<Profile> Profiles { get; set; }
        public IList<PhoneDTO> Phones { get; set; }
    }

    [AutoMapFrom(typeof(Broker))]
    public class OutputBrokerListDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string Name { get { return FirstName + " " + LastName; } }
        public string Email { get; set; }
        public string Profile { get { return Profiles?.FirstOrDefault()?.Name?.ToLower(); } }
        public int CampaignsTotal { get { return Campaigns.Count(); } }
        public int PoliciesTotal { get { return Policies.Count();  } }
        public int Status { get; set; }
        [JsonIgnore]
        public IList<OutputPolicyDTO> Policies { get; set; }
        [JsonIgnore]
        public IList<Campaign> Campaigns { get; set; }
        [JsonIgnore]
        public string FirstName { get; set; }
        [JsonIgnore]
        public string LastName { get; set; }
        [JsonIgnore]
        public IList<Profile> Profiles { get; set; }
        public IList<PhoneDTO> Phones { get; set; }
    }

    [AutoMapFrom(typeof(Broker))]
    public class OutputBrokerOnboardDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IList<PhoneDTO> Phones { get; set; }
        public IList<OutputAddressDTO> Addresses { get; set; }
    }


    [AutoMapFrom(typeof(Broker))]
    public class OutputBrokerBaseDTO : UserPictureDTO, IOutputDTO
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public long Id { get; set; }
        public string Email { get; set; }
        public string Location { get { return Addresses.Count() > 0 ? Addresses.FirstOrDefault().Region.City.Name + "," + Addresses.FirstOrDefault().Region.City.State.Abbreviation : ""; } }
        public OutputBrokerageDTO Brokerage { get; set; }
        [JsonIgnore]
        public IList<OutputAddressDTO> Addresses { get; set; }
    }

    public class OutputBrokerInactivateReactivate 
    {
        public int Activateds { get; set; }
        public int Inactivateds { get; set; }
    }
}