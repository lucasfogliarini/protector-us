﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;

    [AutoMapTo(typeof(Insured))]
    public class InsuredInsertDTO : IInputDTO
    {
        [Required]
        public InputBusinessDTO Business { get; set; }
    }

    [AutoMapTo(typeof(Insured))]
    public class InsuredCheckTermostDTO : IInputDTO
    {
        public bool AcceptedTerms { get; set; }
    }


    public class InsuredAccepTermsDTO : IInputDTO
    {
        public bool Accept { get; set; }
    }

    public class InsuredSelfUpdateDTO : UserSelfUpdateDTO
    {
    }

    [AutoMapFrom(typeof(Insured))]
    public class OutputInsuredDTO : OutputUserDTO
    {
        public bool HasActivePolicy { get; set; }
        public IList<OutputPolicyDTO> Policies { get; set; }
        public IList<OutputClaimDTO> Claims { get; set; }
        public IList<OutputCoverageDTO> Coverages { get; set; }
        public IList<OutputEndorsementDTO> Endorsements { get; set; }
    }

    [AutoMapFrom(typeof(Insured))]
    public class OutputInsuredInfoDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Location
        {
            get
            {
                var city = Addresses?.FirstOrDefault()?.Region?.City;
                return city == null ? "" : $"{city.Name}, {city.State?.Abbreviation}";
            }
        }
        string _birthDate;
        public string BirthDate { get { return (!string.IsNullOrEmpty(_birthDate) ? DateTime.Parse(_birthDate).ToString("MMM dd, yyyy") : string.Empty); } set { _birthDate = value; } }
        [JsonIgnore]
        public IList<OutputAddressDTO> Addresses { get; set; }

    }
    public class InputInsuredProfileDTO
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class InputInsuredPictureProfileDTO
    {
        [Required]
        public string Picture { get; set; }
    }

    public class UpdateInsuredByBrokerDTO
    {
        public string Email { get; set; }
        public PhoneBaseDTO Phone { get; set; }
    }

    [AutoMapFrom(typeof(Insured))]
    public class OutputInsuredListDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserStatus Status { get; set; }
        string _createdDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createdDate) ? DateTime.Parse(_createdDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createdDate = value; } }
        public virtual IList<OutputInsuredPolicyItemDTO> Policies { get; set; }
    }

    [AutoMapFrom(typeof(Policy))]
    public class OutputInsuredPolicyItemDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Number { get; set; }
        string _createDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createDate) ? DateTime.Parse(_createDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createDate = value; } }
        public string PeriodDate { get { return PeriodStart + " - " + PeriodEnd; } }
        public decimal? TotalLoss { get { return Claims.Count() > 0 ? Claims.Sum(x => x.LossReserve) : 0; } }
        public decimal TotalPremium { get; set; }
        public string BusinessName { get; set; }
        public PolicyStatus Status { get; set; }
        string _periodStart;
        [JsonIgnore]
        public string PeriodStart { get { return (!string.IsNullOrEmpty(_periodStart) ? DateTime.Parse(_periodStart).ToString("MMM dd, yyyy") : string.Empty); } set { _periodStart = value; } }
        string _periodEnd;
        [JsonIgnore]
        public string PeriodEnd { get { return (!string.IsNullOrEmpty(_periodEnd) ? DateTime.Parse(_periodEnd).ToString("MMM dd, yyyy") : string.Empty); } set { _periodEnd = value; } }
        [JsonIgnore]
        public IEnumerable<Claim> Claims { get; set; }
        [JsonProperty("TotalRevenue")]
        public decimal OrderQuotationRevenue { get; set; }
        [JsonProperty("ProductName")]
        public string ConditionProductName { get; set; }
        [JsonProperty("ProductAbbreviation")]
        public string ConditionProductAbbreviation { get; set; }
        [JsonProperty("Deductible")]
        public decimal DeductibleEachClaim { get; set; }
        public decimal CoverageEachClaim { get; set; }

    }

    #region MyProtector App


    [AutoMapFrom(typeof(Insured))]
    public class MyProtectorMobileOutputInsuredDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IList<MyProtectorMobileOutputPolicyDTO> Policies { get; set; }
    }
    [AutoMapFrom(typeof(Insured))]
    public class MyProtectorOutputInsuredDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public virtual IList<MyProtectorOutputPolicyDTO> Policies { get; set; }
    }

    [AutoMapFrom(typeof(Insured))]
    public class MyProtectorOutputResumeInsuredDTO : UserPictureDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public virtual IList<MyProtectorOutputResumePolicyDTO> Policies { get; set; }
    }

    #endregion
}
