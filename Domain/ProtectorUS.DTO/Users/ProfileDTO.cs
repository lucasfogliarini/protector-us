﻿using ProtectorUS.Model;

namespace ProtectorUS.DTO
{
    [AutoMapFrom(typeof(Profile))]
    public class OutputProfileDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}