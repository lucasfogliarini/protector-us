﻿using ProtectorUS.Model;

namespace ProtectorUS.DTO
{
    [AutoMapFrom(typeof(Company))]
    public class CompanyDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}