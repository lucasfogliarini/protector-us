﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    [AutoMapTo(typeof(ManagerUser))]
    public class ManagerUserInsertDTO : IInputDTO
    {
        [Required]
        public long ProfileId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public long CompanyId { get; set; }
        public string LastName { get; set; }
    }
    [AutoMapTo(typeof(ManagerUser))]
    public class ManagerUserUpdateDTO
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public long CompanyId { get; set; }
        public long ProfileId { get; set; }
    }

    [AutoMapTo(typeof(ManagerUser))]
    public class ManagerUserSelfUpdateDTO : UserSelfUpdateDTO
    {
    }

    [AutoMapFrom(typeof(ManagerUser))]
    public class OutputManagerUserDTO : OutputUserDTO
    {
        public string LastChange { get; set; }
    }

    [AutoMapFrom(typeof(ManagerUser))]
    public class OutputManagerUserItemDTO : UserBaseDTO, IOutputUserDTO
    {
        public string Id { get; set; }

        [JsonIgnore]
        public IList<Profile> Profiles { get; set; }
        public long? ProfileId { get { return Profiles?.FirstOrDefault()?.Id; } }
        public UserStatus Status { get; set; }
    }

    [AutoMapFrom(typeof(ManagerUser))]
    public class OutputManagerUserProfileDTO : OutputManagerUserDTO
    {
        [JsonIgnore]
        public IList<Profile> Profiles { get; set; }
        public string Profile { get { return Profiles?.FirstOrDefault()?.Name; } }

    }
}