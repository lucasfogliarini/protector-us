﻿using Autofac;
using AutoMapper;
using AutoMapper.Mappers;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace ProtectorUS.DTO
{
    using Extensions;
    public class AutoMapperModule : Autofac.Module
    {
        private static bool _createdMappingsBefore = false;
        private static readonly object _syncObj = new object();
        protected override void Load(ContainerBuilder builder)
        {

            var myAssembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(myAssembly)
                .Where(t => t.IsSubclassOf(typeof(Profile))).As<Profile>();

            // Here's where you dynamically get all the registered profiles
            // and add them at the same time to the config store.
            builder.Register(ctx =>
            {
                // Create a new AutoMapper Configuration Store.
                var configurationStore = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);

                // Tells AutoMapper to use Autofac container to resolve its dependencies
                configurationStore.ConstructServicesUsing(ctx.Resolve);

                var profiles = ctx.Resolve<IEnumerable<Profile>>();
                foreach (var profile in profiles)
                {
                    configurationStore.AddProfile(profile);
                }

                CreateMappings(configurationStore);

                return configurationStore;
            })
            .SingleInstance() // We only want a singleton of the configuration
            .AutoActivate() // When the container is built we'll create this singleton
            .As<IConfigurationProvider>()
            .As<IConfiguration>();

            builder.RegisterType<MappingEngine>().As<IMappingEngine>();
       }

        internal static void CreateMappings(ConfigurationStore configurationStore)
        {
            lock (_syncObj)
            {
                //We should prevent duplicate mapping in an application, since AutoMapper is static.
                if (_createdMappingsBefore)
                {
                    return;
                }

               FindAndAutoMapTypes(configurationStore);
               
               // dont´t work in test project
               // _createdMappingsBefore = true;
            }
        }

        private static void FindAndAutoMapTypes(ConfigurationStore configurationStore)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            var types = Find(type =>
            type.IsDefined(typeof(AutoMapAttribute), true) ||
            type.IsDefined(typeof(AutoMapFromAttribute), true) ||
            type.IsDefined(typeof(AutoMapToAttribute), true));


            foreach (var type in types)
            {
                AutoMapperHelper.CreateMap(type, configurationStore);
            }
        }

        public static Type[] Find(Func<Type, bool> predicate)
        {
            return typeof(IDTO).Assembly.GetTypes().Where(predicate).ToArray();
        }

        private static List<Type> GetAllTypes()
        {
            var allTypes = new List<Type>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().ToList().Distinct())
            {
                Type[] typesInThisAssembly;

                typesInThisAssembly = assembly.GetTypes();


                if (typesInThisAssembly.IsNullOrEmpty())
                {
                    continue;
                }

                allTypes.AddRange(typesInThisAssembly.Where(type => type != null));

            }

            return allTypes.ToList();
        }
    }
}
