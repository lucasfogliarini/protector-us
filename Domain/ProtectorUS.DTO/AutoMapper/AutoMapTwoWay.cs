﻿using System;

namespace ProtectorUS.DTO
{
    public class AutoMapTwoWayAttribute : AutoMapAttribute
    {
        internal override AutoMapDirection Direction
        {
            get { return AutoMapDirection.TwoWay; }
        }

        public AutoMapTwoWayAttribute(params Type[] targetTypes)
            : base(targetTypes)
        {

        }
    }
}