﻿using System;

namespace ProtectorUS.DTO
{ 
    [Flags]
    public enum AutoMapDirection
    {
        From,
        To,
        TwoWay
    }
}