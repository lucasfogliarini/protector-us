﻿using System;
using System.Reflection;
using AutoMapper;
using System.Collections.Generic;


namespace ProtectorUS.DTO
{
    using ProtectorUS.Extensions;
    internal static class AutoMapperHelper
    {
        public static void CreateMap(Type type, ConfigurationStore configurationStore)
        {
            CreateMap<AutoMapFromAttribute>(type, configurationStore);
            CreateMap<AutoMapToAttribute>(type, configurationStore);
            CreateMap<AutoMapAttribute>(type, configurationStore);
        }


        public static void CreateMap<TAttribute>(Type type, ConfigurationStore configurationStore)
            where TAttribute : AutoMapAttribute
        {
            if (!type.IsDefined(typeof (TAttribute)))
            {
                return;
            }

            foreach (var autoMapToAttribute in type.GetCustomAttributes<TAttribute>())
            {
                if (autoMapToAttribute.TargetTypes.IsNullOrEmpty())
                {
                    continue;
                }

                foreach (var targetType in autoMapToAttribute.TargetTypes)
                {
                    if (autoMapToAttribute.Direction ==  AutoMapDirection.To)
                    {

                            configurationStore.CreateMap(type, targetType);

                    }

                    if (autoMapToAttribute.Direction == AutoMapDirection.From)
                    {
                      //  if (targetType.IsEnum)
                      //      configurationStore.CreateMap(targetType, Type).ForMember(ev => ev,
                      //          m => m.MapFrom(a => ((Enum)a).Description())
                      //);
                      //  else
                            configurationStore.CreateMap(targetType, type);
                    }

                    if (autoMapToAttribute.Direction == AutoMapDirection.TwoWay)
                    {
                        configurationStore.CreateMap(type, targetType);
                        configurationStore.CreateMap(targetType, type);
                    }
                }
            }
        }
    }
}