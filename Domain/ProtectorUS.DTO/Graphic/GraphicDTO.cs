﻿using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    public class GraphicDTO : IOutputDTO
    {
        public string Name { get; set; }
        public IList<ItemGraphicDTO> List { get; set; }
    }

    public class ItemGraphicDTO : IOutputDTO
    {
        public string Key { get; set; }
        public decimal Value { get; set; }
    }
}
