﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(RiskAnalysisField))]
    public class InputRiskAnalysisFieldDTO : IInputDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    [AutoMapFrom(typeof(RiskAnalysisField))]
    public class OutputRiskAnalysisFieldDTO : IOutputDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string ValueLabel { get; set; }
    }
}