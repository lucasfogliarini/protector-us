﻿using ProtectorUS.Model;
using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    [AutoMapTo(typeof(RiskAnalysis))]
    public class InputRiskAnalysisDTO : IInputDTO
    {
        public string HashCode { get; set; }
        public IList<InputRiskAnalysisFieldDTO> Fields { get; set; }
    }
    [AutoMapFrom(typeof(RiskAnalysis))]
    public class OutputRiskAnalysisDTO : IOutputDTO
    {
        public IList<OutputRiskAnalysisFieldDTO> Fields { get; set; }
    }    
}
