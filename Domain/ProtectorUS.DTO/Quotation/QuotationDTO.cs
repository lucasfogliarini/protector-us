﻿using System;
namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTwoWay(typeof(Quotation))]
    public class QuotationHashDTO : IOutputDTO, IInputDTO
    {
        public Guid Hash { get; set; }
    }

    [AutoMapFrom(typeof(Quotation))]
    public class QuotationResponseDTO : QuotationHashDTO
    {
        public decimal PolicyPremium { get; set; }
        public bool IsDeclined { get; set; }
    }

    public class QuotationContactDTO : QuotationHashDTO
    {
        public string ProponentPhone { get; set; }
    }

    public abstract class QuotationBaseDTO : QuotationHashDTO
    {
        private const string regexAllowLettersDotSpaceAccents = "^[a-zA-Zà-úÀ-Ú\\.\\s]*$";
        public SocialTitle? ProponentSocialTitle { get; set; }
        [MinLength(2), MaxLength(50)]
        [Required]

        [RegularExpression(regexAllowLettersDotSpaceAccents, ErrorMessage = "The first name must not contain non-alphanumeric characters.")]
        public string ProponentFirstName { get; set; }
        [MinLength(2), MaxLength(50)]
        [Required]
        [RegularExpression(regexAllowLettersDotSpaceAccents, ErrorMessage = "The last name must not contain non-alphanumeric characters.")]
        public string ProponentLastName { get; set; }

        [Required]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,10})$", ErrorMessage = "Invalid email format.")]
        public string ProponentEmail { get; set; }

    }
    [AutoMapTo(typeof(Quotation))]
    public class QuotationInsertDTO : QuotationBaseDTO, IInputDTO
    {
        public long? InsuredId { get; set; }
        public long? CampaignId { get; set; }
        [Required]
        public InputRiskAnalysisDTO RiskAnalysis { get; set; }
        [Required]
        public long BrokerageProductId { get; set; }
    }
    [AutoMapTo(typeof(Quotation))]
    public class QuotationUpdateDTO : QuotationBaseDTO, IInputDTO
    {
        public decimal? CoverageLimit { get; set; }
        public decimal? Revenue { get; set; }
        public DateTime? PolicyStartDate { get; set; }
    }

    [AutoMapFrom(typeof(Quotation))]
    public class OutputQuotationDTO : QuotationBaseDTO, IOutputDTO
    {
        public long Id { get; set; }
        public OutputRiskAnalysisDTO RiskAnalysis { get; set; }
        public ActivationStatus Status { get; set; }
        public string RegionZipCode { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime PolicyStartDate { get; set; }
        public decimal PolicyPremium { get; set; }
        public decimal SurchargeAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal Total { get; set; }
    }
}
