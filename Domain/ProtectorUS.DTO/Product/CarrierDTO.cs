﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapFrom(typeof(Carrier))]
    public class CarrierDTO : IOutputDTO
    {
        public string Name { get; set; }
        public OutputAddressDTO Address { get; set; }
    }
    
}