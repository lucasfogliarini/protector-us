﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapFrom(typeof(ProductCondition))]
    public class OutputProductConditionBaseDTO : IOutputDTO
    {
        public OutputProductBaseDTO Product { get; set; }
    }

    [AutoMapFrom(typeof(ProductCondition))]
    public class OutputProductConditionDTO : OutputProductConditionBaseDTO
    {
        public OutputStateDTO State { get; set; }
        public string StateCarrierName { get; set; }
        public long StateId { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public bool Active { get; set; }
    }
}