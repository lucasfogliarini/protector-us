﻿using ProtectorUS.Model;

namespace ProtectorUS.DTO
{

    [AutoMapFrom(typeof(Coverage))]
    public class OutputCoverageDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
