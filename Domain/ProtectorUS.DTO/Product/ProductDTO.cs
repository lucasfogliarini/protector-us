﻿namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    [AutoMapFrom(typeof(Product))]
    public class OutputProductBaseDTO : IOutputDTO
    {
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Abbreviation { get; set; }
        public string Slogan { get; set; }
        public bool Active { get; set; }
        public long Id { get; set; }
        
    }


    [AutoMapFrom(typeof(Product))]
    public class OutputProductDTO : OutputProductBaseDTO
    {
        public string Code { get; set; }
        public string Icon { get; set; }
        public new bool Active { get; set; }
    }
}