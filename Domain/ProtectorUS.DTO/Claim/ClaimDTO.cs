﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(Claim))]
    public class InputClaimDTO : IInputDTO
    {
        [Required]
        public DateTime OccurrenceDate { get; set; }
        [Required]
        public DateTime ClaimDate { get; set; }
        [Required]
        public bool PriorNotification { get; set; }
        public string DescriptionFacts { get; set; }
        public InputThirdPartyDTO ThirdParty { get; set; }
        public IEnumerable<InputClaimDocumentDTO> ClaimDocuments { get; set; }
    }

    [AutoMapFrom(typeof(Claim))]
    public class OutputClaimDTO : IOutputDTO
    {
        public long Id { get; set; }
        string _occurrenceDate;
        public string OccurrenceDate { get { return (!string.IsNullOrEmpty(_occurrenceDate) ? DateTime.Parse(_occurrenceDate).ToString("MMM dd, yyyy") : string.Empty); } set { _occurrenceDate = value; } }
        public string Number { get; set; }
        string _claimDate;
        public string ClaimDate { get { return (!string.IsNullOrEmpty(_claimDate) ? DateTime.Parse(_claimDate).ToString("MMM dd, yyyy") : string.Empty); } set { _claimDate = value; } }
        string _createdDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createdDate) ? DateTime.Parse(_createdDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createdDate = value; } }
        public bool PriorNotification { get; set; }
        public ClaimStatus Status { get; set; }
        public decimal? LossReserve { get; set; }
        public decimal? ALAEReserve { get; set; }
        public decimal? TotalPaid { get; set; }
        public string DescriptionFacts { get; set; }
        public OutputThirdPartyDTO ThirdParty { get; set; }
        public OutputPolicyItemDTO Policy { get; set; }
        public IEnumerable<OutputClaimDocumentDTO> ClaimDocuments { get; set; }
    }

    [AutoMapTo(typeof(Claim))]
    public class MyProtectorInputClaimDTO : IInputDTO
    {
        [Required]
        public long PolicyId { get; set; }
        [Required]
        public DateTime OccurrenceDate { get; set; }
        [Required]
        public DateTime ClaimDate { get; set; }
        [Required]
        public bool PriorNotification { get; set; }
        public string DescriptionFacts { get; set; }

        public InputThirdPartyDTO ThirdParty { get; set; }
        public IEnumerable<MyProtectorMobileInputClaimDocumentDTO> ClaimDocuments { get; set; }
    }

    [AutoMapFrom(typeof(Claim))]
    public class MyProtectorOutputPolicyClaimsDTO
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public long PolicyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime OccurrenceDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public string DescriptionFacts { get; set; }
        public ClaimStatus Status { get; set; }
        public OutputThirdPartyDTO ThirdParty { get; set; }
        public List<MyProtectorOutputClaimDocumentDTO> ClaimDocuments { get; set; }
    }
}
