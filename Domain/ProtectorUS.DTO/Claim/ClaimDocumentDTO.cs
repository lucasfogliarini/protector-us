﻿namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(ClaimDocument))]
    public class InputClaimDocumentDTO : IInputDTO
    {
        public string Path { get; set; }
    }

    [AutoMapTwoWay(typeof(ClaimDocument))]
    public class OutputClaimDocumentDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public FileType Type { get; set; }
        public string Path { get; set; }
        public long ClaimId { get; set; }
    }
    [AutoMapTwoWay(typeof(ClaimDocument))]
    public class MyProtectorOutputClaimDocumentDTO : IOutputDTO
    {
        public long Id { get; set; }
        public FileType Type { get; set; }
        public string Path { get; set; }
        public long ClaimId { get; set; }
    }


    [AutoMapTwoWay(typeof(ClaimDocument))]
    public class MyProtectorMobileInputClaimDocumentDTO : IOutputDTO
    {
        public string Path { get; set; }
    }
}
