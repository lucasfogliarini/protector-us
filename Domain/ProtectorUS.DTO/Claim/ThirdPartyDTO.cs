﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(ThirdParty))]
    public class InputThirdPartyDTO : IInputDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    [AutoMapTwoWay(typeof(ThirdParty))]
    public class OutputThirdPartyDTO :  IOutputDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
