﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    
    public abstract class BrokerageDTO
    {
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual IList<PhoneDTO> Phones { get; set; }
        public virtual string AgentNumber { get; set; }

    }

    [AutoMapTo(typeof(Brokerage))]
    public class InputBrokerageDTO : BrokerageDTO, IInputDTO
    {
        [Required]
        public override string Name { get; set; }
        [Required]
        public override string Email { get; set; }
        [Required]
        public AddressDTO Address { get; set; }
        [Required]
        public IList<InputWebpageProductDTO> WebpageProducts { get; set; }
    }
    [AutoMapTo(typeof(Brokerage))]
    public class InputBrokerageUpdateDTO : BrokerageDTO, IInputDTO
    {
        public string Logo { get; set; }
        [Required]
        public string Alias { get; set; }
        [Required]
        public override string Name { get; set; }
        [Required]
        public override string Email { get; set; }
        [Required]
        public AddressDTO Address { get; set; }
    }

    [AutoMapTwoWay(typeof(Brokerage))]
    public class BrokerageDefaultWebPageDTO : IInputDTO, IOutputDTO
    {
        public string Logo { get; set; }
        public string Cover { get; set; }
    }


    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageDTO : BrokerageDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string Alias { get; set; }
        public BrokerageStatus Status { get; set; }
        public string Cover { get; set; }
        public string Logo { get; set; }
        public IList<BrokerageProductDTO> BrokerageProducts { get; set; }
        public IList<OutputBrokerDTO> Brokers { get; set; }
        public virtual OutputAddressDTO Address { get; set; }
        public string Address1 { get { return $"{Address?.StreetLine1} {Address?.StreetLine2}, { Address?.Region?.City?.Name}, { Address?.Region?.City?.State?.Abbreviation}"; } }
        public string Address2 { get { return $"{Address?.StreetLine1} {Address?.StreetLine2}"; } }
        public string Address3 { get { return $"{Address?.Region?.City?.Name}, {Address?.Region?.City?.State?.Abbreviation} { Address?.Region?.ZipCode}"; } }
        public ChargeAccountStatus ChargeAccountStatus { get; set; }
        string _createDate;
        public string CreatedDate { get { return (!string.IsNullOrEmpty(_createDate) ? DateTime.Parse(_createDate).ToString("MMM dd, yyyy") : string.Empty); } set { _createDate = value; } }
    }

    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageInfoDTO : IOutputDTO
    {
        public long Id { get; set; }
        public virtual string Name { get; set; }
        public BrokerageStatus Status { get; set; }
        public string Cover { get; set; }
        public string Logo { get; set; }
    }

    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageItemDTO : IOutputDTO
    { 
        public long Id { get; set; }

        public string Name { get; set; }
        public BrokerageStatus Status { get; set; }
        public string City { get { return Address?.Region?.City.Name; } }
        public string State { get { return Address?.Region?.City.State?.Abbreviation; } }
        public int? BrokersLength { get { return Brokers?.Count; } }
        public int? PoliciesLength { get { return Brokers?.Sum(b => b.Policies.Count); } }
        [JsonIgnore]
        public OutputAddressDTO Address { get; set; }
        [JsonIgnore]
        public IReadOnlyCollection<OutputBrokerDTO> Brokers { get; set; }
        public string Logo { get; set; }
    }



    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageTopListDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Logo { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string City { get { return Address?.Region?.City.Name + "," + Address?.Region?.City.State?.Abbreviation; } }
        [JsonIgnore]
        public OutputAddressDTO Address { get; set; }
        public decimal? Total { get { return Brokers?.Sum(b => b.Policies.Sum(p=>p.TotalPremium)); } }
        [JsonIgnore]
        public IReadOnlyCollection<OutputBrokerDTO> Brokers { get; set; }
    }

    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageMyProtectorDTO : OutputBrokerageDTO
    {
        [JsonIgnore]
        public override OutputAddressDTO Address { get; set; }
    }

    [AutoMapFrom(typeof(Brokerage))]
    public class OutputBrokerageOnboardDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public PhoneDTO Phone { get { return Phones?.FirstOrDefault(); } }
        public string ZipCode { get { return Address?.Region?.ZipCode; } }
        public string City { get { return Address?.Region?.City.Name; } }
        public string State { get { return Address?.Region?.City.State?.Abbreviation; } }
        public string StreetLine1 { get { return Address?.StreetLine1; } }
        public string StreetLine2 { get { return Address?.StreetLine2; } }
        public IReadOnlyCollection<PhoneDTO> Phones { get; set; }
        public OutputAddressDTO Address { get; set; }
    }
}
