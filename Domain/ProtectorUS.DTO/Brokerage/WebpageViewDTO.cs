﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(WebpageView))]
    public class InputWebpageViewDTO : IInputDTO
    {
        [Required]
        public AccessType Access { get; set; }
        public long? CampaignId { get; set; }
        public long BrokerageId { get; set; }
        [Required]
        public long? WebpageProductId { get; set; }
    }

    [AutoMapTwoWay(typeof(WebpageView))]
    public class OutputWebpageViewDTO : IOutputDTO
    {
        public long Id { get; set; }
        public AccessType Access { get; set; }
        public long? CampaignId { get; set; }
        public long BrokerageId { get; set; }
        public long? WebpageProductId { get; set; }

    }

}