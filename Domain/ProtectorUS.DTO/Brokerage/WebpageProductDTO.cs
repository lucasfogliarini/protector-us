﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;


    [AutoMapTo(typeof(WebpageProduct))]
    public class InputWebpageProductDTO : IInputDTO
    {
        [Required]
        public long ProductId { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public decimal BrokerageComission { get; set; }
    }

    [AutoMapTwoWay(typeof(WebpageProduct))]
    public class WebpageProductDTO : InputWebpageProductDTO, IOutputDTO
    {
        public long Id { get; set; }
        public string Alias { get; set; }
        public string Logo { get; set; }
        public string Cover { get; set; }
        public bool Default { get; set; }
        public OutputBrokerageProductDTO BrokerageProduct { get; set; }

    }

    [AutoMapFrom(typeof(WebpageProduct))]
    public class WebpageProductListItemDTO : InputWebpageProductDTO
    {
        public OutputBrokerageProductDTO BrokerageProduct { get; set; }
    }

}