﻿using System.Collections.Generic;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;
    public class ARGOToggleProductDTO : IInputDTO
    {
        public bool Active { get; set; }
        public long ProductId { get; set; }
    }
    public class BrokerageToggleProductDTO : IInputDTO
    {
        public bool Active { get; set; }
        public long ProductId { get; set; }
    }

    public class BrokerageActiveInactiveBrokeragesAndAgentstDTO : IOutputDTO
    {
        public int ActiveBrokerages { get; set; }
        public int InactiveBrokerages { get; set; }
        public int ActiveAgents { get; set; }
    }

    [AutoMapFrom(typeof(BrokerageProduct))]
    public class BrokerageProductDTO : IOutputDTO
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        
        public decimal? BrokerageComission { get; set; }

        public bool ActiveARGO { get; set; }

        public bool ActiveProduct { get; set; }

        public bool ActiveBrokerage { get; set; }

        public bool Active { get; set; }
        public OutputProductBaseDTO Product { get; set; }
    }


    [AutoMapFrom(typeof(BrokerageProduct))]
    public class OutputBrokerageProductDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public bool ActiveARGO { get; set; }

        public bool ActiveProduct { get; set; }

        public bool ActiveBrokerage { get; set; }
        public bool Active { get; set; }
        public long ProductId { get; set; }
        public OutputProductBaseDTO Product { get; set; }
    }


    [AutoMapTwoWay(typeof(BrokerageProduct))]
    public class BrokerageProductEnableDisableInputDTO : IInputDTO
    {
        public bool Active { get; set; }
        public long BrokerageProductId { get; set; }
        public long ProductId { get; set; }
    }

    [AutoMapTwoWay(typeof(BrokerageProduct))]
    public class BrokerageProductEnableDisableOutputDTO : IOutputDTO
    {
        public bool ActiveARGO { get; set; }

        public bool ActiveProduct { get; set; }

        public bool ActiveBrokerage { get; set; }
        public long BrokerageProductId { get; set; }
        public long ProductId { get; set; }
    }
}