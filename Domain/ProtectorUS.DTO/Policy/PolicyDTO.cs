﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.DTO
{
    using Model;
    using Newtonsoft.Json;

    [AutoMapFrom(typeof(Policy))]
    public class PolicyNumberDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Number { get; set; }
    }


    [AutoMapFrom(typeof(Policy))]
    public class OutputPolicyItemDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string Number { get; set; }
        string _periodStart;
        public string PeriodStart { get { return (!string.IsNullOrEmpty(_periodStart) ? DateTime.Parse(_periodStart).ToString(Policy.dateFormat) : string.Empty); } set { _periodStart = value; } }
        string _periodEnd;
        public string PeriodEnd { get { return (!string.IsNullOrEmpty(_periodEnd) ? DateTime.Parse(_periodEnd).ToString(Policy.dateFormat) : string.Empty); } set { _periodEnd = value; } }
        public string Issued { get { return CreatedDate.ToString(Policy.dateFormat); } }
        public long OrderPaymentMethodId { get; set; }
        public PolicyType Type { get; set; }
        public PolicyStatus Status { get; set; }
        public OutputBrokerBaseDTO Broker { get; set; }
        public decimal CoverageEachClaim { get; set; }
        public decimal CoverageAggregate { get; set; }
        public decimal DeductibleEachClaim { get; set; }
        public decimal DeductibleAggregate { get; set; }
        public decimal? TotalLoss { get { return Claims?.Count() > 0 ?  Claims.Sum(x => x.LossReserve) : 0; } }
        public OutputInsuredInfoDTO Insured { get; set; }
        public OutputProductConditionDTO Condition { get; set; }
        [JsonIgnore]
        public IEnumerable<Claim> Claims { get; set; }
        public OutputBusinessDTO Business { get; set; }
        public decimal TotalPremium { get; set; }
        [JsonProperty("TotalRevenue")]
        public decimal OrderQuotationRevenue { get; set; }
    }

    [AutoMapFrom(typeof(Policy))]
    public class OutputPolicyDTO : OutputPolicyItemDTO
    {
        public string PolicyPath { get; set; }
        public string CertificatePath { get; set; }
        public OutputOrderDTO Order { get; set; }
        public IEnumerable<OutputCoverageDTO> Coverages { get; set; }
    }



    [AutoMapFrom(typeof(Policy))]
    public class MyProtectorOutputPolicyDTO : IOutputDTO
    {
        public long Id { get; set; }
        public OutputMyProtectorMobileBrokerDTO Broker { get; set; }
        public string Number { get; set; }
        public string ConditionProductName { get; set; }
        public string ConditionProductSlogan { get; set; }
        public string PolicyPath { get; set; }
        public string CertificatePath { get; set; }
        public PolicyType Type { get; set; }
        public PolicyStatus Status { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public DateTime OrderCreatedDate { get; set; }
        public string OrderPaymentStatus { get; set; }
        public string OrderPaymentMethodName { get; set; }
        public string OrderTotalInstallments { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal CoverageEachClaim { get; set; }
        public decimal CoverageAggregate { get; set; }
    }

    [AutoMapFrom(typeof(Policy))]
    public class MyProtectorOutputResumePolicyDTO : IOutputDTO
    {
        public long Id { get; set; }
        public OutputMyProtectorMobileBrokerDTO Broker { get; set; }
        public string Number { get; set; }
        public string ConditionProductName { get; set; }
        public string ConditionProductSlogan { get; set; }

    }

    [AutoMapFrom(typeof(Policy))]
    public class MyProtectorMobileOutputPolicyDTO : MyProtectorOutputPolicyDTO
    {
        public List<MyProtectorOutputPolicyClaimsDTO> Claims { get; set; }
        public List<OutputCoverageDTO> Coverages { get; set; }
    }
}
