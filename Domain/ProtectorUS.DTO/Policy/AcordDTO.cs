﻿using System.ComponentModel.DataAnnotations;
namespace ProtectorUS.DTO
{
    using Model;
    using System;
    [AutoMapTo(typeof(AcordTransaction))]
    public class InputAcordTransactionDTO : IInputDTO
    {
        public string TransactionId { get; set; }
        public string Xml { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    [AutoMapFrom(typeof(AcordTransaction))]
    public class OutputAcordTransactionDTO : IOutputDTO
    {
        public string TransactionId { get; set; }
        public string Xml { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
