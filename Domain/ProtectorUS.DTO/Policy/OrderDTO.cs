﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;
    using System.Collections.Generic;

    [AutoMapTo(typeof(Order))]
    public class InputOrderDTO : IInputDTO
    {
        [Required]
        public Guid QuotationHash { get; set; }
        [Required]
        public int TotalInstallments { get; set; }
        [Required]
        public CreditCardInfoDTO CreditCard { get; set; }
        [Required]
        public InputBusinessDTO Business { get; set; }
        [Required]
        public List<PhoneBaseDTO> Phones { get; set; }
    }

    [AutoMapFrom(typeof(Order))]
    public class OutputOrderDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string TransactionCode { get; set; }
        public int TotalInstallments { get; set; }
        string _orderDate;
        public string OrderDate { get { return (!string.IsNullOrEmpty(_orderDate) ? DateTime.Parse(_orderDate).ToString("MMM dd, yyyy") : string.Empty); } set { _orderDate = value; } }
        public OutputPolicyDTO Policy { get; set; }
        public decimal BrokerageFee { get; set; }
        public long QuotationId { get; set; }
        public long PaymentMethodId { get; set; }
        public OutputPaymentMethodDTO PaymentMethod { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
    }
}
