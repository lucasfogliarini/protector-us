﻿using System.ComponentModel.DataAnnotations;
namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(Business))]
    public class InputBusinessDTO : IInputDTO
    {
        public string Name { get; set; }
        public BusinessType Type { get; set; }
        [Required]
        public BusinessAddressDTO Address { get; set; }
    }

    [AutoMapTo(typeof(Business))]
    public class ManagedAccountBusinessDTO : IInputDTO
    {
        public string Name { get; set; }
        public long RegionId { get; set; }
        public AddressStreetDTO Address { get; set; }
    }

    [AutoMapFrom(typeof(Business))]
    public class OutputBusinessDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string Name { get; set; }
        public BusinessType Type { get; set; }
        public OutputAddressDTO Address { get; set; }
    }
}
