﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapFrom(typeof(AppClient))]
    public class OutputAppClientDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int RefreshTokenLifeTime { get; set; }
        public string AllowedOrigin { get; set; }
        public string CurrentVersion { get; set; }
        public string ReviewVersion { get; set; }
        public bool InReview { get; set; }
    }

    [AutoMapTo(typeof(AppClient))]
    public class InputAppClientDTO : IInputDTO
    {
        public bool? Active { get; set; }
        public string CurrentVersion { get; set; }
        public string ReviewVersion { get; set; }
        public bool? InReview { get; set; }
    }
}
