﻿namespace ProtectorUS.DTO.RatingPlan
{
    using Model.DocumentDB.RatingPlan;
    [AutoMapFrom(typeof(Condition))]
    public class OutputConditionDTO : IOutputDTO
    {
        public string Ref { get; set; }
        public string value { get; set; }
        public string Operator { get; set; }
    }
}
