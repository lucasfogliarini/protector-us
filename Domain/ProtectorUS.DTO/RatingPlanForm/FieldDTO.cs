﻿using System.Collections.Generic;

namespace ProtectorUS.DTO.RatingPlan
{
    using Model.DocumentDB.RatingPlan;
    using System.Linq;
    [AutoMapFrom(typeof(Field))]
    public class OutputFieldDTO : IOutputDTO
    {
        public string label { get; set; }
        public string labelAfter { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string placeholder { get; set; }
        public string tooltip { get; set; }
        public string classes { get; set; }
        public string invalidMessage { get; set; }
        public IList<OutputConditionDTO> conditionals { get; set; }

        #region text
        public int? maxlength { get; set; }
        public int? minlength { get; set; }
        #endregion

        #region select
        public ICollection<OutputOptionDTO> options { get; set; }
        #endregion
    }
}