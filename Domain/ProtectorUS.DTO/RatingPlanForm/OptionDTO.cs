﻿namespace ProtectorUS.DTO.RatingPlan
{
    using Model.DocumentDB.RatingPlan;
    using System.Collections.Generic;
    [AutoMapFrom(typeof(Option))]
    public class OutputOptionDTO : IOutputDTO
    {
        public string label { get; set; }
        public string value { get; set; }
        public IList<OutputConditionDTO> conditionals { get; set; }
    }
}
