﻿using ProtectorUS.Model.DocumentDB.RatingPlan;
using System.Collections.Generic;

namespace ProtectorUS.DTO.RatingPlan
{
    [AutoMapFrom(typeof(RatingPlanForm))]
    public class OutputRatingPlanFormDTO : IOutputDTO
    {
        public string HashCode { get; set; }
        public string minIssuedDate { get; set; }
        public IEnumerable<OutputFieldDTO> fields { get; set; }
    }
}
