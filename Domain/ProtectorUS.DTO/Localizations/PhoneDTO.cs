﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTwoWay(typeof(Phone))]
    public class PhoneBaseDTO : IInputDTO, IOutputDTO
    {
        public string Number { get; set; }
        public PhoneType Type { get; set; }
    }
    [AutoMapTwoWay(typeof(Phone))]
    public class PhoneDTO : IInputDTO, IOutputDTO
    {
        public long Id { get; set; }
        [Required]
        public string Number { get; set; }
        [EnumDataType(typeof(PhoneType), ErrorMessage = "The field Phone Type is invalid")]
        public PhoneType Type { get; set; }
        public string Description { get; set; }
    }
}