﻿
using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(Region))]
    public class InputRegionDTO : IInputDTO
    {
        [Required]
        public string ZipCode { get; set; }
    }

    [AutoMapFrom(typeof(Region))]
    public class OutputRegionDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string ZipCode { get; set; }
        public OutputCityDTO City { get; set; }
    }
}
