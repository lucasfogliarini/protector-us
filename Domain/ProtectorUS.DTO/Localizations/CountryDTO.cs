﻿namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTwoWay(typeof(Country))]
    public class InputCountryDTO : IInputDTO, IOutputDTO
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}
