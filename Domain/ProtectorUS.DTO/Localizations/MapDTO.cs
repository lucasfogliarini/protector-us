﻿using System.Collections.Generic;
namespace ProtectorUS.DTO
{

    #region General
    public class MapPointDTO : AuditableEntityDTO<long>, IOutputDTO
    {
        public string RequestInfo { get; set; }
        public virtual IList<PointDTO> Pins { get; set; }
    }

    public class PointDTO
    {
        public long Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public long idProduct { get; set; }
        public string Tag { get; set; }
        public string Image { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    #endregion

    #region BrokerageMyProtector
    public class MapBrokerageMyProtectorDTO : IOutputDTO
    {
        public virtual IList<PointBrokerageMyProtectorDTO> Pins { get; set; }
    }

    public class PointBrokerageMyProtectorDTO
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Alias { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    #endregion

}
