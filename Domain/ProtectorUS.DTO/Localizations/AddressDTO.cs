﻿using System.ComponentModel.DataAnnotations;

namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(Address))]
    public class BusinessAddressDTO : AddressStreetDTO, IInputDTO
    {
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }
    }

    [AutoMapTo(typeof(Address))]
    public class AddressStreetDTO : IDTO
    {
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
    }

    [AutoMapTwoWay(typeof(Address))]
    public class AddressDTO : AddressStreetDTO, IInputDTO, IOutputDTO
    {
        [Required]
        public long RegionId { get; set; }
    }

    [AutoMapFrom(typeof(Address))]
    public class OutputAddressDTO : AddressStreetDTO, IOutputDTO
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public long RegionId { get; set; }
        public OutputRegionDTO Region { get; set; }
    }
}
