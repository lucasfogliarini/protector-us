﻿namespace ProtectorUS.DTO
{
    using Model;
    [AutoMapTo(typeof(City))]
    public class InputCityDTO : IInputDTO
    {
        public OutputStateDTO State { get; set; }
    }

    [AutoMapFrom(typeof(City))]
    public class OutputCityDTO : IOutputDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long StateId { get; set; }
        public OutputStateDTO State { get; set; }
    }
}
