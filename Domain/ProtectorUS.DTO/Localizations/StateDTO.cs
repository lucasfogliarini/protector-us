﻿namespace ProtectorUS.DTO
{
    using Model;

    [AutoMapTo(typeof(State))]
    public class InputStateDTO : IInputDTO
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }

    [AutoMapFrom(typeof(State))]
    public class OutputStateDTO : IOutputDTO
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public InputCountryDTO Country { get; set; }
        public CarrierDTO Carrier { get; set; }
    }
}
