﻿using System;

namespace ProtectorUS.DTO
{
    /// <summary>
    /// Implements common properties for entity based DTOs.
    /// </summary>
    /// <typeparam name="TPrimaryKey">Type of the primary key</typeparam>
    [Serializable]
    public abstract class AuditableEntityDTO<TPrimaryKey> : IAuditableEntityDTO<TPrimaryKey>
    {
        /// <summary>
        /// Id of the entity.
        /// </summary>
        public TPrimaryKey Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        /// <summary>
        /// Creates a new <see cref="AuditableEntityDTO{TPrimaryKey}"/> object.
        /// </summary>
        public AuditableEntityDTO()
        {

        }

        /// <summary>
        /// Creates a new <see cref="AuditableEntityDTO{TPrimaryKey}"/> object.
        /// </summary>
        /// <param name="id">Id of the entity</param>
        public AuditableEntityDTO(TPrimaryKey id)
        {
            Id = id;
        }
    }
}
