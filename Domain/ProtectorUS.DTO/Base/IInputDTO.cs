﻿namespace ProtectorUS.DTO
{
    /// <summary>
    /// This interface is used to define DTOs those are used as input parameters.
    /// </summary>
    public interface IInputDTO : IDTO
    {
    }
}
