﻿namespace ProtectorUS.DTO
{
    /// <summary>
    /// This interface must be implemented by all DTO classes to identify them by convention.
    /// </summary>
    public interface IDTO
    {

    }
}
