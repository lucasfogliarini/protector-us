﻿using System;

namespace ProtectorUS.DTO
{
    /// <summary>
    /// This interface must be implemented by all DTO classes to identify them by convention.
    /// </summary>
    /// <summary>
    /// Defines common properties for entity based DTOs.
    /// </summary>
    /// <typeparam name="TPrimaryKey"></typeparam>
    public interface IAuditableEntityDTO<TPrimaryKey> : IDTO
    {
        /// <summary>
        /// Id of the entity.
        /// </summary>
        TPrimaryKey Id { get; set; }
        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
    }
}

