﻿using System.Linq.Expressions;
using System;

namespace ProtectorUS.Model.Repositories
{
    public interface IBrokerageRepository : IRepository<Brokerage, long>
    {

    }
}
