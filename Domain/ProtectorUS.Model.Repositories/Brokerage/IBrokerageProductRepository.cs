﻿namespace ProtectorUS.Model.Repositories
{
    public interface IBrokerageProductRepository : IRepository<BrokerageProduct, long>
    {
    }
}
