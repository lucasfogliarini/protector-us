﻿namespace ProtectorUS.Model.Repositories
{
    public interface IBrokerageGroupRepository : IRepository<BrokerageGroup, long>
    {
    }
}
