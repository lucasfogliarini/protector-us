﻿namespace ProtectorUS.Model.Repositories
{
    public interface IWebpageProductRepository : IRepository<WebpageProduct, long>
    {
    }
}
