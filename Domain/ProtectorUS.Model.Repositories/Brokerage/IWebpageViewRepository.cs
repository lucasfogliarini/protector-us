﻿namespace ProtectorUS.Model.Repositories
{
    public interface IWebpageViewRepository : IRepository<WebpageView, long>
    {
    }
}
