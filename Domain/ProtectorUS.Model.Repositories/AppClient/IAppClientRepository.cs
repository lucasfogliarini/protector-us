﻿namespace ProtectorUS.Model.Repositories
{
    public interface IAppClientRepository : IRepository<AppClient, long>
    {
    }
}
