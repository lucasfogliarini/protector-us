﻿namespace ProtectorUS.Model.Repositories
{
    public interface IPaymentMethodRepository : IRepository<PaymentMethod, long>
    {
    }
}
