﻿namespace ProtectorUS.Model.Repositories
{
    public interface IRefreshTokenRepository : IRepository<RefreshToken, long>
    {
    }
}
