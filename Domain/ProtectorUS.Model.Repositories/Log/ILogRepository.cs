﻿namespace ProtectorUS.Model.Repositories
{
    public interface ILogRepository : IRepository<Log, long>
    {
    }
}
