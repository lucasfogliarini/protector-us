﻿namespace ProtectorUS.Model.Repositories
{
    public interface IDeclineReasonRepository : IRepository<DeclineReason, long>
    {
    }
}
