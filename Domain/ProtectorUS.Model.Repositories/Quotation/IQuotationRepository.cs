﻿using System;

namespace ProtectorUS.Model.Repositories
{
    public interface IQuotationRepository : IRepository<Quotation, long>
    {
        Quotation Find(Guid hash);
    }
}
