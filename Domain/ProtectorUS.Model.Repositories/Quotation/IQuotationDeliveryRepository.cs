﻿namespace ProtectorUS.Model.Repositories
{
    public interface IQuotationDeliveryRepository : IRepository<QuotationDelivery, long>
    {
    }
}
