﻿namespace ProtectorUS.Model.Repositories
{
    public interface IClaimDocumentRepository : IRepository<ClaimDocument, long>
    {
    }
}
