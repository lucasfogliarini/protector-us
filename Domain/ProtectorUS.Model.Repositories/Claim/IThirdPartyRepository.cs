﻿namespace ProtectorUS.Model.Repositories
{
    public interface IThirdPartyRepository : IRepository<ThirdParty, long>
    {
    }
}
