﻿namespace ProtectorUS.Model.Repositories
{
    public interface IClaimRepository : IRepository<Claim, long>
    {
        int? GetLastSequentialNumber();
    }
}
