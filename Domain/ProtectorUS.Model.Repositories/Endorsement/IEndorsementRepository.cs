﻿namespace ProtectorUS.Model.Repositories
{
    public interface IEndorsementRepository : IRepository<Endorsement, long>
    {
    }
}
