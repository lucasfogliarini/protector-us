﻿namespace ProtectorUS.Model.Repositories
{
    public interface ICancelReasonRepository : IRepository<CancelReason, long>
    {
    }
}
