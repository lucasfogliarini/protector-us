﻿namespace ProtectorUS.Model.Repositories
{
    public interface ICompanyRepository : IRepository<Company, long>
    {
    }
}
