﻿namespace ProtectorUS.Model.Repositories
{
    public interface IManagerUserRepository : IUserRepository<ManagerUser>
    {
    }
}
