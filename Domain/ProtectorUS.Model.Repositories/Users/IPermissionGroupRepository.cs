﻿namespace ProtectorUS.Model.Repositories
{
    public interface IPermissionGroupRepository : IRepository<PermissionGroup, long>
    {
    }
}
