﻿namespace ProtectorUS.Model.Repositories
{
    public interface IProfileRepository : IRepository<Profile, long>
    {
    }
}
