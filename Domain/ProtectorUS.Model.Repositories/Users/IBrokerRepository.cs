﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Repositories
{
    public interface IBrokerRepository : IUserRepository<Broker>
    {
        Broker GetMasterByBrokerageProduct(long webpageProductId);
        Broker GetMasterByBrokerage(long brokerageId);
        IEnumerable<Broker> AllDeleted();
        Task<IEnumerable<Broker>> AllDeletedAsync();
        int Inactivate(IEnumerable<long> active);
        int Reactivate(IEnumerable<long> active);
    }
}
