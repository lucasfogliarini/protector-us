﻿namespace ProtectorUS.Model.Repositories
{
    public interface IBusinessRepository : IRepository<Business, long>
    {
    }
}
