﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Repositories
{
    public interface IUserRepository<TUser> : IRepository<TUser, long>
    {
        IEnumerable<Permission> GetPermissions(long userId);
        bool IsUsed(string email, string exceptEmail = null);
    }
    public interface IUserRepository : IUserRepository<User>
    {
    }
}
