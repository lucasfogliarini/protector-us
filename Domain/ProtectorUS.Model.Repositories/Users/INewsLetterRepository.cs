﻿namespace ProtectorUS.Model.Repositories
{
    public interface INewsLetterRepository : IRepository<NewsLetter, long>
    {
    }
}
