﻿namespace ProtectorUS.Model.Repositories
{
    public interface IInsuredRepository : IUserRepository<Insured>
    {
    }
}
