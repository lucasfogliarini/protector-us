﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Repositories
{
    /// <summary>
    /// Defines basic behavior for repositories.
    /// </summary>
    /// <typeparam name="E">entity type.</typeparam>
    /// <typeparam name="K">entity's unique identifier type.</typeparam>
    public interface IRepository<E, K> : IRepositoryList<E>
    {
        E Get(K id, params Expression<Func<E, object>>[] includeExpressions);
        /// <summary>
        /// Try to get the Entity, if null throw <see cref="NotFoundException"/>
        /// </summary>
        E TryGet(K id, params Expression<Func<E, object>>[] includeExpressions);
        E TryGetAsNoTracking(K id, params Expression<Func<E, object>>[] includeExpressions);
        E Find(Func<E, bool> match, params Expression<Func<E, object>>[] includeExpressions);
        /// <summary>
        /// Try to find the Entity, if null throw <see cref="NotFoundException"/>
        /// </summary>
        E TryFind(Func<E, bool> match, params Expression<Func<E, object>>[] includeExpressions);
        E Add(E entity);
        void Update(E entity);
        void Remove(E entity);
        Task<E> GetAsync(K id, params Expression<Func<E, object>>[] includeExpressions);
        /// <summary>
        /// Try to get the Entity asynchronously, if null throw <see cref="NotFoundException"/>
        /// </summary>
        Task<E> TryGetAsync(K id, params Expression<Func<E, object>>[] includeExpressions);
        Task<E> FindAsync(Expression<Func<E, bool>> match, params Expression<Func<E, object>>[] includeExpressions);
        /// <summary>
        /// Try to find the Entity asynchronously, if null throw <see cref="NotFoundException"/>
        /// </summary>
        Task<E> TryFindAsync(Expression<Func<E, bool>> match, params Expression<Func<E, object>>[] includeExpressions);
    }
}