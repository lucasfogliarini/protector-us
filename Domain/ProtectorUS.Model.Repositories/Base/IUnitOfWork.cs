﻿using ProtectorUS.Diagnostics.Logging;
using System.Threading.Tasks;
namespace ProtectorUS.Model.Repositories
{
    /// <summary>
    /// Defines the behavior for controls a set of database context commands.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Commit all context changes to database.
        /// </summary>
        void Commit();

        /// <summary>
        /// Commit async all context changes to database.
        /// </summary>
        Task CommitAsync();

        ILogger Logger { get; }
    }
}
