﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Repositories
{
    /// <summary>
    /// Defines basic behavior for repositories with listing all elements.
    /// </summary>
    /// <typeparam name="TEntity">entity type.</typeparam>
    public interface IRepositoryList<E>
    {

        IEnumerable<E> GetAll(params Expression<Func<E, object>>[] includeExpressions);
        IEnumerable<E> FindAll(Func<E, bool> match, params Expression<Func<E, object>>[] includeExpressions);
        IEnumerable<E> FindAll(Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions);
        IEnumerable<E> FindAll(string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions);
        PagedResult<E> GetAllByPage(int page, int pageSize, string sort, params Expression<Func<E, object>>[] includeExpressions);
        Task<IEnumerable<E>> GetAllAsync(params Expression<Func<E, object>>[] includeExpressions);
        Task<IEnumerable<E>> FindAllAsync(Expression<Func<E, bool>> match, params Expression<Func<E, object>>[] includeExpressions);
        PagedResult<E> FindByPage(int page, int pageSize, string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions);
        Task<PagedResult<E>> FindByPageAsync(int page, int pageSize, string sort, Filters<E> filters, params Expression<Func<E, object>>[] includeExpressions);
    }
}