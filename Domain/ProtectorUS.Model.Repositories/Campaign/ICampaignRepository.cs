﻿namespace ProtectorUS.Model.Repositories
{
    public interface ICampaignRepository : IRepository<Campaign, long>
    {
    }
}
