﻿namespace ProtectorUS.Model.Repositories
{
    public interface INotificationRepository : IRepository<Notification, long>
    {
    }
}
