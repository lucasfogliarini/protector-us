﻿namespace ProtectorUS.Model.Repositories
{
    public interface IPolicyRepository : IRepository<Policy, long>
    {
        int? GetLastSequentialNumber(long productId);
        int GetLastRenewal(int sequentialNumber);
    }
}
