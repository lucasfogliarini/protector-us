﻿namespace ProtectorUS.Model.Repositories
{
    public interface IOrderRepository : IRepository<Order, long>
    {
    }
}
