﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Repositories
{
    public interface ICountySurchargeRepository : IRepository<CountySurcharge, long>
    {
        SurchargePercentage GetPercentage(long cityId);
    }
}
