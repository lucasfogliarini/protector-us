﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Repositories
{
    public interface IStateSurchargeRepository : IRepository<StateSurcharge, long>
    {
        SurchargePercentage GetPercentage(long stateId);
    }
}
