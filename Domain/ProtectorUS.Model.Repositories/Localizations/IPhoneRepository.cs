﻿namespace ProtectorUS.Model.Repositories
{
    public interface IPhoneRepository : IRepository<Phone, long>
    {
    }
}
