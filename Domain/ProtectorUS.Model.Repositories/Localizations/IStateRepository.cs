﻿namespace ProtectorUS.Model.Repositories
{
    public interface IStateRepository : IRepository<State, long>
    {
    }
}
