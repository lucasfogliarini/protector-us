﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Repositories
{
    public interface IAddressRepository : IRepository<Address, long>
    {
        IEnumerable<Address> AllDeleted();
        Task<IEnumerable<Address>> AllDeletedAsync();
    }
}
