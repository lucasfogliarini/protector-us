﻿namespace ProtectorUS.Model.Repositories
{
    public interface ICityRepository : IRepository<City, long>
    {
    }
}
