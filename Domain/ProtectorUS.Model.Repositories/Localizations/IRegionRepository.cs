﻿namespace ProtectorUS.Model.Repositories
{
    public interface IRegionRepository : IRepository<Region, long>
    {
    }
}
