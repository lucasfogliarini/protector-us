﻿namespace ProtectorUS.Model.Repositories
{
    public interface ICountryRepository : IRepository<Country, long>
    {
    }
}
