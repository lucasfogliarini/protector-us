﻿namespace ProtectorUS.Model.Repositories
{
    public interface IProductConditionRepository : IRepository<ProductCondition, long>
    {
    }
}
