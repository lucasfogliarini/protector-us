﻿namespace ProtectorUS.Model.Repositories
{
    public interface IProductPaymentMethodRepository : IRepository<ProductPaymentMethod, long>
    {
    }
}
