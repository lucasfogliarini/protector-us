﻿namespace ProtectorUS.Model.Repositories
{
    public interface IProductRepository : IRepository<Product, long>
    {
    }
}
