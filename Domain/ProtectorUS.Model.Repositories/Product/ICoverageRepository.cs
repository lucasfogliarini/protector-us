﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Repositories
{
    public interface ICoverageRepository : IRepository<Coverage, long>
    {
        IEnumerable<Coverage> FindAll(long productId);
    }
}
