﻿namespace ProtectorUS.Model.Repositories
{
    public interface IEmailTemplateRepository : IRepository<EmailTemplate, long>
    {
    }
}
