﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ProtectorUS.Model.DocumentDB.Repositories
{
    public interface IDocumentDBRepository<T>
    {
        T Find(Expression<Func<T, bool>> predicate);
        IEnumerable<T> FindAll(Expression<Func<T, bool>> predicate);
        int Count(Expression<Func<T, bool>> predicate);
        void Upsert(T item);
    }
}
