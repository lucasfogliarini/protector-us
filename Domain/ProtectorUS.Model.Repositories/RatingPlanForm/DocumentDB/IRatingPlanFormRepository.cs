﻿namespace ProtectorUS.Model.DocumentDB.Repositories
{
    public interface IRatingPlanFormRepository : IDocumentDBRepository<RatingPlan.RatingPlanForm>
    {
        RatingPlan.RatingPlanForm Find(long formid);
    }
}
