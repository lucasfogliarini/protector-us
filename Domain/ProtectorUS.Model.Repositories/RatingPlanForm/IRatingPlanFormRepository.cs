﻿namespace ProtectorUS.Model.Repositories
{
    public interface IRatingPlanFormVersionRepository : IRepository<RatingPlanFormVersion, long>
    {
        RatingPlanFormVersion GetLatestVersion(string productAlias, long stateId);
    }
}
