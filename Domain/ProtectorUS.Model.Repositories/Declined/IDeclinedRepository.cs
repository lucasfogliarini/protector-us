﻿namespace ProtectorUS.Model.Repositories
{
    public interface IDeclinedRepository : IRepository<Declined, long>
    {
    }
}
