﻿namespace ProtectorUS.Model.Repositories
{
    public interface IDeclinedBlockRepository : IRepository<DeclinedBlock, long>
    {
    }
}
