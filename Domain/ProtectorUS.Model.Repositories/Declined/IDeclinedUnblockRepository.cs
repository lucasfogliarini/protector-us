﻿namespace ProtectorUS.Model.Repositories
{
    public interface IDeclinedUnblockRepository : IRepository<DeclinedUnblock, long>
    {
    }
}
