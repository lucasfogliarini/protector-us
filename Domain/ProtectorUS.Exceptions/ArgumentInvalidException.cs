﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Exception thrown when a one of the arguments provided to a method is not valid.
    /// </summary>
    public class ArgumentInvalidException : BaseException
    {
        public ArgumentInvalidException(Expression<Func<MessageCode.MessageCodes, string>> code, params object[] args) : base(code, args)
        {
        }
        public ArgumentInvalidException(Expression<Func<MessageCode.MessageCodes, string>> code, object[] args, string[] messages) : base(code, args, messages)
        {
        }
    }
}
