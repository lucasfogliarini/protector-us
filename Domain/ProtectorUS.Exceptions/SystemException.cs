﻿using System;

namespace ProtectorUS.Exceptions
{
    public class SystemException : BaseException
    {
        public SystemException(Exception ex)
            : base(c => c.EL014, ex.Message, ex.InnerException?.Message)
        {
        }
    }
}
