﻿namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Exception thrown when the user is unauthorized.
    /// </summary>
    public class AuthorizationException : BaseException
    {
        public AuthorizationException() : base(m=>m.EL002)
        {
        }
    }
}
