﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Exception thrown when a required parameter is not provided.
    /// </summary>
    public class ArgumentRequiredException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ArgumentRequiredException"/> class.
        /// </summary>
        public ArgumentRequiredException(Expression<Func<MessageCode.MessageCodes, string>> code, params string[] args) : base(code, args)
        {
        }
    }
}
