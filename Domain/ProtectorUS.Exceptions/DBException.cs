﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    public abstract class DbException : BaseException
    {
        public DbException(Expression<Func<MessageCode.MessageCodes, string>> code, params string[] args) : base(code, args)
        {
        }
    }
}
