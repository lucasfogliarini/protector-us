﻿using System;

namespace ProtectorUS.Exceptions
{
    public class SeedException : BaseException
    {
        public SeedException(params string[] messages)
            : base(c => c.EL056, messages)
        {
        }
    }
}
