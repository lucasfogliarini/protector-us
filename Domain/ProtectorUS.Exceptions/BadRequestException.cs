﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    public class BadRequestException : BaseException
    {
        public BadRequestException(Expression<Func<MessageCode.MessageCodes, string>> code, params string[] messages) : base(code, messages)
        {
        }
    }
}
