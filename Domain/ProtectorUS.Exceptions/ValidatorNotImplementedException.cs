﻿namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Exception thrown when a entity not have a validator.
    /// </summary>
    public class ValidatorNotImplementedException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ValidatorNotImplementedException"/> class.
        /// </summary>
        /// <param name="name">the parameter name.</param>
        public ValidatorNotImplementedException(string name)
            : base(c=>c.EL013, name)
        {
        }
    }
}
