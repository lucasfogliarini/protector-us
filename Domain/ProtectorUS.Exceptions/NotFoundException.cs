﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    public class NotFoundException : BaseException
    {
        public NotFoundException(Expression<Func<MessageCode.MessageCodes, string>> code, params string[] args) :  base(code, args)
        {
        }
    }
}
