﻿namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Exception thrown whena user not accepted the terms.
    /// </summary>
    public class TermsNotAcceptableException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of <see cref="TermsNotAcceptableException"/> class.
        /// </summary>
        /// <param name="name">the parameter name.</param>
        public TermsNotAcceptableException(long id)
            : base(c=>c.EL057, id)
        {
        }
    }
}
