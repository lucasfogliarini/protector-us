﻿using System;

namespace ProtectorUS.Exceptions
{
    public class PaymentException : BaseException
    {
        public PaymentException(string innerMessage)
            : base(c => c.EL021, null, new[] { innerMessage })
        {
        }
    }
}
