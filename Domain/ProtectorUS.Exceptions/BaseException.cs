﻿using ProtectorUS.Communication;
using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    /// <summary>
    /// Application's base exception.
    /// </summary>
    public abstract class BaseException : Exception
    {
        /// <summary>
        /// Initializes a new instance of <see cref="BaseException"/>.
        /// </summary>
        /// <param name="message">the exception message.</param>
        protected BaseException(Expression<Func<MessageCode.MessageCodes, string>> code, params object[] messageArgs)
        {
            MessageCode = new MessageCode(code, messageArgs);
            Data.Add("code", Code);
        }
        protected BaseException(Expression<Func<MessageCode.MessageCodes, string>> code, object[] messageArgs, string[] innerMessages)
        {
            MessageCode = new MessageCode(code, messageArgs, innerMessages);
            Data.Add("code", Code);
        }
        public MessageCode MessageCode { get; private set; }
        public override string Message
        {
            get
            {
                return MessageCode.Message;
            }
        }
        public string Code { get { return MessageCode.Code; } }
        public bool Log { get { return MessageCode.MustLog(); } }
        /// <summary>
        /// Initializes a new instance of <see cref="BaseException"/>.
        /// </summary>
        /// <param name="message">the exception message.</param>
        protected BaseException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
