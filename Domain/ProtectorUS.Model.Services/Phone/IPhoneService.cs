﻿namespace ProtectorUS.Model.Services
{
    public interface IPhoneService : IBaseService
    {
        bool CheckPhone(string phone);
    }
}
