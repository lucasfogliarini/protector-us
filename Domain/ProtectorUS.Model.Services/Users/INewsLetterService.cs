﻿namespace ProtectorUS.Model.Services
{
    using DTO;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface INewsLetterService : IBaseService<NewsLetter>
    {
        OutputNewsLetterDTO Create(InputNewsLetterDTO quotation);
        Task<OutputNewsLetterDTO> CreateAsync(InputNewsLetterDTO newsletterIn);
    }
}
