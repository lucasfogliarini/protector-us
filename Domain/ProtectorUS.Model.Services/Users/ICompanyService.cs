﻿using ProtectorUS.DTO;
using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    public interface ICompanyService : IBaseService<Company>
    {
        IEnumerable<CompanyDTO> GetAll();
    }
}
