﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using ProtectorUS.DTO;
    using System;
    using System.Threading.Tasks;

    public interface IBrokerService : IUserService<Broker>
    {
        OutputBrokerOnboardDTO Onboarding(long brokerId);
        OutputProfileBrokerDTO Update(long brokerId, BrokerSelfUpdateDTO brokerIn);
        OutputProfileBrokerDTO Get(long brokerId);
        OutputBrokerDTO Delete(long brokerId);
        PagedResult<OutputBrokerListDTO> FindAll(int page, int pageSize, string sort, Filters<Broker> filters = null);
        GraphicDTO GetTotalTypes(Filters<Broker> filters);
        GraphicDTO GetTotalLastMonth(Filters<Broker> filters);
        List<GraphicDTO> GetTotalLastYear(Filters<Broker> filters);
        bool IsMember(long brokerId, long brokerageId);
        Task<OutputProfileBrokerDTO> UpdateAsync(long brokerId, BrokerSelfUpdateDTO brokerIn);
        Task<PagedResult<OutputBrokerListDTO>> FindAllAsync(int page, int pageSize, string sort, Filters<Broker> filters = null);
        Task<OutputProfileBrokerDTO> UpdateAsync(long id, BrokerUpdateDTO brokerIn);
        Task<OutputBrokerDTO> CreateAsync(long brokerageId, BrokerInsertDTO broker);
        Task<OutputProfileBrokerDTO> GetAsync(long brokerId);
        Task<OutputBrokerDTO> DeleteAsync(long id);
        Task<OutputBrokerInactivateReactivate> InactivateReactivate();
    }
}
