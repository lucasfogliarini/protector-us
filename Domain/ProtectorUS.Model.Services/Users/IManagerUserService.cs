﻿using ProtectorUS.DTO;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Services
{
    public interface IManagerUserService : IUserService<ManagerUser>
    {
        OutputManagerUserDTO Create(ManagerUserInsertDTO managerUserIn);
        OutputManagerUserProfileDTO Get(long managerUserId);
        OutputManagerUserDTO Update(long managerUserId, ManagerUserUpdateDTO managerUserIn);
        OutputManagerUserDTO Delete(long managerUserId);
        OutputManagerUserDTO Onboarding(long managerUserId);
        PagedResult<OutputManagerUserItemDTO> FindAll(int page, int pageSize, string sort, Filters<ManagerUser> filters);
        OutputManagerUserDTO Update(long managerUserId, ManagerUserSelfUpdateDTO managerUserIn);
        Task<OutputManagerUserDTO> DeleteAsync(long id);
    }
}
