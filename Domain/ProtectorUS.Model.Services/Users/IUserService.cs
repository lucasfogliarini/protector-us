﻿using ProtectorUS.Communication;
using ProtectorUS.DTO;
using System;
using System.Linq.Expressions;


namespace ProtectorUS.Model.Services
{
    public interface IUserService<T> : IBaseService<T>
    {
        TUserOut FindUser<TUserOut>(string email, params Expression<Func<T, object>>[] includeExpressions) where TUserOut : IOutputUserDTO;
        bool IsUsed(string email, string exceptEmail = null);
        bool IsUsed(string email, long userId);
        MessageCode ResetPasswordSendInstructions(string email);
        MessageCode ResetPassword(long userId, string token, string newPassword = null);
        MessageCode ChangePassword(long id, string password);
        string ChangePicture(long id, string fileName);
        Profile TryGetProfile(long profileId);
        string[] GetPermissions(long userId, bool allowed = true);
    }

    public interface IUserService : IUserService<User>
    {
    }
}
