﻿using ProtectorUS.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Services
{
    public interface IInsuredService : IUserService<Insured>
    {
        OutputInsuredDTO Update(long insuredId, InputInsuredProfileDTO insuredIn);
        OutputInsuredDTO Update(long insuredId, InsuredSelfUpdateDTO insuredIn);
        void Update(long brokerId, UpdateInsuredByBrokerDTO insuredIn);
        MyProtectorMobileOutputInsuredDTO GetProfilePoliciesClaimsCoverages(long insuredId);
        MyProtectorOutputInsuredDTO GetProfilePolicies(long insuredId);
        OutputInsuredDTO Get(long insuredId);
        PagedResult<OutputInsuredDTO> GetAll(int page, int pageSize, string sort);
        MapPointDTO GetMapPoints(Filters<Insured> filters = null);
        GraphicDTO CountByStatus(long? brokerageId = null);
        List<GraphicDTO> GetTotalLastYearByType(Filters<Insured> filter);
        List<ItemGraphicDTO> GetTotalLastYear(Filters<Insured> filter);
        OutputInsuredDTO Onboarding(long insuredId);
        PagedResult<OutputInsuredListDTO> FindAll(int page, int pageSize, string sort, Filters<Insured> filters);
        Task<MyProtectorOutputInsuredDTO> GetProfilePoliciesAsync(long insuredId);
        Task<OutputInsuredDTO> GetAsync(long id);
        Task<OutputInsuredDTO> UpdateAsync(long insuredId, InputInsuredProfileDTO insuredIn);
        OutputInsuredDTO Update(long insuredId, InputInsuredPictureProfileDTO insuredIn);
        bool CheckInsuredExists(string email, string productAlias = null);
        IEnumerable<UserNameDTO> FindAll(string name, long? brokerageId = null);
        bool IsOwnCustomer(long brokerageId, long insuredId);
        bool CheckTerms(long insuredId);
        bool AcceptTerms(long insuredId, bool accept);
    }
}
