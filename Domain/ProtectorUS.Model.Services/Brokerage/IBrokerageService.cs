﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Threading.Tasks;
    public interface IBrokerageService : IBaseService<Brokerage>
    {
        OutputBrokerageDTO Find(string alias, bool includeProducts = false);
        Task<bool> TestAliasAsync(string alias, long brokerageId);
        bool TestAlias(string alias, long brokerageId);
        Task<OutputBrokerageMyProtectorDTO> FindMyProtectorAsync(string alias);
        OutputBrokerageMyProtectorDTO FindMyProtector(string alias);
        OutputBrokerageDTO Delete(long brokerageId);
        OutputBrokerageDTO Get(long brokerageId);
        OutputBrokerageDTO GetWithOnlyArgoActiveProducts(long brokerageId);
        bool CheckChargeAccountStatus(long brokerageId);
        OutputBrokerageDTO Create(InputBrokerageDTO brokerageIn);
        OutputBrokerageDTO Update(long brokerageId, InputBrokerageUpdateDTO brokerageIn);
        OutputBrokerageDTO Update(long brokerageId, InputBrokerageDTO brokerageIn);
        PagedResult<OutputBrokerageItemDTO> FindAll(int page, int pageSize, string sort, Filters<Brokerage> filters = null);
        MapBrokerageMyProtectorDTO FindABroker(string zipCode = "", string productAlias = "");
        MapPointDTO GetMapPoints(string zipCode = "", string aliasProduct = "");
        BrokerageDefaultWebPageDTO UpdateDefaultWebPage(long brokerageId, BrokerageDefaultWebPageDTO webpageIn);
        IEnumerable<WebpageProductDTO> FindWebpages(long brokerageId);
        OutputBrokerageOnboardDTO OnBoarding(long brokerageId);
        IReadOnlyCollection<OutputBrokerageTopListDTO> GetTop(int quant);
        GraphicDTO GetTotalStatus();
        IReadOnlyCollection<GraphicDTO> GetTotalLastYear();
        void ToggleProduct(long brokerageId, ARGOToggleProductDTO productToggle);
        void ToggleProduct(long brokerageId, BrokerageToggleProductDTO productToggle);
        void CreateChargeAccount(long brokerageId, InputLegalEntityDTO legalEntityIn, BankAccount bankAccount, byte[] tosIp);
        void UpdateChargeAccount(long brokerageId, InputLegalEntityDTO legalEntityIn, BankAccount bankAccount, byte[] tosIp);
        ChargeAccount GetChargeAccount(long brokerageId);
        void UpdateChargeAccountStatus(ChargeAccount chargeAccount);
        BrokerageProductEnableDisableOutputDTO EnableDisableProduct(long brokerageId, BrokerageProductEnableDisableInputDTO brokerageProductIn);
        BrokerageActiveInactiveBrokeragesAndAgentstDTO GetActiveInactiveBrokeragesAndAgents();

        #region Async
        Task<OutputBrokerageDTO> FindAsync(string alias, bool includeProducts = false);
        Task<PagedResult<OutputBrokerageItemDTO>> FindAllAsync(int page, int pageSize, string sort, Filters<Brokerage> filters);
        Task<OutputBrokerageDTO> UpdateAsync(long brokerageId, InputBrokerageUpdateDTO brokerageIn);
        Task<OutputBrokerageDTO> GetAsync(long brokerageId);
        Task<OutputBrokerageDTO> DeleteAsync(long brokerageId);
        Task<IEnumerable<WebpageProductDTO>> FindWebpagesAsync(long brokerageId);
        Task<MapPointDTO> GetMapPointsAsync(string zipCode = "", string aliasProduct = "");
        Task<IReadOnlyCollection<OutputBrokerageTopListDTO>> GetTopAsync(int quant);
        Task<GraphicDTO> GetTotalStatusAsync();
        Task<IReadOnlyCollection<GraphicDTO>> GetTotalLastYearAsync();
        Task<BrokerageDefaultWebPageDTO> UpdateDefaultWebPageAsync(long brokerageId, BrokerageDefaultWebPageDTO webpageIn);
        
        #endregion
    }
}