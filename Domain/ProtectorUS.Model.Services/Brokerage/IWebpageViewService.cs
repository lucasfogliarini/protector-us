﻿using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using DTO;
    using System;
    using System.Threading.Tasks;

    public interface IWebpageViewService : IBaseService<WebpageView>
    {
        OutputWebpageViewDTO Create(InputWebpageViewDTO webviewIn);
        IReadOnlyCollection<GraphicDTO> GetTotalInterval(Filters<WebpageView> filters, PeriodFilter interval);
        IReadOnlyCollection<GraphicDTO> GetTotalWeek(Filters<WebpageView> filters, PeriodFilter interval);
        IReadOnlyCollection<GraphicDTO> GetTotalWebBanner(Filters<WebpageView> filters, PeriodFilter interval);
        Task<OutputWebpageViewDTO> CreateAsync(InputWebpageViewDTO view);

    }
}