namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface ICancelReasonService : IBaseService<CancelReason>
    {
        OutputCancelReasonDTO Create(InputCancelReasonDTO cancelReasonIn);
        void Delete(long cancelReasonId);
        OutputCancelReasonDTO Find(string updateBy);
        OutputCancelReasonDTO Get(long cancelReasonId);
        IReadOnlyCollection<OutputCancelReasonDTO> GetAll();
        OutputCancelReasonDTO Update(long id, InputCancelReasonDTO cancelReasonIn);
        Task<OutputCancelReasonDTO> UpdateAsync(long id, InputCancelReasonDTO cancelReasonIn);
        Task<OutputCancelReasonDTO> FindAsync(string updateBy);
        Task<IReadOnlyCollection<OutputCancelReasonDTO>> GetAllAsync();
        Task<OutputCancelReasonDTO> CreateAsync(InputCancelReasonDTO cancelReasonIn);
        Task<OutputCancelReasonDTO> GetAsync(long id);
    }
}
