﻿namespace ProtectorUS.Model.Services
{
    using System;
    using System.Linq.Expressions;
    public interface IBaseService<T> : IBaseService
    {
        TOut TryGet<TOut>(long id, params Expression<Func<T, object>>[] includeExpressions);
        TOut Get<TOut>(long id, params Expression<Func<T, object>>[] includeExpressions);
    }

    public interface IBaseService
    {
        void InitializeLogger(string userName);
    }
}
