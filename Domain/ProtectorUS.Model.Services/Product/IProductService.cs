﻿namespace ProtectorUS.Model.Services
{
    using ProtectorUS.DTO;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IProductService : IBaseService<Product>
    {
        OutputProductDTO Toggle(long productId, bool toggle);
        OutputProductBaseDTO Find(string alias);
        IEnumerable<OutputProductDTO> GetAll();
        IEnumerable<OutputProductConditionDTO> GetConditions(long productId, long stateId);
        IEnumerable<OutputProductConditionDTO> GetConditions(string alias, string zipcode);
        OutputProductConditionDTO AttachCondition(long productId, long stateId, string conditionFilePath);
        Task<IEnumerable<OutputProductConditionDTO>> GetConditionsAsync(long id, long stateId);
        Task<IEnumerable<OutputProductConditionDTO>> GetConditionsAsync(string alias, string zipcode);
        Task<IEnumerable<OutputProductDTO>> GetAllAsync();
        Task<OutputProductBaseDTO> FindAsync(string alias);
    }
}
