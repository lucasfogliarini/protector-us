namespace ProtectorUS.Model.Services
{
    using DTO;
    using System;
    public interface IEndorsementService : IBaseService<Endorsement>
    {
        OutputEndorsementDTO Create(InputEndorsementDTO endorsementIn);
        OutputEndorsementDTO Get(long endorsementId);
        OutputEndorsementDTO Find(Func<Endorsement, bool> filter);
        PagedResult<OutputEndorsementDTO> FindAll(int page, int pageSize, string sort, Filters<Endorsement> filters);
        OutputEndorsementDTO Reply(long endorsementId, bool approve);
    }
}
