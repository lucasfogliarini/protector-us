﻿namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Threading.Tasks;
    public interface IAppClientService : IBaseService<AppClient>
    {
        OutputAppClientDTO TryFind(string clientId);
        OutputAppClientDTO Update(string clientId, string token, InputAppClientDTO appClientIn);
        Task<OutputAppClientDTO> UpdateAsync(string clientId, string token, InputAppClientDTO appClientIn);
    }
}