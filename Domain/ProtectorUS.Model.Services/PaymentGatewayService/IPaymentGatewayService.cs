﻿namespace ProtectorUS.Model.Services
{
    public interface IPaymentGatewayService : IBaseService
    {
        void UpdateChargeAccountStatus(string paymentGatewayBody);
    }
}