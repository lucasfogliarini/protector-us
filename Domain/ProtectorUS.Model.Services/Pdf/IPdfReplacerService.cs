﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Services
{
    public interface IPdfReplacerService
    {
        void ReplacePdfForm(string currentPath, string newPath, Dictionary<string, string> keyValues);

        void ReplacePdfForm(string currentPath, string newPath, string QrCode, byte[] logoBrokerage);
    }
}
