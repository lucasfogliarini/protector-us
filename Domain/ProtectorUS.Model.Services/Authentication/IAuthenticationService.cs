﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using DTO;
    public interface IAuthenticationService : IBaseService<User>
    {
        TUser Authenticate<TUser>(string login, string password) where TUser : User;
        IEnumerable<OutputPermissionDTO> FindPermissions(long userId);

        /// <summary>
        /// Try to find AppClient.
        /// Exceptions:
        /// - InactiveException
        /// - NotFoundException
        /// </summary>
        OutputAppClientDTO TryFindAppClient(string appClientId);
        RefreshToken RefreshToken(RefreshToken refreshingToken, DateTime newIssuedDate, DateTime newExpiresDate, string newProtectedTicket);
        RefreshToken CreateToken(string appClientId, string identityName, DateTime issuedDate, DateTime expiresDate, string protectorTicket);
        RefreshToken GetRefreshToken(string refreshTokenId);
        void AddDevice(User user, string deviceId);
        OutputBrokerDTO GetMasterByBrokerage(long brokerageId);
    }
}
