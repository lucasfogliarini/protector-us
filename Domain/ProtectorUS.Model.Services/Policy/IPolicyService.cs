﻿namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Collections.Generic;
    using System.IO;
    public interface IPolicyService : IBaseService<Policy>
    {
        MapPointDTO GetMapPoints(Filters<Policy> filters = null);
        List<GraphicDTO> GetTotalDashboardTypes(Filters<Policy> filter, PolicyPeriodFilter intervalDays, PolicyTypeView typeView);
        GraphicDTO GetTotalInterval(Filters<Policy> filter, PolicyIntervalFilter interval);
        GraphicDTO GetTotalGeneral(Filters<Policy> filter);
        GraphicDTO GetRenewalsTotal(Filters<Policy> filter);
        GraphicDTO CountInEffect(long? brokerageId = null);
        GraphicDTO GetTotalLastYear(Filters<Policy> filter);
        PagedResult<OutputPolicyItemDTO> FindAll(int page, int pageSize, string sort, Filters<Policy> filters = null);
        OutputPolicyDTO RequestCancel(long policyId, long reasonId, long requesterId);
        OutputPolicyDTO Cancel(long policyId);
        IEnumerable<OutputCoverageDTO> GetCoverages(long policyId);
        bool IsOwner(long insuredId, long policyId);
        bool IsOwnBusiness(long brokerageId, long policyId);
        IEnumerable<PolicyNumberDTO> FindAll(long insuredId, long? brokerageId = null);
        MemoryStream GetPolicyFile(Policy policy, string token);
    }
}
