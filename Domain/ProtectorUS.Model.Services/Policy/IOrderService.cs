﻿namespace ProtectorUS.Model.Services
{
    using System.Threading.Tasks;
    using ProtectorUS.DTO;
    public interface IOrderService : IBaseService<Order>
    {
        OutputOrderDTO Create(InputOrderDTO order);
        PagedResult<OutputOrderDTO> FindAll(long policyId, int page, int pageSize, string sort);
        Task<PagedResult<OutputOrderDTO>> FindAllAsync(long policyId, int page, int pageSize, string sort);
        void GenerateCertificate(Order order);
    }
}
