﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Services
{
    using DTO;

    public interface IAcordService
    {

        bool GenerateAcordXML(Order order);

        bool RegenPdf(long PolicyId);
    }
}