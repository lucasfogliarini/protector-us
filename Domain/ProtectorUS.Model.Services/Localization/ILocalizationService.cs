﻿
using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Device.Location;
    public interface ILocalizationService : IBaseService<Address>
    {
        void DeleteAddress(long id);
        OutputRegionDTO GetRegion(string zipcode);
        Region GetRegion(long regionId);
        IEnumerable<OutputStateDTO> GetStates();
        GeoCoordinate GetCoordinates(string streetLine1, string zipCode);
    }
}
