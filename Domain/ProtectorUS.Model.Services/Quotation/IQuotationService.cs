﻿namespace ProtectorUS.Model.Services
{
    using DTO;
    using System;
    using System.Collections.Generic;

    public interface IQuotationService : IBaseService<Quotation>
    {
        QuotationResponseDTO Create(QuotationInsertDTO quotation);
        OutputQuotationDTO Get(Guid hash);
        IReadOnlyCollection<GraphicDTO> GetTotalStatus(Filters<Quotation> filters, PeriodFilter period);
        QuotationContactDTO Contact(QuotationContactDTO quotationIn);
        GraphicDTO GetDeclinesTotalByType(PeriodFilter period);
        GraphicDTO GetDeclinesTotalByType(Filters<Quotation> filters, PeriodFilter period);
    }
}
