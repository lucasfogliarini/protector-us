﻿namespace ProtectorUS.Model.Services
{
    public interface IEmailTemplateService
    {
        string GetEmailMarketingAccountantsBody(EmailMarketing emailMarketing, Product product, Brokerage brokerage);
        string GetQuotationBody(Quotation quotation);
        string GetContactBrokerBody(Quotation quotation, string phone = "");
        string GetActivationBody(Order order);
        string GetWelcomeInsuredBody(Order order);
        string GetWelcomeBrokerBody(Broker broker);
        string GetWelcomeManagerUserBody(ManagerUser managerUser);
        string GetWelcomeBrokerageBody(Broker broker);
        string GetFnolClaimInsuredBody(Claim claim);
        string GetFnolClaimBrokerBody(Claim claim);
        string GetFnolClaimArgoBody(Claim claim);
        string GetClaimStatusInsuredBody(Claim claim);
        string GetClaimStatusBrokerBody(Claim claim);
        string GetClaimStatusArgoBody(Claim claim);

        string GetForgotPasswordBody(User user, string token, ProtectorApp protectorApp);

        void Send(string templateId, string to, string body = null, string replyTo = null);
    }
}
