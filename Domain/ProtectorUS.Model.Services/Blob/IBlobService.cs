﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProtectorUS.Model.Services
{
    public interface IBlobService : IBaseService
    {
        MemoryStream DownloadStream(BlobType blobtype, string blobPath, bool directoryPrefix = false);
        string DownloadText(BlobType blobtype, string blobPath, bool directoryPrefix = false);
        Task<string> UploadAsync(BlobType blobtype, StreamContent streamContent, string fileName = null);
        /// <summary>
        /// Upload blob asynchronously and return blobpath (directoryName + fileName)
        /// </summary>
        string UploadAsync(BlobType blobtype, byte[] buffer, string fileName);
        /// <summary>
        /// Upload blob synchronously and return blobpath (directoryName + fileName)
        /// </summary>
        string Upload(BlobType blobtype, byte[] buffer, string fileName);
    }
}
