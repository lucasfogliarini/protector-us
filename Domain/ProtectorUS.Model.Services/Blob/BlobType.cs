﻿namespace ProtectorUS.Model.Services
{
    public enum BlobType
    {
        Image = 1,
        ClaimDocument = 2,
        PolicyCertificate = 3,
        ProductTerm = 4,
        EmailTemplate = 5,
        Brochure = 6,
        Policy =7,
    }
}
