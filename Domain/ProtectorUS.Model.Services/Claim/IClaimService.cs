using System;
using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using DTO;
    using System.Threading.Tasks;
    public interface IClaimService : IBaseService<Claim>
    {
        OutputClaimDTO Get(long id);
        OutputClaimDTO Delete(long claimId);
        OutputClaimDTO Create(long policyId, InputClaimDTO claimIn);
        OutputClaimDTO Update(long Id, InputClaimDTO claimIn);
        OutputClaimDTO AttachDocuments(long claimId, string[] documentsPath);
        OutputClaimDocumentDTO DeleteDocument(long claimId, long documentId);
        OutputClaimDTO ChangeStatus(long claimId, ClaimStatus status, long userId);
        MapPointDTO GetMapPoints(Filters<Claim> filter = null);
        IReadOnlyCollection<GraphicDTO> GetTotalInterval(ClaimPeriodFilter interval, Filters<Claim> filters = null);
        bool IsOwner(long insuredId, long claimId);
        bool IsOwnBusiness(long brokerageId, long claimId);
        Task<OutputClaimDTO> UpdateAsync(long id, InputClaimDTO claimIn);
        PagedResult<OutputClaimDTO> FindAll(int page, int pageSize, string sort, Filters<Claim> filters);
        Task<IEnumerable<OutputClaimDTO>> FindAllAsync(long policyId);
        IEnumerable<OutputClaimDTO> FindAll(long policyId);
        Task<OutputClaimDTO> GetAsync(long id);
        Task<MapPointDTO> GetMapPointsAsync(Filters<Claim> filter = null);
        Task<OutputClaimDTO> AttachDocumentsAsync(long id, string[] documentsPath);
        Task<OutputClaimDocumentDTO> DeleteDocumentAsync(long claimId, long documentId);
        OutputClaimDocumentDTO DeleteDocument(long id, string fileName);
    }
}
