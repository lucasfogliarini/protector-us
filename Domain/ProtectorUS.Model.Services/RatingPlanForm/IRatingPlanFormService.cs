﻿namespace ProtectorUS.Model.Services
{
    using DTO.RatingPlan;
    public interface IRatingPlanFormService : IBaseService<RatingPlanFormVersion>
    {
        OutputRatingPlanFormDTO GetLatestVersion(string productAlias, string zipCode);
        RatingPlanComputer GetRatingPlanComputer(string hashCode);
    }
}
