﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Model.Services
{
    using System.Threading.Tasks;
    using DTO;

    public interface ICampaignService : IBaseService<Campaign>
    {
        PagedResult<OutputCampaignItemDTO> GetAll(int page, int pageSize, string sort);
        List<GraphicDTO> GetTotalbyType(Filters<Campaign> filter);
        List<GraphicDTO> GetTotalbyTypeView(Filters<Campaign> filter);
        List<GraphicDTO> GetTotalbyTypePeriod(Filters<Campaign> filter, PeriodFilter period);
        PagedResult<OutputCampaignItemDTO> FindAll(int page, int pageSize, string sort, Filters<Campaign> filters);
    }
}
