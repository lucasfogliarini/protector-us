﻿namespace ProtectorUS.Model.Services
{
    using DTO;

    public interface IEmailMarketingService : IBaseService<Campaign>
    {
        void Create(EmailMarketingDTO emailMarketingIn, long brokerId, long brokerageId);
        OutputEmailMarketingDTO Get(long emailMarketingId);
    }
}
