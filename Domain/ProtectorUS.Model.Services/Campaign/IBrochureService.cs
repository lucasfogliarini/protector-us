﻿namespace ProtectorUS.Model.Services
{
    using DTO;
    public interface IBrochureService : IBaseService<Campaign>
    {
        void Create(InputBrochureDTO brochureIn, long brokerId, long brokerageId);
    }
}
