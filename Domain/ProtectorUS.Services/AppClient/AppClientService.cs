﻿using AutoMapper;

namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Model.Services;
    using DTO;
    using Exceptions;
    using System.Threading.Tasks;
    using System;

    public class AppClientService : BaseService<AppClient>, IAppClientService
    {
        public AppClientService(
           IAppClientRepository appClientRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                appClientRepository)
        {
        }

        public OutputAppClientDTO TryFind(string clientId)
        {
            return TryCatch(() =>
            {
                AppClient appClient = _mainRepository.TryFind(c => c.ClientId == clientId);
                ThrowIfInactive(appClient);
                return _mapper.Map<OutputAppClientDTO>(appClient);
            });
        }
        public OutputAppClientDTO Update(string clientId, string token, InputAppClientDTO appClientIn)
        {
            return RunSync(()=>UpdateAsync(clientId, token, appClientIn));
        }
        public async Task<OutputAppClientDTO> UpdateAsync(string clientId, string token, InputAppClientDTO appClientIn)
        {
            return await TryCatch(async() =>
            {
                AppClient appClient = _mainRepository.TryFind(c => c.ClientId == clientId);
                if (token != appClient.Token)
                {
                    throw new ArgumentInvalidException(e => e.EL043);
                }
                if (appClientIn.CurrentVersion != null)  appClient.CurrentVersion = appClientIn.CurrentVersion;
                if (appClientIn.ReviewVersion != null) appClient.ReviewVersion = appClientIn.ReviewVersion;
                if (appClientIn.InReview != null) appClient.InReview = appClientIn.InReview.GetValueOrDefault();
                if (appClientIn.Active != null) appClient.Active = appClientIn.Active.GetValueOrDefault();
                _validationManager.Validate(appClient);
                _mainRepository.Update(appClient);
                await CommitAsync();
                return _mapper.Map<OutputAppClientDTO>(appClient);
            });
        }
    }
}