﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.Threading.Tasks;

    public class EndorsementService : BaseService<Endorsement>, IEndorsementService
    {
        private readonly IPolicyService _policyService;

        public EndorsementService(
           IEndorsementRepository endorsementRepository,
           IPolicyService policyService,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                endorsementRepository,
                policyService as BaseService)
        {
            _mainRepository = endorsementRepository;
            _policyService = policyService;
        }

        public OutputEndorsementDTO Create(InputEndorsementDTO endorsementIn)
        {
            return TryCatch(() =>
            {
                var endorsement = _mapper.Map<Endorsement>(endorsementIn);

                _validationManager.Validate(endorsement);
                _mainRepository.Add(endorsement);

                Commit();
                return _mapper.Map<OutputEndorsementDTO>(endorsement);
            });
        }
        public OutputEndorsementDTO Get(long endorsementId)
        {
            return TryCatch(() =>
            {
                var endorsement = _mainRepository.Find(x =>
                       x.Id == endorsementId,
                    e => e.Policy,
                    e => e.Policy.Broker,
                    e => e.Policy.Broker.Brokerage);


                return _mapper.Map<OutputEndorsementDTO>(endorsement);
            });
        }
        public OutputEndorsementDTO Find(Func<Endorsement,bool> filter)
        {
            return TryCatch(() =>
            {
                Endorsement endorsement = _mainRepository.Find(filter, 
                    e => e.Policy,
                    e => e.Policy.Broker,
                    e => e.Policy.Broker.Brokerage);

                return _mapper.Map<OutputEndorsementDTO>(endorsement);
            });
        }
        public PagedResult<OutputEndorsementDTO> FindAll(int page, int pageSize, string sort, Filters<Endorsement> filters)
        {
            return TryCatch(() =>
            {
                 return FindPaginated<OutputEndorsementDTO>(page, pageSize, sort, filters,
                    e => e.Reason,
                    e => e.Policy,
                    e => e.Policy.Condition,
                    e => e.Policy.Condition.Product,
                    e => e.Policy.Broker,
                    e => e.Policy.Broker.Brokerage);
            });
        }
        public OutputEndorsementDTO Reply(long endorsementId, bool approve)
        {
            return TryCatch(() =>
            {
                var endorsement = _mainRepository.TryGet(endorsementId);
                if (endorsement.Status != EndorsementStatus.Pending)
                {
                    throw new Exceptions.InvalidException(e=>e.EL039);
                }
                if (approve && endorsement.Type == EndorsementType.Cancellation)
                {
                    _policyService.Cancel(endorsement.PolicyId);
                }
                endorsement.Status = approve ? EndorsementStatus.Approved : EndorsementStatus.Denied;
                _validationManager.Validate(endorsement);
                _mainRepository.Update(endorsement);
                Commit();
                return _mapper.Map<OutputEndorsementDTO>(endorsement);
            });
        }
    }
}
