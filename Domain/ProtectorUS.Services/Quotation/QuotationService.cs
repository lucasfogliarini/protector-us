﻿using System;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace ProtectorUS.Services
{
    using DTO;
    using Model.Services;
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Exceptions;

    public class QuotationService : BaseService<Quotation>, IQuotationService
    {
        readonly IRatingPlanFormService _ratingPlanService;
        readonly ILocalizationService _localizationService;
        readonly ICampaignRepository _campaignRepository;
        readonly IBrokerRepository _brokerRepository;
        readonly IEmailTemplateService _emailTemplateService;
        readonly IDeclineReasonRepository _declineReasonRepository;
        readonly IStateSurchargeRepository _stateSurchargePercentageRepository;
        readonly ICountySurchargeRepository _countySurchargeRepository;

        public QuotationService(
           IStateSurchargeRepository stateSurchargePercentageRepository,
           ICountySurchargeRepository countySurchargeRepository,
           IQuotationRepository quotationRepository,
           IRatingPlanFormService ratingPlanService,
           IEmailTemplateService emailTemplateService,
           ILocalizationService localizationService,
           ICampaignRepository campaignRepository,
           IBrokerRepository brokerRepository,
           IDeclineReasonRepository declineReasonRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                quotationRepository,
                ratingPlanService as BaseService,
                localizationService as BaseService)
        {
            _stateSurchargePercentageRepository = stateSurchargePercentageRepository;
            _countySurchargeRepository = countySurchargeRepository;
            _ratingPlanService = ratingPlanService;
            _localizationService = localizationService;
            _campaignRepository = campaignRepository;
            _brokerRepository = brokerRepository;
            _emailTemplateService = emailTemplateService;
            _declineReasonRepository = declineReasonRepository;
        }
        
        private RatingPlanComputer CalculatePremium(Quotation quotation, InputRiskAnalysisDTO riskAnalysisIn)
        {
            RatingPlanComputer ratingPlanComputer = _ratingPlanService.GetRatingPlanComputer(riskAnalysisIn.HashCode);
            IEnumerable<RiskAnalysisField> riskAnalysisFields = _mapper.Map<IEnumerable<RiskAnalysisField>>(riskAnalysisIn.Fields);
            ratingPlanComputer.Validate(riskAnalysisFields);
            quotation.PolicyPremium = ratingPlanComputer.CalculatePolicyPremium();
            quotation.Revenue = ratingPlanComputer.Revenue;
            quotation.Rounding = ratingPlanComputer.Rounding;
            quotation.PolicyStartDate = ratingPlanComputer.PolicyStartDate;
            quotation.SetRiskAnalysis(ratingPlanComputer.RiskAnalysis);
            quotation.RegionId = ratingPlanComputer.RegionId;
            quotation.RatingPlanFormVersionId = ratingPlanComputer.RatingPlanFormVersionId;
            CalculateSurcharges(quotation);
            ratingPlanComputer.VerifyMaxRevenueDecline();
            quotation.IsDeclined = ratingPlanComputer.IsDeclined;
            return ratingPlanComputer;
        }
        private void CalculateSurchargePercentage(Quotation quotation, SurchargePercentage surchargePercentage)
        {
            if (surchargePercentage != null)
            {
                quotation.Surcharges = quotation.Surcharges ?? new List<SurchargePercentage>();
                quotation.Surcharges.Add(surchargePercentage);

                decimal amount = quotation.PolicyPremium * surchargePercentage.Percentage;
                decimal? minTax = surchargePercentage.Surcharge?.MinTax;
                amount = amount < minTax ? minTax.GetValueOrDefault() : amount;
                switch (surchargePercentage.Surcharge.Type)
                {
                    case SurchargeType.Tax:
                        quotation.TaxAmount += amount;
                        break;
                    case SurchargeType.Fee:
                        quotation.FeeAmount += amount;
                        break;
                }
            }
        }
        private void CalculateSurcharges(Quotation quotation)
        {
            var region = _localizationService.GetRegion(quotation.RegionId);

            SurchargePercentage stateSurcharge = _stateSurchargePercentageRepository.GetPercentage(region.City.StateId);
            CalculateSurchargePercentage(quotation, stateSurcharge);

            SurchargePercentage countySurcharge = _countySurchargeRepository.GetPercentage(region.CityId);
            CalculateSurchargePercentage(quotation, countySurcharge);
        }
        public OutputQuotationDTO Get(Guid hash)
        {
            return TryCatch(() =>
            {
                Quotation quotation =  _mainRepository.TryFind(x => x.Hash == hash,
                    q => q.Region,
                    q => q.Region.City,
                    q => q.Region.City.State,
                    q => q.Broker,
                    q => q.Broker.Brokerage);
                if (quotation.InsuredTimeNow.Date > quotation.ExpirationDate)
                {
                    throw new InvalidException(e => e.EL015);
                }
                if (quotation.Status == ActivationStatus.Activated)
                {
                    throw new InvalidException(e => e.EL046);
                }
                if (quotation.InsuredTimeNow.Date > quotation.PolicyStartDate)
                {
                    quotation.PolicyStartDate = quotation.InsuredTimeNow.Date;
                    _mainRepository.Update(quotation);
                    Commit();
                }
                var quotationOut = _mapper.Map<OutputQuotationDTO>(quotation);
                quotationOut.RiskAnalysis = _mapper.Map<OutputRiskAnalysisDTO>(quotation.DeserializeRiskAnalysis());
                return quotationOut;
            });
        }
        public IReadOnlyCollection<GraphicDTO> GetTotalStatus(Filters<Quotation> filters, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                int intervaleixo = 0;

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 24;
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 7;
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = int.Parse(dateend.Subtract(datebegin).TotalDays.ToString());
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 12;
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var quotations = _mainRepository.FindAll(filters, x=>x.BrokerageProduct);

                var quotationsTotal = quotations.Count();
                var onActivated = quotations.Where(x => x.Status == ActivationStatus.Activated).Count();
                var onCheckout = 0;// quotations.Where(x => x.Status == ActivationStatus.DetailsCompleted).Count();
                var onQuotation = quotations.Where(x => x.Status == ActivationStatus.Quoted).Count();

                decimal hitratio = quotationsTotal != 0 && onActivated != 0 ? ((onActivated * 100) / quotationsTotal) : 0;


                GraphicDTO totals = new GraphicDTO();
                totals.List = new List<ItemGraphicDTO>();
                totals.Name = "Totals";
                ItemGraphicDTO itemtotal1 = new ItemGraphicDTO() { Key = "On Quotation", Value = onQuotation };
                totals.List.Add(itemtotal1);
                ItemGraphicDTO itemtotal2 = new ItemGraphicDTO() { Key = "On Checkout", Value = onCheckout };
                totals.List.Add(itemtotal2);
                ItemGraphicDTO itemtotal3 = new ItemGraphicDTO() { Key = "Completed", Value = onActivated };
                totals.List.Add(itemtotal3);

                result.Add(totals);

                GraphicDTO graphic2 = new GraphicDTO();
                graphic2.List = new List<ItemGraphicDTO>();
                graphic2.Name = "Ration";

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Hit Ratio", Value = hitratio };
                graphic2.List.Add(itemgraphic);

                result.Add(graphic2);

                return result;
            });
        }
        private long GetAssignedBroker(Quotation quotation)
        {
            if (quotation.CampaignId > 0)
            {
                long campaignId = quotation.CampaignId.GetValueOrDefault();
                Campaign campaign = _campaignRepository.TryGet(campaignId);
                return campaign.BrokerId;
            }
            else
            {
                Broker broker = _brokerRepository.GetMasterByBrokerageProduct(quotation.BrokerageProductId);
                if (broker == null)
                {
                    throw new NotFoundException(e => e.EL025);
                }
                return broker.Id;
            }
        }
        public QuotationResponseDTO Create(QuotationInsertDTO quotationIn)
        {
            return TryCatch(() =>
            {
                Quotation quotation = _mapper.Map<Quotation>(quotationIn);
                quotation.BrokerId = GetAssignedBroker(quotation);
                RatingPlanComputer ratingPlanComputer = CalculatePremium(quotation, quotationIn.RiskAnalysis);
                quotation.InsuredTimezone = ratingPlanComputer.InsuredTimezone;
                quotation.Status = ActivationStatus.Quoted;
                quotation.ExpirationDate = quotation.InsuredTimeNow.Date.AddDays(15);
                quotation.Hash = Guid.NewGuid();
                if (ratingPlanComputer.DeclineReasons != null && ratingPlanComputer.DeclineReasons.Count > 0)
                {
                    quotation.DeclineReasons =  new List<DeclineReason>();
                    ratingPlanComputer.DeclineReasons.Distinct().ToList().ForEach(x => quotation.DeclineReasons.Add(x));
                }                            
                _validationManager.Validate(quotation);
                _mainRepository.Add(quotation);
                Commit();
                if (!quotation.IsDeclined)
                {
                    quotation = _mainRepository.TryGet(quotation.Id,
                        q => q.Region,
                        q => q.Region.City,
                        q => q.Region.City.State,
                        q => q.BrokerageProduct,
                        q => q.BrokerageProduct.Product,
                        q => q.Broker,
                        q => q.Broker.Brokerage,
                        q => q.Broker.Brokerage.Address,
                        q => q.Broker.Brokerage.Address.Region,
                        q => q.Broker.Brokerage.Address.Region.City,
                        q => q.Broker.Brokerage.Address.Region.City.State);

                    string body = _emailTemplateService.GetQuotationBody(quotation);
                    _emailTemplateService.Send(EmailTemplate.Quotation.Id, quotation.ProponentEmail, body);
                }
                return _mapper.Map<QuotationResponseDTO>(quotation);
            });
        }
        public QuotationContactDTO Contact(QuotationContactDTO quotationIn)
        {
            return TryCatch(() =>
            {
                Quotation quotation = _mainRepository.TryFind(x => x.Hash == quotationIn.Hash,
                                    q => q.Broker,
                                    q => q.BrokerageProduct,
                                    q => q.BrokerageProduct.Product);
                
                string body = _emailTemplateService.GetContactBrokerBody(quotation, quotationIn.ProponentPhone);
                _emailTemplateService.Send(EmailTemplate.ContactBroker.Id, quotation.Broker.Email, body, quotation.ProponentEmail);
                return quotationIn;
            });
        }

        public GraphicDTO GetDeclinesTotalByType(Filters<Quotation> filters, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                int intervaleixo = 0;

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 24;
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 7;
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = int.Parse(dateend.Subtract(datebegin).TotalDays.ToString());
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 12;
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                var quotes = _mainRepository.FindAll(filters, x => x.DeclineReasons, x=>x.BrokerageProduct);
                var declines = quotes.SelectMany(x => x.DeclineReasons).ToList();
                var declinesTotals = declines
                                .GroupBy(x=> x.QuestionFieldName)
                                .Select(g => new Decline {
                                    QuestionName = g.Key,
                                    Total = g.Select(x=> x.Id).Distinct().Count()
                                });


                GraphicDTO totals = new GraphicDTO();
                totals.List = new List<ItemGraphicDTO>();
                totals.Name = "Totals";

                declinesTotals.ToList().ForEach((x => totals.List.Add(new ItemGraphicDTO() {
                                                                    Key = x.QuestionName,
                                                                    Value = x.Total})));

                return totals;
            });
        }


        public GraphicDTO GetDeclinesTotalByType(PeriodFilter period)
        {

           
            return TryCatch(() =>
            {
                var filters = new Filters<DeclineReason>();

                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                int intervaleixo = 0;

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 24;
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 7;
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = int.Parse(dateend.Subtract(datebegin).TotalDays.ToString());
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 12;
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);


                var declines = _declineReasonRepository.FindAll(new Filters<DeclineReason>())
                                .GroupBy(x => x.QuestionFieldName)
                                .Select(g => new Decline
                                {
                                    QuestionName = g.Key,
                                    Total = g.Select(x => x.QuestionFieldName).Distinct().Count()
                                });


                GraphicDTO totals = new GraphicDTO();
                totals.List = new List<ItemGraphicDTO>();
                totals.Name = "Totals";

                declines.ToList().ForEach((x => totals.List.Add(new ItemGraphicDTO()
                {
                    Key = x.QuestionName,
                    Value = x.Total
                })));

                return totals;
            });
        }

        private class Decline
        {
            public string QuestionName { get; set; }
            public int Total { get; set; }
        }

    }
}
