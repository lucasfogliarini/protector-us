﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Net;

namespace ProtectorUS.Services
{
    using AutoMapper;
    using Configuration;
    using Configuration.ProtectorUS;
    using Diagnostics.Logging;
    using Exceptions;
    using Model;
    using Model.Repositories;
    using Model.Services;
    using Model.Validation;
    using System.Collections.Generic;
    public class AcordService : BaseService<AcordTransaction>, IAcordService
    {
        #region Properties by Products

        private string _policyCompanyProgramCd;
        private string _configurationProductCode;

        private string _coverageCdMain;
        private string _coverageDescMain;

        private string _coverageDefenseCd;
        private string _coverageDefenseDesc;
        private decimal _coverageDefenseAmt;

        private string _companyClassCode;
        private string _companyClassCodeDesc;

        #endregion

        #region Properties by State

        private PolicyCreditOrSurcharge _policySurcharge;

        #endregion

        #region Acord Properties

        private string _retroactiveDate;

        private AcordIssue _acordIssue;
        private RiskAnalysis _riskAnalysis;
        private AcordTransaction _acordTransaction;
        private CultureInfo _cultureInfo = new CultureInfo("en-US");

        private readonly IBlobService _blobservice;
        private readonly IAcordTransactionRepository _acordTransactionRepository;
        private readonly IPolicyRepository _policyRepository;

        const string _urlPostToken = "http://10.40.19.40:8181/cxf/import/acord?authKey=INTDEVIMPORT&sync=false";
        const string _urlPostPdf = "http://10.40.19.40:8181/cxf/forms/generateDocuments?authKey=INTDEVFORMS";

        readonly ApiEsbElement ESBConfig = ProtectorUSConfiguration.GetConfig().ApiESB;

        #endregion

        public AcordService(
            IBlobService blobService,
            IPolicyRepository policyRepository,
           IAcordTransactionRepository acordTransactionRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                acordTransactionRepository)
        {
            _policyRepository = policyRepository;
            _blobservice = blobService;
            _acordTransactionRepository = acordTransactionRepository;
        }

        #region XML Acord

        public bool GenerateAcordXML(Order order)
        {
            try
            {
                AddAcordTransaction(order);

                _riskAnalysis = order.Quotation.DeserializeRiskAnalysis();

                ValidateProperties();
                ChangePropertiesByProduct(order);
                //ChangePropertiesByState(order);

                _acordIssue = new AcordIssue
                {
                    Transaction = new Transaction()
                    {
                        TransactionId = _acordTransaction.Id.ToString(),
                        TransactionType = "Issue",
                        EventType = "Generic",
                        TransactionStatus = "CM",
                        PolicyId = order.Id.ToString(),
                        SubmissionNumber = "",
                        SubmissionId = "",
                        BusinessUnit = "EO",
                        ProductVersionEffectiveDate = "",
                        ConfigurationProductCode = _configurationProductCode,
                        ConfigurationProductVersion = "",
                        IsRenewal = "false",
                        StripeFee = FormatMoney(order.StripeFee),
                        ACORD = new ACORD
                        {
                            #region SignonRs                        
                            SignonRs = new SignonRs
                            {
                                ClientDt = FormatDate(order.OrderDate),
                                ServerDt = "",
                                Language = "US",
                                CustLangPref = "US",
                                CustId = new CustId
                                {
                                    SPName = "A",
                                    CustPermId = $"{order.Policy.Insured.FirstName} {order.Policy.Insured.LastName}",
                                    CustLoginId = order.Policy.Insured.Id.ToString(),
                                },
                                ClientApp = new ClientApp { Org = "Argo", Name = "PROTECTOR", Version = "", },
                            },
                            #endregion

                            #region InsuranceSvcRs
                            InsuranceSvcRs = new InsuranceSvcRs
                            {
                                SPName = "Proposal",
                                CommlPkgPolicyModRs = new CommlPkgPolicyAddRs
                                {
                                    TransactionResponseDt = "",
                                    CurCd = "USD",
                                    MsgStatus = new MsgStatus { MsgStatusCd = "Success", },

                                    #region Producer
                                    Producer = new Producer
                                    {
                                        ProducerItemIdInfo = new ProducerItemIdInfo { AgencyId = order.Policy.Broker.Brokerage.AgentNumber, },
                                        ProducerGeneralPartyInfo = new ProducerGeneralPartyInfo
                                        {
                                            ProducerNameInfo = new ProducerNameInfo
                                            {
                                                ProducerCommlName = new ProducerCommlName { CommercialName = $"{order.Policy.Broker.FullName}", }
                                            },
                                            ProducerAddr = new ProducerAddr
                                            {
                                                Addr1 = order.Policy.Broker.Brokerage.Address.StreetLine1,
                                                Addr2 = order.Policy.Broker.Brokerage.Address.StreetLine2,
                                                City = order.Policy.Broker.Brokerage.Address.Region.City.Name,
                                                StateProvCd = order.Policy.Broker.Brokerage.Address.Region.City.State.Abbreviation,
                                                StateProv = order.Policy.Broker.Brokerage.Address.Region.City.State.Name,
                                                PostalCode = order.Policy.Broker.Brokerage.Address.Region.ZipCode,
                                                CountryCd = order.Policy.Broker.Brokerage.Address.Region.City.State.Country.Abbreviation,
                                                Country = order.Policy.Broker.Brokerage.Address.Region.City.State.Country.Name,
                                            }
                                        }
                                    },
                                    #endregion

                                    #region InsuredOrPrincipal                                         
                                    Insured = new InsuredOrPrincipal
                                    {
                                        InsuredItemIdInfo = new InsuredItemIdInfo { InsuredSystemId = order.Quotation.Broker.Brokerage.ChargeAccountId, },
                                        InsuredInfo = new InsuredInfo { InsuredOrPrincipalRoleCd = "Primary Insured", },
                                        InsuredGeneralPartyInfo = new InsuredGeneralPartyInfo
                                        {
                                            InsuredNameInfo = new InsuredNameInfo
                                            {
                                                InsuredCommlName = new InsuredCommlName { CommercialName = $"{order.Policy.Business.Name}" },
                                                InsuredSupplementaryNameInfo = new InsuredSupplementaryNameInfo
                                                {
                                                    SupplementaryName = "",
                                                    SupplementaryNameCd = "",
                                                }
                                            },
                                            InsuredAddr = new InsuredAddr
                                            {
                                                Addr1 = order.Policy.Business.Address.StreetLine1,
                                                Addr2 = order.Policy.Business.Address.StreetLine2,
                                                City = order.Policy.Business.Address.Region.City.Name,
                                                StateProvCd = order.Policy.Business.Address.Region.City.State.Abbreviation,
                                                StateProv = order.Policy.Business.Address.Region.City.State.Name,
                                                PostalCode = order.Policy.Business.Address.Region.ZipCode,
                                                CountryCd = order.Policy.Business.Address.Region.City.State.Country.Abbreviation,
                                                Country = order.Policy.Business.Address.Region.City.State.Country.Name,
                                            }
                                        }
                                    },
                                    #endregion

                                    #region MiscParty
                                    MiscParty = new MiscParty
                                    {
                                        MiscPartyInfo = new MiscPartyInfo { MiscPartyRoleCd = "CarrierName", },
                                        MiscPartyGeneralPartyInfo = new MiscPartyGeneralPartyInfo
                                        {
                                            MiscPartyNameInfo = new MiscPartyNameInfo
                                            {
                                                MiscPartyCommlName = new MiscPartyCommlName { CommercialName = order.Policy.Condition.State.Carrier.Name, }
                                            },
                                            MiscPartyAddr = new MiscPartyAddr
                                            {
                                                Addr1 = order.Policy.Condition.State.Carrier.Address.StreetLine1,
                                                Addr2 = order.Policy.Condition.State.Carrier.Address.StreetLine2,
                                                City = order.Policy.Condition.State.Carrier.Address.Region.City.Name,
                                                StateProvCd = order.Policy.Condition.State.Carrier.Address.Region.City.State.Abbreviation,
                                                StateProv = order.Policy.Condition.State.Carrier.Address.Region.City.State.Name,
                                                PostalCode = order.Policy.Condition.State.Carrier.Address.Region.ZipCode,
                                                CountryCd = order.Policy.Condition.State.Carrier.Address.Region.City.State.Country.Abbreviation,
                                                Country = order.Policy.Condition.State.Carrier.Address.Region.City.State.Country.Name,
                                            }
                                        }
                                    },
                                    #endregion

                                    #region CommlPolicy
                                    Policy = new CommlPolicy
                                    {
                                        PolicyNumber = $"{ order.Policy.Number }",
                                        PolicyCompanyProgramCd = _policyCompanyProgramCd,
                                        PolicyLOBCd = "EO",
                                        PolicyCommissionafterContributionPct = Convert.ToUInt32(order.BrokerageFeePercentage).ToString(),
                                        PolicyControllingStateProvCd = order.Policy.Condition.State.Abbreviation,
                                        PolicyContractTerm = new PolicyContractTerm { EffectiveDt = FormatDate(order.Policy.PeriodStart), ExpirationDt = FormatDate(order.Policy.PeriodEnd), },
                                        PolicyWrittenAmt = new PolicyWrittenAmt { Amt = FormatMoney((order.TotalCharged)), },
                                        PolicyMiscPremAmt = new PolicyMiscPremAmt { Amt = FormatMoney(order.Policy.TotalPremium), },
                                        PolicyAdditionalInterest = new PolicyAdditionalInterest
                                        {
                                            PolicyAdditionalInterestInfo = new PolicyAdditionalInterestInfo { AccountNumberId = "J2", },
                                            PolicyGeneralPartyInfo = new PolicyGeneralPartyInfo
                                            {
                                                PolicyGeneralPartyRoleCd = "Underwriter",
                                                PolicyNameInfo = new PolicyNameInfo
                                                {
                                                    PolicyPersonName = new PolicyPersonName { Surname = "Donovan-Schager", GivenName = "David", },
                                                },
                                                PolicyCommunications = new PolicyCommunications
                                                {
                                                    PolicyEmailInfo = new PolicyEmailInfo { PolicyEmailAddr = "dschager@argoprous.com", },
                                                    PolicyPhoneInfo = new PolicyPhoneInfo { PolicyPhoneNumber = "480-751-3667", },
                                                },
                                            }
                                        },
                                        PolicySupplement = new PolicySupplement { NYFreeTradeZone = "", NYFreeTradeZoneClassCd = "", },
                                        PolicyCreditOrSurcharge = GetSurcharges(order),
                                    },
                                    #endregion

                                    #region PriorPolicy 
                                    PriorPolicy = new PriorPolicy
                                    {
                                        FinalPremAmt = new PriorPolicyFinalPremAmt { Amt = "", },
                                        PriorPolicyContractTerm = new PriorPolicyContractTerm { EffectiveDt = "", ExpirationDt = "", },
                                    },
                                    #endregion

                                    #region ErrorsAndOmissionsLineBusiness

                                    ErrorsAndOmissionsLineBusiness = new ErrorsAndOmissionsLineBusiness
                                    {
                                        LOBCd = "EO",
                                        Coverage = new EOCoverage[]
                                           {
                                            new EOCoverage
                                            {
                                                //
                                                CoverageCd = _coverageCdMain,
                                                CoverageDesc = _coverageDescMain,
                                                EOLimit = new EOLimit[] {
                                                                   new EOLimit { EOLimitAppliesToCd = "EachClaim", LimitFormatCurrencyAmt = new EOLimitFormatCurrencyAmt { Amt =  order.Policy.CoverageEachClaim.ToString() } },
                                                                   new EOLimit { EOLimitAppliesToCd = "Aggregate", LimitFormatCurrencyAmt = new EOLimitFormatCurrencyAmt { Amt = order.Policy.CoverageAggregate.ToString() } }
                                                                   },
                                                EODeductible = new EODeductible[] {
                                                                   new EODeductible { DeductibleFormatCurrencyAmt = new EODeductibleFormatCurrencyAmt {Amt=order.Policy.DeductibleEachClaim.ToString() }, DeductibleAppliesToCd = "CL" },
                                                                   new EODeductible { DeductibleFormatCurrencyAmt = new EODeductibleFormatCurrencyAmt {Amt=order.Policy.DeductibleAggregate.ToString() }, DeductibleAppliesToCd = "Aggregate" }
                                                                   },
                                                EOWrittenAmt = new EOWrittenAmt { Amt = FormatMoney(order.Policy.TotalPremium - _coverageDefenseAmt), },

                                                EOOption = new EOOption[] {
                                                                   new EOOption { EOOptionTypeCd="Opt1", EOOptionCd = "CompanyClassCode", EOOptionValue= _companyClassCode, EOOptionValueDesc=_companyClassCodeDesc },
                                                                   new EOOption { EOOptionTypeCd="Opt2", EOOptionCd = "BaseRate", EOOptionValue= FormatMoney(_riskAnalysis.GetBasePremium().GetValueOrDefault()), EOOptionValueDesc=FormatMoney(_riskAnalysis.GetBasePremium().GetValueOrDefault()) },
                                                                   new EOOption { EOOptionTypeCd="Opt3", EOOptionCd = "RateMod", EOOptionValue= FormatMoney(_riskAnalysis.GetRateMod()), EOOptionValueDesc=FormatMoney(_riskAnalysis.GetRateMod()) },
                                                                   new EOOption { EOOptionTypeCd="Opt4", EOOptionCd = "Exposure", EOOptionValue= FormatMoney(order.Quotation.Revenue), EOOptionValueDesc=FormatMoney(order.Quotation.Revenue) }
                                                                   },
                                                EOCoverageSupplement = new EOCoverageSupplement { EOClaimsMadeInfo = new EOClaimsMadeInfo { ClaimsMadeInd = "1", CurrentRetroactiveDt = _retroactiveDate } }
                                            },
                                            new EOCoverage
                                            {
                                                CoverageCd = _coverageDefenseCd,
                                                CoverageDesc = _coverageDefenseDesc,
                                                EOWrittenAmt = new EOWrittenAmt { Amt = FormatMoney(_coverageDefenseAmt)  },
                                            },
                                            //new EOCoverage
                                            //{
                                            //    CoverageCd = "NETSP",
                                            //    CoverageDesc = "Network Security and Privacy Liability",
                                            //    EOWrittenAmt = new EOWrittenAmt { Amt = "1" }
                                            //},
                                           }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw new InvalidException(e => e.EL052);
            }

            string xmlAcord = ConvertAcordToXml(_acordIssue);

            UpdateAcordTransaction(xmlAcord);

            return ESBIntegration(_acordTransaction, order, xmlAcord);

        }

        private PolicyCreditOrSurcharge[] GetSurcharges(Order order)
        {
            List<PolicyCreditOrSurcharge> listCreditOrSurcharge = new List<PolicyCreditOrSurcharge>();

            IList<SurchargePercentage> surchargeList = order.Quotation.Surcharges;

            if (surchargeList != null)
                foreach (var surcharge in surchargeList)
                {
                    if (surcharge.Surcharge.Type == SurchargeType.Fee)
                    {
                        _policySurcharge = new PolicyCreditOrSurcharge
                        {
                            CreditSurchargeDt = FormatDate(order.Policy.PeriodStart),
                            CreditSurchargeCd = surcharge.Surcharge.Code,
                            CreditSurchargeAmtDesc = surcharge.Surcharge.Description,
                            PolicyNumericValue = new PolicyNumericValue { FormatInteger = FormatMoney(order.Quotation.FeeAmount) }
                        };
                    }

                    else {
                        _policySurcharge = new PolicyCreditOrSurcharge
                        {
                            CreditSurchargeDt = FormatDate(order.Policy.PeriodStart),
                            CreditSurchargeCd = "KYMUN",
                            CreditSurchargeAmtDesc = "KY Tax",
                            PolicyNumericValue = new PolicyNumericValue { FormatInteger = FormatMoney(order.Quotation.TaxAmount) }
                        };
                    }
                    listCreditOrSurcharge.Add(_policySurcharge);
                }

            return listCreditOrSurcharge.ToArray();
        }

        private string ConvertAcordToXml(AcordIssue xmlAcord)
        {
            string xmlConverted = "";
            XmlSerializer xsSubmit = new XmlSerializer(typeof(AcordIssue));
            var subReq = xmlAcord;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                xsSubmit.Serialize(writer, subReq);
                xmlConverted = sww.ToString();
            }
            return xmlConverted;
        }

        #endregion

        #region ESB Integration

        private bool ESBIntegration(AcordTransaction acordTransaction, Order order, string xmlAcord)
        {
            if (PostImportXml(acordTransaction, xmlAcord))
                if (PostReturnPdf(order.Policy, xmlAcord))
                    return true;

            return false;
        }

        private bool PostImportXml(AcordTransaction acordTransaction, string acordXml)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(ESBConfig.urlToken);

                var postData = acordXml;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/xml";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(responseString);
                XmlNodeList elemlist = xml.GetElementsByTagName("token");
                if (elemlist.Count > 0)
                {
                    string token = elemlist[0].InnerXml;

                    acordTransaction.Token = token;
                    _acordTransactionRepository.Update(acordTransaction);
                    Commit();

                    return true;
                }
                else
                    LogTransactions("PDF", acordTransaction.Policy.Number, "RESPONSE", responseString);

                return false;
            }
            catch (WebException wb)
            {
                //if (wb.Status == WebExceptionStatus.ConnectFailure)
                //    throw new InvalidException(e => e.EL054);


                LogTransactions("TOKEN", acordTransaction.Policy.Number, wb.Message, wb.InnerException.ToString());




                return false;
            }
        }

        private bool PostReturnPdf(Policy policy, string xmlAcord)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(ESBConfig.urlPDF);

                var postData = xmlAcord;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/xml";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(responseString);
                XmlNodeList elemlist = xml.GetElementsByTagName("pdf");

                if (elemlist.Count > 0)
                {
                    string pdf64 = elemlist[0].InnerXml;

                    byte[] pdfBytes = Convert.FromBase64String(pdf64);

                    string policyfilename = policy.Number;
                    policy.PolicyPath = _blobservice.Upload(BlobType.Policy, pdfBytes, policyfilename);

                    _policyRepository.Update(policy);
                    Commit();


                    SendToImageRight(policy, pdf64);

                    return true;
                }
                else
                    LogTransactions("PDF", policy.Number, "RESPONSE", responseString);

                return false;
            }
            catch (Exception wb)
            {
                //if (wb.Status == WebExceptionStatus.ConnectFailure)
                //    throw new InvalidException(e => e.EL054);

                //LogTransactions("PDF", policy.Number, wb.Message, wb.InnerException.ToString());

                return false;
            }
        }

        public bool RegenPdf(long PolicyId)
        {
            Policy policy = _policyRepository.TryGet(PolicyId);

            AcordTransaction acordTransaction = _acordTransactionRepository.Find(a => a.PolicyId == policy.Id);

            if (acordTransaction != null && !string.IsNullOrEmpty(acordTransaction.Xml))
            {
                if (string.IsNullOrEmpty(acordTransaction.Token))
                    return ESBIntegration(acordTransaction, policy.Order, acordTransaction.Xml);
                else
                    return (PostReturnPdf(policy, acordTransaction.Xml));
            }
            else if (acordTransaction != null && string.IsNullOrEmpty(acordTransaction.Xml))
            {
                _acordTransactionRepository.Remove(acordTransaction);
                return GenerateAcordXML(policy.Order);
            }
            else
                return GenerateAcordXML(policy.Order);
        }

        public void SendToImageRight(Policy policy, string pdf64)
        {
            try
            {
                string bodySoap = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:com=""com.argo.esb.dms"">
                                       <soapenv:Header/>
                                       <soapenv:Body>
                                          <com:create>
                                             <user>
                                                <userId>twitt</userId>
                                                <authenticationSystem>LDAP</authenticationSystem>
                                             </user>
                                             <createValues id="""" name=""UW02"" type=""DRAWER"">
                                                <children>
                                                   <path id ="""" name="".POLICYNUMBER."" type=""FILE"">
                                                      <children>
                                                         <path id="""" name=""Policy Documents"" type=""FOLDER"">
                                                            <documents>
                                                               <document id="""" name="".POLICYNUMBER..pdf"" type=""POLF"" mimeType="""">
                                                                  <attributes>
                                                                     <attribute>
                                                                        <name> Description </name>
                                                                        <value> This is policy as of 9 / 24 / 2013 </value>
                                                                     </attribute>
                                                                  </attributes>
                                                                        <documentBody> .BASE64. </documentBody>
                                                               </document>
                                                            </documents>
                                                         </path>
                                                      </children>
                                                   </path>
                                                </children>
                                             </createValues>
                                             <securityToken>ImageRightSecurityToken</securityToken>
                                          </com:create>
                                       </soapenv:Body>
                                    </soapenv:Envelope>";

                bodySoap = bodySoap.Replace("ImageRightSecurityToken", ESBConfig.imageRightSecurityToken);
                bodySoap = bodySoap.Replace(".BASE64.", pdf64);
                bodySoap = bodySoap.Replace(".POLICYNUMBER.", policy.Number);

                HttpWebRequest request = CreateWebRequestSOAP();
                XmlDocument soapEnvelopeXml = new XmlDocument();

                soapEnvelopeXml.LoadXml(bodySoap);

                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();

                        XmlDocument xml = new XmlDocument();
                        xml.LoadXml(soapResult);
                        XmlNodeList elemlist = xml.GetElementsByTagName("success");

                        if (elemlist.Count > 0)
                        {
                            string returnSuccess = elemlist[0].InnerXml;
                        }
                        else
                            LogTransactions("IMAGE RIGHT", policy.Number, "RESPONSE", soapResult);
                    }
                }



            }
            catch (Exception ex)
            {

                LogTransactions("IMAGE RIGHT", policy.Number, ex.Message, ex.InnerException.ToString());

            }

        }

        public HttpWebRequest CreateWebRequestSOAP()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ESBConfig.urlImageRight);
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        public void LogTransactions(string stepAcord, string policyNumber, string exMessage, string exception)
        {
            List<AuditLog> listAuditLog = new List<AuditLog>();

            AuditLog auditLog = new AuditLog
            {
                UserName = stepAcord,
                TypeFullName = $"Message: {exMessage} || Exception: {exception}",
            };

            listAuditLog.Add(auditLog);

            var logInfo = new LogInfo(policyNumber, "", listAuditLog);
            Logger.LogDataWithoutUserIdentified(logInfo);
        }

        #endregion

        #region CRUD ACORD TRANSACTION

        private void AddAcordTransaction(Order order)
        {
            _acordTransaction = new AcordTransaction
            {
                PolicyId = order.Policy.Id,
                CreatedDate = DateTime.Now,
            };
            _acordTransactionRepository.Add(_acordTransaction);
            Commit();
        }

        private void UpdateAcordTransaction(string xmlAcord)
        {
            _acordTransaction.Xml = xmlAcord;
            _acordTransactionRepository.Update(_acordTransaction);
            Commit();
        }

        #endregion

        #region Format Values

        private string FormatMoney(decimal? value, bool withDollar = false)
        {
            string currencyFormat = withDollar ? "{0:C}" : "{0:0.00}";
            return string.Format(_cultureInfo, currencyFormat, value);
        }

        private string FormatDate(DateTime date)
        {
            return String.Format("{0:yyyy-MM-dd}", date);
        }

        #endregion

        #region Properties Values

        private void ValidateProperties()
        {
            try
            {
                _retroactiveDate = FormatDate(Convert.ToDateTime(_riskAnalysis.GetFieldValue(RatingPlanComputer.retroactiveDateField)));
            }
            catch (Exception ex)
            {
                _retroactiveDate = "";
            }


        }


        private void ChangePropertiesByState(Order order)
        {





            //string state = order.Quotation.Region.City.State.Abbreviation;

            //string creditSurchargeCode = "";
            //string creditSurchargeAmtDesc = "";

            //switch (state)
            //{
            //    case "WV":
            //        creditSurchargeCode = "WVFC";
            //        creditSurchargeAmtDesc = "Fire & Casualty Surcharge";
            //        break;

            //    case "NJ":
            //        creditSurchargeCode = "LIGA";
            //        creditSurchargeAmtDesc = "PLIGA";
            //        break;

            //    default:
            //        break;
            //}

            //if (!string.IsNullOrEmpty(creditSurchargeCode))
            //{
            //    _policySurcharge = new PolicyCreditOrSurcharge
            //    {
            //        CreditSurchargeDt = FormatDate(order.Policy.PeriodStart),
            //        CreditSurchargeCd = creditSurchargeCode,
            //        CreditSurchargeAmtDesc = creditSurchargeAmtDesc,
            //        PolicyNumericValue = new PolicyNumericValue { FormatInteger = FormatMoney(order.SurchargeAmount) }
            //    };
            //}


        }

        private void ChangePropertiesByProduct(Order order)
        {
            long IdProduct = order.Policy.Condition.Product.Id;

            switch (IdProduct)
            {
                case Product.ArchitectectsEngineersId:
                    _policyCompanyProgramCd = "P59";
                    _configurationProductCode = "AE";

                    _coverageCdMain = "AEPLB";
                    _coverageDescMain = "A&E Professional Liability";

                    _coverageDefenseCd = "DEFCO";
                    _coverageDefenseDesc = "First Dollar Defense Option";
                    _coverageDefenseAmt = _riskAnalysis.GetCoverageAmount(EngineersComputer.firstDollarDefenseField).GetValueOrDefault();

                    _companyClassCode = "73909";
                    _companyClassCodeDesc = "";
                    break;

                case Product.AccountantsId:
                    _policyCompanyProgramCd = "P69";
                    _configurationProductCode = "AC";

                    _coverageCdMain = "ACCPL";
                    _coverageDescMain = "Accountants Professional Liability";

                    _coverageDefenseCd = "DefenseLimitOutside";
                    _coverageDefenseDesc = "Defense Outside Limit";
                    _coverageDefenseAmt = _riskAnalysis.GetCoverageAmount(AccountantsComputer.defenseOutsideLimitField).GetValueOrDefault();

                    _companyClassCode = "73101";
                    _companyClassCodeDesc = "Accountants: Certified";
                    break;

                default:
                    break;
            }

        }

        #endregion
    }
}
