﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.IO;

namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Model.Services;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model;
    using System.Threading.Tasks;
    using System.Linq.Expressions;
    public class PolicyService : BaseService<Policy>, IPolicyService
    {
        readonly IProductRepository _productRepository;
        readonly IAcordService _acordService;
        readonly IBlobService _blobService;
        public PolicyService(IUnitOfWork unitOfWork,
           IPolicyRepository policyRepository,
           IProductRepository productRepository,
           IAcordService acordService,
           IBlobService blobService,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                policyRepository
                )
        {
            _productRepository = productRepository;
            _acordService = acordService;
            _blobService = blobService;
        }
        
        public MapPointDTO GetMapPoints(Filters<Policy> filters = null)
        {
            return TryCatch(() =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();

                map.RequestInfo = "/policies/{id}";
                filters = filters ?? new Filters<Policy>();
                var policies = _mainRepository.FindAll(filters,
                     p => p.Broker,
                     p => p.Business,
                     p => p.Business.Address,
                     p => p.Insured,
                     p => p.Condition);


                foreach (var item in policies)
                {
                    PointDTO pinitem = new PointDTO() { Id = item.Id, Name = item.Insured.FirstName, idProduct = item.Condition.ProductId, Image = item.Insured.Picture, Latitude = item.Business.Address.Latitude.GetValueOrDefault(), Longitude = item.Business.Address.Longitude.GetValueOrDefault() };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public List<GraphicDTO> GetTotalDashboardTypes(Filters<Policy> filter, PolicyPeriodFilter intervalDays, PolicyTypeView typeView)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now;
                bool semanal = false;
                bool mensal = false;
                switch (intervalDays)
                {
                    case PolicyPeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.Last3Month:
                        semanal = true;
                        datebegin = DateTime.Now.AddMonths(-3);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.Last6Month:
                        semanal = true;
                        datebegin = DateTime.Now.AddMonths(-6);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.LastYear:
                        mensal = true;
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyPeriodFilter.AllTime:
                        mensal = true;
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                }

                var policiescount = _mainRepository.FindAll(filter, x => x.Broker);

                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var policies = _mainRepository.FindAll(filter, x => x.Broker);

                GraphicDTO resumo = new GraphicDTO();
                resumo.List = new List<ItemGraphicDTO>();
                resumo.Name = "Total Polices";
                resumo.List.Add(new ItemGraphicDTO { Key = "Total", Value = Decimal.Parse(policiescount.Count().ToString()) });
                result.Add(resumo);

                foreach (PolicyType item in Enum.GetValues(typeof(PolicyType)))
                {
                    DateTime dateaux = datebegin;

                    GraphicDTO graphic = new GraphicDTO();
                    graphic.List = new List<ItemGraphicDTO>();
                    graphic.Name = EnumExtensions.Description((PolicyType)item);
                    int numberddif;
                    if (mensal)
                    {
                        numberddif = 12;
                        while (numberddif > 0)
                        {
                            ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM yyyy"), Value = (typeView == PolicyTypeView.Number ? policies.Where(x => x.CreatedDate >= new DateTime(dateaux.Year, dateaux.Month, 1)  && x.CreatedDate < new DateTime(dateaux.AddMonths(1).Year, dateaux.AddMonths(1).Month, 1) && x.Type == item).Count() : policies.Where(x => x.CreatedDate >= new DateTime(dateaux.Year, dateaux.Month, 1) && x.CreatedDate < new DateTime(dateaux.AddMonths(1).Year, dateaux.AddMonths(1).Month, 1) && x.Type == item).Sum(s => s.TotalPremium)) };
                            graphic.List.Add(itemgraphic);

                            dateaux = dateaux.AddMonths(1);
                            numberddif--;
                        }
                    }
                    else if (intervalDays != PolicyPeriodFilter.Today)
                    {
                        numberddif = (int)dateend.Subtract(datebegin).TotalDays;
                        int auxcount = 1;
                        decimal auxtotal = 0;
                        string auxlabel = "";
                        bool executa = false;
                        while (numberddif >= 0)
                        {
                            if(semanal)
                            {
                                if(auxcount == 1)
                                    auxlabel = dateaux.ToString("MMM dd");

                                auxtotal += (typeView == PolicyTypeView.Number ? policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1) && x.Type == item).Count() : policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1) && x.Type == item).Sum(s => s.TotalPremium));

                                if (auxcount == 7 || numberddif == 0)
                                {
                                    auxlabel += auxlabel.Substring(0,3) != dateaux.ToString("MMM") ?  auxlabel != dateaux.ToString("MMM dd") ?  "-" + dateaux.ToString("MMM dd") : "" : "-" + dateaux.ToString("dd");
                                    executa = true;
                                }
                            }
                            else{
                                auxlabel = dateaux.ToString("MMM dd");
                                auxtotal = (typeView == PolicyTypeView.Number ? policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1) && x.Type == item).Count() : policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1) && x.Type == item).Sum(s => s.TotalPremium));
                                executa = true;
                            }

                            if (executa)
                            {
                                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = auxlabel, Value = auxtotal };
                                graphic.List.Add(itemgraphic);
                                auxlabel = "";
                                auxcount = 0;
                                auxtotal = 0;
                                executa = false;
                            }
                            auxcount++;
                            dateaux = dateaux.AddDays(1);
                            numberddif--;
                        }
                    }
                    else
                    {
                        numberddif = 24;
                        while (numberddif > 0)
                        {
                            ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("HH:mm"), Value = (typeView == PolicyTypeView.Number ? policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddHours(1) && x.Type == item).Count() : policies.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddHours(1) && x.Type == item).Sum(s => s.TotalPremium)) };
                            graphic.List.Add(itemgraphic);

                            dateaux = dateaux.AddHours(1);
                            numberddif--;
                        }
                    }
                    result.Add(graphic);
                };

                return result;
            });
        }
        public GraphicDTO GetTotalInterval(Filters<Policy> filter, PolicyIntervalFilter interval)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now;
                GraphicDTO graphic = new GraphicDTO();
                switch (interval)
                {
                    case PolicyIntervalFilter.Today:
                        graphic.Name = EnumExtensions.Description((PolicyIntervalFilter)interval);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PolicyIntervalFilter.Month:
                        graphic.Name = datebegin.ToString("MMM");
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, 1, 0, 0, 0);
                        break;
                    case PolicyIntervalFilter.Year:
                        graphic.Name = datebegin.ToString("yyyy");
                        datebegin = new DateTime(datebegin.Year, 1, 1, 0, 0, 0);
                        break;
                }
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                graphic.List = new List<ItemGraphicDTO>();

                var policies = _mainRepository.FindAll(filter, (p => p.Broker));

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Count New", Value = policies.Where(x=>x.Type == PolicyType.New).Count() };
                graphic.List.Add(itemgraphic);

                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Count Renew", Value = policies.Where(x => x.Type == PolicyType.Renewal).Count() };
                graphic.List.Add(itemgraphic2);

                ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Total", Value = policies.Sum(x => x.TotalPremium) };
                graphic.List.Add(itemgraphic3);

                return graphic;
            });
        }
        public GraphicDTO GetTotalGeneral(Filters<Policy> filter)
        {
            return TryCatch(() =>
            {
                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                var policies = _mainRepository.FindAll(filter, (p => p.Broker));

                int totalpolicies = policies.Count();
                decimal totalpremium = 0;
                decimal averagepolicies = 0;
                if (totalpolicies > 0)
                {
                    totalpremium = policies.Sum(x => x.TotalPremium);
                    averagepolicies = totalpremium / totalpolicies;
                }

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Total Policies", Value = totalpolicies };
                graphic.List.Add(itemgraphic);
                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Average Premium", Value = averagepolicies };
                graphic.List.Add(itemgraphic2);
                ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Gross Written Premium", Value = totalpremium };
                graphic.List.Add(itemgraphic3);

                return graphic;
            });
        }
        public GraphicDTO GetRenewalsTotal(Filters<Policy> filter)
        {
            return TryCatch(() =>
            {
                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                var policies = _mainRepository.FindAll(filter, (p => p.Broker));

                int policiestimerenewals = policies.Where(x => x.PeriodRenewal == PolicyPeriod.Renewal).Count();
                int policiesexpireds = policies.Where(x => x.Status == PolicyStatus.Expired).Count();
                int policiesrenew = policies.Where(x => x.Type == PolicyType.Renewal).Count();
                
                decimal percents = policiesrenew == 0 ? 0 : (policiesexpireds / policiesrenew) * 100;

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Renewals", Value = policiestimerenewals };
                graphic.List.Add(itemgraphic);
                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Renewal Ratio", Value = percents };
                graphic.List.Add(itemgraphic2);

                return graphic;
            });
        }
        public GraphicDTO CountInEffect(long? brokerageId = null)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                if(brokerageId != null)
                    filters.Add(x => x.Broker.BrokerageId == brokerageId);
               IEnumerable<Policy> policies = _mainRepository.FindAll(filters, p => p.Broker).ToList();
               policies = policies.Where(p => p.Status == PolicyStatus.InEffect);

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                foreach (PolicyType policyType in Enum.GetValues(typeof(PolicyType)))
                {
                    graphic.List.Add(new ItemGraphicDTO()
                    {
                        Key = policyType.Description(),
                        Value = policies.Count(x => x.Type == policyType)
                    });
                };

                return graphic;
            });
        }
        public GraphicDTO GetTotalLastYear(Filters<Policy> filter)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                DateTime dateaux = datebegin;

                var policies = _mainRepository.FindAll(filter, x => x.Broker);

                List<DateTime> result = new List<DateTime>();
                int numberMonth = 12;
                //iterate until the difference is two months 
                while (numberMonth > 0)
                {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM/yy"), Value = policies.Where(x => x.CreatedDate.Month == dateaux.Month && x.CreatedDate.Year == dateaux.Year).Count() };
                    graphic.List.Add(itemgraphic);

                    dateaux = dateaux.AddMonths(1);
                    numberMonth--;
                }

                return graphic;
            });
        }
        public PagedResult<OutputPolicyItemDTO> FindAll(int page, int pageSize, string sort, Filters<Policy> filters = null)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputPolicyItemDTO>(page, pageSize, sort, filters,
                     p => p.Order,
                     p => p.Order.Quotation,
                     p => p.Broker,
                     p => p.Broker.Brokerage,
                     p => p.Claims,
                     p => p.Condition,
                     p => p.Coverages,
                     p => p.Condition.Product,
                     p => p.Insured,
                     p => p.Business,
                     p => p.Business.Address,
                     p => p.Insured.Addresses,
                     p => p.Insured.Addresses.Select(s=>s.Region),
                     p => p.Insured.Addresses.Select(s => s.Region.City),
                     p => p.Insured.Addresses.Select(s => s.Region.City.State));
            });

        }              
        public OutputPolicyDTO RequestCancel(long policyId, long reasonId, long requesterId)
        {
            return TryCatch(() =>
            {
                var policy = _mainRepository.TryGet(policyId, p=>p.Endorsements);
                policy.Endorsements = policy.Endorsements ?? new List<Endorsement>();
                policy.Endorsements.Add(new Endorsement(EndorsementType.Cancellation, reasonId, requesterId));

                _validationManager.Validate(policy);
                _mainRepository.Update(policy);
                Commit();
                return _mapper.Map<OutputPolicyDTO>(policy);
            });
        }
        public OutputPolicyDTO Cancel(long policyId)
        {
            return TryCatch(() =>
            {
                var policy = _mainRepository.TryGet(policyId);
                policy.Canceled = true;
                _validationManager.Validate(policy);
                _mainRepository.Update(policy);

                Commit();
                return _mapper.Map<OutputPolicyDTO>(policy);
            });
        }
        public IEnumerable<OutputCoverageDTO> GetCoverages(long policyId)
        {
            return TryCatch(() =>
            {
                var policy = _mainRepository.TryGet(policyId, x => x.Coverages);
                return _mapper.Map<IEnumerable<OutputCoverageDTO>>(policy.Coverages);
            });
        }
        public bool IsOwner(long insuredId, long policyId)
        {
            return TryCatch(() =>
            {
                var policy = _mainRepository.Find(p => p.Id == policyId && p.InsuredId == insuredId);
                return policy != null;
            });
        }
        public bool IsOwnBusiness(long brokerageId, long policyId)
        {
            return TryCatch(() =>
            {
                var policy = _mainRepository.Find(p => p.Id == policyId && p.Broker.BrokerageId == brokerageId, e => e.Broker);
                return policy != null;
            });
        }
        public IEnumerable<PolicyNumberDTO> FindAll(long insuredId, long? brokerageId = null)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                if (brokerageId != null)
                    filters.Add(p => p.Broker.BrokerageId == brokerageId);
                filters.Add(i => i.InsuredId == insuredId);

                var policies = _mainRepository.FindAll(filters,
                    i => i.Broker);
                policies.ToList().Where(p => p.Status == PolicyStatus.InEffect);
                return _mapper.Map<IEnumerable<PolicyNumberDTO>>(policies);
            });
        }
        public MemoryStream GetPolicyFile(Policy policy, string token)
        {
            return TryCatch(() =>
            {
                ThrowArgumentRequiredIfNull(policy);
                if (policy.FileToken() != token)
                {
                    throw new Exceptions.ArgumentInvalidException(e => e.EL043);
                }

                if (!RegenPolicyFile(policy.Id))
                {
                    throw new Exceptions.InvalidException(e => e.EL055);
                }

                return _blobService.DownloadStream(BlobType.Policy, policy.PolicyPath);
            });
        }
        private bool RegenPolicyFile(long policyId)
        {
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    if (_acordService.RegenPdf(policyId))
                        return true;
                }
            }
            catch
            {
            }
            return false;
        }
    }
}
