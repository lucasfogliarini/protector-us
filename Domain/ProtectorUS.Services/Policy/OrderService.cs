﻿namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Model.Services;
    using Diagnostics.Logging;
    using Model.Validation;
    using AutoMapper;
    using Model;
    using DTO;
    using Integrations.Stripe;
    using System;
    using Exceptions;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Communication;
    using System.IO;
    using System.Linq;
    public class OrderService : BaseService<Order>, IOrderService
    {
        private readonly ILocalizationService _localizationService;
        private readonly IBlobService _blobService;
        private readonly IPdfReplacerService _pdfReplacerService;
        private readonly IQuotationService _quotationService;
        private readonly IPolicyRepository _policyRepository;
        private readonly IInsuredService _insuredService;
        private readonly IBrokerageService _brokerageService;
        private readonly IQuotationRepository _quotationRepository;
        private readonly IStripeApi _stripeApi;
        private readonly IProductConditionRepository _productConditionRepository;
        private readonly ICoverageRepository _coverageRepository;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IAcordService _acordService;
        private readonly IInsuredRepository _insuredRepository;

        public OrderService(IUnitOfWork unitOfWork,
           IPdfReplacerService pdfReplacerService,
           IBlobService blobService,
           IOrderRepository orderRepository,
           IProductConditionRepository productConditionRepository,
           IPolicyRepository policyRepository,
           IQuotationRepository quotationRepository,
           IStripeApi stripeApi,
           ILocalizationService localizationService,
           IEmailTemplateService emailTemplateService,
           IInsuredRepository insuredRepository,
           ICoverageRepository coverageRepository,
           IInsuredService insuredService,
           IBrokerageService brokerageService,
           IQuotationService quotationService,
           IAcordService acordService,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                orderRepository)
        {
            _policyRepository = policyRepository;
            _blobService = blobService;
            _pdfReplacerService = pdfReplacerService;
            _quotationRepository = quotationRepository;
            _stripeApi = stripeApi;
            _localizationService = localizationService;
            _quotationService = quotationService;
            _productConditionRepository = productConditionRepository;
            _coverageRepository = coverageRepository;
            _insuredService = insuredService;
            _brokerageService = brokerageService;
            _acordService = acordService;
            _emailTemplateService = emailTemplateService;
            _insuredRepository = insuredRepository;
        }

        public void GenerateCertificate(Order order)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string certificateBaseLocalPath = Path.Combine(baseDirectory, "policy_certificates", "certificate_base.pdf");
            string policyCertificateLocalPath = Path.Combine(baseDirectory, order.Policy.CertificatePath);
            try
            {
                var pdfValues = new Dictionary<string, string>();
                pdfValues.Add("BusinessName", order.Policy.Business.Name);
                pdfValues.Add("BusinessAddress", $"{order.Policy.Business.Address.StreetLine1} - {order.Policy.Business.Address.StreetLine2}");
                pdfValues.Add("BusinessLocation", $"{order.Policy.Business.Address.Region.City.Name} - {order.Policy.Business.Address.Region.City.State.Name} - {order.Policy.Business.Address.Region.ZipCode}");
                pdfValues.Add("InsurerName", order.Policy.Condition.State?.Carrier?.Name);
                pdfValues.Add("InsurerNAIC", "");
                pdfValues.Add("PolicyholderName", $"{order.Policy.Insured.FirstName}- {order.Policy.Insured.LastName}");
                pdfValues.Add("PolicyholderEmail", order.Policy.Insured.Email);
                pdfValues.Add("BrokerName", $"{order.Policy.Broker.FirstName}- {order.Policy.Broker.LastName}");
                pdfValues.Add("BrokerAddress", $"{order.Policy.Broker.Brokerage.Address.StreetLine1} - {order.Policy.Broker.Brokerage.Address.StreetLine2}");
                pdfValues.Add("BrokerLocation", $"{order.Policy.Broker.Brokerage.Address.Region.City.Name} - {order.Policy.Broker.Brokerage.Address.Region.City.State.Name} - {order.Policy.Broker.Brokerage.Address.Region.ZipCode}");
                pdfValues.Add("ProductName", order.Policy.Condition.Product.Name);
                pdfValues.Add("PolicyNumberAndRenovation", order.Policy.Number);
                pdfValues.Add("TotalPremium", order.Policy.TotalPremium.ToString());
                pdfValues.Add("PolicyStart", order.Policy.PeriodStart.ToString(Policy.dateFormat));
                pdfValues.Add("PolicyEnd", order.Policy.PeriodEnd.ToString(Policy.dateFormat));
                pdfValues.Add("LiabilityClaim", order.Policy.CoverageEachClaim.ToString());
                pdfValues.Add("LiabilityAggregate", order.Policy.CoverageAggregate.ToString());
                pdfValues.Add("DeductibleClaim", order.Policy.DeductibleEachClaim.ToString());
                pdfValues.Add("DeductibleAggregate", order.Policy.DeductibleAggregate.ToString());

                _pdfReplacerService.ReplacePdfForm(certificateBaseLocalPath, policyCertificateLocalPath, pdfValues);

                byte[] buffer = File.ReadAllBytes(policyCertificateLocalPath);
                _blobService.UploadAsync(BlobType.PolicyCertificate, buffer, order.Policy.CertificateFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                File.Delete(policyCertificateLocalPath);
            }
        }

        public PolicyNumber GetPolicyNumber(int policyEdition, Product product)
        {
            PolicyNumber policyNumber = new PolicyNumber(policyEdition, product.Code);
            if (policyNumber.Type == PolicyType.New)
            {
                int? lastSequentialNumber = _policyRepository.GetLastSequentialNumber(product.Id);
                policyNumber.SequentialNumber = PolicyNumber.NextSequentialNumber(lastSequentialNumber);
            }
            return policyNumber;
        }

        public OutputOrderDTO Create(InputOrderDTO orderIn)
        {
            return TryCatch(() =>
            {
                var quotation = SetQuotationDetails(orderIn.QuotationHash, orderIn.Business, orderIn.Phones);
                var insured = quotation.Insured;

                #region policy
                long productId = quotation.BrokerageProduct.ProductId;
                long stateId = quotation.Region.City.StateId;

                ProductCondition productCondition = _productConditionRepository.TryFind(x =>
                                                    x.ProductId == productId && 
                                                    x.StateId == stateId 
                                                    && x.Active,
                                                    x=>x.Product);//for get Product.Code
                int policyEdition = 0;//always for while
                PolicyNumber policyNumber = GetPolicyNumber(policyEdition, productCondition.Product);                

                RiskAnalysis riskAnalisys = quotation.DeserializeRiskAnalysis();

                decimal coverage = riskAnalisys.GetFieldValue<decimal>(RatingPlanComputer.coverageField);
                decimal deductible = riskAnalisys.GetFieldValue<decimal>(RatingPlanComputer.deductibleField);
                var policy = new Policy(policyNumber, quotation.InsuredTimezone)
                {
                    Condition = productCondition,
                    Insured = quotation.Insured,
                    BrokerId = quotation.BrokerId,
                    Business = quotation.Business,
                    TotalPremium = quotation.PolicyPremium,
                    PeriodStart = quotation.PolicyStartDate,
                    CoverageEachClaim = coverage,
                    CoverageAggregate = coverage,
                    DeductibleEachClaim = deductible,
                    DeductibleAggregate = deductible,
                    PeriodEnd = quotation.PolicyStartDate.AddYears(1)
                };
                #endregion

                #region order

                Brokerage brokerage = quotation.BrokerageProduct.Brokerage;
                var webPageProductDefault = quotation.BrokerageProduct.WebpageProducts.FirstOrDefault(wp => wp.Default);

                Order order = _mapper.Map<Order>(orderIn);
                order.OrderDate = DateTime.Now;
                CreditCardBrand creditCardBrand = CreditCardInfo.CreditCardBrand(orderIn.CreditCard.Number);
                order.PaymentMethodId = (int)creditCardBrand;
                order.Quotation = quotation;
                order.BrokerageFeePercentage = webPageProductDefault.BrokerageComission;
                order.Rounding = quotation.Rounding;
                order.Discount = 0;
                order.SurchargeAmount = quotation.SurchargeAmount;
                order.BrokerageFee = policy.TotalPremium * order.BrokerageFeePercentage / 100;
                order.ArgoFee = policy.TotalPremium - order.BrokerageFee;
                order.TotalCharged = order.ArgoFee + order.BrokerageFee + order.SurchargeAmount;

                #endregion

                #region validations

                _validationManager.Validate(insured);
                _validationManager.Validate(quotation);
                _validationManager.Validate(policy);
                _validationManager.Validate(order);
                if (creditCardBrand == CreditCardBrand.Invalid)
                {
                    _validationManager.AddError(new MessageCode(x => x.ES023, orderIn.CreditCard.Number).Message, typeof(CreditCardInfo).Name);
                }

                _validationManager.ThrowErrors();
                #endregion

                ChargeAccount chargeAccount = _stripeApi.GetManagedAccount(brokerage.ChargeAccountId);
                if (chargeAccount == null || !chargeAccount.CanCharge)
                {
                    throw new ArgumentInvalidException(e => e.EL048, brokerage.ChargeAccountId);
                }
                Charge(order, chargeAccount, orderIn.CreditCard, policy);

                if (order.PaymentStatus == PaymentStatus.Approved)
                {
                    IEnumerable<Coverage> coverages = _coverageRepository.FindAll(productCondition.ProductId);
                    policy.Coverages = new List<Coverage>(coverages);
                    order.Policy = policy;
                    quotation.Status = ActivationStatus.Activated;
                }

                _mainRepository.Add(order);
                Commit();

                if (order.PaymentStatus == PaymentStatus.Approved)
                {
                    #region get order
                    order = _mainRepository.TryGet(order.Id,
                        o => o.Quotation,
                        o => o.Quotation.Region,
                        o => o.Quotation.Region.City,
                        o => o.Quotation.Region.City.State,
                        o => o.Quotation.Surcharges,
                        o => o.Quotation.Surcharges.Select(e=>e.Surcharge),
                        o => o.Policy,
                        o => o.Policy.Business,
                        o => o.Policy.Business.Address,
                        o => o.Policy.Business.Address.Region,
                        o => o.Policy.Business.Address.Region.City,
                        o => o.Policy.Business.Address.Region.City.State,
                        o => o.Policy.Condition.Product,
                        o => o.Policy.Condition.State,
                        o => o.Policy.Condition.State.Carrier,
                        o => o.Policy.Condition.State.Carrier.Address,
                        o => o.Policy.Condition.State.Carrier.Address.Region,
                        o => o.Policy.Condition.State.Carrier.Address.Region.City,
                        o => o.Policy.Condition.State.Carrier.Address.Region.City.State,
                        o => o.Policy.Insured,
                        o => o.Policy.Broker,
                        o => o.Policy.Broker.Brokerage,
                        o => o.Policy.Broker.Brokerage.Address,
                        o => o.Policy.Broker.Brokerage.Address.Region,
                        o => o.Policy.Broker.Brokerage.Address.Region.City,
                        o => o.Policy.Broker.Brokerage.Address.Region.City.State);
                    #endregion
                    
                    #region send email
                    string body = _emailTemplateService.GetActivationBody(order);
                    _emailTemplateService.Send(EmailTemplate.Activation.Id, quotation.Insured.Email, body);

                    body = _emailTemplateService.GetWelcomeInsuredBody(order);
                    _emailTemplateService.Send(EmailTemplate.WelcomeInsured.Id, quotation.Insured.Email, body);

                    #endregion

                    GenerateCertificate(order);
                    _acordService.GenerateAcordXML(order);
                }
                else
                {
                    throw new InvalidException(e => e.EL045);
                }
                
                return _mapper.Map<OutputOrderDTO>(order);
            });
        }
        public PagedResult<OutputOrderDTO> FindAll(long policyId, int page, int pageSize, string sort)
        {
            return TryCatch(() =>
            {
                Filters<Order> filters = new Filters<Order>();
                filters.Add(x => x.Policy?.Id == policyId);
                return  FindPaginated<OutputOrderDTO>(page, pageSize, sort, filters);
            });
        }
        private Quotation SetQuotationDetails(Guid quotationHash, InputBusinessDTO businessIn, List<PhoneBaseDTO> phones)
        {

            Quotation quotation = _quotationRepository.TryFind(q => q.Hash == quotationHash, 
                                                                q => q.Broker,
                                                                q => q.Region.City,
                                                                q => q.Broker.Brokerage,
                                                                q => q.BrokerageProduct,
                                                                q => q.BrokerageProduct.WebpageProducts);

            if (quotation.IsDeclined)
            {
                throw new BadRequestException(e => e.EL044);
            }
            quotation.Business = _mapper.Map<Business>(businessIn);
            quotation.Business.Address.RegionId = quotation.RegionId;

            bool isUsed = _insuredService.IsUsed(quotation.ProponentEmail);
            if (isUsed)
            {
                _validationManager.AddExistingUserError<Insured>(quotation.ProponentEmail);
            }
            else
            {
                quotation.Insured = new Insured(quotation.ProponentEmail)
                {
                    Status = UserStatus.Onboarding,
                    FirstName = quotation.ProponentFirstName,
                    LastName = quotation.ProponentLastName,
                    SocialTitle = quotation.ProponentSocialTitle,
                    Phones = _mapper.Map<List<Phone>>(phones),
                    Profiles = new List<Model.Profile>()
                    {
                        _insuredService.TryGetProfile(Model.Profile.Ids.Insured)
                    }
                };
            }           

            return quotation;
        }
        private void Charge(Order order, ChargeAccount chargeAccount, CreditCardInfoDTO creditCardIn, Policy policy)
        {
            CreditCardInfo creditCard = _mapper.Map<CreditCardInfo>(creditCardIn);
            var region = _localizationService.GetRegion(creditCard.ZipCode);
            creditCard.City = region.City.Name;
            creditCard.State = region.City.State.Name;
            creditCard.Country = region.City.State.Country.Name;

            decimal applicationFee = order.ArgoFee + order.SurchargeAmount;
            PolicyNumber policyNumber = policy.GetPolicyNumber();
            ChargeTransactionParameters chargeParameters = new ChargeTransactionParameters()
            {
                Amount = Convert.ToInt32(order.TotalCharged * 100),
                ApplicationFee = Convert.ToInt32(applicationFee * 100),
                CreditCard = creditCard,
                Description = policy.Number,
                ChargeAccount = chargeAccount,
                MetaData = new Dictionary<string, string>()
                {
                    { "policy_number", policyNumber.NumberSimplified },
                    { "policy_edition", policyNumber.Edition.ToString() },
                    { "premium_amount", order.ArgoFee.ToString("n") },
                    { "tax_amount", order.Quotation.TaxAmount.ToString() },
                    { "fee_amount", order.Quotation.FeeAmount.ToString() }
                }
            };
            ChargeTransaction chargeTransaction = _stripeApi.Charge(chargeParameters);
            order.PaymentStatus = chargeTransaction.PaymentStatus;
            order.TransactionCode = chargeTransaction.TransactionCode;
            order.StripeFee = chargeTransaction.StripeFee;
        }
        public async Task<PagedResult<OutputOrderDTO>> FindAllAsync(long policyId, int page, int pageSize, string sort)
        {
            return await TryCatch(async() =>
            {
                Filters<Order> filters = new Filters<Order>();
                filters.Add(x => x.Id == policyId);
                return await FindPaginatedAsync<OutputOrderDTO>(page, pageSize, sort, filters);
            });
        }
    }
}