﻿using System.Collections.Generic;
using System.Text;
namespace ProtectorUS.Services
{
    using Model.Services;
    using Communication;
    using Diagnostics.Logging;
    using Model.Repositories;
    using Model;
    using Data.AzureStorage;
    using System.Net.Mail;
    using System;
    using System.IO;
    public class EmailTemplateService : BaseService, IEmailTemplateService
    {
        readonly IEmailTemplateRepository _emailTemplateRepository;
        readonly SendGridMailer _sendGridMailer;
        readonly IBlobService _blobService;
        readonly static Uri azureBlobEndPoint = AzureBlob.GetEndpoint();
        readonly static string publicContainer = $"{azureBlobEndPoint.AbsoluteUri}public";
        readonly string emailFolderPath = $"{publicContainer}/email";
        readonly string protectorDomain = ProtectorDomain.Domain();
        public EmailTemplateService(ILogger logger, 
            IEmailTemplateRepository emailTemplateRepository,
            IBlobService blobService) : 
            base(logger)
        {
            _emailTemplateRepository = emailTemplateRepository;
            _sendGridMailer = new SendGridMailer();
            _blobService = blobService;
        }
        public string GetForgotPasswordBody(User user, string token, ProtectorApp protectorApp)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.ForgotPassword.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.ForgotPassword.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.ForgotPassword.Id}");

            dictionary.Add(EmailTemplate.ForgotPassword.user_firstname, user.FirstName);
            dictionary.Add(EmailTemplate.ForgotPassword.protector_app, protectorApp.ToAppString());
            dictionary.Add(EmailTemplate.ForgotPassword.user_lastname, user.LastName);
            dictionary.Add(EmailTemplate.ForgotPassword.user_id, user.Id.ToString());

            dictionary.Add(EmailTemplate.ForgotPassword.token, token);

            return BuildBodyHtml(EmailTemplate.ForgotPassword.Id, dictionary);
        }

        private string GetEmailMarketingBody(string templateId, EmailMarketing emailMarketing, Product product, Brokerage brokerage)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.EmailMarketingCampaign.accountantsId}");

            string callToActionLink = ProtectorUS.ProtectorDomain.BrokerageProductPath(brokerage.Alias, product.Alias);
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.cover_path, $"{publicContainer}/{emailMarketing.CoverPath}");
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.product_name, product.Name);
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.call_to_action_link, callToActionLink);
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.button_text, emailMarketing.ButtonText);
            dictionary.Add(EmailTemplate.EmailMarketingCampaign.body_text, emailMarketing.Body);

            return BuildBodyHtml(templateId, dictionary);
        }
        public string GetEmailMarketingAccountantsBody(EmailMarketing emailMarketing, Product product, Brokerage brokerage)
        {
            return GetEmailMarketingBody(EmailTemplate.EmailMarketingCampaign.accountantsId, emailMarketing, product, brokerage);
        }

        #region Quotation and Activation
        public string GetQuotationBody(Quotation quotation)
        {
            var brokerage = quotation.Broker.Brokerage;
            string productFrontAlias = Product.GetProductAlias(quotation.BrokerageProduct.ProductId);
            string brokerageProductUrl = ProtectorUS.ProtectorDomain.BrokerageProductUrl(brokerage.Alias, productFrontAlias);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.Quotation.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.Quotation.brokerage_alias, brokerage.Alias);
            dictionary.Add(EmailTemplate.Quotation.brokerage_product_url, brokerageProductUrl);
            dictionary.Add(EmailTemplate.Quotation.brokerage_logo_path, $"{publicContainer}/{brokerage.Logo}");
            dictionary.Add(EmailTemplate.Quotation.brokerage_name, brokerage.Name);
            dictionary.Add(EmailTemplate.Quotation.brokerage_street, $"{brokerage.Address.StreetFull}");
            dictionary.Add(EmailTemplate.Quotation.brokerage_cityname, brokerage.Address.Region.City.Name);
            dictionary.Add(EmailTemplate.Quotation.brokerage_stateabbr, brokerage.Address.Region.City.State.Abbreviation);
            dictionary.Add(EmailTemplate.Quotation.brokerage_zipcode, brokerage.Address.Region.ZipCode);
            dictionary.Add(EmailTemplate.Quotation.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.Quotation.Id}");
            dictionary.Add(EmailTemplate.Quotation.proponent_firstname, quotation.ProponentFirstName);
            dictionary.Add(EmailTemplate.Quotation.proponent_lastname, quotation.ProponentLastName);
            dictionary.Add(EmailTemplate.Quotation.product_name, quotation.BrokerageProduct.Product.Name);
            dictionary.Add(EmailTemplate.Quotation.quote_date, quotation.CreatedDate.ToString(EmailTemplate.DateFormat));
            dictionary.Add(EmailTemplate.Quotation.coverage, quotation.DeserializeRiskAnalysis().GetFieldValue<decimal>(RatingPlanComputer.coverageField).ToString("n"));
            dictionary.Add(EmailTemplate.Quotation.deductible, quotation.DeserializeRiskAnalysis().GetFieldValue<decimal>(RatingPlanComputer.deductibleField).ToString("n"));
            dictionary.Add(EmailTemplate.Quotation.business_cityname, quotation.Region.City.Name);
            dictionary.Add(EmailTemplate.Quotation.business_stateabbr, quotation.Region.City.State.Abbreviation);
            dictionary.Add(EmailTemplate.Quotation.business_zipcode, quotation.Region.ZipCode);
            dictionary.Add(EmailTemplate.Quotation.total_premium, quotation.PolicyPremium.ToString("n"));
            dictionary.Add(EmailTemplate.Quotation.quote_hash, quotation.Hash.ToString());
            dictionary.Add(EmailTemplate.Quotation.product_alias, quotation.BrokerageProduct.Product.Alias);
            return BuildBodyHtml(EmailTemplate.Quotation.Id, dictionary);
        }
        public string GetContactBrokerBody(Quotation quotation, string phone = "")
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.ContactBroker.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.ContactBroker.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.ContactBroker.Id}");
            dictionary.Add(EmailTemplate.ContactBroker.broker_firstname, quotation.Broker.FirstName);
            dictionary.Add(EmailTemplate.ContactBroker.broker_lastname, quotation.Broker.LastName);
            dictionary.Add(EmailTemplate.ContactBroker.proponent_firstname, quotation.ProponentFirstName);
            dictionary.Add(EmailTemplate.ContactBroker.proponent_lastname, quotation.ProponentLastName);
            dictionary.Add(EmailTemplate.ContactBroker.quote_date, quotation.CreatedDate.ToString("MMM dd, yyyy"));
            dictionary.Add(EmailTemplate.ContactBroker.proponent_phone, phone);
            dictionary.Add(EmailTemplate.ContactBroker.proponent_email, quotation.ProponentEmail);
            dictionary.Add(EmailTemplate.ContactBroker.product_name, quotation.BrokerageProduct.Product.Name);
            return BuildBodyHtml(EmailTemplate.ContactBroker.Id, dictionary);
        }
        public string GetActivationBody(Order order)
        {
            string productFrontAlias = Product.GetProductAlias(order.Quotation.BrokerageProduct.ProductId);
            var brokerage = order.Policy.Broker.Brokerage;
            //refact plz
            string host = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split("api")[0];
            string token = order.Policy.FileToken();
            string policyPath = Path.Combine(host, "api/policies", order.Policy.Id.ToString(), $"file?token={token}");

            string brokerageProductUrl = ProtectorUS.ProtectorDomain.BrokerageProductUrl(brokerage.Alias, productFrontAlias);

            Dictionary <string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.Activation.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.Activation.brokerage_product_url, brokerageProductUrl);
            dictionary.Add(EmailTemplate.Activation.brokerage_logo_path, $"{publicContainer}/{brokerage.Logo}");
            dictionary.Add(EmailTemplate.Activation.brokerage_name, brokerage.Name);
            dictionary.Add(EmailTemplate.Activation.brokerage_street, $"{brokerage.Address.StreetFull}");
            dictionary.Add(EmailTemplate.Activation.brokerage_cityname, brokerage.Address.Region.City.Name);
            dictionary.Add(EmailTemplate.Activation.brokerage_stateabbr, brokerage.Address.Region.City.State.Abbreviation);
            dictionary.Add(EmailTemplate.Activation.brokerage_zipcode, brokerage.Address.Region.ZipCode);
            dictionary.Add(EmailTemplate.Activation.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.Activation.Id}");
            dictionary.Add(EmailTemplate.Activation.insured_firstname, order.Policy.Insured.FirstName);
            dictionary.Add(EmailTemplate.Activation.insured_lastname, order.Policy.Insured.LastName);
            dictionary.Add(EmailTemplate.Activation.product_name, order.Policy.Condition.Product.Name);
            dictionary.Add(EmailTemplate.Activation.policy_number, order.Policy.Number);
            dictionary.Add(EmailTemplate.Activation.coverage, order.Policy.CoverageEachClaim.ToString());
            dictionary.Add(EmailTemplate.Activation.deductible, order.Policy.DeductibleEachClaim.ToString());
            dictionary.Add(EmailTemplate.Activation.policy_start, order.Policy.PeriodStart.ToString(EmailTemplate.DateFormat));
            dictionary.Add(EmailTemplate.Activation.policy_end, order.Policy.PeriodEnd.ToString(EmailTemplate.DateFormat));
            dictionary.Add(EmailTemplate.Activation.business_address, $"{order.Policy.Business.Address.StreetFull}");
            dictionary.Add(EmailTemplate.Activation.business_cityname, order.Quotation.Region.City.Name);
            dictionary.Add(EmailTemplate.Activation.business_stateabbr, order.Quotation.Region.City.State.Abbreviation);
            dictionary.Add(EmailTemplate.Activation.business_zipcode, order.Quotation.Region.ZipCode);
            dictionary.Add(EmailTemplate.Activation.policy_path, policyPath);
            dictionary.Add(EmailTemplate.Activation.total_premium, order.Policy.TotalPremium.ToString("n"));
            return BuildBodyHtml(EmailTemplate.Activation.Id, dictionary);
        }
        #endregion

        #region Welcome Protector
        public string GetWelcomeInsuredBody(Order order)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.WelcomeInsured.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.WelcomeInsured.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.WelcomeInsured.Id}");
            dictionary.Add(EmailTemplate.WelcomeInsured.insured_firstname, order.Policy.Insured.FirstName);
            dictionary.Add(EmailTemplate.WelcomeInsured.insured_lastname, order.Policy.Insured.LastName);
            dictionary.Add(EmailTemplate.WelcomeInsured.insured_email, order.Policy.Insured.Email);
            dictionary.Add(EmailTemplate.WelcomeInsured.insured_password, order.Policy.Insured.PasswordPlain);
            return BuildBodyHtml(EmailTemplate.WelcomeInsured.Id, dictionary);
        }
        public string GetWelcomeBrokerBody(Broker broker)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.WelcomeBroker.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.WelcomeBroker.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.WelcomeBroker.Id}");
            dictionary.Add(EmailTemplate.WelcomeBroker.brokerage_name, broker.Brokerage.Name);
            dictionary.Add(EmailTemplate.WelcomeBroker.broker_firstname, broker.FirstName);
            dictionary.Add(EmailTemplate.WelcomeBroker.broker_lastname, broker.LastName);
            dictionary.Add(EmailTemplate.WelcomeBroker.broker_email, broker.Email);
            dictionary.Add(EmailTemplate.WelcomeBroker.broker_password, broker.PasswordPlain);
            return BuildBodyHtml(EmailTemplate.WelcomeBroker.Id, dictionary);
        }
        public string GetWelcomeBrokerageBody(Broker broker)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.WelcomeBrokerage.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.WelcomeBrokerage.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.WelcomeBrokerage.Id}");
            dictionary.Add(EmailTemplate.WelcomeBrokerage.broker_email, broker.Email);
            dictionary.Add(EmailTemplate.WelcomeBrokerage.broker_password, broker.PasswordPlain);
            return BuildBodyHtml(EmailTemplate.WelcomeBrokerage.Id, dictionary);
        }
        public string GetWelcomeManagerUserBody(ManagerUser managerUser)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.WelcomeManagerUser.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.WelcomeManagerUser.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.WelcomeManagerUser.Id}");
            dictionary.Add(EmailTemplate.WelcomeManagerUser.manageruser_firstname, managerUser.FirstName);
            dictionary.Add(EmailTemplate.WelcomeManagerUser.manageruser_lastname, managerUser.LastName);
            dictionary.Add(EmailTemplate.WelcomeManagerUser.manageruser_email, managerUser.Email);
            dictionary.Add(EmailTemplate.WelcomeManagerUser.manageruser_password, managerUser.PasswordPlain);
            return BuildBodyHtml(EmailTemplate.WelcomeManagerUser.Id, dictionary);
        }
        #endregion

        #region claims
        private Dictionary<string, string> GetFnolClaimBody(Claim claim)
        {
            var policy = claim.Policy;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.FnolClaim.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.FnolClaim.insured_firstname, policy.Insured.FirstName);
            dictionary.Add(EmailTemplate.FnolClaim.insured_lastname, policy.Insured.LastName);
            dictionary.Add(EmailTemplate.FnolClaim.insured_business_name, policy.Business.Name);
            dictionary.Add(EmailTemplate.FnolClaim.policy_number, policy.Number);
            dictionary.Add(EmailTemplate.FnolClaim.product_name, policy.Condition.Product.Name);
            dictionary.Add(EmailTemplate.FnolClaim.claim_date, claim.ClaimDate.ToString(EmailTemplate.DateFormat));
            dictionary.Add(EmailTemplate.FnolClaim.claim_number, claim.Number);
            dictionary.Add(EmailTemplate.FnolClaim.claim_id, claim.Id.ToString());
            dictionary.Add(EmailTemplate.FnolClaim.brokerage_name, policy.Broker.Brokerage.Name);
            dictionary.Add(EmailTemplate.FnolClaim.broker_firstname, policy.Broker.FirstName);
            dictionary.Add(EmailTemplate.FnolClaim.broker_lastname, policy.Broker.LastName);
            return dictionary;
        }
        public string GetFnolClaimInsuredBody(Claim claim)
        {
            var policy = claim.Policy;
            Dictionary<string, string> dictionary = GetFnolClaimBody(claim);
            dictionary.Add(EmailTemplate.FnolClaim.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.FnolClaim.InsuredId}");
            dictionary.Add(EmailTemplate.FnolClaim.receiver_socialtitle, policy.Insured.SocialTitle?.Description());
            dictionary.Add(EmailTemplate.FnolClaim.receiver_firstname, policy.Insured.FirstName);
            dictionary.Add(EmailTemplate.FnolClaim.receiver_lastname, policy.Insured.LastName);
            return BuildBodyHtml(EmailTemplate.FnolClaim.InsuredId, dictionary);
        }
        public string GetFnolClaimBrokerBody(Claim claim)
        {
            var policy = claim.Policy;
            Dictionary<string, string> dictionary = GetFnolClaimBody(claim);
            dictionary.Add(EmailTemplate.FnolClaim.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.FnolClaim.BrokerId}");
            dictionary.Add(EmailTemplate.FnolClaim.receiver_socialtitle, policy.Broker.SocialTitle?.Description());
            dictionary.Add(EmailTemplate.FnolClaim.receiver_firstname, policy.Broker.FirstName);
            dictionary.Add(EmailTemplate.FnolClaim.receiver_lastname, policy.Broker.LastName);
            return BuildBodyHtml(EmailTemplate.FnolClaim.BrokerId, dictionary);
        }        
        public string GetFnolClaimArgoBody(Claim claim)
        {
            var policy = claim.Policy;
            Dictionary<string, string> dictionary = GetFnolClaimBody(claim);
            dictionary.Add(EmailTemplate.FnolClaim.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.FnolClaim.ArgoId}");
            return BuildBodyHtml(EmailTemplate.FnolClaim.ArgoId, dictionary);
        }
        private Dictionary<string, string> GetClaimStatusBody(Claim claim)
        {
            var policy = claim.Policy;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add(EmailTemplate.ClaimStatus.protector_domain, protectorDomain);
            dictionary.Add(EmailTemplate.ClaimStatus.product_name, policy.Condition.Product.Name);
            dictionary.Add(EmailTemplate.ClaimStatus.claim_date, claim.ClaimDate.ToString(EmailTemplate.DateFormat));
            dictionary.Add(EmailTemplate.ClaimStatus.claim_number, claim.Number);
            dictionary.Add(EmailTemplate.ClaimStatus.claim_id, claim.Id.ToString());
            dictionary.Add(EmailTemplate.ClaimStatus.brokerage_name, policy.Broker.Brokerage.Name);
            return dictionary;
        }
        public string GetClaimStatusInsuredBody(Claim claim)
        {
            Dictionary<string, string> dictionary = GetClaimStatusBody(claim);
            dictionary.Add(EmailTemplate.ClaimStatus.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.ClaimStatus.InsuredId}");
            dictionary.Add(EmailTemplate.ClaimStatus.receiver_firstname, claim.Policy.Insured.FirstName);
            dictionary.Add(EmailTemplate.ClaimStatus.receiver_lastname, claim.Policy.Insured.LastName);
            return BuildBodyHtml(EmailTemplate.ClaimStatus.InsuredId, dictionary);
        }
        public string GetClaimStatusBrokerBody(Claim claim)
        {
            Dictionary<string, string> dictionary = GetClaimStatusBody(claim);
            dictionary.Add(EmailTemplate.ClaimStatus.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.ClaimStatus.BrokerId}");
            dictionary.Add(EmailTemplate.ClaimStatus.receiver_firstname, claim.Policy.Broker.FirstName);
            dictionary.Add(EmailTemplate.ClaimStatus.receiver_lastname, claim.Policy.Broker.LastName);
            return BuildBodyHtml(EmailTemplate.ClaimStatus.BrokerId, dictionary);
        }
        public string GetClaimStatusArgoBody(Claim claim)
        {
            Dictionary<string, string> dictionary = GetClaimStatusBody(claim);
            dictionary.Add(EmailTemplate.ClaimStatus.blob_template_folder, $"{emailFolderPath}/{EmailTemplate.ClaimStatus.ArgoId}");
            return BuildBodyHtml(EmailTemplate.ClaimStatus.ArgoId, dictionary);
        }
        #endregion

        private string BuildBodyHtml(string templateId, Dictionary<string, string> bodyVariables)
        {
            string blobText = _blobService.DownloadText(BlobType.EmailTemplate, $"{templateId}/index.html", true);
            StringBuilder body = new StringBuilder(blobText);
            if (bodyVariables != null)
            {
                foreach (var bodyVar in bodyVariables)
                {
                    body.Replace(bodyVar.Key, bodyVar.Value);
                }
            }
            return body.ToString();
        }

        public void Send(string templateId, string to, string body = null, string replyTo = null)
        {
            TryCatch(() =>
            {
                if (string.IsNullOrWhiteSpace(to))
                    return;

                EmailTemplate emailTemplate = _emailTemplateRepository.TryFind(et => et.TemplateId == templateId);
                MailMessage mailMessage = new MailMessage(emailTemplate.From, to, emailTemplate.Subject, body);
                mailMessage.From = new MailAddress(emailTemplate.From, emailTemplate.FromName);
                if(replyTo != null)
                    mailMessage.ReplyToList.Add(replyTo);
                mailMessage.IsBodyHtml = true;
                _sendGridMailer.Send(mailMessage);
            });
        }        
    }
}
