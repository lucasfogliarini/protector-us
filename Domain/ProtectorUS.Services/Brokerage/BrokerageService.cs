﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.Linq.Expressions;
    using Exceptions;
    using System.Threading.Tasks;
    using Integrations.Stripe;
    using System.Device.Location;
    public class BrokerageService : BaseService<Brokerage>, IBrokerageService
    {
        private readonly IWebpageProductRepository _webpageProductRepository;
        private readonly IClaimRepository _claimRepository;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IBrokerRepository _brokerRepository;
        private readonly IProfileRepository _profileRepository;
        private readonly ILocalizationService _localizationService;
        private readonly IStripeApi _stripeApi;
        private readonly IBrokerageProductRepository _brokerageProductRepository;

        public BrokerageService(
           IBrokerageRepository brokerageRepository,
           IBrokerageProductRepository brokerageProductRepository,
           IProfileRepository profileRepository,
           IWebpageProductRepository webpageProductRepository,
           IClaimRepository claimRepository,
           IBrokerRepository brokerRepository,
           ILocalizationService localizationService,
           IStripeApi stripeApi,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IEmailTemplateService emailTemplateService,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                brokerageRepository)
        {
            _webpageProductRepository = webpageProductRepository;
            _brokerageProductRepository = brokerageProductRepository;
            _claimRepository = claimRepository;
            _brokerRepository = brokerRepository;
            _profileRepository = profileRepository;
            _localizationService = localizationService;
            _stripeApi = stripeApi;
            _emailTemplateService = emailTemplateService;
        }

        public OutputBrokerageDTO Find(string alias, bool includeProducts = false)
        {
            return TryCatch(() =>
            {
                Expression<Func<Brokerage, object>>[] includes = null;
                if (includeProducts)
                {
                    includes = new Expression<Func<Brokerage, object>>[]
                    {
                        b => b.BrokerageProducts,
                        b => b.BrokerageProducts.Select(y => y.Product),
                        x => x.Address,
                        y => y.Address.Region,
                        m => m.Address.Region.City,
                        c => c.Address.Region.City.State
                    };
                }
                Brokerage brokerage = _mainRepository.TryFind(x => x.Alias == alias, includes);
                // brokerage.BrokerageProducts.RemoveAll(bp => bp.Product.Active == false || bp.Active == false);
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public async Task<bool> TestAliasAsync(string alias, long brokerageId)
        {
            return await TryCatch(async () =>
            {
                List<string> listblock = new List<string> { "accountants", "architects-and-engineers", "login", "manager", "my-protector", "onboarding", "quote-and-purchase", "protector" };

                if (listblock.Contains(alias))
                    return false;

                Brokerage brokerage = await _mainRepository.FindAsync(x => x.Alias == alias && x.Id != brokerageId);
                return brokerage == null;
            });
        }
        public bool TestAlias(string alias, long brokerageId)
        {
            return TryCatch(() =>
            {
                List<string> listblock = new List<string> { "accountants", "architects-and-engineers", "login", "manager", "my-protector", "onboarding", "quote-and-purchase", "protector" };

                if (listblock.Contains(alias))
                    return false;

                Brokerage brokerage = _mainRepository.Find(x => x.Alias == alias && x.Id != brokerageId);
                return brokerage == null;
            });
        }
        public async Task<OutputBrokerageMyProtectorDTO> FindMyProtectorAsync(string alias)
        {
            return await TryCatch(async () =>
            {
                Brokerage brokerage = await _mainRepository.TryFindAsync(x => x.Alias == alias,
                                        x => x.Address,
                                        x => x.Address.Region,
                                        x => x.Address.Region.City,
                                        x => x.Address.Region.City.State,
                                        x => x.Phones);
                return _mapper.Map<OutputBrokerageMyProtectorDTO>(brokerage);
            });
        }

        private void SetCoordinates(Address address)
        {
            address.Region = _localizationService.GetRegion(address.RegionId);
            GeoCoordinate coordinate = _localizationService.GetCoordinates(address.StreetLine1, address.Region.ZipCode);
            ThrowCoordinateRequiredIfNull(coordinate, address.Region.ZipCode);
            address.Latitude = coordinate.Latitude;
            address.Longitude = coordinate.Longitude;
        }
        public OutputBrokerageDTO Create(InputBrokerageDTO brokerageIn)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mapper.Map<Brokerage>(brokerageIn);
                SetCoordinates(brokerage.Address);
                brokerage.BrokerageProducts = new List<BrokerageProduct>();
                foreach (InputWebpageProductDTO webpageProduct in brokerageIn.WebpageProducts)
                {
                    var brokerageProduct = new BrokerageProduct();
                    brokerageProduct.CreateWebpage(webpageProduct.Active, webpageProduct.ProductId, webpageProduct.BrokerageComission);
                    brokerage.BrokerageProducts.Add(brokerageProduct);
                }
                brokerage.Alias = brokerage.Name.Simplify().Left(20);
                brokerage.Status = BrokerageStatus.Onboarding;

                Broker brokerMaster = new Broker(brokerage.Email)
                {
                    Status = UserStatus.Onboarding,
                    Profiles = new List<Profile>()
                    {
                        _profileRepository.TryGet(Profile.Ids.MasterBroker)
                    }
                };
                brokerage.Brokers = new List<Broker>()
                {
                    brokerMaster
                };

                bool isUsed = _brokerRepository.IsUsed(brokerMaster.Email);
                if (isUsed)
                    _validationManager.AddExistingUserError(brokerMaster);

                _validationManager.Validate(brokerMaster);
                _validationManager.Validate(brokerage);

                _validationManager.ThrowErrors();
                _mainRepository.Add(brokerage);
                Commit();

                string body = _emailTemplateService.GetWelcomeBrokerageBody(brokerMaster);
                _emailTemplateService.Send(EmailTemplate.WelcomeBrokerage.Id, brokerMaster.Email, body);

                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public OutputBrokerageDTO Update(long brokerageId, InputBrokerageDTO brokerageIn)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId, b => b.BrokerageProducts, b => b.BrokerageProducts.Select(bp => bp.WebpageProducts));
                brokerage.Name = brokerageIn.Name;
                brokerage.Email = brokerageIn.Email;
                brokerage.Website = brokerageIn.Website;

                foreach (InputWebpageProductDTO webpageProduct in brokerageIn.WebpageProducts)
                {
                    var brokerageProduct = brokerage.BrokerageProducts.FirstOrDefault(bp => bp.ProductId == webpageProduct.ProductId);
                    brokerageProduct.ActiveARGO = webpageProduct.Active;
                    brokerageProduct.WebpageProducts.FirstOrDefault(wp => wp.Default).BrokerageComission = webpageProduct.BrokerageComission;
                }

                if (brokerageIn.Address != null)
                {
                    brokerage.Address = _mapper.Map<Address>(brokerageIn.Address);
                    SetCoordinates(brokerage.Address);
                }
                var phone = brokerageIn.Phones?.FirstOrDefault();
                if (phone != null)
                {
                    brokerage.Phones = brokerage.Phones ?? new List<Phone>();
                    brokerage.Phones.Add(_mapper.Map<Phone>(phone));
                }
                _validationManager.Validate(brokerage);
                _mainRepository.Update(brokerage);

                Commit();
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public OutputBrokerageDTO Update(long brokerageId, InputBrokerageUpdateDTO brokerageIn)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId);
                brokerage.Alias = brokerageIn.Alias.Simplify();
                brokerage.Name = brokerageIn.Name;
                brokerage.Email = brokerageIn.Email;
                brokerage.Logo = brokerageIn.Logo;
                brokerage.Website = brokerageIn.Website;
                brokerage.Status = BrokerageStatus.Active;

                brokerage.Address = _mapper.Map<Address>(brokerageIn.Address);
                SetCoordinates(brokerage.Address);

                var phone = brokerageIn.Phones?.FirstOrDefault();
                if (phone != null)
                {
                    brokerage.Phones = brokerage.Phones ?? new List<Phone>();
                    brokerage.Phones.Add(_mapper.Map<Phone>(phone));
                }
                _validationManager.Validate(brokerage);
                _mainRepository.Update(brokerage);
                Commit();
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public OutputBrokerageDTO Delete(long brokerageId)
        {
            return TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId);
                _mainRepository.Remove(brokerage);
                Commit();
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public OutputBrokerageDTO Get(long brokerageId)
        {
            return TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId,
                        b => b.BrokerageGroup,
                        b => b.BrokerageProducts,
                        b => b.BrokerageProducts.Select(c => c.Product),
                        b => b.BrokerageProducts.Select(d => d.WebpageProducts),
                        b => b.Brokers,
                        b => b.Phones,
                        b => b.Address,
                        b => b.Address.Region,
                        b => b.Address.Region.City,
                        b => b.Address.Region.City.State);

                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public OutputBrokerageDTO GetWithOnlyArgoActiveProducts(long brokerageId)
        {
            return TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId,
                       b => b.BrokerageGroup,
                       b => b.BrokerageProducts,
                       b => b.BrokerageProducts.Select(c => c.Product),
                       b => b.Brokers,
                       b => b.Phones,
                       b => b.Address,
                       b => b.Address.Region,
                       b => b.Address.Region.City,
                       b => b.Address.Region.City.State);
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public MapBrokerageMyProtectorDTO FindABroker(string zipCode, string productAlias)
        {
            return TryCatch(() =>
            {
                Predicate<Brokerage> hasCoordinates = b => b.Address.Longitude != null && b.Address.Latitude != null;
                Predicate<Brokerage> hasActiveProduct = b => b.BrokerageProducts.Any(y => y.Active && y.Product.Alias == productAlias);
                Predicate<Brokerage> chargeAccountIsVerified = b => b.ChargeAccountStatus == ChargeAccountStatus.Verified;

                //find the region
                var brokerages = _mainRepository.FindAll(b => hasCoordinates(b) && hasActiveProduct(b) && chargeAccountIsVerified(b) &&
                                                        b.Address.Region.ZipCode == zipCode,
                                                        b => b.Address,
                                                        b => b.Address.Region,
                                                        b => b.BrokerageProducts,
                                                        b => b.BrokerageProducts.Select(bp => bp.Product));

                if (!brokerages.Any())
                {
                    var region = _localizationService.GetRegion(zipCode);
                    if (region != null)
                    {
                        //find the city
                        brokerages = _mainRepository.FindAll(b => hasCoordinates(b) && hasActiveProduct(b) && chargeAccountIsVerified(b) &&
                                                            b.Address.Region.CityId == region.City.Id,
                                                            b => b.Address,
                                                            b => b.Address.Region,
                                                            b => b.Address.Region.City,
                                                            b => b.BrokerageProducts,
                                                            b => b.BrokerageProducts.Select(bp => bp.Product));
                        if (!brokerages.Any())
                        {
                            //find the state
                            brokerages = _mainRepository.FindAll(b => hasCoordinates(b) && hasActiveProduct(b) && chargeAccountIsVerified(b) &&
                                                                    b.Address.Region.City.StateId == region.City.StateId,
                                                                    b => b.Address,
                                                                    b => b.Address.Region,
                                                                    b => b.Address.Region.City,
                                                                    b => b.BrokerageProducts,
                                                                    b => b.BrokerageProducts.Select(bp => bp.Product));
                            if (!brokerages.Any())
                            {
                                string[] stateAbbrs = State.GetRegionStates(zipCode);
                                //find the state region
                                brokerages = _mainRepository.FindAll(b => hasCoordinates(b) && hasActiveProduct(b) && chargeAccountIsVerified(b) &&
                                                                    stateAbbrs.Contains(b.Address.Region.City.State.Abbreviation),
                                                                    b => b.Address,
                                                                    b => b.Address.Region.City.State,
                                                                    b => b.BrokerageProducts,
                                                                    b => b.BrokerageProducts.Select(bp => bp.Product));
                                if (!brokerages.Any())
                                {
                                    //find all
                                    brokerages = _mainRepository.FindAll(b => hasCoordinates(b) && hasActiveProduct(b) && chargeAccountIsVerified(b),
                                                                        b => b.Address,
                                                                        b => b.BrokerageProducts,
                                                                        b => b.BrokerageProducts.Select(bp => bp.Product));
                                }
                            }
                        }
                    }
                }
                MapBrokerageMyProtectorDTO map = new MapBrokerageMyProtectorDTO();
                map.Pins = new List<PointBrokerageMyProtectorDTO>();
                foreach (var item in brokerages)
                {
                    PointBrokerageMyProtectorDTO pinitem = new PointBrokerageMyProtectorDTO()
                    {
                        Alias = item.Alias,
                        Name = item.Name,
                        Image = item.Logo,
                        Latitude = item.Address.Latitude.Value,
                        Longitude = item.Address.Longitude.Value
                    };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public MapPointDTO GetMapPoints(string zipCode = "", string aliasProduct = "")
        {
            return TryCatch(() =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();
                map.RequestInfo = "/brokerage/{alias}";

                var brokerages = _mainRepository.FindAll(f => (!string.IsNullOrEmpty(zipCode) ? f.Address.Region.ZipCode == zipCode : true) && (!string.IsNullOrEmpty(aliasProduct) ? f.BrokerageProducts.Where(y => y.Active && y.Product.Alias == aliasProduct).Count() > 0 : true), b => b.Address, b => b.Address.Region, b => b.BrokerageProducts, b => b.BrokerageProducts.Select(bp => bp.Product));

                foreach (var item in brokerages)
                {
                    PointDTO pinitem = new PointDTO() { Id = item.Id, Alias = item.Alias, Name = item.Name, Image = item.Logo, Latitude = item.Address.Latitude.Value, Longitude = item.Address.Longitude.Value };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public BrokerageDefaultWebPageDTO UpdateDefaultWebPage(long id, BrokerageDefaultWebPageDTO webpageIn)
        {
            return TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(id,
                    b => b.BrokerageGroup,
                    b => b.BrokerageProducts,
                    b => b.Brokers,
                    b => b.Phones,
                    b => b.Address
                );

                brokerage.Logo = webpageIn.Logo;
                brokerage.Cover = webpageIn.Cover;

                _validationManager.Validate(brokerage);
                _mainRepository.Update(brokerage);

                Commit();
                return _mapper.Map<BrokerageDefaultWebPageDTO>(brokerage);
            });
        }
        public IEnumerable<WebpageProductDTO> FindWebpages(long brokerageId)
        {
            return TryCatch(() =>
            {
                var webpages = _webpageProductRepository.FindAll(x => x.BrokerageProduct.BrokerageId == brokerageId,
                    b => b.BrokerageProduct,
                    b => b.BrokerageProduct.Product);

                return _mapper.Map<IEnumerable<WebpageProductDTO>>(webpages);
            });
        }
        public PagedResult<OutputBrokerageItemDTO> FindAll(int page, int pageSize, string sort, Filters<Brokerage> filters = null)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputBrokerageItemDTO>(page, pageSize, sort, filters,
                       b => b.Brokers,
                       b => b.Brokers.Select(e => e.Policies),
                       b => b.Address,
                       b => b.Address.Region,
                       b => b.Address.Region.City,
                       b => b.Address.Region.City.State);
            });
        }
        public OutputBrokerageOnboardDTO OnBoarding(long brokerageId)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId,
                    x => x.Address,
                    x => x.Address.Region,
                    x => x.Address.Region.City,
                    x => x.Address.Region.City.State,
                    x => x.Phones);

                if (brokerage.Status != BrokerageStatus.Onboarding)
                {
                    throw new ArgumentInvalidException(e => e.EL028);
                }
                return _mapper.Map<OutputBrokerageOnboardDTO>(brokerage);
            });
        }
        public IReadOnlyCollection<OutputBrokerageTopListDTO> GetTop(int quant)
        {
            return TryCatch(() =>
            {
                var returnDTO = new List<OutputBrokerageTopListDTO>();

                var brokerages = _mainRepository.GetAll(x => x.Address, x => x.Address.Region, x => x.Address.Region.City, x => x.Address.Region.City.State, x => x.Brokers, x => x.Brokers.Select(y => y.Policies));

                foreach (Brokerage brokerage in brokerages)
                {
                    returnDTO.Add(_mapper.Map<OutputBrokerageTopListDTO>(brokerage));
                }
                returnDTO = returnDTO.OrderByDescending(x => x.Total).Take(quant).ToList();

                return returnDTO;
            });
        }
        public GraphicDTO GetTotalStatus()
        {
            return TryCatch(() =>
            {
                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                var brokerage = _mainRepository.GetAll();

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = EnumExtensions.Description(BrokerageStatus.Active), Value = brokerage.Where(x => x.Status == BrokerageStatus.Active).Count() };
                graphic.List.Add(itemgraphic);

                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = EnumExtensions.Description(BrokerageStatus.Inactive), Value = brokerage.Where(x => x.Status == BrokerageStatus.Inactive).Count() };
                graphic.List.Add(itemgraphic2);

                return graphic;
            });
        }
        public IReadOnlyCollection<GraphicDTO> GetTotalLastYear()
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var brokerages = _mainRepository.FindAll(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                DateTime dateaux = datebegin;

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                int numberMonth = 12;
                while (numberMonth > 0)
                {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM/yy"), Value = brokerages.Where(x => x.CreatedDate.Month == dateaux.Month && x.CreatedDate.Year == dateaux.Year).Count() };
                    graphic.List.Add(itemgraphic);

                    dateaux = dateaux.AddMonths(1);
                    numberMonth--;
                }
                result.Add(graphic);

                return result;
            });
        }
        public void ToggleProduct(long brokerageId, ARGOToggleProductDTO productToggle)
        {
            TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId,
                    b => b.BrokerageProducts
                );

                BrokerageProduct brokerageproduct = brokerage.BrokerageProducts.FirstOrDefault(x => x.ProductId == productToggle.ProductId);
                ThrowNotFoundIfNull(brokerageproduct);
                brokerageproduct.ActiveARGO = productToggle.Active;
                _mainRepository.Update(brokerage);

                Commit();
            });
        }
        public void ToggleProduct(long brokerageId, BrokerageToggleProductDTO productToggle)
        {
            TryCatch(() =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId,
                    b => b.BrokerageProducts
                );

                BrokerageProduct brokerageproduct = brokerage.BrokerageProducts.FirstOrDefault(x => x.ProductId == productToggle.ProductId);
                ThrowNotFoundIfNull(brokerageproduct);
                brokerageproduct.ActiveBrokerage = productToggle.Active;
                _mainRepository.Update(brokerage);
                Commit();
            });
        }
        public ChargeAccount GetChargeAccount(long brokerageId)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId);
                var chargeAccount = _stripeApi.GetManagedAccount(brokerage.ChargeAccountId);
                if (chargeAccount == null)
                {
                    throw new ArgumentInvalidException(e => e.EL048, brokerage.ChargeAccountId);
                }
                return chargeAccount;
            });
        }
        public void CreateChargeAccount(long brokerageId, InputLegalEntityDTO legalEntityIn, BankAccount bankAccount, byte[] tosIp)
        {
            TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId);
                LegalEntity legalEntity = _mapper.Map<LegalEntity>(legalEntityIn);
                long? regionId = legalEntityIn?.Business?.RegionId;
                #region validation
                if (regionId == null)
                    _validationManager.AddError("The Business RegionId is required", "Business");
                else
                    legalEntity.Business.Address.Region = _localizationService.GetRegion(regionId.GetValueOrDefault());
                _validationManager.Validate(legalEntity);
                _validationManager.Validate(bankAccount);
                _validationManager.ThrowErrors();
                #endregion

                ChargeAccount chargeAccount = _stripeApi.GetManagedAccount(brokerage.ChargeAccountId);
                if (chargeAccount == null)
                {
                    chargeAccount = _stripeApi.CreateManagedAccount(legalEntity, bankAccount, tosIp);
                    brokerage.ChargeAccountId = chargeAccount.Id;
                    brokerage.ChargeAccountStatus = chargeAccount.Status;
                    _mainRepository.Update(brokerage);
                    Commit();
                }
                else
                {
                    UpdateChargeAccount(brokerage, legalEntity, bankAccount);
                }
            });
        }
        public void UpdateChargeAccount(long brokerageId, InputLegalEntityDTO legalEntityIn, BankAccount bankAccount, byte[] tosIp = null)
        {
            TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId);
                LegalEntity legalEntity = _mapper.Map<LegalEntity>(legalEntityIn);
                long? regionId = legalEntityIn?.Business?.RegionId;
                if (regionId != null)
                    legalEntity.Business.Address.Region = _localizationService.GetRegion(regionId.GetValueOrDefault());
                if (bankAccount != null)
                    _validationManager.ValidateAndThrowErrors(bankAccount);

                UpdateChargeAccount(brokerage, legalEntity, bankAccount, tosIp);
            });
        }
        private void UpdateChargeAccount(Brokerage brokerage, LegalEntity legalEntity, BankAccount bankAccount, byte[] tosIp = null)
        {
            ChargeAccount chargeAccount = _stripeApi.UpdateManagedAccount(brokerage.ChargeAccountId, legalEntity, bankAccount, tosIp);
            brokerage.ChargeAccountStatus = chargeAccount.Status;
            _mainRepository.Update(brokerage);
            Commit();
        }
        public bool CheckChargeAccountStatus(long brokerageId)
        {
            return TryCatch(() =>
            {
                return _mainRepository.TryGet(brokerageId).ChargeAccountStatus != ChargeAccountStatus.NoAccount;
            });
        }
        public void UpdateChargeAccountStatus(ChargeAccount chargeAccount)
        {
            TryCatch(() =>
            {
                ThrowArgumentRequiredIfNull(chargeAccount);
                var brokerage = _mainRepository.TryFind(b => b.ChargeAccountId == chargeAccount.Id);
                brokerage.ChargeAccountStatus = chargeAccount.Status;
                _mainRepository.Update(brokerage);
                Commit();
            });
        }

        public BrokerageProductEnableDisableOutputDTO EnableDisableProduct(long brokerageId, BrokerageProductEnableDisableInputDTO brokerageProductIn)
        {
            return TryCatch(() =>
            {
                var dtoReturn = new BrokerageProductEnableDisableOutputDTO();
                var brokerageProduct = _brokerageProductRepository.Find(x => x.BrokerageId == brokerageId && x.ProductId == brokerageProductIn.ProductId && x.Id == brokerageProductIn.BrokerageProductId);
                brokerageProduct.ActiveARGO = brokerageProductIn.Active;
                _brokerageProductRepository.Update(brokerageProduct);

                Commit();

                brokerageProduct = _brokerageProductRepository.Get(brokerageProductIn.BrokerageProductId);
                dtoReturn = _mapper.Map<BrokerageProductEnableDisableOutputDTO>(brokerageProduct);
                dtoReturn.BrokerageProductId = brokerageProduct.Id;
                return dtoReturn;
            });
        }

        #region Async
        public async Task<OutputBrokerageDTO> GetAsync(long brokerageId)
        {
            return await TryCatch(async () =>
            {
                var brokerage = await _mainRepository.TryGetAsync(brokerageId,
                       b => b.BrokerageGroup,
                       b => b.BrokerageProducts,
                       b => b.BrokerageProducts.Select(c => c.Product),
                       b => b.Brokers,
                       b => b.Phones,
                       b => b.Address,
                       b => b.Address.Region,
                       b => b.Address.Region.City,
                       b => b.Address.Region.City.State);

                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public async Task<PagedResult<OutputBrokerageItemDTO>> FindAllAsync(int page, int pageSize, string sort, Filters<Brokerage> filters)
        {
            return await TryCatch(async () =>
            {
                return await FindPaginatedAsync<OutputBrokerageItemDTO>(page, pageSize, sort, filters,
                       b => b.Brokers,
                       b => b.Brokers.Select(e => e.Policies),
                       b => b.Address,
                       b => b.Address.Region,
                       b => b.Address.Region.City,
                       b => b.Address.Region.City.State);
            });
        }
        public async Task<OutputBrokerageDTO> UpdateAsync(long brokerageId, InputBrokerageUpdateDTO brokerageIn)
        {
            return await TryCatch(async () =>
            {
                Brokerage brokerage = _mainRepository.TryGet(brokerageId);
                brokerage.Alias = brokerageIn.Alias.Simplify();
                brokerage.Name = brokerageIn.Name;
                brokerage.Email = brokerageIn.Email;
                brokerage.Logo = brokerageIn.Logo;
                brokerage.Website = brokerageIn.Website;
                brokerage.Status = BrokerageStatus.Active;

                if (brokerageIn.Address != null)
                {
                    brokerage.Address = _mapper.Map<Address>(brokerageIn.Address);
                }
                var phone = brokerageIn.Phones?.FirstOrDefault();
                if (phone != null)
                {
                    brokerage.Phones = brokerage.Phones ?? new List<Phone>();
                    brokerage.Phones.Add(_mapper.Map<Phone>(phone));
                }
                _validationManager.Validate(brokerage);

                if (!_validationManager.HasErrors)
                    _mainRepository.Update(brokerage);

                await CommitAsync();
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public async Task<OutputBrokerageDTO> DeleteAsync(long brokerageId)
        {
            return await TryCatch(async () =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId);
                _mainRepository.Remove(brokerage);
                await CommitAsync();
                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }
        public async Task<IEnumerable<WebpageProductDTO>> FindWebpagesAsync(long brokerageId)
        {
            return await TryCatch(async () =>
            {
                var webpages = await _webpageProductRepository.FindAllAsync(x => x.BrokerageProduct.BrokerageId == brokerageId,
                    b => b.BrokerageProduct,
                    b => b.BrokerageProduct.Product);

                return _mapper.Map<IEnumerable<WebpageProductDTO>>(webpages);
            });
        }
        public async Task<MapPointDTO> GetMapPointsAsync(string zipCode = "", string aliasProduct = "")
        {
            return await TryCatch(async () =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();
                map.RequestInfo = "/brokerage/{alias}";

                var brokerages = await _mainRepository.FindAllAsync(f => (!string.IsNullOrEmpty(zipCode) ? f.Address.Region.ZipCode == zipCode : true) &&
                                                                    (!string.IsNullOrEmpty(aliasProduct) ? f.BrokerageProducts.Where(y => y.Active && y.Product.Alias == aliasProduct).Count() > 0 : true),
                                                                    b => b.Address, b => b.Address.Region, b => b.BrokerageProducts, b => b.BrokerageProducts.Select(bp => bp.Product));

                foreach (var item in brokerages)
                {
                    PointDTO pinitem = new PointDTO() { Id = item.Id, Alias = item.Alias, Name = item.Name, Image = item.Logo, Latitude = item.Address.Latitude.Value, Longitude = item.Address.Longitude.Value };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public async Task<IReadOnlyCollection<OutputBrokerageTopListDTO>> GetTopAsync(int quant)
        {
            return await TryCatch(async () =>
            {
                var returnDTO = new List<OutputBrokerageTopListDTO>();

                var brokerages = await _mainRepository.GetAllAsync(x => x.Address, x => x.Address.Region, x => x.Address.Region.City, x => x.Address.Region.City.State, x => x.Brokers, x => x.Brokers.Select(y => y.Policies));

                foreach (Brokerage brokerage in brokerages)
                {
                    returnDTO.Add(_mapper.Map<OutputBrokerageTopListDTO>(brokerage));
                }
                returnDTO = returnDTO.OrderByDescending(x => x.Total).Take(quant).ToList();

                return returnDTO;
            });
        }
        public async Task<GraphicDTO> GetTotalStatusAsync()
        {
            return await TryCatch(async () =>
            {
                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                var brokerage = await _mainRepository.GetAllAsync();

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = EnumExtensions.Description(BrokerageStatus.Active), Value = brokerage.Where(x => x.Status == BrokerageStatus.Active).Count() };
                graphic.List.Add(itemgraphic);

                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = EnumExtensions.Description(BrokerageStatus.Inactive), Value = brokerage.Where(x => x.Status == BrokerageStatus.Inactive).Count() };
                graphic.List.Add(itemgraphic2);

                return graphic;
            });
        }
        public async Task<IReadOnlyCollection<GraphicDTO>> GetTotalLastYearAsync()
        {
            return await TryCatch(async () =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var brokerages = await _mainRepository.FindAllAsync(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                DateTime dateaux = datebegin;

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                int numberMonth = 12;
                while (numberMonth > 0)
                {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM/yy"), Value = brokerages.Where(x => x.CreatedDate.Month == dateaux.Month && x.CreatedDate.Year == dateaux.Year).Count() };
                    graphic.List.Add(itemgraphic);

                    dateaux = dateaux.AddMonths(1);
                    numberMonth--;
                }
                result.Add(graphic);

                return result;
            });
        }
        public async Task<BrokerageDefaultWebPageDTO> UpdateDefaultWebPageAsync(long brokerageId, BrokerageDefaultWebPageDTO webpageIn)
        {
            return await TryCatch(async () =>
            {
                var brokerage = _mainRepository.TryGet(brokerageId,
                    b => b.BrokerageGroup,
                    b => b.BrokerageProducts,
                    b => b.Brokers,
                    b => b.Phones,
                    b => b.Address
                );

                brokerage.Logo = webpageIn.Logo;
                brokerage.Cover = webpageIn.Cover;

                _validationManager.Validate(brokerage);
                _mainRepository.Update(brokerage);


                await CommitAsync();
                return _mapper.Map<BrokerageDefaultWebPageDTO>(brokerage);
            });
        }

        public async Task<OutputBrokerageDTO> FindAsync(string alias, bool includeProducts = false)
        {
            return await TryCatch(async () =>
            {
                Expression<Func<Brokerage, object>>[] includes = null;
                if (includeProducts)
                    includes = new Expression<Func<Brokerage, object>>[] { b => b.BrokerageProducts, b => b.BrokerageProducts.Select(y => y.Product), x => x.Address, y => y.Address.Region, m => m.Address.Region.City, c => c.Address.Region.City.State };
                Brokerage brokerage = await _mainRepository.FindAsync(x => x.Alias == alias, includes);

                return _mapper.Map<OutputBrokerageDTO>(brokerage);
            });
        }

        public OutputBrokerageMyProtectorDTO FindMyProtector(string alias)
        {
            return TryCatch(() =>
            {
                Brokerage brokerage = _mainRepository.TryFind(x => x.Alias == alias,
                                        x => x.Address,
                                        x => x.Address.Region,
                                        x => x.Address.Region.City,
                                        x => x.Address.Region.City.State,
                                        x => x.Phones);
                return _mapper.Map<OutputBrokerageMyProtectorDTO>(brokerage);
            });
        }

        public BrokerageActiveInactiveBrokeragesAndAgentstDTO GetActiveInactiveBrokeragesAndAgents()
        {
            var brokerageActiveInactiveBrokeragesAndAgentstDTO = new BrokerageActiveInactiveBrokeragesAndAgentstDTO();
            var brokerages = _mainRepository.GetAll(h => h.Brokers);
            if (brokerages.Count() > 0)
            {
                brokerageActiveInactiveBrokeragesAndAgentstDTO.ActiveBrokerages = brokerages.Where(x => x.Status == BrokerageStatus.Active).Count();
                brokerageActiveInactiveBrokeragesAndAgentstDTO.InactiveBrokerages = brokerages.Where(x => x.Status == BrokerageStatus.Inactive).Count();
                brokerageActiveInactiveBrokeragesAndAgentstDTO.ActiveAgents = brokerages.SelectMany(x => x.Brokers).Where(x => x.Status == UserStatus.Active).Count();
            }
            return brokerageActiveInactiveBrokeragesAndAgentstDTO;
        }

        #endregion
    }
}