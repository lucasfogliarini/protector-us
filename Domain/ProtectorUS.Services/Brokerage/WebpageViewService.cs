﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using Extensions;
    using System.Threading.Tasks;

    public class WebpageViewService : BaseService<WebpageView>, IWebpageViewService
    {
        private readonly IQuotationRepository _quotationRepository;
        public WebpageViewService(
           IWebpageViewRepository webpageViewRepository, 
           IQuotationRepository quotationRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                webpageViewRepository)
        {
            _quotationRepository = quotationRepository;
        }

        public OutputWebpageViewDTO Create(InputWebpageViewDTO webviewIn)
        {

            return TryCatch(() =>
            {
                WebpageView webpageView = _mapper.Map<WebpageView>(webviewIn);

                _validationManager.Validate(webpageView);
                _mainRepository.Add(webpageView);

                Commit();
                return _mapper.Map<OutputWebpageViewDTO>(webpageView);
            });
        }
        public async Task<OutputWebpageViewDTO> CreateAsync(InputWebpageViewDTO webviewIn)
        {
            return await TryCatch(async() =>
            {
                WebpageView webpageView = _mapper.Map<WebpageView>(webviewIn);

                _validationManager.Validate(webpageView);
                _mainRepository.Add(webpageView);

                await CommitAsync();
                return _mapper.Map<OutputWebpageViewDTO>(webpageView);
            });
        }
        public IReadOnlyCollection<GraphicDTO> GetTotalInterval(Filters<WebpageView> filters, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                int count = 0;
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var views = _mainRepository.FindAll(filters, x => x.WebpageProduct, x => x.WebpageProduct.BrokerageProduct, x => x.WebpageProduct.BrokerageProduct.Product).ToList();
                var listWebPagesid = views.Select(x => x.WebpageProductId).Distinct();

                var quotations = _quotationRepository.FindAll(x => x.CreatedDate > datebegin && x.CreatedDate < dateend && listWebPagesid.Contains(x.BrokerageProductId), y => y.Orders, y => y.Orders.Select(x => x.Policy)).ToList();

                var listquotationsActives = quotations.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();
                var totalquotation = quotations.Count();
                decimal hitratio = totalquotation != 0 && listquotationsActives != 0 ? ((listquotationsActives * 100) / totalquotation) : 0;

                int totalviews = views.Count();

                count += totalviews + totalquotation;
                GraphicDTO total = new GraphicDTO();
                total.List = new List<ItemGraphicDTO>();
                total.Name = "Total Views";
                ItemGraphicDTO itemtotal = new ItemGraphicDTO() { Key = "Total Views", Value = totalviews };
                total.List.Add(itemtotal);
                result.Add(total);

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                graphic.Name = "Views/Access";

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = EnumExtensions.Description(AccessType.Direct), Value = views.Where(c => c.Access == AccessType.Direct).Count() };
                graphic.List.Add(itemgraphic);

                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = EnumExtensions.Description(AccessType.Campaign), Value = views.Where(c => c.Access == AccessType.Campaign).Count() };
                graphic.List.Add(itemgraphic2);

                ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = EnumExtensions.Description(AccessType.WebBanner), Value = views.Where(c => c.Access == AccessType.WebBanner).Count() };
                graphic.List.Add(itemgraphic3);

                result.Add(graphic);


                GraphicDTO graphic2 = new GraphicDTO();
                graphic2.List = new List<ItemGraphicDTO>();
                graphic2.Name = "Product/Access";

                foreach (var item in views.Select(x => x.WebpageProduct.BrokerageProduct.Product).Distinct().ToList())
                {
                    ItemGraphicDTO itemgraphic4 = new ItemGraphicDTO() { Key = item.Abbreviation, Value = views.Where(c => c.WebpageProduct.BrokerageProduct.ProductId == item.Id).Count() };
                    graphic2.List.Add(itemgraphic4);
                };
                result.Add(graphic2);

                GraphicDTO quotationsg = new GraphicDTO();
                quotationsg.List = new List<ItemGraphicDTO>();
                quotationsg.Name = "Quotation";
                //ainda não definido
                ItemGraphicDTO itemquotation = new ItemGraphicDTO() { Key = "Total Quotations", Value = quotations.Count() };
                quotationsg.List.Add(itemquotation);
                ItemGraphicDTO itemquotation2 = new ItemGraphicDTO() { Key = "Hit Ratio", Value = hitratio };
                quotationsg.List.Add(itemquotation2);

                result.Add(quotationsg);

                return count > 0 ? result : new List<GraphicDTO>();
            });
        }

        public IReadOnlyCollection<GraphicDTO> GetTotalWeek(Filters<WebpageView> filters, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                int count = 0;
                int intervaleixo = 0;
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 24;
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-6);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 7;
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = (int)Math.Round(dateend.Subtract(datebegin).TotalDays);
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 12;
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var views = _mainRepository.FindAll(filters, x => x.WebpageProduct, x => x.WebpageProduct.BrokerageProduct, x => x.WebpageProduct.BrokerageProduct.Product).ToList();
                var listWebPagesid = views.Select(x => x.WebpageProductId).Distinct();

                var quotations = _quotationRepository.FindAll(x => x.CreatedDate > datebegin && x.CreatedDate < dateend && listWebPagesid.Contains(x.BrokerageProductId), y => y.Orders, y => y.Orders.Select(x => x.Policy)).ToList();

                var listquotationsActives = quotations.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();
                var totalquotation = quotations.Count();
                decimal hitratio = totalquotation != 0 && listquotationsActives != 0 ? ((listquotationsActives * 100) / totalquotation) : 0;

                int totalviews = views.Count();

                count += totalviews + totalquotation;

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                DateTime dateaux = datebegin;

                for (int i = 1; i <= intervaleixo; i++)
                {

                    switch (period)
                    {
                        case PeriodFilter.Today:
                            ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("HH:mm"), Value = views.Where(x=>x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddHours(1)).Count()  };
                            graphic.List.Add(itemgraphic);
                            dateaux = dateaux.AddHours(1);
                            break;
                        case PeriodFilter.Week:
                            ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = dateaux.DayOfWeek.ToString().Substring(0,3), Value = views.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1)).Count() };
                            graphic.List.Add(itemgraphic2);
                            dateaux = dateaux.AddDays(1);
                            break;
                        case PeriodFilter.LastMonth:
                            ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = dateaux.ToString("dd MMM"), Value = views.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddDays(1)).Count() };
                            graphic.List.Add(itemgraphic3);
                            dateaux = dateaux.AddDays(1);
                            break;
                        case PeriodFilter.LastYear:
                            ItemGraphicDTO itemgraphic4 = new ItemGraphicDTO() { Key = dateaux.ToString("MMM"), Value = views.Where(x => x.CreatedDate >= dateaux && x.CreatedDate < dateaux.AddMonths(1)).Count() };
                            graphic.List.Add(itemgraphic4);
                            dateaux = dateaux.AddMonths(1);
                            break;
                    }
                }

                result.Add(graphic);

                return count > 0 ? result : new List<GraphicDTO>();
            });
        }

        public IReadOnlyCollection<GraphicDTO> GetTotalWebBanner(Filters<WebpageView> filters, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                int count = 0;
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                switch (period)
                {
                    case PeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                    case PeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        break;
                }

                filters.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);
                filters.Add(x => x.Access == AccessType.WebBanner);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var views = _mainRepository.FindAll(filters, x => x.WebpageProduct, x => x.WebpageProduct.BrokerageProduct, x => x.WebpageProduct.BrokerageProduct.Product).ToList();
                var products = views.Select(x => x.WebpageProduct.BrokerageProduct.Product).Distinct();

                int totalviews = views.Count();

                foreach (var item in products)
                {
                    var listWebPagesid = views.Where(c=>c.WebpageProduct.BrokerageProduct.ProductId == item.Id).Select(x => x.WebpageProductId).Distinct();

                    var quotations = _quotationRepository.FindAll(x => x.CreatedDate > datebegin && x.CreatedDate < dateend && listWebPagesid.Contains(x.BrokerageProductId), y => y.Orders, y => y.Orders.Select(x => x.Policy)).ToList();

                    var listquotationsActives = quotations.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();

                    var totalPremium = quotations.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Sum(y=>y.Orders.Sum(z=>z.Policy.TotalPremium));

                    var totalquotation = quotations.Count();
                    decimal hitratio = totalquotation != 0 && listquotationsActives != 0 ? ((listquotationsActives * 100) / totalquotation) : 0;

                    count += totalviews + totalquotation;

                    GraphicDTO graphic = new GraphicDTO();
                    graphic.List = new List<ItemGraphicDTO>();
                    graphic.Name = item.Name;

                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Viewed", Value = views.Where(c => c.WebpageProduct.BrokerageProduct.ProductId == item.Id).Count() };
                    graphic.List.Add(itemgraphic);

                    ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Bound", Value = listquotationsActives };
                    graphic.List.Add(itemgraphic2);

                    ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Ratio", Value = hitratio };
                    graphic.List.Add(itemgraphic3);

                    ItemGraphicDTO itemgraphic4 = new ItemGraphicDTO() { Key = "Total", Value = totalPremium };
                    graphic.List.Add(itemgraphic4);

                    result.Add(graphic);
                }

                return count > 0 ? result : new List<GraphicDTO>();
            });
        }

    }
}