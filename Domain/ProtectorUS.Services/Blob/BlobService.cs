﻿namespace ProtectorUS.Services
{
    using Model.Services;
    using Diagnostics.Logging;
    using System.Threading.Tasks;
    using Data.AzureStorage;
    using System.Net.Http;
    using Exceptions;
    using System.IO;

    public class BlobService : BaseService, IBlobService
    {
        public BlobService(ILogger logger) : base(logger)
        {
        }

        public MemoryStream DownloadStream(BlobType blobtype, string blobPath, bool directoryPrefix = false)
        {
            return TryCatch(() =>
            {
                AzureBlob azureBlob = CreateInstance(blobtype);
                return azureBlob.DownloadStream(blobPath, directoryPrefix);
            });
        }
        public string DownloadText(BlobType blobtype, string blobPath, bool directoryPrefix = false)
        {
            return TryCatch(() =>
            {
                AzureBlob azureBlob = CreateInstance(blobtype);
                return azureBlob.DownloadText(blobPath, directoryPrefix);
            });
        }

        #region Upload
        public async Task<string> UploadAsync(BlobType blobtype, StreamContent streamContent, string fileName = null)
        {
            return await TryCatch(async () =>
            {
                if (streamContent == null || streamContent.Headers.ContentType == null)
                    throw new BadRequestException(x => x.EL030);

                AzureBlob azureBlob = CreateInstance(blobtype);
                return await azureBlob.UploadAsync(streamContent, fileName);
            });
        }
        
        public string UploadAsync(BlobType blobtype, byte[] bytes, string fileName)
        {
            return TryCatch(() =>
            {
                if (bytes == null)
                    throw new BadRequestException(x => x.EL030);
                AzureBlob azureBlob = CreateInstance(blobtype);
                return azureBlob.UploadAsync(bytes, fileName);
            });
        }
        public string Upload(BlobType blobtype, byte[] bytes, string fileName)
        {
            return TryCatch(() =>
            {
                if (bytes == null)
                    throw new BadRequestException(x => x.EL030);
                AzureBlob azureBlob = CreateInstance(blobtype);
                return azureBlob.Upload(bytes, fileName);
            });
        }
        #endregion
        private AzureBlob CreateInstance(BlobType blobtype)
        {
            switch (blobtype)
            {
                case BlobType.Image:
                    return new ImageAzureBlob();
                case BlobType.ClaimDocument:
                    return new ClaimDocumentAzureBlob();
                case BlobType.PolicyCertificate:
                    return new PolicyCertificateAzureBlob();
                case BlobType.ProductTerm:
                    return new ProductTermAzureBlob();
                case BlobType.EmailTemplate:
                    return new EmailTemplateAzureblob();
                case BlobType.Brochure:
                    return new BrochureAzureBlob();
                case BlobType.Policy:
                    return new PolicyAzureBlob();
            }
            return null;
        }
    }
}
