﻿using AutoMapper;


namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;


    public class NotificationService : BaseService<Notification>, INotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        IClaimRepository _claimRepository;
        private readonly IPolicyRepository _policyRepository;

        public NotificationService(
           INotificationRepository notificationRepository,
           IPolicyRepository policyRepository,
           IClaimRepository claimRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                notificationRepository)
        {
            _mainRepository = notificationRepository;
            _notificationRepository = notificationRepository;
            _claimRepository = claimRepository;
            _policyRepository = policyRepository;
        }

        public OutputNotificationDTO Create(InputNotificationDTO notificationIn)
        {
            return TryCatch(() =>
            {
                var notification = _mapper.Map<Notification>(notificationIn);
                switch (notificationIn.ActivityType)
                {
                    //notify claim insured and brokers
                    case ActivityType.ClaimStatusChange:
                        {
                            //create My Protector insured claim notification
                            notification = this.ComposeNotification(notification, ActivityType.ClaimStatusChange, DestinationType.Insured);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);

                            //create  broker manager claim notification for all brokers
                            notification = this.ComposeNotification(notification, ActivityType.ClaimStatusChange, DestinationType.Broker);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);

                        }
                        break;


                    //notify claim insured 30 days left, brokers 35 days left and argo mamager user 40 days left.
                    case ActivityType.PolicyMustBeRenewal:
                        {
                            //create My Protector policy renewal notification
                            notification = this.ComposeNotification(notification, ActivityType.PolicyMustBeRenewal, DestinationType.Insured);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);

                            //create broker manager policy renewal notification for all brokers
                            notification = this.ComposeNotification(notification, ActivityType.PolicyMustBeRenewal, DestinationType.Broker);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);

                            //create argo manager policy renewal notification for all argo users
                            notification = this.ComposeNotification(notification, ActivityType.PolicyMustBeRenewal, DestinationType.ManagerUser);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);
                        }
                        break;

                    //notify all manager users
                    case ActivityType.BrokerageOnBoardingCompleted:
                        {
                            //create argo manager new broker notification for all argo users
                            notification = this.ComposeNotification(notification, ActivityType.BrokerageOnBoardingCompleted, DestinationType.ManagerUser);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);
                        }
                        break;

                    //notify all brokers
                    case ActivityType.AgentOnBoardingCompleted:
                        {
                            //create broker manager new broker notification for all brokers
                            notification = this.ComposeNotification(notification, ActivityType.AgentOnBoardingCompleted, DestinationType.Broker);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);
                        }
                        break;

                    //notify all brokers
                    case ActivityType.PolicyCancelRequest:
                        {
                            //create broker manager policy cancel request notification for all brokers
                            notification = this.ComposeNotification(notification, ActivityType.PolicyCancelRequest, DestinationType.Broker);
                            _validationManager.Validate(notification);
                            _mainRepository.Add(notification);
                        }
                        break;

                    default:
                        break;
                }



                _validationManager.Validate(notification);
                _mainRepository.Add(notification);

                Commit();
                return _mapper.Map<OutputNotificationDTO>(notification);
            });
        }
        public OutputNotificationDTO Get(long id)
        {
            return TryCatch(() =>
            {
                var notification = _mainRepository.TryGet(id);
                return _mapper.Map<OutputNotificationDTO>(notification);
            });
        }
        public OutputNotificationDTO Find(string updateBy)
        {
            return TryCatch(() =>
            {
                Notification notification = _mainRepository.Find(x => x.UpdatedBy == updateBy);
                return _mapper.Map<OutputNotificationDTO>(notification);
            });
        }
        public OutputNotificationDTO Update(InputNotificationDTO notificationIn)
        {
            return TryCatch(() =>
            {
                var notification = _mapper.Map<Notification>(notificationIn);

                _validationManager.Validate(notification);
                _mainRepository.Update(notification);

                Commit();
                return _mapper.Map<OutputNotificationDTO>(notification);
            });
        }
        public void Delete(long id)
        {
            TryCatch(() =>
            {
                var notification = _mainRepository.TryGet(id);
                notification.Delete();
                _mainRepository.Remove(notification);
                Commit();
            });
        }


        #region private methods 
        private Notification ComposeNotification(Notification notification, ActivityType activityType, DestinationType destinationType)
        {
            switch (destinationType)
            {
                case DestinationType.Insured:
                    {
                        switch (activityType)
                        {
                            case ActivityType.ClaimStatusChange:
                                {
                                    var claim = _claimRepository.TryGet(notification.SourceId.Value, x => x.Policy, x => x.Policy.Condition, x => x.Policy.Condition.Product);
                                    var claimEdited = _claimRepository.TryGetAsNoTracking(notification.SourceId.Value);
                                    notification.Title = "Status Changed";
                                    notification.Description = claim.ThirdParty.Name;
                                    notification.Message = $"{claim.Policy.Condition.Product.Name}{claim.Policy.Number} have our status change from {claim.Status} to {claimEdited.Status}";

                                }
                                break;
                            case ActivityType.PolicyMustBeRenewal:
                                {
                                    var policy = _policyRepository.TryGet(notification.SourceId.Value);
                                    notification.Title = "Reminder";
                                    notification.Description = "Renewal";
                                    notification.Message = $"Staty alert. Your policy {policy.Number} is about to expire.";
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case DestinationType.Broker:
                    {
                        switch (activityType)
                        {
                            case ActivityType.ClaimStatusChange:
                                {
                                    var claim = _claimRepository.TryGet(notification.SourceId.Value, x => x.Policy, x => x.Policy.Condition, x => x.Policy.Condition.Product);
                                    var claimEdited = _claimRepository.TryGetAsNoTracking(notification.SourceId.Value);
                                    notification.Message = $" The Claim {claim.Number} have our status change from {claim.Status} to {claimEdited.Status}";

                                }
                                break;
                            case ActivityType.PolicyMustBeRenewal:
                                {
                                    var policy = _policyRepository.TryGet(notification.SourceId.Value);
                                    notification.Message = $"The policy {policy.Number} is about to expire.";
                                }
                                break;

                            case ActivityType.AgentOnBoardingCompleted:
                                break;
                            case ActivityType.PolicyCancelRequest:
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case DestinationType.ManagerUser:
                    {
                        switch (activityType)
                        {
                            case ActivityType.PolicyMustBeRenewal:
                                {
                                    var policy = _policyRepository.TryGet(notification.SourceId.Value);
                                    notification.Message = $"The policy {policy.Number} is about to expire.";
                                }
                                break;
                            case ActivityType.BrokerageOnBoardingCompleted:
                                break;
                            case ActivityType.AgentOnBoardingCompleted:
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }

            return notification;
        }
        #endregion
    }
}
