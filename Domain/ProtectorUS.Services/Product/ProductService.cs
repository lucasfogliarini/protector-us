﻿using System.Collections.Generic;


namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Model.Services;
    using Diagnostics.Logging;
    using Model.Validation;
    using AutoMapper;
    using DTO;
    using Model;
    using Exceptions;
    using System;
    using System.Threading.Tasks;

    public class ProductService : BaseService<Product>, IProductService
    {
        private readonly IProductConditionRepository _productConditionRepository;
        private readonly ILocalizationService _localizationService;
        public ProductService(IUnitOfWork unitOfWork,
           ILocalizationService localizationService,
           IProductRepository productRepository,
           IProductConditionRepository productConditionRepository,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                productRepository)
        {
            _productConditionRepository = productConditionRepository;
            _localizationService = localizationService;
        }        

        public OutputProductDTO Toggle(long productId, bool toggle)
        {
            return TryCatch(() =>
            {
                var product = _mainRepository.Find(p=>p.Id == productId && p.Active == !toggle);
                if (product == null)
                {
                    throw new InvalidException(e => e.EL038, toggle ? "active" : "inactive");
                }
                product.Active = toggle;
                _mainRepository.Update(product);
                _validationManager.Validate(product);
                Commit();
                return _mapper.Map<OutputProductDTO>(product);
            });
        }

        public IEnumerable<OutputProductDTO> GetAll()
        {
            return  TryCatch( () =>
            {
                var products =  _mainRepository.GetAll();
                return _mapper.Map<IEnumerable<OutputProductDTO>>(products);
            });

        }
        public IEnumerable<OutputProductConditionDTO> GetConditions(long productId, long stateId)
        {
            return  TryCatch( () =>
            {
                var productConditions =  _productConditionRepository.FindAll(pc => pc.ProductId == productId && pc.StateId == stateId);
                return _mapper.Map<IEnumerable<OutputProductConditionDTO>>(productConditions);
            });
        }
        public IEnumerable<OutputProductConditionDTO> GetConditions(string alias, string zipcode)
        {
            return TryCatch( () =>
            {
                var region = _localizationService.GetRegion(zipcode);

                var productConditions =  _productConditionRepository.FindAll(pc => pc.Product.Alias == alias && pc.StateId == region.City.StateId, x => x.Product);
                return _mapper.Map<IEnumerable<OutputProductConditionDTO>>(productConditions);
            });

        }
        public OutputProductConditionDTO AttachCondition(long productId, long stateId, string conditionFilePath)
        {
            return TryCatch(() =>
            {
                var condition = new ProductCondition()
                {
                    FilePath = conditionFilePath,
                    ProductId = productId,
                    StateId = stateId,
                    Active = true
                };
                _validationManager.Validate(condition);
                _productConditionRepository.Add(condition);
                Commit();
                return _mapper.Map<OutputProductConditionDTO>(condition);
            });
        }
        public async Task<IEnumerable<OutputProductConditionDTO>> GetConditionsAsync(long productId, long stateId)
        {
            return await TryCatch(async() =>
            {
                var productConditions = await _productConditionRepository.FindAllAsync(pc => pc.ProductId == productId && pc.StateId == stateId);
                return _mapper.Map<IEnumerable<OutputProductConditionDTO>>(productConditions);
            });
        }

        public async Task<IEnumerable<OutputProductConditionDTO>> GetConditionsAsync(string alias, string zipcode)
        {
            return await TryCatch(async () =>
            {
                var region = _localizationService.GetRegion(zipcode);

                var productConditions = await _productConditionRepository.FindAllAsync(pc => pc.Product.Alias == alias && pc.StateId == region.City.StateId, x => x.Product);
                return _mapper.Map<IEnumerable<OutputProductConditionDTO>>(productConditions);
            });
        }

        public async Task<IEnumerable<OutputProductDTO>> GetAllAsync()
        {
            return await TryCatch(async() =>
            {
                var products = await _mainRepository.GetAllAsync();
                return _mapper.Map<IEnumerable<OutputProductDTO>>(products);
            });
        }
        public async Task<OutputProductBaseDTO> FindAsync(string alias)
        {
            return await TryCatch(async() =>
            {
                var outProduct = await _mainRepository.TryFindAsync(x => x.Alias == alias);
                return _mapper.Map<OutputProductBaseDTO>(outProduct);
            });
        }

        public OutputProductBaseDTO Find(string alias)
        {
            return  TryCatch( () =>
            {
                var outProduct =_mainRepository.TryFind(x => x.Alias == alias);
                return _mapper.Map<OutputProductBaseDTO>(outProduct);
            });
        }
    }
}
