﻿using AutoMapper;
using System.Collections.Generic;



namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.Threading.Tasks;
    using System.Diagnostics;
    using System;

    public class CancelReasonService : BaseService<CancelReason>, ICancelReasonService
    {
        private readonly ICancelReasonRepository _cancelReasonRepository;

        public CancelReasonService(
           ICancelReasonRepository cancelReasonRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                cancelReasonRepository)
        {
            _mainRepository = cancelReasonRepository;
            _cancelReasonRepository = cancelReasonRepository;
        }

        public OutputCancelReasonDTO Create(InputCancelReasonDTO cancelReasonIn)
        {
            return TryCatch(() =>
            {
                var cancelReason = _mapper.Map<CancelReason>(cancelReasonIn);

                _validationManager.Validate(cancelReason);
                _mainRepository.Add(cancelReason);

                 Commit();
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });        
        }
        public OutputCancelReasonDTO Get(long id)
        {
            return TryCatch(() =>
            {
                var cancelReason =  _mainRepository.TryGet(id);
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public OutputCancelReasonDTO Find(string updateBy)
        {
            return TryCatch(() =>
            {
                CancelReason cancelReason = _mainRepository.Find(x => x.UpdatedBy == updateBy);
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public OutputCancelReasonDTO Update(long id, InputCancelReasonDTO cancelReasonIn)
        {
            return TryCatch(() =>
            {
                CancelReason cancelReason = _mainRepository.TryGet(id);
                cancelReason.Description = cancelReasonIn.Description;

                _validationManager.Validate(cancelReason);
                _mainRepository.Update(cancelReason);

                Commit();

                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public void Delete(long cancelReasonId)
        {
            TryCatch(() =>
            {
                var cancelReason = _mainRepository.Get(cancelReasonId);
                if (cancelReason != null)
                {
                    cancelReason.Delete();
                    _mainRepository.Remove(cancelReason);
                }
                Commit();
            });
        }
        public IReadOnlyCollection<OutputCancelReasonDTO> GetAll()
        {
            return TryCatch(() =>
            {
                var reasons = _mainRepository.GetAll();
                return _mapper.Map<IReadOnlyCollection<OutputCancelReasonDTO>>(reasons);
            });

        }

        public async Task<IReadOnlyCollection<OutputCancelReasonDTO>> GetAllAsync()
        {
            return await TryCatch(async () =>
            {
                var reasons = await _mainRepository.GetAllAsync();
                return _mapper.Map<IReadOnlyCollection<OutputCancelReasonDTO>>(reasons);
            });
        }
        public async Task<OutputCancelReasonDTO> CreateAsync(InputCancelReasonDTO cancelReasonIn)
        {
            return await TryCatch(async() =>
            {
                var cancelReason = _mapper.Map<CancelReason>(cancelReasonIn);

                _validationManager.Validate(cancelReason);
                _mainRepository.Add(cancelReason);

                await CommitAsync();
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public async Task<OutputCancelReasonDTO> GetAsync(long id)
        {
            return await TryCatch(async() =>
            {
                var cancelReason = await _mainRepository.TryGetAsync(id);
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public async Task<OutputCancelReasonDTO> UpdateAsync(long id, InputCancelReasonDTO cancelReasonIn)
        {
            return await TryCatch(async () =>
            {
                CancelReason cancelReason = _mainRepository.TryGet(id);
                cancelReason.Description = cancelReasonIn.Description;

                _validationManager.Validate(cancelReason);
                _mainRepository.Update(cancelReason);

                await CommitAsync();

                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
        public async Task<OutputCancelReasonDTO> FindAsync(string updateBy)
        {
            return await TryCatch(async () =>
            {
                CancelReason cancelReason = await _mainRepository.FindAsync(x => x.UpdatedBy == updateBy);
                return _mapper.Map<OutputCancelReasonDTO>(cancelReason);
            });
        }
    }
}
