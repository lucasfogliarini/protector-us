﻿using System.Collections.Generic;

namespace ProtectorUS.Services
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Model.Services;
    using System.IO;
    public class PdfReplacerService : IPdfReplacerService
    {
        public void ReplacePdfForm(string currentPath, string newPath, Dictionary<string, string> keyValues)
        {
            string fileNameExisting = currentPath;
            string fileNameNew = newPath;

            using (var existingFileStream = new FileStream(fileNameExisting, FileMode.Open))
            using (var newFileStream = new FileStream(fileNameNew, FileMode.Create))
            {
                // Open existing PDF
                var pdfReader = new PdfReader(existingFileStream);

                // PdfStamper, which will create
                var stamper = new PdfStamper(pdfReader, newFileStream);
                var form = stamper.AcroFields;
                stamper.Writer.CloseStream = false;
                var fieldKeys = form.Fields.Keys;

                foreach (var key in keyValues.Keys)
                {
                    form.SetField(key, keyValues[key]);
                }

                // "Flatten" the form so it wont be editable/usable anymore
                stamper.FormFlattening = true;

                // You can also specify fields to be flattened, which
                // leaves the rest of the form still be editable/usable

                stamper.Close();
                pdfReader.Close();
            }
        }



        public void ReplacePdfForm(string currentPath, string newPath, string QrCodeUrl, byte[] logoBrokerage)
        {
            string fileNameExisting = currentPath;
            string fileNameNew = newPath;

            using (var existingFileStream = new FileStream(fileNameExisting, FileMode.Open))
            using (var newFileStream = new FileStream(fileNameNew, FileMode.Create))
            {
                // Open existing PDF
                var pdfReader = new PdfReader(existingFileStream);

                // PdfStamper, which will create
                var stamper = new PdfStamper(pdfReader, newFileStream);

                //novo
                var pdfContentByte = stamper.GetOverContent(1);

                var form = stamper.AcroFields;
                stamper.Writer.CloseStream = false;
                var fieldKeys = form.Fields.Keys;

                stamper.FormFlattening = true;


                
                //logo brokerage
                Image logoBroker = Image.GetInstance(logoBrokerage);
                //logoBroker.SetAbsolutePosition(554, 528);
                logoBroker.SetAbsolutePosition(554, 540);
                logoBroker.ScaleAbsolute(62, 62);
                pdfContentByte.AddImage(logoBroker);                            

                //qrcode
                BarcodeQRCode qrcode = new BarcodeQRCode(QrCodeUrl, 1, 1, null);
                Image qrcodeImage = qrcode.GetImage();
                qrcodeImage.SetAbsolutePosition(356, 463);
                qrcodeImage.ScaleAbsolute(100, 100);
                pdfContentByte.AddImage(qrcodeImage);


                stamper.Close();
                pdfReader.Close();
            }
        }


    }
}
