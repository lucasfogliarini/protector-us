﻿namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Diagnostics.Logging;
    using System;
    using Model.Validation;
    using Exceptions;
    using AutoMapper;
    using DTO;
    using Model;
    using System.Linq.Expressions;
    using System.Collections.Generic;
    using Model.Services;
    using System.Threading.Tasks;
    using System.Device.Location;
    public abstract class BaseService<T> : BaseService where T : BaseEntity
    {
        readonly IUnitOfWork _unitOfWork;
        protected IRepository<T, long> _mainRepository;
        protected IValidationManager _validationManager { get; set; }
        protected IMappingEngine _mapper { get; private set; }

        public BaseService(
            IUnitOfWork unitOfWork,
            ILogger logger,
            IValidationManager validationManager,
            IMappingEngine mapper,
            IRepository<T, long> mainRepository, params BaseService[] dependentServices) : base(logger, dependentServices)
        {
            _unitOfWork = unitOfWork;
            _validationManager = validationManager;
            _mapper = mapper;
            _mainRepository = mainRepository;
        }
        public override void InitializeLogger(string userName)
        {
            base.InitializeLogger(userName);
            _unitOfWork.Logger.Initialize(_unitOfWork.GetType(), userName);
        }
        /// <summary>
        /// Checks for validation errors, throw them if have or Commit changes.
        /// </summary>
        protected void Commit()
        {
            if (CanCommit)
            {
                ThrowValidationErrors();
                _unitOfWork.Commit();
            }
        }
        /// <summary>
        /// Checks for validation errors, throw them if have or Commit changes. Asynchronously.
        /// </summary>
        protected async Task CommitAsync()
        {
            if (CanCommit)
            {
                ThrowValidationErrors();
                await _unitOfWork.CommitAsync();
            }
        }
        /// <summary>
        /// Checks for validation errors and throw them if have
        /// </summary>
        protected void ThrowValidationErrors()
        {
            if (_validationManager.HasErrors)
                _validationManager.ThrowErrors();
        }      
        protected PagedResult<TOut> FindPaginated<TOut>(int page, int pageSize, string sort, Filters<T> filters, params Expression<Func<T, object>>[] includeExpressions)
        {
            var itensPaged = _mainRepository.FindByPage(page, pageSize, sort, filters, includeExpressions);
            var pageOfItens = new PagedResult<TOut>
            {
                Items = _mapper.Map<IReadOnlyCollection<TOut>>(itensPaged.Items),
                TotalItems = itensPaged.TotalItems,
                ItemsPerPage = itensPaged.ItemsPerPage,
                Page = page,
                Sort = sort
            };
            return pageOfItens;
        }
        protected async Task<PagedResult<TOut>> FindPaginatedAsync<TOut>(int page, int pageSize, string sort, Filters<T> filters, params Expression<Func<T, object>>[] includeExpressions) where TOut : IOutputDTO
        {
            var itensPaged = await _mainRepository.FindByPageAsync(page, pageSize, sort, filters, includeExpressions);
            var pageOfItens = new PagedResult<TOut>
            {
                Items = _mapper.Map<IReadOnlyCollection<TOut>>(itensPaged.Items),
                TotalItems = itensPaged.TotalItems,
                ItemsPerPage = itensPaged.ItemsPerPage,
                Page = page,
                Sort = sort
            };
            return pageOfItens;
        }
        /// <summary>
        /// Try to get the Entity, if null throw <see cref="NotFoundException"/> 
        /// </summary>
        public TOut TryGet<TOut>(long id, params Expression<Func<T, object>>[] includeExpressions)
        {
            var entityOut = _mainRepository.TryGet(id, includeExpressions);
            return _mapper.Map<TOut>(entityOut);
        }
        public TOut Get<TOut>(long id, params Expression<Func<T, object>>[] includeExpressions)
        {
            var entityOut = _mainRepository.Get(id, includeExpressions);
            return _mapper.Map<TOut>(entityOut);
        }
    }

    public abstract class BaseService : IBaseService
    {
        public BaseService(ILogger logger, params BaseService[] dependentServices)
        {
            Logger = logger;
            DependentServices(dependentServices);
        }
        public ILogger Logger { get; private set; }
        public virtual void InitializeLogger(string userName)
        {
            Logger.Initialize(GetType(), userName);
        }
        /// <summary>
        /// True for set as the main service, false for set as the dependent service.
        /// </summary>
        protected bool CanCommit { get; private set; } = true;
        /// <summary>
        /// Set the current service as the main with dependent services. Only the main service can Commit the changes.
        /// </summary>
        protected void DependentServices(params BaseService[] dependentServices)
        {
            foreach (BaseService service in dependentServices)
            {
                service.CanCommit = false;
            }
        }
        /// <summary>
        /// Try execute the pagedResult method.
        /// 
        /// Exceptions:
        ///     Throw and log <see cref="BaseException"/>
        ///     Throw and log <see cref="Exceptions.SystemException"/>
        ///     
        /// </summary>
        /// <returns>The PagedResult<T> returned from <param name="_try""/></returns>
        protected PagedResult<TOutputDTO> TryCatch<TOutputDTO>(Func<PagedResult<TOutputDTO>> _try) where TOutputDTO : IOutputDTO
        {
            PagedResult<TOutputDTO> dtoOut = default(PagedResult<TOutputDTO>);
            TryCatch(() =>
            {
                dtoOut = _try();
            });
            return dtoOut;
        }
        /// <summary>
        /// Try execute the method.
        /// 
        /// Exceptions:
        ///     Throw and log <see cref="BaseException"/>
        ///     Throw and log <see cref="Exceptions.SystemException"/>
        ///     
        /// </summary>
        /// <returns>The object returned from <param name="_try""/></returns>
        protected TOut TryCatch<TOut>(Func<TOut> _try)
        {
            TOut _out = default(TOut);
            TryCatch(() =>
            {
                _out = _try();
            });
            return _out;
        }
        /// <summary>
        /// Try execute the method asynchronously.
        /// 
        /// Exceptions:
        ///     Throw and log <see cref="BaseException"/>
        ///     Throw and log <see cref="Exceptions.SystemException"/>
        ///     
        /// </summary>
        /// <returns>The object returned from <param name="_try""/></returns>
        protected async Task<TOut> TryCatch<TOut>(Func<Task<TOut>> _try)
        {
            try
            {
                return await _try();
            }
            catch (BaseException ex)
            {
                if (ex.Log) Logger.LogException(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                throw new Exceptions.SystemException(ex);
            }
        }
        protected IEnumerable<TOutputDTO> TryCatch<TOutputDTO>(Func<IEnumerable<TOutputDTO>> _try) where TOutputDTO : IOutputDTO
        {
            IEnumerable<TOutputDTO> dtoOut = null;
            TryCatch(() =>
            {
                dtoOut = _try();
            });
            return dtoOut;
        }
        /// <summary>
        /// Try execute the method. 
        /// 
        /// Exceptions:
        ///     Throw and log <see cref="BaseException"/>
        ///     Throw and log <see cref="Exceptions.SystemException"/>
        ///  
        /// </summary>
        protected void TryCatch(Action _try)
        {
            try
            {
                _try();
            }
            catch (BaseException ex)
            {
                if (ex.Log) Logger.LogException(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                throw new Exceptions.SystemException(ex);
            }
        }
        /// <summary>
        /// Run a async method synchronously.
        /// </summary>
        protected TOut RunSync<TOut>(Func<Task<TOut>> task)
        {
            return Task.Run(task).Result;
        }
        /// <summary>
        /// Checks if DTO is null. If is throw <see cref="ArgumentRequiredException"/> code ES004.
        /// </summary>
        protected void ThrowArgumentRequiredIfNull<TObject>(TObject obj) where TObject : class
        {
            if (obj == null)
            {
                throw new ArgumentRequiredException(e => e.ES004, typeof(TObject).Name);
            }
        }
        /// <summary>
        /// Checks if entity is null. If is throw <see cref="NotFoundException"/> code EL026.
        /// </summary>
        protected void ThrowNotFoundIfNull<TObject>(TObject obj) where TObject : class
        {
            if (obj == null)
            {
                throw new NotFoundException(e => e.EL026, typeof(TObject).Name);
            }
        }
        /// <summary>
        /// Checks if entity is inactive. If is throw <see cref="InactiveException"/> code ES006.
        /// </summary>
        protected void ThrowIfInactive(ISwitch entity)
        {
            if (entity != null && !entity.Active)
                throw new InactiveException(m => m.ES006, entity.GetType().Name);
        }
        protected void ThrowCoordinateRequiredIfNull(GeoCoordinate coordinate, string address)
        {
            if (coordinate == null)
                throw new ArgumentRequiredException(m => m.EL058, address);
        }
    }
}