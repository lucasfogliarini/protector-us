﻿namespace ProtectorUS.Services
{

    using Model;
    using Model.Services;
    using DocumentDBRepositories = Model.DocumentDB.Repositories;
    using Exceptions;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using AutoMapper;
    using DTO;
    using DocumentDB = Model.DocumentDB.RatingPlan;
    using DTO.RatingPlan;
    using System;
    using TimeZone;
    using System.Linq;
    public class RatingPlanFormService : BaseService<RatingPlanFormVersion>, IRatingPlanFormService
    {
        readonly DocumentDBRepositories.IRatingPlanFormRepository _ratingPlanFormDocDbRepository;
        readonly IRatingPlanFormVersionRepository _ratingPlanFormVersionRepository;
        readonly ILocalizationService _localizationService;
        public RatingPlanFormService(
            DocumentDBRepositories.IRatingPlanFormRepository ratingPlanFormDocDbRepository,
            IRatingPlanFormVersionRepository ratingPlanFormVersionRepository,
            ILocalizationService localizationService,
            IRegionRepository regionRepository,
            IUnitOfWork unitOfWork,
            ILogger logger,
            IValidationManager validationManager,
            IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                ratingPlanFormVersionRepository)
        {
            _ratingPlanFormDocDbRepository = ratingPlanFormDocDbRepository;
            _ratingPlanFormVersionRepository = ratingPlanFormVersionRepository;
            _localizationService = localizationService;
        }

        public OutputRatingPlanFormDTO GetLatestVersion(string productAlias, string zipCode)
        {
            return TryCatch(() =>
            {
                OutputRegionDTO regionOut = _localizationService.GetRegion(zipCode);
                long? stateId = regionOut?.City?.StateId;
                RatingPlanFormVersion ratingPlanFormVersion = _ratingPlanFormVersionRepository.GetLatestVersion(productAlias, stateId.GetValueOrDefault());
                ThrowNotFoundIfNull(ratingPlanFormVersion);
                ThrowIfInactive(ratingPlanFormVersion.RatingPlanForm);
                DocumentDB.RatingPlanForm ratingPlanFormDocDb = _ratingPlanFormDocDbRepository.Find(ratingPlanFormVersion.Id);
                if (ratingPlanFormDocDb == null)
                    throw new NotFoundException(e => e.EL024);

                ratingPlanFormDocDb.fields.RemoveAll(f=>!f.active);

                var ratingPlanFormOut = _mapper.Map<OutputRatingPlanFormDTO>(ratingPlanFormDocDb);
                int insuredTimezone = TimeZoneSearcher.GetTimeZoneOffset(zipCode);
                ratingPlanFormOut.minIssuedDate = DateTime.UtcNow.AddSeconds(insuredTimezone).AddMinutes(30).ToString("MM-dd-yyyy");
                ratingPlanFormOut.HashCode = RatingPlanComputer.GenerateHash(ratingPlanFormVersion.Id, ratingPlanFormVersion.RatingPlanForm.ProductId, regionOut.Id, insuredTimezone);
                return ratingPlanFormOut;
            });
        }
        public RatingPlanComputer GetRatingPlanComputer(string hashCode)
        {
            return TryCatch(() =>
            {
                RatingPlanComputer ratingPlanInfo = RatingPlanComputer.Get(hashCode);
                ratingPlanInfo.RatingPlanForm = _ratingPlanFormDocDbRepository.Find(ratingPlanInfo.RatingPlanFormVersionId);
                string originalHashCode = ratingPlanInfo.GenerateHash();
                if (originalHashCode != hashCode)
                    throw new InvalidException(e => e.EL010);
                return ratingPlanInfo;
            });
        }
    }    
}
