﻿using AutoMapper;
using System;

namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.IO;
    using System.Drawing;
    using System.Drawing.Text;
    public class EmailMarketingService : BaseService<Campaign>, IEmailMarketingService
    {
        readonly ICampaignRepository _campaignRepository;
        readonly IWebpageProductRepository _webpageProductRepository;
        readonly IBlobService _blobservice;
        readonly IEmailTemplateService _emailTemplateService;

        public EmailMarketingService(
           ICampaignRepository campaignRepository,
           IWebpageProductRepository webpageProductRepository,
           IBlobService blobService,
           IEmailTemplateService emailTemplateService,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                campaignRepository)
        {
            _campaignRepository = campaignRepository;
            _webpageProductRepository = webpageProductRepository;
            _blobservice = blobService;
            _emailTemplateService = emailTemplateService;
        }

        public OutputEmailMarketingDTO Get(long emailMarketingId)
        {
            return TryCatch(() =>
            {
                var campaign = _campaignRepository.TryGet(emailMarketingId,
                                                                x => x.WebpageProduct.BrokerageProduct,
                                                                x => x.WebpageProduct.BrokerageProduct.Product,
                                                                x => x.WebpageProduct.BrokerageProduct.Brokerage);
                EmailMarketing emailMarketing = campaign as EmailMarketing;
                ThrowNotFoundIfNull(emailMarketing);
                var product = emailMarketing.WebpageProduct.BrokerageProduct.Product;
                var brokerage = emailMarketing.WebpageProduct.BrokerageProduct.Brokerage;

                string html = _emailTemplateService.GetEmailMarketingAccountantsBody(emailMarketing, product, brokerage);

                return new OutputEmailMarketingDTO()
                {
                    Id = emailMarketing.Id,
                    Title = emailMarketing.Title,
                    Html = html
                };
            });
        }

        public void Create(EmailMarketingDTO emailMarketingIn, long brokerId, long brokerageId)
        {
            TryCatch(() =>
            {
                EmailMarketing emailMarketing = _mapper.Map<EmailMarketing>(emailMarketingIn);
                var webPageProduct = _webpageProductRepository.TryFind(x => x.BrokerageProduct.ProductId == emailMarketing.ProductId &&
                                                                            x.BrokerageProduct.BrokerageId == brokerageId,
                                                                            x => x.BrokerageProduct,
                                                                            x => x.BrokerageProduct.Brokerage);

                Brokerage brokerage = webPageProduct.BrokerageProduct.Brokerage;
                emailMarketing.WebpageProductId = webPageProduct.Id;
                emailMarketing.BrokerId = brokerId;
                _validationManager.ValidateAndThrowErrors(emailMarketing);

                byte[] coverBytes = _blobservice.DownloadStream(BlobType.EmailTemplate, $"{EmailTemplate.EmailMarketingCampaign.emailmarketingTemplateCovers}/{(int)emailMarketing.CoverId}.png", true).ToArray();
                byte[] protectorLogoBytes = _blobservice.DownloadStream(BlobType.EmailTemplate, $"{EmailTemplate.EmailMarketingCampaign.emailmarketingTemplateCovers}/protector_logo.png", true).ToArray();
                byte[] brokerageLogoBytes = _blobservice.DownloadStream(BlobType.Image, brokerage.Logo).ToArray();
                emailMarketing.CoverPath = GenerateCover(coverBytes, brokerageLogoBytes, protectorLogoBytes, emailMarketing.Title);
                _campaignRepository.Add(emailMarketing);
                Commit();
            });
        }
        private string GenerateCover(byte[] coverBytes, byte[] brokerageLogoBytes, byte[] protectorLogoBytes, string coverTitle)
        {
            using (MemoryStream coverMemoryStream = new MemoryStream(coverBytes))
            {
                Bitmap coverBitMap = new Bitmap(coverMemoryStream);
                using (MemoryStream brokerageLogoMemoryStream = new MemoryStream(brokerageLogoBytes))
                {
                    Bitmap brokerageLogoBitMap = new Bitmap(brokerageLogoMemoryStream);
                    using (MemoryStream protectorLogoMemoryStream = new MemoryStream(protectorLogoBytes))
                    {
                        Bitmap protectorLogoBitMap = new Bitmap(protectorLogoMemoryStream);

                        const int borderSize = 2;
                        int heightimage = coverBitMap.Height + borderSize + (brokerageLogoBitMap.Height / 2);

                        Bitmap ImageFinal = new Bitmap(coverBitMap.Width, heightimage);

                        Graphics gr = Graphics.FromImage(ImageFinal);

                        Rectangle ImageSize = new Rectangle(0, 0, coverBitMap.Width, heightimage);
                        gr.FillRectangle(Brushes.White, ImageSize);

                        gr.DrawImage(coverBitMap, 0, (brokerageLogoBitMap.Height / 2), coverBitMap.Width, coverBitMap.Height);

                        using (Brush border = new SolidBrush(Color.LightGray))
                        {
                            gr.FillRectangle(border, ((coverBitMap.Width / 2) - (brokerageLogoBitMap.Width / 2)) - borderSize, 0,
                                brokerageLogoBitMap.Width + (borderSize * 2), brokerageLogoBitMap.Height + (borderSize * 2));
                        }

                        gr.DrawImage(brokerageLogoBitMap, ((coverBitMap.Width / 2) - (brokerageLogoBitMap.Width / 2)), borderSize, brokerageLogoBitMap.Width, brokerageLogoBitMap.Height);

                        gr.DrawImage(protectorLogoBitMap, ((coverBitMap.Width / 2) - (protectorLogoBitMap.Width / 2)), heightimage - (protectorLogoBitMap.Height + 20), protectorLogoBitMap.Width, protectorLogoBitMap.Height);

                        StringFormat format = new StringFormat();
                        format.LineAlignment = StringAlignment.Center;
                        format.Alignment = StringAlignment.Center;

                        int fontSize = GetCoverFontSize(coverTitle);
                        Font fontArial = new Font("Arial", fontSize, FontStyle.Bold, GraphicsUnit.Pixel);

                        Rectangle rect = new Rectangle(0, 25, coverBitMap.Width, coverBitMap.Height + (brokerageLogoBitMap.Height / 2));
                        gr.DrawString(coverTitle, fontArial, Brushes.Azure, rect, format);

                        var memoryStream = new MemoryStream();
                        ImageFinal.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);

                        string fileName = FileManager.CreateFileName("png");
                        return _blobservice.UploadAsync(BlobType.Image, memoryStream.GetBuffer(), fileName);
                    }
                }
            }
        }

        private int GetCoverFontSize(string coverTitle)
        {
            if (coverTitle.Length < 10)
                return 45;
            else if ((coverTitle.Length >= 10) && (coverTitle.Length < 20))
                return 40;
            else
                return 32;
        }
    }
}

