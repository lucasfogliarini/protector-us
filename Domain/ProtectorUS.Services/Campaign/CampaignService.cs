﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.Collections.ObjectModel;
    public class CampaignService : BaseService<Campaign>, ICampaignService
    {
        private readonly ICampaignRepository _campaignRepository;
        private readonly IQuotationRepository _quotationRepository;

        public CampaignService(
           ICampaignRepository campaignRepository,
           IQuotationRepository quotationRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                campaignRepository)
        {
            _campaignRepository = campaignRepository;
            _quotationRepository = quotationRepository;
        }
        
        public PagedResult<OutputCampaignItemDTO> GetAll(int page, int pageSize, string sort)
        {
            return TryCatch(() =>
            {
                var campaignsPagined = FindPaginated<Campaign>(page, pageSize, sort, null,
                    c => c.WebpageProduct,
                    c => c.WebpageProduct.BrokerageProduct,
                    c => c.WebpageProduct.BrokerageProduct.Product);

                var campaings = campaignsPagined.Items.Select(c => new OutputCampaignItemDTO()
                {
                    FilePath = (c as Brochure)?.FilePath,
                    CreatedDate = c.CreatedDate,
                    Id = c.Id,
                    Type = c.Type,
                    WebpageProduct = _mapper.Map<WebpageProductListItemDTO>(c.WebpageProduct)
                });
                return new PagedResult<OutputCampaignItemDTO>()
                {
                    Items = campaings.ToList().AsReadOnly(),
                    Page = campaignsPagined.Page,
                    ItemsPerPage = campaignsPagined.ItemsPerPage,
                    TotalItems = campaignsPagined.TotalItems,
                    Sort = campaignsPagined.Sort
                };
            });
        }


        public PagedResult<OutputCampaignItemDTO> FindAll(int page, int pageSize, string sort, Filters<Campaign> filters)
        {
            return TryCatch(() =>
            {
                var campaignsPagined = FindPaginated<Campaign>(page, pageSize, sort, filters,
                    c => c.Broker,
                    c => c.WebpageProduct,
                    c => c.WebpageProduct.BrokerageProduct,
                    c => c.WebpageProduct.BrokerageProduct.Product);

                var campaings = campaignsPagined.Items.Select(c => new OutputCampaignItemDTO()
                {
                    FilePath = (c as Brochure)?.FilePath,
                    CreatedDate = c.CreatedDate,
                    Id = c.Id,
                    Type = c.Type,
                    WebpageProduct = _mapper.Map<WebpageProductListItemDTO>(c.WebpageProduct)
                });
                return new PagedResult<OutputCampaignItemDTO>()
                {
                    Items = campaings.ToList().AsReadOnly(),
                    Page = campaignsPagined.Page,
                    ItemsPerPage = campaignsPagined.ItemsPerPage,
                    TotalItems = campaignsPagined.TotalItems,
                    Sort = campaignsPagined.Sort
                };
            });
        }

        public List<GraphicDTO> GetTotalbyType(Filters<Campaign> filter)
        {
            //return TryCatch(() =>
            //{
            //    int count = 0;
            //    List<GraphicDTO> result = new List<GraphicDTO>();
            //    var campaigns = _mainRepository.FindAll(filter, (p => p.Broker));
            //    List<long> listIds = campaigns.Select(x => x.Id).ToList();

            //    var quotations = _quotationRepository.GetAll(i => i.QuotationDeliveries, i => i.Campaign, i => i.Orders, i => i.Orders.Select(y => y.Policy));
            //    quotations = quotations.Where(x => x.Campaign != null && listIds.Contains(x.CampaignId.Value)).ToList();

            //    foreach (CampaignType item in Enum.GetValues(typeof(CampaignType)))
            //    {
            //        GraphicDTO graphic = new GraphicDTO();
            //        graphic.List = new List<ItemGraphicDTO>();
            //        graphic.Name = EnumExtensions.Description((CampaignType)item);

            //        var quotationsbyType = quotations.Where(x => x.Campaign.Type == item).ToList();
            //        var quotationsDelivery = quotationsbyType.Sum(x => x.QuotationDeliveries.Count());
            //        var quotationsbyActive = quotationsbyType.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();

            //        int total = campaigns.Where(x => x.Type == item).Count();
            //        count += total + quotationsDelivery + quotationsbyActive;
            //        ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Total", Value = total };
            //        graphic.List.Add(itemgraphic);

            //        ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Ignored", Value = quotationsDelivery - quotationsbyActive };
            //        graphic.List.Add(itemgraphic2);

            //        ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Clicked", Value = quotationsDelivery };
            //        graphic.List.Add(itemgraphic3);

            //        ItemGraphicDTO itemgraphic4 = new ItemGraphicDTO() { Key = "Completed", Value = quotationsbyActive };
            //        graphic.List.Add(itemgraphic4);

            //        result.Add(graphic);
            //    }

            //    return count > 0 ? result : new List<GraphicDTO>();
            //});
            return new List<GraphicDTO>();
        }
        public List<GraphicDTO> GetTotalbyTypeView(Filters<Campaign> filter)
        {
            //return TryCatch(() =>
            //{
            //    List<GraphicDTO> result = new List<GraphicDTO>();
            //    var campaigns = _mainRepository.FindAll(filter, (p => p.Broker));
            //    List<long> listIds = campaigns.Select(x => x.Id).ToList();

            //    var quotations = _quotationRepository.GetAll(i => i.QuotationDeliveries, i => i.Campaign, i => i.Orders, i => i.Orders.Select(y => y.Policy));
            //    quotations = quotations.Where(x => x.Campaign != null && listIds.Contains(x.CampaignId.Value));

            //    foreach (CampaignType item in Enum.GetValues(typeof(CampaignType)))
            //    {
            //        GraphicDTO graphic = new GraphicDTO();
            //        graphic.List = new List<ItemGraphicDTO>();
            //        graphic.Name = EnumExtensions.Description((CampaignType)item);

            //        var quotationsbyType = quotations.Where(x => x.Campaign.Type == item).ToList();
            //        var quotationsDelivery = quotationsbyType.Sum(x => x.QuotationDeliveries.Count());
            //        var quotationsbyActive = quotationsbyType.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();
            //        var premiumTotal = quotationsbyType.Sum(x => x.Orders.Where(y => y.Policy != null).Sum(u => u.Policy.TotalPremium));

            //        int total = campaigns.Where(x => x.Type == item).Count();
            //        decimal ratio = quotationsDelivery == 0 || quotationsbyActive == 0 ? 0 : ((quotationsbyActive * 100) / quotationsDelivery);
            //        ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Viewed", Value = quotationsDelivery };
            //        graphic.List.Add(itemgraphic);

            //        ItemGraphicDTO itemgraphic1 = new ItemGraphicDTO() { Key = "Bound", Value = quotationsbyActive };
            //        graphic.List.Add(itemgraphic1);

            //        ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Hit Ratio", Value = ratio };
            //        graphic.List.Add(itemgraphic3);

            //        ItemGraphicDTO itemgraphic4 = new ItemGraphicDTO() { Key = "Premium", Value = premiumTotal };
            //        graphic.List.Add(itemgraphic4);

            //        result.Add(graphic);
            //    };

            //    return result;
            //});

            return new List<GraphicDTO>();
        }
        public List<GraphicDTO> GetTotalbyTypePeriod(Filters<Campaign> filter, PeriodFilter period)
        {
            //return TryCatch(() =>
            //{
            //    int count = 0;
            //    DateTime datebegin = DateTime.Now;
            //    DateTime dateend = DateTime.Now.AddDays(1);

            //    int intervaleixo = 0;

            //    switch (period)
            //    {
            //        case PeriodFilter.Today:
            //            datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
            //            intervaleixo = 24;
            //            break;
            //        case PeriodFilter.Week:
            //            datebegin = DateTime.Now.AddDays(-7);
            //            datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
            //            intervaleixo = 7;
            //            break;
            //        case PeriodFilter.LastMonth:
            //            datebegin = DateTime.Now.AddMonths(-1);
            //            datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
            //            intervaleixo = Convert.ToInt32(dateend.Subtract(datebegin).TotalDays);
            //            break;
            //        case PeriodFilter.LastYear:
            //            datebegin = DateTime.Now.AddYears(-1);
            //            datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
            //            intervaleixo = 12;
            //            break;
            //    }

            //    filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

            //    List<GraphicDTO> result = new List<GraphicDTO>();

            //    var campaigns = _mainRepository.FindAll(filter, (p => p.Broker));
            //    List<long> listIds = campaigns.Select(x => x.Id).ToList();

            //    var quotations = _quotationRepository.GetAll(i => i.QuotationDeliveries, i => i.Campaign, i => i.Orders, i => i.Orders.Select(y => y.Policy));
            //    quotations = quotations.Where(x => x.Campaign != null && listIds.Contains(x.CampaignId.Value)).ToList();

            //    foreach (CampaignType item in Enum.GetValues(typeof(CampaignType)))
            //    {
            //        GraphicDTO graphic = new GraphicDTO();
            //        graphic.List = new List<ItemGraphicDTO>();
            //        graphic.Name = EnumExtensions.Description((CampaignType)item);

            //        var quotationsbyType = quotations.Where(x => x.Campaign.Type == item).ToList();
            //        var quotationsDelivery = quotationsbyType.Sum(x => x.QuotationDeliveries.Count());
            //        var quotationsbyActive = quotationsbyType.Where(x => x.Orders.Where(y => y.Policy != null).Count() > 0).Count();
            //        var premiumTotal = quotationsbyType.Sum(x => x.Orders.Where(y => y.Policy != null).Sum(u => u.Policy.TotalPremium));

            //        int total = campaigns.Where(x => x.Type == item).Count();

            //        count += quotationsDelivery + quotationsbyActive;

            //        decimal ratio = quotationsbyActive != 0 && quotationsDelivery != 0 ? ((quotationsbyActive * 100) / quotationsDelivery) : 0;
            //        ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Viewed", Value = quotationsDelivery };
            //        graphic.List.Add(itemgraphic);

            //        ItemGraphicDTO itemgraphic1 = new ItemGraphicDTO() { Key = "Bound", Value = quotationsbyActive };
            //        graphic.List.Add(itemgraphic1);

            //        ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = "Ratio", Value = ratio };
            //        graphic.List.Add(itemgraphic2);

            //        ItemGraphicDTO itemgraphic3 = new ItemGraphicDTO() { Key = "Total", Value = premiumTotal };
            //        graphic.List.Add(itemgraphic3);

            //        result.Add(graphic);
            //    };

            //    return count > 0 ? result : new List<GraphicDTO>();
            //});
            return new List<GraphicDTO>();
        }
    }
}

