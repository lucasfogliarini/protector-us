﻿using AutoMapper;
using System;

namespace ProtectorUS.Services
{
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using Model.Services;
    using System.IO;
    using System.Threading.Tasks;
    using Configuration;
    public class BrochureService : BaseService<Campaign>, IBrochureService
    {
        readonly ICampaignRepository _campaignRepository;
        readonly IPdfReplacerService _pdfReplacerService;
        readonly IWebpageProductRepository _webpageProductRepository;
        readonly IBlobService _blobservice;

        public BrochureService(
           ICampaignRepository campaignRepository,
           IWebpageProductRepository webpageProductRepository,
           IBlobService blobService,
           IPdfReplacerService pdfReplacerService,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                campaignRepository)
        {
            _campaignRepository = campaignRepository;
            _webpageProductRepository = webpageProductRepository;
            _blobservice = blobService;
            _pdfReplacerService = pdfReplacerService;
        }

        public void Create(InputBrochureDTO brochureIn, long brokerId, long brokerageId)
        {
            TryCatch(() =>
            {
                Brochure brochure = _mapper.Map<Brochure>(brochureIn);
                var webPageProduct = _webpageProductRepository.TryFind(x => x.BrokerageProduct.ProductId == brochure.ProductId && 
                                                                        x.BrokerageProduct.BrokerageId == brokerageId, 
                                                                        x => x.BrokerageProduct,
                                                                        x => x.BrokerageProduct.Product,
                                                                        x => x.BrokerageProduct.Brokerage);
                brochure.WebpageProductId = webPageProduct.Id;
                brochure.BrokerId = brokerId;

                _validationManager.ValidateAndThrowErrors(brochure);

                var brokerage = webPageProduct.BrokerageProduct.Brokerage;
                var product = webPageProduct.BrokerageProduct.Product;
                brochure.FilePath = GenerateBrochure(brochure.TemplateType, brokerage, product);

                _campaignRepository.Add(brochure);
                Commit();
            });
        }
        private string GenerateBrochure(BrochureTemplateType templateType, Brokerage brokerage, Product product)
        {
            string brochureTemplateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Brochure.brochureTemplatesFolder);

            string brochureTemplateFileName = templateType.Description();
            string brochureTemplateFilePath = Path.Combine(brochureTemplateFolderPath, brochureTemplateFileName);

            string brochureCreatedFileName = FileManager.CreateFileName("pdf");
            string brochureCreatedFilePath = Path.Combine(brochureTemplateFolderPath, brochureCreatedFileName);

            string urlQrcode = ProtectorDomain.BrokerageProductUrl(brokerage.Alias, product.Alias);
            byte[] brokerageLogoBytes = _blobservice.DownloadStream(BlobType.Image, brokerage.Logo).ToArray();

            _pdfReplacerService.ReplacePdfForm(brochureTemplateFilePath, brochureCreatedFilePath, urlQrcode, brokerageLogoBytes);

            byte[] brochureCreatedBytes = File.ReadAllBytes(brochureCreatedFilePath);
            File.Delete(brochureCreatedFilePath);

            return _blobservice.UploadAsync(BlobType.Brochure, brochureCreatedBytes, brochureCreatedFileName);
        }
    }
}

