﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Services.Authentication
{
    using Model;
    using Model.Repositories;
    using Exceptions;
    using Security.Cryptography;
    using Model.Services;
    using Diagnostics.Logging;
    using Model.Validation;
    using AutoMapper;
    using DTO;
    using System.Threading.Tasks;
    public class AuthenticationService : UserService<User>, IAuthenticationService
    {
        private readonly IAppClientService _appClientService;
        private readonly IInsuredRepository _insuredRepository;
        private readonly IBrokerRepository _brokerRepository;
        private readonly IManagerUserRepository _managerUserRepository;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserRepository _userRepository;

        public AuthenticationService(
            IEmailTemplateService emailTemplate,
            IUserRepository userRepository,
            IProfileRepository profileRepository,
            IPolicyRepository policyRepository,
            IInsuredRepository insuredRepository,
            IManagerUserRepository managerUserRepository,
            IBrokerRepository brokerRepository,
            IAppClientService appClientService,
            IRefreshTokenRepository refreshTokenRepository,
            IUnitOfWork unitOfWork,
            ILogger logger,
            IValidationManager validationManager,
            IMappingEngine mapper) : 
            base(emailTemplate,
                profileRepository, 
                policyRepository,
                unitOfWork, 
                logger, 
                validationManager, 
                mapper, 
                userRepository)
        {
            _appClientService = appClientService;
            _insuredRepository = insuredRepository;
            _brokerRepository = brokerRepository;
            _managerUserRepository = managerUserRepository;
            _userRepository = userRepository;
            _refreshTokenRepository = refreshTokenRepository;
        }
        public TUser Authenticate<TUser>(string email, string password) where TUser : User
        {
            return TryCatch(() =>
            {
                if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
                    throw new ArgumentInvalidException(e=>e.ES027);
                User user;
                Type userType = typeof(TUser);
                if (userType == typeof(Insured))
                {
                    user = _insuredRepository.TryFind(u => u.Email == email, e=> e.Profiles, e=>e.Devices);
                }
                else if (userType == typeof(Broker))
                {
                    user = _brokerRepository.TryFind(u => u.Email == email, e => e.Profiles, e => e.Devices);
                }
                else if (userType == typeof(ManagerUser))
                {
                    user = _managerUserRepository.TryFind(u => u.Email == email, e => e.Profiles, e => e.Devices);
                }
                else
                {
                    user = _mainRepository.TryFind(u => u.Email == email, e => e.Profiles, e => e.Devices);
                }

                if (!user.PasswordMatch(password))
                    throw new ArgumentInvalidException(e => e.ES001);
                return user as TUser;
            });            
        }
        public IEnumerable<OutputPermissionDTO> FindPermissions(long userId)
        {
            return TryCatch(() =>
            {
               var permissions = _userRepository.GetPermissions(userId);
                return _mapper.Map<IEnumerable<OutputPermissionDTO>>(permissions);
            });            
        }
        public OutputAppClientDTO TryFindAppClient(string appClientId)
        {
            return TryCatch(() =>
            {
                return _appClientService.TryFind(appClientId);
            });
        }
        public RefreshToken CreateToken(string appClientId, string identityName, DateTime issuedDate, DateTime expiresDate, string protectedTicket)
        {
            return TryCatch(() =>
            {
                var refreshTokensObsolete = _refreshTokenRepository.FindAll(rt => rt.IdentityName == identityName && rt.AppClientId == appClientId);
                refreshTokensObsolete.ForEach(rt => _refreshTokenRepository.Remove(rt));

                var refreshToken = new RefreshToken()
                {
                    RefreshTokenId = Hasher.NewSHA256Hash(),
                    AppClientId = appClientId,
                    IdentityName = identityName,
                    IssuedDate = issuedDate,
                    ExpiresDate = expiresDate,
                    ProtectedTicket = protectedTicket
                };
                _refreshTokenRepository.Add(refreshToken);
                Commit();

                return refreshToken;
            });
        }
        public RefreshToken RefreshToken(RefreshToken refreshingToken, DateTime newIssuedDate, DateTime newExpiresDate, string newProtectedTicket)
        {
            return TryCatch(() =>
            {
                refreshingToken.ProtectedTicket = newProtectedTicket;
                refreshingToken.IssuedDate = newIssuedDate;
                refreshingToken.ExpiresDate = newExpiresDate;
                refreshingToken.RefreshTokenId = Hasher.NewSHA256Hash();
                _refreshTokenRepository.Update(refreshingToken);
                Commit();
                return refreshingToken;
            });
        }
        public RefreshToken GetRefreshToken(string refreshTokenId)
        {
            return TryCatch(() =>
            {
                return _refreshTokenRepository.Find(rt => rt.RefreshTokenId == refreshTokenId);
            });
        }        
        public void AddDevice(User user, string deviceId)
        {
            TryCatch(() =>
            {
                if (string.IsNullOrWhiteSpace(deviceId))
                {
                    throw new ArgumentRequiredException(e=>e.EL042);
                }
                Device device = new Device(deviceId, "", user.Id);
                bool hasDevice = user.Devices.Any(d => d.DeviceId == deviceId);
                if (!hasDevice)
                {
                    user.Devices.Add(device);
                }
                _userRepository.Update(user);
            });
        }
        public OutputBrokerDTO GetMasterByBrokerage(long brokerageId)
        {
            return TryCatch(() =>
            {
                var _brokerOut = _brokerRepository.GetMasterByBrokerage(brokerageId);
                return _mapper.Map<OutputBrokerDTO>(_brokerOut);
            });
        }
    }
}
