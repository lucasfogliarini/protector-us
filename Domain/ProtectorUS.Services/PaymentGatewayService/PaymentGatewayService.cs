﻿using ProtectorUS.Diagnostics.Logging;

namespace ProtectorUS.Services
{
    using Integrations.Stripe;
    using Model;
    using Model.Services;
    public class PaymentGatewayService : BaseService, IPaymentGatewayService
    {
        IBrokerageService _brokerageService;
        public PaymentGatewayService(
            IBrokerageService brokerageService,
            ILogger logger) : 
            base(logger)
        {
            _brokerageService = brokerageService;
        }

        public void UpdateChargeAccountStatus(string paymentGatewayBody)
        {
            TryCatch(() =>
            {
                ChargeAccount chargeAccount = StripeApi.ToChargeAccount(paymentGatewayBody);
                _brokerageService.UpdateChargeAccountStatus(chargeAccount);
            });
        }
    }
}