﻿using AutoMapper;
using System.Linq;

namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Model;
    using DTO;
    using Exceptions;
    using System.Linq.Expressions;
    using System;
    using Model.Services;
    using Communication;
    using Security.Cryptography;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Configuration;
    public class UserService : UserService<User>
    {
        public UserService(
            IEmailTemplateService emailTemplateService,
            IProfileRepository profileRepository,
            IPolicyRepository policyRepository,
            IUnitOfWork unitOfWork,
            ILogger logger,
            IValidationManager validationManager,
            IMappingEngine mapper,
            IUserRepository userRepository) :
                base(
                    emailTemplateService,
                    profileRepository,
                policyRepository,
                unitOfWork,
                logger,
                validationManager,
                mapper,
                userRepository)
        {
        }
    }

    public abstract class UserService<TUser> : BaseService<TUser>, IUserService<TUser> where TUser : User
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IUserRepository<TUser> _userRepository;
        private readonly IPolicyRepository _policyRepository;
        protected readonly IEmailTemplateService _emailTemplateService;

        public UserService(
           IEmailTemplateService emailTemplateService,
           IProfileRepository profileRepository,
           IPolicyRepository policyRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper,
           IUserRepository<TUser> userRepository) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
               userRepository)
        {
            _emailTemplateService = emailTemplateService;
            _userRepository = userRepository;
            _profileRepository = profileRepository;
            _policyRepository = policyRepository;
        }
        public bool IsUsed(string email, string exceptEmail = null)
        {
            return TryCatch(() =>
            {
                return _userRepository.IsUsed(email, exceptEmail);
            });
        }
        public bool IsUsed(string email, long exceptUserId)
        {
            return TryCatch(() =>
            {
                var user = _mainRepository.Find(x => x.Email == email && x.Id != exceptUserId);
                return user != null;
            });
        }        
        public MessageCode ChangePassword(long id, string password)
        {
            return TryCatch(() =>
            {
                TUser insured = _mainRepository.TryGet(id);
                insured.GeneratePassword(password);
                _validationManager.Validate(insured);
                _mainRepository.Update(insured);
                Commit();
                return new MessageCode(e => e.S004);
            });
        }
        public string ChangePicture(long userId, string fileName)
        {
            return TryCatch(() =>
            {
                var user = _mainRepository.TryGet(userId);
                user.Picture = fileName;
                _validationManager.Validate(user);
                _mainRepository.Update(user);
                Commit();
                return user.Picture;
            });
        }
        public TUserOut FindUser<TUserOut>(string email, params Expression<Func<TUser, object>>[] includeExpressions) where TUserOut : IOutputUserDTO
        {
            return TryCatch(() =>
            {
                var user = _mainRepository.TryFind(x => x.Email == email, includeExpressions);
                return _mapper.Map<TUserOut>(user);
            });
        }
        protected TUser Onboarding(long userId)
        {
            return TryCatch(() =>
            {
                TUser user = _mainRepository.TryGet(userId, 
                    x => x.Addresses, 
                    x => x.Addresses.Select(y => y.Region), 
                    x => x.Addresses.Select(y => y.Region.City), 
                    x => x.Addresses.Select(y => y.Region.City.State),
                    x => x.Phones);

                if (user.Status != UserStatus.Onboarding)
                {
                    throw new ArgumentInvalidException(e => e.EL029);
                }
                return user;
            });
        }
        protected TUser Updating(long userId, InputUserDTO userIn)
        {
            return TryCatch(() =>
            {
                TUser user = _mainRepository.TryGet(userId);
                user.FirstName = userIn.FirstName;
                user.LastName = userIn.LastName;
                user.Picture = userIn.Picture;
                user.SocialTitle = userIn.SocialTitle;
                if (user.Status == UserStatus.Onboarding) user.Status = UserStatus.Active;
                return user;
            });
        }
        protected void AddExistingProfile(TUser user, params long[] profileIds)
        {
            //validation
            foreach (long profileId in profileIds)
            {
                if (user is Broker && !Profile.Ids.BrokerProfiles.Contains(profileId)) throw new ArgumentInvalidException(e => e.EL035);
                if (user is ManagerUser && !Profile.Ids.ManagerUserProfiles.Contains(profileId)) throw new ArgumentInvalidException(e => e.EL035);
                if (user is Insured && Profile.Ids.Insured != profileId) throw new ArgumentInvalidException(e => e.EL035);
            }
            user.Profiles = user.Profiles ?? new List<Profile>();
            foreach (long profileId in profileIds)
            {
                if (user.Profiles.Any(p => p.Id == profileId)) continue;
                var profile = _profileRepository.TryGet(profileId);
                user.Profiles.Add(profile);
            }
        }
        public OutputUserLoginDTO FindUserLogin(string email)
        {
            return TryCatch(() =>
            {
                var user = _mainRepository.TryFind(x => x.Email == email);
                return _mapper.Map<OutputUserLoginDTO>(user);
            });
        }
        private TUser TryGetUserRecovery(long userId, string token)
        {
            var user = _mainRepository.TryGet(userId);
            if (PBKDF2Hasher.Match(token, user.Password))
            {
                return user;
            }
            throw new InvalidException(e => e.EL033);
        }
        public MessageCode ResetPasswordSendInstructions(string email)
        {
            return TryCatch(() =>
            {
                var user = _mainRepository.TryFind(x => x.Email == email);

                string token = PBKDF2Hasher.GenerateHash(user.Password);
                
                string body = _emailTemplateService.GetForgotPasswordBody(user, token, GetProtectorApp());
          
                _emailTemplateService.Send(EmailTemplate.ForgotPassword.Id, email, body);
                return new MessageCode(e => e.S002);
            });
        }

        private ProtectorApp GetProtectorApp()
        {
            if (typeof(TUser) == typeof(ManagerUser))
            {
                return ProtectorApp.ArgoManager;
            }
            else if (typeof(TUser) == typeof(Broker))
            {
                return ProtectorApp.BrokerCenter;
            }
            else if (typeof(TUser) == typeof(Insured))
            {
                return ProtectorApp.MyProtector;
            }
            return ProtectorApp.MyProtector;
        }
        public MessageCode ResetPassword(long userId, string token, string newPassword = null)
        {
            return TryCatch(() =>
            {
                var user = TryGetUserRecovery(userId, token);
                if (newPassword == null)//only verifying
                {
                    return new MessageCode(e => e.S001);
                }

                user.GeneratePassword(newPassword);
                _mainRepository.Update(user);
                _validationManager.Validate(user);
                Commit();
                return new MessageCode(e => e.S003);
            });
        }
        public Profile TryGetProfile(long profileId)
        {
            return _profileRepository.TryGet(profileId);
        }
        public string[] GetPermissions(long userId, bool allowed = true)
        {
            return TryCatch(() =>
            {
                IEnumerable<Permission> permissions = _userRepository.GetPermissions(userId);
                IEnumerable<string> permissionKeys = permissions.Select(p => p.Key);
                if (!allowed)
                {
                    string userTypeName = typeof(TUser).Name;
                    string[] allPermissions = { };
                    if (userTypeName == typeof(Broker).Name)
                    {
                        allPermissions = Profile.BrokerPermissions.AllPermissions;
                    }
                    else if (userTypeName == typeof(ManagerUser).Name)
                    {
                        allPermissions = Profile.ManagerUserPermissions.AllPermissions;
                    }

                    permissionKeys = allPermissions.Where(perm => !permissions.Any(p => p.ActionApi == perm)).Select(p => p.Simplify());
                }

                return permissionKeys.ToArray();
            });
        }
    }
}
