﻿using AutoMapper;

namespace ProtectorUS.Services
{
    using DTO;
    using Model.Services;
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using System.Collections.Generic;

    public class CompanyService : BaseService<Company>, ICompanyService
    {
        public CompanyService(IUnitOfWork unitOfWork,
           ICompanyRepository companyRepository,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                companyRepository
                )
        {
        }

        public IEnumerable<CompanyDTO> GetAll()
        {
            return TryCatch(() =>
            {
                var companies = _mainRepository.GetAll();
                return _mapper.Map<IEnumerable<CompanyDTO>>(companies);
            });
        }
    }
}
