﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Services
{
    using Model.Services;
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using ProtectorUS.DTO;
    using System.Threading.Tasks;

    public class BrokerService : UserService<Broker>, IBrokerService
    {
        readonly IBrokerRepository _brokerRepository;
        readonly IBrokerageRepository _brokerageRepository;
        readonly IEmailTemplateService _emailTemplateService;
        readonly IQuotationRepository _quotationRepository;
        readonly ICampaignRepository _campainRepository;
        readonly IPolicyRepository _policyRepository;

        public BrokerService(
           IBrokerRepository brokerRepository,
           IBrokerageRepository brokerageRepository,
           IEmailTemplateService emailTemplateService,
           IProfileRepository profileRepository,
           IPolicyRepository policyRepository,
           IQuotationRepository quotationRepository,
           ICampaignRepository campainRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
               emailTemplateService,
                profileRepository,
                policyRepository,
                unitOfWork,
                logger,
                validationManager,
                mapper, 
                brokerRepository)
        {
            _emailTemplateService = emailTemplateService;
            _brokerRepository = brokerRepository;
            _policyRepository = policyRepository;
            _quotationRepository = quotationRepository;
            _campainRepository = campainRepository;
            _brokerageRepository = brokerageRepository;
        }

        public OutputProfileBrokerDTO Get(long brokerId)
        {
            return  TryCatch( () =>
            {
                Broker broker =  _brokerRepository.TryGet(brokerId, 
                                                        x => x.Profiles, 
                                                        x => x.Brokerage,
                                                        x => x.Phones,
                                                        x => x.Brokerage.Address,
                                                        x => x.Brokerage.Address.Region);
                return _mapper.Map<OutputProfileBrokerDTO>(broker);
            });

        }
        public OutputProfileBrokerDTO Update(long brokerId, BrokerSelfUpdateDTO brokerIn)
        {
            return  TryCatch( () =>
            {
                Broker broker = base.Updating(brokerId, brokerIn);
                if (IsUsed(brokerIn.Email, broker.Email))
                {
                    _validationManager.AddExistingUserError(broker);
                }
                broker.Email = brokerIn.Email;
                if (brokerIn.Password != null)
                    broker.GeneratePassword(brokerIn.Password);
                _validationManager.Validate(broker);

                _mainRepository.Update(broker);
                 Commit();

                return _mapper.Map<OutputProfileBrokerDTO>(broker);
            });
        }
        public OutputBrokerDTO Delete(long brokerId)
        {
            return  TryCatch( () =>
            {
                var broker = _brokerRepository.TryGet(brokerId);
                if (broker != null)
                {
                    broker.Delete();
                    _brokerRepository.Remove(broker);
                }
                 Commit();
                return _mapper.Map<OutputBrokerDTO>(broker);
            });
        }
        public PagedResult<OutputBrokerListDTO> FindAll(int page, int pageSize, string sort, Filters<Broker> filters = null)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputBrokerListDTO>(page, pageSize, sort, filters,
                          i => i.Policies,
                          i => i.Campaigns,
                          i => i.Profiles,
                          i => i.Phones,
                          i => i.Addresses,
                          i => i.Addresses.Select(a=>a.Region),
                          i => i.Addresses.Select(a => a.Region.City));
            });
        }
        public new OutputBrokerOnboardDTO Onboarding(long brokerId)
        {
            return TryCatch(() =>
            {
                Broker broker = base.Onboarding(brokerId);

                return _mapper.Map<OutputBrokerOnboardDTO>(broker);
            });
        }
        public GraphicDTO GetTotalTypes(Filters<Broker> filter)
        {
            return TryCatch(() =>
            {
                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();

                var brokers = _mainRepository.FindAll(filter);

                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = EnumExtensions.Description(UserStatus.Active), Value = brokers.Where(x => x.Status == UserStatus.Active).Count() };
                graphic.List.Add(itemgraphic);

                ItemGraphicDTO itemgraphic2 = new ItemGraphicDTO() { Key = EnumExtensions.Description(UserStatus.Inactive), Value = brokers.Where(x => x.Status == UserStatus.Inactive).Count() };
                graphic.List.Add(itemgraphic2);

                return graphic;
            });
        }
        public GraphicDTO GetTotalLastMonth(Filters<Broker> filter)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddMonths(-1);
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                var insureds = _mainRepository.FindAll(filter);

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = "Last Month", Value = insureds.Count() };
                graphic.List.Add(itemgraphic);

                return graphic;
            });
        }
        public List<GraphicDTO> GetTotalLastYear(Filters<Broker> filter)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var insureds = _mainRepository.FindAll(filter);

                DateTime dateaux = datebegin;

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                graphic.Name = "New Brokers";
                int numberMonth = 12;
                while (numberMonth > 0)
                {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM/yy"), Value = insureds.Where(x => x.CreatedDate.Month == dateaux.Month && x.CreatedDate.Year == dateaux.Year).Count() };
                    graphic.List.Add(itemgraphic);

                    dateaux = dateaux.AddMonths(1);
                    numberMonth--;
                }
                result.Add(graphic);

                return result;
            });
        }
        public bool IsMember(long brokerId, long brokerageId)
        {
            return TryCatch(() =>
            {
                var broker = _mainRepository.Find(b => b.Id == brokerId && b.BrokerageId == brokerageId);
                return broker != null;
            });
        }
        public async Task<OutputProfileBrokerDTO> UpdateAsync(long brokerId, BrokerSelfUpdateDTO brokerIn)
        {
            return await TryCatch(async() =>
            {
                Broker broker = base.Updating(brokerId, brokerIn);
                if (IsUsed(brokerIn.Email, broker.Email))
                {
                    _validationManager.AddExistingUserError(broker);
                }
                broker.Email = brokerIn.Email;
                if (brokerIn.Password != null)
                    broker.GeneratePassword(brokerIn.Password);
                _mainRepository.Update(broker);
                _validationManager.Validate(broker);
                await CommitAsync();

                return _mapper.Map<OutputProfileBrokerDTO>(broker);
            });
        }
        public async Task<PagedResult<OutputBrokerListDTO>> FindAllAsync(int page, int pageSize, string sort, Filters<Broker> filters = null)
        {
            return await TryCatch(async() =>
            {
                return await FindPaginatedAsync<OutputBrokerListDTO>(page, pageSize, sort, filters,
                          i => i.Policies,
                          i => i.Campaigns,
                          i => i.Profiles);
            });
        }
        public async Task<OutputProfileBrokerDTO> UpdateAsync(long id, BrokerUpdateDTO brokerIn)
        {
            return await TryCatch(async() =>
            {
                Broker broker = base.Updating(id, brokerIn);
                if (brokerIn.Status != null)
                    broker.Status = brokerIn.Status.GetValueOrDefault();

                if (IsUsed(brokerIn.Email, broker.Email))
                {
                    _validationManager.AddExistingUserError(broker);
                }
                broker.Email = brokerIn.Email;
                _mainRepository.Update(broker);
                _validationManager.Validate(broker);
                await CommitAsync();

                return _mapper.Map<OutputProfileBrokerDTO>(broker);
            });
        }
        public async Task<OutputBrokerDTO> CreateAsync(long brokerageId, BrokerInsertDTO brokerIn)
        {
            return await TryCatch(async() =>
            {
                Broker broker = _mapper.Map<Broker>(brokerIn);
                if(IsUsed(broker.Email))
                    _validationManager.AddExistingUserError(broker);

                broker.BrokerageId = brokerageId;
                AddExistingProfile(broker, brokerIn.ProfileId);
                broker.Status = UserStatus.Onboarding;
                broker.GeneratePassword();
                _validationManager.Validate(broker);
                _mainRepository.Add(broker);
                await CommitAsync();
                broker.Brokerage = _brokerageRepository.Get(brokerageId);

                string body = _emailTemplateService.GetWelcomeBrokerBody(broker);
                _emailTemplateService.Send(EmailTemplate.WelcomeBroker.Id, broker.Email, body);

                return _mapper.Map<OutputBrokerDTO>(broker);
            });
        }
        public async Task<OutputProfileBrokerDTO> GetAsync(long brokerId)
        {
            return await TryCatch(async() =>
            {
                Broker broker = await _brokerRepository.TryGetAsync(brokerId, x => x.Profiles, x => x.Brokerage, a => a.Phones);
                return _mapper.Map<OutputProfileBrokerDTO>(broker);
            });
        }
        public async Task<OutputBrokerDTO> DeleteAsync(long brokerId)
        {
            return await TryCatch(async() =>
            {
                var broker = _brokerRepository.TryGet(brokerId);
                if (broker != null)
                {
                    broker.Delete();
                    _brokerRepository.Remove(broker);
                }
                await CommitAsync();
                return _mapper.Map<OutputBrokerDTO>(broker);
            });
        }
        public async Task<OutputBrokerInactivateReactivate> InactivateReactivate()
        {
            var totals = new OutputBrokerInactivateReactivate();
            return await TryCatch(async () =>
            {
                var activesByCampain = _campainRepository.FindAll(x => x.CreatedDate >= DateTime.Now, c => c.Broker).Select(x => x.Broker);
                var activesByQuotation = _quotationRepository.FindAll(x => x.CreatedDate >= DateTime.Now, c => c.Broker).Select(x => x.Broker);
                var activesByPolicies = _policyRepository.FindAll(x => x.CreatedDate >= DateTime.Now, c => c.Broker).Select(x => x.Broker);

                var actives = activesByCampain.Concat(activesByPolicies).Concat(activesByQuotation).Select(x => x.Id).Distinct();
                var inactivateds = _brokerRepository.Inactivate(actives);
                var reactivateds = _brokerRepository.Reactivate(actives);

                totals.Activateds = reactivateds;
                totals.Inactivateds = inactivateds;

                await CommitAsync();
                return totals;
            });
        }
    }
}
