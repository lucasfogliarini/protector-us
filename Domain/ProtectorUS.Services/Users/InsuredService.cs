﻿using AutoMapper;
using System;
using System.Linq;

namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Model;
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Exceptions;

    public class InsuredService : UserService<Insured>, IInsuredService
    {
        readonly IPolicyRepository _policyRepository;
        readonly IInsuredRepository _insuredRepository;
        public InsuredService(
            IEmailTemplateService emailTemplateService,
           IInsuredRepository insuredRepository,
           IProfileRepository profileRepository,
           IPolicyRepository policyRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
               emailTemplateService,
                profileRepository,
                policyRepository,
                unitOfWork,
                logger,
                validationManager,
                mapper,
                insuredRepository)
        {
            _policyRepository = policyRepository;
            _insuredRepository = insuredRepository;
        }

        public bool CheckInsuredExists(string email, string productAlias = null)
        {
            return TryCatch(() =>
            {
                var insured = _insuredRepository.Find(x => x.Email == email);
                bool exists = insured != null;
                if (exists && productAlias != null)
                {
                    Policy policy = _policyRepository.Find(x => x.InsuredId == insured.Id && x.Condition.Product.Alias == productAlias,
                        i => i.Condition,
                        c => c.Condition.Product);
                    exists = policy != null;
                }
                return exists;
            });
        }

        public bool CheckTerms(long insuredId)
        {
            return TryCatch(() =>
            {
                var insured = _insuredRepository.Find(x => x.Id == insuredId);
                var accepted = insured.AcceptedTerms ?? false;
                if (!accepted)
                {
                    throw new TermsNotAcceptableException(insuredId);
                }
                return accepted;
            });
        }


        public bool AcceptTerms(long insuredId, bool accept)
        {
            return TryCatch(() =>
            {
                var insured = _insuredRepository.Find(x => x.Id == insuredId);
                insured.AcceptedTerms = accept;
                _mainRepository.Update(insured);
                _validationManager.Validate(insured);
                Commit();

                return accept;
            });
        }

        public OutputInsuredDTO Update(long id, InsuredSelfUpdateDTO insuredIn)
        {
            return TryCatch( () =>
            {
                Insured insured = Updating(id, insuredIn);
                if (IsUsed(insuredIn.Email, insured.Email))
                {
                    _validationManager.AddExistingUserError(insured);
                }

                insured.Email = insuredIn.Email;
                insured.GeneratePassword(insuredIn.Password);
                _mainRepository.Update(insured);
                _validationManager.Validate(insured);
                 Commit();

                return _mapper.Map<OutputInsuredDTO>(insured);
            });
        }
        public OutputInsuredDTO Update(long insuredId, InputInsuredProfileDTO insuredIn)
        {
            return TryCatch(() =>
            {
                var insured = _mainRepository.TryGet(insuredId);
                if (IsUsed(insuredIn.Email, insured.Email))
                {
                    _validationManager.AddExistingUserError(insured);
                }
                insured.Email = insuredIn.Email;
                insured.GeneratePassword(insuredIn.Password);
                _mainRepository.Update(insured);
                 Commit();

                return _mapper.Map<OutputInsuredDTO>(insured);
            });
        }
        public void Update(long id, UpdateInsuredByBrokerDTO insuredIn)
        {
            TryCatch(() =>
            {
                var insured = _mainRepository.TryGet(id, x=>x.Phones);
                if (insuredIn.Email != null)
                {
                    if (IsUsed(insuredIn.Email, insured.Email))
                    {
                        _validationManager.AddExistingUserError<Insured>(insuredIn.Email);
                    }
                    insured.Email = insuredIn.Email;
                }

                //Update the current phone types
                if (insuredIn.Phone != null)
                {
                    var phone = _mapper.Map<Phone>(insuredIn.Phone);
                    _validationManager.Validate(phone);

                    if(insured.Phones != null && insured.Phones.Where(x => x.Type == phone.Type).FirstOrDefault() != null)
                    {
                        insured.Phones.Where(x => x.Type == phone.Type).FirstOrDefault().Number = phone.Number;

                    }
                    else
                    {

                        insured.Phones = new List<Phone>()
                        {
                            phone
                        };

                    }

                   
                }
                _mainRepository.Update(insured);
                Commit();
            });
        }
        public OutputInsuredDTO Get(long id)
        {
            return  TryCatch( () =>
            {
                var inputInsured = _mainRepository.TryGet(id,
                    i => i.Phones,
                    i => i.Policies,
                    i => i.Declineds,
                    i => i.Policies.Select(p=> p.Order),
                    i => i.Policies.Select(p => p.Order.Quotation),
                    i => i.Policies.Select(p => p.Condition),
                    i => i.Policies.Select(p => p.Condition.Product),
                    i => i.Policies.Select(p => p.Broker),
                    i => i.Policies.Select(p => p.Broker.Phones),
                    i => i.Policies.Select(p => p.Broker.Brokerage));

                return _mapper.Map<OutputInsuredDTO>(inputInsured);
            });
        }
        public MyProtectorMobileOutputInsuredDTO GetProfilePoliciesClaimsCoverages(long insuredId)
        {
            return  TryCatch( () =>
            {
                var inputInsured = _mainRepository.TryGet(insuredId,
                   i => i.Phones,
                   i => i.Declineds,
                   i => i.Addresses,
                   i => i.Addresses.Select(y => y.Region),
                   i => i.Addresses.Select(y => y.Region.City),
                   i => i.Addresses.Select(y => y.Region.City.State),
                   i => i.Policies,
                   i => i.Policies.Select(p => p.Order),
                   i => i.Policies.Select(p => p.Order.PaymentMethod),
                   i => i.Policies.Select(p => p.Condition),
                   i => i.Policies.Select(p => p.Condition.Product),
                   i => i.Policies.Select(p => p.Broker),
                   i => i.Policies.Select(p => p.Broker.Phones),
                   i => i.Policies.Select(p => p.Broker.Brokerage),
                   i => i.Policies.Select(p => p.Claims),
                   i => i.Policies.Select(x => x.Claims.Select(m => m.ClaimDocuments)),
                   i => i.Policies.Select(x => x.Claims.Select(m => m.ThirdParty)),
                   i => i.Policies.Select(p => p.Coverages));

                return _mapper.Map<MyProtectorMobileOutputInsuredDTO>(inputInsured);
            });
        }

        public MyProtectorOutputInsuredDTO GetProfilePolicies(long insuredId)
        {
            return  TryCatch( () =>
            {
                var inputInsured =  _mainRepository.TryGet(insuredId,
                   i => i.Phones,
                   i => i.Declineds,
                   i => i.Policies,
                   i => i.Policies.Select(p => p.Order),
                   i => i.Policies.Select(p => p.Condition),
                   i => i.Policies.Select(p => p.Condition.Product),
                   i => i.Policies.Select(p => p.Broker),
                   i => i.Policies.Select(p => p.Broker.Phones),
                   i => i.Policies.Select(p => p.Broker.Brokerage));

                return _mapper.Map<MyProtectorOutputInsuredDTO>(inputInsured);
            });
        }
        public IEnumerable<UserNameDTO> FindAll(string name, long? brokerageId = null)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                if(brokerageId != null)
                     filters.Add(i => i.Policies.Any(p => p.Broker.BrokerageId == brokerageId));
                filters.Add(i => i.FullName.Contain(name));
                filters.Add(i => i.Status == UserStatus.Active);

                var insureds = _insuredRepository.FindAll(filters,
                    i => i.Policies,
                    i => i.Policies.Select(p => p.Broker));
                return _mapper.Map<IEnumerable<UserNameDTO>>(insureds);
            });
        }
        public PagedResult<OutputInsuredDTO> GetAll(int page, int pageSize, string sort)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputInsuredDTO>(page, pageSize, sort, null,
                    i => i.Policies,
                    i => i.Declineds,
                    i => i.Policies.Select(p => p.Broker),
                    i => i.Policies.Select(p => p.Broker.Brokerage));
            });
        }
        public MapPointDTO GetMapPoints(Filters<Insured> filters = null)
        {
            return TryCatch(() =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();
                map.RequestInfo = "/insured/{id}";
                filters = filters ?? new Filters<Insured>();
                var insureds = _mainRepository.FindAll(filters,
                    i => i.Addresses,
                    i => i.Policies,
                    i => i.Policies.Select(a => a.Condition));

                foreach (var item in insureds)
                {
                    if (item.Policies != null && item.Policies.Count() > 0)
                    {

                        PointDTO pinitem = new PointDTO() { Id = item.Id, Name = item.FullName, idProduct = item.Policies[0].Condition.ProductId, Image = item.Picture, Latitude = item.Addresses[0].Latitude.Value, Longitude = item.Addresses[0].Longitude.Value };
                        map.Pins.Add(pinitem);
                    }
                };

                return map;
            });
        }
        public GraphicDTO CountByStatus(long? brokerageId = null)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                if(brokerageId != null)
                    filters.Add(x => x.Policies.Any(p => p.Broker.BrokerageId == brokerageId));
                //filters.Add(x => x.Policies.OrderByDescending(p => p.CreatedDate).FirstOrDefault().Status == PolicyStatus.InEffect);

                var insureds = _mainRepository.FindAll(filters, 
                    p => p.Policies,
                    p => p.Policies.Select(x => x.Broker));

                GraphicDTO count = new GraphicDTO();
                count.List = new List<ItemGraphicDTO>();
                foreach (UserStatus status in Enum.GetValues(typeof(UserStatus)))
                {
                    count.List.Add(new ItemGraphicDTO()
                    {
                        Key = status.Description(),
                        Value = insureds.Count(x => x.Status == status)
                    });
                };

                return count;
            });
        }
        public List<ItemGraphicDTO> GetTotalLastYear(Filters<Insured> filter)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<ItemGraphicDTO> result = new List<ItemGraphicDTO>();

                var insureds = _mainRepository.FindAll(filter, 
                    p => p.Policies, 
                    p => p.Policies.Select(x => x.Broker),
                    p => p.Policies.Select(x => x.Business));

                    DateTime dateaux = datebegin;
                    int numberMonth = 12;
                    while (numberMonth > 0)
                    {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() {
                        Key = dateaux.ToString("MMM/yy"),
                        Value = insureds.Where(x => x.CreatedDate.Month == dateaux.Month
                        && x.CreatedDate.Year == dateaux.Year).Count()
                    };
                        result.Add(itemgraphic);

                        dateaux = dateaux.AddMonths(1);
                        numberMonth--;
                    }

                return result;
            });
        }

        public List<GraphicDTO> GetTotalLastYearByType(Filters<Insured> filter)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now.AddYears(-1).AddMonths(1);
                datebegin = new DateTime(datebegin.Year, datebegin.Month, 1);
                DateTime dateend = DateTime.Now.AddDays(1);

                filter.Add(x => x.CreatedDate > datebegin && x.CreatedDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var insureds = _mainRepository.FindAll(filter,
                    p => p.Policies,
                    p => p.Policies.Select(x => x.Broker),
                    p => p.Policies.Select(x => x.Business));

                foreach (BusinessType item in Enum.GetValues(typeof(BusinessType)))

                {
                    DateTime dateaux = datebegin;

                    GraphicDTO graphic = new GraphicDTO();
                    graphic.List = new List<ItemGraphicDTO>();
                    graphic.Name = EnumExtensions.Description((BusinessType)item);
                    int numberMonth = 12;
                    while (numberMonth > 0)
                    {
                        ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = dateaux.ToString("MMM/yy"), Value = insureds.Where(x => x.CreatedDate.Month == dateaux.Month && x.CreatedDate.Year == dateaux.Year && x.Policies?.FirstOrDefault()?.Business.Type == item).Count() };
                        graphic.List.Add(itemgraphic);

                        dateaux = dateaux.AddMonths(1);
                        numberMonth--;
                    }
                    result.Add(graphic);
                };

                return result;
            });
        }

        public new OutputInsuredDTO Onboarding(long id)
        {
            return TryCatch(() =>
            {
                var insured = base.Onboarding(id);
                return _mapper.Map<OutputInsuredDTO>(insured);
            });
        }

        public bool IsOwnCustomer(long brokerageId, long insuredId)
        {
            return TryCatch(() =>
            {
                var insured = _mainRepository.Find(i => i.Id == insuredId && i.Policies.Any(p => p.Broker.BrokerageId == brokerageId), 
                                                e => e.Policies, i => i.Policies.Select(p => p.Broker));
                return insured != null;
            });
        }

        public PagedResult<OutputInsuredListDTO> FindAll(int page, int pageSize, string sort, Filters<Insured> filters)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputInsuredListDTO>(page, pageSize, sort, filters,
                    i => i.Addresses,
                    i => i.Addresses.Select(a => a.Region),
                    i => i.Addresses.Select(a => a.Region.City),
                    i => i.Addresses.Select(a => a.Region.City.State),
                    i => i.Policies,
                    i => i.Policies.Select(p => p.Order),
                    i => i.Policies.Select(p => p.Order.Quotation),
                    i => i.Policies.Select(p => p.Condition),
                    i => i.Policies.Select(p => p.Condition.Product),
                    i => i.Policies.Select(p => p.Business),
                    i => i.Policies.Select(p => p.Business.Address),
                    i => i.Policies.Select(p => p.Business.Address.Region),
                    i => i.Policies.Select(p => p.Business.Address.Region.City),
                    i => i.Policies.Select(p => p.Business.Address.Region.City.State),
                    i => i.Policies.Select(p => p.Broker),
                    i => i.Policies.Select(p => p.Broker.Brokerage));
            });
        }
        public  MyProtectorMobileOutputInsuredDTO GetProfilePoliciesClaimsCoveragesAsync(long insuredId)
        {
            return  TryCatch(() =>
            {
                var inputInsured =  _mainRepository.TryGet(insuredId,
                   i => i.Phones,
                   i => i.Declineds,
                   i => i.Addresses,
                   i => i.Addresses.Select(y=>y.Region),
                   i => i.Addresses.Select(y => y.Region.City),
                   i => i.Addresses.Select(y => y.Region.City.State),
                   i => i.Policies,
                   i => i.Policies.Select(p => p.Order),
                   i => i.Policies.Select(p => p.Order.PaymentMethod),
                   i => i.Policies.Select(p => p.Condition),
                   i => i.Policies.Select(p => p.Condition.Product),
                   i => i.Policies.Select(p => p.Broker),
                   i => i.Policies.Select(p => p.Broker.Phones),
                   i => i.Policies.Select(p => p.Broker.Brokerage),
                   i => i.Policies.Select(p => p.Claims),
                   i => i.Policies.Select(x => x.Claims.Select(m => m.ClaimDocuments)),
                   i => i.Policies.Select(x => x.Claims.Select(m => m.ThirdParty)),
                   i => i.Policies.Select(p => p.Coverages) );

                return _mapper.Map<MyProtectorMobileOutputInsuredDTO>(inputInsured);
            });
        }
        public async Task<MyProtectorOutputInsuredDTO> GetProfilePoliciesAsync(long insuredId)
        {
            return await TryCatch(async() =>
            {
                var inputInsured = await _mainRepository.TryGetAsync(insuredId,
                   i => i.Phones,
                   i => i.Declineds,
                   i => i.Policies,
                   i => i.Policies.Select(p => p.Order),
                   i => i.Policies.Select(p => p.Condition),
                   i => i.Policies.Select(p => p.Condition.Product),
                   i => i.Policies.Select(p => p.Broker),
                   i => i.Policies.Select(p => p.Broker.Phones),
                   i => i.Policies.Select(p => p.Broker.Brokerage));

                return _mapper.Map<MyProtectorOutputInsuredDTO>(inputInsured);
            });
        }
        public async Task<OutputInsuredDTO> GetAsync(long id)
        {
            return await TryCatch(async() =>
            {
                var inputInsured = await _mainRepository.TryGetAsync(id,
                    i => i.Phones,
                    i => i.Policies,
                    i => i.Declineds,
                    i => i.Policies.Select(p => p.Condition),
                    i => i.Policies.Select(p => p.Condition.Product),
                    i => i.Policies.Select(p => p.Broker),
                    i => i.Policies.Select(p => p.Broker.Phones),
                    i => i.Policies.Select(p => p.Broker.Brokerage));

                return _mapper.Map<OutputInsuredDTO>(inputInsured);
            });
        }
        public async Task<OutputInsuredDTO> UpdateAsync(long insuredId, InputInsuredProfileDTO insuredIn)
        {
            return await TryCatch(async() =>
            {
                var insured = _mainRepository.TryGet(insuredId);
                if (IsUsed(insuredIn.Email, insured.Email))
                {
                    _validationManager.AddExistingUserError(insured);
                }
                insured.Email = insuredIn.Email;
                insured.GeneratePassword(insuredIn.Password);
                _mainRepository.Update(insured);
                await CommitAsync();

                return _mapper.Map<OutputInsuredDTO>(insured);
            });
        }
        public  OutputInsuredDTO Update(long insuredId, InputInsuredPictureProfileDTO insuredIn)
        {
            return  TryCatch( () =>
            {
                var insured = _mainRepository.TryGet(insuredId);
                insured.Picture = insuredIn.Picture;

                _mainRepository.Update(insured);
                 Commit();

                return _mapper.Map<OutputInsuredDTO>(insured);
            });
        }
    }
}
