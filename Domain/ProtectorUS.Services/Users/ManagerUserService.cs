﻿using AutoMapper;

namespace ProtectorUS.Services
{
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Model;
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    public class ManagerUserService : UserService<ManagerUser>, IManagerUserService
    {
        public ManagerUserService(
           IEmailTemplateService emailTemplateService,
           IManagerUserRepository managerUserRepository,
           IProfileRepository profileRepository,
           IPolicyRepository policyRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
               emailTemplateService,
                profileRepository,
                policyRepository,
                unitOfWork,
                logger,
                validationManager,
                mapper,
                managerUserRepository)
        {
        }
        public OutputManagerUserDTO Create(ManagerUserInsertDTO managerUserIn)
        {
            return TryCatch(() =>
            {
                ManagerUser managerUser = _mapper.Map<ManagerUser>(managerUserIn);
                if (IsUsed(managerUser.Email))
                {
                    _validationManager.AddExistingUserError(managerUser);
                }
                managerUser.Status = UserStatus.Onboarding;
                managerUser.GeneratePassword();
                AddExistingProfile(managerUser, managerUserIn.ProfileId);
                _mainRepository.Add(managerUser);
                _validationManager.Validate(managerUser);
                 Commit();

                string body = _emailTemplateService.GetWelcomeManagerUserBody(managerUser);
                _emailTemplateService.Send(EmailTemplate.WelcomeManagerUser.Id, managerUser.Email, body);

                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
        public OutputManagerUserDTO Update(long managerUserId, ManagerUserUpdateDTO managerUserIn)
        {
            return TryCatch(() =>
            {
                ManagerUser managerUser = _mainRepository.TryGet(managerUserId, m => m.Profiles);
                managerUser.FirstName = managerUserIn.FirstName;
                managerUser.LastName = managerUserIn.LastName;
                managerUser.CompanyId = managerUserIn.CompanyId;
                AddExistingProfile(managerUser, managerUserIn.ProfileId);
                _mainRepository.Update(managerUser);
                _validationManager.Validate(managerUser);
                 Commit();
                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
        public new OutputManagerUserDTO Onboarding(long managerUserId)
        {
            return TryCatch(() =>
            {
                var managerUser = base.Onboarding(managerUserId);
                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
        public OutputManagerUserProfileDTO Get(long managerUserId)
        {
            return TryCatch(() =>
            {
                var managerUser =  _mainRepository.TryGet(managerUserId, a => a.Profiles, a => a.Phones);
                return _mapper.Map<OutputManagerUserProfileDTO>(managerUser);
            });
        }
        public OutputManagerUserDTO Delete(long managerUserId)
        {
            return TryCatch(() =>
            {
                var managerUser = _mainRepository.TryGet(managerUserId);
                _mainRepository.Remove(managerUser);
                 Commit();
                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
        public OutputManagerUserDTO Update(long managerUserId, ManagerUserSelfUpdateDTO managerUserIn)
        {
            return TryCatch(() =>
            {
                ManagerUser managerUser = Updating(managerUserId, managerUserIn);
                if (IsUsed(managerUserIn.Email, managerUser.Email))
                {
                    _validationManager.AddExistingUserError(managerUser);
                }
                managerUser.Email = managerUserIn.Email;
                managerUser.GeneratePassword(managerUserIn.Password);
                _mainRepository.Update(managerUser);
                _validationManager.Validate(managerUser);
                 Commit();

                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
        public PagedResult<OutputManagerUserItemDTO> FindAll(int page, int pageSize, string sort, Filters<ManagerUser> filters)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputManagerUserItemDTO>(page, pageSize, sort, filters,
                    m => m.Profiles, a => a.Phones);
            });
        }
        public async Task<OutputManagerUserDTO> DeleteAsync(long managerUserId)
        {
            return await TryCatch(async () =>
            {
                var managerUser = _mainRepository.TryGet(managerUserId);
                _mainRepository.Remove(managerUser);
                await CommitAsync();
                return _mapper.Map<OutputManagerUserDTO>(managerUser);
            });
        }
    }
}
