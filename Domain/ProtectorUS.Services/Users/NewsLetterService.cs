﻿using System;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace ProtectorUS.Services
{
    using DTO;
    using Model.Services;
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Newtonsoft.Json.Linq;
    using Exceptions;
    using DTO.RatingPlan;
    using Newtonsoft.Json;
    using System.Threading.Tasks;
    public class NewsLetterService : BaseService<NewsLetter>, INewsLetterService
    {
        public NewsLetterService(IUnitOfWork unitOfWork,
           INewsLetterRepository newslatterRepository,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                newslatterRepository
                )
        {
        }

        public OutputNewsLetterDTO Create(InputNewsLetterDTO newsletterIn)
        {
            return  TryCatch( () =>
            {
                NewsLetter newsletter = _mapper.Map<NewsLetter>(newsletterIn);
                _mainRepository.Add(newsletter);
                Commit();
                return _mapper.Map<OutputNewsLetterDTO>(newsletter);
            });
        }
  
        public async Task<OutputNewsLetterDTO> CreateAsync(InputNewsLetterDTO newsletterIn)
        {
            return await TryCatch(async() =>
            {
                NewsLetter newsletter = _mapper.Map<NewsLetter>(newsletterIn);
            
                _mainRepository.Add(newsletter);
                await CommitAsync();
                return _mapper.Map<OutputNewsLetterDTO>(newsletter);
            });
        }
    }
}
