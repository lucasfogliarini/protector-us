using System;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace ProtectorUS.Services
{
    using Model.Services;
    using DTO;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using Model;
    using Exceptions;
    using System.Threading.Tasks;
    using Configuration;
    public class ClaimService : BaseService<Claim>, IClaimService
    {
        readonly ProtectorUSConfiguration _config = ProtectorUSConfiguration.GetConfig();
        readonly IClaimRepository _claimRepository;
        readonly INotificationService _notificationService;
        readonly IEmailTemplateService _emailTemplateService;
        public ClaimService(IUnitOfWork unitOfWork,
          IClaimRepository claimRepository,
          IEmailTemplateService emailTemplateService,
          INotificationService notificationService,
          ILogger logger,
          IValidationManager validationManager,
          IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                claimRepository)
        {
            _notificationService = notificationService;
            _claimRepository = claimRepository;
            _emailTemplateService = emailTemplateService;
        }

        public OutputClaimDTO Get(long id)
        {
            return TryCatch( () =>
            {
                var claim =  _mainRepository.TryGet(id,
                    c => c.Policy,
                    c => c.Policy.Condition,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Insured,
                    c => c.Policy.Broker.Addresses,
                    c => c.Policy.Broker.Addresses.Select(d => d.Region),
                    c => c.Policy.Broker.Addresses.Select(d => d.Region.City),
                    c => c.Policy.Broker.Addresses.Select(d => d.Region.City.State),
                    c => c.ThirdParty,
                    c => c.ClaimDocuments);
                claim.ClaimDocuments.RemoveAll(x => x.IsDeleted);
                return _mapper.Map<OutputClaimDTO>(claim);
            });
           
        }
        public OutputClaimDTO Delete(long claimId)
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.TryGet(claimId);
                _mainRepository.Remove(claim);
                Commit();

                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public OutputClaimDTO Create(long policyId, InputClaimDTO claimIn)
        {
            return TryCatch( () =>
            {
                Claim claim = _mapper.Map<Claim>(claimIn);
                claim.PolicyId = policyId;
                claim.Status = ClaimStatus.FNOL;
                int? lastSequentialNumber = _claimRepository.GetLastSequentialNumber();
                int sequentialNumber = Claim.NextSequentialNumber(lastSequentialNumber);
                claim.Numerate(sequentialNumber);
                _validationManager.Validate(claim);
                _mainRepository.Add(claim);
                 Commit();
                #region sendemails
                claim = _claimRepository.Get(claim.Id, 
                    c=> c.Policy,
                    c => c.Policy.Business,
                    c => c.Policy.Insured,
                    c => c.Policy.Broker,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker.Brokerage);

                string body = _emailTemplateService.GetFnolClaimInsuredBody(claim);
                _emailTemplateService.Send(EmailTemplate.FnolClaim.InsuredId, claim.Policy.Insured.Email, body);

                body = _emailTemplateService.GetFnolClaimBrokerBody(claim);
                _emailTemplateService.Send(EmailTemplate.FnolClaim.BrokerId, claim.Policy.Broker.Email, body);

                body = _emailTemplateService.GetFnolClaimArgoBody(claim);
                _emailTemplateService.Send(EmailTemplate.FnolClaim.ArgoId, _config.ArgoEmail.Value, body);

                #endregion
                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public OutputClaimDTO Update(long Id, InputClaimDTO claimIn)
        {
            return  TryCatch( () =>
            {
                Claim claim = Updating(Id, claimIn);

                _mainRepository.Update(claim);

                _validationManager.Validate(claim);
                 Commit();

                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public IEnumerable<OutputClaimDTO> GetAll()
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.GetAll(
                    c => c.Policy,
                    c => c.Policy.Insured,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Broker.Brokerage);
                return _mapper.Map<IEnumerable<OutputClaimDTO>>(claim);
            });
        }
        public IReadOnlyCollection<GraphicDTO> GetTotalInterval(ClaimPeriodFilter interval, Filters<Claim> filters = null)
        {
            return TryCatch(() =>
            {
                DateTime datebegin = DateTime.Now;
                DateTime dateend = DateTime.Now.AddDays(1);

                int intervaleixo = 0;

                switch (interval)
                {
                    case ClaimPeriodFilter.Today:
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 24;
                        break;
                    case ClaimPeriodFilter.Week:
                        datebegin = DateTime.Now.AddDays(-7);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 7;
                        break;
                    case ClaimPeriodFilter.LastMonth:
                        datebegin = DateTime.Now.AddMonths(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = (int)Math.Round(dateend.Subtract(datebegin).TotalDays);
                        break;
                    case ClaimPeriodFilter.LastYear:
                        datebegin = DateTime.Now.AddYears(-1);
                        datebegin = new DateTime(datebegin.Year, datebegin.Month, datebegin.Day, 0, 0, 0);
                        intervaleixo = 12;
                        break;
                }
                filters = filters ?? new Filters<Claim>();
                filters.Add(x => x.ClaimDate > datebegin && x.ClaimDate < dateend);

                List<GraphicDTO> result = new List<GraphicDTO>();

                var claims = _mainRepository.FindAll(filters, x => x.Policy, x => x.Policy.Broker);

                int totalclaims = claims.Count();
                GraphicDTO total = new GraphicDTO();
                total.List = new List<ItemGraphicDTO>();
                total.Name = "Total Claims";
                ItemGraphicDTO itemtotal = new ItemGraphicDTO() { Key = "Total Claims", Value = totalclaims };
                total.List.Add(itemtotal);

                result.Add(total);

                GraphicDTO lossratio = new GraphicDTO();
                lossratio.List = new List<ItemGraphicDTO>();
                lossratio.Name = "Loss Ratio";
                //ainda n�o definido
                ItemGraphicDTO itemlossratio = new ItemGraphicDTO() { Key = "Loss Ratio", Value = 0 };
                lossratio.List.Add(itemlossratio);

                result.Add(lossratio);

                GraphicDTO graphic = new GraphicDTO();
                graphic.List = new List<ItemGraphicDTO>();
                graphic.Name = "Claims/Status";

                foreach (ClaimStatus item in Enum.GetValues(typeof(ClaimStatus)))
                {
                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = EnumExtensions.Description(item), Value = claims.Where(c => c.Status == item).Count() };
                    graphic.List.Add(itemgraphic);
                };
                result.Add(graphic);


                GraphicDTO graphic2 = new GraphicDTO();
                graphic2.List = new List<ItemGraphicDTO>();
                graphic2.Name = "Claims Period";

                DateTime dtaux = datebegin;

                while (intervaleixo > 0)
                {
                    string key = "";
                    decimal values = 0;

                    switch (interval)
                    {
                        case ClaimPeriodFilter.Today:
                            key = datebegin.ToString("HH:mm");
                            values = claims.Where(c => c.ClaimDate >= datebegin && c.ClaimDate < datebegin.AddHours(1)).Count();
                            datebegin = datebegin.AddHours(1);
                            break;
                        case ClaimPeriodFilter.Week:
                            key = datebegin.ToString("dd MMM");
                            values = claims.Where(c => c.ClaimDate >= datebegin && c.ClaimDate < datebegin.AddDays(1)).Count();
                            datebegin = datebegin.AddDays(1);
                            break;
                        case ClaimPeriodFilter.LastMonth:
                            key = datebegin.ToString("dd MMM");
                            values = claims.Where(c => c.ClaimDate >= datebegin && c.ClaimDate < datebegin.AddDays(1)).Count();
                            datebegin = datebegin.AddDays(1);
                            break;
                        case ClaimPeriodFilter.LastYear:
                            key = datebegin.ToString("MMM");
                            values = claims.Where(c => c.ClaimDate >= datebegin && c.ClaimDate < datebegin.AddMonths(1)).Count();
                            datebegin = datebegin.AddMonths(1);
                            break;
                    }

                    ItemGraphicDTO itemgraphic = new ItemGraphicDTO() { Key = key, Value = values };
                    graphic2.List.Add(itemgraphic);

                    intervaleixo--;
                }

                result.Add(graphic2);


                return result;
            });
        }
        public MapPointDTO GetMapPoints(Filters<Claim> filter = null)
        {
            return  TryCatch( () =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();

                map.RequestInfo = "/claims/{id}";

                var claims =  _mainRepository.GetAll(
                 c => c.Policy,
                 c => c.Policy.Insured,
                 c => c.Policy.Condition,
                 c => c.Policy.Business,
                 c => c.Policy.Business.Address);


                foreach (var item in claims)
                {
                    PointDTO pinitem = new PointDTO() { Id = item.Id, idProduct = item.Policy.Condition.ProductId, Tag = item.Status.ToString(), Name = item.Policy.Insured.FullName, Image = item.Policy.Insured.Picture, Latitude = item.Policy.Business.Address.Latitude.Value, Longitude = item.Policy.Business.Address.Longitude.Value };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public OutputClaimDTO AttachDocuments(long claimId, string[] documentsPath)
        {
            return  TryCatch( () =>
            {
                var claim = _mainRepository.TryGet(claimId,
                   c => c.ClaimDocuments);

                if (claim.Status != ClaimStatus.FNOL)
                    throw new BadRequestException(x => x.EL036);

                claim.ClaimDocuments = claim.ClaimDocuments ?? new List<ClaimDocument>();
                foreach (string documentPath in documentsPath)
                {
                    claim.ClaimDocuments.Add(new ClaimDocument(documentPath));
                }

                _mainRepository.Update(claim);
                 Commit();
                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public OutputClaimDocumentDTO DeleteDocument(long claimId, long documentId)
        {
            return  TryCatch( () =>
            {
                var claim = _mainRepository.TryGet(claimId, c => c.ClaimDocuments);
                if (claim.Status != ClaimStatus.FNOL)
                    throw new BadRequestException(x => x.EL036);
                var claimDocument = claim.ClaimDocuments.FirstOrDefault(cd => cd.Id == documentId);
                ThrowNotFoundIfNull(claimDocument);
                claimDocument.Delete();
                _mainRepository.Update(claim);
                 Commit();
                return _mapper.Map<OutputClaimDocumentDTO>(claimDocument);
            });
        }

        public OutputClaimDocumentDTO DeleteDocument(long id, string fileName)
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.TryGet(id, c => c.ClaimDocuments);
                if (claim.Status != ClaimStatus.FNOL)
                    throw new BadRequestException(x => x.EL036);
                var claimDocument = claim.ClaimDocuments.FirstOrDefault(cd => cd.Path == fileName);
                ThrowNotFoundIfNull(claimDocument);
                claimDocument.Delete();
                _mainRepository.Update(claim);
                Commit();
                return _mapper.Map<OutputClaimDocumentDTO>(claimDocument);
            });

        }
        public OutputClaimDTO ChangeStatus(long claimId, ClaimStatus status, long actorId)
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.TryGet(claimId);
                claim.Status = status;
                _mainRepository.Update(claim);

                var notification = new InputNotificationDTO
                {
                    ActivityType = ActivityType.ClaimStatusChange,
                    SourceId = claim.Id,
                    SourceType = SourceType.Claim,
                    ActorType = ActorType.ManagerUser,
                    ActorId = actorId,
                };
                Commit();
                #region sendemails
                claim = _claimRepository.Get(claim.Id,
                    c => c.Policy,
                    c => c.Policy.Insured,
                    c => c.Policy.Broker,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker.Brokerage);

                if (status == ClaimStatus.Analisys)//exists only the template for analysis ��
                {
                    string body = _emailTemplateService.GetClaimStatusInsuredBody(claim);
                    _emailTemplateService.Send(EmailTemplate.ClaimStatus.InsuredId, claim.Policy.Insured.Email, body);

                    body = _emailTemplateService.GetClaimStatusBrokerBody(claim);
                    _emailTemplateService.Send(EmailTemplate.ClaimStatus.BrokerId, claim.Policy.Broker.Email, body);

                    body = _emailTemplateService.GetClaimStatusArgoBody(claim);
                    _emailTemplateService.Send(EmailTemplate.ClaimStatus.ArgoId, _config.ArgoEmail.Value, body);
                }

                #endregion
                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public bool IsOwner(long insuredId, long claimId)
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.Find(c => c.Id == claimId && c.Policy.InsuredId == insuredId, c=>c.Policy);
                return claim != null;
            });
        }
        public bool IsOwnBusiness(long brokerageId, long claimId)
        {
            return TryCatch(() =>
            {
                var claim = _mainRepository.Find(c => c.Id == claimId && c.Policy.Broker.BrokerageId == brokerageId, e => e.Policy, e=>e.Policy.Broker);
                return claim != null;
            });
        }
        public async Task<OutputClaimDTO> UpdateAsync(long Id, InputClaimDTO claimIn)
        {
            return await TryCatch(async() =>
            {
                Claim claim = Updating(Id, claimIn);

                _mainRepository.Update(claim);

                _validationManager.Validate(claim);
                await CommitAsync();

                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        protected Claim Updating(long Id, InputClaimDTO claimIn)
        {
            return TryCatch(() =>
            {
                Claim claim = _mainRepository.TryGet(Id);
                claim.DescriptionFacts = claimIn.DescriptionFacts;
                claim.ThirdParty = _mapper.Map<ThirdParty>(claimIn.ThirdParty);

                return claim;
            });
        }
        public PagedResult<OutputClaimDTO> FindAll(int page, int pageSize, string sort, Filters<Claim> filters)
        {
            return TryCatch(() =>
            {
                return FindPaginated<OutputClaimDTO>(page, pageSize, sort, filters,
                    c => c.ThirdParty,
                    c => c.Policy,
                    c => c.Policy.Insured,
                    c => c.Policy.Business,
                    c => c.Policy.Condition,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Broker.Brokerage);
            });
        }
        public IEnumerable<OutputClaimDTO> FindAll(long policyId)
        {
            return TryCatch(() =>
            {
                var claims = _mainRepository.FindAll(c => c.PolicyId == policyId,
                    c => c.Policy,
                    c => c.ThirdParty,
                    c => c.Policy.Insured,
                    c => c.Policy.Condition,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Broker.Brokerage);
                return _mapper.Map<IEnumerable<OutputClaimDTO>>(claims);
            });
        }
        public async Task<IEnumerable<OutputClaimDTO>> FindAllAsync(long policyId)
        {
            return await TryCatch(async () =>
            {
                var claims = await _mainRepository.FindAllAsync(c => c.PolicyId == policyId,
                    c => c.Policy,
                    c => c.Policy.Insured,
                    c => c.Policy.Condition,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Broker.Brokerage);
                return _mapper.Map<IEnumerable<OutputClaimDTO>>(claims);
            });
        }
        public async Task<OutputClaimDTO> GetAsync(long id)
        {
            return await TryCatch(async() =>
            {
                var claim = await _mainRepository.TryGetAsync(id,
                    c => c.Policy,
                    c => c.Policy.Condition,
                    c => c.Policy.Condition.Product,
                    c => c.Policy.Broker,
                    c => c.Policy.Insured,
                    c => c.Policy.Broker.Addresses,
                    c => c.Policy.Broker.Addresses.Select(d=>d.Region),
                    c => c.Policy.Broker.Addresses.Select(d => d.Region.City),
                    c => c.Policy.Broker.Addresses.Select(d => d.Region.City.State),
                    c => c.ThirdParty,
                    c => c.ClaimDocuments);
                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public async Task<MapPointDTO> GetMapPointsAsync(Filters<Claim> filter = null)
        {
            return await TryCatch(async() =>
            {
                MapPointDTO map = new MapPointDTO();
                map.Pins = new List<PointDTO>();

                map.RequestInfo = "/claims/{id}";

                var claims = await _mainRepository.GetAllAsync(//filter,
                 c => c.Policy,
                 c => c.Policy.Insured,
                 c => c.Policy.Condition,
                 c => c.Policy.Business,
                 c => c.Policy.Business.Address);


                foreach (var item in claims)
                {
                    PointDTO pinitem = new PointDTO() { Id = item.Id, idProduct = item.Policy.Condition.ProductId, Tag = item.Status.ToString(), Name = item.Policy.Insured.FullName, Image = item.Policy.Insured.Picture, Latitude = item.Policy.Business.Address.Latitude.Value, Longitude = item.Policy.Business.Address.Longitude.Value };
                    map.Pins.Add(pinitem);
                };

                return map;
            });
        }
        public async Task<OutputClaimDTO> AttachDocumentsAsync(long claimId, string[] documentsPath)
        {
            return await TryCatch(async() =>
            {
                var claim = _mainRepository.TryGet(claimId,
                   c => c.ClaimDocuments);

                if (claim.Status != ClaimStatus.FNOL)
                    throw new BadRequestException(x => x.EL036);

                claim.ClaimDocuments = claim.ClaimDocuments ?? new List<ClaimDocument>();
                foreach (string documentPath in documentsPath)
                {
                    claim.ClaimDocuments.Add(new ClaimDocument(documentPath));
                }

                _mainRepository.Update(claim);
                await CommitAsync();
                return _mapper.Map<OutputClaimDTO>(claim);
            });
        }
        public async Task<OutputClaimDocumentDTO> DeleteDocumentAsync(long claimId, long documentId)
        {
            return await TryCatch(async() =>
            {
                var claim = _mainRepository.TryGet(claimId, c => c.ClaimDocuments);
                if (claim.Status != ClaimStatus.FNOL)
                    throw new BadRequestException(x => x.EL036);
                var claimDocument = claim.ClaimDocuments.FirstOrDefault(cd => cd.Id == documentId);
                ThrowNotFoundIfNull(claimDocument);
                claimDocument.Delete();
                _mainRepository.Update(claim);
                await CommitAsync();
                return _mapper.Map<OutputClaimDocumentDTO>(claimDocument);
            });
        }
      
    }
}
