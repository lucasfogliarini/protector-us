using AutoMapper;
using System.Collections.Generic;
using ProtectorUS.Exceptions;

namespace ProtectorUS.Services
{
    using Model.Services;
    using Model;
    using Model.Repositories;
    using Diagnostics.Logging;
    using Model.Validation;
    using DTO;
    using System.Device.Location;
    using Geocoding;
    public class LocalizationService : BaseService<Address>, ILocalizationService
    {
        private readonly IAddressRepository _addressRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IRegionRepository _regionRepository;
        private readonly IStateRepository _stateRepository;

        public LocalizationService(
           IAddressRepository addressRepository,
           ICityRepository cityRepository,
           IRegionRepository regionRepository,
           IStateRepository stateRepository,
           IUnitOfWork unitOfWork,
           ILogger logger,
           IValidationManager validationManager,
           IMappingEngine mapper) : base(
                unitOfWork,
                logger,
                validationManager,
                mapper,
                addressRepository)
        {
            _addressRepository = addressRepository;
            _cityRepository = cityRepository;
            _regionRepository = regionRepository;
            _stateRepository = stateRepository;
        }
      
        public void DeleteAddress(long id)
        {
            TryCatch(() =>
            {
                var address = _addressRepository.Get(id);
                if(address != null)
                {
                    address.Delete();
                    _addressRepository.Remove(address);
                }
            
                Commit();
            });
        }
        public GeoCoordinate GetCoordinates(string streetLine1, string zipCode)
        {
            return TryCatch(() =>
            {
                return Geocode.GetCoordinates(zipCode, streetLine1);
            });           
        }
        public OutputRegionDTO GetRegion(string zipCode)
        {
            return TryCatch(() =>
            {
                Region region = new Region() { ZipCode = zipCode };
                _validationManager.Validate(region);
                if (!_validationManager.HasErrors)
                {
                    region = _regionRepository.Find(x => x.ZipCode == zipCode,
                        l => l.City,
                        l => l.City.State,
                        l => l.City.State.Country);

                    if (region == null)
                    {
                        region = Geocode.GetRegion(zipCode);
                        if (region == null)
                            throw new NotFoundException(e => e.EL018, zipCode);
                        
                        City existingCity = _cityRepository.Find(x =>
                            x.Name == region.City.Name &&
                            x.State.Abbreviation == region.City.State.Abbreviation,
                            l => l.State,
                            l => l.State.Country);
                        if (existingCity == null)
                        {
                             region.City.State = _stateRepository.TryFind(s => s.Abbreviation == region.City.State.Abbreviation);
                        }
                        else
                        {
                            region.City = existingCity;
                        }

                        _regionRepository.Add(region);
                        Commit();
                    }
                }
                ThrowValidationErrors();
                return _mapper.Map<OutputRegionDTO>(region);
            });
        }
        public Region GetRegion(long regionId)
        {
            return TryCatch(() =>
            {
                Region region = _regionRepository.TryGet(regionId,
                        l => l.City,
                        l => l.City.State,
                        l => l.City.State.Country);

                return region;
            });
        }
        public IEnumerable<OutputStateDTO> GetStates()
        {
            return TryCatch(() =>
            {
                var states =_stateRepository.GetAll();
                return _mapper.Map<IEnumerable<OutputStateDTO>>(states);
            });
        }
    }
}
