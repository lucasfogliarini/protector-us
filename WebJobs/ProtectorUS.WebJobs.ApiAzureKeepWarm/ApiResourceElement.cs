﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace ProtectorUS.WebJobs.ApiAzureKeepWarm
{
    /// <summary>
    /// Represents the Api configuration environment element.
    /// </summary>
    public class ApiResourceElement : ConfigurationElement
    {

        /// <summary>
        /// The url attribute.
        /// </summary>
        [ConfigurationProperty("url")]
        public string Url
        {
            get { return this["url"].ToString(); }
            set { this["url"] = value; }
        }

    }

    [ConfigurationCollection(typeof(ApiResourceElement))]
    public class ApiResourceElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "apiResource";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ApiResourceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ApiResourceElement)(element)).Url;
        }

        public ApiResourceElement this[int idx]
        {
            get { return (ApiResourceElement)BaseGet(idx); }
        }
    }
}