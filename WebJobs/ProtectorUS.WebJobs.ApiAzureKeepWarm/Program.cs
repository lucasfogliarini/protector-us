﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Configuration;
using System.Net.Http;

namespace ProtectorUS.WebJobs.ApiAzureKeepWarm
{
    public class Program
    {

        static void Main(string[] args)
        {
            var runner = new Runner();
            var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            var waitTime = int.Parse(ConfigurationManager.AppSettings["WaitTime"]);
            var urls = (IEnumerable<string>)WebJobApiConfiguration.Resources.Select(x => x.Url);

            Task.WaitAll(runner.HitSite(urls, waitTime));


        }

        private class Runner
        {
            private HttpClient client = new HttpClient();

            public async Task HitSite(IEnumerable<string> siteUrls, int waitTime)
            {
                while (true)
                {
                    try
                    {
                        siteUrls.ToList().ForEach(async url =>
                        {
                            var request = await client.GetAsync(new Uri(url));
                            Console.WriteLine($"| Request url: {url} | Time: {DateTime.Now} | Return Status Code: {request.StatusCode}");
                        });

                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.ToString());
                    }
                    await Task.Delay(waitTime * 1000);
                }
            }
        }
    }
}
