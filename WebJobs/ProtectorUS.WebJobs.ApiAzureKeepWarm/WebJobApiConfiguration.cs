﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace ProtectorUS.WebJobs.ApiAzureKeepWarm
{
    /// <summary>
    /// Defines the test api configuration.
    /// </summary>
    public class WebJobApiConfiguration : ConfigurationSection
    {


        /// <summary>
        /// The api element collection.
        /// </summary>
        [ConfigurationProperty("apiResources")]
        public ApiResourceElementCollection ApiResources
        {
            get { return (ApiResourceElementCollection)this["apiResources"]; }
            set { this["apiResources"] = value; }
        }


        /// <summary>
        /// Gets the test api configuration.
        /// </summary>
        /// <returns>the protectorUS configuration section.</returns>
        public static WebJobApiConfiguration GetConfig()
        {
            return ConfigurationManager.GetSection(
               "webJobApiConfiguration") as WebJobApiConfiguration;
        }

        /// <summary>
        /// Gets the user collection configuration.
        /// </summary>
        public static ApiResourceElementCollection GetResources()
        {
            return GetConfig().ApiResources;
        }

        public static IEnumerable<ApiResourceElement> Resources
        {
            get
            {
                foreach (ApiResourceElement selement in GetResources())
                {
                    if (selement != null)
                        yield return selement;
                }
            }
        }
    }
}
