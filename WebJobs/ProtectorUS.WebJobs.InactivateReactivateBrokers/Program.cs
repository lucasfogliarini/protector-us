﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.IO;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace ProtectorUS.WebJobs.InactivateReactivateBrokers
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        public static void Main()
        {
            using (JobHost host = new JobHost())
            {
                // Invoke the function from code
                host.CallAsync(typeof(Program).GetMethod("ActivateInactivateBrokers"));
                // The RunAndBlock here is optional. However,
                // if you want to be able to invoke the function below
                // from the dashboard, you need the host to be running
                host.RunAndBlock();
                // Alternative to RunAndBlock is Host.Start and you
                // have to create your own infinite loop that keeps the
                // process alive
            }
        }


        // In order for a function to be indexed and visible in the dashboard it has to 
        // - be in a public class
        // - be public and static
        // - have at least one WebJobs SDK attribute
        [NoAutomaticTrigger]
        public static async Task ActivateInactivateBrokers(TextWriter log)
        {

            while (true)
            {
                try
                {
                    string baseURL = ConfigurationManager.AppSettings["URL"];
                    OutputBrokerInactivateReactivate totalInactivations = null;
                    Task.Run(async () =>
                    {
                        // Do any async anything you need here without worry
                        HttpClient _client;
                        _client = new HttpClient();
                        _client.BaseAddress = new Uri(baseURL);
                        _client.DefaultRequestHeaders.Accept.Clear();
                        _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string formData = "client_id=web&grant_type=password&user_type=Broker&username=master@brokerage1.com&password=broker";
                        var responseToken = _client.PostAsync("token", new StringContent(formData)).Result;

                        var _token = JsonConvert.DeserializeObject<Token>(responseToken.Content.ReadAsStringAsync().Result);
                        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.AccessToken);

                        // Act
                        var response = await _client.GetAsync("brokers/inactivate");
                        await response.Content.ReadAsStringAsync().ContinueWith((x) => totalInactivations = JsonConvert.DeserializeObject<OutputBrokerInactivateReactivate>(x.Result));

                    }).Wait();

                    log.WriteLine(totalInactivations.Inactivateds + totalInactivations.Activateds);


                }
                catch (Exception ex)
                {
                    log.WriteLine("Error occurred in processing pending requests. Error : {0}", ex.Message);
                }
                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        }
    }

    public class OutputBrokerInactivateReactivate
    {
        public int Activateds { get; set; }
        public int Inactivateds { get; set; }
    }

    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
