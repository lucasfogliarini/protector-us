﻿namespace ProtectorUS.Communication
{
    public partial class MessageCode
    {
        //Error
        public readonly string E001 = "The password is incorrect.";
        public readonly string E002 = "Unauthorized.";
        public readonly string E004 = "{0} is required.";
        public readonly string E005 = "The {0} with id '{1}' was not found.";
        public readonly string E006 = "Consumer Device is inactive.";
        public readonly string E007 = "Refresh token is issued to a different clientId.";
        public readonly string E008 = "Unauthenticated.";
        public readonly string E009 = "Message.StatusCode not implemented.";
        public readonly string E010 = "Ops! The Rating Plan data don't match.";
        public readonly string E012 = "Ops! An error occurred in the system.";
        public readonly string E013 = "The entity {0} must have a validor class implemented.";
        public readonly string E014 = "Generic service exception. Exception message: {0}. Inner exception message: {1}";
        public readonly string E016 = "The input model have invalid fields. See errors: {0}";
        public readonly string E017 = "The entity(s) model(s) have invalid fields. See errors: {0}";
        public readonly string E018 = "The ZipCode '{0}' was not found.";
        public readonly string E019 = "Generic database exception. Exception message: {0}. Inner exception message: {1}";
        public readonly string E020 = "There’s already a {0} with that {1}.";
        public readonly string E021 = "Generic payment exception. Exception message: {0}.Inner exception message: {1}.";
        public readonly string E022 = "There’s not a {0} with that id.";
        public readonly string E023 = "The credit card number '{0}' is invalid.";
        public readonly string E024 = "The Rating Plan was not found.";
        public readonly string E025 = "The Quotation with id '{0}' is not complete or does not exist.";
        public readonly string E026 = "The {0} was not found.";
        public readonly string E027 = "The username and password can't be empty.";
        public readonly string E028 = "The Brokerage has already been created.";
        public readonly string E029 = "The User has already been created.";
        public readonly string E030 = "The Request is not valid.";
        public readonly string E031 = "The Image Format is not valid";
        public readonly string E032 = "The Email '{0}' already exists in database";
        public readonly string E033 = "The FIle Format is not valid";
        //Success
        public readonly string S001 = "Thanks! Form registered successfully.";

        //Warning
    }
}
