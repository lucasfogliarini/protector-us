﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace ProtectorUS.Communication
{
    public partial class MessageCode
    {
        public MessageCode(Expression<Func<MessageCodes, string>> code, params object[] args) : this(code.Body as MemberExpression, args)
        {
        }
        public MessageCode(Expression<Func<MessageCodes, string>> code, object[] args, string[] messages) : this(code.Body as MemberExpression, args, messages)
        {
        }
        protected MessageCode(MemberExpression memberExpression, object[] args, string[] messages = null)
        {            
            Message = (memberExpression.Member as FieldInfo).GetValue(new MessageCodes()) as string;
            Message = args == null || args.Length == 0 ? Message : string.Format(Message, args);
            Code = memberExpression.Member.Name;
            Messages = messages;
        }
        public string Message { get; private set; }
        public string Code { get; private set; }
        public string[] Messages { get; private set; }
        public bool MustLog()
        {
            return Code.Contains("EL");
        }
    }
}
