﻿using System.Net;

namespace ProtectorUS.Communication.StatusCode
{
    public static class MessageStatusCode
    {
        public static HttpStatusCode StatusCode(this MessageCode messageCode)
        {
            var statusCodeField = typeof(MessageStatusCode).GetField(messageCode.Code);
            if (statusCodeField == null)
            {
                var message = new MessageCode(m => m.EL009);
                throw new System.Exception(message.Message);
            }
            return (HttpStatusCode)statusCodeField.GetValue(null);
        }

        //EL = Errors being logged
        //ES = Errors not being logged
        public const HttpStatusCode ES001 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL002 = HttpStatusCode.Forbidden;
        public const HttpStatusCode ES003 = HttpStatusCode.BadRequest;
        public const HttpStatusCode ES004 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL005 = HttpStatusCode.NotFound;
        public const HttpStatusCode ES006 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL007 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL008 = HttpStatusCode.Unauthorized;
        public const HttpStatusCode EL009 = HttpStatusCode.InternalServerError;
        public const HttpStatusCode EL010 = HttpStatusCode.BadRequest;
        public const HttpStatusCode ES011 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL012 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL013 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL014 = HttpStatusCode.InternalServerError;
        public const HttpStatusCode EL015 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL016 = HttpStatusCode.BadRequest;
        public const HttpStatusCode ES017 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL018 = HttpStatusCode.NotFound;
        public const HttpStatusCode EL019 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL020 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL021 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL022 = HttpStatusCode.BadRequest;
        public const HttpStatusCode ES023 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL024 = HttpStatusCode.NotFound;
        public const HttpStatusCode EL025 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL026 = HttpStatusCode.NotFound;
        public const HttpStatusCode ES027 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL028 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL029 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL030 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL031 = HttpStatusCode.BadRequest;
        public const HttpStatusCode ES032 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL033 = HttpStatusCode.Forbidden;
        public const HttpStatusCode EL034 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL035 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL036 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL037 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL038 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL039 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL040 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL041 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL042 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL043 = HttpStatusCode.Forbidden;
        public const HttpStatusCode EL044 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL045 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL046 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL047 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL048 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL049 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL050 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL051 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL052 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL053 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL054 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL055 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL056 = HttpStatusCode.InternalServerError;
        public const HttpStatusCode EL057 = HttpStatusCode.Forbidden;
        public const HttpStatusCode EL058 = HttpStatusCode.BadRequest;
        public const HttpStatusCode EL059 = HttpStatusCode.BadRequest;


        //Success
        public const HttpStatusCode S001 = HttpStatusCode.OK;
        public const HttpStatusCode S002 = HttpStatusCode.OK;
        public const HttpStatusCode S003 = HttpStatusCode.OK;
        public const HttpStatusCode S004 = HttpStatusCode.OK;
        public const HttpStatusCode S005 = HttpStatusCode.OK;
    }
}
