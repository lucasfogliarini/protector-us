﻿namespace ProtectorUS.Communication
{
    public partial class MessageCode
    {
        public class MessageCodes
        {
            //EL = Errors being logged
            //ES = Errors not being logged
            public readonly string ES001 = "The password must be correct to authenticate.";
            public readonly string EL002 = "Unauthorized. You need permission to access the resource.";
            public readonly string ES003 = "'{0}' must be greater than now";
            public readonly string ES004 = "'{0}' is required.";
            public readonly string EL005 = "The {0} with id '{1}' was not found.";
            public readonly string ES006 = "'{0}' must be ACTIVE to complete the process.";
            public readonly string EL007 = "Refresh token is issued to a different clientId.";
            public readonly string EL008 = "Unauthenticated. You need a correct key to access the resource.";
            public readonly string EL009 = "Message.StatusCode not implemented.";
            public readonly string EL010 = "Risk Analysis responses must be accurate to complete the process.";
            public readonly string ES011 = "'{0}' must be greater than 0";
            public readonly string EL012 = "The field '{0}' needs to be sent and valid.";
            public readonly string EL013 = "The entity '{0}' must have a validor class implemented.";
            public readonly string EL014 = "Ops! An error occurred in the system. Exception message: {0}. Inner exception message: {1}";
            public readonly string EL015 = "Quotation was expired.";
            public readonly string EL016 = "'{0}' must be smaller than now";
            public readonly string ES017 = "The entity(s) model(s) have invalid fields. See inner messages.";
            public readonly string EL018 = "The ZipCode '{0}' was not found.";
            public readonly string EL019 = "Database validation exception. Messages: {0}";
            public readonly string EL020 = "There’s already a {0} with that {1}.";
            public readonly string EL021 = "Payment Failed.";
            public readonly string EL022 = "There’s not a {0} with that id.";
            public readonly string ES023 = "The credit card number '{0}' is invalid.";
            public readonly string EL024 = "The Rating Plan was not found.";
            public readonly string EL025 = "The Broker Master was not found.";
            public readonly string EL026 = "The {0} was not found.";
            public readonly string ES027 = "The username and password can't be empty.";
            public readonly string EL028 = "The Brokerage has already been created.";
            public readonly string EL029 = "The User has already been created.";
            public readonly string EL030 = "The request must be 'form-data' with only one file ContentType.";
            public readonly string EL031 = "There’s already a similar Rating Plan. Verify the fields: id or formid.";
            public readonly string ES032 = "There’s already a {0} with the email '{1}'";
            public readonly string EL033 = "The User can't reset the password with that token.";
            public readonly string EL034 = "The File Format is not valid.";
            public readonly string EL035 = "Profile not allowed for this user.";
            public readonly string EL036 = "The Claim can only be update at FNOL status.";
            public readonly string EL037 = "The sequential number must be between {0} and {1}.";
            public readonly string EL038 = "Product already {0} or does not exist.";
            public readonly string EL039 = "Endorsement already replied.";
            public readonly string EL040 = "The following Risk Analysis fields are required: {0}";
            public readonly string EL041 = "It was not possible to calculate the Rating Plan.";
            public readonly string EL042 = "The 'device_id' is required for mobile authentication.";
            public readonly string EL043 = "The token is incorrect.";
            public readonly string EL044 = "The quotation was declined.";
            public readonly string EL045 = "Payment denied.";
            public readonly string EL046 = "Quotation already active.";
            public readonly string EL047 = "The Uploaded request must have one file per time.";
            public readonly string EL048 = "The Charge Account with id '{0}' is invalid.";
            public readonly string EL049 = "The Uploaded request must have one file per time and must have a file name.";
            public readonly string EL050 = "Could not create a Charge Account. See inner messages.";
            public readonly string EL051 = "Could not update the Charge Account with id '{0}'. See inner messages.";
            public readonly string EL052 = "Acord - XML invalid format.";
            public readonly string EL053 = "Base premium can't be less than or equal to 0.";
            public readonly string EL054 = "ESB Integration - Return Token - Connection Failure";
            public readonly string EL055 = "It was not possible to generate the policy";
            public readonly string EL056 = "An error occurred while running the seed";
            public readonly string EL057 = "The user have not accept the terms";
            public readonly string EL058 = "Coordinate for the address '{0}' was not found.";
            public readonly string EL059 = "Timezone was not found.";

            //Success
            public readonly string S001 = "Authorized.";
            public readonly string S002 = "An email was sent to '{0}' with instructions to reset the password.";
            public readonly string S003 = "Password successfully reseted.";
            public readonly string S004 = "Password successfully changed.";
            public readonly string S005 = "The user accept the terms.";
        }
    }
}
