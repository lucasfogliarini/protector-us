﻿using System.Net;
using System.Net.Mail;

namespace ProtectorUS.Communication
{
    using Configuration;
    using System.Threading.Tasks;
    public class SendGridMailer
    {
        private readonly ProtectorUSConfiguration _config;
        protected string User { get; private set; }
        protected string Password { get; private set; }
        public SendGridMailer()
        {
            _config = ProtectorUSConfiguration.GetConfig();
            this.User = _config.ApiMail.User;
            this.Password = _config.ApiMail.Password;
        }

        public async Task Send(MailMessage mail)
        {
           // Create the email object first, then add the properties.
            var sendGridMessage = new SendGrid.SendGridMessage();

            foreach(MailAddress to in mail.To)
            {
                sendGridMessage.AddTo(to.Address);
            }
            foreach (MailAddress cc in mail.CC)
            {
                sendGridMessage.AddCc(cc);
            }
            
            foreach (MailAddress bcc in mail.Bcc)
            {
                sendGridMessage.AddBcc(bcc);
            }
            sendGridMessage.Html = mail.Body;
            sendGridMessage.From = mail.From;
            sendGridMessage.Subject = mail.Subject;
            sendGridMessage.EnableClickTracking(true);
            sendGridMessage.EnableOpenTracking();
            
            foreach (Attachment attach in mail.Attachments)
            {
                sendGridMessage.AddAttachment(attach.ContentStream,attach.Name);
            }

            var credentials = new NetworkCredential(User, Password);

            // Create a Web transport for sending email.
            var transportWeb = new SendGrid.Web(credentials);
            await transportWeb.DeliverAsync(sendGridMessage);
        }
    }
}
