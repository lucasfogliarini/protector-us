﻿using System.ComponentModel;

namespace ProtectorUS.Configuration
{
    public enum ProtectorUSEnvironment
    {
        /// <summary>
        /// For localhost environment.
        /// </summary>
        [Description("development")]
        Development,

        /// <summary>
        /// For homolog environment.
        /// </summary>
        [Description("homolog")]
        Homolog,

        /// <summary>
        /// For test environment.
        /// </summary>
        [Description("test")]
        Test,

        /// <summary>
        /// For production environment.
        /// </summary>
        [Description("production")]
        Production,

        /// <summary>
        /// For uat environment.
        /// </summary>
        [Description("uat")]
        UAT
    }
}
