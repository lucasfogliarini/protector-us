﻿using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration database element.
    /// </summary>
    public class DatabaseElement : ConfigurationElement
    {
        /// <summary>
        /// The data source attribute.
        /// </summary>
        [ConfigurationProperty("data-source")]
        public string DataSource
        {
            get { return this["data-source"].ToString(); }
            set { this["data-source"] = value; }
        }

        /// <summary>
        /// The database attribute.
        /// </summary>
        [ConfigurationProperty("database")]
        public string Database
        {
            get { return this["database"].ToString(); }
            set { this["database"] = value; }
        }

        /// <summary>
        /// The username attribute.
        /// </summary>
        [ConfigurationProperty("username")]
        public string Username
        {
            get { return this["username"].ToString(); }
            set { this["username"] = value; }
        }

        /// <summary>
        /// The password attribute.
        /// </summary>
        [ConfigurationProperty("password")]
        public string Password
        {
            get { return this["password"].ToString(); }
            set { this["password"] = value; }
        }
    }
}