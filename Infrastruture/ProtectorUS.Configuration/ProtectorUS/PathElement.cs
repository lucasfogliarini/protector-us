﻿using ProtectorUS.Exceptions;
using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration api element.
    /// </summary>
    public class PathElement : ConfigurationElement
    {
        /// <summary>
        /// The url attribute.
        /// </summary>
        [ConfigurationProperty("url")]
        public string Url
        {
            get { return this["url"].ToString(); }
            set { this["url"] = value; }
        }

        /// <summary>
        /// The folder path attribute.
        /// </summary>
        [ConfigurationProperty("path")]
        public string Path
        {
            get
            {
                if (string.IsNullOrEmpty(this["path"].ToString()))
                    throw new ConfigurationNotFoundException("pathPictures", "path");
                  
                return this["path"].ToString();
            }
            set { this["path"] = value; }
        }
    }
}
