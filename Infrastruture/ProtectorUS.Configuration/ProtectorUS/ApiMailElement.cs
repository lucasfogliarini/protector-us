﻿using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration api stripe element.
    /// </summary>
    public class ApiMailElement : ConfigurationElement
    {
        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("key")]
        public string Key
        {
            get { return this["key"].ToString(); }
            set { this["key"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("user")]
        public string User
        {
            get { return this["user"].ToString(); }
            set { this["user"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("pass")]
        public string Password
        {
            get { return this["pass"].ToString(); }
            set { this["pass"] = value; }
        }
    }
}
