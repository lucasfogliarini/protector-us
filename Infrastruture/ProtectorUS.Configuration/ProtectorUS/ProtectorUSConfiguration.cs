﻿using ProtectorUS.Configuration.DocumentDB;
using ProtectorUS.Configuration.Mongo;
using ProtectorUS.Configuration.ProtectorUS;
using System.Configuration;

namespace ProtectorUS.Configuration
{
    /// <summary>
    /// Defines the protectorUS's configuration.
    /// </summary>
    public class ProtectorUSConfiguration : ConfigurationSection
    {
        /// <summary>
        /// The og database element.
        /// </summary>
        [ConfigurationProperty("logDatabase")]
        public DatabaseElement LogDatabase
        {
            get { return (DatabaseElement)this["logDatabase"]; }
            set { this["logDatabase"] = value; }
        }
        /// <summary>
        /// The database element.
        /// </summary>
        [ConfigurationProperty("database")]
        public DatabaseElement Database
        {
            get { return (DatabaseElement)this["database"]; }
            set { this["database"] = value; }
        }

        /// <summary>
        /// The database element (MongoDb).
        /// </summary>
        [ConfigurationProperty("mongoDatabase")]
        public MongoConfiguration MongoDatabase
        {
            get { return (MongoConfiguration)this["mongoDatabase"]; }
            set { this["mongoDatabase"] = value; }
        }

        /// <summary>
        /// The database element (MongoDb).
        /// </summary>
        [ConfigurationProperty("documentDb")]
        public DocumentDBElement DocumentDb
        {
            get { return (DocumentDBElement)this["documentDb"]; }
            set { this["documentDb"] = value; }
        }

        /// <summary>
        /// The application environment.
        /// </summary>
        [ConfigurationProperty("environment")]
        public EnvironmentElement Environment
        {
            get { return (EnvironmentElement)this["environment"]; }
            set { this["environment"] = value; }
        }

        public string GetProtectorDomain()
        {
            switch (Env)
            {
                case ProtectorUSEnvironment.Development:
                    return "http://dev-protector.azurewebsites.net";
                case ProtectorUSEnvironment.Homolog:
                    return "http://homolog.argo-protector.com";
                case ProtectorUSEnvironment.UAT:
                    return "http://uat.argo-protector.com";
                case ProtectorUSEnvironment.Production:
                    return "http://www.argo-protector.com";
            }
            return null;
        }
        public ProtectorUSEnvironment Env
        {
            get
            {
                switch (Environment.Type)
                {
                    case "production":
                        return ProtectorUSEnvironment.Production;
                    case "test":
                        return ProtectorUSEnvironment.Test;
                    case "homolog":
                        return ProtectorUSEnvironment.Homolog;
                    case "uat":
                        return ProtectorUSEnvironment.UAT;
                    default:
                        return ProtectorUSEnvironment.Development;
                }
            }
        }

        const string allowOrigins = "allowOrigins";
        [ConfigurationProperty(allowOrigins)]
        public NameValueConfigurationElement AllowOrigins
        {
            get { return (NameValueConfigurationElement)this[allowOrigins]; }
        }
        const string azureStorageAccount = "azureStorageAccount";
        [ConfigurationProperty(azureStorageAccount)]
        public NameValueConfigurationElement AzureStorageAccount
        {
            get { return (NameValueConfigurationElement)this[azureStorageAccount]; }
        }
        const string applicationInsights = "applicationInsights";
        [ConfigurationProperty(applicationInsights)]
        public KeyValueConfigurationElement ApplicationInsights
        {
            get { return (KeyValueConfigurationElement)this[applicationInsights]; }
        }
        const string argoEmail = "argoEmail";
        [ConfigurationProperty(argoEmail)]
        public NameValueConfigurationElement ArgoEmail
        {
            get { return (NameValueConfigurationElement)this[argoEmail]; }
        }
        /// <summary>
        /// The api Stripe payment element.
        /// </summary>
        [ConfigurationProperty("apiStripe")]
        public ApiStripeElement ApiStripe
        {
            get { return (ApiStripeElement)this["apiStripe"]; }
            set { this["apiStripe"] = value; }
        }

        /// <summary>
        /// The api Stripe payment element.
        /// </summary>
        [ConfigurationProperty("apiESB")]
        public ApiEsbElement ApiESB
        {
            get { return (ApiEsbElement)this["apiESB"]; }
            set { this["apiESBGetToken"] = value; }
        }

        /// <summary>
        /// The api Mail element.
        /// </summary>
        [ConfigurationProperty("apiMail")]
        public ApiMailElement ApiMail
        {
            get { return (ApiMailElement)this["apiMail"]; }
            set { this["apiMail"] = value; }
        }

        /// <summary>
        /// The path Picture element.
        /// </summary>
        [ConfigurationProperty("pathPictures")]
        public PathElement PathPictures
        {
            get { return (PathElement)this["pathPictures"]; }
            set { this["pathPictures"] = value; }
        }

        /// <summary>
        /// The path Picture element.
        /// </summary>
        [ConfigurationProperty("pathPolicies")]
        public PathElement PathPolicies
        {
            get { return (PathElement)this["pathPolicies"]; }
            set { this["pathPolicies"] = value; }
        }

        /// <summary>
        /// The path Picture element.
        /// </summary>
        [ConfigurationProperty("pathClaimsFiles")]
        public PathElement PathClaimsFiles
        {
            get { return (PathElement)this["pathClaimsFiles"]; }
            set { this["pathClaimsFiles"] = value; }
        }


        



        ///// <summary>
        ///// The path Picture element.
        ///// </summary>
        //[ConfigurationProperty("urlBlobs")]
        //public PathElement UrlBlobs
        //{
        //    get { return (PathElement)this["urlBlobs"]; }
        //    set { this["urlBlobs"] = value; }
        //}

        /// <summary>
        /// Gets the protectorUS configuration.
        /// </summary>
        /// <returns>the protectorUS configuration section.</returns>
        public static ProtectorUSConfiguration GetConfig()
        {
            return ConfigurationManager.GetSection(
                KnownConfigurations.ProtectorUS) as ProtectorUSConfiguration;
        }
    }
}
