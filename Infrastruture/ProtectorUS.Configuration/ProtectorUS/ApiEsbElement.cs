﻿using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration api stripe element.
    /// </summary>
    public class ApiEsbElement : ConfigurationElement
    {
        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("urlToken")]
        public string urlToken
        {
            get { return this["urlToken"].ToString(); }
            set { this["urlToken"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("urlPDF")]
        public string urlPDF
        {
            get { return this["urlPDF"].ToString(); }
            set { this["urlPDF"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("urlImageRight")]
        public string urlImageRight
        {
            get { return this["urlImageRight"].ToString(); }
            set { this["urlImageRight"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("imageRightSecurityToken")]
        public string imageRightSecurityToken
        {
            get { return this["imageRightSecurityToken"].ToString(); }
            set { this["imageRightSecurityToken"] = value; }
        }
    }
}
