﻿using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration api element.
    /// </summary>
    public class ApiElement : ConfigurationElement
    {
        /// <summary>
        /// The url attribute.
        /// </summary>
        [ConfigurationProperty("url")]
        public string Url
        {
            get { return this["url"].ToString(); }
            set { this["url"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("key")]
        public string Key
        {
            get { return this["key"].ToString(); }
            set { this["key"] = value; }
        }
    }
}
