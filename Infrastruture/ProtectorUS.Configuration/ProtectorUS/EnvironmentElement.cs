﻿using System;
using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration environment element.
    /// </summary>
    public class EnvironmentElement : ConfigurationElement
    {
        /// <summary>
        /// Gets the environment configuration.
        /// </summary>
        [ConfigurationProperty("type")]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
            set { this["type"] = value; }
        }
    }
}