﻿using System.Configuration;

namespace ProtectorUS.Configuration.ProtectorUS
{
    /// <summary>
    /// Represents the protectorUS's configuration api stripe element.
    /// </summary>
    public class ApiStripeElement : ConfigurationElement
    {
        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("keyPrivate")]
        public string KeyPrivate
        {
            get { return this["keyPrivate"].ToString(); }
            set { this["keyPrivate"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("keyPublic")]
        public string KeyPublic
        {
            get { return this["keyPublic"].ToString(); }
            set { this["keyPublic"] = value; }
        }

        /// <summary>
        /// The key attribute.
        /// </summary>
        [ConfigurationProperty("currency")]
        public string Currency
        {
            get { return this["currency"].ToString(); }
            set { this["currency"] = value; }
        }
    }
}
