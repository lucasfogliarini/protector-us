﻿using ProtectorUS.Configuration;

namespace ProtectorUS
{
    public static class ProtectorDomain
    {
        public static string BrokerageProductUrl(string brokerageAlias, string productAlias)
        {
            return $"{Domain()}/{BrokerageProductPath(brokerageAlias, productAlias)}";
        }
        public static string BrokerageProductPath(string brokerageAlias, string productAlias)
        {
            return $"#/{brokerageAlias}/{productAlias}";
        }
        public static string Domain()
        {
            return ProtectorUSConfiguration.GetConfig().GetProtectorDomain();
        }
    }
}