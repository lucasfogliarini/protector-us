﻿namespace ProtectorUS
{
    public static class ProtectorApplication
    {
        public const string argoManager = "argo";
        public const string brokerCenter = "broker";
        public const string myProtector = "my-protector";

        public static string ToAppString(this ProtectorApp protectorApp)
        {
            switch (protectorApp)
            {
                case ProtectorApp.ArgoManager:
                    return argoManager;
                case ProtectorApp.BrokerCenter:
                    return brokerCenter;
                case ProtectorApp.MyProtector:
                    return myProtector;
            }
            return null;
        }        
    }

    public enum ProtectorApp
    {
        ArgoManager = 1,
        BrokerCenter = 2,
        MyProtector = 3,
    }
}