﻿using System.Configuration;

namespace ProtectorUS.Configuration.DocumentDB
{
    public class DocumentDBElement : ConfigurationElement
    {
        /// <summary>
        /// The connection attribute [url]
        /// </summary>
        [ConfigurationProperty("url")]
        public string Url
        {
            get { return this["url"].ToString(); }
            set { this["url"] = value; }
        }

        /// <summary>
        /// The connection attribute [name]
        /// </summary>
        [ConfigurationProperty("key")]
        public string Key
        {
            get { return this["key"].ToString(); }
            set { this["key"] = value; }
        }

        /// <summary>
        /// The connection attribute [name]
        /// </summary>
        [ConfigurationProperty("database")]
        public string Database
        {
            get { return this["database"].ToString(); }
            set { this["database"] = value; }
        }
    }
}
