﻿using System;
using System.Linq.Expressions;

namespace ProtectorUS.Exceptions
{
    public class ConfigurationNotFoundException : Exception
    {
        public ConfigurationNotFoundException(string element, string property)
            : base($"Element = {element}, Property = {property}")
        {
        }

    }
}
