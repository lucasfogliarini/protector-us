﻿namespace ProtectorUS.Configuration
{
    /// <summary>
    /// Define the names of known configurations.
    /// </summary>
    public static class KnownConfigurations
    {
        /// <summary>
        /// ProtectorUS's configuration.
        /// </summary>
        public static string ProtectorUS { get; } = "protectorUS";


        /// <summary>
        /// Api configuration.
        /// </summary>
        public static string ApiConfiguration { get; } = "apiConfig";


        /// <summary>
        /// Facebook's configuration.
        /// </summary>
        public static string Facebook { get; } = "facebook";

    }
}