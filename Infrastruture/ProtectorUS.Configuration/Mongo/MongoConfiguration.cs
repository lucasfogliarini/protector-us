﻿using System.Configuration;

namespace ProtectorUS.Configuration.Mongo
{
    public class MongoConfiguration : ConfigurationElement
    {
        /// <summary>
        /// The connection attribute [connection]
        /// </summary>
        [ConfigurationProperty("connection")]
        public string Connection
        {
            get { return this["connection"].ToString(); }
            set { this["connection"] = value; }
        }

        /// <summary>
        /// The connection attribute [name]
        /// </summary>
        [ConfigurationProperty("name")]
        public string Name
        {
            get { return this["name"].ToString(); }
            set { this["name"] = value; }
        }
    }
}
