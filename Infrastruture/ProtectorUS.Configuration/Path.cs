﻿using System;

namespace ProtectorUS
{
    public static class FileManager
    {
        /// <summary>
        /// A Guid based file name.
        /// </summary>
        /// <param name="extension">without '.', i.e.: 'png'</param>
        public static string CreateFileName(string extension)
        {
            return $"{Guid.NewGuid()}.{extension}";
        }
    }
}