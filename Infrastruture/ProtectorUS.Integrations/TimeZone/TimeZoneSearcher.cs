﻿using GoogleMapsApi;
using GoogleMapsApi.Entities.TimeZone.Request;
using GoogleMapsApi.Entities.TimeZone.Response;
using GoogleMapsApi.Entities.Common;
using System;

namespace ProtectorUS.TimeZone
{
    using Geocoding;
    using Exceptions;
    public static class TimeZoneSearcher
    {
        public static DateTime GetTimeNowAt(string zipCode)
        {
            var coordinates = Geocode.GetCoordinates(zipCode);
            return GetTimeNowAt(coordinates.Latitude, coordinates.Longitude);
        }
        public static int GetTimeZoneOffset(string zipCode)
        {
            var coordinates = Geocode.GetCoordinates(zipCode);
            TimeZoneResponse timezone = GetTimezone(coordinates.Latitude, coordinates.Longitude);
            return (int)timezone.RawOffSet;
        }
        public static DateTime GetTimeNowAt(double latitude, double longitude)
        {
            TimeZoneResponse response = GetTimezone(latitude, longitude);
            double timestamp = response.RawOffSet + response.OffSet + DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            DateTimeOffset datetimeOffset = DateTimeOffset.FromUnixTimeSeconds((long)timestamp);
            return datetimeOffset.DateTime;
        }
        private static TimeZoneResponse GetTimezone(double latitude, double longitude, DateTime? dateTime = null)
        {
            TimeZoneResponse response = GoogleMaps.TimeZone.Query(new TimeZoneRequest()
            {
                TimeStamp = dateTime ?? DateTime.UtcNow,
                Location = new Location(latitude, longitude),
                Language = language
            });
            if (response.Status != Status.OK)
            {
                throw new NotFoundException(e => e.EL059);
            }
            return response;
        }
        const string language = "en-US";
    }
}
