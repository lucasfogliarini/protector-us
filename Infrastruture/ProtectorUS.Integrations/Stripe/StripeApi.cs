using ProtectorUS.Exceptions;
using ProtectorUS.Model;
using Stripe;
using System;
namespace ProtectorUS.Integrations.Stripe
{
    using Configuration;
    using Configuration.ProtectorUS;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net;
    /// <summary>
    /// https://github.com/jaymedavis/stripe.net
    /// </summary>
    public class StripeApi : IStripeApi
    {
        readonly ApiStripeElement stripeConfig = ProtectorUSConfiguration.GetConfig().ApiStripe;
        const string country = "US";
        public StripeApi()
        {
            StripeConfiguration.SetApiKey(stripeConfig.KeyPrivate);
        }
        /// <summary>
        /// https://stripe.com/docs/api#charge_object
        /// </summary>
        /// <param name="Amount">in cents</param>
        /// <param name="ApplicationFee">in cents</param>
        /// <returns></returns>
        public ChargeTransaction Charge(ChargeTransactionParameters chargeParameters)
        {
            CreditCardInfo creditCard = chargeParameters.CreditCard;
            var chargeOptions = new StripeChargeCreateOptions()
            {
                Destination = chargeParameters.ChargeAccount.Id,
                Amount = chargeParameters.Amount,
                ApplicationFee = chargeParameters.ApplicationFee,
                Currency = stripeConfig.Currency,
                Description = chargeParameters.Description,
                Metadata = chargeParameters.MetaData,
                SourceCard = new SourceCard()
                {
                    Number = creditCard.Number,
                    ExpirationYear = creditCard.ExpirationYear,
                    ExpirationMonth = creditCard.ExpirationMonth,
                    Cvc = creditCard.Cvc,
                    Capture = true,
                    AddressLine1 = creditCard.AddressLine1,
                    AddressLine2 = creditCard.AddressLine2,
                    AddressZip = creditCard.ZipCode,
                    AddressCity = creditCard.City,
                    AddressState = creditCard.State,
                    AddressCountry = creditCard.Country,
                    Name = creditCard.CardHoldName,
                },
            };

            try
            {
                var stripeChargeService = new StripeChargeService();
                StripeCharge stripeCharge = stripeChargeService.Create(chargeOptions);

                var balanceService = new StripeBalanceService();
                StripeBalanceTransaction balanceTransaction = balanceService.Get(stripeCharge.BalanceTransactionId);

                StripeFee stripeFee = balanceTransaction.FeeDetails.Find(x => x.Type == "stripe_fee");

                return new ChargeTransaction()
                {
                    PaymentStatus = stripeCharge.Paid ? PaymentStatus.Approved : PaymentStatus.Denied,
                    TransactionCode = stripeCharge.Id,
                    StripeFee = ConvertStripeFeeCentsToDollar(stripeFee),
                };
            }
            catch (StripeException ex)
            {
                throw new PaymentException(ex.StripeError.Message);
            }
        }

        public decimal ConvertStripeFeeCentsToDollar(StripeFee stripeFee)
        {
            int dollar = stripeFee.Amount / 100;
            int cents = stripeFee.Amount % 100;

            string valueFee = dollar + "." + cents;

            return decimal.Parse(valueFee, CultureInfo.InvariantCulture);
        }

        public ChargeAccount GetManagedAccount(string accountId)
        {
            try
            {
                var accountService = new StripeAccountService();
                StripeAccount stripeAccount = accountService.Get(accountId);
                return ToChargeAccount(stripeAccount);
            }
            catch
            {
                return null;
            }
        }
        private static ChargeAccount ToChargeAccount(StripeAccount stripeAccount)
        {
            ChargeAccountVerification accountVerification = null;
            if (stripeAccount.AccountVerification?.FieldsNeeded.Length > 0)
            {
                accountVerification = new ChargeAccountVerification()
                {
                    DisabledReason = stripeAccount.AccountVerification.DisabledReason,
                    DueBy = stripeAccount.AccountVerification.DueBy,
                    FieldsNeeded = stripeAccount.AccountVerification.FieldsNeeded
                };
            }
            string legalEntityStatus = stripeAccount.LegalEntity?.LegalEntityVerification?.Status ?? ChargeAccountStatus.Unverified.Description();
            ChargeAccountStatus chargeAccountStatus = EnumExtensions.GetValue<ChargeAccountStatus>(legalEntityStatus);
            return new ChargeAccount()
            {
                Id = stripeAccount.Id,
                Status = chargeAccountStatus,
                ChargesEnabled = stripeAccount.ChargesEnabled,
                TransfersEnabled = stripeAccount.TransfersEnabled,
                Verification = accountVerification,
            };
        }
        public ChargeAccount CreateManagedAccount(LegalEntity legalEntity, BankAccount bankAccount, byte[] termsAcceptanceIp)
        {
            try
            {
                var accountService = new StripeAccountService();
                StripeAccount account = accountService.Create(new StripeAccountCreateOptions()
                {
                    Managed = true,
                    Country = country
                });
                return UpdateManagedAccount(account.Id, legalEntity, bankAccount, termsAcceptanceIp);
            }
            catch (StripeException ex)
            {
                throw new ArgumentInvalidException(e => e.EL050, null , new string[] { ex.Message });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ChargeAccount UpdateManagedAccount(string accountId, LegalEntity legalEntity, BankAccount bankAccount, byte[] termsAcceptanceIp = null)
        {
            try
            {
                var accountService = new StripeAccountService();
                var business = legalEntity?.Business;
                var address = business?.Address;
                var birthDate = legalEntity?.BirthDate;
                var updateOptions = new StripeAccountUpdateOptions()
                {
                    LegalEntity = new StripeAccountLegalEntityOptions()
                    {
                        BirthDay = birthDate?.Day.ToString(),
                        BirthMonth = birthDate?.Month.ToString(),
                        BirthYear = birthDate?.Year.ToString(),
                        FirstName = legalEntity?.FirstName,
                        LastName = legalEntity?.LastName,
                        SSNLast4 = legalEntity?.SSNLast4?.ToString(),
                        BusinessName = business?.Name,
                        BusinessTaxId = legalEntity?.BusinessTaxId,
                        AddressLine1 = address?.StreetLine1,
                        AddressLine2 = address?.StreetLine2,
                        AddressPostalCode = address?.Region?.ZipCode,
                        AddressCity = address?.Region?.City?.Name,
                        AddressState = address?.Region?.City?.State?.Name,
                        AddressTwoLetterCountry = country,
                        VerificationDocumentFileId = legalEntity?.VerificationDocumentFileId,
                        PersonalIdNumber = legalEntity?.PersonalIdNumber,
                        Type = PersonalType.Company.Description(),
                    }
                };
                if (bankAccount != null)
                {
                    updateOptions.ExternalBankAccount = new StripeAccountBankAccountOptions()
                    {
                        AccountNumber = bankAccount?.AccountNumber,
                        AccountHolderType = bankAccount?.AccountHolderType.Description(),
                        RoutingNumber = bankAccount?.RoutingNumber,
                        Country = country,
                        Currency = stripeConfig.Currency,
                    };
                }

                if (termsAcceptanceIp != null)
                {
                    updateOptions.TosAcceptanceDate = DateTime.Now;
                    updateOptions.TosAcceptanceIp = new IPAddress(termsAcceptanceIp).ToString();
                }

                StripeAccount stripeAccount = accountService.Update(accountId, updateOptions);
                return ToChargeAccount(stripeAccount);
            }
            catch (StripeException ex)
            {
                throw new ArgumentInvalidException(e => e.EL051, new object[] { accountId }, new string[] { ex.Message });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static PaymentGatewayEvent ParseEvent(string stripeEventBody)
        {
            try
            {
                var stripeEvent = StripeEventUtility.ParseEvent(stripeEventBody);
                return new PaymentGatewayEvent()
                {
                    Created = stripeEvent.Created,
                    LiveMode = stripeEvent.LiveMode,
                    Object = stripeEvent.Data.Object,
                    PreviousAttributes = stripeEvent.Data.PreviousAttributes,
                    Type = stripeEvent.Type
                };
            }
            catch
            {
                return null;
            }
        }
        public static ChargeAccount ToChargeAccount(string stripeEventBody)
        {
            try
            {
                var stripeEvent = StripeEventUtility.ParseEvent(stripeEventBody);
                if (stripeEvent.Type == StripeEvents.AccountUpdated)
                {
                    StripeAccount stripeAccount = Mapper<StripeAccount>.MapFromJson(stripeEvent.Data.Object.ToString());
                    return ToChargeAccount(stripeAccount);
                }
            }
            catch
            {
            }
            return null;
        }
    }
}
