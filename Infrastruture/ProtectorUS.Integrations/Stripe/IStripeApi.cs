﻿using ProtectorUS.Model;

namespace ProtectorUS.Integrations.Stripe
{
    public interface IStripeApi
    {
        /// <summary>
        /// https://stripe.com/docs/api#charge_object
        /// </summary>
        /// <param name="amount">in cents</param>
        /// <param name="applicationFee">in cents</param>
        /// <returns></returns>
        ChargeTransaction Charge(ChargeTransactionParameters chargeTransactionParameters);
        ChargeAccount GetManagedAccount(string accountId);
        ChargeAccount CreateManagedAccount(LegalEntity legalEntity, BankAccount bankAccount, byte[] termsAcceptanceIp);
        ChargeAccount UpdateManagedAccount(string accountId, LegalEntity legalEntity, BankAccount bankAccount, byte[] termsAcceptanceIp = null);
    }
}