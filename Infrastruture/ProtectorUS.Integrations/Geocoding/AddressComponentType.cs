﻿using System.ComponentModel;
namespace ProtectorUS.Geocoding
{
    public enum AddressComponentType
    {
        [Description("street_address")]
        StreetAddress,// indicates a precise street address.
        [Description("route")]
        Route,// indicates a named route (such as "US 101").
        [Description("intersection")]
        Intersection,// indicates a major intersection, usually of two major roads.
        [Description("political")]
        Political,// indicates a political entity. Usually, this type indicates a polygon of some civil administration.
        [Description("country")]
        Country,// indicates the national political entity, and is typically the highest order type returned by the Geocoder.
        [Description("administrative_area_level_1")]
        AdministrativeAreaLevel1,// indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels.
        [Description("administrative_area_level_2")]
        AdministrativeAreaLvel2, // indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
        [Description("administrative_area_level_3")]
        AdministrativeAreaLevel3, // indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        [Description("colloquial_area")]
        ColloquialArea, // indicates a commonly-used alternative name for the entity.
        [Description("locality")]
        Locality, // indicates an incorporated city or town political entity.
        [Description("sublocality")]
        Sublocality, // indicates an first-order civil entity below a locality
        [Description("neighborhood")]
        Neighborhood, // indicates a named neighborhood
        [Description("premise")]
        Premise, // indicates a named location, usually a building or collection of buildings with a common name
        [Description("subpremise")]
        Subpremise, // indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
        [Description("postal_code")]
        PostalCode, // indicates a postal code as used to address postal mail within the country.
        [Description("natural_feature")]
        NaturalFeature, // indicates a prominent natural feature.
        [Description("airport")]
        Airport, // indicates an airport.
        [Description("park")]
        Park, // indicates a named park.
        [Description("point_of_interest")]
        PointOfInterest, // indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category such as "Empire State Building" or "Statue of Liberty."
                         //In addition to the above, address components may exhibit the following types:
        [Description("post_box")]
        PostBox,// indicates a specific postal box.
        [Description("street_number")]
        StreetNumber, // indicates the precise street number.
        [Description("floor")]
        Floor, // indicates the floor of a building address.
        [Description("room")]
        Room // indicates the room of a building address.
    }
}
