﻿using System.Linq;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Geocoding.Response;
using GoogleMapsApi.Entities.Geocoding.Request;
namespace ProtectorUS.Geocoding
{
    using Model;
    using System;
    using System.Device.Location;

    /// <summary>
    /// https://developers.google.com/maps/pricing-and-plans/#web-services
    /// 2.500 solicitações gratuitas por dia
    /// 10 solicitações por segundo
    /// </summary>
    public static class Geocode
    {
        public static GeoCoordinate GetCoordinates(string zipcode, string streetLine1 = null)
        {
            try
            {
                Result addressResult = GetAddress(zipcode, streetLine1);
                return new GeoCoordinate(addressResult.Geometry.Location.Latitude, addressResult.Geometry.Location.Longitude);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public static Region GetRegion(string zipCode)
        {
            try
            {
                Result addressResult = GetAddress(zipCode);
                AddressComponent city = addressResult.GetCity();
                AddressComponent state = addressResult.GetState();
                return new Region()
                {
                    ZipCode = zipCode,
                    City = new City()
                    {
                        Name = city.LongName,
                        State = new State()
                        {
                            Abbreviation = state.ShortName,
                            Name = state.LongName,
                        }
                    }
                };
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        private static Result GetAddress(string zipcode, string streetLine1)
        {
            var request = new GeocodingRequest()
            {
                Address = streetLine1,
                Components = new GeocodingComponents()
                {
                    PostalCode = zipcode,
                    Country = country
                }
            };
            Result addressResult = Request(request);
            if (addressResult == null)
            {
                request.Address = null;
                return Request(request);
            }

            return addressResult;
        }
        private static Result GetAddress(string zipCode)
        {
            return Request(new GeocodingRequest()
            {
                Components = new GeocodingComponents()
                {
                    PostalCode = zipCode,
                    Country = country
                }
            });
        }
        private static Result Request(GeocodingRequest request)
        {
            request.Address = request.Address ?? ".";
            request.Language = language;
            GeocodingResponse geocodingResponse = GoogleMaps.Geocode.Query(request);
            return geocodingResponse.Results?.FirstOrDefault();
        }

        const string country = "US";
        const string language = "en-US";
    }
}
