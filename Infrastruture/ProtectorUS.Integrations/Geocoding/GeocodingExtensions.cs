﻿using System.Linq;
using GoogleMapsApi.Entities.Geocoding.Response;
using System;

namespace ProtectorUS.Geocoding
{
    public static class GeocodingExtensions
    {
        /// <summary>
        /// https://developers.google.com/maps/documentation/geocoding/intro#Types
        /// </summary>
        public static AddressComponent GetAddressComponent(this Result result, params AddressComponentType[] locationTypes)
        {
            foreach (AddressComponentType locationType in locationTypes)
            {
                AddressComponent addressComponent = result.GetAddressComponent(locationType);
                if (addressComponent != null)
                    return addressComponent;
            }
            return null;
        }
        public static AddressComponent GetCity(this Result result)
        {
            AddressComponentType[] cityTypes =
            {
                AddressComponentType.Locality,
                AddressComponentType.AdministrativeAreaLevel3,
                AddressComponentType.Sublocality,
                AddressComponentType.Neighborhood,
            };
            return result.GetAddressComponent(cityTypes);
        }
        public static AddressComponent GetState(this Result result)
        {
            return result.GetAddressComponent(AddressComponentType.AdministrativeAreaLevel1);
        }
        private static AddressComponent GetAddressComponent(this Result result, AddressComponentType addressComponentType)
        {
            return result.AddressComponents.FirstOrDefault(c => c.Types.Contains(addressComponentType.Description()));
        }
    }
}
