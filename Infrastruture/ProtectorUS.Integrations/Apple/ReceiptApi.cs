﻿using Newtonsoft.Json;
using RestSharp;
namespace ProtectorUS.Integrations.Apple
{
    using Models;
    internal class ReceiptApi : AppleBaseApi, IReceiptApi
    {

        public VerifyReceiptResponse Verify(string receiptData)
        {
            return Verify(receiptData, AppleEnvironmentType.Release);
        }



        private VerifyReceiptResponse Verify(string receiptData, AppleEnvironmentType environment)
        {
            var client = base.BuildClient(environment);

            var request = new RestRequest("verifyReceipt", Method.POST)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharpJsonSerializer()
            };

            request.AddBody(new VerifyReceiptRequest
            {
                Password = base.Password,
                ReceiptData = receiptData,
            });

            var response = client.Execute(request);

            if (response != null)
            {
                var receipt = JsonConvert.DeserializeObject<VerifyReceiptResponse>(response.Content);

                if (receipt.ReceiptStatus == AppleReceiptStatusType.ReceiptFromTestEnvironment && environment != AppleEnvironmentType.SandBox)
                {
                    return Verify(receiptData, AppleEnvironmentType.SandBox);
                }

                return receipt;
            }

            return null;
        }

    }
}
