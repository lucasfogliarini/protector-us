using System.Configuration;

namespace ProtectorUS.Integrations.Apple.Config
{
    /// <summary>
    /// Represents the apple configuration <credentials> element.
    /// </summary>
    public class CredentialsElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the credentials element password attribute.
        /// </summary>
        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return this["password"].ToString(); }
            set { this["password"] = value; }
        }
    }
}