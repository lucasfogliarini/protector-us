﻿using System.Configuration;

namespace ProtectorUS.Integrations.Apple.Config
{
    /// <summary>
    /// Represents the apple configuration section.
    /// </summary>
    public class AppleConfiguration : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the apple configuration environment element.
        /// </summary>
        [ConfigurationProperty("environment", IsRequired = true)]
        public EnvironmentElement Environment
        {
            get { return (EnvironmentElement)this["environment"]; }
            set { this["environment"] = value; }
        }

        /// <summary>
        /// Gets or sets the apple configuration credentials element.
        /// </summary>
        [ConfigurationProperty("credentials", IsRequired = true)]
        public CredentialsElement Credentials
        {
            get { return (CredentialsElement)this["credentials"]; }
            set { this["credentials"] = value; }
        }

        /// <summary>
        /// Gets the apple configuration section.
        /// </summary>
        public static AppleConfiguration Get()
        {
            return ConfigurationManager.GetSection("apple") as AppleConfiguration;
        }
    }
}
