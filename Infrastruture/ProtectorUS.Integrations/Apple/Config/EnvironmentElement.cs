using System.Configuration;

namespace ProtectorUS.Integrations.Apple.Config
{
    /// <summary>
    /// Represents the apple configuration <environment> element.
    /// </summary>
    public class EnvironmentElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the environment element test url attribute.
        /// </summary>
        [ConfigurationProperty("sandbox", IsRequired = true)]
        public string Sandbox
        {
            get { return this["sandbox"].ToString(); }
            set { this["sandbox"] = value; }
        }

        /// <summary>
        /// Gets or sets the environment element release url attribute.
        /// </summary>
        [ConfigurationProperty("release", IsRequired = true)]
        public string Release
        {
            get { return this["release"].ToString(); }
            set { this["release"] = value; }
        }

    }
}