﻿namespace ProtectorUS.Integrations.Apple
{
    public interface IAppleApi
    {
        IReceiptApi Receipt { get; set; }
    }
}