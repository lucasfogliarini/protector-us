﻿using ProtectorUS.Integrations.Apple.Models;
using RestSharp;

namespace ProtectorUS.Integrations.Apple
{  
    using Config;

    internal class AppleBaseApi
    {
        /// <summary>
        /// Apple config.
        /// </summary>
        private readonly AppleConfiguration _config;

        protected string Password { get; private set; }


        public AppleBaseApi()
        {
            _config = AppleConfiguration.Get();
            this.Password = _config.Credentials.Password;
        }


        /// <summary>
        /// Builds a apple api client.
        /// </summary>
        /// <returns>the client.</returns>
        public RestClient BuildClient(AppleEnvironmentType environment)
        {
            var url = environment == AppleEnvironmentType.Release ? _config.Environment.Release : _config.Environment.Sandbox;

            var client = new RestClient(url);
            client.AddDefaultHeader("Content-Type", "application/json");
            client.AddDefaultHeader("Accept", "application/json");
            client.Timeout = 40000;

            return client;
        }
    }
}
