namespace ProtectorUS.Integrations.Apple.Models
{
    public enum AppleReceiptStatusType
    {
        /// <summary>
        /// Status ok
        /// </summary>
        Ok = 0,
        /// <summary>
        /// The App Store could not read the JSON object you provided.
        /// </summary>
        InvalidJsonObject = 21000,
        /// <summary>
        /// The data in the receipt-data property was malformed or missing.
        /// </summary>
        InvalidReceiptData = 21002,
        /// <summary>
        /// The receipt could not be authenticated.
        /// </summary>
        NotAuthenticatedReceiptData = 21003,
        /// <summary>
        /// The shared secret you provided does not match the shared secret on file for your account.
        /// Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions
        /// </summary>
        InvalidPassword = 21004,
        /// <summary>
        /// The receipt server is not currently available.
        /// </summary>
        ServerNotAvailable = 21005,
        /// <summary>
        /// This receipt is valid but the subscription has expired. When this status code is returned to your server, 
        /// the receipt data is also decoded and returned as part of the response.
        /// Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.
        /// </summary>
        SubscriptionHasExpired = 21006,
        /// <summary>
        /// This receipt is from the test environment, but it was sent to the production environment for verification.
        /// Send it to the test environment instead.
        /// </summary>
        ReceiptFromTestEnvironment = 21007,
        /// <summary>
        /// This receipt is from the production environment, but it was sent to the test environment for verification. 
        /// Send it to the production environment instead.
        /// </summary>
        ReceiptFromProductionEnvironment = 21008,
    }
}