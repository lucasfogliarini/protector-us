using System;
using Newtonsoft.Json;

namespace ProtectorUS.Integrations.Apple.Models
{
    
    /// <summary>
    /// This is the representation for an App Receipt from Apple store.
    /// Follow a example of a JSON  JSON  from a App Receipt:
    /// </summary>
    /// <example>
    ///  {
    ///     "receipt_type": "ProductionSandbox",
    ///     "adam_id": 0,
    ///     "app_item_id": 0,
    ///     "bundle_id": "com.ProtectorUS.IonStorm",
    ///     "application_version": "4.1.173",
    ///     "download_id": 0,
    ///     "version_external_identifier": 0,
    ///     "request_date": "2014-10-30 17:56:36 Etc/GMT",
    ///     "request_date_ms": "1414691796445",
    ///     "request_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///     "original_purchase_date": "2013-08-01 07:00:00 Etc/GMT",
    ///     "original_purchase_date_ms": "1375340400000",
    ///     "original_purchase_date_pst": "2013-08-01 00:00:00 America/Los_Angeles",
    ///     "original_application_version": "1.0",
    ///     "in_app": []
    /// }
    /// </example>
    public class AppReceipt
    {
        /// <summary>
        /// A string that the App Store uses to uniquely identify the application that created the transaction.
        /// 
        /// Obs.: Apps are assigned an identifier only in the production environment, 
        /// so this key is not present for receipts created in the test environment.
        /// </summary>
        [JsonProperty("app_item_id")]
        public string AppItemId { get; set; }

        /// <summary>
        /// The app's bundle identifier.
        /// </summary>
        /// <example> Ex.: "fm.ProtectorUS.IonStorm"</example>
        [JsonProperty("bundle_id")]
        public string BundleId { get; set; }

        /// <summary>
        /// The app's version number.
        /// </summary>
        /// <example>Ex.: "4.1.173"</example>
        [JsonProperty("application_version")]
        public string ApplicationVersion { get; set; }

        /// <summary>
        /// An arbitrary number that uniquely identifies a revision of your application.
        /// 
        /// Obs.: This key is not present for receipts created in the test environment.
        /// </summary>
        [JsonProperty("version_external_identifier")]
        public long VersionExternalIdentifier { get; set; }

        /// <summary>
        /// The OriginalPurchaseDateMs.
        /// </summary>
        /// <example>Ex: 1414516878492</example>
        [JsonProperty("original_purchase_date_ms")]
        public long OriginalPurchaseDateMs { get; set; }

        /// <summary>
        /// Returns the OriginalPurchaseDateMs in a DateTime format.
        /// </summary>
        public DateTime OriginalPurchaseDate
        {
            get
            {
                var time = TimeSpan.FromMilliseconds(OriginalPurchaseDateMs);
                var result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return result.Add(time);
            }
        }

        /// <summary>
        /// The version of the app that was originally purchased.
        /// 
        /// Obs.: In the sandbox environment, the value of this field is always 1.0.
        /// </summary>
        /// <example>Ex.: 1.0</example>
        [JsonProperty("original_application_version")]
        public string OriginalApplicationVersion { get; set; }

        /// <summary>
        /// The representation of the all in-app purchase receipts.
        /// 
        /// In the JSON file, the value of this key is an array containing all in-app purchase receipts.
        /// </summary>
        [JsonProperty("in-app")]
        public InAppPurchaseReceipt[] InAppPurchaseReceipts { get; set; }
    }
}