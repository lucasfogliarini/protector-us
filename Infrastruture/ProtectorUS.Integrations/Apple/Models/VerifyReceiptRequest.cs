namespace ProtectorUS.Integrations.Apple.Models
{
    using Newtonsoft.Json;

    public class VerifyReceiptRequest
    {
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("receipt-data")]
        public string ReceiptData { get; set; }
    }
}