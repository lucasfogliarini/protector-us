﻿namespace ProtectorUS.Integrations.Apple.Models
{
    public enum AppleEnvironmentType
    {
        SandBox = 1,
        Release
    }
}