using System;
using Newtonsoft.Json;

namespace ProtectorUS.Integrations.Apple.Models
{
    /// <summary>
    /// This is the representation for an in-app purchase receipt.
    /// </summary>
    /// <example>
    /// Follow an example of a response JSON of a In-App Purchase Receipt from Apple store:
    /// {
    ///     "quantity": "1",
    ///     "product_id": "ios_monthly",
    ///     "transaction_id": "1000000129871188",
    ///     "original_transaction_id": "1000000129871188",
    ///     "purchase_date": "2014-10-30 16:09:08 Etc/GMT",
    ///     "purchase_date_ms": "1414685348000",
    ///     "purchase_date_pst": "2014-10-30 09:09:08 America/Los_Angeles",
    ///     "original_purchase_date": "2014-10-30 16:09:04 Etc/GMT",
    ///     "original_purchase_date_ms": "1414685344000",
    ///     "original_purchase_date_pst": "2014-10-30 09:09:04 America/Los_Angeles",
    ///     "expires_date": "2014-10-30 16:14:03 Etc/GMT",
    ///     "expires_date_ms": "1414685643000",
    ///     "expires_date_pst": "2014-10-30 09:14:03 America/Los_Angeles",
    ///     "web_order_line_item_id": "1000000028772105",
    ///     "is_trial_period": "false"
    /// }
    /// </example>
    public class InAppPurchaseReceipt
    {
        /// <summary>
        /// The number of items purchased.
        /// </summary>
        /// <example>Ex: 1</example>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// The product identifier of the item that was purchased.
        /// </summary>
        /// <example>Ex: ios_monthly</example>
        [JsonProperty("product_id")]
        public string ProductId { get; set; }

        /// <summary>
        /// The transaction identifier of the item that was purchased.
        /// </summary>
        /// <example>Ex: 1000000129871188</example>
        [JsonProperty("transaction_id")]
        public long TransactionId { get; set; }

        /// <summary>
        /// For a transaction that restores a previous transaction, the transaction identifier of the original transaction. 
        /// Otherwise, identical to the transaction identifier.
        /// 
        /// All receipts in a chain of renewals for an auto-renewable subscription have the same value for this field.
        /// 
        /// </summary>
        /// <example>Ex: 1000000129871188</example>
        [JsonProperty("original_transaction_id")]
        public long OriginalTransactionId { get; set; }

        /// <summary>
        /// The expiration date for the subscription, expressed as the number of milliseconds since January 1, 1970, 00:00:00 GMT.
        /// </summary>
        /// <example>Ex: 1414516877874</example>
        [JsonProperty("purchase_date_ms")]
        public long PurchaseDateMs { get; set; }

        /// <summary>
        /// Returns the PurchaseDateMs in a DateTime format
        /// </summary>
        public DateTime PurchaseDate
        {
            get
            {
                var time = TimeSpan.FromMilliseconds(PurchaseDateMs);
                var result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return result.Add(time);
            }
        }

        /// <example>
        /// Ex: 1414685344000
        /// </example>
        [JsonProperty("original_purchase_date_ms")]
        public long OriginalPurchaseDateMs { get; set; }

        /// <summary>
        /// Returns the OriginalPurchaseDateMs in a DateTime format.
        /// </summary>
        public DateTime OriginalPurchaseDate
        {
            get
            {
                var time = TimeSpan.FromMilliseconds(OriginalPurchaseDateMs);
                var result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return result.Add(time);
            }
        }

        /// <summary>
        /// The expiration date for the subscription, expressed as the number of milliseconds since January 1, 1970, 00:00:00 GMT.
        /// </summary>
        /// <example>Ex: 1414685643000</example>
        [JsonProperty("expires_date_ms")]
        public long ExpiresDateMs { get; set; }

        /// <summary>
        /// Returns the ExpiresDateMs in a DateTime format
        /// </summary>
        public DateTime ExpiresDate
        {
            get
            {
                var time = TimeSpan.FromMilliseconds(ExpiresDateMs);
                var result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return result.Add(time);
            }
        }

        /// <summary>
        /// The primary key for identifying subscription purchases.
        /// </summary>
        /// <example>Ex: 1000000028772105</example>
        [JsonProperty("web_order_line_item_id")]
        public long WebOrderLineItemId { get; set; }

        /// <summary>
        /// True if the subscription is in trial period. False if not.
        /// </summary>
        [JsonProperty("is_trial_period")]
        public bool IsTrialPeriod { get; set; }
    }
}