using Newtonsoft.Json;

namespace ProtectorUS.Integrations.Apple.Models
{
    /// <summary>
    /// A representation for the JSON response by verifyReceipt method from apple api.
    /// </summary>
    /// <example>
    /// Follow a JSON example of an apple app receipt response:
    /// {
    ///     "status": 0,
    ///     "environment": "Sandbox",
    ///     "receipt": {
    ///         "receipt_type": "ProductionSandbox",
    ///         "adam_id": 0,
    ///         "app_item_id": 0,
    ///         "bundle_id": "fm.ProtectorUS.IonStorm",
    ///         "application_version": "4.1.173",
    ///         "download_id": 0,
    ///         "version_external_identifier": 0,
    ///         "request_date": "2014-10-30 17:56:36 Etc/GMT",
    ///         "request_date_ms": "1414691796445",
    ///         "request_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///         "original_purchase_date": "2013-08-01 07:00:00 Etc/GMT",
    ///         "original_purchase_date_ms": "1375340400000",
    ///         "original_purchase_date_pst": "2013-08-01 00:00:00 America/Los_Angeles",
    ///         "original_application_version": "1.0",
    ///         "in_app": [
    ///             {
    ///                 "quantity": "1",
    ///                 "product_id": "ios_monthly",
    ///                 "transaction_id": "1000000129871188",
    ///                 "original_transaction_id": "1000000129871188",
    ///                 "purchase_date": "2014-10-30 16:09:08 Etc/GMT",
    ///                 "purchase_date_ms": "1414685348000",
    ///                 "purchase_date_pst": "2014-10-30 09:09:08 America/Los_Angeles",
    ///                 "original_purchase_date": "2014-10-30 16:09:04 Etc/GMT",
    ///                 "original_purchase_date_ms": "1414685344000",
    ///                 "original_purchase_date_pst": "2014-10-30 09:09:04 America/Los_Angeles",
    ///                 "expires_date": "2014-10-30 16:14:03 Etc/GMT",
    ///                 "expires_date_ms": "1414685643000",
    ///                 "expires_date_pst": "2014-10-30 09:14:03 America/Los_Angeles",
    ///                 "web_order_line_item_id": "1000000028772105",
    ///                 "is_trial_period": "false"
    ///             }
    ///         ]
    ///     },
    ///     "latest_receipt_info": [
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129871188",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:09:04 Etc/GMT",
    ///             "original_purchase_date_ms": "1414685344000",
    ///             "original_purchase_date_pst": "2014-10-30 09:09:04 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:14:03 Etc/GMT",
    ///             "expires_date_ms": "1414685643000",
    ///             "expires_date_pst": "2014-10-30 09:14:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772105",
    ///             "is_trial_period": "false"
    ///         },
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129871594",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:12:43 Etc/GMT",
    ///             "original_purchase_date_ms": "1414685563000",
    ///             "original_purchase_date_pst": "2014-10-30 09:12:43 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:19:03 Etc/GMT",
    ///             "expires_date_ms": "1414685943000",
    ///             "expires_date_pst": "2014-10-30 09:19:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772104",
    ///             "is_trial_period": "false"
    ///         },
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129872683",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:17:07 Etc/GMT",
    ///             "original_purchase_date_ms": "1414685827000",
    ///             "original_purchase_date_pst": "2014-10-30 09:17:07 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:24:03 Etc/GMT",
    ///             "expires_date_ms": "1414686243000",
    ///             "expires_date_pst": "2014-10-30 09:24:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772120",
    ///             "is_trial_period": "false"
    ///         },
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129873332",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:22:19 Etc/GMT",
    ///             "original_purchase_date_ms": "1414686139000",
    ///             "original_purchase_date_pst": "2014-10-30 09:22:19 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:29:03 Etc/GMT",
    ///             "expires_date_ms": "1414686543000",
    ///             "expires_date_pst": "2014-10-30 09:29:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772147",
    ///             "is_trial_period": "false"
    ///         },
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129874375",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:28:03 Etc/GMT",
    ///             "original_purchase_date_ms": "1414686483000",
    ///             "original_purchase_date_pst": "2014-10-30 09:28:03 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:34:03 Etc/GMT",
    ///             "expires_date_ms": "1414686843000",
    ///             "expires_date_pst": "2014-10-30 09:34:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772172",
    ///             "is_trial_period": "false"
    ///         },
    ///         {
    ///             "quantity": "1",
    ///             "product_id": "ios_monthly",
    ///             "transaction_id": "1000000129874737",
    ///             "original_transaction_id": "1000000129871188",
    ///             "purchase_date": "2014-10-30 17:56:36 Etc/GMT",
    ///             "purchase_date_ms": "1414691796434",
    ///             "purchase_date_pst": "2014-10-30 10:56:36 America/Los_Angeles",
    ///             "original_purchase_date": "2014-10-30 16:32:06 Etc/GMT",
    ///             "original_purchase_date_ms": "1414686726000",
    ///             "original_purchase_date_pst": "2014-10-30 09:32:06 America/Los_Angeles",
    ///             "expires_date": "2014-10-30 16:39:03 Etc/GMT",
    ///             "expires_date_ms": "1414687143000",
    ///             "expires_date_pst": "2014-10-30 09:39:03 America/Los_Angeles",
    ///             "web_order_line_item_id": "1000000028772212",
    ///             "is_trial_period": "false"
    ///         }
    ///     ],
    ///     "latest_receipt" : "Base64"
    /// }
    /// </example>
    public class VerifyReceiptResponse
    {
        /// <summary>
        /// the status from request.
        /// </summary>
        [JsonProperty("status")]
        public AppleReceiptStatusType ReceiptStatus { get; set; }

        /// <summary>
        /// environment where receipt was genereted;
        /// Ex.: "Sandbox"
        /// </summary>
        [JsonProperty("environment")]
        public string Environment { get; set; }

        /// <summary>
        /// The representation of the receipt
        /// </summary>
        [JsonProperty("receipt")]
        public AppReceipt Receipt { get; set; }

        /// <summary>
        /// The representation of the receipt for the most recent renewal
        /// </summary>
        [JsonProperty("latest_receipt_info")]
        public InAppPurchaseReceipt[] LatestReceipts { get; set; }

        /// <summary>
        /// The base-64 encoded transaction receipt for the most recent renewal.
        /// </summary>
        [JsonProperty("latest_receipt")]
        public string LatestReceipt { get; set; }
    }
}