﻿namespace ProtectorUS.Integrations.Apple
{
    using Models;

    public interface IReceiptApi
    {
        VerifyReceiptResponse Verify(string receiptData);
    }
}