﻿namespace ProtectorUS.Integrations.Apple
{
    public class AppleApi : IAppleApi
    {
        public AppleApi()
        {
            this.Receipt = new ReceiptApi();
        }

        public IReceiptApi Receipt { get; set; }
    }
}
