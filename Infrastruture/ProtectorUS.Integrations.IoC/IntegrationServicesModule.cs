﻿using Autofac;

namespace ProtectorUS.Integrations.IoC
{
   // using Apple;
    using Stripe;
    /// <summary>
    /// Group registration of external services.
    /// </summary>
    public class IntegrationServicesModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <example>
        /// Example to register a service that needs configuration by constructor
        ///     builder.Register(svc => new ExternalService(new ExternalServiceConfiguration(
        ///            baseUrl: "http://externalservice/1.0",
        ///            apiKey: "dd6671cf7faae5be48a25f493ce9a3bf"
        ///     ))).As<ILastFmApi>();
        /// </example>
        /// <example>
        /// <param name="builder">The builder through which components can be
        ///             registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            // apple api ...
           // builder.RegisterType<AppleApi>().AsImplementedInterfaces();

            // Stripe api ...
            builder.RegisterType<StripeApi>().AsImplementedInterfaces();
        }
    }
}
