﻿using System;

namespace ProtectorUS.Diagnostics.Logging
{
    public interface ILogger
    {
        string UserName { get; }
        /// <summary>
        /// Initialize a log creates a user identified logger.
        /// </summary>
        /// <param name="user">logged user name</param>
        void Initialize(Type calledType, string userName);
       
        /// <summary>
        /// Writes a log entry when entering the method.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        void EnterMethod(string methodName);

        /// <summary>
        /// Writes a log entry when leaving the method.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        void LeaveMethod(string methodName);

        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        void LogException(Exception exception);

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogError(string message);

        /// <summary>
        /// Logs the warning message.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogWarningMessage(string message);

        /// <summary>
        /// Logs the info message.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogInfoMessage(string message);


        /// <summary>
        /// Method to log the start of async log data with a transaction description 
        /// and the entire data log register
        /// </summary>
        /// <param name="message">The message.</param>
        void LogData(object data);

        void LogDataWithoutUserIdentified(object data);

        /// <summary>
        /// Method to log the finish of async log data with a transaction description 
        /// </summary>
        /// <param name="data"></param>
        void FinishLogData(string transactionDescription);

    }
}
