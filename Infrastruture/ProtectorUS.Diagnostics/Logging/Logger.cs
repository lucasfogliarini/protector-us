﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Threading;

namespace ProtectorUS.Diagnostics.Logging
{
    public class Logger : ILogger
    {
        ILog Log = LogManager.GetLogger("application");
        public string UserName { get; private set; }

        public bool IsUserIdentified
        {
            get
            {
                if (string.IsNullOrWhiteSpace(MDC.Get("user")))
                    MDC.Set("user", Thread.CurrentPrincipal.Identity.Name);
                return !string.IsNullOrWhiteSpace(MDC.Get("user"));
            }
        }

        public Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
            GlobalContext.Properties["host"] = Environment.MachineName;
        }
        public void Initialize(Type calledType, string userName)
        {
            UserName = string.IsNullOrWhiteSpace(Thread.CurrentPrincipal.Identity.Name) ? "Unauthenticated" : userName;
            MDC.Set("user", Thread.CurrentPrincipal.Identity.Name);
            Log = LogManager.GetLogger(calledType);
        }
        public void EnterMethod(string methodName)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Info(string.Format(CultureInfo.InvariantCulture, "Entering Method {0}", methodName));

        }
        public void LeaveMethod(string methodName)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Info(string.Format(CultureInfo.InvariantCulture, "Leaving Method {0}", methodName));
        }
        public void LogException(Exception exception)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Error(string.Format(CultureInfo.InvariantCulture, "{0}", exception.Message), exception);
        }
        public void LogError(string message)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Error(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }
        public void LogWarningMessage(string message)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Warn(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }
        public void LogInfoMessage(string message)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
                Log.Info(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }
        public void LogData(object data)
        {
            if (Log.IsInfoEnabled && IsUserIdentified)
            {
                if (data != null)
                {
                    LogInfo logInfo = data as LogInfo;
                    if (logInfo.Logs.Count > 0)
                    {
                        string jsonData = JsonConvert.SerializeObject(logInfo.Logs, Formatting.Indented,
                            new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                        MDC.Set("additionalInfo", jsonData);
                    }
                    Log.Info(logInfo.Title);

                    //Clear additionalInfo Key at MDC after log data
                    MDC.Set("additionalInfo", "");
                }
            }
        }


        public void LogDataWithoutUserIdentified(object data)
        {
                if (data != null)
                {
                    LogInfo logInfo = data as LogInfo;
                    if (logInfo.Logs.Count > 0)
                    {
                        string jsonData = JsonConvert.SerializeObject(logInfo.Logs, Formatting.Indented,
                            new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                        MDC.Set("additionalInfo", jsonData);
                    }
                    Log.Info(logInfo.Title);

                    //Clear additionalInfo Key at MDC after log data
                    MDC.Set("additionalInfo", "");
                }         
        }

        public void FinishLogData(string transactionDescription)
        {
            LogInfoMessage(transactionDescription);
        }

    }
}
