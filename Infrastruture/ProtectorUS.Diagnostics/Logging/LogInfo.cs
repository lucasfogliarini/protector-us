﻿using System.Collections.Generic;

namespace ProtectorUS.Diagnostics.Logging
{
    using ProtectorUS.Model;
    public class LogInfo
    {
        public string Title;
        public string TransactionIdentify;
        public List<AuditLog> Logs;

        public LogInfo(string title, string transaction, List<AuditLog> logs)
        {
            this.Title = title;
            this.TransactionIdentify = transaction;
            this.Logs = logs;
        }
    }
}
