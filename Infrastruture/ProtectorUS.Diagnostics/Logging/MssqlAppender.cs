﻿using log4net.Appender;
using System.Configuration;

namespace ProtectorUS.Diagnostics.Logging
{
    public class MssqlAppender : AdoNetAppender
    {
        private const string connectionStringName = "logProtectorUS";

        public new string ConnectionString
        {
            get { return base.ConnectionString; }
            set
            {
                base.ConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            }
        }
    }
}
