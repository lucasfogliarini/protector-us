﻿using Autofac;

namespace ProtectorUS.Diagnostics.IoC
{
    using Logging;
    /// <summary>
    /// Diagnostics module registration
    /// </summary>
    public class DiagnosticsModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        ///             registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            // log service.
            builder.RegisterType<Logger>().AsImplementedInterfaces();
        }
    }
}
