﻿using System.Security.Cryptography;
using System;


namespace ProtectorUS.Security.Cryptography
{
    using Extensions;
   
    /// <summary>
    /// Helper for hash computation.
    /// </summary>
    public class Hasher
    {
        /// <summary>
        /// Computes a hash string based on a string key.
        /// </summary>
        /// <param name="key">the key to compute the hash.</param>
        /// <param name="algorithmName">the algorithm's name used to compute the hash.</param>
        /// <returns>a safe base 64 hash.</returns>
        public static string Compute(string key, string algorithmName = "SHA256")
        {
            var buffer = System.Text.Encoding.UTF8.GetBytes(key);

            using (var algorithm = (HashAlgorithm)CryptoConfig.CreateFromName(algorithmName))
                return Convert.ToBase64String(algorithm.ComputeHash(buffer))
                    .Clear("/", "+", "=");
        }

        /// <summary>
        /// Get a SHA256Hash from a string valuee
        /// </summary>
        /// <param name="input"></param>
        /// <returns>a safe base 64 hash.</returns>
        public static string GetSHA256Hash(string input)
        {
            var byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            using (var hashAlgorithm = new SHA256CryptoServiceProvider())
                return Convert.ToBase64String(hashAlgorithm.ComputeHash(byteValue));
        }

        public static string NewSHA256Hash()
        {
            string newGuid = Guid.NewGuid().ToString();
            return GetSHA256Hash(newGuid);
        }
    }
}
