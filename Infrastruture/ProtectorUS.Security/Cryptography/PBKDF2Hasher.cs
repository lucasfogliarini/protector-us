﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ProtectorUS.Security.Cryptography
{
    public static class PBKDF2Hasher
    {
        const string key = "PBKDF2_H@sh&r_k&y";
        static readonly byte[] salt = Encoding.UTF8.GetBytes(key);
        const int defaultLength = 70;
        public static string GenerateHash(string textPlain, int length = defaultLength)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(textPlain, salt))
            {
                var hash = pbkdf2.GetBytes(length);
                return Convert.ToBase64String(hash).Replace("+","");
            }
        }
        public static bool Match(string hash, string textPlain, int length = defaultLength)
        {
            return hash?.Length > 0 ? hash.Replace("+", "") == GenerateHash(textPlain) : false;
        }
    }
}
