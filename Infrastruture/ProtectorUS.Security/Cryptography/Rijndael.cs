﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ProtectorUS.Security.Cryptography
{
    public static class Rijndael
    {
        /// <summary>
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </summary>
        private const string PassPhrase = "R0ckH4ck3rs!@";
        /// <summary>
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </summary>
        private const string SaltValue = "s@1tV4lu3";
        /// <summary>
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </summary>
        private const int PasswordIterations = 2;
        /// <summary>
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be 
        /// exactly 16 ASCII characters long.
        /// </summary>
        private const string InitVector = "@1B2c3D4e5F6g7H8";
        /// <summary>
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        /// Longer keys are more secure than shorter keys.
        /// </summary>
        private const int KeySize = 256;

        /// <summary>
        /// Encrypts specified plaintext using Rijndael symmetric key algorithm
        /// and returns a base64-encoded result.
        /// </summary>
        /// <param name="plainText">
        /// Plaintext value to be encrypted.
        /// </param>
        /// <returns>
        /// Encrypted value formatted as a base64-encoded string.
        /// </returns>
        public static string Encrypt(string plainText)
        {
            // Convert strings into byte arrays.
            // Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            // encoding.
            var initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            var saltValueBytes = Encoding.ASCII.GetBytes(SaltValue);
            var passPhraseBytes = Encoding.ASCII.GetBytes(PassPhrase);

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes;
            // First, we must create a password, from which the key will be derived.
            // This password will be generated from the specified passphrase and 
            // salt value. The password will be created using the specified hash 
            // algorithm. Password creation can be done in several iterations.
            using (var password = new Rfc2898DeriveBytes(passPhraseBytes, saltValueBytes, PasswordIterations))
            {
                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                keyBytes = password.GetBytes(KeySize / 8);

            }

            ICryptoTransform encryptor;
            // Create uninitialized Rijndael encryption object.
            using (var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC })
            {
                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.

                // Generate encryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            }

            // Define memory stream which will be used to hold encrypted data.
            using (var memoryStream = new MemoryStream())
            {
                // Define cryptographic stream (always use Write mode for encryption).
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    // Start encrypting.
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                    // Finish encrypting.
                    cryptoStream.FlushFinalBlock();

                    // Convert our encrypted data from a memory stream into a byte array.
                    var cipherTextBytes = memoryStream.ToArray();

                    // Convert encrypted data into a base64-encoded string.
                    var encryptText = Convert.ToBase64String(cipherTextBytes);

                    // Return encrypted string.
                    return encryptText;
                }
            }
        }

        /// <summary>
        /// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        /// </summary>
        /// <param name="encryptText">
        /// Base64-formatted ciphertext value.
        /// </param>
        /// <returns>
        /// Decrypted string value.
        /// </returns>
        /// <remarks>
        /// Most of the logic in this function is similar to the Encrypt
        /// logic. In order for decryption to work, all parameters of this function
        /// - except cipherText value - must match the corresponding parameters of
        /// the Encrypt function which was called to generate the
        /// ciphertext.
        /// </remarks>
        public static string Decrypt(string encryptText)
        {
            // Convert strings defining encryption key characteristics into byte
            // arrays. Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8
            // encoding.
            var initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            var saltValueBytes = Encoding.ASCII.GetBytes(SaltValue);
            var passPhraseBytes = Encoding.ASCII.GetBytes(PassPhrase);

            // Convert our ciphertext into a byte array.
            var cipherTextBytes = Convert.FromBase64String(encryptText);

            byte[] keyBytes;
            // First, we must create a password, from which the key will be 
            // derived. This password will be generated from the specified 
            // passphrase and salt value. The password will be created using
            // the specified hash algorithm. Password creation can be done in
            // several iterations.
            using (var password = new Rfc2898DeriveBytes(passPhraseBytes, saltValueBytes, PasswordIterations))
            {
                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                keyBytes = password.GetBytes(KeySize / 8);
            }

            ICryptoTransform decryptor;
            // Create uninitialized Rijndael encryption object.
            using (var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC })
            {
                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.

                // Generate decryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                decryptor = symmetricKey.CreateDecryptor
                (
                    keyBytes,
                    initVectorBytes
                );
            }

            // Define memory stream which will be used to hold encrypted data.
            using (var memoryStream = new MemoryStream(cipherTextBytes))
            {
                // Define cryptographic stream (always use Read mode for encryption).
                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    // Since at this point we don't know what the size of decrypted data
                    // will be, allocate the buffer long enough to hold ciphertext;
                    // plaintext is never longer than ciphertext.
                    var plainTextBytes = new byte[cipherTextBytes.Length];

                    // Start decrypting.
                    var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                    // Convert decrypted data into a string. 
                    // Let us assume that the original plaintext string was UTF8-encoded.
                    var plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);

                    // Return decrypted string.   
                    return plainText;
                }
            }
        }
    }
}
