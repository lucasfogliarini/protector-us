﻿using System.Collections.Generic;
using System.Linq;

namespace ProtectorUS.Security.Cryptography
{
    /// <summary>
    /// Represents an unique key.
    /// </summary>
    public class UniqueKey
    {
        private const int DefaultLength = 100;
        private const string AllowedChars = "abcdefghijklmnopqrstuvxyz1234567890";


        /// <summary>
        /// Creates a new unique key.
        /// </summary>
        /// <param name="length">the key length.</param>
        /// <param name="allowed">allowed characters to compund the key.</param>
        /// <returns>an unique key.</returns>
        public static string New(int length = 100, string allowed = "")
        {
            if (length < 0)
                length = DefaultLength;

            if (string.IsNullOrWhiteSpace(allowed))
                allowed = AllowedChars;



            const int byteSize = 0x100;
            var charset = new HashSet<char>(allowed).ToArray();

            using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider())
            {
                var builder = new System.Text.StringBuilder();
                var buffer = new byte[128];

                while (builder.Length < length)
                {
                    rng.GetBytes(buffer);

                    for (int i = 0; (i < buffer.Length && builder.Length < length); i++)
                    {
                        var start = byteSize - (byteSize % charset.Length);

                        if (start > buffer[i])
                            builder.Append(charset[(buffer[i] % charset.Length)]);
                    }
                }

                return builder.ToString();
            }
        }
    }
}
