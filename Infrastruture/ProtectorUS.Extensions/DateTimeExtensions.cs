﻿using System;
using System.Collections.Generic;

namespace ProtectorUS.Extensions
{
    /// <summary>
    /// <see cref="DateTime"/> extension methods.
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// Verify if a date are between others two dates.
        /// </summary>
        /// <param name="date">the date to be verify.</param>
        /// <param name="start">the start date.</param>
        /// <param name="end">the end date.</param>
        /// <returns></returns>
        public static bool Between(this DateTime date, DateTime start, DateTime end)
        {
            return date >= start && date <= end;
        }

        /// <summary>
        /// Assure to datetime is on UTC time zone. Uses Datetime.Kind to shift time
        /// </summary>
        /// <param name="self">datetime</param>s
        /// <returns>datetime on UTC time zone</returns>
        public static DateTime? EnsureAsUtc(this DateTime? self)
        {
            if (self == null)
            {
                return null;
            }

            return EnsureAsUtc(self.Value);
        }

        /// <summary>
        /// Assure to datetime is on UTC time zone. Uses Datetime.Kind to shift time
        /// </summary>
        /// <param name="self">datetime</param>s
        /// <returns>datetime on UTC time zone</returns>
        public static DateTime EnsureAsUtc(this DateTime self)
        {
            switch (self.Kind)
            {
                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(self, DateTimeKind.Utc);

                case DateTimeKind.Local:
                    return self.ToUniversalTime();
            }
            return self;
        }

        public static DateTime ClearSeconds(this DateTime self)
        {
            return self.AddSeconds(-self.Second);
        }

        public static DateTime ClearMilliSeconds(this DateTime self)
        {
            return self.AddMilliseconds(-self.Millisecond);
        }

        /// <summary>
        /// Converts a date time to Brazilia time zone (GMT-3).
        /// </summary>
        /// <param name="self">The date time.</param>
        /// <returns>The date time in Brazilia time zone.</returns>
        public static DateTime ToBraziliaTimeZone(this DateTime self)
        {
            self = self.EnsureAsUtc();

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");

            return TimeZoneInfo.ConvertTimeFromUtc(self, timeZoneInfo);
        }


        /// <summary>
        /// Converts a date time to US time zone.
        /// </summary>
        /// <param name="self">The date time.</param>
        /// <param name="timeZone">The time zone by param.</param>
        /// <returns>The date time in a US time zone.</returns>
        public static DateTime ToUSTimeZone(this DateTime self, string timeZone)
        {
            self.EnsureAsUtc();
            var zones = new List<TimeZoneInfo> {
                TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time"),
                TimeZoneInfo.FindSystemTimeZoneById("Alaskan Standard Time"),
            };

            var timeZoneInfo = zones.Find(x=> x.Id == timeZone);

            return TimeZoneInfo.ConvertTimeFromUtc(self, timeZoneInfo);
        }
    }
}
