﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace System
{
    /// <summary>
    /// <see cref="string"/> extension methods.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Remove a serie of strings from a string.
        /// </summary>
        /// <param name="value">the string to be cleared.</param>
        /// <param name="trash">the strings to remove from <paramref name="value"/>.</param>
        /// <returns><paramref name="value"/> without any of <paramref name="trash"/>.</returns>
        public static string Clear(this string value, params string[] trash)
        {
            return trash.Aggregate(
                    value, (current, t) =>
                        current.Replace(t, string.Empty)
                   );
        }

        /// <summary>
        /// Injects arguments into a tokenized string.
        /// </summary>
        /// <param name="value">the tokenized string.</param>
        /// <param name="args">the arguments.</param>
        /// <returns>the formated string with the arguments values injected into it.</returns>
        public static string Inject(this string value, params object[] args)
        {
            return string.Format(value, args);
        }

        /// <summary>
        /// Remove accents from a string
        /// </summary>
        /// <param name="value">original string</param>
        /// <returns>new string with no accents and other especial caracters</returns>
        /// <example>
        ///     á, â, à => a
        ///     ç => c
        /// </example>
        public static string RemoveDiacritics(this string value)
        {
            var normalizedString = value.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        /// <summary>
        /// Simply text
        /// </summary>
        /// <param name="value">string to simply</param>
        /// <returns>new string with simply caracters</returns>
        ///<example>
        ///& => e
        ///not letter or number replace by '-'
        ///</example>
        public static string Simplify(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            StringBuilder result = new StringBuilder();
            value = value.ToLower().RemoveDiacritics().Replace(" ",string.Empty);
            foreach (char character in value)
            {
                if ((character >= 'a' && character <= 'z') || (character >= '0' && character <= '9') || character == '_' || character == '-')
                {
                    result.Append(character);
                }
            }

            return result.ToString();
        }

        public static string Left(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value) || maxLength <= 0)
                return string.Empty;

            if (maxLength < value.Length)
                return value.Substring(0, maxLength);

            return value;
        }

        /// <summary>
        /// Set a string empty to null value
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Null if string is empty otherwise the string value</returns>
        public static string ToNullIfEmpty(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }

        public static string[] Split(this string value, params string[] separator)
        {
            return value.Split(separator, StringSplitOptions.None);
        }

        public static string Match(this string value, string initial, string final)
        {
            int index = value.IndexOf(initial) + initial.Length;
            string initialSplit = value.Substring(index);
            index = initialSplit.IndexOf(final);
            return initialSplit.Substring(0, index);
        }

        public static DateTime ToDate(this string date)
        {
            return DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Checks if is a valid email
        /// </summary>
        /// <param name="s">Object</param>
        /// <returns>True if valid</returns>
        public static bool IsValidEmailAddress(this String s)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(s);
        }

        /// <summary>
        /// Convert string to stream
        /// </summary>
        /// <param name="str">Object</param>
        /// <returns>Stream Object</returns>
        public static Stream ToStream(this string str)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(str);
            return new MemoryStream(byteArray);
        }

        /// <summary>
        /// Change first character to lower
        /// </summary>
        /// <param name="input">Object</param>
        /// <returns>Text with first character lower case</returns>
        public static string ToLowerFirstChar(this string input)
        {
            string newString = input;
            if (!String.IsNullOrEmpty(newString) && Char.IsUpper(newString[0]))
                newString = Char.ToLower(newString[0]) + newString.Substring(1);
            return newString;
        }

        public static string ToBase64Encode(this string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static bool IsValidFormat(this string fileName, string formats)
        {
            var extension = fileName.Split('.').Last().ToLower();
            return formats.Split(',').Contains(extension);
        }

        public static string GetFileFormat(this string fileFullName)
        {
            return fileFullName.Split('.').Last().ToLower();
          
        }
        public static bool Contain(this string input, string value)
        {
            return input == null ? false : input.ToLower().Contains(value.ToLower());
        }
        public static string Take(this string input, int maxLength)
        {
            int length = input.Length < maxLength ? input.Length : maxLength;
            return input.Substring(0, length);
        }
    }
}
