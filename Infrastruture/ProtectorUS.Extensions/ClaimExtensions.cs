﻿using System.Linq;

namespace System.Security.Claims
{
    public static class ClaimIdentityExtensions
    {
        /// <summary>
        /// Add a Claim, if it not exists.
        /// </summary>
        public static void IncludeClaim(this ClaimsIdentity claimsIdentity, string type, string value)
        {
            if (claimsIdentity.HasClaim(type, value))
            {
                return;
            }
            claimsIdentity.AddClaim(new Claim(type, value));
        }

        public static void RemoveAllClaims(this ClaimsIdentity claimsIdentity, Func<Claim, bool> match)
        {
            var permissionClaims = claimsIdentity.Claims.Where(match).ToArray();
            foreach (var claim in permissionClaims)
            {
                claimsIdentity.RemoveClaim(claim);
            }
        }
        public static string GetClaimValue(this ClaimsIdentity claimsIdentity, string type)
        {
            return claimsIdentity.FindFirst(type)?.Value;
        }
    }
}
