﻿using System.Text;

namespace ProtectorUS.Extensions
{
    /// <summary>
    /// <see cref="StringBuilder"/> extension methods.
    /// </summary>
    public static class StringBuilderExtensions
    {
        /// <summary>
        /// Append line with a format
        /// </summary>
        /// <param name="self"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public static void AppendLine(this StringBuilder self, string format, params object[] args)
        {
            self.AppendFormat(format, args);
            self.AppendLine();
        }
    }
}
