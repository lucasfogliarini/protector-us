﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtectorUS.Extensions
{
    public static class ApiControllerExtension
    {



        public static async Task<string> SaveFile(this ApiController con, string path, string invalidMessage, string baseUrl, string validFormats)
        {
            if (!con.Request.Content.IsMimeMultipartContent("form-data"))
                throw new BadRequestException();

            var streamProvider = await con.Request.Content.ReadAsMultipartAsync();
            if (streamProvider.Contents.Count != 1)
                throw new BadRequestException();

            var sendFile = streamProvider.Contents[0];
            var fileName = sendFile.Headers.ContentDisposition.FileName.Trim('\"');
            var stream = await sendFile.ReadAsByteArrayAsync();

            if (!fileName.IsValidFormat(validFormats))
            {
                var validationManager = new ValidationResultsManager();
                validationManager.AddValidationResultNotValid(invalidMessage);
                validationManager.ThrowBusinessValidationError();
            }

            var name = Guid.NewGuid().ToString("n") + Path.GetExtension(fileName);
            File.WriteAllBytes(path + name, stream);

            return baseUrl + name;
        }

        public static ForbiddenResult Forbidden(this ApiController con, HttpRequestMessage request)
        {
            return new ForbiddenResult(request);
        }

        public static NoContentResult NoContent(this ApiController con, HttpRequestMessage request)
        {
            return new NoContentResult(request);
        }

        public static TextResult TextResult(this ApiController con, string value, HttpRequestMessage request)
        {
            return new TextResult(value, request);
        }

        public static FileActionResult FileActionResult(this ApiController con, string fileHash, string filePath)
        {
            return new FileActionResult(fileHash, filePath);
        }
    }
}
