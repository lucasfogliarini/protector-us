﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class DictionaryExtensions
    {
        public static TValue Get<TKey, TValue>(this IDictionary<TKey,TValue> dictionary, TKey key)
        {
            TValue value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, params TValue[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                dictionary[dictionary.Keys.ElementAt(i)] = values[i];
            }
        }

        public static void Upsert<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = value;
            }
            else
            {
                dictionary.Add(key, value);
            }
        }
    }
}
