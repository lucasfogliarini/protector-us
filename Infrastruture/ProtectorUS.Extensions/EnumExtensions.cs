﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace System
{
    /// <summary>
    /// Enum extensions class.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Gets the enum member description (<see cref="DescriptionAttribute"/>).
        /// </summary>
        /// <param name="member">The enum member.</param>
        /// <returns>
        ///     A string representing the enum's member description.
        ///     Or string.Empty if the member lacks of <see cref="DescriptionAttribute"/> definition.
        /// </returns>
        public static string Description(this Enum member)
        {
            var field = member.GetType().GetField(member.ToString());

            var attribute =
                Attribute.GetCustomAttribute(
                    field, typeof(DescriptionAttribute)) as DescriptionAttribute;

            return attribute == null ? string.Empty : attribute.Description;
        }

        /// <summary>
        /// Gets an enum member based on yours description attribute.
        /// <see cref="DescriptionAttribute"/>.
        /// </summary>
        /// <typeparam name="T">The enum type.</typeparam>
        /// <param name="description">The description.</param>
        /// <returns>The enumerator member.</returns>
        public static T MemberOf<T>(string description)
        {
            var type = typeof(T);

            if (!type.IsEnum)
                throw new ArgumentException(
                    string.Format("{0} is not an Enum.", type.Name));


            var fields = type.GetFields();

            var field = fields
                            .SelectMany(
                                fld =>
                                    fld.GetCustomAttributes(typeof(DescriptionAttribute), false),
                                (fld, attr) =>
                                    new { Field = fld, Attr = attr }
                            ).SingleOrDefault(
                                attr =>
                                    ((DescriptionAttribute)attr.Attr)
                                        .Description.ToLower() == description.ToLower()
                            );

            return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        }

        /// <summary>
        /// Gets an enum member based on yours description attribute.
        /// <see cref="DescriptionAttribute"/>.
        /// </summary>
        public static TEnum GetValue<TEnum>(string description) where TEnum : struct
        {
            var type = typeof(TEnum);
            if (type.IsEnum && !string.IsNullOrEmpty(description))
            {
                var fields = type.GetFields();
                foreach (var field in fields)
                {
                    var descriptionAttr = field.GetCustomAttribute<DescriptionAttribute>();
                    if (descriptionAttr != null && descriptionAttr.Description.ToLower().Contains(description.ToLower()))
                        return (TEnum)field.GetRawConstantValue();
                }
            }

            return default(TEnum);
        }

        /// <summary>
        /// Gets all enum members descriptions.
        /// </summary>
        /// <typeparam name="T">the enum type.</typeparam>
        /// <returns>an array with the enumeration's member descriptions.</returns>
        public static string[] GetDescriptions<T>()
        {
            var type = typeof(T);

            if (!type.IsEnum)
                throw new ArgumentException(
                    string.Format("{0} is not an Enum.", type.Name));


            var fields = type.GetFields();


            var descriptions =
                    fields.SelectMany(
                        fld =>
                            fld.GetCustomAttributes(typeof(DescriptionAttribute), false),
                       (fld, attr) =>
                            new { Field = fld, Attr = attr }
                ).Select(attr => ((DescriptionAttribute)attr.Attr).Description).ToArray();


            return descriptions;
        }

        /// <summary>
        /// Gets the enumerator values.
        /// </summary>
        /// <typeparam name="T">the enumerator type.</typeparam>
        /// <returns>a collection of values.</returns>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}
