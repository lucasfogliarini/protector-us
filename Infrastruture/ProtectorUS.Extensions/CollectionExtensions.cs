﻿using System.Linq;
namespace System.Collections.Generic
{
    /// <summary>
    /// Extension methods for Collections.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Checks whatever given collection object is null or has no item.
        /// </summary>
        public static bool IsNullOrEmpty<T>(this ICollection<T> source)
        {
            return source == null || source.Count <= 0;
        }
        public static void AddIfNot<T,TNew>(this ICollection<T> collection, TNew newitem, Func<TNew, T,bool> predicate) where TNew : T
        {
            collection.AddRangeIfNot(new[] { newitem }, predicate);
        }
        public static void AddRangeIfNot<T, TNew>(this ICollection<T> collection, IEnumerable<TNew> newitems, Func<TNew, T, bool> predicate) where TNew : T
        {
            foreach (TNew newItem in newitems)
            {
                bool contains = collection.Any(i => predicate(newItem, i));
                if (!contains)
                {
                    collection.Add(newItem);
                }
            }
        }
        public static int RemoveAll<T>(this IList<T> collection, Predicate<T> match)
        {
            int count = 0;
            for (int i = collection.Count - 1; i >= 0; i--)
            {
                if (match(collection[i]))
                {
                    ++count;
                    collection.RemoveAt(i);
                }
            }

            return count;
        }
    }
}
