﻿using System.IO;

namespace System
{
    public static class AppDomainExtensions
    {
        public static string GetAppData(this AppDomain appDomain, string file = null)
        {
            return Path.Combine(appDomain.BaseDirectory, "App_Data", file);
        }
        public static string GetPath(this AppDomain appDomain, params string[] paths)
        {
            return Path.GetFullPath($"{appDomain.BaseDirectory}{Path.Combine(paths)}");
        }
    }
}
