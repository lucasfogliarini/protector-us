﻿using System.Linq;

namespace System.Collections.Generic
{
    /// <summary>
    /// Collections helper class. 
    /// Group all <see cref="IEnumerable{T}"/>, <see cref="ICollection{T}"/> and <see cref="IList{T}"/> 
    /// class extension methods.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Selects a random collection's item.
        /// </summary>
        /// <typeparam name="T">the type of source.</typeparam>
        /// <param name="source">the source of items.</param>
        /// <returns>a random item of source.</returns>
        public static T Anyone<T>(this IEnumerable<T> source)
        {
            if (source == null)
                return default(T);

            return source.ElementAtOrDefault(new Random().Next(source.Count()));
        }
        /// <summary>
        /// Selects a random collection's item that obey the given predicate.
        /// </summary>
        /// <typeparam name="T">the source item's type.</typeparam>
        /// <param name="source">the source of items.</param>
        /// <param name="predicate">a predicate to filter the source.</param>
        /// <returns>a random item of source.</returns>
        public static T Anyone<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var filtered = source.Where(predicate);

            return filtered.Anyone();
        }
        /// <summary>
        /// Computes the sum of the sequence of <see cref="TimeSpan"/>.
        /// </summary>
        /// <typeparam name="TSource">source type.</typeparam>
        /// <param name="source">the elements source.</param>
        /// <param name="selector">a transform function to apply on each element.</param>
        /// <returns>the sum of each TimeSpan on source.</returns>
        public static TimeSpan Sum<TSource>(this IEnumerable<TSource> source,
            Func<TSource, TimeSpan> selector)
        {
            return source.Select(selector).Aggregate(TimeSpan.Zero, (t1, t2) => t1 + t2);
        }
        /// <summary>
        /// Shuffles the set of <see cref="T"/>.
        /// </summary>
        /// <typeparam name="T">source type.</typeparam>
        /// <param name="source">source.</param>
        /// <returns>the shuffled source set.</returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(b => Guid.NewGuid());
        }
        private static readonly Random RandomSeed = new Random();
        public static IOrderedEnumerable<T> ThenShuffle<T>(this IOrderedEnumerable<T> self)
        {
            return self.ThenBy(x => RandomSeed.Next());
        }

        public static string Join<T>(this IEnumerable<T> source, string separator)
        {
            if (source == null)
                return string.Empty;

            return string.Join(separator, source);
        }

        public static List<T> ToNonNullList<T>(this IEnumerable<T> obj)
        {
            return obj == null ? new List<T>() : obj.ToList();
        }

        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if(collection != null)
                foreach (T item in collection)
                    action(item);
        }
        public static IEnumerable<TLeft> Outer<TLeft, TRight>(this IEnumerable<TLeft> collection, IEnumerable<TRight> rightItems, Func<TLeft, TRight, bool> predicate) where TRight : TLeft
        {
            foreach (TLeft leftItem in collection)
            {
                bool contains = rightItems.Any(right => predicate(leftItem, right));
                if (!contains)
                {
                    yield return leftItem;
                }
            }
        }

        public static bool HasItem(this IEnumerable collection)
        {
            if (collection == null)
                return false;
            return collection.GetEnumerator().MoveNext();
        }

        public static void AddRange<T>(this HashSet<T> hashSet, IEnumerable<T> items, bool throwIfExists = false)
        {
            items.ForEach(i => hashSet.Add(i, true));
        }
        public static bool Add<T>(this HashSet<T> hashSet, T item, bool throwIfExists = false)
        {
            bool added = hashSet.Add(item);
            if (throwIfExists && !added)
                throw new ArgumentException("Item with same value has already been added.");
            return added;
        }
    }
}
