﻿namespace System.IO
{
    public static class StreamExtension
    {
        public static string ToBase64String(this MemoryStream memoryStream)
        {
            return Convert.ToBase64String(memoryStream.ToArray());
        }
    }
}
