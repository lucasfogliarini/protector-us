﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProtectorUS.Data;
using ProtectorUS.Model;
using System;
using System.Linq;

namespace ProtectorUS.Test.Connectivity
{
    [TestClass]
    public class TransientFailure
    {
        [TestMethod, TestCategory("Connection Resiliency")]
        [Ignore]
        public void CanHitSqlAzureDbWithTransientFailure()
        {
            try
            {
                using (var context = new ProtectorUSDatabase())
                {
                    //quering over any DbSet only to force throwing fake exception from interceptor matching the word TransientFailure at DbCommand command text.
                    var stateSet = context.Set<State>();
                    var totalStates = stateSet.SqlQuery("SELECT TransientFailure FROM dbo.State where Name=@p0", "TransientFailure").Count();
                }
            }
            catch (System.Data.Entity.Infrastructure.RetryLimitExceededException ae)
            {
                var errorMessageRetry = "Maximum number of retries (2) exceeded while executing database operations with 'SqlAzureExecutionStrategy'. See inner exception for the most recent failure.";
                Assert.AreEqual(errorMessageRetry, ae.Message);
            }
            catch (System.Data.SqlClient.SqlException ae)
            {
                var errorMessageConnection = "A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: SQL Network Interfaces, error: 26 - Error Locating Server/Instance Specified)";
                Assert.AreEqual(errorMessageConnection, ae.Message);
            }
            catch (Exception e)
            {
                Assert.Fail(
                     string.Format("Unexpected exception of type {0} caught: {1}",
                                    e.GetType(), e.Message)
                );
            }
        }
    }
}
