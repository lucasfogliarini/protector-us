﻿Feature: Activation

Scenario: Activating with a new Insured
	Given I have entered a email newinsured@insured.com
	When I try to get the insured passing the email
	Then the result should be a message code saying that the User was not found

Scenario: Activating with a existing Insured
	Given I have entered a email insured1@insured.com
	When I try to get the insured passing the email
	Then the result should be a Insured

Scenario: Activating with a existing Insured giving the password
	Given I have entered a email insured1@insured.com
	And I have entered a password insured
	When I try to authenticate passing the email and password
	Then the result should be a InsuredUser

Scenario Outline: Getting a RatingPlan
	Given I have entered a <productAlias>, <zipCode>
	When I try to get the RatingPlan
	Then the result should be a RatingPlan
	Examples: 
	| productAlias         | zipCode |
	| architects_engineers | 35611   |
	| architects_engineers | 35614   |

Scenario Outline: Getting a inexisting RatingPlan
	Given I have entered a <productAlias>, <zipCode>
	When I try to get the RatingPlan
	Then the result should be a message code saying that the Rating Plan was not found
	Examples: 
	| productAlias | zipCode |
	| engineers    | 35611   |
	| architects   | 35614   |
	| doctors      | 35613   |


Scenario Outline: Creating a Quotation
	Given a productAlias and zipCode 'architects_engineers', '35611' to get a RatingPlan HashCode
	Given the essencial part of the Quotation <email>, <socialTitle>, <firstName>, <lastName>, <insuredId>, <brokerageProductId>, <campaignId>
	When I try to create a Quotation
	Then the result should be a Quotation
	Examples: 
	| email                   | socialTitle | firstName               | lastName               | insuredId | brokerageProductId | campaignId |
	| proponent1@mail.com     | 1           | ProponentFirstName1     | ProponentLastName1     | -1        | 1                  | -1         |
	| insuredcreated@mail.com | 2           | InsuredCreatedFirstName | InsuredCreatedLastName | 22        | 1                  | -1         |

Scenario Outline: Updating Quotation details
	Given more details of the Quotation <quotationHash>, <phoneType>, <phone>, <homeRegionId>, <homeStreetAddressLine1>, <homeStreetAddressLine2>, <businessType>, <businessName>, <businessAddressLine1>, <businessAddressLine2>
	When I try to update a Quotation passing the data
	Then the result should be a Quotation
	Examples: 
	| quotationHash                        | phoneType | phone       | homeRegionId | homeStreetAddressLine1  | homeStreetAddressLine2  | businessType | businessName | businessAddressLine1  | businessAddressLine2  |
	| B2D5116B-2FB7-481D-8A6F-26EF632A36E1 | 1         | 51987654321 | 1            | homeStreet1AddressLine1 | homeStreet1AddressLine2 | 1            | business1    | business1AddressLine1 | business1AddressLine2 |
	| 96F9CBC1-FF84-45D0-8419-3E2EC98E3E93 | 1         | 51123456789 | 1            | homeStreet2AddressLine1 | homeStreet2AddressLine2 | 2            | business2    | business2AddressLine1 | business2AddressLine2 |

Scenario Outline: Activating
	Given a Order <quotationHash>, <totalInstallments>, <cardHoldName>, <addressLine1>, <addressLine2>, <zipCode>, <numberCreditCard>, <cvc>, <expirationYear>, <expirationMonth>
	When I try to activate
	Then the result should be a Order
	Examples: 
	| quotationHash                        | totalInstallments | cardHoldName      | addressLine1 | addressLine2 | zipCode | numberCreditCard | cvc | expirationYear | expirationMonth |
	| B2D5116B-2FB7-481D-8A6F-26EF632A36E1 | 1                 | Insured Activator | addressLine1 | addressLine2 | 35614   | 4012888888881881 | 989 | 2020           | 1               |