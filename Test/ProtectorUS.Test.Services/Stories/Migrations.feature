﻿Feature:_Migrations

Scenario: _Update and Seed
	When I try initialize the Database
	Then everything must be ok

Scenario: Check DevAzure migrations
	Given all migrations from dev database
	And all migrations from ProtectorUS Configuration
	When I check the db migrations in local migrations
	Then all migrations should be checked

@ignore
Scenario: Check Production migrations
	Given all migrations from poduction database
	And all migrations from ProtectorUS Configuration
	When I check the db migrations in local migrations
	Then all migrations should be checked
