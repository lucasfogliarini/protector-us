﻿Feature: LocalizationService

Scenario Outline: Getting a existing Region
	Given I have entered a <zipcode>
	When I try to get the region passing the zipcode
	Then the result should be a Region
	Examples: 
	| zipcode |
	| 35611   |
	| 35614   |
	| 35613   |

Scenario Outline: Getting a inexisting Region
	Given I have entered a <zipcode>
	When I try to get the region passing the zipcode
	Then the result should be a message code saying the was not found
	Examples: 
	| zipcode |
	| 11111   |
	| 33333   |

Scenario Outline: Validating a Region with errors
	Given I have entered a <zipcode>
	When I try to get the region passing the zipcode
	Then the result should be validation errors of the Region
	Examples:
	| zipcode  |
	|          |
	| 222      |
	| 90250020 |

Scenario Outline: Creating a Address
	Given I have entered <streetLine1>, <streetLine2>, <zipcode>
	When I try to create the address passing the data
	Then the result should be a Address
	Examples:
	| streetLine1     | streetLine2 | zipcode |
	| Sullivan St	  | 262         | 35611   |
	| Broadway		  | 663         | 35611   |
	| Bleecker St     | 194         | 35611   |
