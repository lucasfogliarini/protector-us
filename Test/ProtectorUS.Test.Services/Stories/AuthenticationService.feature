﻿Feature: AuthenticationService

Scenario Outline: Authentication with username or password empty
	Given a UserType, Email and Password <userType>, <email>, <password>
	When i try to authenticate the user passing the data
	Then the result should be a message code saying that the username and password can't be empty
	Examples: 
	| userType | email    | password |
	| Broker   |          | pass123  |
	| Insured  | login123 |          |

Scenario Outline: Authentication with incorrect password
	Given a UserType, Email and Password <userType>, <email>, <password>
	When i try to authenticate the user passing the data
	Then the result should be a message code saying that the password is incorrect
	Examples: 
	| userType    | email                   | password  |
	| ManagerUser | manager@manageruser.com | trying    |
	| Broker      | master@brokerage1.com   | missPass  |
	| Broker      | admin@brokerage2.com    | passWrong |

Scenario Outline: Authentication valid
	Given a UserType, Email and Password <userType>, <email>, <password>
	When i try to authenticate the user passing the data
	Then the return should be the User
	Examples: 
	| userType    | email                       | password    |
	| Broker      | master@brokerage1.com       | broker      |
	| ManagerUser | admin@manageruser.com       | manageruser |
	| ManagerUser | underwriter@manageruser.com | manageruser |


Scenario Outline: User should be at least one profile
	Given a UserType, Email and Password <userType>, <email>, <password>
	When i try to authenticate the user passing the data
	Then the user should be at least one profile
	Examples: 
	| userType    | email                     | password    |
	| Broker      | admin@brokerage3.com      | broker      |
	| Broker      | agent@brokerage2.com      | broker      |
	| ManagerUser | assistant@manageruser.com | manageruser |

Scenario Outline: User with valid permissions
	Given a user with UserId and permission <userId>, <permission>
	When i try to find the permissions passing the data
	Then the permission should be in returned permissions
	Examples: 
	| userId | permission         |
	| 1      | Brokerages.GetAll  |
	| 2      | Brokerages.Create  |
	| 3      | Brokerages.Insureds|

Scenario Outline: User with invalid permissions
	Given a user with UserId and permission <userId>, <permission>
	When i try to find the permissions passing the data
	Then the permission should NOT be in returned permissions
	Examples:
	| userId | permission     |
	| 8      | Brokers.Delete |
	| 9      | Brokers.Get    |
