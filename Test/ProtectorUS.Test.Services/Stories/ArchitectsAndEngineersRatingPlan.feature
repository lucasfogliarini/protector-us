﻿Feature: ArchitectsAndEngineersRatingPlan

Scenario Outline: 1_Base Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the total revenue <totalRevenue>
	When try to calculate the Base Premium
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | totalRevenue | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 650,000      | 4,800                   | 0                       |
	| alabama2       | 100,000      | 975                     | 0                       |
	| arkansas1      | 1,200,000    | 9,186                   | 1                       |
	| california1    | 750,001      | 6,855                   | 2                       |
	| colorado1      | 270,500      | 4,108.5                 | 3                       |
	| florida1       | 1,600,000    | 10,269                  | 4                       |
	| georgia1       | 520,000      | 5,889                   | 5                       |
	| washington1    | 1,100,000    | 9,630                   | 6                       |
	| washington2    | 10,000,000   | 62,250                  | 6                       |
	| washington3    | 2,000,000    | 18,650                  | 6                       |

Scenario Outline: 2_Structural and HVAC Areas
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the revenueFromHVAC <revenueFromHVAC>
	When try to calculate the Structural and HVAC Areas
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey |  revenueFromHVAC | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       |  1               | 3840                    | 0                       |
	| alabama2       |  0               | 877.5                   | 0                       |
	| arkansas1      |  0               | 9,186                   | 1                       |
	| california1    |  0               | 6,855                   | 2                       |
	| colorado1      |  1               | 3903.075                | 3                       |
	| florida1       |  1               | 10,269                  | 4                       |
	| georgia1       |  0               | 5,741.775               | 5                       |
	| washington1    |  0               | 9630                    | 6                       |
	| washington2    |  1               | 62250                   | 6                       |
	| washington3    |  1               | 18,650                  | 6                       |

Scenario Outline: 3_Prior Acts Coverage
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And past years from now <pastYearsFromNow>
	When try to calculate Prior Acts Coverage
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | pastYearsFromNow | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 0                | 3840                    | 0                       |
	| alabama2       | 1                | 877.5                   | 0                       |
	| arkansas1      | 8                | 9,186                   | 1                       |
	| california1    | 4                | 6,855                   | 2                       |
	| colorado1      | 2                | 3903.075                | 3                       |
	| florida1       | 10               | 10,269                  | 4                       |
	| georgia1       | 3                | 5,741.775               | 5                       |
	| washington1    | 5                | 9630                    | 6                       |
	| washington2    | 7                | 62250                   | 6                       |
	| washington3    | 4                | 18,650                  | 6                       |

Scenario Outline: 4_Premium Modification Factors
	//declines respectively: lessthan, 1, lessthan
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And practice modificators <writtenContracts>, <revenueFromServices>, <revenueFromSegments>
	When try to calculate Premium Modification Factors
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | writtenContracts | revenueFromServices | revenueFromSegments | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | atleast          | 1                   | atleast             | 11,424                  | 0                       |
	| alabama2       | lessthan         | 0                   | atleast             | 852,93                  | 0                       |
	| arkansas1      | atleast          | 1                   | lessthan            | 20,550.9192             | 1                       |
	| california1    | atleast          | 0                   | atleast             | 12,818.85               | 2                       |
	| colorado1      | atleast          | 0                   | atleast             | 9,405.43498125          | 3                       |
	| florida1       | atleast          | 0                   | atleast             | 36,782.5311             | 4                       |
	| georgia1       | lessthan         | 0                   | lessthan            | 10,929.4687125          | 5                       |
	| washington1    | atleast          | 0                   | atleast             | 19,710.684              | 6                       |
	| washington2    | lessthan         | 1                   | lessthan            | 350,778.75              | 6                       |
	| washington3    | atleast          | 0                   | atleast             | 82,690.8075             | 6                       |

Scenario Outline: 5_Schedule Modifications
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the risks <claimsLastFive>, <businessYears>, <professionalMemberships>
	When try to calculate Schedule Modifications
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | claimsLastFive | businessYears | professionalMemberships | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 1              | 10            | 1                       | 6,528                   | 0                       |
	| alabama2       | 0              | 1             | 0                       | 852,93                  | 0                       |
	| arkansas1      | 0              | 20            | 1                       | 20,550.9192             | 1                       |
	| california1    | 0              | 8             | 1                       | 12,818,85               | 2                       |
	| colorado1      | 0              | 7             | 0                       | 5,374.534275            | 3                       |
	| florida1       | 0              | 30            | 1                       | 21,018.5892             | 4                       |
	| georgia1       | 2              | 6             | 0                       | 10,929.4687125          | 5                       |
	| washington1    | 0              | 12            | 1                       | 19,710.684              | 6                       |
	| washington2    | 1              | 5             | 0                       | 200,445                 | 6                       |
	| washington3    | 0              | 3             | 0                       | 54,047.7                | 6                       |

Scenario Outline: 6_Coverage
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the coverage <coverage>
	When try to calculate Coverage
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | coverage  | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 500,000   | 7,680                   | 0                       |
	| alabama2       | 100,000   | 947.7                   | 0                       |
	| arkansas1      | 2,000,000 | 24,177.552              | 1                       |
	| california1    | 750,000   | 15,081                  | 2                       |
	| colorado1      | 250,000   | 6,322.9815              | 3                       |
	| florida1       | 2,000,000 | 23,189.04               | 4                       |
	| georgia1       | 1,000,000 | 12,143.854125           | 5                       |
	| washington1    | 2,000,000 | 24,356.92               | 6                       |
	| washington2    | 2,000,000 | 20,055.5                | 6                       |
	| washington3    | 2,000,000 | 60,053                  | 6                       |

Scenario Outline: 7_Deductible
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the deductible <deductible>
	When try to calculate Deductible
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | deductible | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 2,500      | 3,840                   | 0                       |
	| alabama2       | 1,000      | 947.7                   | 0                       |
	| arkansas1      | 5,000      | 8,634.84                | 1                       |
	| california1    | 2,500      | 6,855                   | 2                       |
	| colorado1      | 1,000      | 4,215.321               | 3                       |
	| florida1       | 10,000     | 8,831.34                | 4                       |
	| georgia1       | 7,500      | 5,167.5975              | 5                       |
	| washington1    | 10,000     | 8,281.8                 | 6                       |
	| washington2    | 0          | 71,587.5                | 6                       |
	| washington3    | 0          | 21447.5                 | 6                       |

Scenario Outline: 8_First Dollar Defense
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the fdd option <fdd>
	When try to calculate First Dollar Defense
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | fdd   | currentPremiumEstimated | ratingPlanComputeIndex |
	| alabama1       | false | 6528                    | 0                       |
	| alabama2       | false | 852,93                  | 0                       |
	| arkansas1      | true  | 20,550.9192             | 1                       |
	| california1    | true  | 12,818.85               | 2                       |
	| colorado1      | false | 5,374.534275            | 3                       |
	| florida1       | true  | 21,018.5892             | 4                       |
	| georgia1       | true  | 10,929.4687125          | 5                       |
	| washington1    | false | 19,710.684              | 6                       |
	| washington2    | true  | 200,445                 | 6                       |
	| washington3    | true  | 54,047.7                | 6                       |


Scenario Outline: 9_Minimum Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	When try to checks the Minimum Premium
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | currentPremiumEstimated | ratingPlanComputerIndex |
	| alabama1       | 10,708                  | 0                       |
	| alabama2       | 1,400                   | 0                       |
	| arkansas1      | 18,647                  | 1                       |
	| california1    | 9,806                   | 2                       |
	| colorado1      | 5,918                   | 3                       |
	| florida1       | 36,782.5311             | 4                       |
	| georgia1       | 10,929.4687125          | 5                       |
	| washington1    | 20,703                  | 6                       |
	| washington2    | 384,899                 | 6                       |
	| washington3    | 82,691                  | 6                       |

Scenario Outline: 10_Rounding and Final Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	When try to calculate the Final Premium then rounding him
	Then the current premium calculated should be equals the <finalPremiumExpected>
	And checks if the quote was <declined>
	Examples:
	| calculationKey | finalPremiumExpected | ratingPlanComputerIndex | declined |
	| alabama1       | 10,708               | 0                       | true     |
	| alabama2       | 1,400                | 0                       | true     |
	| arkansas1      | 18,647               | 1                       | true     |
	| california1    | 9,806                | 2                       | false    |
	| colorado1      | 5,918                | 3                       | false    |
	| florida1       | 36,783               | 4                       | false    |
	| georgia1       | 10,929               | 5                       | true     |
	| washington1    | 20,703               | 6                       | false    |
	| washington2    | 384,899              | 6                       | true     |
	| washington3    | 82,691               | 6                       | false    |