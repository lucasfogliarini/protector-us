﻿Feature: AccountantsRatingPlan

Scenario Outline: 1_Base Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the total revenue <totalRevenue>
	When try to calculate the Base Premium
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | totalRevenue | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 600000       | 2,197                   | 0                       |


Scenario Outline: 2_Revenue Per Staff Credits
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the staff number <staffNumber>
	When try to calculate the Revenue Per Staff Credits
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | staffNumber | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 20          | 1,867.45                | 0                       |
	
	
Scenario Outline: 3_Prior Acts Coverage
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And past years from now <pastYearsFromNow>
	When try to calculate Prior Acts Coverage
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | pastYearsFromNow | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 4                | 3,473.457               | 0                       |

Scenario Outline: 4_Clients
	//revenueFromIndustries: construction, factoringcompanies, entertainment, financialinstitutions, healthcareorgs, insurancecompanies, pensionfunds, professionalathletes, realestatedeveloper, unions
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the clients <revenueFromIndustries>
	When try to calculate the Client Factors
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | revenueFromIndustries | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | financialinstitutions | 5,210.1855                | 0                       |
	

Scenario Outline: 5_Premium Modification Factors
    //writtenContracts lessthan decline atLeast factor 1
	//revenueFromServices ido decline odonot factor 1
	//revenueFromSegments Lessthan decline atLeast factor 1
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And premium modificators <writtenContracts>, <revenueFromServices>, <revenueFromSegments>
	When try to calculate Premium Modification Factors
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | writtenContracts | revenueFromServices | revenueFromSegments | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1      | atleast          | 0                   | atleast             | 5,210.1855           | 0                       |
	
		 
Scenario Outline: 6_Coverage
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the coverage <coverage>
	When try to calculate Coverage
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | coverage | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 500000   | 8,857.31535                | 0                       |


Scenario Outline: 7_Deductible
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the deductible <deductible>
	When try to calculate Deductible
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | deductible | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1      | 500        | 10185.9126525                 | 0                       |


Scenario Outline: 8_Schedule Modifications
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the risks <claimsLastFive>, <businessYears>, <professionalMemberships>
	When try to calculate Schedule Modifications
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | claimsLastFive | businessYears | professionalMemberships | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 0              | 11            | 1                       | 7639.434489375               | 0                       |


Scenario Outline: 9_Defense Outside Limit
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	And the dol option <dol>
	When try to calculate Defense Outside Limit
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | dol  | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | true | 8,403.377938125         | 0                       |




Scenario Outline: 10_Minimum Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	When try to checks the Minimum Premium
	Then the current premium calculated should be equals the <currentPremiumEstimated>
	Examples:
	| calculationKey | currentPremiumEstimated | ratingPlanComputerIndex |
	| arizona1       | 8,403.377938125         | 0                       |



Scenario Outline: 11_Rounding and Final Premium
	Given the Rating Plan Form and current calculation <ratingPlanComputerIndex>, <calculationKey>
	When try to calculate the Final Premium then rounding him
	Then the current premium calculated should be equals the <finalPremiumEstimated>
	And checks if the quote was <declined>
	Examples:
	| calculationKey | finalPremiumEstimated | ratingPlanComputerIndex | declined |
	| arkansas1      | 8,403                 | 0                       | false    |


