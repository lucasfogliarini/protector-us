﻿Feature: Check Permissions

Scenario: Check ManagerUser permissions
	Given the ManagerUser permission strings
	And the all controllers in ArgoManager API
	When I check the permission string in all controllers
	Then all permissions should be checked

Scenario: Check Broker permissions
	Given the Broker permission strings
	And the all controllers in BrokerCenter API
	When I check the permission string in all controllers
	Then all permissions should be checked
