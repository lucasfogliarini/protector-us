﻿Feature: BrokerageService

Scenario Outline: Creating a Brokerage
	Given a Brokerage <name>, <email>, <website>, <phone>, <regionId>, <streetLine1>, <streetLine2>, <comission1>, <comission2>, <productId1>, <productId2>
	When I try to create the brokerage passing the data
	Then the result should be a Brokerage
	Examples: 
	| name               | email                         | website                | phone     | regionId | streetLine1     | streetLine2 | comission1 | comission2 | productId1 | productId2 |
	| CreatingBrokerage1 | master@creatingbrokerage1.com | creatingbrokerage1.com | 987654321 | 1        | Xavier Ferreira | 137         | 20         | 10         | 1          | 2          |
	| CreatingBrokerage2 | master@creatingbrokerage2.com | creatingbrokerage2.com | 123456789 | 2        | 24 de Outubro   | 200         | 30         | 15         | 1          | 2          |
	| CreatingBrokerage3 | master@creatingbrokerage3.com | creatingbrokerage3.com | 098765432 | 3        | New York        | 500         | 15         | 10         | 1          | 2          |


Scenario: Creating a Brokerage with a existing name
	Given a Brokerage 'CreatingBrokerage5', 'master@brokerage5.com', 'creatingbrokerage4.com', '987654321', 1, 'Xavier Ferreira', 20, 1
	And another Brokerage with same name 'CreatingBrokerage5', 'master@brokerage5.com', 'anotherbrokerage.com', '987654321', 2, 'Another Street', 20, 1
	When I try to create the brokerage passing the data
	Then the result should be a message code saying that there is already a brokerage with this name

Scenario Outline: Validating a Brokerage with errors
	Given a Brokerage <name>, <email>, <website>, <phone>, <regionId>, <streetLie1>, <streetLine2>, <comission1>, <comission2>, <productId1>, <productId2>
	When I try to create the brokerage passing the data
	Then the result should be validation errors of the Brokerage
	Examples: 
	| name       | email             | website        | phone     | regionId | streetLine1     | streetLine2 | comission1 | comission2 | productId1 | productId2 |
	|            | master@broker.com | brokerage1.com | 987654321 | 1        | Xavier Ferreira | 137         | 20         | 10         | 1          | 2          |
	| Brokerage2 |                   | brokerage2.com | 123456789 | 2        | 24 de Outubro   | 200         | 30         | 15         | 1          | 2          |
	| Brokerage3 | master@broker.com | brokerage3.com | 098765432 | 1        | 24 de Outubro   | 500         | 15         | 10         | 0          | 0          |

Scenario Outline:  On Boarding
	Given a Brokerage <name>, <email>, <website>, <phone>, <regionId>, <streetLine1>, <streetLine2>, <comission1>, <comission2>, <productId1>, <productId2>
	When I create a Brokerage and try to board passing the created BrokerageId
	Then the result should be a BrokerageOnboard
	When I try to update the Brokerage passing the onboarding Brokerage
	Then the result should be a Brokerage equals to given
	Examples: 
	| name       | email              | website        | phone     | regionId | streetLine1     | streetLine2 | comission1 | comission2 | productId1 | productId2 |
	| Brokerage6 | master6@broker.com | brokerage1.com | 987654321 | 1        | Xavier Ferreira | 137         | 20         | 10         | 1          | 2          |
	| Brokerage7 | master7@broker.com | brokerage2.com | 123456789 | 2        | 24 de Outubro   | 200         | 30         | 15         | 1          | 2          |