﻿namespace ProtectorUS.Test.Services.Steps
{
    using Model;
    using NUnit.Framework;
    using System;
    using TechTalk.SpecFlow;
    public abstract class RatingPlanSteps : BaseSteps
    {
        protected const string ratingPlanComputerPrefixKey = "ratingPlanComputer";
        protected CalculationRatingPlan CurrentCalculation { get; set; }
        protected RatingPlanComputer Computer { get; set; }

        protected DateTime? GetDateFromYears(int pastYearsFromNow)
        {
            if (pastYearsFromNow <= 0) return null;            
            return DateTime.Now.AddYears(-pastYearsFromNow);
        }

        [Given(@"the Rating Plan Form and current calculation (.*), (.*)")]
        public void GivenTheRatingPlanFormAndCurrentCalculation(int ratingPlanComputerIndex, string currentCalculationKey)
        {
            SetCurrentRatingPlanComputer(ratingPlanComputerIndex);
            SetCurrentCalculation(currentCalculationKey);
        }
        private void SetCurrentRatingPlanComputer(int ratingPlanComputerIndex)
        {
            string key = ratingPlanComputerPrefixKey + ratingPlanComputerIndex;
            Computer = GetCache<RatingPlanComputer>(key);
           // Computer.IsDeclined = false;
        }
        private void SetCurrentCalculation(string currentCalculationSufixKey)
        {
            string key = "currentCalculation" + currentCalculationSufixKey;
            CurrentCalculation = GetCache<CalculationRatingPlan>(key);
            if (CurrentCalculation == null)
            {
                CurrentCalculation = new CalculationRatingPlan(key);
                UpdateCache(CurrentCalculation.Key, CurrentCalculation);
            }
        }
        
        [Then(@"the current premium calculated should be equals the (.*)")]
        public void ThenTheCurrentPremiumCalculatedShouldBeEqualsThe(decimal currentPremiumExpected)
        {
            Assert.AreEqual(currentPremiumExpected, CurrentCalculation.CurrentPremium, $"The calculation is wrong!");
        }

        [Then(@"checks if the quote was (.*)")]
        public void ThenChecksIfTheQuoteWas(bool declined)
        {
            Assert.AreEqual(declined, CurrentCalculation.IsDeclined, "The quote was {0} declined. Different than expected.", CurrentCalculation.IsDeclined ? "" : "NOT");
        }
    }

    public class CalculationRatingPlan
    {
        public CalculationRatingPlan(string key)
        {
            Key = key;
        }
        public string Key { get; private set; }
        public decimal CurrentPremium { get; set; }
        public decimal Revenue { get; set; }
        public bool DefenseOutsideLimit { get; set; }
        public string CoverageValue { get; set; }
        public string DeductibleValue { get; set; }
        public bool IsDeclined { get; internal set; }
    }
}
