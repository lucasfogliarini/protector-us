﻿using TechTalk.SpecFlow;
using System.Linq;

namespace ProtectorUS.Test.Services.Steps
{
    using System.Collections.Generic;
    using Api.Controllers;
    using System;
    using NUnit.Framework;
    using Model;
    [Binding, Scope(Feature = "Check Permissions")]
    public sealed class CheckPermissionsSteps : BaseSteps
    {
        private IEnumerable<string> _permissions;
        private IEnumerable<Type> _controllers;
        private List<string> _permissionsNotFounded = new List<string>();

        #region Check ManagerUser permissions

        [Given(@"the ManagerUser permission strings")]
        public void GivenTheManagerUserPermissionStrings()
        {
            _permissions = Profile.ManagerUserPermissions.AllPermissions;
        }
        [Given(@"the all controllers in ArgoManager API")]
        public void GivenTheAllControllersInArgoManagerAPI()
        {
            TryCatch(() =>
            {
                var argoManagerAssembly = typeof(Api.ArgoManager.Start).Assembly;
                _controllers = argoManagerAssembly.GetTypes().Where(t => t.IsSubclassOf(typeof(BaseController)));
            });
        }
        #endregion
        #region Check Broker permissions

        [Given(@"the Broker permission strings")]
        public void GivenTheBrokerPermissionStrings()
        {
            _permissions = Profile.BrokerPermissions.AllPermissions;
        }

        [Given(@"the all controllers in BrokerCenter API")]
        public void GivenTheAllControllersInBrokerCenterAPI()
        {
            TryCatch(() =>
            {
                var assembly = typeof(Api.BrokerCenter.Start).Assembly;
                _controllers = assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(BaseController)));
            });
        }

        #endregion

        [When(@"I check the permission string in all controllers")]
        public void WhenICheckThePermissionStringInAllControllers()
        {
            TryCatch(() =>
            {
                foreach (string permission in _permissions)
                {
                    var permissionSplited = permission.Split('.');
                    string controllerName = permissionSplited[0];
                    string actionName = permissionSplited[1];
                    var controller = _controllers.FirstOrDefault(c => c.Name == $"{controllerName}Controller");
                    if (controller == null || !controller.GetMethods().Any(m => m.Name == actionName))
                    {
                        _permissionsNotFounded.Add(permission);
                    }
                }
            });
        }

        [Then(@"all permissions should be checked")]
        public void ThenAllPermissionsShouldBeChecked()
        {
            var permissionsNotFounded = _permissionsNotFounded.Join(", ");
            Assert.True(!_permissionsNotFounded.Any(), "Not founded: " + permissionsNotFounded);
        }
    }
}
