﻿using Autofac;
using TechTalk.SpecFlow;
using System.Linq;

namespace ProtectorUS.Test.Services.Steps
{
    using Model.Services;
    using NUnit.Framework;
    using DTO;
    using Communication;
    using Model;
    using System.Collections.Generic;
    using DTO.RatingPlan;
    using Model.DocumentDB.RatingPlan;
    using System;
    [Binding, Scope(Feature = "Activation")]
    public class ActivationSteps : BaseSteps
    {
        #region Fields
        readonly IQuotationService _quotationService;
        readonly IOrderService _orderService;
        readonly IInsuredService _insuredService;
        readonly IAuthenticationService _authenticationService;
        readonly IRatingPlanFormService _ratingPlanService;
        private string _email;
        private string _password;
        private string _productAlias;
        private string _zipCode;
        private InsuredInsertDTO _insuredIn;
        private OutputInsuredDTO _insuredOut;
        private OutputRatingPlanFormDTO _ratingPlanFormOutput;
        private QuotationInsertDTO _quotationIn;
        private QuotationHashDTO _quotationOut;
        private OutputOrderDTO _orderOut;
        private Guid _quotationHash;
        private InputOrderDTO _orderIn;
        private User _insuredUser;
        private OutputRatingPlanFormDTO _ratingPlanFormOut;
        #endregion

        public ActivationSteps()
        {
            _quotationService = container.Resolve<IQuotationService>();
            _orderService = container.Resolve<IOrderService>();
            _insuredService = container.Resolve<IInsuredService>();
            _authenticationService = container.Resolve<IAuthenticationService>();
            _ratingPlanService = container.Resolve<IRatingPlanFormService>();
        }

        #region Activating with a new Insured

        [Given(@"I have entered a email (.*)")]
        public void GivenIHaveEnteredAEmail(string email)
        {
            _email = email;
        }
        [When(@"I try to get the insured passing the email")]
        public void WhenITryToGetTheInsuredPassingTheEmail()
        {
            TryCatch(() =>
            {
                _insuredOut = _insuredService.FindUser<OutputInsuredDTO>(_email);
            });
        }
        [Then(@"the result should be a message code saying that the User was not found")]
        public void ThenTheResultShouldBeAMessageCodeSayingThatTheUserWasNotFound()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.EL026));
        }

        #endregion
        #region Activating with a existing Insured
        [Then(@"the result should be a Insured")]
        public void ThenTheResultShouldBeAInsured()
        {
            Assert.IsInstanceOf<OutputInsuredDTO>(_insuredOut);
        }
        #endregion
        #region Activating with a existing Insured giving the password
        [Given(@"I have entered a password (.*)")]
        public void GivenIHaveEnteredAPassword(string password)
        {
            _password = password;
        }
        [When(@"I try to authenticate passing the email and password")]
        public void WhenITryToAuthenticatePassingTheEmailAndPassword()
        {
            TryCatch(() =>
            {
                _insuredUser = _authenticationService.Authenticate<Insured>(_email, _password);
            });
        }
        [Then(@"the result should be a InsuredUser")]
        public void ThenTheResultShouldBeAInsuredUser()
        {
            Assert.IsInstanceOf<Insured>(_insuredUser);
        }

        #endregion
        #region Getting a existing RatingPlan
        [Given(@"I have entered a (.*), (.*)")]
        public void GivenIHaveEntered(string productAlias, string zipCode)
        {
            _productAlias = productAlias;
            _zipCode = zipCode;
        }

        [When(@"I try to get the RatingPlan")]
        public void WhenITryToGetTheRatingPlan()
        {
            TryCatch(() =>
            {
                _ratingPlanFormOutput = _ratingPlanService.GetLatestVersion(_productAlias, _zipCode);
            });
        }

        [Then(@"the result should be a RatingPlan")]
        public void ThenTheResultShouldBeARatingPlan()
        {
            Assert.IsInstanceOf<OutputRatingPlanFormDTO>(_ratingPlanFormOutput);
        }
        #endregion
        #region Getting a inexisting RatingPlan
        [Then(@"the result should be a message code saying that the Rating Plan was not found")]
        public void ThenTheResultShouldBeAMessageCodeSayingThatTheRatingPlanWasNotFound()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.EL024));
        }
        #endregion
        #region Creating a Quotation

        [Given(@"a productAlias and zipCode '(.*)', '(.*)' to get a RatingPlan HashCode")]
        public void GivenAProductAliasAndZipCodeToGetARatingPlanHashCode(string productAlias, string zipCode)
        {
            TryCatch(() =>
            {
                _ratingPlanFormOut = _ratingPlanService.GetLatestVersion(productAlias, zipCode);
            });
        }


        [Given(@"the essencial part of the Quotation (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public void GivenTheEssencialPartOfTheQuotation(string email, SocialTitle socialTitle, string firstName, string lastName, long insuredId, long brokerageProductId, long campaignId)
        {
            long? insuredIdNullable = null; if (insuredId > 0) insuredIdNullable = insuredId;
            long? campaignIdNullable = null; if (campaignId > 0) campaignIdNullable = campaignId;

            List<InputRiskAnalysisFieldDTO> fieldInputs = new List<InputRiskAnalysisFieldDTO>();
            foreach (var field in _ratingPlanFormOut.fields)
            {
                string value = field.name + 1;
                switch (field.name?.ToLower())
                {
                    case "revenue":
                        value = "50000";
                        break;
                    default:
                        var optionValue = field.options?.FirstOrDefault();
                        value = optionValue == null ? field.name + 1 : optionValue.value;
                        break;
                }
                fieldInputs.Add(new InputRiskAnalysisFieldDTO()
                {
                    Name = field.name,
                    Value = value
                });
            }
            fieldInputs.Add(new InputRiskAnalysisFieldDTO() { Name = RatingPlanComputer.retroactiveDateField, Value = "01/01/2015" });

            _quotationIn = new QuotationInsertDTO()
            {
                ProponentEmail = email,
                ProponentSocialTitle = socialTitle,
                ProponentFirstName = firstName,
                ProponentLastName = lastName,
                InsuredId = insuredIdNullable,
                BrokerageProductId = brokerageProductId,
                CampaignId = campaignIdNullable,
                RiskAnalysis = new InputRiskAnalysisDTO()
                {
                    HashCode = _ratingPlanFormOut.HashCode,                    
                    Fields = fieldInputs
                }
            };
        }
        [When(@"I try to create a Quotation")]
        public void WhenITryToCreateAQuotation()
        {
            TryCatch(() =>
            {
                _quotationOut = _quotationService.Create(_quotationIn);
            });
        }
        [Then(@"the result should be a Quotation")]
        public void ThenTheResultShouldBeAQuotation()
        {
            Assert.IsInstanceOf<OutputQuotationDTO>(_quotationOut);
        }

        #endregion
        #region Updating Quotation details
        [Given(@"more details of the Quotation (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public void GivenMoreDetailsOfTheQuotation(string quotationHash, PhoneType phoneType, string phone, long homeRegionId, string homeStreetAddressLine1, string homeStreetAddressLine2, BusinessType businessType, string businessName, string businessAddressLine1, string businessAddressLine2)
        {
            _quotationHash = Guid.Parse(quotationHash);
            _insuredIn = new InsuredInsertDTO()
            {
                //Phone = new PhoneDTO()
                //{
                //    Number = phone,
                //    Type = phoneType
                //},
                //Address = new AddressDTO()
                //{
                //    StreetLine1 = homeStreetAddressLine1,
                //    StreetLine2 = homeStreetAddressLine2,
                //    RegionId = homeRegionId
                //},
                Business = new InputBusinessDTO()
                {
                    Name = businessName,
                    Type = businessType,
                    Address = new BusinessAddressDTO()
                    {
                        StreetLine1 = businessAddressLine1,
                        StreetLine2 = businessAddressLine2,
                    }
                }
            };
        }
        [When(@"I try to update a Quotation passing the data")]
        public void WhenITryToUpdateAQuotationPassingTheData()
        {
            TryCatch(() =>
            {
                ScenarioContext.Current.Pending();
            });
        }
        #endregion

        #region Activating
        [Given(@"a Order (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public void GivenAOrder(string quotationHash, int totalInstallments, string cardHoldName, string addressLine1, string addressLine2, string zipCode, string numberCreditCard, string cvc, string expirationYear, string expirationMonth)
        {
            _orderIn = new InputOrderDTO()
            {
                QuotationHash = Guid.Parse(quotationHash),
                TotalInstallments = totalInstallments,
                CreditCard = new CreditCardInfoDTO()
                {
                    Number = numberCreditCard,
                    Cvc = cvc,
                    ExpirationMonth = expirationMonth,
                    ExpirationYear = expirationYear,
                    CardHoldName = cardHoldName,
                    AddressLine1 = addressLine1,
                    AddressLine2 = addressLine2,
                    ZipCode = zipCode,
                }
            };
        }
        [When(@"I try to activate")]
        public void WhenITryToActivate()
        {
            TryCatch(() =>
            {
                _orderOut = _orderService.Create(_orderIn);
            });
        }
        [Then(@"the result should be a Order")]
        public void ThenTheResultShouldBeAOrder()
        {
            Assert.IsInstanceOf<OutputOrderDTO>(_orderOut);
        }

        #endregion

    }
}
