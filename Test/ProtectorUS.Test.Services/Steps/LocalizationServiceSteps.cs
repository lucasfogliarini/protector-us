﻿using Autofac;
using TechTalk.SpecFlow;

namespace ProtectorUS.Test.Services.Steps
{
    using Model.Services;
    using NUnit.Framework;
    using Communication;
    using DTO;

    [Binding, Scope(Feature = "LocalizationService")]
    public class LocalizationServiceSteps : BaseSteps
    {
        private ILocalizationService _localizationService;
        private OutputRegionDTO _region;
        private string _zipcode;
        private OutputAddressDTO _addressOut;
        private AddressDTO _addressIn;

        public LocalizationServiceSteps()
        {
            _localizationService = container.Resolve<ILocalizationService>();
        }

        #region GetRegion

        [Given(@"I have entered a (.*)")]
        public void GivenIHaveEnteredA(string zipcode)
        {
            _zipcode = zipcode;
        }
        
        [When(@"I try to get the region passing the zipcode")]
        public void WhenITryToGetTheRegionPassingTheZipcode()
        {
            TryCatch(() =>
            {
                _region = _localizationService.GetRegion(_zipcode);
            });
        }
        
        [Then(@"the result should be a Region")]
        public void ThenTheResultShouldBeARegion()
        {
            Assert.IsInstanceOf<OutputRegionDTO>(_region);
        }

        [Then(@"the result should be a message code saying the was not found")]
        public void ThenTheResultShouldBeAMessageCodeSayingTheWasNotFound()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.EL018));
        }

        [Then(@"the result should be validation errors of the Region")]
        public void ThenTheResultShouldBeValidationErrorsOfTheRegion()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.ES017));
        }

        #endregion

        #region CreateAddress

        [Given(@"I have entered (.*), (.*), (.*)")]
        public void GivenIHaveEntered(string streetLine1, string streetLine2, string zipcode)
        {
            OutputRegionDTO region = _localizationService.GetRegion(zipcode);
            _addressIn = new AddressDTO()
            {
                StreetLine1 = streetLine1,
                StreetLine2 = streetLine2,
                RegionId = region.Id,
            };
        }

        [When(@"I try to create the address passing the data")]
        public void WhenITryToCreateTheAddressPassingTheData()
        {
            TryCatch(() =>
            {               
                //_addressOut = _localizationService.CreateAddress(_addressIn);
            });
        }

        [Then(@"the result should be a Address")]
        public void ThenTheResultShouldBeAAddress()
        {
            Assert.IsInstanceOf<OutputAddressDTO>(_addressOut);
        }
        
        #endregion

    }
}
