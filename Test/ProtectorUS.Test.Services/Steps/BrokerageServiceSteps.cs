﻿using Autofac;
using System;
using TechTalk.SpecFlow;

namespace ProtectorUS.Test.Services.Steps
{
    using Model.Services;
    using Exceptions;
    using NUnit.Framework;
    using Communication;
    using DTO;
    using System.Collections.Generic;
    using Model.Repositories;
    [Binding, Scope(Feature = "BrokerageService")]
    public class BrokerageServiceSteps : BaseSteps
    {
        private readonly IBrokerageService _brokerageService;
        private readonly ILocalizationService _localizationService;
        private readonly IBrokerRepository _brokerRepository;
        private InputBrokerageDTO _brokerageIn;
        private OutputBrokerageDTO _brokerageOut;
        private InputBrokerageUpdateDTO _brokerageUpdate;
        private OutputBrokerageOnboardDTO _brokerageOnboardingOut;

        public BrokerageServiceSteps()
        {
            _brokerageService = container.Resolve<IBrokerageService>();
            _brokerRepository = container.Resolve<IBrokerRepository>();
            _localizationService = container.Resolve<ILocalizationService>();
        }

        private void InputBrokerage(string name, string email, string website, string phone, long regionId, string streetLine1, string streetLine2, IList<InputWebpageProductDTO> webpageProducts)
        {
            _brokerageIn = new InputBrokerageDTO()
            {
                Name = name,
                Email = email,
                Website = website,
                Address = new AddressDTO()
                {
                    StreetLine1 = streetLine1,
                    StreetLine2 = streetLine2,
                    RegionId = regionId,
                },
                Phones = new List<PhoneDTO>()
                {
                     new PhoneDTO() { Number = phone }
                },
                WebpageProducts = webpageProducts
            };
        }

        #region Creating a Brokerage

        [Given(@"a Brokerage (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public void GivenABrokerage(string name, string email, string website, string phone, long regionId, string streetLine1, string streetLine2, decimal comission1, decimal comission2, long productId1, long productId2)
        {
            var webpageProducts = new List<InputWebpageProductDTO>();
            if (productId1 != 0)
            {
                var webpageProduct = new InputWebpageProductDTO() { ProductId = productId1, BrokerageComission = comission1 };
                webpageProducts.Add(webpageProduct);
            }
            if (productId2 != 0)
            {
                var webpageProduct = new InputWebpageProductDTO() { ProductId = productId2, BrokerageComission = comission2 };
                webpageProducts.Add(webpageProduct);
            }

            InputBrokerage(name, email, website, phone, regionId, streetLine1, streetLine2, webpageProducts);
        }

        [When(@"I try to create the brokerage passing the data")]
        public void WhenITryToCreateTheBrokeragePassingTheData()
        {
            TryCatch(() =>
            {
                _brokerageOut = _brokerageService.Create(_brokerageIn);
            });
        }

        [Then(@"the result should be a Brokerage")]
        public void ThenTheResultShouldBeABrokerage()
        {
            Assert.IsInstanceOf<OutputBrokerageDTO>(_brokerageOut);
        }

        #endregion

        #region Creating a Brokerage with a existing name

        [Given(@"a Brokerage '(.*)', '(.*)', '(.*)', '(.*)', (.*), '(.*)', (.*), (.*)")]
        public void GivenABrokerage(string name, string email, string website, string phone, long regionId, string streetLine1, int comission, int productId)
        {
            var webpageProducts = new List<InputWebpageProductDTO>()
            {
                new InputWebpageProductDTO() { ProductId = productId, BrokerageComission = comission },
            };

            InputBrokerage(name, email, website, phone, regionId, streetLine1, null, webpageProducts);

            TryCatch(() =>
            {
                _brokerageOut = _brokerageService.Create(_brokerageIn);
            });
        }
        [Given(@"another Brokerage with same name '(.*)', '(.*)', '(.*)', '(.*)', (.*), '(.*)', (.*), (.*)")]
        public void GivenAnotherBrokerageWithSameName(string name, string email, string website, string phone, long regionId, string streetLine1, int comission, int productId)
        {
            var webpageProducts = new List<InputWebpageProductDTO>()
            {
                new InputWebpageProductDTO() { ProductId = productId, BrokerageComission = comission },
            };

            InputBrokerage(name, email, website, phone, regionId, streetLine1, null, webpageProducts);
        }
        [Then(@"the result should be a message code saying that there is already a brokerage with this name")]
        public void ThenTheResultShouldBeAMessageCodeSayingThatThereIsAlreadyABrokerageWithThisName()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.EL020));
        }

        #endregion

        #region Validating a Brokerage with errors

        [Then(@"the result should be validation errors of the Brokerage")]
        public void ThenTheResultShouldBeValidationErrorsOfTheBrokerage()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.ES017));
        }

        #endregion

        #region On Boarding
        [When(@"I create a Brokerage and try to board passing the created BrokerageId")]
        public void WhenICreateABrokerageAndTryToBoardPassingTheCreatedBrokerageId()
        {
            TryCatch(() =>
            {
                var brokerageOutCreated = _brokerageService.Create(_brokerageIn);
                _brokerageOnboardingOut = _brokerageService.OnBoarding(brokerageOutCreated.Id);
            });
        }
        [Then(@"the result should be a BrokerageOnboard")]
        public void ThenTheResultShouldBeABrokerageOnboard()
        {
            Assert.IsInstanceOf<OutputBrokerageOnboardDTO>(_brokerageOnboardingOut);
        }
        [When(@"I try to update the Brokerage passing the onboarding Brokerage")]
        public void WhenITryToUpdateTheBrokeragePassingTheOnboardingBrokerage()
        {
            TryCatch(() =>
            {
                var addressOut = _brokerageOnboardingOut.Address;
                _brokerageUpdate = new InputBrokerageUpdateDTO()
                {
                    Name = _brokerageOnboardingOut.Name + "_diff",
                    Alias = _brokerageOnboardingOut.Alias + "_diff",
                    Email = "diff_"+ _brokerageOnboardingOut.Email,
                    Website = _brokerageOnboardingOut.Website + "_diff",
                    Address = new AddressDTO()
                    {
                        RegionId = addressOut.RegionId,
                        StreetLine1 = addressOut?.StreetLine1,
                        StreetLine2 = addressOut?.StreetLine2,
                    },
                    Phones = new List<PhoneDTO>() { _brokerageOnboardingOut.Phone }
                };
                _brokerageOut = _brokerageService.Update(_brokerageOnboardingOut.Id, _brokerageUpdate);
            });
        }

        [Then(@"the result should be a Brokerage equals to given")]
        public void ThenTheResultShouldBeABrokerageEqualsToGiven()
        {
            Assert.True(_brokerageUpdate.Name == _brokerageOut.Name, "Same Name");
            Assert.True(_brokerageUpdate.Alias.Simplify() == _brokerageOut.Alias, "Simplifying the Alias must be the same.");
            Assert.True(_brokerageUpdate.Email == _brokerageOut.Email, "Same Email");
            Assert.True(_brokerageUpdate.Website == _brokerageOut.Website, "Same Website");
            Assert.True(_brokerageOut.Status == Model.BrokerageStatus.Active, "Active!");
        }
        #endregion
    }
}
