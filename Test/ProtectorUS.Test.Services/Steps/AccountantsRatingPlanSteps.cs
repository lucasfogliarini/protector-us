﻿using TechTalk.SpecFlow;
using System.Linq;
using System.Collections.Generic;
namespace ProtectorUS.Test.Services.Steps
{
    using Model;
    using RatingPlanDoc = Model.DocumentDB.RatingPlan;
    [Binding, Scope(Feature = "AccountantsRatingPlan")]
    public sealed class AccountantsRatingPlanSteps : RatingPlanSteps
    {
        private static RatingPlanDoc.RatingPlanForm CreateRatingPlanForm()
        {
            #region fields

            string totalClaimsLosses = "totalClaimsLosses";

            var fields = new List<RatingPlanDoc.Field>()
            {                
                #region Revenue
  
                new RatingPlanDoc.Field()
                {
                    name = RatingPlanComputer.revenueField,
                    type = "money",
                    placeholder = "Enter amount",
                    tooltip = ""
                },

                #endregion

                #region StaffNumber
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.staffNumberField,
                    type = "number",
                    placeholder = "Type number",
                    tooltip = ""
                },
                #endregion

                #region Prior Acts Coverage
                new RatingPlanDoc.Field()
                {
                    name = RatingPlanComputer.existingPolicyField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do", "1"),
                        new RatingPlanDoc.Option("I do not", "0")
                    }
                },

                new RatingPlanDoc.Field()
                {
                    label = "The retroactive date on the current policy is",
                    name = RatingPlanComputer.retroactiveDateField,
                    type = "dataPicker",
                    tooltip = "",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(RatingPlanComputer.existingPolicyField, "1") { Operator = RatingPlanDoc.Condition.EqualsOperator }
                    }
                },

                #endregion Prio Acts Coverage               

                #region Deductible

                new RatingPlanDoc.Field()
                {
                    name = RatingPlanComputer.deductibleField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("500","500", 1.15m) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.coverageField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("1,000","1000", 1.03m),
                        new RatingPlanDoc.Option("2,500","2500", 1m),
                        new RatingPlanDoc.Option("5,000","5000", 0.95m),
                        new RatingPlanDoc.Option("7,500","7500", 0.93m),
                        new RatingPlanDoc.Option("10,000","10000", 0.91m),
                    }
                },

                #endregion Deductible Factors

                #region Coverage

                new RatingPlanDoc.Field()
                {
                    name = RatingPlanComputer.coverageField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("100,000","100000", 1) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.revenueField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("250,000","250000", 1.35m),
                        new RatingPlanDoc.Option("500,000","500000", 1.7m),
                        new RatingPlanDoc.Option("1 million","1000000", 2.15m),
                        new RatingPlanDoc.Option("2 millions","2000000", 2.75m),
                    }
                },

                #endregion Increased Limit Factor
                
                #region Schedule Modifications

                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.businessManagementField,
                    type = "monthPicker",
                    placeholder = "Click to select",
                    tooltip = ""
                },
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.professionalMembershipsField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I'm","1", 0.9m ),
                        new RatingPlanDoc.Option("I'm not","0", 1)
                    }
                },
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.claimsLastFiveField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("hadno","0", 0.9m),
                        new RatingPlanDoc.Option("hadone","1") { isDeclined = true },
                        new RatingPlanDoc.Option("hadtwo","2") { isDeclined = true },
                        new RatingPlanDoc.Option("hadtreeormore","3") { isDeclined = true },
                    }
                },
                new RatingPlanDoc.Field()
                {
                    name = totalClaimsLosses,
                    type = "money",
                    placeholder = "Type amount",
                    tooltip = "",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(AccountantsComputer.claimsLastFiveField, "0") {Operator = RatingPlanDoc.Condition.DifferentOperator }
                    }
                },

                #endregion Schedule Modifications
                
                #region Defense Outside Limits

                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.defenseOutsideLimitField,
                    type = "select",
                    tooltip = "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("want","1"),
                        new RatingPlanDoc.Option("don't want","0"),
                    }
                },

                #endregion Defense Outside Limits
                                
                #region Practice Modification Factors
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.revenueFromServicesField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do","1"){isDeclined = true },
                        new RatingPlanDoc.Option("I do not","0", 1)
                    }
                },
                new RatingPlanDoc.Field()
                {
                    name = AccountantsComputer.revenueFromSegmentsField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("At Least","atleast", 1),
                        new RatingPlanDoc.Option("Less Than","lessthan") { isDeclined = true }
                    }
                },

                new RatingPlanDoc.Field()
                {  
                    name = AccountantsComputer.revenueFromIndustriesField,
                    type = "checkbox",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("Construction","construction", 1.15m),
                        new RatingPlanDoc.Option("Factoring Companies","factoringcompanies", 1.35m),
                        new RatingPlanDoc.Option("Entertainment","entertainment", 1.15m),
                        new RatingPlanDoc.Option("Financial Institutions","financialinstitutions", 1.5m),
                        new RatingPlanDoc.Option("Health Care Orgs","healthcareorgs", 1.15m),
                        new RatingPlanDoc.Option("Insurance Companies","insurancecompanies", 1.35m),
                        new RatingPlanDoc.Option("Pension Funds","pensionfunds", 1.35m),
                        new RatingPlanDoc.Option("Professional Athletes","professionalathletes", 1.35m),
                        new RatingPlanDoc.Option("Real Estate Developer","realestatedeveloper", 1.25m),
                        new RatingPlanDoc.Option("Inions","unions", 1.5m),
                    }
                }

                #endregion 
            };


            #endregion

            return new RatingPlanDoc.RatingPlanForm()
            {
                rounding = true,
                maxRevenue = 2000000,
                minPremium = 500,
                basePremium = new RatingPlanDoc.BasePremium()
                {
                    incremetalRate = 1000
                },
                priorActsCoverages = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(1, 1.48m),
                    new RatingPlanDoc.YearFactor(2, 1.66m),
                    new RatingPlanDoc.YearFactor(3, 1.78m),
                    new RatingPlanDoc.YearFactor(4, 1.86m),
                    new RatingPlanDoc.YearFactor(5, 1.92m),
                    new RatingPlanDoc.YearFactor(6, 1.97m),
                    new RatingPlanDoc.YearFactor(7, 2),
                },
                businessManagement = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(5, 0.9m),
                    new RatingPlanDoc.YearFactor(10, 0.85m),
                },
                scheduleModifications = new RatingPlanDoc.ScheduleModifications()
                {
                    min = 0.75m,
                    max = 1.25m,
                    riskCaracteristics = new List<RatingPlanDoc.RiskCaracteristic>()
                    {
                        new RatingPlanDoc.RiskCaracteristic(AccountantsComputer.professionalMembershipsField , 0.85m, 1.25m),
                        new RatingPlanDoc.RiskCaracteristic(AccountantsComputer.businessManagementField, 0.85m, 1.25m),
                        new RatingPlanDoc.RiskCaracteristic(AccountantsComputer.claimsLastFiveField, 0.85m, 1.25m),
                    }
                },
                defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
                {
                    minPremium = 650,
                    factors = new List<RatingPlanDoc.DefenseOutsideLimitFactor>()
                    {
                        new RatingPlanDoc.DefenseOutsideLimitFactor("100000", 1.14m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("250000", 1.12m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("500000", 1.1m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("1000000", 1.09m),
                        new RatingPlanDoc.DefenseOutsideLimitFactor("2000000", 1.08m),
                    }
                },
                fields = fields
            };
        }

        [BeforeFeature]
        public static void GenerateRatingPlans()
        {
            #region AccountantsForms

            List<RatingPlanComputer> ratingPlanComputers = new List<RatingPlanComputer>();
            RatingPlanDoc.RatingPlanForm ratingPlanForm;

            #region Arizona, Lousiana, Massachussets, Maryland, New Jersey

            ratingPlanForm = CreateRatingPlanForm();
           
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(75000, 286, 0),
                new RatingPlanDoc.BasePremiumRange(500000, 286, 3.82m),
                new RatingPlanDoc.BasePremiumRange(750000, 1910, 2.87m),
                new RatingPlanDoc.BasePremiumRange(9999999, 2628, 2.15m),
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region Alabama, Arkansas, Colorado,Connecticut, District of Columbia, Delaware, Georgia, Iowa, Idaho, Ilinois, Indiana, Kansas, Kentucky, Maine, Michigan, Minessota, Missouri, Mississippi, Montana, North Carolina, North Dakota, Nevada, Nebraska, New Hampshire, New Mexico, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina, Soth Dakota, Tennesse, Utah, Virginia, Vermont, Wisconsin, West Virginia, Wyoming
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
            };
            ratingPlanForm.defenseOutsideLimit  = new RatingPlanDoc.DefenseOutsideLimit()
            {
                minPremium = 650
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region District of Columbia, Maine,  Virginia

            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.coverageField).FirstOrDefault());

            var newCoverage = new RatingPlanDoc.Field()
            {
                name = RatingPlanComputer.coverageField,
                type = "select",
                tooltip = "",
                options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("100,000","100000", 1) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.revenueField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("250,000","250000", 1.35m),
                        new RatingPlanDoc.Option("500,000","500000", 1.7m),
                        new RatingPlanDoc.Option("1 million","1000000", 2.15m),
                        new RatingPlanDoc.Option("2 millions","2000000", 2.5m),
                    }
            };

            ratingPlanForm.fields.Add(newCoverage);
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
            };
            ratingPlanForm.defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
            {
                minPremium = 650
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region Califórnia

            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.minPremium = 650;
            ratingPlanForm.basePremium = new RatingPlanDoc.BasePremium()
            {
                incremetalRate = 1000,
                ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 338, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 338, 4.51m),
                    new RatingPlanDoc.BasePremiumRange(750000, 2255, 3.38m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 3100, 2.54m),
                }
            };
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.coverageField).FirstOrDefault());
            ratingPlanForm.fields.Add(newCoverage);

            ratingPlanForm.defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
            {
                minPremium = 700,
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion
            
            #region Flórida

            ratingPlanForm.minPremium = 650;
            ratingPlanForm.basePremium = new RatingPlanDoc.BasePremium()
            {
                incremetalRate = 1000,
                ranges = new List<RatingPlanDoc.BasePremiumRange>()
                {
                    new RatingPlanDoc.BasePremiumRange(75000, 237, 0),
                    new RatingPlanDoc.BasePremiumRange(500000, 237, 3.16m),
                    new RatingPlanDoc.BasePremiumRange(750000, 1579, 2.37m),
                    new RatingPlanDoc.BasePremiumRange(9999999, 2170, 1.78m),
                }
            };
            ratingPlanForm.defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
            {
                minPremium = 700
            };

            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region Oklahoma

            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.coverageField).FirstOrDefault());      
            ratingPlanForm.fields.Add(newCoverage);

            var oklahomaDeductible = new RatingPlanDoc.Field()
            {
                name = RatingPlanComputer.deductibleField,
                type = "select",
                tooltip = "",
                options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("500","500", 1.15m) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.coverageField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("1,000","1000", 1.03m),
                        new RatingPlanDoc.Option("2,500","2500", 1m),
                        new RatingPlanDoc.Option("5,000","5000", 0.95m),
                        new RatingPlanDoc.Option("7,500","7500", 0.93m),
                        new RatingPlanDoc.Option("10,000","10000", 0.91m),
                    }
            };
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.deductibleField).FirstOrDefault());
            ratingPlanForm.fields.Add(oklahomaDeductible);

            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(75000, 260, 0),
                new RatingPlanDoc.BasePremiumRange(500000, 260, 3.47m),
                new RatingPlanDoc.BasePremiumRange(750000, 1735, 2.6m),
                new RatingPlanDoc.BasePremiumRange(9999999, 2385, 1.95m),
            };
            ratingPlanForm.defenseOutsideLimit = new RatingPlanDoc.DefenseOutsideLimit()
            {
                minPremium = 650
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region New York

            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(75000, 204, 0),
                new RatingPlanDoc.BasePremiumRange(500000, 204, 2.73m),
                new RatingPlanDoc.BasePremiumRange(750000, 1363, 2.04m),
                new RatingPlanDoc.BasePremiumRange(9999999, 1874, 1.53m),
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));
            
            #endregion

            #region Texas
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                 new RatingPlanDoc.BasePremiumRange(75000, 251, 0),
                 new RatingPlanDoc.BasePremiumRange(500000, 251, 3.35m),
                 new RatingPlanDoc.BasePremiumRange(750000, 1676, 2.52m),
                 new RatingPlanDoc.BasePremiumRange(9999999, 2306, 1.89m),
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #region Washington

            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                 new RatingPlanDoc.BasePremiumRange(75000, 273, 0),
                 new RatingPlanDoc.BasePremiumRange(500000, 273, 3.64m),
                 new RatingPlanDoc.BasePremiumRange(750000, 1820, 2.73m),
                 new RatingPlanDoc.BasePremiumRange(9999999, 2503, 2.05m),
            };
            ratingPlanComputers.Add(new AccountantsComputer(ratingPlanForm));

            #endregion

            #endregion
            foreach (var ratingPlan in ratingPlanComputers)
            {
                string key = ratingPlanComputerPrefixKey + ratingPlanComputers.IndexOf(ratingPlan);
                UpdateCache(key, ratingPlan);
            }
        }

        #region 1 Base Premium
        [Given(@"the total revenue (.*)")]
        public void GivenThe(decimal revenue)
        {
            CurrentCalculation.Revenue = revenue;
        }

        [When(@"try to calculate the Base Premium")]
        public void WhenTryToCalculateTheBasePremium()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium =  Computer.CalculateBasePremium(CurrentCalculation.Revenue);
                CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 2 Revenue Per Staff Credits
        private int _staffNumber;
        [Given(@"the staff number (.*)")]
        public void GivenTheStaffNumber(int staffNumber)
        {
            _staffNumber = staffNumber;
        }

        [When(@"try to calculate the Revenue Per Staff Credits")]
        public void WhenTryToCalculateTheRevenuePerStaffCredits()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateRevenueStaffCredits(CurrentCalculation.CurrentPremium, CurrentCalculation.Revenue, _staffNumber);
            });
        }


        #endregion

        #region 3 Prior Acts Coverage
        private int _pastYearsFromNow;
        [Given(@"past years from now (.*)")]
        public void GivenPastYearsFromNow(int pastYearsFromNow)
        {
            _pastYearsFromNow = pastYearsFromNow;
        }
        [When(@"try to calculate Prior Acts Coverage")]
        public void WhenTryToCalculatePriorActsCoverage()
        {
            TryCatch(() =>
            {
                var existingPolicyStartingDate = GetDateFromYears(_pastYearsFromNow);
                if(existingPolicyStartingDate != null) CurrentCalculation.CurrentPremium = Computer.CalculatePriorActsCoverage(CurrentCalculation.CurrentPremium, existingPolicyStartingDate.GetValueOrDefault());
            });
        }

        #endregion

        #region 4 Clients
        private string _revenueFromIndustries;
        [Given(@"the clients (.*)")]
        public void GivenTheClients(string revenueFromIndustries)
        {
            _revenueFromIndustries = revenueFromIndustries;
        }

        [When(@"try to calculate the Client Factors")]
        public void WhenTryToCalculateTheClientFactors()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateHighestFactor(CurrentCalculation.CurrentPremium, AccountantsComputer.revenueFromIndustriesField, _revenueFromIndustries);
                if (Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 5 Premium Modification Factors
        private string _revenueFromServices;
        private string _revenueFromSegments;
        [Given(@"premium modificators (.*), (.*), (.*)")]
        public void GivenPracticeModificators(string writtenContracts, string revenueFromServices, string revenueFromSegments)
        {
            _revenueFromServices = revenueFromServices;
            _revenueFromSegments = revenueFromSegments;
        }

        [When(@"try to calculate Premium Modification Factors")]
        public void WhenTryToCalculatePracticeModificationFactors()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculatePremiumModificationFactors(CurrentCalculation.CurrentPremium, _writtenContracts, _revenueFromSegments, _revenueFromServices);
                if (Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 6 Coverage
        [Given(@"the coverage (.*)")]
        public void GivenTheCoverage(string coverage)
        {
            CurrentCalculation.CoverageValue = coverage;
        }
        [When(@"try to calculate Coverage")]
        public void WhenTryToCalculateCoverage()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium = Computer.CalculateCoverage(CurrentCalculation.CurrentPremium, CurrentCalculation.CoverageValue, "");
            });
        }
        #endregion

        #region 7 Deductible
        private string _deductible;
        [Given(@"the deductible (.*)")]
        public void GivenTheDeductible(string deductible)
        {
            _deductible = deductible;
        }
        [When(@"try to calculate Deductible")]
        public void WhenTryToCalculateDeductible()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateDeductible(CurrentCalculation.CurrentPremium, _deductible);
            });
        }
        #endregion

        #region 8 Schedule Modifications
        private string _claimsLastFiveValue;
        private int _businessYears;
        private string _professionalMembershipsValue;
        [Given(@"the risks (.*), (.*), (.*)")]
        public void GivenTheRisks(string claimsLastFiveValue, int businessYears, string professionalMembershipsValue)
        {
            _claimsLastFiveValue = claimsLastFiveValue;
            _businessYears = businessYears;
            _professionalMembershipsValue = professionalMembershipsValue;
        }
        [When(@"try to calculate Schedule Modifications")]
        public void WhenTryToCalculateScheduleModifications()
        {
            TryCatch(() =>
            {
                var businessFoundationDate = GetDateFromYears(_businessYears).GetValueOrDefault();
                //CurrentCalculation.CurrentPremium = Computer.CalculateScheduleModifications(CurrentCalculation.CurrentPremium, businessFoundationDate, _professionalMembershipsValue);
                if (Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 9 Defense Outside Limit

        [Given(@"the dol option (.*)")]
        public void GivenTheDolOption(bool dol)
        {
            CurrentCalculation.DefenseOutsideLimit = dol;
        }

        [When(@"try to calculate Defense Outside Limit")]
        public void WhenTryToCalculateDefenseOutsideLimit()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateDefenseOutsideLimit(CurrentCalculation.CurrentPremium, CurrentCalculation.CoverageValue, CurrentCalculation.DefenseOutsideLimit);
            });
        }

        #endregion

        #region 10 Minimum Premium
        [When(@"try to checks the Minimum Premium")]
        public void WhenTryToChecksTheMinimumPremium()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.ChecksMinimumPremium(CurrentCalculation.CurrentPremium, CurrentCalculation.DefenseOutsideLimit);
            });
        }
        #endregion

        #region 11 Rounding and Final Premium
        [When(@"try to calculate the Final Premium then rounding him")]
        public void WhenTryToCalculateTheFinalPremiumThenRoundingHim()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium = Computer.RoundPremium(CurrentCalculation.CurrentPremium);
            });
        }

        #endregion        
    }
}
