﻿using TechTalk.SpecFlow;

namespace ProtectorUS.Test.Services.Steps
{
    using Model;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using RatingPlanDoc = Model.DocumentDB.RatingPlan;
    [Binding, Scope(Feature = "ArchitectsAndEngineersRatingPlan")]
    public sealed class ArchitectsAndEngineersRatingPlanSteps : RatingPlanSteps
    {
        private static RatingPlanDoc.RatingPlanForm CreateRatingPlanForm()
        {
            #region fields

            string totalClaimsLosses = "totalClaimsLosses";

            var engineersFields = new List<RatingPlanDoc.Field>()
            {                
                #region Revenue
  
                new RatingPlanDoc.Field()
                {
                    label = "My business revenue in the last 12 months was approximately US$",
                    name = RatingPlanComputer.revenueField,
                    type = "money",
                    placeholder = "Enter amount",
                    tooltip = ""
                },

                #endregion Base Premium Limit
                
                #region Prior Acts Coverage
                 new RatingPlanDoc.Field()
                {
                    labelAfter = "have an active Architects and Engineers Professional Liability coverage.",
                    name = RatingPlanComputer.existingPolicyField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do", "1"),
                        new RatingPlanDoc.Option("I do not", "0")
                    }
                },

                new RatingPlanDoc.Field()
                {
                    label = "The retroactive date on the current policy is",
                    name = RatingPlanComputer.retroactiveDateField,
                    type = "dataPicker",
                    tooltip = "",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(RatingPlanComputer.existingPolicyField, "1") { Operator = RatingPlanDoc.Condition.EqualsOperator }
                    }
                },

                #endregion Prio Acts Coverage               

                #region Deductible

                 new RatingPlanDoc.Field()
                {

                    label = "With a deductible of US$",
                    name = RatingPlanComputer.deductibleField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("0","0", 1.15m) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.coverageField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("1,000","1000", 1.08m),
                        new RatingPlanDoc.Option("2,500","2500", 1m),
                        new RatingPlanDoc.Option("5,000","5000", 0.94m),
                        new RatingPlanDoc.Option("7,500","7500", 0.9m),
                        new RatingPlanDoc.Option("10,000","10000", 0.86m),
                    }
                },

                #endregion Deductible Factors

                #region Coverage

                new RatingPlanDoc.Field()
                {

                    label = "I'd like Protector coverage of US$",
                    name = RatingPlanComputer.coverageField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("100,000","100000", 1) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.revenueField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("250,000","250000", 1.5m),
                        new RatingPlanDoc.Option("500,000","500000", 2),
                        new RatingPlanDoc.Option("750,000","750000", 2.2m),
                        new RatingPlanDoc.Option("1 million","1000000", 2.35m),
                        new RatingPlanDoc.Option("2 millions","2000000", 2.8m),
                    }
                },

                #endregion Increased Limit Factor

                #region Starting On

                new RatingPlanDoc.Field()
                {
                    label = "Starting on",
                    name = RatingPlanComputer.startingDateField,
                    type = "datePickerLimited",
                    tooltip = ""
                },

                #endregion 
                
                #region Schedule Modifications

                new RatingPlanDoc.Field()
                {
                    label = "My company has been in business since",
                    name = EngineersComputer.businessManagementField,
                    type = "monthPicker",
                    placeholder = "Click to select",
                    tooltip = ""
                },
                new RatingPlanDoc.Field()
                {
                    label = "I",
                    labelAfter = "a member of a professional society.",
                    name = EngineersComputer.professionalMembershipsField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I'm","1", 0.9m ),
                        new RatingPlanDoc.Option("I'm not","0", 1)
                    }
                },
                new RatingPlanDoc.Field()
                {

                    label = "I",
                    labelAfter = "claims in the last 5 years.",
                    name = EngineersComputer.claimsLastFiveField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("hadno","0", 0.9m),
                        new RatingPlanDoc.Option("hadone","1") { isDeclined = true },
                        new RatingPlanDoc.Option("hadtwo","2") { isDeclined = true },
                        new RatingPlanDoc.Option("hadtreeormore","3") { isDeclined = true },
                    }
                },
                new RatingPlanDoc.Field()
                {
                    label = "These claims resulted in total losses of US$",
                    name = totalClaimsLosses,
                    type = "money",
                    placeholder = "Type amount",
                    tooltip = "",
                    conditionals = new List<RatingPlanDoc.Condition>()
                    {
                        new RatingPlanDoc.Condition(EngineersComputer.claimsLastFiveField, "0") {Operator = RatingPlanDoc.Condition.DifferentOperator }
                    }
                },

                #endregion Schedule Modifications

                #region First Dollar Defense

                new RatingPlanDoc.Field()
                {
                    label = "I",
                    labelAfter = "additional First dollar defense for claim expenses to my policy.",
                    name = EngineersComputer.firstDollarDefenseField,
                    type = "select",
                    tooltip = "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("want","1"),
                        new RatingPlanDoc.Option("don't want","0"),
                    }
                },

                #endregion Defense Outside Limits
                
                #region Practice Modification Factors
                new RatingPlanDoc.Field()
                {
                    labelAfter = "provide any of the following services: aerospace, geotechnical, marine, nuclear or petrochemical engineering, product design, real estate development, superfund/ environmental remediation work, attorney, audits of SEC registrants, or audits of private firms with revenues or assets higher than US$ 25 million.",
                    name = EngineersComputer.revenueFromServicesField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do","1"){isDeclined = true },
                        new RatingPlanDoc.Option("I do not","0", 1)
                    }
                },
                new RatingPlanDoc.Field()
                {
                    labelAfter = "75% of my business revenue was derived from: acoustical, architecture, civil, electrical, communications, forensic, mechanical, process or traffic engineering; landscape or golf course architecture, interior design or unlicensed design such as kitchen & bath, exhibit & trade show, lighting, etc. (excluding for product design) in the last 12 months.",
                    name = EngineersComputer.revenueFromSegmentsField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("At Least","atleast", 1),
                        new RatingPlanDoc.Option("Less Than","lessthan") { isDeclined = true }
                    }
                },
                new RatingPlanDoc.Field()
                {
                    labelAfter = "provide Structural or HVAC services that account for over 50% of my revenue.",
                    name = EngineersComputer.revenueFromHVACField,
                    type = "select",
                    tooltip= "",
                    options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("I do","1", 1.75m),
                        new RatingPlanDoc.Option("I do not","0")
                    }
                },

                #endregion Decline 
            };


            #endregion engineersFields

            return new RatingPlanDoc.RatingPlanForm()
            {
                rounding = true,
                maxRevenue = 2000000,
                minPremium = 1400,
                basePremium = new RatingPlanDoc.BasePremium()
                {
                    incremetalRate = 100
                },
                priorActsCoverages = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(0, 0.8m),
                    new RatingPlanDoc.YearFactor(1, 0.9m),
                    new RatingPlanDoc.YearFactor(2, 0.95m),
                    new RatingPlanDoc.YearFactor(3, 0.975m),
                    new RatingPlanDoc.YearFactor(4, 1),
                },
                businessManagement = new List<RatingPlanDoc.YearFactor>()
                {
                    new RatingPlanDoc.YearFactor(5, 0.9m),
                    new RatingPlanDoc.YearFactor(10, 0.85m),
                },

                scheduleModifications = new RatingPlanDoc.ScheduleModifications()
                {
                    min = 0.75m,
                    max = 1.25m,
                    riskCaracteristics = new List<RatingPlanDoc.RiskCaracteristic>()
                    {
                        new RatingPlanDoc.RiskCaracteristic(EngineersComputer.professionalMembershipsField , 0.85m, 1.25m),
                        new RatingPlanDoc.RiskCaracteristic(EngineersComputer.businessManagementField, 0.85m, 1.25m),
                        new RatingPlanDoc.RiskCaracteristic(EngineersComputer.claimsLastFiveField, 0.85m, 1.25m),
                    }
                },
                firstDollarDefense = new RatingPlanDoc.FirstDollarDefense()
                {
                    minEndorsementPremium = 300,
                    factors = new List<RatingPlanDoc.FirstDollarDefenseFactor>()
                    {
                        new RatingPlanDoc.FirstDollarDefenseFactor("0", 0.0m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("1000", 0.03m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("2500", 0.05m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("5000", 0.07m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("7500", 0.08m),
                        new RatingPlanDoc.FirstDollarDefenseFactor("10000", 0.09m),
                    }
                },
                fields = engineersFields
            };
        }
        [BeforeFeature]
        public static void GenerateRatingPlans()
        {
            #region EngineersForms
            List<RatingPlanComputer> ratingPlanComputers = new List<RatingPlanComputer>();
            RatingPlanDoc.RatingPlanForm ratingPlanForm;

            #region Alabama
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1650, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1650, 1.7m),
                new RatingPlanDoc.BasePremiumRange(500000, 4026, 1.1m),
                new RatingPlanDoc.BasePremiumRange(750000, 6966 , 0.5m),
                new RatingPlanDoc.BasePremiumRange(1000000, 5375, 0.55m),
                new RatingPlanDoc.BasePremiumRange(1500000, 8226, 0.48m),
                new RatingPlanDoc.BasePremiumRange(1500000, 9426, 0.38m),
                new RatingPlanDoc.BasePremiumRange(2000000, 11346, 0.38m),

                new RatingPlanDoc.BasePremiumRange(9999999, 13266, 0.38m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Arkansas, Arizona, Colorado, Connecticut, District of Columbia, Delaware, Iowa, Idaho, Kentucky, Mariland, Michigan, South Carolina, Texas, Wisconsin, Wyoming
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1375, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1375, 1.42m),
                new RatingPlanDoc.BasePremiumRange(500000, 3505, 0.92m),
                new RatingPlanDoc.BasePremiumRange(750000, 5805, 0.42m),
                new RatingPlanDoc.BasePremiumRange(1000000, 6855, 0.4m),
                new RatingPlanDoc.BasePremiumRange(1500000, 7855, 0.32m),
                new RatingPlanDoc.BasePremiumRange(2000000, 9455, 0.32m),
                new RatingPlanDoc.BasePremiumRange(99999999, 11055, 0.32m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region California
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1400, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1400, 1.71m),
                new RatingPlanDoc.BasePremiumRange(500000, 3965, 0.70m),
                new RatingPlanDoc.BasePremiumRange(750000, 6740, 0.506m),
                new RatingPlanDoc.BasePremiumRange(1000000, 8005, 0.484m),
                new RatingPlanDoc.BasePremiumRange(1500000, 9215, 0.385m),
                new RatingPlanDoc.BasePremiumRange(2000000, 11140, 0.385m),
                new RatingPlanDoc.BasePremiumRange(99999999, 13065, 0.385m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Florida
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.minPremium = 750;
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1441, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1441, 1.49m),
                new RatingPlanDoc.BasePremiumRange(500000, 3679, 0.96m),
                new RatingPlanDoc.BasePremiumRange(750000, 6079, 0.44m),
                new RatingPlanDoc.BasePremiumRange(1000000, 7179, 0.42m),
                new RatingPlanDoc.BasePremiumRange(1500000, 8229, 0.34m),
                new RatingPlanDoc.BasePremiumRange(2000000, 9929, 0.34m),
                new RatingPlanDoc.BasePremiumRange(99999999, 11629, 0.34m),
            };

            var floridaCoverages = new RatingPlanDoc.Field()
            {

                label = "I'd like Protector coverage of US$",
                name = RatingPlanComputer.coverageField,
                type = "select",
                tooltip = "",
                options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("100,000","100000", 1) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.revenueField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("250,000","250000", 1.5m),
                        new RatingPlanDoc.Option("500,000","500000", 2),
                        new RatingPlanDoc.Option("750,000","750000", 2.2m),
                        new RatingPlanDoc.Option("1 million","1000000", 2.50m),
                        new RatingPlanDoc.Option("2 millions","2000000", 2.91m),
                    }
            };
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.coverageField).FirstOrDefault());
            ratingPlanForm.fields.Add(floridaCoverages);
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Georgia,  Ilinois,  Indiana, Missouri, New Jersey, Nevada, Tennesse, Vermont, Lousiana
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1375, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1375, 1.42m),
                new RatingPlanDoc.BasePremiumRange(500000, 3505, 0.92m),
                new RatingPlanDoc.BasePremiumRange(750000, 5805, 0.42m),
                new RatingPlanDoc.BasePremiumRange(1000000, 6855, 0.4m),
                new RatingPlanDoc.BasePremiumRange(1500000, 7855, 0.32m),
                new RatingPlanDoc.BasePremiumRange(2000000, 9455, 0.32m),
                new RatingPlanDoc.BasePremiumRange(99999999, 11055, 0.32m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Kansas, Massachusetts, Maine, Minnesota, Mississippi, Montana, North Carolina, North Dakota, Nebraska, New Hampshire, New Mexico, Ohio, Oklahoma, Oregon, Rhode Island, South Dakota, Utah, Virginia, West Virginia
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1375, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1375, 1.42m),
                new RatingPlanDoc.BasePremiumRange(500000, 3505, 0.92m),
                new RatingPlanDoc.BasePremiumRange(750000, 5805, 0.42m),
                new RatingPlanDoc.BasePremiumRange(1000000, 6855, 0.4m),
                new RatingPlanDoc.BasePremiumRange(1500000, 7855, 0.32m),
                new RatingPlanDoc.BasePremiumRange(2000000, 9455, 0.32m),
                new RatingPlanDoc.BasePremiumRange(99999999, 11055, 0.32m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Nova York
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1316, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1316, 1.357m),
                new RatingPlanDoc.BasePremiumRange(500000, 3351, 0.875m),
                new RatingPlanDoc.BasePremiumRange(750000, 5538, 0.4m),
                new RatingPlanDoc.BasePremiumRange(1000000, 6583 , 0.385m),
                new RatingPlanDoc.BasePremiumRange(1500000, 7500, 0.307m),
                new RatingPlanDoc.BasePremiumRange(2000000, 9035, 0.307m),
                new RatingPlanDoc.BasePremiumRange(999999999, 10570, 0.307m),
            };


            var newYorkDeductible = new RatingPlanDoc.Field()
            {
                name = RatingPlanComputer.deductibleField,
                type = "select",
                tooltip = "",
                options = new List<RatingPlanDoc.Option>()
                    {
                        new RatingPlanDoc.Option("500","500", 1.15m) {
                            conditionals = new List<RatingPlanDoc.Condition>() {
                                new RatingPlanDoc.Condition(RatingPlanComputer.coverageField,"500000") {Operator = RatingPlanDoc.Condition.SmallerAndEqualOperator }
                            }
                        },
                        new RatingPlanDoc.Option("1,000","1000", 1.14m),
                        new RatingPlanDoc.Option("2,500","2500", 1.07m),
                        new RatingPlanDoc.Option("5,000","5000", 1.03m),
                        new RatingPlanDoc.Option("7,500","7500", 1.01m),
                        new RatingPlanDoc.Option("10,000","10000", 0.96m),
                    }
            };
            ratingPlanForm.fields.Remove(ratingPlanForm.fields.Where(x => x.name == RatingPlanComputer.deductibleField).FirstOrDefault());
            ratingPlanForm.fields.Add(newYorkDeductible);
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Pennsylvania
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1650, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1650, 1.7m),
                new RatingPlanDoc.BasePremiumRange(500000, 4206, 1.1m),
                new RatingPlanDoc.BasePremiumRange(750000, 6966, 0.5m),
                new RatingPlanDoc.BasePremiumRange(1000000, 8226, 0.48m),
                new RatingPlanDoc.BasePremiumRange(1500000, 9426, 0.38m),
                new RatingPlanDoc.BasePremiumRange(2000000, 11346, 0.38m),
                new RatingPlanDoc.BasePremiumRange(99999999, 13266, 0.38m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #region Washington
            ratingPlanForm = CreateRatingPlanForm();
            ratingPlanForm.basePremium.ranges = new List<RatingPlanDoc.BasePremiumRange>()
            {
                new RatingPlanDoc.BasePremiumRange(100000, 1365, 0),
                new RatingPlanDoc.BasePremiumRange(250000, 1365, 1.26m),
                new RatingPlanDoc.BasePremiumRange(500000, 3255, 0.88m),
                new RatingPlanDoc.BasePremiumRange(750000, 5455, 0.82m),
                new RatingPlanDoc.BasePremiumRange(1000000, 7505, 0.76m),
                new RatingPlanDoc.BasePremiumRange(1500000, 9405, 0.71m),
                new RatingPlanDoc.BasePremiumRange(2000000, 12955, 0.67m),
                new RatingPlanDoc.BasePremiumRange(2500000, 16305, 0.65m),
                new RatingPlanDoc.BasePremiumRange(999999999, 19555, 0.6m),
            };
            ratingPlanComputers.Add(new EngineersComputer(ratingPlanForm));

            #endregion

            #endregion            

            foreach (var ratingPlan in ratingPlanComputers)
            {
                string key = ratingPlanComputerPrefixKey + ratingPlanComputers.IndexOf(ratingPlan);
                UpdateCache(key, ratingPlan);
            }
        }

        #region 1 Base Premium
        [Given(@"the total revenue (.*)")]
        public void GivenThe(decimal revenue)
        {
            CurrentCalculation.Revenue = revenue;
        }

        [When(@"try to calculate the Base Premium")]
        public void WhenTryToCalculateTheBasePremium()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium =  Computer.CalculateBasePremium(CurrentCalculation.Revenue);
                CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 2 Structural and HVAC Areas
        private string _revenueFromHVAC;
        [Given(@"the revenueFromHVAC (.*)")]
        public void GivenTheRevenueFromHVAC(string revenueFromHVAC)
        {
            _revenueFromHVAC = revenueFromHVAC;
        }

        [When(@"try to calculate the Structural and HVAC Areas")]
        public void WhenTryToCalculateTheStructuralAndHVACAreas()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium = Computer.CalculateCheckingDecline(CurrentCalculation.CurrentPremium, EngineersComputer.revenueFromHVACField, _revenueFromHVAC);
                if (Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 3 Prior Acts Coverage
        private int _pastYearsFromNow;
        [Given(@"past years from now (.*)")]
        public void GivenPastYearsFromNow(int pastYearsFromNow)
        {
            _pastYearsFromNow = pastYearsFromNow;
        }
        [When(@"try to calculate Prior Acts Coverage")]
        public void WhenTryToCalculatePriorActsCoverage()
        {
            TryCatch(() =>
            {
                var retroactiveDate = GetDateFromYears(_pastYearsFromNow);
                CurrentCalculation.CurrentPremium = Computer.CalculatePriorActsCoverage(CurrentCalculation.CurrentPremium, retroactiveDate);
            });
        }

        #endregion

        #region 4 Premium Modification Factors
        private string _revenueFromServices;
        private string _revenueFromSegments;
        [Given(@"practice modificators (.*), (.*), (.*)")]
        public void GivenPremiumModificators(string writtenContracts, string revenueFromServices, string revenueFromSegments)
        {
            _revenueFromServices = revenueFromServices;
            _revenueFromSegments = revenueFromSegments;
        }
        [When(@"try to calculate Premium Modification Factors")]
        public void WhenTryToCalculatePremiumModificationFactors()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculatePremiumModificationFactors(CurrentCalculation.CurrentPremium, _writtenContracts, _revenueFromSegments, _revenueFromServices);
                if (Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion
        
        #region 5 Coverage
        [Given(@"the coverage (.*)")]
        public void GivenTheCoverage(string coverage)
        {
            CurrentCalculation.CoverageValue = coverage;
        }
        [When(@"try to calculate Coverage")]
        public void WhenTryToCalculateCoverage()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium = Computer.CalculateCoverage(CurrentCalculation.CurrentPremium, CurrentCalculation.CoverageValue, "");
            });
        }
        #endregion

        #region 6 Deductible
        [Given(@"the deductible (.*)")]
        public void GivenTheDeductible(string deductible)
        {
            CurrentCalculation.DeductibleValue = deductible;
        }
        [When(@"try to calculate Deductible")]
        public void WhenTryToCalculateDeductible()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateDeductible(CurrentCalculation.CurrentPremium, CurrentCalculation.DeductibleValue);
            });
        }
        #endregion

        #region 7 Schedule Modifications
        private string _claimsLastFiveValue;
        private int _businessYears;
        private string _professionalMembershipsValue;
        [Given(@"the risks (.*), (.*), (.*)")]
        public void GivenTheRisks(string claimsLastFiveValue, int businessYears, string professionalMembershipsValue)
        {
            _claimsLastFiveValue = claimsLastFiveValue;
            _businessYears = businessYears;
            _professionalMembershipsValue = professionalMembershipsValue;
        }
        [When(@"try to calculate Schedule Modifications")]
        public void WhenTryToCalculateScheduleModifications()
        {
            TryCatch(() =>
            {
                var businessFoundationDate = GetDateFromYears(_businessYears).GetValueOrDefault();
                //CurrentCalculation.CurrentPremium = Computer.CalculateScheduleModifications(CurrentCalculation.CurrentPremium, businessFoundationDate, _professionalMembershipsValue);
                if(Computer.IsDeclined)
                    CurrentCalculation.IsDeclined = Computer.IsDeclined;
            });
        }

        #endregion

        #region 8 First Dollar Defense
        private bool _fdd;
        [Given(@"the fdd option (.*)")]
        public void GivenTheFddOption(bool fdd)
        {
            _fdd = fdd;
        }
        [When(@"try to calculate First Dollar Defense")]
        public void WhenTryToCalculateFirstDollarDefense()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.CalculateFirstDollarDefense(CurrentCalculation.CurrentPremium, CurrentCalculation.DeductibleValue, _fdd);
            });
        }

        #endregion        

        #region 9 Minimum Premium
        [When(@"try to checks the Minimum Premium")]
        public void WhenTryToChecksTheMinimumPremium()
        {
            TryCatch(() =>
            {
                //CurrentCalculation.CurrentPremium = Computer.ChecksMinimumPremium(CurrentCalculation.CurrentPremium, CurrentCalculation.DefenseOutsideLimit);
            });
        }
        #endregion

        #region 10 Rounding and Final Premium
        [When(@"try to calculate the Final Premium then rounding him")]
        public void WhenTryToCalculateTheFinalPremiumThenRoundingHim()
        {
            TryCatch(() =>
            {
                CurrentCalculation.CurrentPremium = Computer.RoundPremium(CurrentCalculation.CurrentPremium);
            });
        }

        #endregion
    }
}
