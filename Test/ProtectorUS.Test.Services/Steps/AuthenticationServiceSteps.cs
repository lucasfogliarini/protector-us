﻿using Autofac;
using TechTalk.SpecFlow;
using System.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ProtectorUS.Test.Services.Steps
{
    using Model.Services;
    using Model;
    using Exceptions;
    using DTO;
    using Communication;
    [Binding, Scope(Feature = "AuthenticationService")]
    public sealed class AuthenticationServiceSteps : BaseSteps
    {
        private readonly IAuthenticationService _authenticationService;
        private User _user;
        private string _email;
        private string _password;
        private long _userId;
        private IEnumerable<OutputPermissionDTO> _permissions;
        private string _permission;
        private string _userType;

        public AuthenticationServiceSteps()
        {
            _authenticationService = container.Resolve<IAuthenticationService>();
        }
        [Given(@"a user with UserId and permission (.*), (.*)")]
        public void GivenAUserWithUserIdAndPermission(long userId, string permission)
        {
            _userId = userId;
            _permission = permission;
        }
        [When(@"i try to find the permissions passing the data")]
        public void WhenITryToFindThePermissionsPassingTheData()
        {
            TryCatch(() =>
            {
                _permissions = _authenticationService.FindPermissions(_userId);
            });
        }
        [Given(@"a UserType, Email and Password (.*), (.*), (.*)")]
        public void GivenAUserTypeEmailAndPassword(string userType, string email, string password)
        {
            _userType = userType;
            _email = email;
            _password = password;
        }
        [When(@"i try to authenticate the user passing the data")]
        public void WhenITryToAuthenticateTheUserPassingTheData()
        {
            TryCatch(() =>
            {
                if (_userType == typeof(Insured).Name)
                {
                    _user = _authenticationService.Authenticate<Insured>(_email, _password);
                }
                else if (_userType == typeof(Broker).Name)
                {
                    _user = _authenticationService.Authenticate<Broker>(_email, _password);
                }
                else
                {
                    _user = _authenticationService.Authenticate<ManagerUser>(_email, _password);
                }
            });
        }
        [Then(@"the user should be at least one profile")]
        public void ThenTheUserShouldBeAtLeastOneProfile()
        {
            Assert.IsTrue(_user.Has(e => e.Profiles), "OK - if greater or equal 1");
        }
        [Then(@"the result should be a message code saying that the password is incorrect")]
        public void ThenTheResultShouldBeAMessageCodeSayingThatThePasswordIsIncorrect()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.ES001));
        }
        [Then(@"the result should be a message code saying that the username and password can't be empty")]
        public void ThenTheResultShouldBeAMessageCodeSayingThatTheUsernameAndPasswordCanTBeEmpty()
        {
            Assert.IsTrue(_baseException?.MessageCode?.Code == nameof(MessageCode.MessageCodes.ES027));
        }        
        [Then(@"the return should be the User")]
        public void ThenTheReturnShouldBeTheUser()
        {
            Assert.IsInstanceOf<User>(_user);
        }
        [Then(@"the permission should NOT be in returned permissions")]
        public void ThenThePermissionShouldNOTBeInReturnedPermissions()
        {
            Assert.False(_permissions.Any(p => p.ActionApi == _permission));
        }

        [Then(@"the permission should be in returned permissions")]
        public void ThenThePermissionShouldBeInReturnedPermissions()
        {
            Assert.True(_permissions.Any(p => p.ActionApi == _permission));
        }

    }
}
