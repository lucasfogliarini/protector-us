﻿using Autofac;

namespace ProtectorUS.Test.Services
{
    using Data.IoC;
    using Data;
    using ProtectorUS.Services.IoC;
    using System.Data.Entity;
    using Diagnostics.IoC;
    using Model.Validators.IoC;
    using DTO;
    using Exceptions;
    using System;
    using Integrations.IoC;
    using System.Runtime.Caching;
    using System.Linq;
    public class BaseSteps
    {
        protected readonly IContainer container;
        protected Exception _exception;
        protected BaseException _baseException
        {
            get
            {
                if (_exception is AggregateException)
                {
                    var aggException = _exception as AggregateException;
                    _exception = aggException.InnerExceptions.FirstOrDefault(e => e is BaseException);
                }
                return _exception is BaseException ? _exception as BaseException : null;
            }
        }

        public BaseSteps()
        {
            if (container != null) return;
            
            var builder = new ContainerBuilder();
            builder.RegisterModule<DataConnectionModule>();
            builder.RegisterModule<RepositoriesModule>();
            builder.RegisterModule<ServicesModule>();
            builder.RegisterModule<DiagnosticsModule>();
            builder.RegisterModule<ValidatorsModule>();
            builder.RegisterModule<AutoMapperModule>();
            builder.RegisterModule<IntegrationServicesModule>();
            container = builder.Build();
        }

        public static T GetCache<T>(string key)
        {
            var item = MemoryCache.Default.Get(key);
            if (item != null && item is T)
            {
                return (T)item;
            }
            return default(T);
        }

        public static void UpdateCache(string key, object item)
        {
            if (MemoryCache.Default.Contains(key))
            {
                MemoryCache.Default[key] = item;
            }
            else
            {
                MemoryCache.Default.Add(key, item, DateTimeOffset.MaxValue);
            }
        }

        public void TryCatch(Action _try)
        {
            try
            {
                _try();
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }
    }
}
