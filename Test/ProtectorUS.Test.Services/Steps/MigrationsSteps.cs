﻿using TechTalk.SpecFlow;
using NUnit.Framework;

namespace ProtectorUS.Test.Services.Steps
{
    using Data;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Linq;
    [Binding, Scope(Feature = "_Migrations")]
    public sealed class MigrationsSteps : BaseSteps
    {
        public MigrationsSteps()
        {
            var protectorUSConfiguration = new Data.Migrations.Configuration();
            _localDbMigrator = new DbMigrator(protectorUSConfiguration);

            var stagingConnectionString = ConfigurationManager.ConnectionStrings[stagingConnectionStringName];
            protectorUSConfiguration.TargetDatabase = new DbConnectionInfo(stagingConnectionString.ConnectionString, stagingConnectionString.ProviderName);
            _devDbMigrator = new DbMigrator(protectorUSConfiguration);

            var productionConnectionString = ConfigurationManager.ConnectionStrings[productionConnectionStringName];
            protectorUSConfiguration.TargetDatabase = new DbConnectionInfo(productionConnectionString.ConnectionString, productionConnectionString.ProviderName);
            _productionDbMigrator = new DbMigrator(protectorUSConfiguration);
        }

        #region UpdateAndSeed
        const string MigrationPendingChangesMessage = "There are pending changes, and need generate a new migration. Make sure if you got migrations through VCS generated before your last migration. If so, you need to merge the two migrations using the 'add-migration merge-migrations -ignorechanges' command.";
        private ProtectorUSDatabase _protectorUSDatabase;

        //[Given(@"a ProtectorUSDatabase")]
        //public void GivenAProtectorUSDatabase()
        //{
        //    TryCatch(() =>
        //    {
        //        _protectorUSDatabase = new ProtectorUSDatabase();
        //    });
        //}
        [When(@"I try initialize the Database")]
        public void WhenITryInitializeTheDatabase()
        {
            TryCatch(() =>
            {
                _protectorUSDatabase = new ProtectorUSDatabase();
            });
        }
        [Then(@"everything must be ok")]
        public void ThenEverythingMustBeOk()
        {
            Assert.IsNotInstanceOf<AutomaticMigrationsDisabledException>(_exception, MigrationPendingChangesMessage);
            Assert.IsNull(_exception);
        }
        #endregion

        #region Check Migrations

        const string stagingConnectionStringName = "protectorUS_dev";
        const string productionConnectionStringName = "protectorUS_production";
        private readonly DbMigrator _localDbMigrator;
        private readonly DbMigrator _devDbMigrator;
        private readonly DbMigrator _productionDbMigrator;
        private IEnumerable<string> _DbMigrations;
        private IEnumerable<string> _localMigrations;
        private List<string> _migrationsNotFounded = new List<string>();

        [Given(@"all migrations from ProtectorUS Configuration")]
        public void GivenAllMigrationsFromProtectorUSConfiguration()
        {
            _localMigrations = _localDbMigrator.GetLocalMigrations();
        }

        [Given(@"all migrations from dev database")]
        public void GivenAllMigrationsFromDevDatabase()
        {
            _DbMigrations = _devDbMigrator.GetDatabaseMigrations();
        }

        [Given(@"all migrations from poduction database")]
        public void GivenAllMigrationsFromPoductionDatabase()
        {
            _DbMigrations = _productionDbMigrator.GetDatabaseMigrations();
        }

        [When(@"I check the db migrations in local migrations")]
        public void WhenICheckTheDbMigrationsInLocalMigrations()
        {
            TryCatch(() =>
            {
                foreach (string dbMigration in _DbMigrations)
                {
                    bool exists = _localMigrations.Contains(dbMigration);
                    if (!exists) _migrationsNotFounded.Add(dbMigration);
                }
            });
        }

        [Then(@"all migrations should be checked")]
        public void ThenAllMigrationsShouldBeChecked()
        {
            var migrationsNotFounded = _migrationsNotFounded.Join(", ");
            Assert.True(!_migrationsNotFounded.Any(), "Not founded: " + migrationsNotFounded);
        }
        #endregion
    }
}
