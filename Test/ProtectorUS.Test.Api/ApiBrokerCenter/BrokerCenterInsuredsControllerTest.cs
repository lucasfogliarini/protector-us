﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    [TestClass]
    public class BrokerCenterInsuredsControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "insureds";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetAll()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetInsured()
        {
            SetResourceId("206");
            var response = await GetAsync(_URL, Users.Broker);
            OutputInsuredDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputInsuredDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetClaims()
        {
            SetResourceId("206");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetEndorsements()
        {
            SetResourceId("206");
            SetMethodExtraRoute("endorsements");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalTypes()
        {
            SetMethodRoute("graphic/types");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

