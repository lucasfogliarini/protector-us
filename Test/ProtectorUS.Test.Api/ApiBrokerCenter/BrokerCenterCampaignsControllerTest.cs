﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class BrokerCenterCampaignsControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "campaigns";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetAll()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        //[TestMethod, TestCategory("API")]
        //public async Task BrokerCenterGetCampaign()
        //{
        //}

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostCampaign()
        {
            InputBrochureDTO input = new InputBrochureDTO
            {
                TemplateType =  Model.BrochureTemplateType.Accountants1
            };
            var response = await PostAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalbyType()
        {
            SetMethodRoute("graphic/total");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
 
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalbyTypeView()
        {
            SetMethodRoute("graphic/resume/total");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalbyTypePeriod()
        {
            SetMethodRoute("graphic/resume/total/period");
            SetMethodParams("period", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostGenerationImageTop()
        {
            var dic = new Dictionary<string,string>();
            dic.Add("phrase", "fdsuafhsudfhasuh");
            dic.Add("imageBackground", "images/06c0eeea3dc147358c56ff0d190686c4.jpg");
            SetMethodRoute("images/header/mailmarketing");
            SetMethodParams(dic);
            var response = await PostAsync(_URL, null, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

