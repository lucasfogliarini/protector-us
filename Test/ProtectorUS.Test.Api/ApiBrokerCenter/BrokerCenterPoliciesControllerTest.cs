﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class BrokerCenterPoliciesControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "policies";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetAll()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetPolicy()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.Broker);
            OutputPolicyDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputPolicyDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetOrders()
        {
            SetResourceId("1");
            SetMethodExtraRoute("orders");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetEndorsements()
        {
            SetResourceId("1");
            SetMethodExtraRoute("endorsements");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetClaims()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetDashboardTypes()
        {
            SetMethodRoute("graphic/featured/types");
            var dic = new Dictionary<string, string>();
            dic.Add("intervaldays", "1");
            dic.Add("typeview", "1");
            SetMethodParams(dic);
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalInterval()
        {
            SetMethodRoute("graphic/total/interval");
            SetMethodParams("interval", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalGeneral()
        {
            SetMethodRoute("graphic/total/general");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalTypes()
        {
            SetMethodRoute("graphic/types");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetRenewalsTotal()
        {
            SetMethodRoute("graphic/renewals/total");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostCreateClaim()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            InputClaimDTO input = new InputClaimDTO()
            {
                ClaimDate = System.DateTime.Now,
                DescriptionFacts = "asdhfudsahf",
                OccurrenceDate = System.DateTime.Now.AddDays(-1),
                PriorNotification = true,
            };
            var response = await PostAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

