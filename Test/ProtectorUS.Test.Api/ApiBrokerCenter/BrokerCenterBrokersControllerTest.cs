﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    [TestClass]
    public class BrokerCenterBrokersControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "brokers";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetAll()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetBroker()
        {
            SetResourceId("134");
            var response = await GetAsync(_URL, Users.Broker);
            OutputProfileBrokerDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputProfileBrokerDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutBroker()
        {
            BrokerUpdateDTO input = new BrokerUpdateDTO
            {
                FirstName = "Troy",
                LastName = "Chavez",
                Email = "troy_master@brokerage3.com",
            };
            SetResourceId("134");
            var response = await PutAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostBroker()
        {
            BrokerInsertDTO input = new BrokerInsertDTO
            {
                FirstName = "NewBroker",
                LastName = "Admin",
                Email = "newbrokeradmin@brokerage3.com",
                ProfileId = 8
            };
            var response = await PostAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterDeleteBroker()
        {
            SetResourceId("134");
            var response = await DeleteAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalStatus()
        {
            SetMethodRoute("graphic/status");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
 
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalLastMonth()
        {
            SetMethodRoute("graphic/lastMonth");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetInactivate()
        {
            SetMethodRoute("inactivate");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

