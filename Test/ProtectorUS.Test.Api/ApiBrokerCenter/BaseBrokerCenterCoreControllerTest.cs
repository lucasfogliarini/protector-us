﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ProtectorUS.Test.Api.BrokerCenter
{
    [TestClass]
    public abstract class BaseBrokerCenterCoreControllerTest : BaseControllerTest
    {

        protected override void SetApiRoute()
        {
            _baseURL = TestApiConfiguration.ApiElements.FirstOrDefault(_element => _element.Type == "BrokerCenter").Url;
        }

    }
}
