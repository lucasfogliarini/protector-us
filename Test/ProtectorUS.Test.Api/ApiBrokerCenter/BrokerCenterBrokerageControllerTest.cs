﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    using System.Net.Http.Headers;
    
    [TestClass]
    public class BrokerCenterBrokerageControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "brokerage";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetOnboarding()
        {
            SetMethodRoute("onboarding");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutBrokerage()
        {
            InputBrokerageUpdateDTO input = new InputBrokerageUpdateDTO
            {
                Alias = "brokerage3",
                Name = "Brokerage 3",
                Email = "contact@brokerage3.com",
                Address = new AddressDTO()
                {
                    StreetLine1 = "stl1",
                    StreetLine2 = "stl2",
                    RegionId = 1
                },
                Website = "www.brokerage3.com"
            };

            var response = await PutAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }        

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetBrokerage()
        {
            OutputBrokerageDTO responseOutput = null;
            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputBrokerageDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTestAlias()
        {
            SetMethodRoute("test");
            SetResourceId("brokeragexyz");
            SetMethodExtraRoute("alias");
            var response = await GetAsync(_URL, Users.Broker);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutDefaultWebpage()
        {
            SetMethodRoute("defaultWebpage");
            BrokerageDefaultWebPageDTO input = new BrokerageDefaultWebPageDTO
            {
                Cover = "cover",
                Logo = "logo"
            };

            var response = await PutAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }        
    }
}

