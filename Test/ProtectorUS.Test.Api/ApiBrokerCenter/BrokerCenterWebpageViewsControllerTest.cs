﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class BrokerCenterWebpageViewsControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "webpageviews";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalInterval()
        {
            SetMethodRoute("graphic/total/interval");
            SetMethodParams("period", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalWeek()
        {
            SetMethodRoute("graphic/total/week");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalWebBanner()
        {
            SetMethodRoute("graphic/total/webbanners");
            SetMethodParams("period", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

