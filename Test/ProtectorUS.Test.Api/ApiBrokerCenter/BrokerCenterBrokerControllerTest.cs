﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    [TestClass]
    public class BrokerCenterBrokerControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "broker";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetOnboarding()
        {
            SetMethodRoute("onboarding");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetBroker()
        {
            OutputProfileBrokerDTO responseOutput = null;
            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputProfileBrokerDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTestEmail()
        {
            SetMethodRoute("test");
            SetResourceId("troy_master@brokerage3.com");
            SetMethodExtraRoute("email");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutBroker()
        {
            BrokerSelfUpdateDTO input = new BrokerSelfUpdateDTO
            {
                FirstName = "Troy",
                LastName = "Chavez",
                Email = "troy_master@brokerage3.com",
            };

            var response = await PutAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }        

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutChangePassword()
        {
            SetMethodRoute("changePassword");
            SetMethodParams("password", "brokeruser");

            var response = await PutAsync(_URL, null, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetResetPasswordSendInstructions()
        {
            SetResourceId("barbara_master@brokerage3.com");
            SetMethodExtraRoute("resetPassword/sendInstructions");

            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostResetPassword()
        {
            SetResourceId("134");
            SetMethodExtraRoute("resetPassword");
            SetMethodParams("token", "missing");
            var response = await PostAsync(_URL, null, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetUserLogin()
        {
            SetResourceId("barbara_master@brokerage3.com");
            SetMethodExtraRoute("userLogin");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

