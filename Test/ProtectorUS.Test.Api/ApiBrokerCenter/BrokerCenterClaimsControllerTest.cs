﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.BrokerCenter
{

    using DTO;
    [TestClass]
    public class BrokerCenterClaimsControllerTest : BaseBrokerCenterCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "claims";
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetAll()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetClaim()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.Broker);
            OutputClaimDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputClaimDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPostAttachDocuments()
        {
            SetResourceId("1");
            SetMethodExtraRoute("documents");
            var documents = new[] {"document1.doc","document2.doc" };
            var response = await PostAsync(_URL, documents, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterDeleteDocument()
        {
            SetResourceId("1");
            SetMethodExtraRoute("documents");
            SetExtraResourceId("3");
            var response = await DeleteAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterPutChangeStatus()
        {
            SetResourceId("1");
            SetMethodExtraRoute("status");
            SetExtraResourceId("1");
            var response = await PutAsync(_URL, null, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetTotalInterval()
        {
            SetMethodRoute("graphic/total/interval");
            SetMethodParams("interval", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task BrokerCenterGetMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

    }
}

