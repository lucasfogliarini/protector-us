﻿using System.Linq;
namespace ProtectorUS.Test.Api
{
    public static class Users
    {

        private static TestApiConfiguration _config = TestApiConfiguration.GetConfig();

        public static UserCredentials Insured
        {
            get
            {
                var insured = TestApiConfiguration.UserElements.FirstOrDefault(_element => _element.Type == "Insured");
                return new UserCredentials(insured.Username, insured.Password, insured.GetUserType);
            }
        }

        public static UserCredentials Broker
        {
            get
            {
                var broker = TestApiConfiguration.UserElements.FirstOrDefault(_element => _element.Type == "Broker");
                return new UserCredentials(broker.Username, broker.Password, broker.GetUserType);
            }
        }
        public static UserCredentials ManagerUser
        {
            get
            {
                var managerUser = TestApiConfiguration.UserElements.FirstOrDefault(_element => _element.Type == "ManagerUser");
                return new UserCredentials(managerUser.Username, managerUser.Password, managerUser.GetUserType);
            }
        }

    }

    public struct UserCredentials
    {
        public string Username;
        public string Password;
        public UserType Type;
        public UserCredentials(string username, string password, UserType type)
        {
            Username = username;
            Password = password;
            Type = type;
        }
    }

    public enum UserType
    {
        Broker,
        ManagerUser,
        Insured
    }

}
