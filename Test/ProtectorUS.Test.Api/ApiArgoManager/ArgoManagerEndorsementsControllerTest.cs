﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    [TestClass]
    public class ArgoManagerEndorsementsControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "endorsements";
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllEndorsements()
        {
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]//Ignore because this test only fit one time, accept only one reply.
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutEndorsement1Reply()
        {
            SetResourceId("1");
            SetMethodRoute("reply");
            SetExtraResourceId("true");
            var response = await PutAsync(_URL, null, Users.ManagerUser);
            OutputEndorsementDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputEndorsementDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }    
    }
}

