﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ProtectorUS.Test.Api.ArgoManager
{
    [TestClass]
    public abstract class BaseArgoManagerControllerTest : BaseControllerTest
    {

        protected override void SetApiRoute()
        {
            _baseURL = TestApiConfiguration.ApiElements.FirstOrDefault(_element => _element.Type == "ArgoManager").Url;
        }

    }
}
