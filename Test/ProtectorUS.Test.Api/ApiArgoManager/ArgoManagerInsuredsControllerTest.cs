﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    [TestClass]
    public class ArgoManagerInsuredsControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "insureds";
        }
        #region CRUD

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllInsureds()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetInsured206()
        {
            SetResourceId("206");
            var response = await GetAsync(_URL, Users.Broker);
            OutputInsuredDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputInsuredDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        #endregion

        #region Insureds Lists  

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetInsured206Claims()
        {
            SetResourceId("206");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetInsured206Endorsements()
        {
            SetResourceId("206");
            SetMethodExtraRoute("endorsements");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region MapPoints

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetInsuredsMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region Graphic&Numbers

        [TestMethod, TestCategory("API")]
        public async Task ArgoManageGetInsuredsTotalTypes()
        {
            SetMethodRoute("graphic/types");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetInsuredsTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion
    }
}

