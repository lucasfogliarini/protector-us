﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;

    /// <summary>
    /// Test class for ManagerUsersController
    /// </summary>
    [TestClass]
    public class ArgoManagerManagerUsersControllerTest : BaseArgoManagerControllerTest
    {
        private const string managerUserCreated = "managerUserCreated";
        protected override void SetControllerRoute()
        {
            _controllerRoute = "managerUsers";
        }

        [Ignore] //Run only one time because manager user has a email unique key
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostManagerUser()
        {
           var input = new ManagerUserInsertDTO
           {
              Email = "teste_manageruser@manageruser.com",
              FirstName = "New ManagerUser FirstName",
              LastName = "New ManagerUser LastName",
              ProfileId = 1,
              CompanyId = 1
            };

            var response = await PostAsync(_URL, new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"), Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutManagerUser5()
        {
            SetResourceId("5");

            var input = new ManagerUserInsertDTO
            {
                Email = "updatemanageruser@manageruser.com",
                FirstName = "ManagerUser FirstName",
                LastName = "ManagerUser LastName",
                ProfileId = 1,
                CompanyId = 1
            };

            var response = await PutAsync(_URL, input, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetManagerUser2()
        {
            SetResourceId("2");
            OutputManagerUserDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputManagerUserDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

      

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAlltManagerUsers()
        {
            List<OutputManagerUserDTO> responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<List<OutputManagerUserDTO>>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerDeleteManagerUser6()
        {
            SetResourceId("6");
            var response = await DeleteAsync(_URL,Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

    }
}

