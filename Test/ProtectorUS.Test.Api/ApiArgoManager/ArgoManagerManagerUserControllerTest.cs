﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;

    /// <summary>
    /// Test class for ManagerUserController
    /// </summary>
    [TestClass]
    public class ArgoManagerManagerUserControllerTest : BaseArgoManagerControllerTest
    {
        private const string ManagerUserCreated = "ManagerUserCreated";
        protected override void SetControllerRoute()
        {
            _controllerRoute = "manageruser";
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetManagerUser()
        {
            OutputManagerUserDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputManagerUserDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetOnboardingManagerUser()
        {
            SetMethodRoute("onboarding");
            var response = await GetAsync(_URL, new UserCredentials {
                Username = "onboardinguser@manageruser.com",
                Password = "manageruser",
                Type = UserType.ManagerUser
            });
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutManagerUser()
        {
            ManagerUserSelfUpdateDTO input = new   ManagerUserSelfUpdateDTO
            {
                FirstName = "Ethan",
                LastName = "Wilhelm",
                Email = "admin@manageruser1.com",
            };

            var response = await PutAsync(_URL, input, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutManagerUserChangePassword()
        {
            SetMethodRoute("changePassword");
            SetMethodParams("password", "manageruser");

            var response = await PutAsync(_URL, null, new UserCredentials
            {
                Username = "onboardinguser@manageruser.com",
                Password = "manageruser",
                Type = UserType.ManagerUser
            });
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetManagerUserResetPasswordSendInstructions()
        {
            SetResourceId("onboardinguser@manageruser.com");
            SetMethodExtraRoute("resetPassword/sendInstructions");

            var response = await GetAsync(_URL, new UserCredentials
            {
                Username = "onboardinguser@manageruser.com",
                Password = "manageruser",
                Type = UserType.ManagerUser
            });
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostManagerUserResetPassword()
        {
            SetResourceId("259");
            SetMethodExtraRoute("resetPassword");
            SetMethodParams("token", "missing");
            var response = await PostAsync(_URL, null, new UserCredentials
            {
                Username = "onboardinguser@manageruser.com",
                Password = "manageruser",
                Type = UserType.ManagerUser
            });
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetManagerUserUserLogin()
        {
            SetResourceId("onboardinguser@manageruser.com");
            SetMethodExtraRoute("userLogin");
            var response = await GetAsync(_URL, new UserCredentials
            {
                Username = "onboardinguser@manageruser.com",
                Password = "manageruser",
                Type = UserType.ManagerUser
            });
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

    }
}
