﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    using System;
    using System.Collections.Generic;
    [TestClass]
    public class ArgoManagerQuotationsControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "quotations";
        }

        [Ignore] //TODO: waiting bug fix taks #588 huboard
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetTotalStatus()
        {
            SetMethodRoute("graphic/status");
            var dic = new Dictionary<string, string>();
            dic.Add("brokerageId", "1");
            dic.Add("period", "1");
            SetMethodParams(dic);
            var response = await GetAsync(_URL, Users.ManagerUser);
            List<GraphicDTO> responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<List<GraphicDTO>>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

    }
}

