﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    using System;
    using System.Net.Http.Headers;

    /// <summary>
    /// Test class for BrokeragesController
    /// </summary>
    [TestClass]
    public class ArgoManagerBrokeragesControllerTest : BaseArgoManagerControllerTest
    {
        private const string brokerageCreated = "brokeragecreated";
        protected override void SetControllerRoute()
        {
            _controllerRoute = "brokerages";
        }

        #region CRUD

        [Ignore] //This test must run only one time 
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostBrokerage()
        {
            InputBrokerageDTO input = new InputBrokerageDTO
            {
                
                Name = brokerageCreated,
                Email = "brokerageCreated@brokerageCreated.com",
                Address = new AddressDTO()
                {
                    StreetLine1 = "stl1",
                    StreetLine2 = "stl2",
                    RegionId = 1
                },
                WebpageProducts = new List<InputWebpageProductDTO>
                {
                    new InputWebpageProductDTO() {ProductId =1, Active = false, BrokerageComission= 0 }
                },
                Website = "brokerageCreated"
            };

            var response = await PostAsync(_URL, new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"), Users.ManagerUser);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutBrokerage5()
        {
            SetResourceId("16");

            InputBrokerageDTO input = new InputBrokerageDTO
            {
                Name = brokerageCreated,
                Email = "brokerageCreated@brokerageCreated.com",
                Address = new AddressDTO()
                {
                    StreetLine1 = "stl1",
                    StreetLine2 = "stl2",
                    RegionId = 1
                },
                WebpageProducts = new List<InputWebpageProductDTO>
                {
                    new InputWebpageProductDTO() {ProductId =1, Active = false, BrokerageComission= 0 }
                },
                Website = "www.brokerageCreated.com"
            };

            var response = await PutAsync(_URL, input, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokerage1()
        {
            SetResourceId("1");
            OutputBrokerageDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputBrokerageDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerFindBrokerageCreated()
        {
            SetResourceId(brokerageCreated);
            SetMethodExtraRoute("Alias");
            OutputBrokerageDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputBrokerageDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerDeleteBrokerageCreated()
        {
            var brokerageCreated = await GetCreatedBrokerage();
            if (brokerageCreated != null)
            {
                SetResourceId(brokerageCreated.Id.ToString());

                var response = await DeleteAsync(_URL,Users.ManagerUser);

                Assert.IsTrue(response.IsSuccessStatusCode);
            }
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllBrokerages()
        {
            List<OutputBrokerageDTO> responseOutput = null;

            var response = await GetAsync(_URL, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<List<OutputBrokerageDTO>>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        #endregion

        #region Brokerage Lists

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokerage1Claims()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokerage1Insureds()
        {
            SetResourceId("1");
            SetMethodExtraRoute("insureds");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokerage1Policies()
        {
            SetResourceId("1");
            SetMethodExtraRoute("policies");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokerage1WebPages()
        {
            SetResourceId("1");
            SetMethodExtraRoute("webpages");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        #endregion

        #region MapPoints
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokeragesMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region Graphic&Numbers

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetBrokeragesTop10()
        {
            SetMethodRoute("graphic/top");
            SetMethodExtraRoute("10");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetTotalTypes()
        {
            SetMethodRoute("graphic/status");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region auxiliar methods


        public async Task<OutputBrokerageDTO> GetCreatedBrokerage()
        {
            SetResourceId(brokerageCreated);
            SetMethodExtraRoute("Alias");

            OutputBrokerageDTO responseOutput = null;
            var response = await GetContentAsync(_URL, Users.ManagerUser);
            responseOutput = JsonConvert.DeserializeObject<OutputBrokerageDTO>(response);

            ClearMethodParms();

            return responseOutput;
        }


        #endregion

    }
}

