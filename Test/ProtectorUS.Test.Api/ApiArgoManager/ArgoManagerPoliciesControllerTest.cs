﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class ArgoManagerPoliciesControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "policies";
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllPolicies()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPolicy1()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.Broker);
            OutputPolicyDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputPolicyDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPolicy1Orders()
        {
            SetResourceId("1");
            SetMethodExtraRoute("orders");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPolicy1Endorsements()
        {
            SetResourceId("1");
            SetMethodExtraRoute("endorsements");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPolicy1Claims()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesDashboardTypes()
        {
            SetMethodRoute("graphic/featured/types");
            var dic = new Dictionary<string, string>();
            dic.Add("intervaldays", "1");
            dic.Add("typeview", "1");
            SetMethodParams(dic);
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesTotalInterval()
        {
            SetMethodRoute("graphic/total/interval");
            SetMethodParams("interval", "1");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesTotalGeneral()
        {
            SetMethodRoute("graphic/total/general");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGePoliciesTotalTypes()
        {
            SetMethodRoute("graphic/types");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesTotalLastYear()
        {
            SetMethodRoute("graphic/lastYear");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetPoliciesRenewalsTotal()
        {
            SetMethodRoute("graphic/renewals/total");
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostPolicy1CreateClaim()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            InputClaimDTO input = new InputClaimDTO()
            {
                ClaimDate = System.DateTime.Now,
                DescriptionFacts = "asdhfudsahf",
                OccurrenceDate = System.DateTime.Now.AddDays(-1),
                PriorNotification = true,
            };
            var response = await PostAsync(_URL, input, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

