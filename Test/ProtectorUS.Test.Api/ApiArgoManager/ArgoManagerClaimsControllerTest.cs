﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    [TestClass]
    public class ArgoManagerClaimsControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "claims";
        }


        #region CRUD

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllClaims()
        {
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetClaim1()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.ManagerUser);
            OutputClaimDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputClaimDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerDeleteClaim5()
        {
            SetResourceId("5");
            var response = await DeleteAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostClaim4AttachDocuments()
        {
            SetResourceId("4");
            SetMethodExtraRoute("documents");
            var documents = new[] {"document1.doc","document2.doc" };
            var response = await PostAsync(_URL, documents, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerClaim4DeleteDocument2()
        {
            SetResourceId("4");
            SetMethodExtraRoute("documents");
            SetExtraResourceId("2");
            var response = await DeleteAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutClaimChangeStatus()
        {
            SetResourceId("1");
            SetMethodExtraRoute("status");
            SetExtraResourceId("1");
            var response = await PutAsync(_URL, null, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region Graphic&Numbers

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetClaimTotalInterval()
        {
            SetMethodRoute("graphic/total/interval");
            SetMethodParams("interval", "1");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

        #region MapPoints

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetClaimMapPoints()
        {
            SetMethodRoute("map");
            var response = await GetAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        #endregion

    }
}

