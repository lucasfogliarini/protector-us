﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ArgoManager
{

    using DTO;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    [TestClass]
    public class ArgoManagerProductsControllerTest : BaseArgoManagerControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "products";
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetAllProducts()
        {
            var response = await GetAsync(_URL, Users.ManagerUser);
            IEnumerable<OutputProductDTO> responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<IEnumerable<OutputProductDTO>>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }


        [Ignore] //TODO: waiting bug fix taks #586 huboard
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPutToggleProduct1()
        {
            SetResourceId("1");
            SetMethodExtraRoute("toggle");
            SetExtraResourceId("false");
            var response = await PutAsync(_URL, null, Users.ManagerUser);
            OutputProductDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputProductDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerGetCondictionsProduct1State1()
        {
            SetResourceId("1");
            SetMethodExtraRoute("state");
            SetExtraResourceId("1");
            var response = await GetAsync(_URL,Users.ManagerUser);
            IEnumerable<OutputProductDTO> responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<IEnumerable<OutputProductDTO>>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ArgoManagerPostAttachConditionProduct1()
        {
            SetResourceId("1");
            SetMethodExtraRoute("conditions");
            var dic = new Dictionary<string, string>();
            dic.Add("stateId", "1");
            dic.Add("conditionFilePath", "conditions.pdf");
            SetMethodParams(dic);
            string result = null;
            var file = File.ReadAllBytes("..\\..\\Files\\Pdf\\conditions.pdf");
            var requestContent = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(file);
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/pdf");
            requestContent.Add(fileContent, "protector", "conditions.pdf");

            // Act
            var response = await PostAsync(_URL, requestContent, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => result = x.Result);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(result);
        }
    }
}

