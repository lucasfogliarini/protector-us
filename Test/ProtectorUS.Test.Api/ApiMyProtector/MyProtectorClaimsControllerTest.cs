﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    [TestClass]
    public class MyProtectorClaimsControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "claims";
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetAllClaims()
        {
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetClaim1()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.ManagerUser);
            OutputClaimDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputClaimDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPostClaim1AttachDocuments()
        {
            SetResourceId("1");
            SetMethodExtraRoute("documents");
            var documents = new[] {"document1.doc","document2.doc" };
            var response = await PostAsync(_URL, documents, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorDeleteDocument2Claim1()
        {
            SetResourceId("1");
            SetMethodExtraRoute("documents");
            SetExtraResourceId("2");
            var response = await DeleteAsync(_URL, Users.ManagerUser);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

