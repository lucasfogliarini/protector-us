﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using System.Net.Http.Headers;
    
    [TestClass]
    public class MyProtectorBrokerageControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "brokerages";
        }
        
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetBrokerage()
        {
            SetResourceId("1");
            var response = await GetAnonymousAsync(_URL);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetBrokerageByAlias()
        {
            SetMethodParams("alias","brokeragexyz");
            var response = await GetAnonymousAsync(_URL);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetBrokerageWithProducts()
        {
            SetResourceId("brokeragexyz");
            SetMethodExtraRoute("products");
            var response = await GetAnonymousAsync(_URL);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetMap()
        {
            SetMethodRoute("map");
            SetResourceId("35611");
            SetExtraResourceId("architects_engineers");
            var response = await GetAnonymousAsync(_URL);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

