﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class MyProtectorWebpageViewsControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "webpageviews";
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPostWebpageView()
        {
            InputWebpageViewDTO input = new InputWebpageViewDTO
            {
                Access = Model.AccessType.Campaign,
                BrokerageId = 1,
                CampaignId = 1,
                WebpageProductId = 1
            };
            var response = await PostAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

