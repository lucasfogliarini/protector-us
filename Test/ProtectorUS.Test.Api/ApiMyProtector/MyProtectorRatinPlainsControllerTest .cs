﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using DTO.RatingPlan;
    using System;
    using System.Collections.Generic;
    [TestClass]
    public class MyProtectorRatinPlainsControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "ratingPlans";
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetAllRatinPlans()
        {
            var response = await GetAsync(_URL, Users.Broker);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetRatinPlan()
        {
            SetResourceId("architects_engineers");
            SetExtraResourceId("35611");
            var response = await GetAsync(_URL, Users.Insured);
            OutputRatingPlanFormDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputRatingPlanFormDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }
    }
}

