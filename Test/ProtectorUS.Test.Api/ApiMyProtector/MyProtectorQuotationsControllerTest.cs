﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using System;
    using System.Collections.Generic;
    [TestClass]
    public class MyProtectorQuotationsControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "quotations";
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetQuotation()
        {
            SetResourceId("B2D5116B-2FB7-481D-8A6F-26EF632A36E1");
            var response = await GetAsync(_URL, Users.Insured);
            OutputQuotationDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputQuotationDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPostQuotation()
        {
            QuotationInsertDTO input = new QuotationInsertDTO
            {
                BrokerageProductId = 1,
                InsuredId = 1,
                ProponentEmail = "mldmkfs@kdlçfk.fgd",
                ProponentFirstName = "sdjkh",
                ProponentLastName = "dfs",
                ProponentSocialTitle = Model.SocialTitle.Miss,
                RiskAnalysis = new InputRiskAnalysisDTO
                {
                    HashCode = "8r3yUcGeiW0ipJBXYfsw/g==",
                    Fields = new List<InputRiskAnalysisFieldDTO> {
                        new InputRiskAnalysisFieldDTO { Name= "existingPolicy",Value= "dont" },
                        new InputRiskAnalysisFieldDTO { Name= "priorActsIssuer",Value= "wasnot" },
                        new InputRiskAnalysisFieldDTO { Name= "priorInsuranceProvider",Value= ""},
                        new InputRiskAnalysisFieldDTO { Name= "effectiveMonth",Value= "2"},
                        new InputRiskAnalysisFieldDTO { Name= "effectiveDay",Value= "1" },
                        new InputRiskAnalysisFieldDTO { Name= "effectiveYear",Value= "2015" },
                        new InputRiskAnalysisFieldDTO { Name= "coverageLimit",Value= "500000"},
                        new InputRiskAnalysisFieldDTO { Name= "revenue",Value= "80000"},
                        new InputRiskAnalysisFieldDTO { Name= "startingMonth",Value= "6" },
                        new InputRiskAnalysisFieldDTO { Name= "startingDay",Value= "1" },
                        new InputRiskAnalysisFieldDTO { Name= "revenueIncome",Value= "atleast"},
                        new InputRiskAnalysisFieldDTO { Name= "engagementLetters",Value= "atleast"},
                        new InputRiskAnalysisFieldDTO { Name= "claimsLastFive",Value= "ihadno" },
                        new InputRiskAnalysisFieldDTO { Name= "totalClaims",Value= "0"},
                        new InputRiskAnalysisFieldDTO { Name= "exposure",Value= "ihaveno"},
                    }
                }


            };
            var response = await PostAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPutQuotation()
        {
            SetResourceId("d1aa07dc-0247-4f45-86d4-957955776078");
            QuotationUpdateDTO input = new QuotationUpdateDTO
            {
                CoverageLimit = 100000,
                Revenue = 1000000,
                PolicyStartDate = DateTime.Now,
                ProponentSocialTitle = Model.SocialTitle.Mister,
                ProponentFirstName = "firstname1",
                ProponentLastName = "lastname",
                ProponentEmail = "email@mail.com"
            };

            var response = await PutAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPutQuotationDetails()
        {
            SetResourceId("48119ced-43ea-4837-822b-8dd930e0a960");
            SetMethodRoute("details");
            QuotationUpdateDTO input = new QuotationUpdateDTO
            {
                CoverageLimit = 100000,
                Revenue = 1000000,
                PolicyStartDate = DateTime.Now,
                ProponentSocialTitle = Model.SocialTitle.Mister,
                ProponentFirstName = "firstname1",
                ProponentLastName = "lastname",
                ProponentEmail = "email@mail.com"
            };

            var response = await PutAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

    }
}

