﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    [TestClass]
    public class MyProtectorInsuredControllerTest : BaseMyProtectorControllerTest

    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "insured";
        }


        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetInsured()
        {
            SetResourceId("206");
            var response = await GetAsync(_URL, Users.Insured);
            OutputInsuredDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputInsuredDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }


        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetInsured206Profile()
        {
            SetResourceId("206");
            SetMethodExtraRoute("profile");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetInsuredOnboarding()
        {
            SetMethodRoute("onboarding");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPutInsured()
        {
            InsuredSelfUpdateDTO input = new InsuredSelfUpdateDTO
            {
                FirstName = "Insured",
                LastName = "Changed",
                Email = "shari_insured@brokerage1.com"

            };
            var response = await PutAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPutInsuredProfile()
        {
            SetMethodRoute("updateProfile");
            InputInsuredProfileDTO input = new InputInsuredProfileDTO
            { 
                Email = "shari_insured@brokerage1.com",
                Password = "insureduser"

            };
            var response = await PutAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPutChangePassword()
        {
            SetMethodRoute("changePassword");
            SetMethodParams("password", "insureduser");

            var response = await PutAsync(_URL, null, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetResetPasswordSendInstructions()
        {
            SetResourceId("shari_insured@brokerage1.com");
            SetMethodExtraRoute("resetPassword/sendInstructions");

            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task PostResetPassword()
        {
            SetResourceId("134");
            SetMethodExtraRoute("resetPassword");
            SetMethodParams("token", "missing");
            var response = await PostAsync(_URL, null, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetUserLogin()
        {
            SetResourceId("shari_insured@brokerage1.com");
            SetMethodExtraRoute("userLogin");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

