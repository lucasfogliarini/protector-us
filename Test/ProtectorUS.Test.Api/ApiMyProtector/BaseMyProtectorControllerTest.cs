﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ProtectorUS.Test.Api.MyProtector
{
    [TestClass]
    public abstract class BaseMyProtectorControllerTest : BaseControllerTest
    {

        protected override void SetApiRoute()
        {
            _baseURL = TestApiConfiguration.ApiElements.FirstOrDefault(_element => _element.Type == "MyProtector").Url;
        }

    }
}
