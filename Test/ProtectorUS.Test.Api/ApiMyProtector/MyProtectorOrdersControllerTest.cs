﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    [TestClass]
    public class MyProtectorOrderControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "orders";
        }


        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPostOrder()
        {
            InputOrderDTO input = new InputOrderDTO
            {
                CreditCard = new CreditCardInfoDTO
                {
                    AddressLine1 = "",
                    AddressLine2 = "jkj",
                    CardHoldName = "dc",
                    Cvc = "",
                    ExpirationMonth = "12",
                    ExpirationYear = "2018",
                    Number = "kdjfjskg",
                    ZipCode = "hkjdhk"
                    
                },
                QuotationHash= new System.Guid(),
                TotalInstallments = 1
            };
            var response = await PostAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

