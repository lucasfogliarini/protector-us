﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class MyProtectorPoliciesControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "policies";
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetAllPolicies()
        {
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetPolicy1()
        {
            SetResourceId("1");
            var response = await GetAsync(_URL, Users.Insured);
            OutputPolicyDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputPolicyDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetPolicy1Claims()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task MyProtectorPostPocicy1CreateClaim()
        {
            SetResourceId("1");
            SetMethodExtraRoute("claims");
            InputClaimDTO input = new InputClaimDTO()
            {
                ClaimDate = System.DateTime.Now,
                DescriptionFacts = "asdhfudsahf",
                OccurrenceDate = System.DateTime.Now.AddDays(-1),
                PriorNotification = true,
            };
            var response = await PostAsync(_URL, input, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }



        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetPolicy1Coverages()
        {
            SetResourceId("1");
            SetMethodExtraRoute("coverages");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }


        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetPolicy1Orders()
        {
            SetResourceId("1");
            SetMethodExtraRoute("orders");
            var response = await GetAsync(_URL, Users.Insured);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}

