﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.MyProtector
{

    using DTO;
    using System.Collections.Generic;
    [TestClass]
    public class MyProtectorProductsControllerTest : BaseMyProtectorControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "products";
        }

        [TestMethod, TestCategory("API")]
        public async Task MyProtectorGetProducts()
        {
            SetResourceId("architects_engineers");
            var response = await GetAsync(_URL, Users.Insured);
            OutputProductBaseDTO responseOutput = null;
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputProductBaseDTO>(x.Result));
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }
    }
}

