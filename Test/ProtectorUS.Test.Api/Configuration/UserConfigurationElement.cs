﻿using System;
using System.Configuration;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Represents the User configuration environment element.
    /// </summary>
    public class UserElement : ConfigurationElement
    {
        /// <summary>
        /// Gets the type configuration.
        /// </summary>
        [ConfigurationProperty("type")]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
            set { this["type"] = value; }
        }

        /// <summary>
        /// The username attribute.
        /// </summary>
        [ConfigurationProperty("username")]
        public string Username
        {
            get { return this["username"].ToString(); }
            set { this["username"] = value; }
        }



        /// <summary>
        /// The password attribute.
        /// </summary>
        [ConfigurationProperty("password")]
        public string Password
        {
            get { return this["password"].ToString(); }
            set { this["password"] = value; }
        }

        public UserType GetUserType
        {
            get
            {
                switch (Type)
                {
                    case "Broker":
                        return UserType.Broker;
                    case "Insured":
                        return UserType.Insured;
                    case "ManagerUser":
                        return UserType.ManagerUser;
                    default:
                        return UserType.Insured;
                }
            }
        }

    }

    [ConfigurationCollection(typeof(UserElement))]
    public class UserElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "user";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserElement)(element)).Username;
        }

        public UserElement this[int idx]
        {
            get { return (UserElement)BaseGet(idx); }
        }
    }
}