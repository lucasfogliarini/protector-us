﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Represents the Api configuration environment element.
    /// </summary>
    public class ApiElement : ConfigurationElement
    {
        /// <summary>
        /// Gets the type configuration.
        /// </summary>
        [ConfigurationProperty("type")]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
            set { this["type"] = value; }
        }

        /// <summary>
        /// The url attribute.
        /// </summary>
        [ConfigurationProperty("url")]
        public string Url
        {
            get { return this["url"].ToString(); }
            set { this["url"] = value; }
        }

        /// <summary>
        /// Gets the Environment configuration.
        /// </summary>
        [ConfigurationProperty("environment")]
        public string Environment
        {
            get { return Convert.ToString(this["environment"]); }
            set { this["environment"] = value; }
        }

        public enum ApiType
        {
            ManagerBroker,
            ManagerArgo,
            MyProtector
        }

        public enum TestEnvironment
        {
            /// <summary>
            /// For localhost environment.
            /// </summary>
            [Description("development")]
            Development,

            /// <summary>
            /// For test environment.
            /// </summary>
            [Description("uat")]
            Uat,


            /// <summary>
            /// For production environment.
            /// </summary>
            [Description("production")]
            Production
        }

        public TestEnvironment GetEnvironmentType
        {
            get
            {
                switch (Environment)
                {
                    case "production":
                        return TestEnvironment.Production;
                    case "uat":
                        return TestEnvironment.Uat;
                    default:
                        return TestEnvironment.Development;
                }
            }
        }

        public ApiType GetApiType
        {
            get
            {
                switch (Type)
                {
                    case "ManagerBroker":
                        return ApiType.ManagerBroker;
                    case "ManagerArgo":
                        return ApiType.ManagerArgo;
                    default:
                        return ApiType.MyProtector;
                }
            }
        }

    }

    [ConfigurationCollection(typeof(ApiElement))]
    public class ApiElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "api";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ApiElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ApiElement)(element)).Type;
        }

        public ApiElement this[int idx]
        {
            get { return (ApiElement)BaseGet(idx); }
        }
    }
}