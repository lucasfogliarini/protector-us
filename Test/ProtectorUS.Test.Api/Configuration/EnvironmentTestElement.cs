﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Represents the protectorUS's configuration environment element.
    /// </summary>
    public class EnvironmentTestElement : ConfigurationElement
    {
        /// <summary>
        /// Gets the environment configuration.
        /// </summary>
        [ConfigurationProperty("type")]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
            set { this["type"] = value; }
        }

        public TestEnvironment Get
        {
            get
            {
                switch (Type)
                {
                    case "production":
                        return TestEnvironment.Production;
                    case "uat":
                        return TestEnvironment.Uat;
                    default:
                        return TestEnvironment.Development;
                }
            }
        }
        public enum TestEnvironment
        {
            /// <summary>
            /// For localhost environment.
            /// </summary>
            [Description("development")]
            Development,

            /// <summary>
            /// For test environment.
            /// </summary>
            [Description("uat")]
            Uat,


            /// <summary>
            /// For production environment.
            /// </summary>
            [Description("production")]
            Production
        }
    }
}