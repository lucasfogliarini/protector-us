﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Represents the Web Client configuration environment element.
    /// </summary>
    public class WebClientConfigurationElement : ConfigurationElement
    {
        /// <summary>
        /// Gets the type configuration.
        /// </summary>
        [ConfigurationProperty("type")]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
            set { this["type"] = value; }
        }

        /// <summary>
        /// The url attribute.
        /// </summary>
        [ConfigurationProperty("name")]
        public string Name
        {
            get { return this["name"].ToString(); }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Gets the Environment configuration.
        /// </summary>
        [ConfigurationProperty("token")]
        public string Token
        {
            get { return Convert.ToString(this["token"]); }
            set { this["token"] = value; }
        }

    }

    [ConfigurationCollection(typeof(WebClientConfigurationElement))]
    public class WebClientConfigurationElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "webclient";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WebClientConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WebClientConfigurationElement)(element)).Type;
        }

        public WebClientConfigurationElement this[int idx]
        {
            get { return (WebClientConfigurationElement)BaseGet(idx); }
        }
    }
}