﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Defines the test api configuration.
    /// </summary>
    public class TestApiConfiguration : ConfigurationSection
    {


        /// <summary>
        /// The web clients element collection.
        /// </summary>
        [ConfigurationProperty("webclients")]
        public WebClientConfigurationElementCollection WebClients
        {
            get { return (WebClientConfigurationElementCollection)this["webclients"]; }
            set { this["webclients"] = value; }
        }

        /// <summary>
        /// The user element collection.
        /// </summary>
        [ConfigurationProperty("users")]
        public UserElementCollection Users
        {
            get { return (UserElementCollection)this["users"]; }
            set { this["users"] = value; }
        }


        /// <summary>
        /// The api element collection.
        /// </summary>
        [ConfigurationProperty("apis")]
        public ApiElementCollection Apis
        {
            get { return (ApiElementCollection)this["apis"]; }
            set { this["apis"] = value; }
        }



        /// <summary>
        /// Gets the test api configuration.
        /// </summary>
        /// <returns>the protectorUS configuration section.</returns>
        public static TestApiConfiguration GetConfig()
        {
            return ConfigurationManager.GetSection(
               "testApiConfiguration") as TestApiConfiguration;
        }

        /// <summary>
        /// Gets the user collection configuration.
        /// </summary>
        public static UserElementCollection GetUsers()
        {
            return GetConfig().Users;
        }

        public static IEnumerable<UserElement> UserElements
        {
            get
            {
                foreach (UserElement selement in GetUsers())
                {
                    if (selement != null)
                        yield return selement;
                }
            }
        }


        /// <summary>
        /// Gets the api element collection configuration.
        /// </summary>
        public static ApiElementCollection GetApis()
        {
            return GetConfig().Apis;
        }

        public static IEnumerable<ApiElement> ApiElements
        {
            get
            {
                foreach (ApiElement selement in GetApis())
                {
                    if (selement != null)
                        yield return selement;
                }
            }
        }

        /// <summary>
        /// Gets the api element collection configuration.
        /// </summary>
        public static WebClientConfigurationElementCollection GetWebClients()
        {
            return GetConfig().WebClients;
        }

        public static IEnumerable<WebClientConfigurationElement> WebClientElements
        {
            get
            {
                foreach (WebClientConfigurationElement selement in GetWebClients())
                {
                    if (selement != null)
                        yield return selement;
                }
            }
        }


    }
}
