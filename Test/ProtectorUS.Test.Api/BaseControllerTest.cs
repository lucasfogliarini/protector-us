﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ProtectorUS.Test.Api
{
    /// <summary>
    /// Base class for Api Controllers and Login Tests
    /// </summary>
    [TestClass]
    public abstract class BaseControllerTest
    {
        protected Token _token;
        protected string _baseURL;
        protected string _controllerRoute;
        protected string _methodRoute;
        protected string _methodParams;
        protected string _resourceId;
        protected string _extraResourceId;
        protected string _methodExtraRoute;
        protected string _URL
        {
            /// If exists method route and/or params concat the same whith controller route
            /// else put only the controller route at the URL that will be request by the client
            get
            {
                var url = string.IsNullOrEmpty(_methodRoute) ? _controllerRoute : $"{_controllerRoute}/{_methodRoute}";
                url = string.IsNullOrEmpty(_resourceId) ? url : $"{url}/{_resourceId}";
                url = string.IsNullOrEmpty(_methodExtraRoute) ? url : $"{url}/{_methodExtraRoute}";
                url = string.IsNullOrEmpty(_extraResourceId) ? url : $"{url}/{_extraResourceId}";
                return string.IsNullOrEmpty(_methodParams) ? url : $"{url}?{_methodParams}";
            }
        }
        public BaseControllerTest()
        {
            SetControllerRoute();
            SetApiRoute();

        }

        #region Additional test attributes

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        // Use TestInitialize to run code before running each test 
        public void Setup()
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public void BaseTestClean()
        {

        }

        #endregion

        #region Requests methods

        protected async Task<HttpResponseMessage> GetAsync(string url, UserCredentials user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                LoginAs(user, client);

                return await client.GetAsync(url);
            }
        }

        protected async Task<HttpResponseMessage> GetAnonymousAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                return await client.GetAsync(url);
            }
        }

        protected async Task<string> GetContentAsync(string url, UserCredentials user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                LoginAs(user, client);

                using (HttpResponseMessage response = await client.GetAsync(url))
                    return await response.Content.ReadAsStringAsync();
            }
        }

        protected async Task<HttpResponseMessage> PutAsync(string url, object input, UserCredentials user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                LoginAs(user, client);

                return await client.PutAsync(url, new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"));
            }
        }

        protected async Task<HttpResponseMessage> PostAsync(string url, HttpContent content, UserCredentials user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                LoginAs(user, client);

                return await client.PostAsync(url, content);
            }
        }

        protected async Task<HttpResponseMessage> PostAsync(string url, object input, UserCredentials user)
        {
            return await PostAsync(url, new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"), user);
        }

        protected async Task<HttpResponseMessage> PostAnonymousAsync(string url, HttpContent content)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                return await client.PostAsync(url, content);
            }
        }

        protected async Task<HttpResponseMessage> DeleteAsync(string url, UserCredentials user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                LoginAs(user, client);

                return await client.DeleteAsync(url);
            }
        }

        #endregion

        #region Auxiliar authentication methods

        protected void LoginAs(UserCredentials user, HttpClient client)
        {
            Dictionary<string, string> credentials = SetRequestUserCredentials(user);
            var responseToken = client.PostAsync("token", new FormUrlEncodedContent(credentials)).Result;
            var _token = JsonConvert.DeserializeObject<Token>(responseToken.Content.ReadAsStringAsync().Result);
            if (_token.AccessToken != null)
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.AccessToken);
            else
                throw new Exception($"Cannot get the Beaurer Toker for user with Username: {user.Username} and Password : {user.Password}");
        }

        protected static Dictionary<string, string> SetRequestUserCredentials(UserCredentials user)
        {
            var credentials = new Dictionary<string, string>();
            credentials.Add("client_id", "web");
            credentials.Add("user_type", Enum.GetName(typeof(UserType), user.Type));
            credentials.Add("grant_type", "password");
            credentials.Add("username", user.Username);
            credentials.Add("password", user.Password);
            return credentials;
        }

        #endregion

        #region Must override methods
        protected abstract void SetControllerRoute();

        protected abstract void SetApiRoute();

        #endregion Must override methods

        #region Can override methods
        
        /// <summary>
        /// Set the params key and values to request
        /// </summary>
        /// <param name="parameters"></param>
        protected virtual void SetMethodParams(string key, string value)
        {
            SetMethodParams(new Dictionary<string, string>() { { key, value } });
        }

        /// <summary>
        /// Set the params key and values to request
        /// </summary>
        /// <param name="parameters"></param>
        protected virtual void SetMethodParams(Dictionary<string, string> parameters)
        {
            _methodParams = string.Join("&", parameters.Select(x => x.Key + "=" + x.Value).ToArray());
        }

        /// <summary>
        /// Set the resource id for routes  ex: resource/1
        /// </summary>
        /// <param name="parameters"></param>
        protected virtual void SetResourceId(string id)
        {
            _resourceId = id;
        }


        /// <summary>
        ///  Set the route template describing the URI pattern to match against.
        /// </summary>
        /// <param name="methodRouteTemplate"></param>
        protected virtual void SetMethodRoute(string methodRouteTemplate)
        {
            _methodRoute = methodRouteTemplate;
        }

        /// <summary>
        ///  Set the route template describing the URI pattern to match against.
        /// </summary>
        /// <param name="methodRouteTemplate"></param>
        protected virtual void SetMethodExtraRoute(string methodExtraTemplate)
        {
            _methodExtraRoute = methodExtraTemplate;
        }
        protected virtual void SetExtraResourceId(string resourceId)
        {
            _extraResourceId = resourceId;
        }


        protected virtual void ClearMethodParms()
        {
            _methodRoute = string.Empty;
            _methodParams = string.Empty;
            _resourceId = string.Empty;
            _methodExtraRoute = string.Empty;
        }

        #endregion Can override methods

    }
}
