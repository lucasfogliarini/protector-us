﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProtectorUS.Test.Api.ApiCore
{

    using DTO;
    /// <summary>
    /// Test class for CancelReasonController
    /// </summary>
    [TestClass]
    public class ApiCoreCancelReasonsControllerTest : BaseApiCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "cancelReasons";
        }


        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetCancelReason1()
        {
            SetResourceId("1");
            OutputCancelReasonDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputCancelReasonDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePutCancelReason1()
        {
            SetResourceId("1");
            InputCancelReasonDTO input = new InputCancelReasonDTO { Description = "There's an error in my policy" };

            var response = await PutAsync(_URL, input, Users.Broker);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetAllCancelReasons()
        {
            List<OutputCancelReasonDTO> responseOutput = null;

            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<List<OutputCancelReasonDTO>>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePostCancelReason()
        {
            InputCancelReasonDTO input = new InputCancelReasonDTO { Description = "There's an error in my policy" };

            var response = await PostAsync(_URL, new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json"), Users.Broker);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }
    }
}
