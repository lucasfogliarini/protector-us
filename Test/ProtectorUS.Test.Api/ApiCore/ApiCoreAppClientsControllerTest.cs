﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ApiCore
{
    using DTO;
    using System.Linq;
    /// <summary>
    /// Test class for AppClientsController
    /// </summary>
    [TestClass]
    public class ApiCoreAppClientsControllerTest : BaseApiCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "appClients/web";
        }



        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetClientWeb()
        {
            OutputAppClientDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputAppClientDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);

        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCorePutClientWeb()
        {
            var token = TestApiConfiguration.WebClientElements.FirstOrDefault(_element => _element.Type.ToLower() == "web").Token;
            SetMethodParams("token", token);
            InputAppClientDTO input = new InputAppClientDTO { Active = true, CurrentVersion = "1.0", InReview = false, ReviewVersion = "1.0" };
            var response = await PutAsync(_URL, input , Users.ManagerUser);

            Assert.IsTrue(response.IsSuccessStatusCode);
        }


    }
}
