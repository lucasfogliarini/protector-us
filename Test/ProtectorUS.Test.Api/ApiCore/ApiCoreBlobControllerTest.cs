﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ProtectorUS.Test.Api.ApiCore
{
    /// <summary>
    /// Test class for BlobControllerTest
    /// </summary>
    [TestClass]
    public class ApiCoreBlobControllerTest : BaseApiCoreControllerTest
    {
        public ApiCoreBlobControllerTest()
        {
        }

        protected override void SetControllerRoute()
        {
            _controllerRoute = "blob";
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePostImages()
        {
            // Setup
            SetMethodRoute("images");
            SetMethodParams("blobName", "protector");

            string result = null;
            var image = File.ReadAllBytes("..\\..\\Files\\Images\\protector.png");
            var requestContent = new MultipartFormDataContent();
            var imageContent = new ByteArrayContent(image);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/png");
            requestContent.Add(imageContent, "protector", "protector.jpg");

            // Act
            var response = await PostAsync(_URL, requestContent, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => result = x.Result);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetImages()
        {
            // Setup
            SetMethodRoute("images");
            SetMethodParams("blobName", "images/109f1356b95b4770b1136df3c319df8d.jpg");

            // Act
            var response = await GetAsync(_URL, Users.Broker);
            Stream contentStream = await response.Content.ReadAsStreamAsync();

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(contentStream);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePostClaimDocuments()
        {
            // Setup
            SetMethodRoute("claimDocuments");
            string result = null;
            var file = File.ReadAllBytes("..\\..\\Files\\Text\\protector.txt");
            var requestContent = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(file);
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/plain");
            requestContent.Add(fileContent, "protector", "protector.txt");

            // Act
            var response = await PostAsync(_URL, requestContent, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => result = x.Result);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetClaimDocuments()
        {
            // Setup
            SetMethodRoute("claimDocuments");
            SetMethodParams("blobName", "claim_documents/38ea1f0b77f44a119e7d750e9110a8b9.txt");

            // Act
            var response = await GetAsync(_URL, Users.Broker);
            Stream contentStream = await response.Content.ReadAsStreamAsync();

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(contentStream);
        }

       // [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePostPolicyCertificates()
        {
            // Setup
            SetMethodRoute("policyCertificates");
            SetMethodParams("blobName", "certificate_base.pdf");
            string result = null;
            var file = File.ReadAllBytes("..\\..\\Files\\Pdf\\certificate_base.pdf");
            var requestContent = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(file);
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/pdf");
            requestContent.Add(fileContent, "policy", "policy.pdf");

            // Act
            var response = await PostAsync(_URL, requestContent, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => result = x.Result);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetPolicyCertificates()
        {
            // Setup
            SetMethodRoute("policyCertificates");
            SetMethodParams("blobName", "policy_certificates/certificate_base.pdf");

            // Act
            var response = await GetAsync(_URL, Users.ManagerUser);
            Stream contentStream = await response.Content.ReadAsStreamAsync();

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(contentStream);
        }

        [Ignore]
        [TestMethod, TestCategory("API")]
        public async Task ApiCorePostProductTerms()
        {
            // Setup
            SetMethodRoute("productTerms");
            string result = null;
            var file = File.ReadAllBytes("..\\..\\Files\\Pdf\\policy.pdf");
            var requestContent = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(file);
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/pdf");
            requestContent.Add(fileContent, "policy", "policy.pdf");

            // Act
            var response = await PostAsync(_URL, requestContent, Users.ManagerUser);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => result = x.Result);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetProductTerms()
        {
            // Setup
            SetMethodRoute("productTerms");
            SetMethodParams("blobName", "product_terms/6039d09bfa4744469c19a6db8590b328.pdf");

            // Act
            var response = await GetAsync(_URL, Users.Broker);
            Stream contentStream = await response.Content.ReadAsStreamAsync();

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(contentStream);
        }
    }
}
