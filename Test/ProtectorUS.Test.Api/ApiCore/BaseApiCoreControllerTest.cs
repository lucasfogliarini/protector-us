﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace ProtectorUS.Test.Api.ApiCore
{
    [TestClass]
    public abstract class BaseApiCoreControllerTest : BaseControllerTest
    {
        protected override void SetApiRoute()
        {
            _baseURL = TestApiConfiguration.ApiElements.FirstOrDefault(_element => _element.Type == "BrokerCenter").Url;
        }

    }
}
