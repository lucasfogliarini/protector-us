﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ProtectorUS.DTO;
using System.Threading.Tasks;

namespace ProtectorUS.Test.Api.ApiCore
{
    /// <summary>
    /// Test class for RegionsController
    /// </summary>
    [TestClass]
    public class ApiCoreRegionsControllerTest : BaseApiCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "regions";
        }


        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetRegionZipCode10001()
        {
            SetResourceId("10001");
            OutputRegionDTO responseOutput = null;

            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<OutputRegionDTO>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }
    }
}
