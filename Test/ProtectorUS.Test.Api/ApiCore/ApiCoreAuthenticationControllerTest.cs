﻿
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace ProtectorUS.Test.Api.ApiCore
{

    using System.Collections.Generic;
    /// <summary>
    /// Test class for AppClientsController
    /// </summary>
    [TestClass]
    public class ApiCoreAuthenticationControllerTest : BaseApiCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "token";
        }


        #region TestMethod, TestCategory("API")s

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreLoginAsABroker()
        {
            // Setup
            Dictionary<string, string> credentials = SetRequestUserCredentials(Users.Broker);

            // Act
            var responseToken = await PostAnonymousAsync(_URL, new FormUrlEncodedContent(credentials));
            var _token = JsonConvert.DeserializeObject<Token>(responseToken.Content.ReadAsStringAsync().Result);

            // Assert
            Assert.IsTrue(responseToken.IsSuccessStatusCode);
            Assert.IsNotNull(_token.AccessToken);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreLoginAsAnInsured()
        {
            // Setup
            Dictionary<string, string> credentials = SetRequestUserCredentials(Users.Insured);

            // Act
            var responseToken = await PostAnonymousAsync(_URL, new FormUrlEncodedContent(credentials));
            var _token = JsonConvert.DeserializeObject<Token>(responseToken.Content.ReadAsStringAsync().Result);

            // Assert
            Assert.IsTrue(responseToken.IsSuccessStatusCode);
            Assert.IsNotNull(_token.AccessToken);
        }

        [TestMethod, TestCategory("API")]
        public async Task ApiCoreLoginAsAManagerUser()
        {
            // Setup
            Dictionary<string, string> credentials = SetRequestUserCredentials(Users.ManagerUser);

            // Act
            var responseToken = await PostAnonymousAsync(_URL, new FormUrlEncodedContent(credentials));
            var _token = JsonConvert.DeserializeObject<Token>(responseToken.Content.ReadAsStringAsync().Result);

            // Assert
            Assert.IsTrue(responseToken.IsSuccessStatusCode);
            Assert.IsNotNull(_token.AccessToken);
        }

        #endregion


    }
}
