﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ProtectorUS.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProtectorUS.Test.Api.ApiCore
{
    /// <summary>
    /// Test class for StatesController
    /// </summary>
    [TestClass]
    public class ApiCoreStatesControllerTest : BaseApiCoreControllerTest
    {
        protected override void SetControllerRoute()
        {
            _controllerRoute = "states";
        }


        [TestMethod, TestCategory("API")]
        public async Task ApiCoreGetStates()
        {
            IEnumerable<OutputStateDTO> responseOutput = null;

            var response = await GetAsync(_URL, Users.Broker);
            await response.Content.ReadAsStringAsync().ContinueWith((x) => responseOutput = JsonConvert.DeserializeObject<IEnumerable<OutputStateDTO>>(x.Result));

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsNotNull(responseOutput);
        }
    }
}
