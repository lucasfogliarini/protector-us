﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(ProtectorUS.Api.ArgoManager.Start))]
namespace ProtectorUS.Api.ArgoManager
{
    public class Start : StartBase
    {
        protected override string Title { get; set; } = "ArgoManager";
    }
}