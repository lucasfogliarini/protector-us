﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System;
    using Model;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/endorsements")]
    public class EndorsementsController : BaseController
    {
        readonly IEndorsementService _endorsementService;
        public EndorsementsController(IEndorsementService endorsementService)
            : base(endorsementService)
        {
            _endorsementService = endorsementService;
        }

        const string EndorsementsRoute = "Endorsements";
        /// <summary>
        /// Get Endorsements
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputEndorsementDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = EndorsementsRoute)]
        public IHttpActionResult GetAll([FromUri] EndorsementStatus? status = null, int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Endorsement>();
                if(status != null) filters.Add(e => e.Status == status);
                var endorsementsPaged = _endorsementService.FindAll(page, pageSize, "-CreatedDate", filters);
                return PaginatedResult(endorsementsPaged, EndorsementsRoute);
            });
        }

        /// <summary>
        /// Reply a Endorsement
        /// </summary>
        [SwaggerOk(typeof(OutputEndorsementDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017), nameof(MessageStatusCode.EL039))]
        [HttpPut, Route("{id}/reply/{approve}")]
        public IHttpActionResult Reply(long id, bool approve)
        {
            return TryCatch(() =>
            {
                var endorsementOut = _endorsementService.Reply(id, approve);
                return Ok(endorsementOut);
            });
        }
    }
}
