﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;

    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/companies")]
    public class CompaniesController : BaseController
    {
        readonly ICompanyService _companyService;
        public CompaniesController(ICompanyService companyService)
            : base(companyService)
        {
            _companyService = companyService;
        }

        /// <summary>
        /// Get Companies
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<CompanyDTO>)), SwaggerInternalServerError]
        [HttpGet, Route("")]
        public IHttpActionResult GetAll()
        {
            return TryCatch(() =>
            {
                var companies = _companyService.GetAll();
                return Ok(companies);
            });
        }
    }
}
