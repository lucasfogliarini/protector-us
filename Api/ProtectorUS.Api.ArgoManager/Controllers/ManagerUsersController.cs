﻿using System.Web.Http;
using System.Linq;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System;
    using Model;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/managerUsers")]
    public class ManagerUsersController : BaseController
    {
        readonly IManagerUserService _managerUserService;
        public ManagerUsersController(IManagerUserService managerUserService)
            : base(managerUserService)
        {
            _managerUserService = managerUserService;
        }

        #region CRUD
        /// <summary>
        /// Get a Manager User
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public  IHttpActionResult Get(long id)
        {
            return  TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Get(id);
                return Ok(managerUserOut);
            });
        }

        const string ManagerUsersRoute = "ManagerUsers";
        /// <summary>
        /// Get Manager Users
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputManagerUserItemDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = ManagerUsersRoute)]
        public IHttpActionResult GetAll(string search = null, [FromUri] long[] type = null, string sort = "FirstName", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<ManagerUser>();
                if (search != null) filters.Add(m => m.FirstName.Contain(search) || m.LastName.Contain(search) || m.Email.Contain(search));
                if (type.HasItem()) filters.Add(m => m.Profiles.Any(p => type.Contains(p.Id)));

                var managerUsersOut = _managerUserService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(managerUsersOut, ManagerUsersRoute);
            });
        }



        /// <summary>
        /// Update a ManagerUser async
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}")]
        public  IHttpActionResult Update(long id, ManagerUserUpdateDTO managerUserIn)
        {
            return  TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Update(id, managerUserIn);
                return Ok(managerUserOut);
            });
        }

        /// <summary>
        /// Create a ManagerUser async
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public  IHttpActionResult Create(ManagerUserInsertDTO managerUserIn)
        {
            return TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Create(managerUserIn);
                return Created(managerUserOut);
            });
        }

        /// <summary>
        /// Delete a ManagerUser
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}")]
        public  IHttpActionResult Delete(long id)
        {
            return  TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Delete(id);
                return Ok(managerUserOut);
            });
        }
        #endregion
    }
}