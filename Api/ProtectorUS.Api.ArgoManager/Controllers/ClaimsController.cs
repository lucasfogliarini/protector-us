﻿using System.Web.Http;
using System.Linq;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System;
    using Model;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/claims")]
    public class ClaimsController : BaseController
    {
        readonly IClaimService _claimService;
        public ClaimsController(IClaimService claimService)
            : base(claimService)
        {
            _claimService = claimService;
        }

        #region CRUD
        const string ClaimsRoute = "Claims";
        /// <summary>
        /// Get Claims async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = ClaimsRoute)]
        public IHttpActionResult GetAll(string search = null, long? productId = null, [FromUri] ClaimStatus[] status = null, string sort = "-ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Claim>();
                if (productId != null) filters.Add(c => c.Policy.Condition.ProductId == productId);
                if (status.HasItem()) filters.Add(c => status.Contains(c.Status));
                if (search != null) filters.Add(c => c.Number.ToLower().Contains(search.ToLower()));

                var claims = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claims, ClaimsRoute);
            });
        }

        /// <summary>
        /// Get a Claim async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var claimOut =  _claimService.Get(id);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Delete a Claim
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}")]
        public IHttpActionResult Delete(long id)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.Delete(id);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Attach Claim Documents
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{id}/documents")]
        public  IHttpActionResult AttachDocuments(long id, string[] documentsPath)
        {
            return  TryCatch(() =>
            {
                 var claimOut =  _claimService.AttachDocuments(id, documentsPath);
                return Created(claimOut);
            });
        }
        /// <summary>
        /// Delete Claim Document
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDocumentDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}/documents/{documentId}")]
        public  IHttpActionResult DeleteDocument(long id, long documentId)
        {
            return TryCatch(() =>
            {
                var claimDocument =  _claimService.DeleteDocument(id, documentId);
                return Ok(claimDocument);
            });
        }

        /// <summary>
        /// Change the Status of Claim
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPut, Route("{id}/status/{status}")]
        public IHttpActionResult ChangeStatus(long id, ClaimStatus status)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.ChangeStatus(id, status, this.IdUserAuthenticated);
                return Ok(claimOut);
            });
        }
        #endregion

        #region MapPoints
        /// <summary>
        /// Get Pins
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public  IHttpActionResult GetMapPoints()
        {
            return  TryCatch(() =>
            {
                var map =  _claimService.GetMapPointsAsync();
                return Ok(map);
            });
        }
        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Get Dashboard Totals by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(IReadOnlyCollection<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total/interval", Name = "GraphicIntervalTotalClaims")]
        public IHttpActionResult GetTotalInterval(ClaimPeriodFilter interval)
        {
            return TryCatch(() =>
            {
                var graphicPolicy = _claimService.GetTotalInterval(interval);

                if (graphicPolicy.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicPolicy);
            });
        }
        #endregion

    }
}
