﻿using System.Web.Http;
using System.Collections.Generic;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System;
    using Model;
    using System.Linq;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/insureds")]
    public class InsuredsController : BaseController
    {
        readonly IInsuredService _insuredService;
        readonly IClaimService _claimService;
        readonly IEndorsementService _endorsementService;

        public InsuredsController(IInsuredService insuredService, IClaimService claimService, IEndorsementService endorsementService)
            : base(insuredService, claimService, endorsementService)
        {
            _insuredService = insuredService;
            _claimService = claimService;
            _endorsementService = endorsementService;
        }

        #region CRUD
        const string InsuredsRoute = "Insureds";
        /// <summary>
        /// Get Insureds async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputInsuredDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = InsuredsRoute)]
        public IHttpActionResult GetAll(string search = null, long? stateId = null, bool? active = null, string sort = "LastName", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                if (search != null) filters.Add(m => m.FirstName.Contain(search) || m.LastName.Contain(search) || m.Email.Contain(search));
                if (stateId != null ) filters.Add(i => i.Addresses.Any(a=> a.Region.City.StateId == stateId));
                if (active != null) filters.Add(i => i.HasActivePolicy == active);

                var insureds = _insuredService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(insureds, InsuredsRoute);
            });
        }

        /// <summary>
        /// Get Insureds Details async
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public  IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var insuredOut =  _insuredService.Get(id);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Find Insureds by name
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<UserNameDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("find")]
        public IHttpActionResult FindAll(string name)
        {
            return TryCatch(() =>
            {
                var insureds = _insuredService.FindAll(name);
                return Ok(insureds);
            });
        }

        #endregion

        #region Insureds Lists       
        const string InsuredClaimsRoute = "InsuredClaims";
        /// <summary>
        /// Get Claims
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/claims", Name = InsuredClaimsRoute)]
        public IHttpActionResult Claims(long id, string sort = "ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Id == id);
                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claimsPaged, InsuredClaimsRoute);
            });
        }

        const string InsuredEndorsementsRoute = "InsuredEndorsements";
        /// <summary>
        /// Get Endorsement`s async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputEndorsementDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/endorsements", Name = InsuredEndorsementsRoute)]
        public IHttpActionResult Endorsements(long id, string sort = "CreatedDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Endorsement>();
                filters.Add(x => x.Policy.Id == id);
                var endorsementsPaged = _endorsementService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(endorsementsPaged, InsuredEndorsementsRoute);
            });
        }

        #endregion

        #region MapPoints
        /// <summary>
        /// Get Insureds Map
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public IHttpActionResult GetMapPoints()
        {
            return TryCatch(() =>
            {
                var map = _insuredService.GetMapPoints();
                return Ok(map);
            });
        }
        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Count By Status
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/types")]
        public IHttpActionResult CountByStatus()
        {
            return TryCatch(() =>
            {
                var graphicInsured = _insuredService.CountByStatus();

                if (graphicInsured.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastYear", Name = "GraphicLastYearInsureds")]
        public IHttpActionResult GetTotalLastYear()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();

                var graphicInsured = _insuredService.GetTotalLastYear(filters);

                if (graphicInsured.Count() <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        #endregion
    }
}
