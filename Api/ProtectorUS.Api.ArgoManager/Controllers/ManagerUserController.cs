﻿﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    using Model;
    [ValidateModelState]
    [RoutePrefix("api/managerUser")]
    public class ManagerUserController : UsersController<ManagerUser>
    {
        readonly IManagerUserService _managerUserService;
        public ManagerUserController(IManagerUserService managerUserService)
            : base(managerUserService)
        {
            _managerUserService = managerUserService;
        }

        #region Authenticated
        /// <summary>
        /// Get authenticated Manager User
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public  IHttpActionResult Get()
        {
            return  TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Get(IdUserAuthenticated);
                return Ok(managerUserOut);
            });
        }

        /// <summary>
        /// Test email to ManagerUser
        /// </summary>
        [SwaggerOk(typeof(bool)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("test/{email}/email")]
        public IHttpActionResult TestEmail(string email)
        {
            return TryCatch(() =>
            {
                bool isUsed = _managerUserService.IsUsed(email, IdUserAuthenticated);
                return Ok(isUsed);
            });
        }

        /// <summary>
        /// Onboarding authenticated ManagerUser 
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL029))]
        [HttpGet, Route("onboarding")]
        public IHttpActionResult Onboarding()
        {
            return TryCatch(() =>
            {
                var managerUserOut = _managerUserService.Onboarding(IdUserAuthenticated);
                return Ok(managerUserOut);
            });
        }

        /// <summary>
        /// Update authenticated ManagerUser async
        /// </summary>
        [SwaggerOk(typeof(OutputManagerUserDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("")]
        public  IHttpActionResult Update(ManagerUserSelfUpdateDTO managerUserIn)
        {
            return  TryCatch(() =>
            {
                var managerUserOut =  _managerUserService.Update(IdUserAuthenticated, managerUserIn);
                return Ok(managerUserOut);
            });
        }
        /// <summary>
        /// Change password
        /// </summary>
        [SwaggerOk(typeof(MessageCode)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("changePassword")]
        public override IHttpActionResult ChangePassword(string password)
        {
            return base.ChangePassword(password);
        }
        #endregion

        #region Not Authenticated
        /// <summary>
        /// Get the User Login
        /// </summary>
        [SwaggerOk(typeof(OutputUserLoginDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/userLogin")]
        public override IHttpActionResult UserLogin(string email)
        {
            return base.UserLogin(email);
        }

        /// <summary>
        /// Get Permissions
        /// </summary>
        [SwaggerOk(typeof(string[])), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("permissions")]
        public override IHttpActionResult GetPermissions(bool allowed = true)
        {
            return base.GetPermissions(allowed);
        }

        /// <summary>
        /// Send instructions for reset the password
        /// </summary>
        [SwaggerOk(nameof(MessageStatusCode.S002)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/resetPassword/sendInstructions")]
        public override IHttpActionResult ResetPasswordSendInstructions(string email)
        {
            return base.ResetPasswordSendInstructions(email);
        }

        /// <summary>
        /// Verify or reset password
        /// </summary>
        /// <remarks>Only verifying: omit the param 'newPassword'<br />Reseting: insert the param 'newPassword'</remarks>
        [SwaggerOk(nameof(MessageStatusCode.S001), nameof(MessageStatusCode.S003)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{userId}/resetPassword")]
        public override IHttpActionResult ResetPassword(long userId, string token, string newPassword = null)
        {
            return base.ResetPassword(userId, token, newPassword);
        }
        #endregion
    }
}

