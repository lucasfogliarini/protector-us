﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Diagnostics.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/products")]
    public class ProductsController : BaseController
    {
        readonly IProductService _productService;
        public ProductsController(IProductService productService)
            : base(productService)
        {
            _productService = productService;
        }

        #region CRUD
        /// <summary>
        /// Get Products
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputProductDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            return await TryCatchAsync(async() =>
            {
                var products = await _productService.GetAllAsync();
                return Ok(products);
            });
        }
        /// <summary>
        /// Toggle the Product
        /// </summary>
        [SwaggerOk(typeof(OutputProductDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017)), SwaggerStatusCode(nameof(MessageStatusCode.EL038))]
        [HttpPut, Route("{id}/enable/{active}")]
        public IHttpActionResult Toggle(long id, bool active)
        {
            return TryCatch(() =>
            {
                var products = _productService.Toggle(id,active);
                return Ok(products);
            });
        }

        /// <summary>
        /// Get product Conditions async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputProductConditionDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/state/{stateId}")]
        public IHttpActionResult GetConditions(long id, long stateId)
        {
            return  TryCatch(() =>
            {
                var productConditions =  _productService.GetConditions(id, stateId);
                return Ok(productConditions);
            });
        }
        /// <summary>
        /// Attach Product Conditions
        /// </summary>
        [SwaggerOk(typeof(OutputProductDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("{id}/conditions")]
        public IHttpActionResult AttachCondition(long id, long stateId, string conditionFilePath)
        {
            return TryCatch(() =>
            {
                var productConditions = _productService.AttachCondition(id, stateId, conditionFilePath);
                return Ok(productConditions);
            });
        }
        #endregion
    }
}
