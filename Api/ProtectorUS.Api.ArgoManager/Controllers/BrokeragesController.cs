﻿using System.Web.Http;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/brokerages")]

    public class BrokeragesController : BaseController
    {
        readonly IBrokerageService _brokerageService;
        readonly IClaimService _claimService;
        readonly IInsuredService _insuredService;
        readonly IPolicyService _policyService;
        public BrokeragesController(
            IBrokerageService brokerageService,
            IClaimService claimService,
            IInsuredService insuredService,
            IPolicyService policyService)
            : base(brokerageService,  claimService, insuredService, policyService)
        {
            _insuredService = insuredService;
            _brokerageService = brokerageService;
            _claimService = claimService;
            _policyService = policyService;
        }

        #region CRUD
        /// <summary>
        /// Create a Brokerage async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public  IHttpActionResult Create(InputBrokerageDTO brokerageIn)
        {
            return  TryCatch( () =>
            {
                var brokerageOut =  _brokerageService.Create(brokerageIn);
                return Created(brokerageOut);
            });
        }

        /// <summary>
        /// Update a Brokerage async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}")]
        public  IHttpActionResult Update(long id, InputBrokerageDTO brokerageIn)
        {
            return TryCatch(() =>
            {
                var brokerageOut =  _brokerageService.Update(id, brokerageIn);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Enable or Disable a Product in a Brokerage
        /// </summary>
        [SwaggerOk(typeof(BrokerageProductEnableDisableOutputDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}/enableProduct")]
        public IHttpActionResult EnableDisableProduct(long id, BrokerageProductEnableDisableInputDTO brokerageProductEnableDisable)
        {
            return TryCatch(() =>
            {
                var brokerageOut = _brokerageService.EnableDisableProduct(id, brokerageProductEnableDisable);
                return Ok(brokerageOut);
            });
        }


        /// <summary>
        /// Get total Active and Inactive Brokerages and Active Broker Agents
        /// </summary>
        [SwaggerOk(typeof(BrokerageActiveInactiveBrokeragesAndAgentstDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("activeInactiveBrokeragesAndAgents")]
        public IHttpActionResult GetActiveInactiveBrokeragesAndAgents()
        {
            return TryCatch(() =>
            {
                var brokerageOut = _brokerageService.GetActiveInactiveBrokeragesAndAgents();
                return Ok(brokerageOut);
            });
        }


        /// <summary>
        /// Get a Brokerage async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public  IHttpActionResult Get(long id)
        {
            return  TryCatch(() =>
            {
                var brokerageOut =  _brokerageService.Get(id);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Find a brokerage by alias
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{alias}/alias")]
        public  IHttpActionResult Find(string alias)
        {
            return  TryCatch( () =>
            {
                var brokerage =  _brokerageService.Find(alias);
                return Ok(brokerage);
            });
        }

        /// <summary>
        /// Delete a Brokerage async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}")]
        public IHttpActionResult Delete(long id)
        {
            return  TryCatch(() =>
            {
                var brokerageOut =  _brokerageService.Delete(id);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Get Brokerages
        /// </summary>
        const string BrokeragesRoute = "Brokerages";
        [SwaggerOk(typeof(IEnumerable<OutputBrokerageItemDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = BrokeragesRoute)]
        public IHttpActionResult GetAll(string search = null, long? stateId = null, [FromUri] long[] status = null, string sort = "Name", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Brokerage>();
                if (stateId != null) filters.Add(b => b.Address.Region.City.StateId == stateId);
                if (search != null) filters.Add(b => b.Name.ToLower().Contains(search.ToLower()));
                if (status.HasItem()) filters.Add(x => status.Contains((int)x.Status));

                var brokerages = _brokerageService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(brokerages, BrokeragesRoute);
            });
        }

        #endregion

        #region Brokerage Lists
        const string BrokerageClaimsRoute = "BrokerageClaims";
        /// <summary>
        /// Get Claims
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/claims", Name = BrokerageClaimsRoute)]
        public IHttpActionResult Claims(long id, string sort = "ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Broker.BrokerageId == id);
                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claimsPaged, BrokerageClaimsRoute);
            });
        }

        const string BrokerageInsuredsRoute = "BrokerageInsureds";
        /// <summary>
        /// Get insureds by Brokerage
        /// </summary>
        [SwaggerOk(typeof(PagedResult<OutputInsuredDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/insureds", Name = "BrokerageInsureds")]
        public IHttpActionResult Insureds(long id, string sort = "LastName", int page = 1, int pageSize = maxPageSize)
        {
            return  TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                filters.Add(x => x.Policies?.FirstOrDefault()?.Broker?.BrokerageId == id);
                var brokerageInsured = _insuredService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(brokerageInsured, BrokerageInsuredsRoute);
            });
        }

        const string BrokeragePoliciesRoute = "BrokeragePolicies";
        /// <summary>
        /// Get latest business by Brokerage 
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputPolicyDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/policies", Name = BrokeragePoliciesRoute)]
        public IHttpActionResult Policies(long id, string sort = "PeriodStart", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                filters.Add(x => x.Broker.BrokerageId == id);
                var brokeragePolicies = _policyService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(brokeragePolicies, BrokeragePoliciesRoute);
            });
        }

        /// <summary>  
        /// Get Brokarage webpages async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<WebpageProductDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/webpages")]
        public  IHttpActionResult Webpages(long id)
        {
            return  TryCatch(() =>
            {
                var wepbpageOut =  _brokerageService.FindWebpages(id);
                return Ok(wepbpageOut);
            });
        }
        #endregion

        #region MapPoints
        /// <summary>
        /// Get pins async
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public IHttpActionResult GetMapPoints()
        {
            return  TryCatch(() =>
            {
                var map =  _brokerageService.GetMapPoints();
                return Ok(map);
            });
        }
        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Get top Brokerages async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputBrokerageTopListDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/top/{quant}", Name = "GraphicTopBrokerages")]
        public  IHttpActionResult GetTop(int quant)
        {
            return TryCatch(() =>
            {
                var graphicInsured =  _brokerageService.GetTop(quant);

                if (graphicInsured.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) async
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/status", Name = "GraphicStatusBrokerages")]
        public  IHttpActionResult GetTotalTypes()
        {
            return  TryCatch(() =>
            {
                var graphicBrokerage =  _brokerageService.GetTotalStatus();

                if (graphicBrokerage.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicBrokerage);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) async
        /// </summary>
        [SwaggerOk(typeof(IReadOnlyCollection<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastYear", Name = "GraphicLastYearBrokerages")]
        public  IHttpActionResult GetTotalLastYear()
        {
            return  TryCatch(() =>
            {
                var graphicBrokerage =  _brokerageService.GetTotalLastYear();

                if (graphicBrokerage.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicBrokerage);
            });
        }


        #endregion        
    }
}