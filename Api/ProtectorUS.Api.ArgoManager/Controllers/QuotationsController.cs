﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System;
    using Model;
    using System.Diagnostics;

    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/quotations")]
    public class QuotationsController : BaseController
    {
        readonly IQuotationService _quotationService;
        public QuotationsController(IQuotationService quotationService)
            : base(quotationService)
        {
            _quotationService = quotationService;
        }

        #region Graphic&Numbers
        /// <summary>
        /// Get Dashboard Totals by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerNoContent]
        [HttpGet, Route("graphic/status", Name = "GraphicStatusQuotation")]
        public IHttpActionResult GetTotalStatus(long brokerageId, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Quotation>();
                filters.Add(x => x.BrokerageProduct.BrokerageId == brokerageId);
                var graphicQuotation = _quotationService.GetTotalStatus(filters, period);

                if (graphicQuotation.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicQuotation);
            });
        }

        /// <summary>
        /// Get Totals Quote Declines by Status (transactions) and Brokerage
        /// If you want ignore the brokerage filter must set brokerageId=0
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerNoContent]
        [HttpGet, Route("graphic/declinesByType")]
        public IHttpActionResult GetDeclinesTotalByType(PeriodFilter period)
        {
            return TryCatch(() =>
            {
               
                var graphicQuotation = _quotationService.GetDeclinesTotalByType(period);

                if (graphicQuotation.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicQuotation);
            });
        }

        /// <summary>
        /// Get Dashboard Totals by Status (transactions) and Brokerage
        /// If you want ignore the brokerage filter must set brokerageId=0
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerNoContent]
        [HttpGet, Route("graphic/declinesByBrokerage")]
        public IHttpActionResult GetDeclinesTotalByBrokerage(long brokerageId, PeriodFilter period)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Quotation>();
                if (brokerageId > 0)
                    filters.Add(x => x.BrokerageProduct.BrokerageId == brokerageId);

                var graphicQuotation = _quotationService.GetDeclinesTotalByType(filters, period);

                if (graphicQuotation.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicQuotation);
            });
        }
        #endregion
    }
}
