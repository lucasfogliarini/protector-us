﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(ProtectorUS.Api.BrokerCenter.Start))]
namespace ProtectorUS.Api.BrokerCenter
{
    public class Start : StartBase
    {
        protected override string Title { get; set; } = "BrokerCenter";
    }
}