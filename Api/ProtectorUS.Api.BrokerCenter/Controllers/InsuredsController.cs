﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System;
    using Model;
    using System.Linq;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [RoutePrefix("api/insureds")]
    [AuthorizationPermissions]
    public class InsuredsController : BrokerageAuthController
    {
        readonly IInsuredService _insuredService;
        readonly IClaimService _claimService;
        readonly IEndorsementService _endorsementService;

        public InsuredsController(
            IBrokerageService brokerageService,
            IInsuredService insuredService,
            IClaimService claimService,
            IEndorsementService endorsementService)
        : base(brokerageService,
                insuredService, 
                claimService, 
                endorsementService)
        {
            _insuredService = insuredService;
            _claimService = claimService;
            _endorsementService = endorsementService;
            Onauthorization += CheckIfIsOwnBusiness;
        }

        /// <summary>
        /// Checks if the Insured being called is owned by Brokerage.
        /// </summary>
        protected void CheckIfIsOwnBusiness(AuthEventArg e)
        {
            object insuredId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (insuredId != null)
            {
                e.IsAuth = _insuredService.IsOwnCustomer(BrokerageId, Convert.ToInt64(insuredId)) ? e.IsAuth : false;
            }
        }

        #region CRUD

        const string InsuredsRoute = "Insureds";
        /// <summary>
        /// Get Insureds
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputInsuredListDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = InsuredsRoute)]
        public IHttpActionResult GetAll(string search = null, long? stateid = null, [FromUri] long[] status = null, string sort = "LastName", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();

                filters.Add(x => x.Policies.Where(y=> y.Broker.BrokerageId == BrokerageId).Count() > 0);
                if (search != null) filters.Add(x => x.FirstName.Contain(search) || x.LastName.Contain(search) || (x.FullName).Contain(search) || x.Email.Contains(search));
                if (stateid != null) filters.Add(x => x.Businesses.Select( a=> a.Address).FirstOrDefault().Region.City.StateId == stateid);
                if (status.HasItem()) filters.Add(x => status.Contains((int)x.Status));

                var brokerageInsured = _insuredService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(brokerageInsured, InsuredsRoute);
            });
        }

        /// <summary>
        /// Find Insureds by name
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<UserNameDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("find")]
        public IHttpActionResult FindAll(string name)
        {
            return TryCatch(() =>
            {
                var insureds = _insuredService.FindAll(name,BrokerageId);
                return Ok(insureds);
            });
        }

        /// <summary>
        /// Get Insureds Details
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}")]
        public  IHttpActionResult Get(long id)
        {
            return  TryCatch(() =>
            {
                var insuredOut =  _insuredService.Get(id);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Update Insured
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}")]
        public IHttpActionResult Update(long id, UpdateInsuredByBrokerDTO insuredIn)
        {
            return TryCatch(() =>
            {
                _insuredService.Update(id, insuredIn);
                return Ok();
            });
        }

        #endregion

        #region Insured Lists
        const string InsuredClaimsRoute = "InsuredClaims";
        /// <summary>
        /// Get Claims async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/claims", Name = InsuredClaimsRoute)]
        public IHttpActionResult Claims(long id, string sort = "-ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Claim>();
                filters.Add(x => x.Policy.InsuredId == id);
                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claimsPaged, InsuredClaimsRoute);
            });
        }

        const string InsuredEndorsementsRoute = "InsuredEndorsements";
        /// <summary>
        /// Get Endorsement`s async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputEndorsementDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/endorsements", Name = InsuredEndorsementsRoute)]
        public IHttpActionResult Endorsements(long id, string sort = "-CreatedDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Endorsement>();
                filters.Add(x => x.Policy.InsuredId == id);
                var endorsementsPaged = _endorsementService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(endorsementsPaged, InsuredEndorsementsRoute);
            });
        }

        #endregion

        #region MapPoints
        /// <summary>
        /// Get Insureds Map
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public IHttpActionResult GetMapPoints(int statusid = 0)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                filters.Add(x => x.Policies.Where(y => y.Broker.BrokerageId == BrokerageId).Count() > 0);
                if(statusid != 0)
                    filters.Add(x => x.Status == (UserStatus)statusid);

                var map = _insuredService.GetMapPoints(filters);
                return Ok(map);
            });
        }
        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Count By Status
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/types")]
        public IHttpActionResult CountByStatus()
        {
            return TryCatch(() =>
            {
                var countByStatus = _insuredService.CountByStatus(BrokerageId);

                if (countByStatus.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(countByStatus);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(ItemGraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastYear", Name = "GraphicLastYearInsureds")]
        public IHttpActionResult GetTotalLastYear()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Insured>();
                filters.Add(x => x.Policies.Where(p => p.Broker.BrokerageId == BrokerageId).Count() > 0);

                var graphicInsured = _insuredService.GetTotalLastYear(filters);

                if (graphicInsured.Count() <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        #endregion

    }
}
