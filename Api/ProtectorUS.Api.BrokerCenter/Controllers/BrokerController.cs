using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    using Model;
    using Communication;
    [ValidateModelState]
    [RoutePrefix("api/broker")]
    public class BrokerController : UsersController<Broker>
    {
        readonly IBrokerService _brokerService;

        public BrokerController(IBrokerService brokerService)
            : base(brokerService)
        {
            _brokerService = brokerService;
        }

        #region Authenticated

        /// <summary>
        /// Onboarding authenticated Broker
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerOnboardDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL029))]
        [HttpGet, Route("onboarding")]
        public IHttpActionResult Onboarding()
        {
            return TryCatch(() =>
            {
                long brokerId = IdUserAuthenticated;
                var brokerOut = _brokerService.Onboarding(brokerId);

                return Ok(brokerOut);
            });
        }

        /// <summary>
        /// Get authenticated Broker
        /// </summary>
        [SwaggerOk(typeof(OutputProfileBrokerDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public  IHttpActionResult Get()
        {
            return TryCatch(() =>
            {
                var brokerOut =  _brokerService.Get(IdUserAuthenticated);
                return Ok(brokerOut);
            });
        }

        /// <summary>
        /// Test email to Broker
        /// </summary>
        [SwaggerOk(typeof(bool)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("test/{email}/email", Name = "brokertestmail")]
        public IHttpActionResult TestEmail(string email)
        {
            return TryCatch(() =>
            {
                bool isUsed = _brokerService.IsUsed(email, IdUserAuthenticated);
                return Ok(isUsed);
            });
        }

        /// <summary>
        /// Update authenticated Broker async
        /// </summary>
        [SwaggerOk(typeof(OutputProfileBrokerDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("")]
        public  IHttpActionResult Update(BrokerSelfUpdateDTO brokerIn)
        {
            return  TryCatch(() =>
            {
                var brokerOut =  _brokerService.Update(IdUserAuthenticated, brokerIn);
                return Ok(brokerOut);
            });
        }
        
        /// <summary>
        /// Change password
        /// </summary>
        [SwaggerOk(typeof(MessageCode)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("changePassword")]
        public override IHttpActionResult ChangePassword(string password)
        {
            return base.ChangePassword(password);
        }

        #endregion

        #region Not Authenticated
        /// <summary>
        /// Get the User Login
        /// </summary>
        [SwaggerOk(typeof(OutputUserLoginDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/userLogin")]
        public override IHttpActionResult UserLogin(string email)
        {
            return base.UserLogin(email);
        }

        /// <summary>
        /// Get Permissions
        /// </summary>
        [SwaggerOk(typeof(string[])), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("permissions")]
        public override IHttpActionResult GetPermissions(bool allowed = true)
        {
            return base.GetPermissions(allowed);
        }

        /// <summary>
        /// Send instructions for reset the password
        /// </summary>
        [SwaggerOk(nameof(MessageStatusCode.S002)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/resetPassword/sendInstructions")]
        public override IHttpActionResult ResetPasswordSendInstructions(string email)
        {
            return base.ResetPasswordSendInstructions(email);
        }

        /// <summary>
        /// Verify or reset password
        /// </summary>
        /// <remarks>Only verifying: omit the param 'newPassword'<br />Reseting: insert the param 'newPassword'</remarks>
        [SwaggerOk(nameof(MessageStatusCode.S001), nameof(MessageStatusCode.S003)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{userId}/resetPassword")]
        public override IHttpActionResult ResetPassword(long userId, string token, string newPassword = null)
        {
            return base.ResetPassword(userId, token, newPassword);
        }
        #endregion
    }
}
