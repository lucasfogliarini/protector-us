﻿using System.Web.Http;
using System.Collections.Generic;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    using System.Linq;

    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/campaigns")]
    public class CampaignsController : BrokerageAuthController
    {
        readonly ICampaignService _campaignService;
        readonly IBlobService _blobservice;
        public CampaignsController(
            IBrokerageService brokerageService, 
            ICampaignService campaignservice, 
            IBlobService blobservice)
            : base(brokerageService, 
                  campaignservice)
        {
            _campaignService = campaignservice;
            _blobservice = blobservice;
        }
        const string CampaignsRoute = "Campaigns";
        /// <summary>
        /// Get all Campaigns
        /// </summary>
        [SwaggerOk(typeof(PagedResult<OutputCampaignItemDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = CampaignsRoute)]
        public IHttpActionResult GetAll([FromUri] long[] type = null, string sort = "-CreatedDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Campaign>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);
                if (type.HasItem())
                    filters.Add(x => type.Contains((int)x.Type));
                var campaignPaginated = _campaignService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(campaignPaginated, CampaignsRoute);
            });
        }

        #region Graphic&Numbers
        /// <summary>
        /// Get Dashboard Totals by Type 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total", Name = "GraphicTotalCampaigns")]
        public IHttpActionResult GetTotalbyType()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Campaign>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicCampaign = _campaignService.GetTotalbyType(filters);

                if (graphicCampaign.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicCampaign);
            });
        }

        /// <summary>
        /// Get Resume Totals by Type 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/resume/total", Name = "GraphicResumeTotalCampaigns")]
        public IHttpActionResult GetTotalbyTypeView()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Campaign>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicCampaign = _campaignService.GetTotalbyTypeView(filters);

                if (graphicCampaign.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicCampaign);
            });
        }

        /// <summary>
        /// Get Resume Totals Filter Period
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/resume/total/period", Name = "GraphicResumeTotalPeriodCampaigns")]
        public IHttpActionResult GetTotalbyTypePeriod(PeriodFilter period)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Campaign>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicCampaign = _campaignService.GetTotalbyTypePeriod(filters, period);

                if (graphicCampaign.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicCampaign);
            });
        }
        #endregion
    }
}