﻿using System.Web.Http;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using System.Linq;
    using Communication.StatusCode;
    [ValidateModelState]
    [RoutePrefix("api/policies")]
    [AuthorizationPermissions]
    public class PoliciesController : BrokerageAuthController
    {
        readonly IPolicyService _policyService;
        readonly IClaimService _claimService;
        readonly IEndorsementService _endorsementService;
        readonly IOrderService _orderService;
        readonly IAcordService _acordService;

        public PoliciesController(
            IBrokerageService brokerageService,
            IPolicyService policyService,
            IClaimService claimService,
            IEndorsementService endorsementService,
            IOrderService orderService, 
            IAcordService acordService)
            : base(brokerageService, 
                  policyService, 
                  claimService, 
                  endorsementService, 
                  orderService)
        {
            _policyService = policyService;
            _claimService = claimService;
            _endorsementService = endorsementService;
            _orderService = orderService;
            _acordService = acordService;
            Onauthorization += CheckIfIsOwnBusiness;
        }
        /// <summary>
        /// Checks if the Policy being edited is owned by Brokerage authenticated Broker.
        /// </summary>
        protected void CheckIfIsOwnBusiness(AuthEventArg e)
        {
            object policyId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (policyId != null)
            {
                e.IsAuth = _policyService.IsOwnBusiness(BrokerageId, Convert.ToInt64(policyId)) ? e.IsAuth : false;
            }
        }

        #region CRUD

        const string PoliciesRoute = "Policies";
        /// <summary>
        /// Get list Policies (transactions)
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputPolicyItemDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = PoliciesRoute)]
        public IHttpActionResult GetAll(string search = null, long? productId = null, [FromUri] PolicyType[] type = null, [FromUri] PolicyStatus[] status = null, string sort = "-PeriodStart", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);
                if (search != null) filters.Add(x => x.Insured.FirstName.Contain(search) || x.Insured.LastName.Contain(search) || (x.Insured.FullName).Contain(search) || x.Insured.Email.Contain(search) || x.Number.Contain(search));
                if (productId != null) filters.Add(x => x.Condition.ProductId == productId);
                if (type.HasItem()) filters.Add(x => type.Contains(x.Type));

                var brokeragePolicy = _policyService.FindAll(page, pageSize, sort, filters);

                if (status.HasItem())
                {
                    brokeragePolicy.Items = brokeragePolicy.Items.Where(w => status.Contains(w.Status)).ToList().AsReadOnly();
                    brokeragePolicy.TotalItems = brokeragePolicy.Items.Count();
                }

                return PaginatedResult(brokeragePolicy, PoliciesRoute);
            });
        }

        /// <summary>
        /// Find Policies by insured id
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<PolicyNumberDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("find")]
        public IHttpActionResult FindAll(long insuredId)
        {
            return TryCatch(() =>
            {
                var policies = _policyService.FindAll(insuredId, BrokerageId);
                return Ok(policies);
            });
        }

        /// <summary>
        /// Get Policy Details
        /// </summary>
        [SwaggerOk(typeof(OutputPolicyDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var policyOut =  _policyService.TryGet<OutputPolicyDTO>(id, 
                     p => p.Insured,
                     p => p.Condition.Product,
                     p => p.Broker,
                     p => p.Order,
                     p => p.Order.Quotation,
                     p => p.Broker.Brokerage,
                     p => p.Business,
                     p => p.Business.Address,
                     p => p.Business.Address.Region,
                     p => p.Business.Address.Region.City,
                     p => p.Business.Address.Region.City.State);
                return Ok(policyOut);
            });
        }
        #endregion

        #region Policy Lists

        const string PolicyEndorsementsRoute = "PolicyEndorsements";
        /// <summary>
        /// Get Endorsement`s async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputEndorsementDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/endorsements", Name = PolicyEndorsementsRoute)]
        public IHttpActionResult Endorsements(long id, string sort = "-CreatedDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Endorsement>();
                filters.Add(x => x.PolicyId == id);
                var endorsementsPaged = _endorsementService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(endorsementsPaged, PolicyEndorsementsRoute);
            });
        }


        const string PolicyOrdersRoute = "PolicyOrders";
        /// <summary>
        /// Get Order`s async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputOrderDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/orders", Name = PolicyOrdersRoute)]
        public IHttpActionResult Orders(long id, string sort = "-CreatedDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var ordersPaged = _orderService.FindAll(id, page, pageSize, sort);
                return PaginatedResult(ordersPaged, PolicyOrdersRoute);
            });
        }
        #endregion

        #region MapPoints
        /// <summary>
        /// Get Policies Map
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public IHttpActionResult GetMapPoints(int productid = 0)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);
                if (productid != 0)
                    filters.Add(x => x.Condition.ProductId == productid);

                var map = _policyService.GetMapPoints(filters);
                return Ok(map);
            });
        }
        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Get Dashboard Totals by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/featured/types", Name = "GraphicFeaturedTypesPolicies")]
        public IHttpActionResult GetDashboardTypes(PolicyPeriodFilter intervaldays, PolicyTypeView typeview)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicPolicy = _policyService.GetTotalDashboardTypes(filters, intervaldays, typeview);

                if (graphicPolicy.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicPolicy);
            });
        }

        /// <summary>
        /// Get total by interval
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total/interval", Name = "GraphicIntevalTotalPolicies")]
        public IHttpActionResult GetTotalInterval(PolicyIntervalFilter interval)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();

                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicInsured = _policyService.GetTotalInterval(filters, interval);

                if (graphicInsured.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        /// <summary>
        /// Get total by general
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet,Route("graphic/total/general", Name = "GraphicGeralTotalPolicies")]
        public IHttpActionResult GetTotalGeneral()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();

                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicInsured = _policyService.GetTotalGeneral(filters);

                if (graphicInsured.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        /// <summary>
        /// Count InEffect Policies
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/types")]
        public IHttpActionResult CountInEffect()
        {
            return TryCatch(() =>
            {
                var policiesInEffect = _policyService.CountInEffect(BrokerageId);

                if (policiesInEffect.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(policiesInEffect);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastYear", Name = "GraphicLastYearPolicies")]
        public IHttpActionResult GetTotalLastYear()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();

                filters.Add(x => x.Broker.BrokerageId == BrokerageId);
                filters.Add(x => x.Type == PolicyType.New);

                var graphicPolicy = _policyService.GetTotalLastYear(filters);

                if (graphicPolicy.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicPolicy);
            });
        }

        /// <summary>
        /// Get Total by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/renewals/total", Name = "GraphicRenewalsPolicies")]
        public IHttpActionResult GetRenewalsTotal()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Policy>();
                filters.Add(x => x.Broker.BrokerageId == BrokerageId);

                var graphicPolicy = _policyService.GetRenewalsTotal(filters);

                if (graphicPolicy.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicPolicy);
            });
        }

        #endregion

        #region Claims
        const string PolicyClaimsRoute = "PolicyClaims";
        /// <summary>
        /// Get Claims
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/claims", Name = PolicyClaimsRoute)]
        public IHttpActionResult Claims(long id, string sort = "-ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                Filters<Claim> filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Id == id);
                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claimsPaged, PolicyClaimsRoute);
            });
        }

        /// <summary>
        /// Create a Claim async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpPost, Route("{id}/claims")]
        public IHttpActionResult CreateClaim(long id, InputClaimDTO claimIn)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.Create(id, claimIn);
                return Created(claimOut);
            });
        }
        #endregion

        #region Policy

        [SwaggerOk(typeof(OutputPolicyDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}/regen")]
        public IHttpActionResult RegenPolicy(long id)
        {
            return TryCatch(() =>
            {
                return Ok(_acordService.RegenPdf(id));

            });
        }

        #endregion

    }
}
