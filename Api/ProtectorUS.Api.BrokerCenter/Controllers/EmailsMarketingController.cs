﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System;
    using System.Text;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/emailsMarketing")]
    public class EmailsMarketingController : BrokerageAuthController
    {
        readonly IEmailMarketingService _emailMarketingService;
        public EmailsMarketingController(
            IBrokerageService brokerageService, 
            IEmailMarketingService emailMarketingService)
            : base(brokerageService, 
                  emailMarketingService)
        {
            _emailMarketingService = emailMarketingService;
        }

        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id, ResultType resultType)
        {
            return TryCatch(() =>
            {
                var emailmarketing = _emailMarketingService.Get(id);
                switch (resultType)
                {
                    case ResultType.Base64:
                        return Ok(emailmarketing.Html.ToBase64Encode());
                    case ResultType.File:
                        string fileName = emailmarketing.Title.Simplify() + ".html";
                        byte[] buffer = Encoding.UTF8.GetBytes(emailmarketing.Html);
                        return File(fileName, buffer);
                }
                return Ok(emailmarketing);
            });
        }

        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpPost, Route("")]
        public IHttpActionResult Create(EmailMarketingDTO emailMarketingIn)
        {
            return TryCatch(() =>
            {
                _emailMarketingService.Create(emailMarketingIn, IdUserAuthenticated, BrokerageId);
                return Created();
            });
        }
    }


}