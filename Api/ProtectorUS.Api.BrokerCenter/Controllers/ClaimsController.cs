﻿using System.Web.Http;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using System.Threading.Tasks;
    using System.Linq;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/claims")]
    public class ClaimsController : BrokerageAuthController
    {
        readonly IClaimService _claimService;
        public ClaimsController(
            IBrokerageService brokerageService, 
            IClaimService claimService)
            : base(brokerageService, 
                  claimService)
        {
            _claimService = claimService;
            Onauthorization += CheckIfIsOwnBusiness;
        }
        /// <summary>
        /// Checks if the Claim being edited is owned by Brokerage authenticated Broker.
        /// </summary>
        protected void CheckIfIsOwnBusiness(AuthEventArg e)
        {
            object claimId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (claimId != null)
            {
                e.IsAuth = _claimService.IsOwnBusiness(BrokerageId, Convert.ToInt64(claimId)) ? e.IsAuth : false;
            }
        }

        #region CRUD
        const string ClaimsRoute = "Claims";
        /// <summary>
        /// Get Claims async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = ClaimsRoute)]
        public IHttpActionResult GetAll(string search = null, long? productId = null, [FromUri] ClaimStatus[] status = null, string sort = "-ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                Filters<Claim> filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Broker.BrokerageId == BrokerageId);

                if (search != null) filters.Add(x => x.Number.Contain(search));
                if (productId != null) filters.Add(x => x.Policy.Condition.ProductId == productId);
                if (status.HasItem()) filters.Add(x => status.Contains(x.Status));

                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(claimsPaged, ClaimsRoute);
            });
        }

        /// <summary>
        /// Get a Claim async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.Get(id);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Attach Claim Documents async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{id}/documents")]
        public IHttpActionResult AttachDocuments(long id, string[] documentsPath)
        {
            return TryCatch(() =>
           {
               var claimOut = _claimService.AttachDocuments(id, documentsPath);
               return Created(claimOut);
           });
        }
        ///// <summary>
        ///// Delete Claim Document async
        ///// </summary>
        [SwaggerOk(typeof(OutputClaimDocumentDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{claimId}/documents/{documentId}")]
        public async Task<IHttpActionResult> DeleteDocument(long claimId, long documentId)
        {
            return await TryCatchAsync(async () =>
            {
                var claimDocument = await _claimService.DeleteDocumentAsync(claimId, documentId);
                return Ok(claimDocument);
            });
        }

        /// <summary>
        /// Delete Claim Document async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDocumentDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}/documents")]
        public IHttpActionResult DeleteDocument(long id, [FromUri] string fileName)
        {
            return TryCatch(() =>
            {
                var claimDocument = _claimService.DeleteDocument(id, fileName);
                return Ok(claimDocument);
            });
        }

        /// <summary>
        /// Change the Status of Claim
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPut, Route("{id}/status/{status}")]
        public IHttpActionResult ChangeStatus(long id, ClaimStatus status)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.ChangeStatus(id, status, this.IdUserAuthenticated);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Update a Claim
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpPut, Route("{id}")]
        public IHttpActionResult Update(long id, InputClaimDTO claimIn)
        {
            return TryCatch(() =>
            {
                _claimService.Update(id, claimIn);
                return Created();
            });
        }

        #endregion

        #region Graphic&Numbers
        /// <summary>
        /// Get Dashboard Totals by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total/interval", Name = "GraphicIntervalTotalClaims")]
        public IHttpActionResult GetTotalInterval(ClaimPeriodFilter interval)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Broker.BrokerageId == BrokerageId);
                var graphicPolicy = _claimService.GetTotalInterval(interval,filters);

                if (graphicPolicy.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicPolicy);
            });
        }
        #endregion

        #region MapPoints
    
        /// <summary>
        ///  Get Claims Map
        /// </summary>
        [SwaggerOk(typeof(MapPointDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("map")]
        public async Task<IHttpActionResult> GetMapPoints()
        {
            return await TryCatchAsync(async() =>
            {
                var filters = new Filters<Claim>();
                filters.Add(x => x.Policy.Broker.BrokerageId == BrokerageId);

                var map = await _claimService.GetMapPointsAsync(filters);
                return Ok(map);
            });
        }
        #endregion

    }
}
