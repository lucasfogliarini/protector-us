﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication.StatusCode;
    [ValidateModelState]
    //[AuthorizationPermissions]
    [RoutePrefix("api/brochures")]
    public class BrochuresController : BrokerageAuthController
    {
        readonly IBrochureService _brochureService;
        readonly IBlobService _blobservice;
        public BrochuresController(
            IBrokerageService brokerageService, 
            IBrochureService brochureService, 
            IBlobService blobservice)
            : base(brokerageService, 
                  brochureService)
        {
            _brochureService = brochureService;
            _blobservice = blobservice;
        }

        /// <summary>
        /// Create a Brochure
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public IHttpActionResult Create(InputBrochureDTO brochureIn)
        {
            return TryCatch(() =>
            {
                _brochureService.Create(brochureIn, IdUserAuthenticated, BrokerageId);
                return Created();
            });
        }
    }


}