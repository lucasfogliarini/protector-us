using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Communication.StatusCode;
    using DTO;
    using Model.Services;
    using Model;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/brokerage")]
    public class BrokerageController : BrokerageAuthController
    {
        readonly IClaimService _claimService;
        readonly IInsuredService _insuredService;
        readonly IPolicyService _policyService;
        readonly IBrokerService _brokerService;

        public BrokerageController(
            IBrokerageService brokerageService,
            IClaimService claimService,
            IInsuredService insuredService,
            IPolicyService policyService,
            IBrokerService brokerService) 
            : base(brokerageService, 
                  claimService, 
                  insuredService,
                  policyService,
                  brokerService)
        {
            _claimService = claimService;
            _insuredService = insuredService;
            _policyService = policyService;
            _brokerService = brokerService;
        }

        #region CRUD
        
        /// <summary>
        /// Create and associate a Charge Account
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017)), SwaggerStatusCode(nameof(MessageStatusCode.EL050)), SwaggerStatusCode(nameof(MessageStatusCode.EL051))]
        [HttpPost, Route("chargeAccount")]
        public override IHttpActionResult CreateChargeAccount(ChargeAccountDTO chargeAccountIn)
        {
            return base.CreateChargeAccount(chargeAccountIn);
        }

        /// <summary>
        /// Update the Charge Account
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017)), SwaggerStatusCode(nameof(MessageStatusCode.EL051))]
        [HttpPut, Route("chargeAccount")]
        public IHttpActionResult UpdateChargeAccount(ChargeAccountDTO chargeAccountIn)
        {
            return TryCatch(() =>
            {
                _brokerageService.UpdateChargeAccount(BrokerageId, chargeAccountIn.LegalEntity, chargeAccountIn.BankAccount, GetIPAddress(chargeAccountIn.AcceptTOS));
                return Ok();
            });
        }

        /// <summary>
        /// Get the Charge Account
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL048))]
        [HttpGet, Route("chargeAccount")]
        public IHttpActionResult GetChargeAccount()
        {
            return TryCatch(() =>
            {
                ChargeAccount chargeAccount = _brokerageService.GetChargeAccount(BrokerageId);
                return Ok(chargeAccount);
            });
        }

        /// <summary>
        /// Brokerage Onboarding
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageOnboardDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("onboarding")]
        public IHttpActionResult Onboarding()
        {
            return TryCatch(() =>
            {
                var brokerageOut = _brokerageService.OnBoarding(BrokerageId);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Update the Brokerage
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)),SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("")]
        public override IHttpActionResult Update(InputBrokerageUpdateDTO brokerageIn)
        {
            return base.Update(brokerageIn);
        }

        /// <summary>
        /// Get a Brokerage async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public IHttpActionResult Get()
        {
            return TryCatch(() =>
            {
                var brokerageOut = _brokerageService.GetWithOnlyArgoActiveProducts(BrokerageId);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Test a alias to Brokerage
        /// </summary>
        [SwaggerOk(typeof(bool)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("test/{alias}/alias", Name = "brokeragetestalias")]
        public IHttpActionResult TestAlias(string alias)
        {
            return TryCatch(() =>
            {
                var result = _brokerageService.TestAlias(alias, BrokerageId);
                return Ok(result);
            });
        }

        /// <summary>
        /// Update default Brokarage webpages
        /// </summary>
        [SwaggerOk(typeof(BrokerageDefaultWebPageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("defaultWebpage")]
        public  IHttpActionResult PutDefaultWebpage(BrokerageDefaultWebPageDTO webpageIn)
        {
            return  TryCatch(() =>
            {
                var brokerageOut =  _brokerageService.UpdateDefaultWebPage(BrokerageId,webpageIn);
                return Ok(brokerageOut);
            });
        }

        /// <summary>
        /// Toggle Product
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("enableProduct")]
        public IHttpActionResult ToggleProduct(BrokerageToggleProductDTO productToggle)
        {
            return TryCatch(() =>
            {
                _brokerageService.ToggleProduct(BrokerageId, productToggle);
                return Ok();
            });
        }
        #endregion
    }
}
