﻿using System.Web.Http;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;

    [RoutePrefix("api/webpageViews")]
    [AuthorizationPermissions]
    public class WebpageViewsController : BrokerageAuthController
    {
        readonly IWebpageViewService _viewService;
        public WebpageViewsController(
            IBrokerageService brokerageService, 
            IWebpageViewService viewService)
            : base(brokerageService, 
                  viewService)
        {
            _viewService = viewService;
        }
        
        /// <summary>
        /// Get Dashboard Totals by Status (transactions) 
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total/interval", Name = "GraphicIntervalTotalViews")]
        public IHttpActionResult GetTotalInterval(PeriodFilter period)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<WebpageView>();
                filters.Add(x => x.BrokerageId == BrokerageId);

                var graphicView = _viewService.GetTotalInterval(filters, period);

            if (graphicView.Count <= 0)
                return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicView);
            });
        }

        /// <summary>
        /// Get Dashboard Web Sites by Week
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/total/week", Name = "GraphicWeekTotalViews")]
        public IHttpActionResult GetTotalWeek()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<WebpageView>();
                filters.Add(x => x.BrokerageId == BrokerageId);

                var graphicView = _viewService.GetTotalWeek(filters, PeriodFilter.Week);

                if (graphicView.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicView);
            });
        }

        /// <summary>
        /// Get WebBanner`s
        /// </summary>
        [SwaggerOk(typeof(List<GraphicDTO>)), SwaggerInternalServerError]
        [HttpGet, Route("graphic/total/webbanners", Name = "GraphicIntervalWebBannersViews")]
        public IHttpActionResult GetTotalWebBanner(PeriodFilter period)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<WebpageView>();
                filters.Add(x => x.BrokerageId == BrokerageId);
                var graphicView = _viewService.GetTotalWebBanner(filters, period);

                if (graphicView.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicView);
            });
        }

    }
}
