using System.Web.Http;
using System.Linq;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;

    using Model;
    using System.Linq;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [AuthorizationPermissions]
    [RoutePrefix("api/brokers")]
    public class BrokersController : BrokerageAuthController
    {
        readonly IBrokerService _brokerService;
        public BrokersController(
            IBrokerageService brokerageService, 
            IBrokerService brokerService)
            : base(brokerageService,
                  brokerService)
        {
            _brokerService = brokerService;
            Onauthorization += CheckIfIsMember;
        }

        /// <summary>
        /// Checks if the Broker being edited is member of Brokerage authenticated Broker.
        /// </summary>
        protected void CheckIfIsMember(AuthEventArg e)
        {
            object brokerId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (brokerId != null)
            {
                e.IsAuth = _brokerService.IsMember(Convert.ToInt64(brokerId), BrokerageId) ? e.IsAuth : false;
            }
        }

        #region CRUD

        const string BrokersRoute = "Brokers";
        /// <summary>
        /// Get Brokers async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputBrokerListDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = BrokersRoute)]
        public IHttpActionResult GetAll(string search = null, long? stateid = null, [FromUri] long[] status = null, string sort = "FirstName", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Broker>();
                filters.Add(x => x.BrokerageId == BrokerageId);
                if (search != null) filters.Add(x => x.FirstName.Contain(search) || x.LastName.Contain(search) || (x.FullName).Contain(search) || x.Email.Contain(search));
                if (stateid != null) filters.Add(x => x.Addresses.FirstOrDefault().Region.City.StateId == stateid);
                if (status.HasItem()) filters.Add(x => status.Contains((int)x.Status));

                var brokers = _brokerService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(brokers, BrokersRoute);
            });
        }

        /// <summary>
        /// Update a Broker async
        /// </summary>
        [SwaggerOk(typeof(OutputProfileBrokerDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}")]
        public async Task<IHttpActionResult> Update(long id, BrokerUpdateDTO brokerIn)
        {
            return await TryCatchAsync(async() =>
            {
                var brokerOut = await _brokerService.UpdateAsync(id, brokerIn);
                return Ok(brokerOut);
            });
        }

        /// <summary>
        /// Get a Broker
        /// </summary>
        [SwaggerOk(typeof(OutputProfileBrokerDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public async Task<IHttpActionResult> Get(long id)
        {
            return await TryCatchAsync(async() =>
            {
                var brokerOut = await _brokerService.GetAsync(id);
                return Ok(brokerOut);
            });
        }

        /// <summary>
        /// Create Broker async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Create(BrokerInsertDTO broker)
        {
            return await TryCatchAsync(async() =>
            {
                var brokerOut = await _brokerService.CreateAsync(BrokerageId, broker);
                return Created(brokerOut);
            });
        }

        /// <summary>
        /// Delete a Broker
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            return await TryCatchAsync(async() =>
            {
                var broker = await _brokerService.DeleteAsync(id);
                return Ok(broker);
            });
        }
        #endregion        

        #region Graphic&Numbers
        /// <summary>
        /// Get total by status
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/status", Name = "GraphicStatusBrokers")]
        public IHttpActionResult GetTotalStatus()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Broker>();
                filters.Add(x => x.BrokerageId == BrokerageId);
                var graphicInsured = _brokerService.GetTotalTypes(filters);

                if (graphicInsured.List.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }
        /// <summary>
        /// Get total new brokers in last month
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastMonth", Name = "GraphicLastMonthBrokers")]
        public IHttpActionResult GetTotalLastMonth()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Broker>();
                filters.Add(x => x.BrokerageId == BrokerageId);
                var graphicInsured = _brokerService.GetTotalLastMonth(filters);

                if (graphicInsured.List.Count() <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        /// <summary>
        /// Get total new brokers in last year
        /// </summary>
        [SwaggerOk(typeof(GraphicDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("graphic/lastYear", Name = "GraphicLastYearBrokers")]
        public IHttpActionResult GetTotalLastYear()
        {
            return TryCatch(() =>
            {
                var filters = new Filters<Broker>();
                filters.Add(x => x.BrokerageId == BrokerageId);

                var graphicInsured = _brokerService.GetTotalLastYear(filters);

                if (graphicInsured.Count() <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(graphicInsured);
            });
        }

        #endregion

        /// <summary>
        /// Inactivate  Brokers
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerInactivateReactivate)), SwaggerInternalServerError]
        [HttpGet, Route("inactivate")]
        public async Task<IHttpActionResult> Inactivate()
        {
            return await TryCatchAsync(async () =>
            {
                var brokersOut = await _brokerService.InactivateReactivate();
                return Ok(brokersOut);
            });
        }
    }
}
