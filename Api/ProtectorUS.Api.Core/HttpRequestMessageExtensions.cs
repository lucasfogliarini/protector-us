﻿using Microsoft.Owin;
using System.Linq;
using System.Net.Http;

namespace System.Web.Http
{
    //ref: http://www.strathweb.com/2013/05/retrieving-the-clients-ip-address-in-asp-net-web-api/
    public static class HttpRequestMessageExtensions
    {
        private const string MSHttpContext = "MS_HttpContext";
        private const string MSOwinContext = "MS_OwinContext";

        public static byte[] GetClientIpAddress(this HttpRequestMessage request)
        {
            try
            {
                string ipAddress = "";
                if (request.Properties.ContainsKey(MSOwinContext))
                {
                    OwinContext owinContext = request.Properties[MSOwinContext] as OwinContext;
                    if (owinContext != null)
                    {
                        ipAddress = owinContext.Request.RemoteIpAddress;
                    }
                }
                if (request.Properties.ContainsKey(MSHttpContext))
                {
                    dynamic httpContext = request.Properties[MSHttpContext];
                    if (httpContext != null)
                    {
                        ipAddress = httpContext.Request.UserHostAddress;
                    }
                }
                return ipAddress.Split('.').Select(c => Convert.ToByte(c)).ToArray();
            }
            catch
            {
                return null;
            }            
        }
    }
}
