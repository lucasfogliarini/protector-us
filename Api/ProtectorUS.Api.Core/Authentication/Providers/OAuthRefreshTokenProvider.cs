﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;

namespace ProtectorUS.Api.Authentication.Providers
{
    using Autofac;
    using Model;
    using System.Security.Claims;
    public class OAuthRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly IContainer _container;
        RefreshToken refreshingToken;
        public OAuthRefreshTokenProvider(IContainer container)
        {
            _container = container;
        }
        public Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientid = context.Ticket.Identity.GetClaimValue(User.ClaimTypes.client_id);

            if (string.IsNullOrEmpty(clientid))
            {
                return Task.CompletedTask;
            }
            DateTime issuedDate = context.Ticket.Properties.IssuedUtc.GetValueOrDefault().UtcDateTime;
            DateTime expiresDate = context.Ticket.Properties.ExpiresUtc.GetValueOrDefault().UtcDateTime;
            expiresDate = expiresDate.AddHours(1);
            context.Ticket.Properties.ExpiresUtc = expiresDate;
            _container.BeginLifetimeScope((authService) =>
            {
                string protectedTicket = context.SerializeTicket();
                RefreshToken newToken = null;
                if (refreshingToken == null)
                {
                    newToken = authService.CreateToken(clientid, context.Ticket.Identity.Name, issuedDate, expiresDate, protectedTicket);
                }
                else
                {
                    newToken = authService.RefreshToken(refreshingToken, issuedDate, expiresDate, protectedTicket);
                    refreshingToken = null;
                }
                context.SetToken(newToken.RefreshTokenId);
            });
            
            return Task.CompletedTask;
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            _container.BeginLifetimeScope((authService) =>
            {
                refreshingToken = authService.GetRefreshToken(context.Token);

                if (refreshingToken != null)
                {
                    context.DeserializeTicket(refreshingToken.ProtectedTicket);
                }
            });
            return Task.CompletedTask;
        }

        #region useless
        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }
        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}