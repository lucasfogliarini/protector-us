﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Claims = System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace ProtectorUS.Api.Authentication.Providers
{
    using Model;
    using Model.Services;
    using Exceptions;
    using Communication;
    using Communication.StatusCode;
    using DTO;
    using Autofac;
    using System.Web.Http;
    using Autofac.Core.Lifetime;
    using System;
    using System.Web.Cors;
    using System.Net.Http;
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private User _user;
        private OutputAppClientDTO _appClient;
        private readonly IContainer _container;

        public OAuthProvider(IContainer container)
        {
            _container = container;
        }
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            try
            {
                _container.BeginLifetimeScope((authService)=>
                {
                    _appClient = authService.TryFindAppClient(clientId);
                });
                context.Validated();
            }
            catch (BaseException ex)
            {
                SetResponseError(context, ex.MessageCode);
            }
            return Task.CompletedTask;
        }
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                _container.BeginLifetimeScope((authService) =>
                {
                    string userType = GetUserType(context.Request);
                    if (userType == typeof(Insured).Name)
                    {
                        _user = authService.Authenticate<Insured>(context.UserName, context.Password);
                    }
                    else if (userType == typeof(Broker).Name)
                    {
                        _user = authService.Authenticate<Broker>(context.UserName, context.Password);
                    }
                    else
                    {
                        _user = authService.Authenticate<ManagerUser>(context.UserName, context.Password);
                    }
                    if (_appClient.ClientId == AppClient.Android || _appClient.ClientId == AppClient.IOS)
                    {
                        string deviceId = GetDeviceId(context.Request);
                        authService.AddDevice(_user, deviceId);
                    }

                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.IncludeClaim(ClaimTypes.Name, context.UserName);
                    identity.IncludeClaim(ClaimTypes.Role, _user.GetType().Name);
                    identity.IncludeClaim(User.ClaimTypes.IdUserAuthenticated, _user.Id.ToString());
                    identity.IncludeClaim(User.ClaimTypes.client_id, context.ClientId);
                    IEnumerable<Claims.Claim> newPermissionClaims = GetPermissionClaims(_user.Id);
                    identity.AddClaims(newPermissionClaims);

                    if (_user is Broker)
                    {
                        Broker broker = _user as Broker;
                        identity.IncludeClaim(Broker.ClaimTypes.BrokerageId, broker.BrokerageId.ToString());
                    }
                    var ticket = new AuthenticationTicket(identity, null);
                    context.Validated(ticket);
                });                
            }
            catch (BaseException ex)
            {
                SetResponseError(context, ex.MessageCode);
            }

            return Task.CompletedTask;
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            if (_user != null)
            {
                if (_user is Broker)
                {
                    _container.BeginLifetimeScope((authService) =>
                    {
                        Broker broker = _user as Broker;
                        var brokerMaster = authService.GetMasterByBrokerage(broker.BrokerageId);
                        if (broker.Id == brokerMaster.Id && brokerMaster.Brokerage.Status == BrokerageStatus.Onboarding)
                        {
                            context.AdditionalResponseParameters.Add("brokerage_onboarding", true);
                        }
                    });
                }
                context.AdditionalResponseParameters.Add("user_id", _user.Id);
                bool onBoarding = _user.Status == UserStatus.Onboarding;
                context.AdditionalResponseParameters.Add("onboarding", onBoarding);
            }

            return Task.CompletedTask;
        }
        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Identity.GetClaimValue(User.ClaimTypes.client_id);
            if (originalClient == context.ClientId)
            {
                context.Ticket.Identity.RemoveAllClaims(c => c.Type == User.ClaimTypes.permission);
                IEnumerable<Claims.Claim> newPermissionClaims = GetPermissionClaims(_user.Id);
                context.Ticket.Identity.AddClaims(newPermissionClaims);
                var newTicket = new AuthenticationTicket(context.Ticket.Identity, context.Ticket.Properties);
                context.Validated(newTicket);
            }
            else
            { 
                SetResponseError(context, new MessageCode(m => m.EL007));
            }
            return Task.CompletedTask;
        }
        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            context.OwinContext.Response.Headers.Append(CorsConstants.AccessControlAllowOrigin, Core.CorsProtectorUS.GetOrigins);
            context.OwinContext.Response.Headers.Append(CorsConstants.AccessControlAllowMethods, $"{HttpMethod.Options.Method}, {HttpMethod.Post.Method}");
            context.OwinContext.Response.Headers.Append(CorsConstants.AccessControlAllowHeaders, "*");
            if (context.IsTokenEndpoint && context.Request.Method == HttpMethod.Options.Method)
            {
                context.RequestCompleted();
            }
            return base.MatchEndpoint(context);
        }

        #region private methods

        private void SetResponseError<TOptions>(BaseValidatingContext<TOptions> context, MessageCode messageCode)
        {
            context.Response.StatusCode = (int)messageCode.StatusCode();
            context.SetError(messageCode.Code, messageCode.Message);
        }
        private IEnumerable<Claims.Claim> GetPermissionClaims(long userId)
        {
            IEnumerable<Claims.Claim> claims = null;
            _container.BeginLifetimeScope((authService) =>
            {
                IEnumerable<OutputPermissionDTO> permissions = authService.FindPermissions(userId);
                claims = permissions.Select(p => new Claims.Claim(User.ClaimTypes.permission, p.ActionApi));
            });
            return claims;
        }
        private FormCollection GetOwinFormCollection(IOwinRequest request)
        {
            return request.Environment["Microsoft.Owin.Form#collection"] as FormCollection;
        }
        private string GetUserType(IOwinRequest request)
        {
            FormCollection formCollection = GetOwinFormCollection(request);
            return formCollection["user_type"];
        }
        private string GetDeviceId(IOwinRequest request)
        {
            FormCollection formCollection = GetOwinFormCollection(request);
            return formCollection["device_id"];
        }

        #endregion
    }
}