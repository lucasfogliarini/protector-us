﻿using Autofac;
using Autofac.Core.Lifetime;
using ProtectorUS.Model.Services;
using System;

namespace ProtectorUS.Api
{
    public static class IContainerExtensions
    {
        public static void BeginLifetimeScope(this IContainer container, Action<IAuthenticationService> action)
        {
            using (var lifetimeScope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag))
            {
                var authService = lifetimeScope.Resolve<IAuthenticationService>();
                action(authService);
            }
        }
    }
}