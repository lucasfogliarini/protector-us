﻿using System.Linq;
using System.Web.Http.Controllers;
using System.Security.Claims;

namespace ProtectorUS.Api
{
    public class AuthorizationPermissions : Authorization
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
        }
        protected override bool IsAuthorized()
        {
            return base.IsAuthorized() && PermissionsAuthorized();
        }
        private bool PermissionsAuthorized()
        {
            return ClaimsPrincipal.Claims.Any(p => p.Type == "permission" && p.Value == ActionFullName);
        }
    }
}