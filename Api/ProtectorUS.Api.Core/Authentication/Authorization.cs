﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;


namespace ProtectorUS.Api
{
    using ProtectorUS.Api.Controllers;
    using ProtectorUS.Communication;
    using ProtectorUS.Communication.StatusCode;
    public class Authorization : ActionFilterAttribute
    {
        public ClaimsPrincipal ClaimsPrincipal { get { return BaseController.User as ClaimsPrincipal; } }
        public HttpActionContext ActionContext { get; private set; }
        public BaseController BaseController { get { return ActionContext.ControllerContext.Controller as BaseController; } }
        public string ActionName { get { return ActionContext.ActionDescriptor.ActionName; } }
        public string ActionFullName { get { return $"{ControllerName}.{ActionName}"; } }
        public string ControllerName { get { return ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName; } }
        public Dictionary<string, object> ActionArguments { get { return ActionContext.ActionArguments; } }
        public HttpMethod RequestMethod { get { return ActionContext.Request.Method; } }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            ActionContext = actionContext;
            if (!IsAuthenticated())
            {
                Unauthorized(m => m.EL008);
                return;
            }
            if (!IsAuthorized())
            {
                Unauthorized(m => m.EL002);
            }
        }
        private bool IsAuthenticated()
        {
            var eventArg = new BaseController.AuthEventArg()
            {
                AuthorizationFilter = this,
                IsAuth = ClaimsPrincipal != null && ClaimsPrincipal.Identity.IsAuthenticated
            };
            BaseController.OnAuthentication(eventArg);
            return eventArg.IsAuth;
        }
        protected virtual bool IsAuthorized()
        {
            var eventArg = new BaseController.AuthEventArg()
            {
                AuthorizationFilter = this,
                IsAuth = true
            };
            BaseController.OnAuthorization(eventArg);
            return eventArg.IsAuth;
        }
        protected void Unauthorized(Expression<Func<MessageCode.MessageCodes, string>> expressionMessage)
        {
            var msg = new MessageCode(expressionMessage);
            ActionContext.Response = BaseController.Request.CreateResponse(msg.StatusCode(), msg);
        }
    }
}