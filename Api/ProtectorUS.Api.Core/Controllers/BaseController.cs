﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using System.Security.Claims;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace ProtectorUS.Api.Controllers
{
    using Communication.StatusCode;
    using Exceptions;
    using Model;
    using DTO;
    using Model.Services;
    using Microsoft.ApplicationInsights;
    using Configuration;
    using System.Net.Http.Headers;
    using System.Net.Http;
    using System.IO;
    using System.Text;
    using Core;
    using Microsoft.ApplicationInsights.Extensibility;

    public abstract class BaseController : ApiController
    {

        readonly TelemetryClient telemetry;
        protected const int maxPageSize = 10;
        protected BaseController(params IBaseService[] services)
        {
            TelemetryConfiguration.Active.InstrumentationKey = ProtectorUSConfiguration.GetConfig().ApplicationInsights.Key;
            telemetry = new TelemetryClient();
            telemetry.TrackTrace("Starting the app, this trace goes to the default source");
            // telemetry.InstrumentationKey = ProtectorUSConfiguration.GetConfig().ApplicationInsights.Key;
            //Set the logged user to current thread
            Thread.CurrentPrincipal = HttpContext.Current.User;
            foreach (var service in services)
            {
                service.InitializeLogger(User.Identity.Name);
            }

        }
        protected IHttpActionResult Catch(Exception exception)
        {
            var properties = exception.Data.Keys.OfType<string>().ToDictionary(key => key, key => exception.Data[key] as string);
            IHttpActionResult actionResult;
            if (exception is BaseException)
            {
                BaseException baseException = exception as BaseException;
                var statusCode = baseException.MessageCode.StatusCode();
                properties.Add("httpStatusCode", statusCode.ToString());
                actionResult =  Content(statusCode, baseException.MessageCode);
            }
            else
            {
                actionResult = InternalServerError(exception);
            }
            telemetry.TrackException(exception, properties);
            return actionResult;
        }

        protected IHttpActionResult TryCatch(Func<IHttpActionResult> _try)
        {
            try
            {
                return _try();
            }
            catch (Exception ex)
            {
                return Catch(ex);
            }
        }
        protected async Task<IHttpActionResult> TryCatchAsync(Func<Task<IHttpActionResult>> _try)
        {
            try
            {
                return await _try();
            }
            catch (Exception ex)
            {
                return Catch(ex);
            }
        }
        protected override ExceptionResult InternalServerError(Exception exception)
        {
            return base.InternalServerError(new SystemException(exception));
        }
        protected IHttpActionResult PaginatedResult<TOut>(PagedResult<TOut> pagedResult, string routeName) where TOut : IOutputDTO
        {
            if (pagedResult.Items.Count <= 0)
                return StatusCode(System.Net.HttpStatusCode.NoContent);

            // ensure the page size isn't larger than the maximum.
            if (pagedResult.ItemsPerPage > maxPageSize)
            {
                pagedResult.ItemsPerPage = maxPageSize;
            }

            var totalPages = (int)Math.Ceiling((double)pagedResult.TotalItems / pagedResult.ItemsPerPage);
            
            var prevLink = pagedResult.Page > 1 ? Url.Link(routeName,
                new
                {
                    page = pagedResult.Page - 1,
                    pageSize = pagedResult.ItemsPerPage,
                    sort = pagedResult.Sort
                }) : "";

            var nextLink = pagedResult.Page < totalPages ? Url.Link(routeName,
                new
                {
                    page = pagedResult.Page + 1,
                    pageSize = pagedResult.ItemsPerPage,
                    sort = pagedResult.Sort
                }) : "";


            var paginationHeader = new
            {
                currentPage = pagedResult.Page,
                pageSize = pagedResult.ItemsPerPage,
                totalCount = pagedResult.TotalItems,
                totalPages = totalPages,
                previousPageLink = prevLink,
                nextPageLink = nextLink
            };
            var response = HttpContext.Current.Response;
            response.AddHeader(CorsProtectorUS.CurrentPage, pagedResult.Page.ToString());
            response.AddHeader(CorsProtectorUS.TotalPages, totalPages.ToString());
            response.AddHeader(CorsProtectorUS.ItemsPerPage, pagedResult.ItemsPerPage.ToString());
            response.AddHeader(CorsProtectorUS.TotalItems, pagedResult.TotalItems.ToString());
            response.AddHeader(CorsProtectorUS.PrevItems, prevLink);
            response.AddHeader(CorsProtectorUS.NextItems, nextLink);

            return Ok(pagedResult.Items);
        }
        protected CreatedNegotiatedContentResult<T> Created<T>(T content, string location = "")
        {
            return Created(location, content);
        }
        /// <summary>
        /// 201 content: ""
        /// </summary>
        protected CreatedNegotiatedContentResult<string> Created()
        {
            return Created("");
        }
        protected ResponseMessageResult File(string fileName, byte[] buffer)
        {
            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            response.Content = new ByteArrayContent(buffer);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };
            string mimeType = MimeMapping.GetMimeMapping(fileName);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(mimeType);
            return ResponseMessage(response);
        }
        protected ClaimsIdentity ClaimsIdentity
        {
            get
            {
                var identity = User?.Identity;
                if (identity is ClaimsIdentity)
                {
                    return identity as ClaimsIdentity;
                }
                return null;
            }
        }
        protected long IdUserAuthenticated
        {
            get
            {
                try
                {
                    string idUser = ClaimsIdentity.FindFirst(Model.User.ClaimTypes.IdUserAuthenticated).Value;
                    return Convert.ToInt64(idUser);
                }
                catch
                {
                    throw new AuthenticationException();
                }
            }
        }
        public event Action<AuthEventArg> Onauthorization;
        public event Action<AuthEventArg> Onauthentication;
        internal void OnAuthorization(AuthEventArg eventArg)
        {
            if(Onauthorization != null)
                Onauthorization(eventArg);
        }
        internal void OnAuthentication(AuthEventArg eventArg)
        {
            if (Onauthentication != null)
                Onauthentication(eventArg);
        }

        public class AuthEventArg
        {
            public Authorization AuthorizationFilter { get; set; }
            public bool IsAuth { get; set; }
        }

        public ForbiddenResult Forbidden()
        {
            return new ForbiddenResult(this.Request);
        }

        public NoContentResult NoContent()
        {
            return new NoContentResult(this.Request);
        }
    }
}