﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    [ValidateModelState]
    [RoutePrefix("api/states")]
    public class StatesController : BaseController
    {
        readonly ILocalizationService _localizationService;
        public StatesController(ILocalizationService localizationService)
            : base(localizationService)
        {
            _localizationService = localizationService;
        }

        /// <summary>
        /// Get States async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputStateDTO>)), SwaggerInternalServerError]
        [HttpGet, Route("")]
        public  IHttpActionResult GetAll()
        {
            return  TryCatch(() =>
            {
                var states =  _localizationService.GetStates();
                return Ok(states);
            });
        }
    }
}
