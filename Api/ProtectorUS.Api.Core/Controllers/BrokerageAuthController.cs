﻿using ProtectorUS.DTO;
using ProtectorUS.Exceptions;
using ProtectorUS.Model.Services;
using System;
using System.Web.Http;
using System.Linq;
using System.Net.Http;

namespace ProtectorUS.Api.Controllers
{
    public abstract class BrokerageAuthController : BaseController
    {
        protected readonly IBrokerageService _brokerageService;
        public BrokerageAuthController(IBrokerageService brokerageService, params IBaseService[] services) : base(services)
        {
            _brokerageService = brokerageService;
            Onauthorization += CheckChargeAccountStatus;

            brokerageService.InitializeLogger(User.Identity.Name);
        }
        protected byte[] GetIPAddress(bool? acceptTOS)
        {
            return acceptTOS.GetValueOrDefault() ? Request.GetClientIpAddress() : null;
        }

        #region BrokerageController Actions
        public virtual IHttpActionResult CreateChargeAccount(ChargeAccountDTO chargeAccountIn)
        {
            return TryCatch(() =>
            {
                _brokerageService.CreateChargeAccount(BrokerageId, chargeAccountIn.LegalEntity, chargeAccountIn.BankAccount, GetIPAddress(chargeAccountIn.AcceptTOS));
                return Ok();
            });
        }
        public virtual IHttpActionResult Update(InputBrokerageUpdateDTO brokerageIn)
        {
            return TryCatch(() =>
            {
                var brokerageOut = _brokerageService.Update(BrokerageId, brokerageIn);
                return Ok(brokerageOut);
            });
        }
        #endregion
        private bool IsAuthorizedAction(AuthEventArg e)
        {
            //can be a Get
            if (e.AuthorizationFilter.RequestMethod == HttpMethod.Get)
                return true;

            //must be a BrokerageController action;
            if (e.AuthorizationFilter.ControllerName != "Brokerage")
                return false;

            string[] authorizedActions =
            {
                nameof(CreateChargeAccount),
                nameof(Update),
            };

            return authorizedActions.Contains(e.AuthorizationFilter.ActionName);
        }
        private void CheckChargeAccountStatus(AuthEventArg e)
        {
            bool authorized = IsAuthorizedAction(e);
            if (!authorized)
            {
                e.IsAuth = _brokerageService.CheckChargeAccountStatus(BrokerageId) ? e.IsAuth : false;
            }
        }
        protected long BrokerageId
        {
            get
            {
                try
                {
                    string brokerageId = ClaimsIdentity.FindFirst(Model.Broker.ClaimTypes.BrokerageId).Value;
                    return Convert.ToInt64(brokerageId);
                }
                catch
                {
                    throw new AuthenticationException();
                }
            }
        }        
    }
}