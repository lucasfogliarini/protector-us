﻿using ProtectorUS.Model.Services;
using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Communication.StatusCode;
    using DTO;
    using System.Threading.Tasks;
    [RoutePrefix("api/appClients")]
    public class AppClientsController : BaseController
    {
        readonly IAppClientService _appClientService;
        public AppClientsController(IAppClientService appClientService) : base(appClientService)
        {
            _appClientService = appClientService;
        }

        /// <summary>
        /// Get a AppClient
        /// </summary>
        [SwaggerOk(typeof(OutputAppClientDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES006))]
        [HttpGet, Route("{clientId}")]
        public IHttpActionResult Get(string clientId)
        {
            return TryCatch(() =>
            {
                var appClientOut = _appClientService.TryFind(clientId);
                return Ok(appClientOut);
            });            
        }
        /// <summary>
        /// Update AppClient async
        /// </summary>
        [SwaggerOk(typeof(OutputAppClientDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL043)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{clientId}")]
        public async Task<IHttpActionResult> Update(string clientId, string token, InputAppClientDTO appClientIn)
        {
            return await TryCatchAsync(async() =>
            {
                var appClientOut = await _appClientService.UpdateAsync(clientId, token, appClientIn);
                return Ok(appClientOut);
            });
        }
    }
}