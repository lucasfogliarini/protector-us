﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;

namespace ProtectorUS.Api.Controllers
{
    using Communication.StatusCode;
    using Exceptions;
    using Model.Services;
    using System;
    using System.IO;
    [RoutePrefix("api/blob")]
    [Authorization]
    public class BlobController : BaseController
    {
        readonly IBlobService _blobservice;
        public BlobController(IBlobService blobService) : base(blobService)
        {
            _blobservice = blobService;
        }
        private async Task<StreamContent> GetStreamContent()
        {
            try
            {
                if (Request.Content.IsMimeMultipartContent("form-data"))
                {
                    MultipartMemoryStreamProvider multipartMemoryStream = await Request.Content.ReadAsMultipartAsync();
                    int totalFiles = multipartMemoryStream.Contents.Where(x => x.Headers.ContentDisposition.DispositionType == "form-data" && !string.IsNullOrEmpty(x.Headers.ContentDisposition.FileName)).Count();
                    if (totalFiles == 1)
                        return multipartMemoryStream.Contents.Where(x => x.Headers.ContentDisposition.DispositionType == "form-data" && !string.IsNullOrEmpty(x.Headers.ContentDisposition.FileName)).FirstOrDefault() as StreamContent;
                    else
                        throw new BadRequestException(x => x.EL047, $"Send files: {totalFiles}" );
                }
            }
            catch
            {
            }
            return null;
        }

        private async Task<IHttpActionResult> UploadAsync(BlobType blobType, string fileName = null)
        {
            return await TryCatchAsync(async () =>
            {
                StreamContent streamContent = await GetStreamContent();
                string blobName = await _blobservice.UploadAsync(blobType, streamContent, fileName);
                return Created(blobName);
            });
        }

        private IHttpActionResult DownloadBase64String(BlobType blobType, string blobName)
        {
            return TryCatch(() =>
            {
                MemoryStream mStream = _blobservice.DownloadStream(blobType, blobName);
                return Ok(mStream.ToBase64String());
            });
        }

        #region Images
        /// <summary>
        /// Upload Image Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpPost, Route("images")]
        public async Task<IHttpActionResult> UploadImage()
        {
            return await UploadAsync(BlobType.Image);
        }

        /// <summary>
        /// Download Image Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpGet, Route("images")]
        public IHttpActionResult DownloadImage(string blobName)
        {
            return DownloadBase64String(BlobType.Image, blobName);
        }
        #endregion

        #region ClaimDocuments
        /// <summary>
        /// Upload ClaimDocument Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpPost, Route("claimDocuments")]
        public async Task<IHttpActionResult> UploadClaimDocument()
        {
            return await UploadAsync(BlobType.ClaimDocument);
        }

        /// <summary>
        /// Download ClaimDocument Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpGet, Route("claimDocuments")]
        public IHttpActionResult DownloadClaimDocument(string blobName)
        {
            return DownloadBase64String(BlobType.ClaimDocument, blobName);
        }
        #endregion

        #region PolicyCetificates
        /// <summary>
        /// Upload PolicyCertificate Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpPost, Route("policyCertificates")]
        public async Task<IHttpActionResult> UploadPolicyCetificate(string blobName = null)
        {
            return await UploadAsync(BlobType.PolicyCertificate, blobName);
        }

        /// <summary>
        /// Download PolicyCertificate Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpGet, Route("policyCertificates")]
        public IHttpActionResult DownloadPolicyCetificate(string blobName)
        {
            return DownloadBase64String(BlobType.PolicyCertificate, blobName);
        }
        #endregion

        #region Policy
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpGet, Route("policy")]
        public IHttpActionResult DownloadPolicy(string blobName)
        {
            return DownloadBase64String(BlobType.Policy, blobName);
        }

        #endregion

        #region ProductTerms
        /// <summary>
        /// Upload ProductTerm Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpPost, Route("productTerms")]
        public async Task<IHttpActionResult> UploadProductTerm()
        {
            return await UploadAsync(BlobType.ProductTerm);
        }

        /// <summary>
        /// Download ProductTerm Blob async
        /// </summary>
        [SwaggerOk(typeof(string)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL026)), SwaggerStatusCode(nameof(MessageStatusCode.EL034), nameof(MessageStatusCode.EL030))]
        [HttpGet, Route("productTerms")]
        public IHttpActionResult DownloadProductTerm(string blobName)
        {
            return DownloadBase64String(BlobType.ProductTerm, blobName);
        }
        #endregion
    }
}
