﻿using System.Web.Http;
namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using System.Net.Http;
    [RoutePrefix("api/paymentGatewayEvents")]
    public class PaymentGatewayEventsController : BaseController
    {
        readonly IPaymentGatewayService _paymentGatewayServiceService;
        public PaymentGatewayEventsController(
            IPaymentGatewayService paymentGatewayServiceService)
            : base(paymentGatewayServiceService)
        {
            _paymentGatewayServiceService = paymentGatewayServiceService;
        }

        [HttpPost, Route("accountUpdated")]
        public IHttpActionResult AccountUpdated(HttpRequestMessage request)
        {
            return TryCatch(() =>
            {
                string paymentGatewayBody = request.Content.ReadAsStringAsync().Result;
                _paymentGatewayServiceService.UpdateChargeAccountStatus(paymentGatewayBody);
                return Ok();
            });
        }
    }
}
