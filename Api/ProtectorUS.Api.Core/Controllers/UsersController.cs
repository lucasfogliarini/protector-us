﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Communication;
    using Communication.StatusCode;
    using DTO;
    using Model.Services;
    using System.Threading.Tasks;
    public abstract class UsersController<T> : BaseController
    {
        protected readonly IUserService<T> _userService;

        public UsersController(IUserService<T> userService, params IBaseService[] services) : base(services)
        {
            _userService = userService;
        }

        public virtual IHttpActionResult ChangePassword(string password)
        {
            return TryCatch(() =>
            {
                MessageCode msgCode = _userService.ChangePassword(IdUserAuthenticated, password);
                return Ok(msgCode);
            });
        }

        public virtual IHttpActionResult ResetPasswordSendInstructions(string email)
        {
            return TryCatch( () =>
            {
                MessageCode message =_userService.ResetPasswordSendInstructions(email);
                return Ok(message);
            });
        }

        public virtual IHttpActionResult ResetPassword(long userId, string token, string newPassword = null)
        {
            return TryCatch(() =>
            {
                var messageCode = _userService.ResetPassword(userId, token, newPassword);
                return Ok(messageCode);
            });
        }                
        public virtual IHttpActionResult UserLogin(string email)
        {
            return TryCatch(() =>
            {
                var user = _userService.FindUser<OutputUserLoginDTO>(email);
                return Ok(user);
            });
        }
        public virtual IHttpActionResult GetPermissions(bool allowed = true)
        {
            return TryCatch(()=>
            {
                var permissions = _userService.GetPermissions(IdUserAuthenticated, allowed);
                return Ok(permissions);
            });
        }
    }
}
