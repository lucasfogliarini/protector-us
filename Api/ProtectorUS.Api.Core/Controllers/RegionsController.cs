﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication.StatusCode;
    [RoutePrefix("api/regions")]
    public class RegionsController : BaseController
    {
        private readonly ILocalizationService _localizationService;

        public RegionsController(ILocalizationService localizationService) : base(localizationService)
        {
            _localizationService = localizationService;
        }

        /// <summary>
        /// Get a Region
        /// </summary>
        [SwaggerOk(typeof(OutputRegionDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL018)), SwaggerStatusCode(nameof(MessageStatusCode.EL018))]
        [HttpGet, Route("{zipCode}")]
        public IHttpActionResult Get(string zipCode)
        {
            return TryCatch(()=>
            {
                var region = _localizationService.GetRegion(zipCode);

                return Ok(region);
            });
        }
    }
}
