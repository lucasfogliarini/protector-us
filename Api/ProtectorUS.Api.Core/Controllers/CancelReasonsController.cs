﻿using System.Web.Http;
using System.Collections.Generic;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [Authorization]
    [RoutePrefix("api/cancelReasons")]
    public class CancelReasonsController : BaseController
    {
        readonly ICancelReasonService _cancelReasonService;
        public CancelReasonsController(ICancelReasonService cancelReasonService)
            : base(cancelReasonService)
        {
            _cancelReasonService = cancelReasonService;
        }

        #region CRUD
        /// <summary>
        /// Get a CancelReason
        /// </summary>
        [SwaggerOk(typeof(OutputCancelReasonDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var cancelReasonOut =  _cancelReasonService.Get(id);
                return Ok(cancelReasonOut);
            });
        }

        /// <summary>
        /// Create a CancelReason async
        /// </summary>
        [SwaggerOk(typeof(OutputCancelReasonDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Create(InputCancelReasonDTO cancelReasonIn)
        {
            return await TryCatchAsync(async() =>
            {
                var cancelReasonOut = await _cancelReasonService.CreateAsync(cancelReasonIn);
                return Created(cancelReasonOut);
            });
        }


        /// <summary>
        /// Get CancelReasons
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputCancelReasonDTO>)), SwaggerInternalServerError]
        [HttpGet, Route("", Name = "CancelReasons")]
        public IHttpActionResult GetAll()
        {
            return  TryCatch(() =>
            {
                var cancelReasons =  _cancelReasonService.GetAll();
                if (cancelReasons.Count <= 0)
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(cancelReasons);
            });
        }

        /// <summary>
        /// Update a Cancel Reason
        /// </summary>
        [SwaggerOk(typeof(OutputCancelReasonDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}")]
        public IHttpActionResult Update(long id, InputCancelReasonDTO insuredIn)
        {
            return TryCatch(() =>
             {
                 var insuredOut =  _cancelReasonService.Update(id, insuredIn);
                 return Ok(insuredOut);
             });
        }

        /// <summary>
        /// Find a Cancel Reason by user update
        /// </summary>
        [SwaggerOk(typeof(OutputCancelReasonDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("findByUpdateUser")]
        public async Task<IHttpActionResult> Find(string user)
        {
            return await TryCatchAsync(async () =>
            {
                var cancelReasonOut = await _cancelReasonService.FindAsync(user);
                return Ok(cancelReasonOut);
            });
        }

        #endregion

    }
}
