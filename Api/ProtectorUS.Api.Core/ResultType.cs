﻿namespace ProtectorUS.Api
{
    public enum ResultType
    {
        Json = 0,
        Base64 = 1,
        File = 2,
    }
}
