﻿using System.ComponentModel;

namespace ProtectorUS.Api.Core
{
    public enum HttpMethod
    {
        [Description("GET")]
        Get = 1,
        [Description("POST")]
        Post = 2,
        [Description("PUT")]
        Put = 3,
        [Description("DELETE")]
        Delete = 4,
        [Description("PATCH")]
        Patch = 5,
    }
}
