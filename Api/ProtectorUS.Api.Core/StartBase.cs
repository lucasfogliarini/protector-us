﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Reflection;
using System.Web.Http;

namespace ProtectorUS.Api
{
    using DTO;
    using Core;
    using Data.IoC;
    using Diagnostics.IoC;
    using Integrations.IoC;
    using Model.Validators.IoC;
    using Services.IoC;
    using Api.Authentication.Providers;
    using Newtonsoft.Json.Serialization;
    using Newtonsoft.Json;
    using Swashbuckle.Application;
    using System.IO;
    using Swagger;
    using System.Configuration;
    using Configuration;
    using Bugsnag.Clients;
    using System.Web.Http.Cors;

    /// <summary>
    /// About Owin: http://www.asp.net/aspnet/overview/owin-and-katana/owin-middleware-in-the-iis-integrated-pipeline
    /// About Owin Startup class: http://www.asp.net/aspnet/overview/owin-and-katana/owin-startup-class-detection
    /// </summary>
    public abstract class StartBase
    {
        protected IAppBuilder OwinApp { get; private set; }
        private HttpConfiguration HttpConfiguration { get; set; } = new HttpConfiguration();
        private ContainerBuilder ContainerBuilder { get; set; } = new ContainerBuilder();
        /// <summary>
        /// API version of application displayed on swagger page.
        /// </summary>
        protected virtual string ApiVersion { get; set; } = "0.19.0";
        /// <summary>
        /// Title of application displayed on swagger page.
        /// </summary>
        protected virtual string Title { get; set; } = "ProtectorUS";
        /// <summary>
        /// Xml documents path of application displayed on swagger page.
        /// </summary>
        protected virtual string XmlCommentsPath { get; set; } = Path.Combine(AppDomain.CurrentDomain.RelativeSearchPath, "ProtectorUS.Api.xml");

        readonly ProtectorUSConfiguration protectorUSConfig = ProtectorUSConfiguration.GetConfig();

        public void Configuration(IAppBuilder owinApp)
        {
            //Keep in this order!
            OwinApp = owinApp;
            ConfigureJsonSerialization();
            ConfigureSwagger();
            IContainer container = RegisterDependencies();//1
            ConfigureOAuth(container, GetTokenExpirationTime());//2
            OwinApp.UseAutofacMiddleware(container);//2
            UseApi();//3
        }

        private TimeSpan GetTokenExpirationTime()
        {
            string tokenExpirationMinutes = ConfigurationManager.AppSettings["tokenExpirationMinutes"];
            int time;
            if (int.TryParse(tokenExpirationMinutes, out time))
                return TimeSpan.FromMinutes(time);

            string tokenExpirationDays = ConfigurationManager.AppSettings["tokenExpirationDays"];
            if (int.TryParse(tokenExpirationDays, out time))
                return TimeSpan.FromDays(time);

            var environment = ProtectorUSConfiguration.GetConfig().Env;
            switch (environment)
            {
                case ProtectorUSEnvironment.Development:
                    return TimeSpan.FromDays(365);
                case ProtectorUSEnvironment.Homolog:
                    return TimeSpan.FromDays(365);
            }

            return TimeSpan.FromMinutes(30);
        }
        private void ConfigureSwagger()
        {
            if (protectorUSConfig.Env != ProtectorUSEnvironment.Production)
            {
                HttpConfiguration.EnableSwagger(c =>
                {
                    c.SingleApiVersion(ApiVersion.Replace('.','-'), Title);
                    c.IncludeXmlComments(XmlCommentsPath);
                    c.DocumentFilter<AuthenticationDocumentFilter>();
                }).EnableSwaggerUi();
            }
        }
        private void UseApi()
        {
            ConfigureCors();//1
            ConfigureRoutes();//2
            OwinApp.UseWebApi(HttpConfiguration);//3
            OwinApp.UseAutofacWebApi(HttpConfiguration);//3
        }
        private void ConfigureCors()
        {
            var corsPolicyProvider = CorsProtectorUS.GetCorsPolicy();
            HttpConfiguration.EnableCors(corsPolicyProvider);
            //OwinApp.UseCors(CorsProtectorUS.GetOwinOptions());//Cors is applied in OAuthProvider.ValidateClientAuthentication
        }
        private void ConfigureRoutes()
        {
            HttpConfiguration.MapHttpAttributeRoutes();

            var bugsnagSettings = ConfigurationManager.GetSection("bugsnagConfig") as Bugsnag.ConfigurationStorage.IConfigurationStorage;
            var bugsnagKey = bugsnagSettings.ApiKey;
            var bugsnag = new BaseClient(bugsnagKey);
            bugsnag.Config.ReleaseStage = protectorUSConfig.Env.ToString();

            HttpConfiguration.Filters.Add(WebAPIClient.ErrorHandler());

            HttpConfiguration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
        private void ConfigureJsonSerialization()
        {            
            HttpConfiguration.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }
        /// <summary>
        /// OAuth Bearer: http://self-issued.info/docs/draft-ietf-oauth-v2-bearer.html
        /// </summary>
        private void ConfigureOAuth(IContainer container, TimeSpan accessTokenExpireTimeSpan)
        {
            //Authorization options
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/Token"),
                AccessTokenExpireTimeSpan = accessTokenExpireTimeSpan,
                Provider = new OAuthProvider(container),
                RefreshTokenProvider = new OAuthRefreshTokenProvider(container),
            };

            // Token Generation
            OwinApp.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            OwinApp.UseOAuthAuthorizationServer(OAuthServerOptions);
        }
        private IContainer RegisterDependencies()
        {
            Assembly[] assemblyModules = new Assembly[]
            {
                typeof(RepositoriesModule).Assembly,
                typeof(ServicesModule).Assembly,
                typeof(AutoMapperModule).Assembly,
                typeof(ValidatorsModule).Assembly,
                typeof(DiagnosticsModule).Assembly,
                typeof(IntegrationServicesModule).Assembly
            };
            ContainerBuilder.RegisterAssemblyModules(assemblyModules);
            ContainerBuilder.RegisterApiControllers(GetType().Assembly, typeof(Controllers.BaseController).Assembly);
            return ContainerBuilder.Build();
        }
    }
}