﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Linq;

namespace ProtectorUS.Api
{
    using ProtectorUS.Exceptions;
    public class ValidateModelState : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var modelStateErrors = actionContext.ModelState.Values.SelectMany(m => m.Errors).Select(x => x.ErrorMessage);
                string messages = string.Join(", ", modelStateErrors);
                actionContext.Response = actionContext.Response ?? new System.Net.Http.HttpResponseMessage();
                actionContext.Response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                actionContext.Response.ReasonPhrase = messages;
            }
        }
    }
}