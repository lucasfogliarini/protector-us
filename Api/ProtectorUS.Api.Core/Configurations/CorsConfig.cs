﻿using Microsoft.Owin.Cors;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Core
{
    /// <summary>
    /// ref: http://benfoster.io/blog/aspnet-webapi-cors
    /// </summary>
    public class CorsConfig : CorsOptions
    {
        private HttpMethod[] MethodsAllowed = { HttpMethod.Get, HttpMethod.Post, HttpMethod.Put, HttpMethod.Delete, HttpMethod.Patch };
        readonly CorsPolicy CorsPolicy = new CorsPolicy();
        public CorsConfig(string[] originsAllowed, string[] exposedHeaders = null, HttpMethod[] methodsAllowed = null)
        {
            ConfigureOrigins(originsAllowed);
            ConfigureHeaders(exposedHeaders);
            ConfigureMethods(methodsAllowed);
            PolicyProvider = new CorsPolicyProvider()
            {
                PolicyResolver =  request => Task.FromResult(CorsPolicy)
            };
        }
        private void ConfigureOrigins(string[] originsAllowed)
        {
            CorsPolicy.AllowAnyOrigin = true;
            if (originsAllowed.HasItem())
            {
                CorsPolicy.AllowAnyOrigin = false;
                originsAllowed.ForEach(o => CorsPolicy.Origins.Add(o));
            }
        }
        private void ConfigureHeaders(string[] exposedHeaders)
        {
            CorsPolicy.AllowAnyHeader = true;
            exposedHeaders.ForEach(o => CorsPolicy.ExposedHeaders.Add(o));
        }
        private void ConfigureMethods(HttpMethod[] methodsAllowed)
        {
            if (methodsAllowed.HasItem())
                MethodsAllowed = methodsAllowed;
            foreach (HttpMethod method in MethodsAllowed)
            {
                CorsPolicy.Methods.Add(method.Description());
            }
        }
    }
}
