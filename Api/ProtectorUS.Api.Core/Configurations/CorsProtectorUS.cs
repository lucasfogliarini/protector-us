﻿namespace ProtectorUS.Api.Core
{
    using System.Configuration;
    using System.Web.Http.Cors;
    using System.Collections.Generic;
    public static class CorsProtectorUS
    {
        public static ICorsPolicyProvider GetCorsPolicy()
        {
            var corsPolicy = new EnableCorsAttribute(GetOrigins, "*", "*");
            PaginationHeaders.ForEach(o => corsPolicy.ExposedHeaders.Add(o));
            return corsPolicy;
        }
        public static CorsConfig GetOwinOptions()
        {
            return new CorsConfig(GetAllowedOrigins(), PaginationHeaders);
        }
        private static string[] GetAllowedOrigins()
        {
            if (GetOrigins == null)
                return null;
            return GetOrigins.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
        }
        public readonly static string GetOrigins = ConfigurationManager.AppSettings["cors:origins"] ?? "*";

        public static readonly string[] PaginationHeaders = { CurrentPage, TotalPages, ItemsPerPage, TotalItems, PrevItems, NextItems };
        public const string CurrentPage = "current-page";
        public const string TotalPages = "total-pages";
        public const string ItemsPerPage = "items-per-page";
        public const string TotalItems = "total-items";
        public const string PrevItems = "prev-items";
        public const string NextItems = "next-items";
    }
}
