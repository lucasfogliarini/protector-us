﻿using ProtectorUS.Communication.StatusCode;
using System.Net;

namespace ProtectorUS.Api
{
    public class SwaggerInternalServerError : SwaggerStatusCode
    {
        public SwaggerInternalServerError() : base(nameof(MessageStatusCode.EL014))
        {
        }
    }
}