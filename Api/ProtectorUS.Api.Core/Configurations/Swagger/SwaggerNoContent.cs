﻿using Swashbuckle.Swagger.Annotations;
using System.Net;

namespace ProtectorUS.Api
{
    public class SwaggerNoContent : SwaggerResponseAttribute
    {
        public SwaggerNoContent() : base(HttpStatusCode.NoContent)
        {
        }
    }
}