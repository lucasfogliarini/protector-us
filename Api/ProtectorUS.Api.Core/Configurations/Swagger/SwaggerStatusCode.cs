﻿using ProtectorUS.Communication;
using ProtectorUS.Communication.StatusCode;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ProtectorUS.Api
{
    public class SwaggerStatusCode : SwaggerResponseAttribute
    {
        public SwaggerStatusCode(params string[] codes) : base(GetStatusCode(codes), GetDescription(codes))
        {            
        }
        public static HttpStatusCode GetStatusCode(params string[] codes)
        {
            string code = codes.FirstOrDefault()?? "";
            var statusCodeField = typeof(MessageStatusCode).GetField(code);
            return statusCodeField == null ? HttpStatusCode.InternalServerError : (HttpStatusCode)statusCodeField.GetRawConstantValue();
        }
        public static string GetDescription(params string[] codes)
        {
            MessageCode.MessageCodes msgCodeMessages = new MessageCode.MessageCodes();
            var messagesType = typeof(MessageCode.MessageCodes);
            List<string> messages = new List<string>();
            foreach (string code in codes)
            {
                var msg = messagesType.GetField(code).GetValue(msgCodeMessages).ToString();
                messages.Add($"<b>{code}</b>: <i>{msg}</i>");
            }
            return messages.Join(",<br />");
        }
    }
}