﻿using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Web.Http.Description;

namespace ProtectorUS.Api.Swagger
{
    public class AuthenticationDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            GetToken(swaggerDoc, schemaRegistry);
            RefreshToken(swaggerDoc, schemaRegistry);
        }

        private void GetToken(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry)
        {
            swaggerDoc.paths.Add("/api/token", new PathItem
            {
                post = new Operation
                {
                    summary = "Get Bearer Token",
                    description = @"Get user's bearer token<br/><br/>
                                   <b>user_type:</b> 'Insured', 'Broker' or 'ManagerUser'<br/>
                                   <b>grant_type:</b> password<br/>
                                   <b>client_id:</b> 'web', 'android' or 'ios'<br/><br/>
                                   <b>Users:</b><br/>
                                   <b>user_type:</b> ManagerUser<br/>
                                   <b>username:</b> ethan_admin@protector-argo.com<br/>
                                   <b>password:</b> manageruser1<br/><br/>
                                   <b>user_type:</b> Broker<br/>
                                   <b>username:</b> troy_master@eagleinsurance.com<br/>
                                   <b>password:</b> brokeruser1<br/><br/>
                                   <b>user_type:</b> Insured<br/>
                                   <b>username:</b> shari_insured@eagleinsurance.com<br/>
                                   <b>password:</b> insureduser1",
                    tags = new List<string> { "Authentication" },
                    parameters = new List<Parameter> {
                        new Parameter
                        {
                            type = "string",
                            name = "user_type",
                            required = true,
                            @in = "formData",
                            @default = "ManagerUser"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "username",
                            required = true,
                            @in = "formData",
                            @default = "ethan_admin@protector-argo.com"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "password",
                            required = true,
                            @in = "formData",
                            @default = "manageruser1"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "grant_type",
                            required = true,
                            @in = "formData",
                            @default = "password"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_id",
                            required = true,
                            @in = "formData",
                            @default = "web"
                        }
                    },
                    responses = new Dictionary<string, Response>()
                    {
                        { "200", new Response { description = "", schema = schemaRegistry.GetOrRegister(typeof(string))}}
                    }
                }
            });
        }

        private void RefreshToken(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry)
        {
            swaggerDoc.paths.Add("/api/Token", new PathItem
            {
                post = new Operation
                {
                    summary = "Refresh Bearer Token",
                    description = @"Refresh user's bearer token<br/><br/>
                                   <b>grant_type:</b> refresh_token<br/>
                                   <b>client_id:</b> 'web' or 'mobile'<br/>
                                   <b>refresh_token:</b> { comes along with the token }",
                    tags = new List<string> { "Authentication" },
                    parameters = new List<Parameter> {
                        new Parameter
                        {
                            type = "string",
                            name = "refresh_token",
                            required = true,
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "grant_type",
                            required = true,
                            @in = "formData",
                            @default = "refresh_token"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_id",
                            required = true,
                            @in = "formData",
                            @default = "web"
                        }
                    },
                    responses = new Dictionary<string, Response>()
                    {
                        { "200", new Response { schema = schemaRegistry.GetOrRegister(typeof(string))}}
                    }
                }
            });
        }
    }
}