﻿using ProtectorUS.Communication.StatusCode;
using System.Net;

namespace ProtectorUS.Api
{
    public class SwaggerUnauthorized : SwaggerStatusCode
    {
        public SwaggerUnauthorized() : base(nameof(MessageStatusCode.EL008))
        {
        }
    }
}