﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using Communication.StatusCode;

    [RoutePrefix("api/newsletters")]
    public class NewsLettersController : BaseController
    {
        private readonly INewsLetterService _newsletterService;

        public NewsLettersController(INewsLetterService newsletterService) : base(newsletterService)
        {
            _newsletterService = newsletterService;
        }
        /// <summary>
        /// Create a NewLetter async
        /// </summary>
        /// <remarks>Create a NewLetter</remarks>
        [SwaggerOk(typeof(OutputNewsLetterDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public IHttpActionResult Create(InputNewsLetterDTO newsletterIn)
        {
            return TryCatch( () =>
            {
                OutputNewsLetterDTO newsletterOut = _newsletterService.Create(newsletterIn);
                return Created(newsletterOut);
            });
        }

    }
}
