﻿using System.Web.Http;


namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO.RatingPlan;
    using Communication.StatusCode;
    [RoutePrefix("api/ratingPlans")]
    public class RatingPlansController : BaseController
    {
        private readonly IRatingPlanFormService _service;

        public RatingPlansController(IRatingPlanFormService service) 
            : base(service)
        {
            _service = service;
        }

        /// <summary>
        /// Get a RatingPlanForm
        /// </summary>
        [SwaggerOk(typeof(OutputRatingPlanFormDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{productAlias}/{zipCode}")]
        public IHttpActionResult Get(string productAlias, string zipCode)
        {
            return TryCatch(() =>
            {
                var ratingPlan = _service.GetLatestVersion(productAlias, zipCode);
                return Ok(ratingPlan);
            });
        }
    }
}
