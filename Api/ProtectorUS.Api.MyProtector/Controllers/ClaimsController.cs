﻿using System.Web.Http;
using System.Collections.Generic;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using System;
    using Communication.StatusCode;
    [ValidateModelState]
    [Authorization]
    [RoutePrefix("api/claims")]
    public class ClaimsController : BaseController
    {
        readonly IClaimService _claimService;
        readonly IInsuredService _insuredService;

        public ClaimsController(IClaimService claimService, IInsuredService insuredService)
            : base(claimService, insuredService)
        {
            _insuredService = insuredService;
            _claimService = claimService;
            Onauthorization += CheckIfIsOwner;

        }

        /// <summary>
        /// Checks if the Claim being edited is owned by Brokerage authenticated Broker.
        /// </summary>
        protected void CheckIfIsOwner(AuthEventArg e)
        {
            object claimId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (claimId != null)
            {
                e.IsAuth = _claimService.IsOwner(IdUserAuthenticated, Convert.ToInt64(claimId)) ? e.IsAuth : false;
            }
        }

        /// <summary>
        /// Attach Claim Documents async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{id}/documents")]
        public IHttpActionResult AttachDocuments(long id, string[] documentsPath)
        {
            return TryCatch(() =>
            {
                var claimOut =  _claimService.AttachDocuments(id, documentsPath);
                return Created(claimOut);
            });
        }

        /// <summary>
        /// Delete Claim Document
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDocumentDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}/documents")]
        public IHttpActionResult DeleteDocument(long id, string fileName)
        {
            return TryCatch(() =>
            {
                var claimDocument = _claimService.DeleteDocument(id, fileName);
                return Ok(claimDocument);
            });
        }


        /// <summary>
        /// Delete Claim Document async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDocumentDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpDelete, Route("{id}/documents/{documentId}")]
        public IHttpActionResult DeleteDocument(long id, long documentId)
        {
            return TryCatch(() =>
           {
               var claimDocument = _claimService.DeleteDocument(id, documentId);
               return Ok(claimDocument);
           });
        }


        const string ClaimsRoute = "Claims";
        /// <summary>
        /// Get Claims
        /// Status:
        ///        FNOL = 0,
        ///        Analisys = 1,
        ///        Payment = 2,
        ///        Closed = 3
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet,Route("", Name = ClaimsRoute)]
        public IHttpActionResult GetAll(string sort = "-ClaimDate", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                Filters<Claim> filters = new Filters<Claim>();
                filters.Add(x => x.Policy.InsuredId == this.IdUserAuthenticated);
                var claimsPaged = _claimService.FindAll(page, pageSize, sort, filters);

                return PaginatedResult(claimsPaged, ClaimsRoute);
            });
        }

        /// <summary>
        /// Get a Claim Details async
        /// Status:
        ///        FNOL = 0,
        ///        Analisys = 1,
        ///        Payment = 2,
        ///        Closed = 3
        /// 
        /// 
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var claimOut =  _claimService.Get(id);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Change the Status of Claim
        /// FNOL = 0,
        /// Analisys = 1,
        /// Payment = 2,
        /// Closed = 3
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPut, Route("{id}/status/{status}")]
        public IHttpActionResult ChangeStatus(long id, ClaimStatus status)
        {
            return TryCatch(() =>
            {
                var claimOut = _claimService.ChangeStatus(id, status, this.IdUserAuthenticated);
                return Ok(claimOut);
            });
        }

        /// <summary>
        /// Update a Claim async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpPut, Route("{id}")]
        public IHttpActionResult Update(long id, InputClaimDTO claimIn)
        {
            return TryCatch( () =>
            {
                var claimOut =  _claimService.Update(id, claimIn);
                return Created(claimOut);
            });
        }
    }
}
