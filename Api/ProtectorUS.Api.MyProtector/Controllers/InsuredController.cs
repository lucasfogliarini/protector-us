﻿using System.Web.Http;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System;
using System.Net.Http.Headers;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    using Model;
    using Communication;
    using System.Collections.Generic;
    using System.Net;

    [ValidateModelState]
    [RoutePrefix("api/insured")]
    public class InsuredController : UsersController<Insured>
    {
        readonly ILocalizationService _localizationService;
        readonly IInsuredService _insuredService;
        readonly IPolicyService _policyService;

        public InsuredController(ILocalizationService localizationService, 
            IInsuredService insuredService,
            IPolicyService policyService)
            : base(insuredService,
                  insuredService,
                  localizationService,
                  policyService)
        {
            _insuredService = insuredService;
            _localizationService = localizationService;
            _policyService = policyService;
        }

        #region Authenticated

        /// <summary>
        /// Get a Insured async
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public IHttpActionResult Get()
        {
            return  TryCatch( () =>
            {
                long insuredId = IdUserAuthenticated;
                var insuredOut =  _insuredService.Get(insuredId);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Get User Profile, Broker Agent Information and ours Policies 
        /// </summary>
        [SwaggerOk(typeof(MyProtectorOutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("profile")]
        public IHttpActionResult GetProfile()
        {
            return TryCatch(() =>
            {
                long insuredId = IdUserAuthenticated;
                var insuredOut = _insuredService.GetProfilePolicies(insuredId);
                insuredOut.Policies = null;
                return Ok(insuredOut);
            });
        }


        /// <summary>
        /// Check if the user terms are accepted
        /// </summary>
        [SwaggerOk(typeof(bool)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerOk(nameof(MessageStatusCode.S005)), SwaggerStatusCode(nameof(MessageStatusCode.EL057))]
        [HttpGet, Route("checkTerms")]
        public IHttpActionResult CheckTerms()
        {
            return TryCatch(() =>
            {
                long insuredId = IdUserAuthenticated;
                var acceptedTerms = _insuredService.CheckTerms(insuredId);
                if (acceptedTerms)
                    return Ok(acceptedTerms);
                else
                    return Forbidden();

            });
        }


        /// <summary>
        ///Accept or decline user terms
        /// </summary>
        [SwaggerOk(typeof(InsuredAccepTermsDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerOk(nameof(MessageStatusCode.S005)), SwaggerStatusCode(nameof(MessageStatusCode.EL057))]
        [HttpPut, Route("acceptTerms")]
        public IHttpActionResult AcceptTerms(InsuredAccepTermsDTO input)
        {
            long insuredId = IdUserAuthenticated;
            var accepted = _insuredService.AcceptTerms(insuredId, input.Accept);
            return Ok(accepted);
        }



        /// <summary>
        /// Insured Onboarding
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL029))]
        [HttpGet, Route("onboarding")]
        public IHttpActionResult Onboarding()
        {
            return TryCatch(() =>
            {
                long insuredId = IdUserAuthenticated;
                var insuredOut = _insuredService.Onboarding(insuredId);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Test email to Insured
        /// </summary>
        [SwaggerOk(typeof(bool)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("test/{email}/email")]
        public IHttpActionResult TestEmail(string email, string productAlias = null)
        {
            return TryCatch(() =>
            {
                bool isUsed = _insuredService.IsUsed(email);//productAlias useless for while.
                return Ok(isUsed);
            });
        }

        /// <summary>
        /// Update a Insured async
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("")]
        public IHttpActionResult Update(InsuredSelfUpdateDTO insuredIn)
        {
            return TryCatch( () =>
            {
                var insuredOut =  _insuredService.Update(IdUserAuthenticated, insuredIn);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Update a Insured Profile async
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("updateProfile")]
        public  IHttpActionResult UpdateProfile(InputInsuredProfileDTO insuredIn)
        {
            return  TryCatch( () =>
            {
                var insuredOut =  _insuredService.Update(IdUserAuthenticated, insuredIn);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Update a Insured Picture async
        /// </summary>
        [SwaggerOk(typeof(OutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("updatePicture")]
        public IHttpActionResult UpdatePicture(InputInsuredPictureProfileDTO insuredIn)
        {
            return  TryCatch( () =>
            {
                var insuredOut =  _insuredService.Update(IdUserAuthenticated, insuredIn);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Get active Policies
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<PolicyNumberDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("policies")]
        public IHttpActionResult GetActivePolicies()
        {
            return TryCatch(() =>
            {
                var policies = _policyService.FindAll(IdUserAuthenticated);
                return Ok(policies);
            });
        }


        #region Policies (Mobile)

        /// <summary>
        /// Get User Profile, Broker Agent Information and ours Policies include Claim, Payments and Coverages async
        /// </summary>
        [SwaggerOk(typeof(MyProtectorMobileOutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("policiesWithClaimsAndCoverages")]
        public IHttpActionResult GetPoliciesClaimsCoverages()
        {
            return  TryCatch( () =>
            {
                long insuredId = IdUserAuthenticated;
                var insuredOut =  _insuredService.GetProfilePoliciesClaimsCoverages(insuredId);
                return Ok(insuredOut);
            });
        }

        /// <summary>
        /// Get User Profile, Broker Agent Information and ours Policies async
        /// </summary>
        [SwaggerOk(typeof(MyProtectorOutputInsuredDTO)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("profileAndPolicies")]
        public IHttpActionResult GetProfilePolicies()
        {
            return  TryCatch( () =>
            {
                long insuredId = IdUserAuthenticated;
                var insuredOut =  _insuredService.GetProfilePolicies(insuredId);
                return Ok(insuredOut);
            });
        }

        #endregion

        /// <summary>
        /// Change password
        /// </summary>
        [SwaggerOk(typeof(MessageCode)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("changePassword")]
        public override IHttpActionResult ChangePassword(string password)
        {
            return base.ChangePassword(password);
        }
        #endregion

        #region Not Authenticated
        /// <summary>
        /// Get the User Login
        /// </summary>
        [SwaggerOk(typeof(OutputUserLoginDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/userLogin")]
        public override IHttpActionResult UserLogin(string email)
        {
            return base.UserLogin(email);
        }

        /// <summary>
        /// Get Permissions
        /// </summary>
        [SwaggerOk(typeof(string[])), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("permissions")]
        public override IHttpActionResult GetPermissions(bool allowed = true)
        {
            return base.GetPermissions(allowed);
        }

        /// <summary>
        /// Send instructions for reset the password
        /// </summary>
        [SwaggerOk(nameof(MessageStatusCode.S002)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{email}/resetPassword/sendInstructions")]
        public override IHttpActionResult ResetPasswordSendInstructions(string email)
        {
            return base.ResetPasswordSendInstructions(email);
        }

        /// <summary>
        /// Verify or reset password
        /// </summary>
        /// <remarks>Only verifying: omit the param 'newPassword'<br />Reseting: insert the param 'newPassword'</remarks>
        [SwaggerOk(nameof(MessageStatusCode.S001), nameof(MessageStatusCode.S003)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpPost, Route("{userId}/resetPassword")]
        public override IHttpActionResult ResetPassword(long userId, string token, string newPassword = null)
        {
            return base.ResetPassword(userId, token, newPassword);
        }
        #endregion
    }
}
