﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication.StatusCode;
    using Configuration;
    [ValidateModelState]
    [RoutePrefix("api/orders")]
    public class OrdersController : BaseController
    {
        readonly IOrderService _orderservice;
        readonly IBlobService _blobService;
        readonly ProtectorUSConfiguration _config;

        public OrdersController(IOrderService orderservice, IBlobService blobService)
            : base(orderservice)
        {
            _orderservice = orderservice;
            _blobService = blobService;
            _config = ProtectorUSConfiguration.GetConfig();
        }

        /// <summary>
        /// Create a Order
        /// </summary>
        [SwaggerOk, SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public IHttpActionResult Create(InputOrderDTO order)
        {
            return TryCatch(() =>
            {
               var orderOutput =  _orderservice.Create(order);
               return Created("");
            });
        }

    }
}
