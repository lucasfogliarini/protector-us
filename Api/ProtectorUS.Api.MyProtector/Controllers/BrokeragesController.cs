﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication.StatusCode;
    [ValidateModelState]
    [RoutePrefix("api/brokerages")]
    public class BrokeragesController : BaseController
    {
        readonly IBrokerageService _brokerageService;

        public BrokeragesController(IBrokerageService brokerageService) 
            : base(brokerageService)
        {
            _brokerageService = brokerageService;

        }

        /// <summary>
        /// Get a Brokerage by alias async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageMyProtectorDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public  IHttpActionResult Get(string alias)
        {
            return TryCatch(() =>
            {
                var brokerage =  _brokerageService.FindMyProtector(alias);
                return Ok(brokerage);
            });
        }

        ///// <summary>
        ///// Get a Brokerage async
        ///// </summary>
        //[SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        //[HttpGet, Route("{id}")]
        //public IHttpActionResult Get(long id)
        //{
        //    return  TryCatch(() =>
        //    {
        //        var brokerageOut =  _brokerageService.Get(id);
        //        return Ok(brokerageOut);
        //    });
        //}

        /// <summary>
        /// Get a Brokerage with products async
        /// </summary>
        [SwaggerOk(typeof(OutputBrokerageDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{alias}/products")]
        public IHttpActionResult GetWithProducts(string alias)
        {
            return TryCatch(() =>
            {
                var brokerage =  _brokerageService.Find(alias, true);
                return Ok(brokerage);
            });
        }

        /// <summary>
        /// Get Brokerage map pins
        /// </summary>
        /// <remarks> Get Brokerage map pins</remarks>
        [SwaggerOk(typeof(MapBrokerageMyProtectorDTO)), SwaggerInternalServerError]
        [HttpGet, Route("map/{zipCode}/{aliasProduct}")]
        public IHttpActionResult GetMapPoints(string zipCode, string aliasProduct)
        {
            return TryCatch(() =>
            {
                var map = _brokerageService.FindABroker(zipCode, aliasProduct);
                return Ok(map);
            });
        }
    }
}
