﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System.Threading.Tasks;
    using Communication.StatusCode;
    [ValidateModelState]
    [RoutePrefix("api/webpageViews")]
    public class WebpageViewsController : BaseController
    {
        readonly IWebpageViewService _viewservice;
        public WebpageViewsController(IWebpageViewService viewservice) 
            : base(viewservice)
        {
            _viewservice = viewservice;

        }

        /// <summary>
        /// Create a WebPageView async
        /// </summary>
        [SwaggerOk(typeof(OutputWebpageViewDTO)),SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Create(InputWebpageViewDTO view)
        {
            return await TryCatchAsync(async() =>
            {
                var add = await _viewservice.CreateAsync(view);
                return Created(add);
            });
        }
    }
}
