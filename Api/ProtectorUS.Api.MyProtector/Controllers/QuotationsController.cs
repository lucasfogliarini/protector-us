﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using System;
    using Communication.StatusCode;

    [ValidateModelState]
    [RoutePrefix("api/quotations")]
    public class QuotationsController : BaseController
    {
        private readonly IQuotationService _service;

        public QuotationsController(IQuotationService quotationService)
            : base(quotationService)
        {
            _service = quotationService;
        }

        /// <summary>
        /// Get a Quotation async
        /// </summary>
        /// <remarks>Get a Quotation</remarks>
        [SwaggerOk(typeof(OutputQuotationDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{quotationHash}")]
        public IHttpActionResult Get(string quotationHash)
        {
            return  TryCatch(() =>
            {
                Guid guidHash = Guid.Parse(quotationHash);
                OutputQuotationDTO quotationOut =  _service.Get(guidHash);
                return Ok(quotationOut);
            });
        }

        /// <summary>
        /// Create a Quotation
        /// </summary>
        /// <remarks>Create a Quotation</remarks>
        [SwaggerOk(typeof(QuotationHashDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("")]
        public IHttpActionResult Create(QuotationInsertDTO quotationIn)
        {
            return TryCatch(() =>
            {
                QuotationHashDTO quotationOut =  _service.Create(quotationIn);
                return Created(quotationOut);
            });
        }

        /// <summary>
        /// Create a Contact Broker
        /// </summary>
        /// <remarks>Create a Contact Broker</remarks>
        [SwaggerOk(typeof(QuotationContactDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("contact")]
        public IHttpActionResult Contact(QuotationContactDTO contactIn)
        {
            return TryCatch(() =>
            {
                QuotationContactDTO quotationOut = _service.Contact(contactIn);
                return Created(quotationOut);
            });
        }
    }
}
