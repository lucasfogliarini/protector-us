﻿using System.Web.Http;
using System.Collections.Generic;
using System;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Model;
    using Communication.StatusCode;
    using System.IO;
    [ValidateModelState]
    [Authorization]
    [RoutePrefix("api/policies")]
    public class PoliciesController : BaseController
    {
        readonly IPolicyService _policyService;
        readonly IClaimService _claimService;
        readonly IAcordService _acordService;

        public PoliciesController(
            IPolicyService policyService,
            IClaimService claimService, 
            IAcordService acordService)
            : base(policyService, claimService)
        {
            _policyService = policyService;
            _claimService = claimService;
            _acordService = acordService;
            Onauthorization += CheckIfIsOwner;
            Onauthentication += OnAuth;
        }
        /// <summary>
        /// Checks if the Policy being edited is owned by Brokerage authenticated Broker.
        /// </summary>
        protected void CheckIfIsOwner(AuthEventArg e)
        {
            object policyId = e.AuthorizationFilter.ActionArguments.Get("id");
            if (policyId != null)
            {
                e.IsAuth = _policyService.IsOwner(IdUserAuthenticated, Convert.ToInt64(policyId)) ? e.IsAuth : false;
            }
        }
        protected void OnAuth(AuthEventArg e)
        {
            if (e.AuthorizationFilter.ActionName == nameof(GetPolicyFile))
                e.IsAuth = true;
        }

        const string PoliciesRoute = "Policies";
        /// <summary>
        /// Get Policies
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputPolicyDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("", Name = PoliciesRoute)]
        public IHttpActionResult GetAll(string sort = "-PeriodStart", int page = 1, int pageSize = maxPageSize)
        {
            return TryCatch(() =>
            {
                Filters<Policy> filters = new Filters<Policy>();
                filters.Add(x => x.InsuredId == IdUserAuthenticated);
                var policiesPaged = _policyService.FindAll(page, pageSize, sort, filters);
                return PaginatedResult(policiesPaged, PoliciesRoute);
            });
        }
        
        [SwaggerOk, SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.EL055)), SwaggerStatusCode(nameof(MessageStatusCode.EL043))]
        [HttpGet, Route("{policyId}/file")]
        public IHttpActionResult GetPolicyFile(long policyId, string token)
        {
            return TryCatch(() =>
            {
                var policy = _policyService.Get<Policy>(policyId);
                MemoryStream mStream = _policyService.GetPolicyFile(policy, token);
                return File(policy.PolicyFileName, mStream.GetBuffer());
            });
        }

        /// <summary>
        /// Get a Policy
        /// </summary>
        [SwaggerOk(typeof(OutputPolicyDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(long id)
        {
            return TryCatch(() =>
            {
                var policieOut = _policyService.TryGet<OutputPolicyDTO>(id, p => p.Insured,
                                                             p => p.Claims,
                                                             p => p.Coverages,
                                                             p => p.Broker,
                                                             p => p.Order,
                                                             p => p.Order.Quotation,
                                                             p => p.Broker.Brokerage,
                                                             p => p.Business);
                return Ok(policieOut);
            });
        }

        /// <summary>
        /// Get Policy Claims
        /// Status:
        ///        FNOL = 0,
        ///        Analisys = 1,
        ///        Payment = 2,
        ///        Closed = 3
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputClaimDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/claims")]
        public IHttpActionResult Claims(long id)
        {
            return TryCatch(() =>
            {
                var outClaims =  _claimService.FindAll(id);
                return Ok(outClaims);
            });
        }

        /// <summary>
        /// Create a Claim async
        /// </summary>
        [SwaggerOk(typeof(OutputClaimDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPost, Route("{id}/claims")]
        public IHttpActionResult CreateClaim(long id, InputClaimDTO claimIn)
        {
            return  TryCatch(() =>
            {
                var claimOut =  _claimService.Create(id, claimIn);
                return Created(claimOut);
            });
        }

        /// <summary>
        /// Get Policy Coverages async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputCoverageDTO>)), SwaggerInternalServerError, SwaggerUnauthorized]
        [HttpGet, Route("{id}/coverages")]
        public IHttpActionResult Coverages(long id)
        {
            return  TryCatch(() =>
            {
                var coverages = _policyService.GetCoverages(id);
                if (!coverages.HasItem())
                    return StatusCode(System.Net.HttpStatusCode.NoContent);

                return Ok(coverages);
            });
        }

        /// <summary>
        /// Request cancel a Policy
        /// </summary>
        [SwaggerOk(typeof(OutputPolicyDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005)), SwaggerStatusCode(nameof(MessageStatusCode.ES017))]
        [HttpPut, Route("{id}/requestCancel")]
        public IHttpActionResult RequestCancel(long id, long reasonId)
        {
            return TryCatch(() =>
            {
                var policieOut = _policyService.RequestCancel(id, reasonId, IdUserAuthenticated);
                return Ok(policieOut);
            });
        }


        [SwaggerOk(typeof(OutputPolicyDTO)), SwaggerInternalServerError, SwaggerUnauthorized, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{id}/regen")]
        public IHttpActionResult RegenPolicy(long id)
        {
            return TryCatch(() =>
            {
                return Ok(_acordService.RegenPdf(id));
            });
        }
    }
}
