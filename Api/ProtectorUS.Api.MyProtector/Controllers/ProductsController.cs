﻿using System.Web.Http;

namespace ProtectorUS.Api.Controllers
{
    using Model.Services;
    using DTO;
    using Communication.StatusCode;
    using System.Collections.Generic;

    [RoutePrefix("api/products")]
    public class ProductsController : BaseController
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService) : base(productService)
        {
            _productService = productService;
        }


        /// <summary>
        /// Get a Product Details async
        /// </summary>
        [SwaggerOk(typeof(IEnumerable<OutputProductBaseDTO>)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("")]
        public IHttpActionResult GetAll()
        {
            return TryCatch(() =>
            {
                var product = _productService.GetAll();
                return Ok(product);
            });
        }

        /// <summary>
        /// Get a Product Details async
        /// </summary>
        [SwaggerOk(typeof(OutputProductBaseDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("{alias}")]
        public IHttpActionResult Get(string alias)
        {
            return TryCatch(()=>
            {
                var product =  _productService.Find(alias);
                return Ok(product);
            });
        }

        /// <summary>
        /// Get a Conditions by ZipCode
        /// </summary>
        [SwaggerOk(typeof(OutputProductConditionDTO)), SwaggerInternalServerError, SwaggerStatusCode(nameof(MessageStatusCode.EL005))]
        [HttpGet, Route("file/{alias}/{zipcode}")]
        public  IHttpActionResult GetCondition(string alias, string zipCode)
        {
            return  TryCatch( () =>
            {
                var productCondition =  _productService.GetConditions(alias, zipCode);
                return Ok(productCondition);
            });
        }

    }
}
