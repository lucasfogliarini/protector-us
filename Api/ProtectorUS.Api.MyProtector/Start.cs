﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(ProtectorUS.Api.MyProtector.Start))]
namespace ProtectorUS.Api.MyProtector
{
    public class Start : StartBase
    {
        protected override string Title { get; set; } = "MyProtector";
    }
}